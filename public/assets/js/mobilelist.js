var _isRun = true;
var page = 1;
var next_page_flag = 1;
function adapterScroll(){
  var h=document.documentElement.clientHeight;
  var scrollTop=document.body.scrollTop || document.documentElement.scrollTop;
  var scrollHeigh=document.body.scrollHeight||document.documentElement.scrollHeight;
  var pos=h+scrollTop+126;
  if(pos>=scrollHeigh){
    return true;
  }
  return false;
}
$(window).scroll( function() {
	if(_isRun){
		_isRun = false;
		if(next_page_flag==1 && adapterScroll()){
			var department_id = $('#department_id').val();
			var url = '/mobile-docface/video-page';
			var nowpage = page;
			var data = {
                        'page'			: nowpage,
                        'department_id'	: department_id
					};
			  $("#container").append('<div class="loading_box" id="loading">加载中...</div>');
			  $.post(url, data, function(json){
				  page = nowpage+1;
				  next_page_flag = json.next_page_flag; 
				  $("#container").children('#loading').remove();
				  $("#container").append(json.html);
				  _isRun = true;
			  },'json');
		}else if(next_page_flag == 0){
			$("#container").children('#loading').remove();
			$("#container").append('<div class="loading_box" id="loading">没有更多记录</div>');
			_isRun = true;
		}else{
			_isRun = true;
		}
	}
});

function departmentGroup(departmentid){
	$('#department_id').val(departmentid);
	//选中状态
	$('.department').removeClass('sele');
	$('.department:eq('+departmentid+')').addClass('sele');
	var url = '/mobile-docface/video-page';
	var data = {
                'page'			: 0,
                'department_id'	: departmentid
			};
	  $.post(url, data, function(json){
		  page = 1;
		  next_page_flag = json.next_page_flag; 
		  $("#container").html(json.html);
	  },'json');
}


function timer(intDiff){
    window.setInterval(function(){
    var day=0,
        hour=0,
        minute=0,
        second=0;//时间默认值        
    if(intDiff > 0){
        day = Math.floor(intDiff / (60 * 60 * 24));
        hour = Math.floor(intDiff / (60 * 60)) - (day * 24);
        minute = Math.floor(intDiff / 60) - (day * 24 * 60) - (hour * 60);
        second = Math.floor(intDiff) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60);
    }
    if (day <= 9) day = '0' + day;
    if (hour <= 9) hour = '0' + hour;
    if (minute <= 9) minute = '0' + minute;
    if (second <= 9) second = '0' + second;
    $('#day_show').html(day);
    $('#hour_show').html(hour);
    $('#minute_show').html(minute);
    $('#second_show').html(second);
    intDiff--;
    }, 1000);
} 

$(function(){
	//倒计时
	var intDiff = $('#timestamp_input').val();
    timer(intDiff);
    //科室滚动
    var mySwiper = new Swiper('.swiper-container',{
    width : 100
});
}); 