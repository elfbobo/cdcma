<?php
	

    /**
     * Created by JetBrains PhpStorm.
     * User: taoqili
     * Date: 12-7-18
     * Time: 上午10:42
     */
    header("Content-Type: text/html; charset=utf-8");
    error_reporting(E_ERROR | E_WARNING);
    date_default_timezone_set("Asia/chongqing");
    include "Uploader.class.php";
    //上传图片框中的描述表单名称，
    $title = htmlspecialchars($_POST['pictitle'], ENT_QUOTES);
    $path = htmlspecialchars($_POST['dir'], ENT_QUOTES);

    //上传配置
    $config = array(
    	"savePath" => "../../../../upload/img/" , //保存路径
        "maxSize" => 1000, //单位KB
        "allowFiles" => array(".png", ".jpg"),
    	"savePathS" => "/upload/img/"
    );

    //生成上传实例对象并完成上传
    $up = new Uploader("upfile", $config);

    /**
     * 得到上传文件所对应的各个参数,数组结构
     * array(
     *     "originalName" => "",   //原始文件名
     *     "name" => "",           //新文件名
     *     "url" => "",            //返回的地址
     *     "size" => "",           //文件大小
     *     "type" => "" ,          //文件类型
     *     "state" => ""           //上传状态，上传成功时必须返回"SUCCESS"
     * )
     */
    $info = $up->getFileInfo();
   	$basedir = substr(dirname(__FILE__),0,-30);
   	include $basedir.'/app/controllers/WeixinBaseController.php';
    $data = array('media'=>'@'.$basedir.'public'.str_replace($config['savePath'], $config['savePathS'], $info["url"]));
    $res = WeixinBaseController::getBaseAddMaterialContentImg($data);
    if(!isset($res->errcode)||(isset($res->errcode)&&$res->errcode==0)){
		$wx_url = $res->url;
	}else{
		$wx_url = '';
		$info['state'] = $res->errcode; 
	}
    //$scheme =  $_SERVER['SERVER_PORT'] == '443' ? 'https://' : 'http://';
    //$domin_name = $scheme.$_SERVER['HTTP_HOST'];
    //echo $domin_name.'/admmaterial/content-img?data='.urlencode($data);
    //ob_start();
   // $wx_url = file_get_contents($domin_name.'/admmaterial/content-img?data='.urlencode($data));
	
    /**
     * 向浏览器返回数据json数据
     * {
     *   'url'      :'a.jpg',   //保存后的文件路径
     *   'title'    :'hello',   //文件描述，对图片来说在前端会添加到title属性上
     *   'original' :'b.jpg',   //原始文件名
     *   'state'    :'SUCCESS'  //上传状态，成功时返回SUCCESS,其他任何值将原样返回至图片上传框中
     * }
     */
	$sOldPath = str_replace($config['savePath'], $config['savePathS'], $info["url"]);
    //echo "{'url':'" . $info["url"] . "','title':'" . $title . "','original':'" . $info["originalName"] . "','state':'" . $info["state"] . "'}";
    echo "{'url':'" . $wx_url . "','title':'" . $sOldPath . "','original':'" . $info["originalName"] . "','state':'" . $info["state"] . "'}";

