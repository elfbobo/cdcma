function saveThumb() {
	$.ajaxFileUpload({
		url: "/user/upload-user-thumb",
		secureuri: false,
		fileElementId: "upload_file",
		dataType: "json",
		success: function(data, status) {
			$("#user_thumb").attr("src", data.user_thumb);
//			alert("上传成功")
		}
	})
}

function submit_order(){
	var order_time = $.trim($('#order_time').val());
	var doc_name = $.trim($('#doc_name').val());
	var doc_department = $.trim($('#doc_department').val());
	if(!order_time){
		alert('请您填写预约时间！');
		return;
	}
	if(!doc_name){
		alert('请您填写预约专家！');
		return;
	}
	if(!doc_department){
		alert('请您填写预约专家科室！');
		return;
	}
	$('#order_now').attr('onclick','');
	var url = '/user/order-now-do/'+order_time+'/'+doc_name+'/'+doc_department;
	$.post(url,{},function(msg){
		if(msg=='success'){
			alert('预约成功');
			window.location.href="/user/my-order";
		}
	})
}

function order_do(docid){
	var url = "/user/order-do/"+docid;
	$.post(url,{},function(msg){
		if(msg=='repeat'){
			$.blockUI({ message: $('#already_order_box')});
			return ;
		}else if(msg=='success'){
			$.blockUI({ message: $('#order_success_box')});
			return ;
		}else{
			alert('预约失败，请刷新当前页面重试！');
		}
	});
}
//医生管理拜新同拜阿用户选择提交
function checkDocType(){
	var newarr = new Array();
	var url = "/user/doc-type-do";
	var uarr = new Array();
	$('input.background_gray').each(function(key,val){
		var this_id = val.id;
		var this_length = this_id.length;
		var uid = this_id.substr(10,(this_length-12));
		var type = this_id.substr(-1,1)
		if(type==1){
			var othertype=2;
		}else{
			var othertype=1;
		}
		if(uarr[uid]==othertype){
			uarr[uid] = 3;
		}else{
			uarr[uid] = type;
		}
	});
	for(var i=0;i<uarr.length;i++){
		if(uarr[i]){
			var a = [i,uarr[i]];
			newarr.push(a);
		}
	}
	$.post(url,{'data':JSON.stringify(newarr)},function(msg){
		if(msg=='success'){
			$('#common_pop_box_submit').click(function(){
				window.location.reload();
			})
			showCommonPopBox('提示','操作成功')
			return ;
		}else{
			showCommonPopBox('提示','操作失败，请刷新当前页面后重试')
			return ;
		}
	});
}

//推送调研操作
function pushSurvey(docid){
	var url = "/user/doc-push-survey-do/"+docid;
	$.post(url,{},function(msg){
		if(msg=='success'){
			showCommonPopBox('提示','已成功推送调研')
//			window.location.reload();
			return ;
		}else if(msg=='survey_closed'){
			showCommonPopBox('提示','调研功能已关闭，无法推送调研');
			return ;
		}else{
			showCommonPopBox('提示','操作失败，请刷新当前页面后重试')
			return ;
		}
	});
}

function check_user_type(type,uid,obj){
	toggleClass(obj,"background_gray");
}

Array.prototype.remove = function (dx) {  
    if (isNaN(dx) || dx > this.length) {  
        return false;  
    }  
    for (var i = 0, n = 0; i < this.length; i++) {  
        if (this[i] != this[dx]) {  
            this[n++] = this[i];  
        }  
    }  
    this.length -= 1;  
};  

function hasClass(obj, cls) {  
    return obj.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));  
} 
function addClass(obj, cls) {  
    if (!this.hasClass(obj, cls)) obj.className += " " + cls;  
}  
  
function removeClass(obj, cls) {  
    if (hasClass(obj, cls)) {  
        var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');  
        obj.className = obj.className.replace(reg, ' ');  
    }  
} 
function toggleClass(obj,cls){  
    if(hasClass(obj,cls)){  
        removeClass(obj, cls);  
    }else{  
        addClass(obj, cls);  
    }  
}  
