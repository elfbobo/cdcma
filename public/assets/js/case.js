function saveCase() {
	$.ajaxFileUpload({
		url: "/case/uploadFile",
		secureuri: false,
		fileElementId: "fileField",
		dataType: "json",
		success: function(data, status) {
			if(data.error == 0){
				//清楚数据避免重复上传
				$("#textfield").val('');
				$("#fileField").val('');
				alert("上传成功")
			}else{
				alert(data.error);
			}
			
		}
	})
}
function saveCaseEdit() {
	var caseId = $("#caseId").val();
	$.ajaxFileUpload({
		url: "/case/uploadFileEdit/"+caseId,
		secureuri: false,
		fileElementId: "fileField",
		dataType: "json",
		success: function(data, status) {
			if(data.error == 0){
				//清楚数据避免重复上传
				$("#textfield").val('');
				$("#fileField").val('');
				alert("上传成功")
			}else{
				alert(data.error);
			}
			
		}
	})
}
function saveThumb(part) {//part区分哪部分的图片上传
	$.ajaxFileUpload({
		url: "/case/uploadPic/"+part,
		secureuri: false,
		fileElementId: part,
		dataType: "json",
		success: function(data, status) {
			if(data.error == 0){
				//找空位 补位
				for(i=1;i<=3;i++){
					var fileUrlV = $("#"+part+"_fileUrl"+i).val();
					if(fileUrlV == ''){
						break;
					}
				}
				$("#"+part+"_fileUrl"+i).val(data.fileUrl);//图片链接地址赋值
				$("#"+part+"_thumb"+i).attr("src", data.fileUrl);//展示缩略图
				$("#"+part+"_del"+i).show();//显示删除
				$("#img_box_one"+i).show();//显示图片
				//修改图片数量
				var num = $('.'+part+'_num').val();
				num = parseInt(num) + 1;
				$('.'+part+'_num').val(num);
				if(num >= 4){//关闭上传
					$("."+part).hide();
				}
			}else{
				alert(data.error);
			}
			
		}
	})
}
function delThumb(num) {
	$("#img_box_one"+num).hide();//不显示图片
	$("#sub_exam_thumb"+num).attr("src",'');//图片不显示
	$("#sub_exam_fileUrl"+num).val('');//图片链接删除
	//$("#sub_exam_del"+num).hide();//删除按钮隐藏
	//修改图片的数量
	var nums = $('.sub_exam_num').val();
	nums = parseInt(nums) - 1;
	$('.sub_exam_num').val(nums);
	//图片上传功能开启
	$(".sub_exam").show();
}