function saveGroupThumb() {
	$.ajaxFileUpload({
		url: "/admaspirininfo/group-thumb",
		secureuri: false,
		fileElementId: "upload_file",
		dataType: "json",
		success: function(data, status) {
			$("#info_group_thumb").val(data.info_group_thumb);
			$("#thumb").attr("src", data.info_group_thumb);
			alert("上传成功");
		}
	})
}

function saveInfoThumb() {
	$.ajaxFileUpload({
		url: "/admaspirininfo/info-thumb",
		secureuri: false,
		fileElementId: "upload_file",
		dataType: "json",
		success: function(data, status) {
			$("#info_thumb").val(data.info_thumb);
			$("#thumb").attr("src", data.info_thumb);
			alert("上传成功");
		}
	})
}

function saveInfoVideoThumb() {
	$.ajaxFileUpload({
		url: "/admaspirininfo/video-thumb",
		secureuri: false,
		fileElementId: "upload_file0",
		dataType: "json",
		success: function(data, status) {
			$("#info_video_thumb").val(data.info_video_thumb);
			$("#videothumb").attr("src", data.info_video_thumb);
			alert("上传成功");
		}
	})
}

function saveTrainThumb() {
	$.ajaxFileUpload({
		url: "/admaspirinresearch/train-thumb",
		secureuri: false,
		fileElementId: "upload_file",
		dataType: "json",
		success: function(data, status) {
			$("#train_thumb").val(data.train_thumb);
			$("#thumb").attr("src", data.train_thumb);
			alert("上传成功");
		}
	})
}

function saveVideoThumb() {
	$.ajaxFileUpload({
		url: "/admaspirinresearch/video-thumb",
		secureuri: false,
		fileElementId: "upload_file0",
		dataType: "json",
		success: function(data, status) {
			$("#video_thumb").val(data.video_thumb);
			$("#videothumb").attr("src", data.video_thumb);
			alert("上传成功");
		}
	})
}