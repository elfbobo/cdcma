function saveThumb() {
	$.ajaxFileUpload({
		url: "/admdocface/upload-video-thumb",
		secureuri: false,
		fileElementId: "upload_file",
		dataType: "json",
		success: function(data, status) {
			$("#video_thumb").val(data.video_thumb);
			$("#thumb").attr("src", data.video_thumb);
			alert("上传成功")
		}
	})
}
function saveBanner() {
	$.ajaxFileUpload({
		url: "/admdocfacerec/upload-video-banner",
		secureuri: false,
		fileElementId: "upload_banner",
		dataType: "json",
		success: function(data, status) {
			$("#video_banner").val(data.video_thumb);
			$("#banner").attr("src", data.video_thumb);
			alert("上传成功")
		}
	})
}

//function endTimeChange(start_id='start_time',end_id='end_time'){
//	var start = $('#'+start_id).val();
//	$('#'+end_id).val(start);
//}
/*
//直播倒计时
function clock(){
    var second = $('#second').html();
    var minute = $('#minute').html();
    var hour = $('#hour').html();
    var day = $('#day').html();
    if(second>0){
        second = second-1;
        if(second<10){
          second = '0'+second;
        }
      $('#second').html(second);
    }else{
      $('#second').html(59);
      if(minute>0){
        minute = minute-1;
          if(minute<10){
            minute = '0'+minute;
          }
        $('#minute').html(minute);
      }else{
        $('#minute').html(59);
          if(hour>0){
            hour = hour-1;
              if(hour<10){
                hour = '0'+hour;
              }
            $('#hour').html(hour);
          }else{
            $('#hour').html(23);
              if(day>0){
                day = day-1;
                  if(day<10){
                    day = '0'+day;
                  }
                $('#day').html(day);
              }else{
                $('#day').html('00');
                $('#hour').html('00');
                $('#minute').html('00');
                $('#second').html('00');
                  //直播开始
                location.reload();   
              }
          }
      }
    }
}
*/
//录播点赞
function docface_video_zan(videoid,userid,catid){
	var url = "/docface/video_zan/"+videoid+"/"+userid+"/"+catid;
	$.post(url,{},function(msg){
		if(msg=='is_zan'){
			alert('您已经支持过该视频，无需重复支持！');
		}else if(msg == 'success'){
			var count = parseInt($('#docface_video_zan').html());
			$('#docface_video_zan').html(count+1);
			alert('支持成功！');
		}else{
			alert('支持失败，请刷新当前页面重试！');
		}
	})
}
function comment_zan(commentid,catid){
	var url = "/docface/comment_zan/"+commentid+"/"+catid;
	$.post(url,{},function(data){
		console.log(data.success);
		if(data.success){
			//alert('支持成功');
			$("#comment_zan_"+commentid).html(data.count);
			return false;
		}else{
			alert(data.msg);
			return false;
		}
	},'json');
}
function insertIcon(emtionurl){
	 var contentinfo = document.getElementById("content");
	 contentinfo.innerHTML+="<img src='"+emtionurl+"'>";
}

function trim(str){ //删除左右两端的空格
	return str.replace(/(^\s*)|(\s*$)/g, "");
}

function reply(iId,iCatId){
	 var reply = $("#reply").html();
	 var content = $("#content").html();
	 if(!trim(content)){
		 alert('评论内容不能为空');
		 return false;
	 }
	 $('#replay_box').attr("disabled",true);
	 var comment = reply+content;
	 var url = "/docface/comment-info/"+iId+'/'+iCatId;
	 $.post(url,{sComment:comment},function(data){
		 if(data=='error'){
			 $('#replay_box').removeAttr('disabled');  
			 alert('参数错误');
			 return false;
		 }
		 if(data=='success'){
			 alert('评论成功');
			 if(iCatId==1){
				 window.location.href="/docface/review-show/"+iId;
			 }else if(iCatId==2){
				 window.location.href="/consultation/show/"+iId;
			 }else{
				 window.location.href="/user/train-review/"+iId;
			 }
			 return false;
		 }
	 })
}

function reply_mobile(iId,iCatId){
	 var comment = $("#content").val();
	 if(!trim(comment)){
		 alert('评论内容不能为空');
		 return false;
	 }
	 $('#replay_box').attr("disabled",true);
	 var url = "/docface/comment-info/"+iId+'/'+iCatId+'/2';
	 $.post(url,{sComment:comment},function(data){
		 if(data=='error'){
			 $('#replay_box').removeAttr('disabled');  
			 alert('参数错误');
			 return false;
		 }
		 if(data=='success'){
			 window.location.href="/mobile-docface/review-comment/"+iId;
			 return;
		 }
	 })
}

function video_reply(userid,username){
	 $("#reply").html('回复'+username+"：");
	 $("#reply").show();
	 $("#content").focus();
}

function video_reply_mobile(userid,username){
	 $("#content").val('回复'+username+"：");
	 $("#content").focus();
}

function addUserMinuteLive(videoid,userid){
	var timestamp = Date.parse(new Date());
	var time_second = timestamp/1000;
	var url = "/docface/video_view_live/"+videoid+"/"+userid+"/"+time_second;
	$.post(url,{},function(msg){
		if(msg=='success'){
			console.log('success');
		}
	})
}

function download_video(openurl,catid,videoid){
	var url = "/download-video/"+catid+"/"+videoid;
	$.post(url,{},function(msg){
		if(msg=='success'){
			window.location.href=openurl;
//			var c=window.open();
//			setTimeout(function(){
//			c.location=openurl;
//			}, 300);
		}
	})
}
