function saveThumb() {
	$.ajaxFileUpload({
		url: "/admelearning/upload-thumb",
		secureuri: false,
		fileElementId: "upload_file",
		dataType: "json",
		success: function(data, status) {
			$("#thumb_url").val(data.thumb);
			$("#thumb").attr("src", data.thumb);
			alert("上传成功")
		}
	})
}

function savePdf() {
	$.ajaxFileUpload({
		url: "/admelearning/upload-pdf",
		secureuri: false,
		fileElementId: "upload_file_pdf",
		dataType: "json",
		success: function(data, status) {
			$("#pdf_url").val(data.url);
			alert("上传成功")
		}
	})
}

function saveSwf() {
	$.ajaxFileUpload({
		url: "/admelearning/upload-swf",
		secureuri: false,
		fileElementId: "upload_file_swf",
		dataType: "json",
		success: function(data, status) {
			$("#swf_url").val(data.url);
			alert("上传成功")
		}
	})
}