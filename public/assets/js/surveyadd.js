function saveThumb() {
	$.ajaxFileUpload({
		url: "/admdocface/upload-doc-thumb",
		secureuri: false,
		fileElementId: "upload_file",
		dataType: "json",
		success: function(data, status) {
			$("#doc_thumb").val(data.doc_thumb);
			$("#thumb").attr("src", data.doc_thumb);
			alert("上传成功")
		}
	})
}