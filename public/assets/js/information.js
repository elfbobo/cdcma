function saveThumb() {
	$.ajaxFileUpload({
		url: "/adminformation/upload-thumb",
		secureuri: false,
		fileElementId: "upload_file",
		dataType: "json",
		success: function(data, status) {
			$("#doc_thumb").val(data.doc_thumb);
			$("#thumb").attr("src", data.doc_thumb);
			alert("上传成功")
		}
	})
}

function saveMaterialImage(type){
	$.ajaxFileUpload({
        url:'/admmaterial/image-upload',//用于文件上传的服务器端请求地址
        secureuri:false,//一般设置为false
        fileElementId:"file_upload",//文件上传空间的id属性  <input type="file" id="file" name="file" />
        dataType: 'json',//返回值类型 一般设置为json
        data:{type:type},
        success: function (data, status)  //服务器成功响应处理函数
        {
        	if(data.success){
        		$('#material_id').val(data.material_id);
        		$('#media_id').val(data.media_id);
        		thumb = $('#thumb');
        		thumb.attr('src',data.path);
        		thumb.show().css("display","block");
        	}else{
        		alert(data.msg);
        		return false;
        	}
        }
    })
}
