function savemeeting(){
	
	var abbr = ''; 
	 var point = '';       
	 $('.bigbox >div').each(function(){            
		 abbr = $(this).attr('class'); 
		 if(abbr == 'time_block sele_time'){
			  point += $(this).attr('title') + ',';
		 } 
	}) 
//	alert(point);return false;
	var time_period = point;
	
	var meeting_time = $.trim($('#meeting_time').val());
	var expert_company = $.trim($('#expert_company').val());
	var expert_subject = $.trim($('#expert_subject').val());
	var expert_name = $.trim($('#expert_name').val());
	var meeting_title = $.trim($('#meeting_title').val());
	var creator_name = $.trim($('#creator_name').val());
	var creator_tel = $.trim($('#creator_tel').val());
	var creator_email = $("#creator_email").val();
	
	if(!meeting_time){
		alert('请选择会议日期！');
		return false;
	}
	if(!time_period){
		alert('请选择会议时间！');
		return false;
	}
	if(!expert_company){
		alert('请输入专家单位！');
		return false;
	}
	if(!expert_subject){
		alert('请输入专家科室！');
		return false;
	}
	if(!expert_name){
		alert('请输入专家姓名！');
		return false;
	}
	if(!meeting_title){
		alert('请输入会议主题！');
		return false;
	}
	if(!creator_name){
		alert('请输入创建人姓名！');
		return false;
	}
	if(!creator_tel){
		alert('请输入创建人手机号！');
		return false;
	}
	if(!creator_email){
		alert('请输入创建人邮箱！');
		return false;
	}
	var reg_mobile = /^[0-9]{11}$/;
	istel= reg_mobile.test(creator_tel);
	if (!istel ) {
	    alert("手机号码格式不正确！");
	    return false;
	}
	var reg = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
	ismail= reg.test(creator_email);
	if (!ismail ) {
	    alert("邮箱格式不正确！");
	    return false;
	}

	$('#create_box1').show();
}
function submitbox(){
	$('#create_box1').hide();
	
	var abbr = ''; 
	 var point = '';       
	 $('.bigbox >div').each(function(){            
		 abbr = $(this).attr('class'); 
		 if(abbr == 'time_block sele_time'){
			  point += $(this).attr('title') + ',';
		 } 
	}) 
	
	var time_period = point;
	var meeting_time = $.trim($('#meeting_time').val());
	var expert_company = $.trim($('#expert_company').val());
	var expert_subject = $.trim($('#expert_subject').val());
	var expert_name = $.trim($('#expert_name').val());
	var meeting_title = $.trim($('#meeting_title').val());
	var creator_name = $.trim($('#creator_name').val());
	var creator_tel = $.trim($('#creator_tel').val());
	var creator_email = $("#creator_email").val();
	
	var data = {
			'time_period':time_period,
			'meeting_time':meeting_time,
			'expert_company':expert_company,
			'expert_subject':expert_subject,
			'expert_name':expert_name,
			'meeting_title':meeting_title,
			'creator_name':creator_name,
			'creator_tel':creator_tel,
			'creator_email':creator_email
	};
	var url = '/meeting/create-do';
	$.post(url,data,function(msg){
		if(msg=='success'){
			$('#create_box2').show();
//			window.location.reload();
		}else if(msg=='time error'){
			alert('创建会议必须提前10天，谢谢！');
//			window.location.reload();
		}else{
			alert('提交失败');
//			window.location.reload();
		}
	})	
}

function cancelbox(){
	$('#create_box1').hide();
	return false;
}
