// JavaScript Document

function init(){
	var img1_h=80;//图片1
	var img2_h=536;//图片2
	var mt=0.23;//向上移动的百分比
	var winW=document.documentElement.clientWidth;//屏幕宽度
	var winH=document.documentElement.clientHeight; //屏幕高度
	var allH=document.documentElement.scrollHeight;//body内容高度
	var bg=document.getElementById("bg");
	
	var h=winW*(img1_h+img2_h-mt*640)/640;//中间部分内容高度  
	var conH1=winH-h;
	var conH2=allH-h;
	if(allH<winH){
		bg.style.height=conH1+"px";
	}else{
		bg.style.height=conH2+"px";
	}
}

//调研提交
function submit_survey(){
	$('#survey_form').submit();
}
//为调研绑定提交事件
$(document).ready(function(){
    $('#survey_form').bind('submit', function(){
    	$('.btn').attr('disabled',true);
        ajaxSubmit(this, function(json){
            data = eval("("+json+")");
            if(data.success==false){
            	$('.btn').attr('disabled',false);
                alert(data.msg);
                return;
            }else if(data.success==true){
            	//跳转到结果页面
            	window.location.href=data.url;
            }else{
            	$('.btn').attr('disabled',false);
                alert('网络错误，请刷新当前页面重试，如有问题可拨打电话联系我们');
                return;
            }
        });
        return false;
    });
});

//将form转为AJAX提交
function ajaxSubmit(frm, fn) {
    var dataPara = getFormJson(frm);
    $.ajax({
        url: frm.action,
        type: frm.method,
        data: dataPara,
        success: fn
    });
}

//将form中的值转换为键值对。
function getFormJson(frm) {
    var o = {};
    var a = $(frm).serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });

    return o;
}