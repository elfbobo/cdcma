@extends('mobile.common.layoutasp')
@section('title')线下会议申请 @stop
@section('description')线下会议申请@stop
@section('keywords')线下会议申请@stop
@section('content')
<script>
function jointooffmeeting(id){
		var result = document.getElementById("result");
		var url = '/mobile-user/apply-check';
		var data = {'id':id};
		$.post(url,data,function(date){
			if(date.msg == 'success'){
				alert(date.show);
				window.location.href="/mobile-user/list";
				return;
			}if(date.msg == 'delete'){
				alert(date.show);
				window.location.href="/mobile-user/list";
				return;
			}else{
				alert(date.show);
				return;
			}
		  },'json')
}
function joininmeeting()
{
	var hiddendiv = document.getElementById('shade');
	hiddendiv.style.display ="block";
	var message_box = document.getElementById('message_box');
	message_box.style.display ="block";
}
function hiddenmessage()
{
	var hiddendiv = document.getElementById('shade');
	hiddendiv.style.display ="none";
	var message_box = document.getElementById('message_box');
	message_box.style.display ="none";
}
</script>
 <div class="wrap">
        <div class="page_top_box">
            <input type="button" class="btn_back" onclick="window.location.href='/mobile-user/list'" />
            <div class="page_top">我的会议</div>
            <input type="button" class="btn_search" style="visibility: hidden"/>
        </div>
        <div class="wrap-style1 flex_content">
            <div class="offline-meeting-detail">
                <div class="item-list">
                    <div class="item-flex">
                        <div class="item-title">会议时间：</div>
                        <div class="item-cont">{{date("Y.m.d H:i",strtotime($oOffline->offline_start_time))}}-{{date("H:i",strtotime($oOffline->offline_end_time))}}</div>
                    </div>
                </div>
                <div class="item-list">
                    <div class="item-flex">
                        <div class="item-title">会议主题：</div>
                        <div class="item-cont">{{$oOffline->offline_title}}</div>
                    </div>
                </div>
                <div class="item-list">
                    <div class="item-flex">
                        <div class="item-title">会议简介：</div>
                        <div class="item-cont item-cont2">{{$oOffline->offline_content}}</div>
                    </div>
                </div>
                <div class="item-list">
                    <div class="item-flex">
                        <div class="item-title">需要消耗：</div>
                        <div class="item-cont"><div class="item-price">{{$oOffline->offline_least_gold}}</div></div>
                    </div>
                </div>
                <div class="btn_box">
                 @if($result =='hasnojoin')  
                 	<button class="btn"id="meeting" id="result" onclick="joininmeeting()">申请参加</button>
                 @else  
                 	<button class="btn"id="meeting" id="result" onclick="alert('您已经参加会议')"  >申请参加</button>
                 @endif
                </div>
            </div>
        </div>
    </div>
    <div class="shade" style="display:none;" id="shade"></div>
    <div class="module-window" style="display:none;" id="message_box">
        <div class="txt">
            <p>即将消耗<span style="color:#fd8f2b">{{$oOffline->offline_least_gold}}</span>个积分，</p>
            <p>报名后不能取消，是否参加会议？</p>
        </div>
        <div class="btn_box">
            <button class="btn" onclick="jointooffmeeting({{$oOffline->id}})" >确认参加</button>
            <button class="btn" onclick="hiddenmessage()">取消</button>
        </div>
    </div>
@stop   