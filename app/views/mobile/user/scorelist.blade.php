@extends('mobile.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
<div class="page_top_box">
    <input type="button" class="btn_back" onclick="window.location.href='/mobile-user'"/>
    <div class="page_top">积分排行</div>
</div>
<div class="page_cont">
	<div class="meeting_box2">
        <div class="meeting_tab">
            <a href="/mobile-user/myscore">我的积分</a>
            <a class="sele" href="#">积分排行</a>
        </div>
    	<div class="integral_box integral_bg">
                <div class="integral_top">
                    <div class="integral_icon">
                        <img class="ranking" src="/assets/images/weixin/menu017.png" />
                    </div>
                    <div class="integral_txt1">
                        我的排名：<span>{{$iUserRank}}</span>
                    </div>
                </div>
        </div>
        <div class="ranking_munu_box">
            <a @if($iRoleId==3) class="sele" @endif href="/mobile-user/score-list/3">医生</a>
            <a @if($iRoleId==2) class="sele" @endif href="/mobile-user/score-list/2"> 医学顾问</a>
        </div>
        <div class="ranking_box">
            <table class="tab_ranking" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th>排名</th>
                    <th>用户姓名</th>
                    <th>直播积分</th>
                    <th>录播积分</th>
                    <th>总积分</th>
                </tr>
                @foreach($aUser as $k=>$v)
	            <tr>
	                <td @if($k==0) class="first" @elseif($k==1) class="second" @elseif($k==2) class="third" @endif">{{$k+1}}</td>
	                <td>{{$v['user_name']}}</td>
	                <td>{{$v['user_live_score']}}</td>
	                <td>{{$v['user_review_score']}}</td>
	                <td>{{$v['user_score']}}</td>
	            </tr>
	            @endforeach
            </table>
        </div>
    </div>
</div>    
@stop