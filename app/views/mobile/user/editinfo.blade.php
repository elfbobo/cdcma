@extends('mobile.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/user.js"></script>
<script>
function change_uinfo(){

	//用户基本信息修改
	var user_tel = $.trim($('#user_tel').val());
	var user_email = $.trim($('#user_email').val());
	var user_address = $.trim($('#user_address').val());
	var user_position = $("#user_pos_select  option:selected").val();
	var user_company_name = $.trim($('#user_company_name').val());
	var user_department = $("#user_dep_select  option:selected").val();
	var card_number = $.trim($('#card_number').val());
	var card_thumb = $.trim($('#card_thumb').val());
	if(!user_tel){
		alert('请输入您的手机号码！');
		return false;
	}
	if(!user_email){
		alert('请输入您的邮箱！');
		return false;
	}
	var reg_mobile = /^[0-9]{11}$/;
	istel= reg_mobile.test(user_tel);
	if (!istel ) {
	    alert("手机号码格式不正确！");
	    return false;
	  }
	  var reg = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
	  ismail= reg.test(user_email);
	  if (!ismail ) {
	    alert("邮箱格式不正确！");
	    return false;
	  }

    if(!user_position||user_position==0){
		alert('请选择您的职称！');
		return false;
	}
	if(!user_department||user_department==0){
		alert('请选择您的科室！');
		return false;
	}
	//省份、城市、区县、医院
	var user_province = $("#hospital1  option:selected").val();
	var user_city = $("#hospital2  option:selected").val();
	var user_county = $("#hospital3 option:selected").val();
	var user_company = $("#hospital4  option:selected").val();
	if(!user_province||user_province==0){
		alert('请选择您的省份！');
		return false;
	}
	if(!user_city||user_city==0){
		alert('请选择您的城市！');
		return false;
	}
	if(!user_county||user_county==0){
		alert('请选择您所在的区/县！');
		return false;
	}
	if(!user_company||user_company==0){
		if(!user_company_name){
			alert('请选择您的医院！');
			return false;
		}
	}

	//不修改密码
	var data = {'user_tel':user_tel,
			'user_email':user_email,
			'user_address':user_address,
			'user_position':user_position,
			'user_province':user_province,
			'user_city':user_city,
			'user_county':user_county,
			'user_company':user_company,
			'user_company_name':user_company_name,
			'user_department':user_department,
			'card_number':card_number,
			'card_thumb':card_thumb
			};


	var url = '/user/change-user-info-all';
	$.post(url,data,function(msg){
		if(msg=='nick_repeat'){
			alert('该用户名已经存在，换换其他用户名试试');
		}else if(msg=='tel_repeat'){
			alert('该手机号码用户已经存在了');
		}else if(msg=='email_repeat'){
			alert('该邮箱用户已经存在了');
		}else if(msg=='old_err'){
			alert('原密码输入错误');
		}else if(msg=='success'){
			alert('用户信息修改成功');
			window.location.href='/mobile-user';
		}else{
			alert('网络错误，请刷新当前页面重试');
		}
	})	
}

function change_hospital(id){
    var pid = id-1;
	var hospital = $("#hospital"+pid+"  option:selected").val();
	console.log(hospital);
	var html_null = '<option value="0">=请选择=</option>';
	if(pid==1){
		$('#user_province').val(hospital);
		$("#hospital"+2).html(html_null);
		$("#hospital"+3).html(html_null);
		$("#hospital"+4).html(html_null);
    }else if(pid==2){
    	$('#user_city').val(hospital);
    	$("#hospital"+3).html(html_null);
		$("#hospital"+4).html(html_null);
    }else if(pid==3){
    	$('#user_county').val(hospital);
    	$("#hospital"+4).html(html_null);
    }else {
    	$('#user_company').val(hospital);
        return ;
    }
	var url = '/user/hospital-child/'+hospital;
	$.post(url,{},function(json){
		data = json;
    	if(data=='noinfo'){
			var html = '<option value="0">=请选择=</option>';
			$("#hospital"+id).html(html);
			
		}else{
			var bShow = false;
			var html = '<option value="0">=请选择=</option>';
			for(var elem in data){
				bShow = true;
				html += '<option value="' + elem + '">' + data[elem] + '</option>';
			}
			if(bShow){
				$("#hospital"+id).html(html);
			}
		}	
		},'json');
}
</script>
<div class="page_top_box">
    	<input type="button" class="btn_back" onclick="window.location.href='/mobile-user/user-info'"/>
        <div class="page_top">修改资料</div>
        <input type="button" class="btn_share" style="visibility:hidden;"/>
    </div>
    	<div class="login_box">
            <div class="login_block">
                <div class="login_left txt_right">
                    姓名：
                </div>
                <div class="login_right">
                    <input class="login_txt" type="text" disabled value="{{$oUser->user_name}}" style="background-color:#eee;" />
                </div>
            </div>
            <div class="login_block">
                <div class="login_left txt_right">
                    性别：
                </div>
                <div class="login_right">
                    <input class="login_txt" type="text" disabled value="@if($oUser->user_sex==1) 男 @else 女 @endif" style="background-color:#eee;"/>
                </div>
            </div>
            <div class="login_block">
                <div class="login_left txt_right">
                    手机：
                </div>
                <div class="login_right">
                    <input class="login_txt" type="text" id="user_tel" name="user_tel" value="{{$oUser->user_tel}}"/>
                </div>
            </div>
            <div class="login_block">
                <div class="login_left txt_right">
                    邮箱：
                </div>
                <div class="login_right">
                    <input class="login_txt" type="text" id="user_email" name="user_email" value="{{$oUser->user_email}}"/>
                </div>
            </div>
            <div class="login_block">
                <div class="login_left txt_right">
                    地址：
                </div>
                <div class="login_right">
                    <input class="login_txt" type="text" id="user_address" name="user_address" value="{{$oUser->user_address}}"/>
                </div>
            </div>
            <?php 
            	$aPos = User::getPosition();
            ?>
            <div class="login_block">
                <div class="login_left txt_right">
                    职称：
                </div>
                <div class="login_right">
                    <select id="user_pos_select" class="login_txt" style="width: 101%">
		            <option value="0">=请选择=</option>
		            @foreach($aPos as $k=>$v)
		            <option value="{{$v}}" @if($oUser->user_position==$v) selected @endif>{{$v}}</option>
		            @endforeach
		            </select>
                </div>
            </div>
            <div class="login_block">
                <div class="login_left txt_right">
                    省份：
                </div>
                <div class="login_right">
                    <select id="hospital1" class="login_txt" style="width: 101%" onchange="change_hospital(2)">
			            <option value="0">=请选择=</option>
			            @foreach($aHosp1 as $k=>$v)
			            <option value="{{$k}}" @if($oUser->user_province==$k) selected @endif>{{$v}}</option>
			            @endforeach
			        </select>
                </div>
            </div>
            <div class="login_block">
                <div class="login_left txt_right">
                    城市：
                </div>
                <div class="login_right">
                    <select id="hospital2" class="login_txt" style="width: 101%" onchange="change_hospital(3)">
		            <option value="0">=请选择=</option>
		            @foreach($aHosp2 as $k=>$v)
		            <option value="{{$k}}" @if($oUser->user_city==$k) selected @endif>{{$v}}</option>
		            @endforeach
		            </select>
                </div>
            </div>
            <div class="login_block">
                <div class="login_left txt_right">
                    区/县：
                </div>
                <div class="login_right">
                    <select id="hospital3" class="login_txt" style="width: 101%" onchange="change_hospital(4)">
		            <option value="0">=请选择=</option>
		            @foreach($aHosp3 as $k=>$v)
		            <option value="{{$k}}" @if($oUser->user_county==$k) selected @endif>{{$v}}</option>
		            @endforeach
		            </select>
                </div>
            </div>
            <div class="login_block">
                <div class="login_left txt_right">
                    医院：
                </div>
                <div class="login_right">
                    <select id="hospital4" class="login_txt" style="width: 101%" onchange="change_hospital(5)">
			            <option value="0">=请选择=</option>
			            @foreach($aHosp4 as $k=>$v)
			            <option value="{{$k}}" @if($oUser->user_company==$k) @if(!$oUser->user_company_name) selected @endif @endif>{{$v}}</option>
			            @endforeach
			        </select>
                </div>
                &nbsp;&nbsp;
			    <a style="cursor:pointer;color:#254797;" onclick="$('#user_company_div').show();">手动填写?</a>
            </div>
            <div class="login_block"  @if(!$oUser->user_company_name) style="display:none;" @endif id="user_company_div">
                <div class="login_left txt_right">
                    &nbsp;
                </div>
                <div class="login_right">
                    <input class="login_txt" type="text" id="user_company_name" name="user_company_name" value="{{$oUser->user_company_name}}"/>
                </div>
            </div>
            <?php 
            	$aDep = User::getDepartment();
       		?>
            <input type="hidden" id="user_province" name="user_province" value="0"/>
            <input type="hidden" id="user_city" name="user_city" value="0"/> 
            <input type="hidden" id="user_county" name="user_county" value="0"/>
            <input type="hidden" id="user_company" name="user_company" value="0"/>
            <div class="login_block">
                <div class="login_left txt_right">
                    科室：
                </div>
                <div class="login_right">
                    <select id="user_dep_select" class="login_txt" style="width: 101%">
		            <option value="0">=请选择=</option>
		            @foreach($aDep as $k=>$v)
		            <option value="{{$v}}" @if($oUser->user_department==$v) selected @endif>{{$v}}</option>
		            @endforeach
		            </select>
		            <input type="hidden" name="user_department" id="user_department" value=""/> 
                </div>
            </div>
            <div class="login_block">
                <div class="login_left">
                    医师证号：
                </div>
                <div class="login_right">
                    <input class="login_txt" type="text" id="card_number" name="card_number" value="{{$oUser->card_number}}"/>
                </div>
            </div>
            <div class="upload_pic_box">
                <div class="upload_pic">
                    <img class="wid100" style="width:54px;height:54px;" id="user_card_thumb" name="user_card_thumb" @if($oUser->card_thumb) src="{{$oUser->card_thumb}}" @else src="/assets/images/weixin/img04.jpg" @endif />
                </div>
                <input class="upload_btn" type="file" id="upload_card_file" name="upload_card_file" onchange="saveCardThumb()"/>
            	<input type="hidden" name="card_thumb" id="card_thumb" value="{{$oUser->card_thumb}}"/>
            </div>
            <div style="text-align:center;">
		                上传医师证照
		    </div>
            <div class="login_btn_block">
                <input class="login_btn" type="button" value="保存" onclick="change_uinfo()"/>
            </div>
        </div>
<script type="text/javascript">
function saveCardThumb() {
	$.ajaxFileUpload({
		url: "/user/upload-user-card-thumb",
		secureuri: false,
		fileElementId: "upload_card_file",
		dataType: "json",
		success: function(data, status) {
			$("#user_card_thumb").attr("src", data.card_thumb);
			$("#card_thumb").val( data.card_thumb);
		}
	})
}
</script>
@stop
