@extends('mobile.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
<link rel="stylesheet" type="text/css" href="/assets/css/fund/common2.css" />
<div class="page_top_box">
    <input type="button" class="btn_back" style="visibility:hidden;" onclick="WeixinJSBridge.call('closeWindow');"/>
    <div class="page_top">个人中心</div>
    <input type="button" class="btn_share" onclick="window.location.href='/logout'"/>
</div>
<div class="page_cont">
        <div class="cont_top">
            <img class="wid100" src="/assets/images/weixin/user_bg.jpg" />
            <div class="user_infor">
                <div class="user">
                    <div class="user_pic">
                        <div class="">
                        <a href="/mobile-user/user-info">
                            <img src="{{$oUser->user_thumb}}" />
                            </a>
                        </div>
<!--                         <input class="btn_file" type="file"/> -->
                    </div>
                    <div class="user_name">
                        <a href="/mobile-user/user-info">{{$oUser->user_name}}</a>
                        @if($oUser->role_id == 3)
                        @if($oUser->card_auth_flag == 2)
	                        <span class="v"><img src="/assets/images/mobile/fund/v.png" alt="" style="width:16px;height:15px;"></span>
                   		@else
                      	   <span class="certification"><img src="/assets/images/mobile/fund/certification.png" alt="" style="width:54px;height:21px;"></span>
                   		@endif
                   		@endif
                    </div>
                </div>
            </div>
        </div>
        <div class="menu_box clearfix">
        	@if($oUser->role_id != 3)
        	<a href="/mobile-user/myscore">
                <div class="menu_icon1 menu_icon12"></div>
                <div class="">
                    我的积分
                </div>
            </a>
            <a href="/mobile-user/contact">
                <div class="menu_icon1 menu_icon7"></div>
                <div class="">
                    联系客服
                </div>
            </a>
            <a href="/mobile-aspirin-online/my-enter-list">
                <div class="menu_icon1 menu_icon3"></div>
                <div class="">
                    我的会议
                </div>
            </a>
        	@else
        	<a href="/mobile-aspirin/auth">
                 <div class="menu_icon1 menu_icon11"></div> 
                 <div class=""> 
                     医师认证 
                 </div> 
            </a>
			 <a href="/mobile-user/notice-list">
                <div class="menu_icon1 menu_icon2"></div>
                <div class="">
                    消息通知
                </div>
            </a>
            <!-- <?php 
            $aPilotCity = User::getPilotCity();
            $aPilotCountry = User::getPilotCountry();
            ?>
            @if(!in_array($oUser->user_city,$aPilotCity) && !in_array($oUser->user_county,$aPilotCountry))
            @else -->
            <a href="/mobile-aspirin-online/my-enter-list">
                <div class="menu_icon1 menu_icon3"></div>
                <div class="">
                    我的会议
                </div>
            </a>
            <a href="/mobile-user/list">
                <div class="menu_icon1 menu_icon4"></div>
                <div class="">
                    线下会议
                </div>
            </a>
            <a href="/aspirinfund/application-researchlist?from='personal-center'">
                <div class="menu_icon1 menu_icon6"></div>
                <div class="">
                    科研培训
                </div>
            </a>
            <!-- @endif -->
            <a href="/mobile-user/contact">
                <div class="menu_icon1 menu_icon7"></div>
                <div class="">
                    联系客服
                </div>
            </a>
            <a href="/mobile-user/myscore">
                <div class="menu_icon1 menu_icon12"></div>
                <div class="">
                    我的积分
                </div>
            </a>
        	@endif 
            <a href="/app">
                <div class="menu_icon1 menu_icon8"></div>
                <div class="">
                    分享APP
                </div>
            </a>
        </div>
</div>
@stop