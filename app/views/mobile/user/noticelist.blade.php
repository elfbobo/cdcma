@extends('mobile.common.layoutasp3')
@section('title')消息通知 @stop
@section('description')消息通知@stop
@section('keywords')消息通知 @stop
@section('content')
<script>

//滑屏加载
//**********************下拉加载S**********************
var _isRun = true;
function cl1(flag){
	if(_isRun){
		_isRun = false;
		var ceil = $('#ceil').val();
		if(ceil==0&&(($(document).scrollTop()>=($(document).height()-$(window).height()-10))||flag==1)){
			$("#container").append('<div id="loading" style="color:#00a0e9;font-size: 0.9rem;margin-left: 40%;margin-top:2%;margin-bottom:5%;">加载中...</div> ');
			var page = parseInt($('#page').val());
			page++;
			var url = '/mobile-user/notice-add';
			var data = {'page':page};
			$.post(url,data,function(msg){
				var json = eval('('+msg+')');
				if(json.ceil==1){
					$('#ceil').val(1);
				}
				
				json.ceil = null;
				$('#page').val(page);
				var i=0;
				var n=(page-1)*10+1;
				$("#container").children('#loading').remove();
				for(i in json){
					if(json[i]){
						str ='<a href="/mobile-user/notice-jump/'+json[i].id+'"';
						str += 'class="alerts_block"';
						if(json[i].has_read == 1){
							str += 'style="background:none"';
						}
						str +='><div class="alerts_tit" class="alerts" style="font-size:13px;" >'+json[i].notice_content+'</div>'+
						  '<div class="alerts_time">'+
						                    '<p style="display:none;">专家：孙宁玲 </p>'+
						                    '<p style="display:none;">北京协和医院</p>'+
						                    '<p>'+json[i].date+'</p>'+
						                '</div></a>';
						$("#container").append(str);
					}	
					n++;
					i++;
				}
				_isRun = true;
				if($('#ceil').val()==1){
					$("#container").children('#loading').remove();
					var res = '<div id="loading" style="color:#00a0e9;font-size: 0.9rem;margin-left: 40%;margin-top:2%;margin-bottom:5%;">已加载全部</div>';
		      		$("#container").append(res);
					document.getElementById("footer").style.display="block";
					$('#ceil').val(2);
					_isRun = true;
				}
			});
		}else if(ceil==1){
			$("#container").children('#loading').remove();
			$("#container").append('<div id="loading" style="color:#00a0e9;font-size: 0.9rem;margin-left: 40%;margin-top:2%;margin-bottom:5%;">已加载全部</div>');
			$('#ceil').val(2);
			_isRun = true;
		}else{
			_isRun = true;
		}
	}
}

//**********************下拉加载E**********************
</script>
 <input type="hidden" id="page" value="1"/> 
 <input type="hidden" id="ceil" value="0"/> 
	
    <div class="page_top_box">
    	<input type="button" class="btn_back" onclick="window.location.href='/mobile-user/index'"/>
        <div class="page_top">消息通知</div>
        <input type="button" class="btn_share" value="分享" style="visibility: hidden;" />
    </div>
    <div class="page_cont">
        <div class="alerts_box" id="container">
        @foreach($oAspirinUserNotice as $value)
            <a href="/mobile-user/notice-jump/{{$value->id}}"  class="alerts_block" @if($value->has_read == 1) style="background:none"  @endif>
                <div class="alerts_tit" class="alerts"  style="font-size:13px;">
                    {{$value->notice_content}}
                </div>
                <div class="alerts_time">
                    <p style="display:none;">专家：孙宁玲 </p>
                    <p style="display:none;">北京协和医院</p>
                    <p>{{date("Y-m-d",strtotime($value->created_at))}}</p>
                </div>
            </a>
         @endforeach
        </div>
    </div>
 <script>

//绑定事件
function changeOpen(e){
	$("#container").on('mousedown touchstart',page_touchstart);
	$(window).scroll(function(){
		page_touchstart();
	});
//	$(document).on('mousedown touchstart',page_touchstart);
};

//开启事件绑定滑动
changeOpen();

//触摸（鼠标按下）开始函数
function page_touchstart(e){
	cl1(0);
};

</script>
@stop   