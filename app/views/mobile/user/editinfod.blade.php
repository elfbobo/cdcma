@extends('mobile.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/user.js"></script>
<script>
function change_uinfo(){

	//用户基本信息修改
	var user_tel = $.trim($('#user_tel').val());
	var user_email = $.trim($('#user_email').val());
	var user_address = $.trim($('#user_address').val());
	var user_position = $.trim($('#user_position').val());
	//省份、城市、区县、医院
	var user_province = $("#hospital21  option:selected").val();
	var user_city = $("#hospital22  option:selected").val();
	var user_regin = $("#regin  option:selected").val();
	var user_area = $("#area  option:selected").val();
	if(!user_tel){
		alert('请输入您的手机号码！');
		return false;
	}
	if(!user_email){
		alert('请输入您的邮箱！');
		return false;
	}
	var reg_mobile = /^[0-9]{11}$/;
	istel= reg_mobile.test(user_tel);
	if (!istel ) {
	    alert("手机号码格式不正确！");
	    return false;
	  }
	  var reg = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
	  ismail= reg.test(user_email);
	  if (!ismail ) {
	    alert("邮箱格式不正确！");
	    return false;
	  }
	
	if(!user_province||user_province==0){
		alert('请选择您的省份！');
		return false;
	}
	if(!user_city||user_city==0){
		alert('请选择您的城市！');
		return false;
	}

	if(!user_regin||user_regin==0){
		alert('请选择您的大区！');
		return false;
	}
	if(!user_area||user_area==0){
		alert('请选择您的地区！');
		return false;
	}

	//不修改密码
	var data = {'user_address':user_address,
			'user_tel':user_tel,
			'user_email':user_email,
			'user_position':user_position,
			'user_province':user_province,
			'user_city':user_city,
			'user_regin':user_regin,
			'user_area':user_area
			};


	var url = '/user/change-user-info-all';
	$.post(url,data,function(msg){
		if(msg=='nick_repeat'){
			alert('该用户名已经存在，换换其他用户名试试');
		}else if(msg=='tel_repeat'){
			alert('该手机号码用户已经存在了');
		}else if(msg=='email_repeat'){
			alert('该邮箱用户已经存在了');
		}else if(msg=='old_err'){
			alert('原密码输入错误');
		}else if(msg=='success'){
			alert('用户信息修改成功');
			window.location.href='/mobile-user';
		}else{
			alert('网络错误，请刷新当前页面重试');
		}
	})	
}

function change_hospital2(id){
    var pid = id-1;
	var hospital2 = $("#hospital2"+pid+"  option:selected").val();
	if(pid==1){
		$('#user_province').val(hospital2);
    }else if(pid==2){
    	$('#user_city').val(hospital2);
    	return ;
    }
	var url = '/user/hospital-child/'+hospital2;
	$.post(url,{},function(json){
		data = json;
    	if(data=='noinfo'){
			var html = '<option value="0">=请选择=</option>';
			$("#hospital2"+id).html(html);
			
		}else{
			var bShow = false;
			var html = '<option value="0">=请选择=</option>';
			for(var elem in data){
				bShow = true;
				html += '<option value="' + elem + '">' + data[elem] + '</option>';
			}
			if(bShow){
				$("#hospital2"+id).html(html);
			}
		}
	},'json');
}
</script>
<div class="page_top_box">
    	<input type="button" class="btn_back" onclick="window.location.href='/mobile-user/user-info'"/>
        <div class="page_top">修改资料</div>
        <input type="button" class="btn_share" style="visibility:hidden;"/>
    </div>
    	<div class="login_box">
            <div class="login_block">
                <div class="login_left txt_right">
                    姓名：
                </div>
                <div class="login_right">
                    <input class="login_txt" type="text" disabled value="{{$oUser->user_name}}" style="background-color:#eee;" />
                </div>
            </div>
            <div class="login_block">
                <div class="login_left txt_right">
                    性别：
                </div>
                <div class="login_right">
                    <input class="login_txt" type="text" disabled value="@if($oUser->user_sex==1) 男 @else 女 @endif" style="background-color:#eee;"/>
                </div>
            </div>
            <div class="login_block">
                <div class="login_left txt_right">
                    手机：
                </div>
                <div class="login_right">
                    <input class="login_txt" type="text" id="user_tel" name="user_tel" value="{{$oUser->user_tel}}"/>
                </div>
            </div>
            <div class="login_block">
                <div class="login_left txt_right">
                    邮箱：
                </div>
                <div class="login_right">
                    <input class="login_txt" type="text" id="user_email" name="user_email" value="{{$oUser->user_email}}"/>
                </div>
            </div>
            <div class="login_block">
                <div class="login_left txt_right">
                    地址：
                </div>
                <div class="login_right">
                    <input class="login_txt" type="text" id="user_address" name="user_address" value="{{$oUser->user_address}}"/>
                </div>
            </div>
            <div class="login_block">
                <div class="login_left txt_right">
                    职称：
                </div>
                <div class="login_right">
                    <input class="login_txt" type="text" id="user_position" name="user_position"  value="{{$oUser->user_position}}"/>
                </div>
            </div>
            <div class="login_block">
                <div class="login_left txt_right">
                    省份：
                </div>
                <div class="login_right">
                    <select id="hospital21" class="login_txt" style="width: 101%" onchange="change_hospital2(2)">
			            <option value="0">=请选择=</option>
			            @foreach($aHosp1 as $k=>$v)
			            <option value="{{$k}}" @if($oUser->user_province==$k) selected @endif>{{$v}}</option>
			            @endforeach
			        </select>
                </div>
            </div>
            <div class="login_block">
                <div class="login_left txt_right">
                    城市：
                </div>
                <div class="login_right">
                    <select id="hospital22" class="login_txt" style="width: 101%" onchange="change_hospital2(3)">
		            <option value="0">=请选择=</option>
		            @foreach($aHosp2 as $k=>$v)
		            <option value="{{$k}}" @if($oUser->user_city==$k) selected @endif>{{$v}}</option>
		            @endforeach
		            </select>
                </div>
            </div>
           
            <input type="hidden" id="user_province" name="user_province" value="0"/>
                        <input type="hidden" id="user_city" name="user_city" value="0"/> 
	                      <?php 
							$sRegin = User::getReginCache();
							$aRegin = json_decode($sRegin);
							?>
							<script>
							function change_regin(){
				            	var regin = $("#regin  option:selected").val();
				            	var url = '/user/area-info/'+regin;
				            	$.post(url,{},function(json){
				            		data = json;
				                	if(data=='noinfo'){
				        				var html = '<option value="0">=请选择=</option>';
				        				$("#area").html(html);
				        				
				        			}else{
				        				var bShow = false;
				        				var html = '<option value="0">=请选择=</option>';
				        				for(var elem in data){
				        					bShow = true;
				        					html += '<option value="' + elem + '">' + data[elem] + '</option>';
				        				}
				        				if(bShow){
				        					$("#area").html(html);
				        				}
				        			}
				            	},'json');
				            }
							</script>
            <div class="login_block">
                <div class="login_left txt_right">
                    大区：
                </div>
                <div class="login_right">
                    <select id="regin" class="login_txt" style="width: 101%" onchange="change_regin()">
		            <option value="0">=请选择=</option>
		            @foreach($aRegin as $k=>$v)
		            <option value="{{$k}}" @if($oUser->user_regin==$k) selected @endif>{{$v}}</option>
		            @endforeach
		            </select>
		            <input type="hidden" name="user_regin" id="user_regin" value=""/>
                </div>
            </div>
            <div class="login_block">
                <div class="login_left txt_right">
                    地区：
                </div>
                <div class="login_right">
                    <select id="area" class="login_txt" style="width: 101%" >
				    <option value="0">=请选择=</option>
				    @foreach($oArea as $k=>$v)
				    <option value="{{$k}}" @if($oUser->user_area==$k) selected @endif>{{$v}}</option>
				    @endforeach
				    </select>
                    <input type="hidden" name="user_area" id="user_area" value=""/>  
                </div>
            </div>
            <div class="login_btn_block">
                <input class="login_btn" type="button" value="保存" onclick="change_uinfo()"/>
            </div>
        </div>

@stop
