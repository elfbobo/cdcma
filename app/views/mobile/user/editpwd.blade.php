@extends('mobile.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
<script>
function change_uinfo(){

	//用户密码修改
	var password_old = $.trim($('#password_old').val());
	var password_new = $.trim($('#password_new').val());
	var password_new_re = $.trim($('#password_new_re').val());
		//修改密码
		if(!password_old){
			alert('请输入旧密码');return;
		}else{
			if(!password_new){
				alert('请输入新密码');return;
			}
			if(password_new!=password_new_re){
				alert('两次修改的密码不一致，请重新输入');
				return;
			}
		}
		var data = {
				'password_old':password_old,
				'password_new':password_new
				};

	var url = '/user/change-user-info-all';
	$.post(url,data,function(msg){
		if(msg=='nick_repeat'){
			alert('该用户名已经存在，换换其他用户名试试');
		}else if(msg=='old_err'){
			alert('原密码输入错误');
		}else if(msg=='success'){
			alert('密码修改成功');
			window.location.href='/mobile-user';
		}else{
			alert('网络错误，请刷新当前页面重试');
		}
	})	
}
</script>
	<div class="page_top_box">
    	<input type="button" class="btn_back" onclick="window.location.href='/mobile-user/user-info'"/>
        <div class="page_top">修改密码</div>
        <input type="button" class="btn_share" style="visibility:hidden;"/>
    </div>
    <div class="page_cont_box">
    	<div class="per_center_box">
            <div class="login_box">
            	<div class="login_block ptop40">
                    <div class="login_right">
                        <input class="login_txt" placeholder="请输入旧密码" type="password" name="password_old" id="password_old"/>
                    </div>
                </div>
                <div class="login_block ">
                    <div class="login_right">
                        <input class="login_txt" placeholder="请输入新密码" type="password" name="password_new" id="password_new"/>
                    </div>
                </div>
                <div class="login_block">
                    <div class="login_right">
                        <input class="login_txt" placeholder="请再次输入新密码" type="password" name="password_new_re" id="password_new_re"/>
                    </div>
                </div>
                <div class="login_btn_block clearfix ptop40">
                    <input class="login_btn" type="button" value="保存" onclick="change_uinfo()"/>
                </div>
            </div>
        </div>
    </div>
    
@stop
