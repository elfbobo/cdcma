@extends('mobile.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
<div class="page_top_box">
    <input type="button" class="btn_back" onclick="window.location.href='/mobile-user'"/>
    <div class="page_top">我的积分</div>
</div>
<div class="page_cont">
	<div class="meeting_box2">
        <div class="meeting_tab">
            <a class="sele" href="#">我的积分</a>
            <a href="/mobile-user/score-list/{{$oUser->role_id}}">积分排行</a>
        </div>
        <div class="integral_box integral_bg">
        	<div class="integral_top">
            	<div class="integral_icon">
                	<img src="/assets/images/weixin/menu015.png" />
                </div>
                <div class="integral_txt1">
                        我的积分：<span>{{$oUser->user_score}}</span>
                </div>
                <div class="integral_txt2">
                    <div class="">
                            累计观看直播时间：{{intval($iLiveMin/60)}}小时{{$iLiveMin%60}}分钟
                    </div>
                    <div class="">
                            累计观看录播时间：{{intval($iReviewMin/60)}}小时{{$iReviewMin%60}}分钟
                    </div>
                </div>
            </div>
        </div>
        <div class="video2_infor">
            <div class="v2_block v2_bg">
                <div class="v2_tit">
                        标题
                </div>
                <div class="v2_form">
                        观看形式
                </div>
                <div class="v2_time">
                        时长(m)
                </div>
            </div>
            @foreach($oVideoLog as $k=>$v)
            @if($v->video_title&&$v->watch_minutes)
            <div class="v2_block">
                <div class="v2_tit">
                    {{$v->video_title}}
                </div>
                <div class="v2_form">
                    @if($v->video_type==1) 录播 @else 直播 @endif
                </div>
                <div class="v2_time">
                    {{$v->watch_minutes}}
                </div>
            </div>
            @endif
            @endforeach
    	</div>
	</div>
</div>
@stop
