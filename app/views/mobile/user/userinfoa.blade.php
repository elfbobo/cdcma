@extends('mobile.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/user.js"></script>
<div class="page_top_box">
    	<input type="button" class="btn_back" onclick="window.location.href='/mobile-user'"/>
        <div class="page_top">个人资料</div>
        <!-- 
        <input type="button" class="btn_share" onclick="window.location.href='/mobile-user/user-info-change'"/>
    	 -->
    	<input type="button" class="btn_share" />
    </div>
    <div class="page_cont_box">
    	<div class="per_center_box">
            <div class="per_center_block">
                <div class="per_center_infor">
                    <div class="name">
                        头像
                    </div>
                    <div class="upload_pic_box mleft">
                        <div class="upload_pic">
                            <img id="user_thumb" class="wid100" src="{{$oUser->user_thumb}}" style="width:54px;height:54px;"/>
                        </div>
                        <div class="upload_txt">
                            更改头像
                        </div>
                        <input class="upload_btn" id="upload_file" name="upload_file"  type="file" onchange="saveThumb()"/>
                    </div>
                </div>
                <div class="per_infor_block">
                    <div class="per_infor_left">
                        姓名
                    </div>
                    <div class="per_infor_right">
                        {{$oUser->user_name}}
                    </div>
                </div>
                <div class="per_infor_block">
                    <div class="per_infor_left">
                        性别
                    </div>
                    <div class="per_infor_right">
                        @if($oUser->user_sex==1) 男 @else 女 @endif
                    </div>
                </div>
                <div class="per_infor_block">
                    <div class="per_infor_left">
                        手机
                    </div>
                    <div class="per_infor_right">
                        {{$oUser->user_tel}}
                    </div>
                </div>
                <div class="per_infor_block">
                    <div class="per_infor_left">
                        邮箱
                    </div>
                    <div class="per_infor_right">
                        {{$oUser->user_email}} 
                    </div>
                </div>
                <div class="per_infor_block">
                    <div class="per_infor_left">
                        地址
                    </div>
                    <div class="per_infor_right">
                        {{$oUser->user_address}}
                    </div>
                </div>
                <div class="per_infor_block">
                    <div class="per_infor_left">
                        用户编码
                    </div>
                    <div class="per_infor_right">
                        {{strtoupper($oUser->invite_code)}}
                    </div>
                </div>
                <div class="bor_btm"></div>
                <div class="per_infor_block">
                    <div class="per_infor_left">
                        用户名
                    </div>
                    <div class="per_infor_right">
                        {{$oUser->user_nick}}
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop