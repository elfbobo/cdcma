@extends('mobile.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
	<div class="page_top_box">
    	<input type="button" class="btn_back" onclick="window.location.href='/mobile-user'"/>
        <div class="page_top">联系客服</div>
        <input type="button" class="btn_share" style="visibility: hidden;" />
    </div>
    <div class="page_cont">
        <div class="page_box">
            <div class="meeting_infor p0">
                <div class="meeting_txt2">
                    整合阿司匹林专项基金、心血管风险筛查项目、世界心脏日主题传播相关视频及资料，提炼并合理分类内容。在平台现有渠道进行零时差同步广泛覆盖。
                </div>
                <div class="meeting_txt2">

                    每一个行动都有获益，越多参与越多获益。积分实时到账
                </div>

                <div class="meeting_explain">
                    <div class="explain">
                        <div>
                            您有任何疑问，可以直接拨打电话：
                        </div>
                        <div class="tele">
                            <?php echo HOTLINE;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var h =$(window).height()-45;
            $('.page_box').css("min-height",h);
        });
    </script>
@stop