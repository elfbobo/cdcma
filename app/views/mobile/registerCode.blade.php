@extends('mobile.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
<script>
function register_code(){
	var code = $.trim($('#invited_code').val());
	window.location.href='/mobile/register/'+code;
}
</script>
	<div class="bg">
        <div class="top">
            <img class="wid100" src="/assets/images/weixin/top.jpg" />
        </div>
    	<div class="login_box0">
            <div class="login_tit">
                请您输入医学顾问的邀请码
            </div>
            <div class="login_block">
                <div class="login_right">
                    <input class="login_txt" type="text" id="invited_code"/>
                    <br/>
                @if($error=='error')
				<span style="color:red;">邀请码输入有误，请重新输入</span>
				@endif
                </div>
            </div>
            <div class="login_btn_block clearfix">
                <input class="login_btn left" type="button" value="提交" onclick="register_code();"/>
                <input class="login_btn right" type="button" value="取消" onclick="window.location.href='/mobile/login'"/>
            </div>
		    <div class="instructions_box">
		        <div class="instructions_line">
		            <div class="instructions_left">
		                温馨提示：
		            </div>
		            <div class="instructions_right">
		                如果您不知道邀请码，请您联系您的医学顾问。
		            </div>
		        </div>
		    </div>
        </div>
        <div class="btm_pic">
	        <img src="/assets/images/weixin/btm2.jpg" />
	    </div>
	</div>
@stop