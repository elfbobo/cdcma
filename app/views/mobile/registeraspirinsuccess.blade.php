@extends('mobile.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
<style>
.cover {position:fixed; left:0; top:0; width:100%; height:100%; background-color:#000; opacity:0.5; filter:alpha(opacity=50);}
.prompt_box { position:absolute; left:4.6%; top:45px; width:90.8%;}
.prompt_pic { text-align:right; padding:10px 0; width:100%;}
.prompt_pic img { width:47px;}
.prompt_txt { font-size:15px; line-height:22px; text-align:center; color:#ffffff;}
.prompt_txt span { color:#fd0202;}
</style>
<?php
$agent = strtolower ( $_SERVER ['HTTP_USER_AGENT'] );
$is_pc = (strpos ( $agent, 'windows nt' )) ? true : false;
$is_iphone = ((strpos ($agent, 'iphone' )) || (strpos ($agent, 'iph' )))? true : false;
$is_ipad = (strpos ($agent, 'ipad' )) ? true : false;
$is_android = ((strpos ($agent, 'android' )) || (strpos($agent, 'adr' )))? true : false;
//判断是否是在微信浏览器中打开
$is_weixin= (strpos ( $agent, 'micromessenger' )) ? true : false;
?>
	<div class="cover" id="cover" style="display:none;"></div>
	<div class="prompt_box" id="box" style=" display:none;">
	    <div class="prompt_pic">
	        <img src="/assets/images/aspirin/arrow.png" />
	    </div>
	    <div class="prompt_txt">
	        @if($is_iphone || $is_ipad)
        	<p>戳右上角，选择“<span>在Safari中打开</span>”</p>
        	@else
        	<p>戳右上角，选择“<span>在浏览器中打开</span>”</p>
        	@endif
	        <p>即可完成下载，等你哦！</p>
	    </div>
	</div>
    <div class="wrap-index wrap-style2">
        <div class="index-cont">
            <div class="wechat_box mart_20">
                <p>您已注册成功</p>
                <p>感谢您参与专项基金项目</p>
                <p>请扫码关注项目公众帐号或下载APP进行参与</p>
                <p>网址：cdcma.bizconf.cn</p>
                <img src="/assets/images/weixin/erweima.jpg" alt="">
                <?php if($is_android){?>
                	@if(user_agent_is_weixin())
                		<button class="btn Android" onclick="$('#cover').show();$('#box').show();">Android版</button>
                	@else
                		<button class="btn Android" onclick="window.location.href='http://cdcma.bizconf.cn/packages/cdcma.apk';">Android版</button>
                	@endif
                <?php }elseif($is_iphone || $is_ipad){?>
                    @if(user_agent_is_weixin())
                    	<button class="btn iPhone" onclick="$('#cover').show();$('#box').show();">iPhone版</button>
                    @else
	                	<a href="itms-services://?action=download-manifest&url=https://jnj.kydev.net:8443/plist/mbglxy_cdcma.plist">
	                	<button class="btn iPhone" >iPhone版</button>
	                	</a>
                    @endif
                <?php }else{?>
                <button class="btn Android" onclick="alert('未识别您的机型');">Android版</button>
                <button class="btn iPhone" onclick="alert('未识别您的机型');">iPhone版</button>
                <?php }?>
            </div>
        </div>
    </div>
@stop