@extends('mobile.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
<div class="bg">
    <div class="top">
    	<img class="wid100" src="/assets/images/weixin/top.jpg" />
    </div>
    <div class="wechat_box">
	    <p>截图保存以下二维码邀请医生注册，</p>
	    <p>参与专项基金项目</p>
	    <img src="{{$sEwmUrl}}" alt="">
    </div>
    <div class="btm_pic">
	    <img src="/assets/images/weixin/btm2.jpg" />
	</div>
</div>
@stop