@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金@stop
@section('content')
@include('mobile.common.stopforward')
	<div class="wrap">
	    <div class="page_top_box">
	        <input type="button" class="btn_back" onclick="window.location.href='/mobile-aspirin';"/>
	        <div class="page_top">在线会议</div>
	        <input type="button" class="btn_search" style="visibility:hidden;"/>
	    </div>
		<div class="wrap-index wrap-style3">
	        <div class="index-cont">
		           <div class="swiper-container" style="height:200px;">
	                <div class="swiper-wrapper">
	                    <div class="swiper-slide">
	                        <img src="{{$oBanner->banner_thumb}}" alt="">
	                        @if($oBanner->show_flag == 0)
	                    	<div class="tip_title" style="bottom:4rem">{{$oBanner->banner_title}}</div>
	                    	@endif
	                        <div class="Tips">
	                            <p>※ 累计举办讲座{{$iHasOpen}}场；共{{$iTotalCount}}人参与会议；</p>
	                            <p>※ 即将开始讲座{{$iPreOpen}}场。</p>
	                        </div>
	                    </div>
	                </div>
	                <!-- Add Pagination -->
	                <!--<div class="swiper-pagination"></div>-->
	            </div>
            
	            <div class="online_meeting">
	                <div class="index-module">
	                    <a href="/mobile-aspirin-online/catid/1" class="skyblue  fixed" style="width:145px;height:145px;">
	                        <dl>
	                            <dt><img src="/assets/images/mobile/fund/online_meeting_icon2.png" alt=""></dt>
	                            <dd>讲者报名</dd>
	                        </dl>
	                    </a>
	                    <a href="/mobile-aspirin-online/catid/2" class="red  fixed" style="width:145px;height:145px;">
	                        <dl>
	                            <dt><img src="/assets/images/mobile/fund/online_meeting_icon1.png" alt=""></dt>
	                            <dd>报名参会</dd>
	                        </dl>
	                    </a>
	                </div>
	                <div class="index-module">
	                    <a href="/mobile-aspirin-online/review-list/0" class="green  fixed" style="width:145px;height:145px;">
	                        <dl>
	                            <dt><img src="/assets/images/mobile/fund/online_meeting_icon3.png" alt=""></dt>
	                            <dd>精彩回顾</dd>
	                        </dl>
	                    </a>
	                    <a href="/mobile-aspirin-online/ppt-list/1" class="darkblue  fixed" style="width:145px;height:145px;">
	                        <dl>
	                            <dt><img src="/assets/images/mobile/fund/online_meeting_icon4.png" alt=""></dt>
	                            <dd>查看课件</dd>
	                        </dl>
	                    </a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@stop
