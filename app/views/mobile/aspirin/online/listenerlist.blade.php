@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金@stop
@section('content')
@include('mobile.common.stopforward')
	<div class="wrap">
		<div class="page_top_box">
		    <input type="button" class="btn_back" onclick="window.location.href='/mobile-aspirin-online/index';"/>
		    <div class="page_top">在线会议</div>
		    <input type="button" class="btn_search" style="visibility:hidden;"/>
		</div>
<!-- 	参会列表页：未报名的 小于5人是绿色，5到10人是蓝色，10是红色；已报名的显示绿色的“参加会议”，点击有相应提示；会议结束不显示 -->
		<div class="wrap sign_up_box">
	        <div class="sign_up">
	            @foreach($aOnlineInfo as $k=>$v)
	            <div class="list">
	                <div class="time">{{$v['online_title']}}</br>{{$v['online_date']}} {{$v['time_period']}}</div>
	                <div class="expert_meeting">
	                	@foreach($v['speaker_info'] as $key=>$val)
	                	@if($val['listener_count'] < 5)
		                	@if($val['btn_flag'] == 2)
		                	<a style="background:#32bbdb;" onclick="submit_attend({{$val['speaker_id']}},{{$v['online_time_id']}})">参加会议</a>
		                    @elseif($val['btn_flag'] == 3)
		                	<a style="background:#b4b4b4;" onclick="alert('会议未开始！');">参加会议</a>
		                    @else
		                    <a style="background:#75b437;" onclick="pre_box({{$v['online_time_id']}},'{{$val['speaker_id']}}','{{$val['doc_name']}}','{{$val['doc_department']}}','{{$val['doc_hospital']}}');">
		                    {{$val['doc_name']}}({{$val['listener_count']}}/{{$val['listener_limit']}})
		                    </a>
		                    @endif
	                    @elseif($val['listener_count'] >= 5 && $val['listener_count'] < 10)
	                    	@if($val['btn_flag'] == 2)
		                    <a style="background:#32bbdb;" onclick="submit_attend({{$val['speaker_id']}},{{$v['online_time_id']}})">参加会议</a>
		                    @elseif($val['btn_flag'] == 3)
		                	<a style="background:#b4b4b4;" onclick="alert('会议未开始！');">参加会议</a>
		                    @else
		                    <a style="background:#32bbdb;" onclick="pre_box({{$v['online_time_id']}},'{{$val['speaker_id']}}','{{$val['doc_name']}}','{{$val['doc_department']}}','{{$val['doc_hospital']}}');">
		                    {{$val['doc_name']}}({{$val['listener_count']}}/{{$val['listener_limit']}})
		                    </a>
		                    @endif
	                    @else
		                    <a style="background:#c8132e;" onclick="alert('报名人数已满');">
		                    {{$val['doc_name']}}({{$val['listener_count']}}/{{$val['listener_limit']}})
		                    </a>
	                    @endif
	                    @endforeach
	                </div>
	            </div>
	            @endforeach
	        </div>
	    </div>
    </div>
    <input type="hidden" id="speakerid" value="0">
    <input type="hidden" id="timeid" value="0">
    
    <div class="shade" id="shade" style="display:none;"></div>
    <div class="module-window" id="pre_box" style="display:none;">
        <div class="txt" id="info">
            <p>北大人民医院 心内科</p>
            <p>孙宁玲</p>
        </div>
        <div class="btn_box">
            <button class="btn" onclick="enter_box();">确认报名</button>
            <button class="btn" onclick="$('#shade').hide();$('#pre_box').hide();">取消</button>
        </div>
    </div>
    <div class="module-window2" id="submit_notice_box" style="display:none;">
        <div class="x_box"><img src="/assets/images/mobile/fund/x.png" onclick="window.location.href='/mobile-aspirin-online/my-attend-list';"></div>
        <div class="txt">
            <p>如会议报名少于5人系统将自动取消此次会议，请您时时关注，谢谢。</p>
        </div>
    </div>
    <script type="text/javascript">
	function pre_box(timeid,speakerid,name,depart,hos){
		if(timeid == 0){
			alert('会议参数错误');return;
		}
		var url = '/mobile-aspirin-online/listener-enter';
		var data = {timeid:timeid};
		$.post(url,data,function(msg){
			if(msg == 'nocard'){
				alert('仅限通过医师认证的用户参与!');
				return;
			}
			if(msg == 'nextweek'){
				alert('请在本周日24点以后至会议开始前报名此会，谢谢！');
				return;
			}else if(msg == 'hasstart'){
	 			alert('报名结束，会议已开始');
				return;
			}else if(msg == 'hasend'){
	 			alert('会议已结束');
				return;
			}else if(msg == 'onlyone'){
	 			alert('同一时间段只允许报名一位专家的会议，谢谢！');
				return;	
			}else if(msg == 'success'){
				$('#speakerid').val(speakerid);
				$('#timeid').val(timeid);
				var str = '';
				str += '<p>'+hos+'</p>'+
							'<p>'+depart+'</p>'+
				    		'<p>'+name+'</p>';
				$('#info').empty();
				$('#info').append(str);
				$('#shade').show();
				$('#pre_box').show();
			}else{
				alert('数据有误，请尝试重新进入!');
				return;
			}
		})
	}
	function enter_box(){
		var speakerid = $('#speakerid').val();
		var timeid = $('#timeid').val();
		if(speakerid == 0 || timeid == 0){
			alert('会议参数错误');return;
		}
		var url = '/mobile-aspirin-online/listener-submit';
		var data = {
				speakerid:speakerid,
				timeid:timeid
		};
		$.post(url,data,function(msg){
			if(msg == 'nocard'){
				alert('仅限通过医师认证的用户参与!');
				return;
			}
			if(msg == 'success'){
				$('#pre_box').hide();
				$('#submit_notice_box').show();
			}else if(msg == 'again'){
				alert('您已报名该讲者会议!');
				return;
			}else{
				alert('数据有误，请尝试重新进入!');
				return;
			}
		})
	}
	function submit_attend(speakerid,timeid){
    	if(!speakerid || !timeid){
			alert('参数错误');return;
		}
		var url = '/mobile-aspirin-online/listener-attend';
		var data = {speakerid:speakerid,timeid:timeid};
		$.post(url,data,function(msg){
			if(msg.success){
				window.location.href = msg.url;
			}else{
				alert(msg.notice);return;
			}
		},'json')
    }
	</script>
@stop