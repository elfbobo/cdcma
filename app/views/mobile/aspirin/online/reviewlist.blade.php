@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金@stop
@section('content')
@include('mobile.common.stopforward')
	<div class="wrap">
	    <div class="page_top_box">
	        <input type="button" class="btn_back" onclick="window.location.href='/mobile-aspirin-online/index';"/>
	        <div class="page_top">精彩回顾</div>
	        <input type="button" class="btn_search" onclick="window.location.href='/mobile-aspirin-online/search';"/>
	    </div>
	    <div class="highlights_tab">
	        <span onclick="window.location.href='/mobile-aspirin-online/review-list/0';" @if($iCatId == 0) class="sel" @endif>全部</span>
	        <span onclick="window.location.href='/mobile-aspirin-online/review-list/1';" @if($iCatId == 1) class="sel" @endif>● 心内科</span>
	        <span onclick="window.location.href='/mobile-aspirin-online/review-list/2';" @if($iCatId == 2) class="sel" @endif>● 神内科</span>
	    </div>
	    <div class="wrap-style1 highlights flex_content" style="background: #fff">
	        <div class="cont-list">
	        	@foreach($aVideoList as $k=>$v)
	        	<a href="/mobile-aspirin-online/review-show/{{$v['video_id']}}">
	            <dl>
	                <dt><img src="{{$v['online_thumb']}}" alt=""></dt>
	                <dd>
	                    <p class="title">{{$v['online_title']}}</p>
	                    <p class="author">{{$v['doc_name']}} {{$v['doc_hospital']}} </p>
	                    <p class="time">{{$v['online_date']}}  阅读量：{{$v['video_hits']}}</p>
	                </dd>
	            </dl>
	            </a>
	            @endforeach
	        </div>
	    </div>
	</div>
@stop
