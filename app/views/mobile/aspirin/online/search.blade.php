@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金@stop
@section('content')
@include('mobile.common.stopforward')
	<div class="wrap">
	    <div class="page_top_box">
	        <input type="button" class="btn_back" onclick="window.location.href='/mobile-aspirin-online/review-list/0';"/>
	        <div class="page_top">精彩回顾</div>
	        <input type="button" class="btn_search" style="visibility:hidden;"/>
	    </div>
		<div class="wrap-index wrap-style2 search_page">
	        <div class="index-cont">
	            <div class="search_box">
	            <form id="search_form" method="get" action="/mobile-aspirin-online/review-list/0">
	                <div class="input_box">
	                    <label for="sname">专家：</label>
	                    <input type="text" class="input_text" id="sname" name="sname">
	                </div>
	                <div class="input_box">
	                    <label for="stitle">主题：</label>
	                    <input type="text" class="input_text" id="stitle" name="stitle">
	                </div>
	                <div class="btn_box">
	                    <button class="btn" type="submit">一键搜索</button>
	                </div>
	            </form>
	            </div>
	        </div>
	    </div>
    </div>
@stop
