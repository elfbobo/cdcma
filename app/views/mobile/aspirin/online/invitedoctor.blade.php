@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金 @stop
@section('keywords')阿司匹林专项基金 @stop
@section('content')
@include('mobile.common.stopforward')
	<input type="hidden" value="0" id="button_type"/>
	<input type="hidden" id="timeid" name="timeid" value="{{$iOnlineTimeId}}"/>
	<div class="wrap">
		<div class="page_top_box">
	        <input type="button" class="btn_back" onclick="window.history.go(-1);"/>
	        <div class="page_top">邀请医生</div>
	        <input type="button" class="btn_txt" value="全选" onclick="selectAll('selected_doc');"/>
	    </div>
		<div class="wrap sign_up_box">
	        <div class="sign_up">
	        	@foreach($aDoctorInfo as $k=>$v)
	            <div class="input_box">
	                <input type="checkbox" class="input_checkbox" id="selected_doc_{{$v['id']}}" name="selected_doc" value="{{$v['id']}}">
	                <label for="selected_doc_{{$v['id']}}" class="checkbox_label">{{$v['user_name']}}</label>
	            </div>
	            @endforeach
	        </div>
	        <div class="signup_foot">
	            <button class="btn" onclick="submit_doctor();">确定</button>
	        </div>
	    </div>
	</div>
	<script>
	function selectAll(name){
		var flag = $('#button_type').val();
		if(flag == 0){
			$("input[name="+name+"]:checkbox").attr('checked','checked');
			$('#button_type').val(1);
		}else if(flag == 1){
			$("input[name="+name+"]:checkbox").removeAttr("checked");
			$('#button_type').val(0);
		}
	}
	//获取多选框值
	function getCheckbox(name,s){
		if(!s){
			s = '|';
		}
		var obj = $("input[name="+name+"]:checked");
		var str = '';
		obj.each(function(){
			if(str){
				str += s ;
			}
			str += $(this).val();
		});
		return str;
	}
	function submit_doctor(){
		var doctorids = getCheckbox('selected_doc');
		if(!doctorids){
			alert("请选择要邀请的医生");
			return false;
		}
		var timeid = $('#timeid').val();
		if(!timeid ||timeid == 0){
			alert('参数错误');return;
		}
		var url = '/mobile-aspirin-online/invite-submit';
		var data = {timeid:timeid,doctorids:doctorids};
		$.post(url,data,function(msg){
			if(msg.success){
				window.location.href = '/mobile-aspirin-online/invite-list/'+timeid;
			}else{
				alert(msg.notice);
			}
		},'json');
	}
	</script>
@stop