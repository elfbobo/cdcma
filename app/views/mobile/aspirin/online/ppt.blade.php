@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金@stop
@section('content')
@include('mobile.common.stopforward')
<div class="wrap">
    <div class="page_top_box">
	    <input type="button" class="btn_back" onclick="window.location.href='/mobile-aspirin-online/index';"/>
        <div class="page_top">课件选择</div>
        <input type="button" class="btn_search" style="visibility:hidden"/>
    </div>
    	@if($oPpt)
    	<div class="choice_block">
	        @foreach($oPpt as $k=>$v)
	        <label class="list">
	            <input type="radio" name="ppt_box" value="{{$v->id}}">
	            <a href="/mobile-aspirin-online/ppt-show/{{$v->id}}/1">
	            <span>{{$v->ppt_title}}</span>
	            </a>
	        </label>
	        @endforeach
	        <div class="tips">
	            <a href="#">点击主题查看课件</a>
	        </div>
	        <div class="btn_box">
	            <button class="btn" onclick="check_box({{$iCatId}});">确定</button>
	        </div>
        </div>
        @endif
</div>
<script>
function check_box(catid){
	var pptid = $("input[name='ppt_box']:checked").val();
	if(!pptid){
		alert('请先选择会议课件');return;
	}
	window.location.href="/mobile-aspirin-online/speaker-list/"+catid+"/"+pptid;
}
</script>
@stop