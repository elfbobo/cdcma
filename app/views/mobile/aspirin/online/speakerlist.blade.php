@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金@stop
@section('content')
@include('mobile.common.stopforward')
	<!-- 1. Define some markup -->
 	<!-- 
	<button class="btn" data-clipboard-text="Just because you can doesn't mean you should — clipboard.js">
	    Copy to clipboard
	</button>
	 -->
    <!-- 2. Include library -->
    <script src="/assets/js/dist/clipboard.min.js"></script>

    <!-- 3. Instantiate clipboard -->
    <script>
    var clipboard = new Clipboard('#btn');

    clipboard.on('success', function(e) {
        console.log(e);
    });

    clipboard.on('error', function(e) {
        console.log(e);
    });
    </script>
	<div class="wrap">
		<div class="wrap sign_up_box speaker_page">
	        <div class="sign_up">
	            <div class="swiper-container">
	                <div class="swiper-wrapper">
	                    <div class="swiper-slide">
	                        <img src="{{$oOnline->online_banner}}" alt="">
	                        @if($iHasOpen != 0 && $iTotalCount != 0)
	                        <div class="Tips">
	                            <p>※ 累计举办讲座{{$iHasOpen}}场；共{{$iTotalCount}}人参与会议；</p>
	                            <p>※ 即将开始讲座{{$iPreOpen}}场。</p>
	                        </div>
	                        @endif
	                    </div>
	                </div>
	            </div>
	            <div class="meeting_time">会议时间（自选）：</div>
	            <div class="choose_time">
	                @foreach($aOnlineTime as $k=>$v)
	                <div class="list">
	                    <div class="time">{{$v['online_date']}} {{$v['time_period']}}</div>
	                    <div class="expert_meeting">
	                    	@if($v['enter_flag'] == 0)
	                        <a class="ban">报名({{$v['speaker_count']}}/{{$v['limit_count']}})</a>
	                        @elseif($v['enter_flag'] == 2)
	                        <a class="ban" onclick="alert('会议未开始！')">启动会议</a>
	                        @elseif($v['enter_flag'] == 3)
	                        <a onclick="meeting_open({{$v['online_time_id']}});">启动会议</a>
	                        @else
	                        <a onclick="pre_box({{$oOnline->id}},{{$v['online_time_id']}});">报名({{$v['speaker_count']}}/{{$v['limit_count']}})</a>
	                        @endif
	                    </div>
	                </div>
	                @endforeach
	            </div>
	        </div>
	        <div class="signup_foot">
	            <p>会议参与人员最低5人，如报名人数不满5人则本次会议将自动取消。</p>
	        </div>
	    </div>
    </div>
    <input type="hidden" id="onlineid" value="{{$oOnline->id}}">
    <input type="hidden" id="timeid" value="0">
    <input type="hidden" id="pptid" value="{{$iPptId}}">
    
    <div class="shade" id="shade" style="display:none;"></div>
    <div class="module-window" id="pre_notice_box" style="display:none;">
        <div class="txt">
            本月讲题仅限选择一场与医生在线交流，您是否确认报名该场在线会议?
        </div>
        <div class="btn_box">
            <button class="btn" onclick="$('#shade').hide();$('#pre_notice_box').hide();">再考虑一下</button>
            <button class="btn" onclick="$('#pre_notice_box').hide();$('#submit_box').show();">确定</button>
        </div>
    </div>
    <div class="module-window" id="submit_box" style="display:none;">
        <div class="txt">
            <div class="title">请您填写以下信息，以便申报专家费用:</div>
            <div class="input_box">
                <label for="declare_name">姓名：</label>
                <input type="text" id="declare_name" @if($oUserInfo) value="{{$oUserInfo->declare_name}}" @endif>
            </div>
            <div class="input_box">
                <label for="declare_id_num">身份证号：</label>
                <input type="text" id="declare_id_num" @if($oUserInfo) value="{{$oUserInfo->declare_id_number}}" @endif>
            </div>
            <div class="input_box">
                <label for="declare_bank">开户行：</label>
                <input type="text" id="declare_bank" @if($oUserInfo) value="{{$oUserInfo->declare_bank}}" @endif>
            </div>
            <div class="input_box">
                <label for="declare_bank_num">银行账号：</label>
                <input type="text" id="declare_bank_num" @if($oUserInfo) value="{{$oUserInfo->declare_bank_number}}" @endif>
            </div>
        </div>
        <div class="btn_box">
            <button class="btn" onclick="$('#shade').hide();$('#submit_box').hide();">再考虑一下</button>
            <button class="btn" id="enterbutton" onclick="enter_box();">确定</button>
        </div>
    </div>
    <div class="module-window2" id="submit_notice_box" style="display:none;">
        <div class="x_box"><img src="/assets/images/mobile/fund/x.png" onclick="window.location.href='/mobile-aspirin-online/my-enter-list';"></div>
        <div class="txt">
            <p><span class="font-weight">恭喜您已报名成功，</span>如会议报名少于5人系统将自动取消此次会议，请您时时关注，谢谢。</p>
        </div>
    </div>
    <div class="module-window" id="meeting_notice_box" style="display:none;">
        <div class="txt">
        	<p>温馨提示：</p>
            <p>会议时间不少于30分钟。</p>
            <p>幻灯分享链接:</p>
            <p>{{$sPptHtml}}进入会议后打开共享网页直接粘贴即可分享幻灯。</p>
        </div>
        <div class="btn_box">
            <button class="btn" onclick="$('#shade').hide();$('#meeting_notice_box').hide();">再考虑一下</button>
            <button class="btn" id="btn" data-clipboard-text="{{$sPptHtml}}" onclick="submit_open();">确定</button>
        </div>
    </div>
    <script type="text/javascript">
	function pre_box(iOnlineId,timeid){
		if(iOnlineId == 0 || timeid == 0){
			alert('会议参数错误');return;
		}
		var url = '/mobile-aspirin-online/speaker-enter';
		var data = {iOnlineId:iOnlineId};
		$.post(url,data,function(msg){
			if(msg == 'nocard'){
				alert('仅限通过医师认证的副高级别以上用户参与!');
				return;
			}else if(msg == 'noauth'){
				alert('仅限副高以上职称用户报名!');
				return;
			}else if(msg == 'one'){
	 			alert('每月只可报名一场会议!');
				return;
			}else if(msg == 'twice'){
	 			alert('试运营期内限每位讲者只能参与两次!');
				return;	
			}else if(msg == 'again'){
	 			alert('您已经申报过本月会议!');
				return;
			}else if(msg == 'success'){
				$('#onlineid').val(iOnlineId);
				$('#timeid').val(timeid);
				$('#shade').show();
				$('#pre_notice_box').show();
			}else if(msg == 'shangxian'){
	 			alert('已达试点地区报名场次数上限!');
				return;	
			}else{
				alert('数据有误，请尝试重新进入!');
				return;
			}
		})
	}
	function enter_box(){
		var iPptId = $('#pptid').val();
		var iOnlineId = $('#onlineid').val();
		var timeid = $('#timeid').val();
		var declare_name = $('#declare_name').val();
		var declare_id_num = $('#declare_id_num').val();
		var declare_bank = $('#declare_bank').val();
		var declare_bank_num = $('#declare_bank_num').val();
		if(iOnlineId == 0 || timeid == 0 || iPptId == 0){
			alert('会议参数错误');return;
		}
		if(!declare_name){
			alert('请输入申报姓名');return;
		}
		if(!declare_id_num){
			alert('请输入有效身份证号');return;
		}
		if(!declare_bank){
			alert('请输入开户行');return;
		}
		if(!declare_bank_num){
			alert('请输入有效银行账号');return;
		}
		var url = '/mobile-aspirin-online/speaker-submit';
		var data = {
				iPptId:iPptId,
				iOnlineId:iOnlineId,
				timeid:timeid,
				declare_name:declare_name,
				declare_id_num:declare_id_num,
				declare_bank:declare_bank,
				declare_bank_num:declare_bank_num
		};
		$.post(url,data,function(msg){
			$('#enterbutton').attr('disabled',true); 
			if(msg == 'nocard'){
				alert('仅限通过医师认证的副高级别以上用户参与!');
				return;
			}else if(msg == 'success'){
				$('#submit_box').hide();
				$('#submit_notice_box').show();
			}else if(msg == 'notime'){
				alert('该时间段已被占满!');
				return;
			}else if(msg == 'meetingerror'){
	 			alert('获取会议链接失败!');
				return;
			}else if(msg == 'codeerror'){
				alert('未获取到会议编码，请重试!');
				return;
			}else if(msg == 'shangxian'){
	 			alert('已达试点地区报名场次数上限!');
				return;
			}else{
				alert('数据有误，请尝试重新进入!');
				return;
			}
		})
	}
	function meeting_open(timeid){
		$('#timeid').val(timeid);
		$('#shade').show();
		$('#meeting_notice_box').show();
	}
	function submit_open(){
		var timeid = $('#timeid').val();
		if(timeid == 0){
			alert('会议参数错误');return;
		}
		var url = '/mobile-aspirin-online/speaker-open';
		var data = {timeid:timeid};
		$.post(url,data,function(msg){
			if(msg.success){
				window.location.href = msg.url;
			}else{
				alert(msg.notice);
				$('#shade').hide();
				$('#meeting_notice_box').hide();
			}
		},'json');
	}
	</script>
	 <script>
	 	var Top = $('#submit_box').offset().top
        $('.input_box input').bind('focus',function(){
            $('#submit_box').css('top',0);
            //或者$('#viewport').height($(window).height()+'px');
        }).bind('blur',function(){
        	$(window).css('top',Top);
            //或者$('#viewport').height('auto');
        })
    </script>
@stop