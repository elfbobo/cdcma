@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金@stop
@section('content')
@include('mobile.common.stopforward')
	<div class="wrap sign_up_box">
        <div class="sign_up">
            <div class="list">
                <div class="time">2016年10月1日 11:00-12:00</div>
                <div class="theme">会议主题：阿司匹林一级预防</div>
                <div class="expert_meeting">
                    <a href="#" class="cancel">取消会议</a>
                    <a href="#" class="ban">启动会议</a>
                    <a href="#" class="default">邀请医生</a>
                </div>
                <div class="expert_meeting">
                    <a href="#" class="ban">取消会议</a>
                    <a href="#" class="default">启动会议</a>
                    <a href="#" class="ban">邀请医生</a>
                </div>
                <div class="expert_meeting">
                    <a href="#" class="ban">会议结束</a>
                    <a href="#" class="ban">启动会议</a>
                    <a href="#" class="ban">邀请医生</a>
                </div>
            </div>
        </div>
        <div class="signup_foot">
            <button class="btn">点击查看幻灯片</button>
        </div>
    </div>
    <div class="shade" style="display:none;"></div>
    <div class="module-window" style="display:none;">
        <div class="txt">
            <p>您确认取消会议？</p>
        </div>
        <div class="btn_box">
            <button class="btn">再考略一下</button>
            <button class="btn">确定</button>
        </div>
    </div>
    <div class="module-window2" style="display:none;">
        <div class="x_box"><img src="res/images/x.png" alt=""></div>
        <div class="txt">
            <p class="text-align">您已成功取消报名</p>
        </div>
    </div>
@stop
