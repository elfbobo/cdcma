@extends('mobile.common.layoutasp2')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金@stop
@section('content')
<style>
.comment_txt img{width:24px;height:24px;}
</style>
<?php 
$isWxFlag = user_agent_is_weixin();
$wx_config = array();
if ($isWxFlag){
	include_once  app_path().'/include/jssdk.class.php';  //先把类包含进来
	$oJssdk = new jssdk();
	$wx_config = $oJssdk->getSignPackage();
}
?>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
@if($isWxFlag)
wx.config({
	debug:false,
	appId: "{{$wx_config['appId']}}",//必填
	timestamp:{{$wx_config['timestamp']}}, // 必填，生成签名的时间戳
	nonceStr: "{{$wx_config['nonceStr']}}", // 必填，生成签名的随机串
	signature: "{{$wx_config['signature']}}",// 必填，签名
	jsApiList: ['onMenuShareTimeline','onMenuShareAppMessage']// 必填，需要使用的JS接口列表，所有JS接口列表见附录2
});
wx.ready(function(){
	wx.onMenuShareTimeline({//分享到朋友圈
	    title:'专项基金在线会议',
	    link: '{{Config::get('app.url')}}/aspirinshare/online-show/{{$oOnline->id}}',
	    imgUrl: '{{Config::get('app.url')}}/assets/images/mobile/logo.png',
	    success: function () {  },
	    cancel: function () {  }
	});
	wx.onMenuShareAppMessage({//分享给朋友
	    title: '专项基金在线会议',
	    desc: '专项基金在线会议',
	    link: '{{Config::get('app.url')}}/aspirinshare/online-show/{{$oOnline->id}}',
	    imgUrl: '{{Config::get('app.url')}}/assets/images/mobile/logo.png',
	    type: '',
	    dataUrl: '',
	    success: function (){ },
	    cancel: function (){ }
	});
});
wx.error(function(res){ });
@endif
</script>
<link rel="stylesheet" type="text/css" href="/assets/css/mobileaspirin/index.css"/>
<style>
.hightlights-detail .evaluation_box .list2,.list3,.list01,.list02,.list03{
display: flex; 
display: -webkit-flex;
justify-content: space-between;
-webkit-justify-content: space-between;
align-items: center;
-webkit-align-items: center;
padding: 0.5rem 0;
}
</style>
<?php 
if($oScoreLog){
	$iBlue1 = $oScoreLog->content_degree;
	$iGray1 = 5-$iBlue1;
	$iBlue2 = $oScoreLog->class_level;
	$iGray2 = 5-$iBlue2;
	$iBlue3 = $oScoreLog->overall_score;
	$iGray3 = 5-$iBlue3;
}
?>
	<input type="hidden" id="score1" value="0"/>
	<input type="hidden" id="score2" value="0"/>
	<input type="hidden" id="score3" value="0"/>
	<input type="hidden" id="videoid" value="{{$iVideoId}}"/>
	
	<div class="page_top_box" style="width:auto;">
    	<input type="button" class="btn_back" onclick="window.location.href='/mobile-aspirin-online/index';"/>
        精彩回顾
        <input type="button" class="btn_share" style="visibility:hidden;"/>
    </div>
    <div class="page_cont_box hightlights-detail">
    	<div class="title_box">
        	<p class="center">{{$oOnline->online_title}}</p>
            <p class="center font12">{{$sUserCompany}}  {{$oUser->user_name}}</p>
        </div>
        <div class="video_box">
        	<video class="wid100" src="{{$oVideo->video_url}}" poster="{{$oOnline->online_banner}}" controls></video>
        </div>
        <div class="video_infor_box">
        	<div class="video_infor_block">
            	浏览量：{{$oVideo->video_hits}}
            </div>
            <div class="video_infor_block">
            	@if($iZan == 1)
            	<a class="white_heart red_heart" onclick="alert('已经赞过了呦')">{{$oVideo->video_zan}}</a>
            	@else
            	<a class="white_heart" onclick="submit_video_zan({{$oVideo->id}})">{{$oVideo->video_zan}}</a>
            	@endif
            </div>
        </div>
        <div class="evaluation_box">
            <div class="evaluation">
            	<!-- 判断是否已经提交过星级评分，每人限评一次，评过之后不再可点（3个分开判断） -->
				@if($oScoreLog && $oScoreLog->content_degree != 0)
                <div class="list01">
                    <div>内容丰富程度：</div>
                    <div>
                    	@for($i=1;$i<=$iBlue1;$i++)
				        <span><img src="/assets/images/mobile/fund/icon_star1.png" class="star"></span>  
				        @endfor
				        @for($i=1;$i<=$iGray1;$i++)
				        <span><img src="/assets/images/mobile/fund/icon_star2.png" class="star"></span>  
				        @endfor
                    </div>
                </div>
                @else
                <div class="list">
                    <div>内容丰富程度：</div>
                    <div>
                    	@for($i=1;$i<=5;$i++)
	                    <span><img src="/assets/images/mobile/fund/icon_star2.png" class="star"></span>
	                    @endfor
                    </div>
                </div>
                @endif
                @if($oScoreLog && $oScoreLog->class_level != 0)
                <div class="list02">
                    <div>专家讲课水平：</div>
                    <div>
                    	@for($i=1;$i<=$iBlue2;$i++)
				        <span><img src="/assets/images/mobile/fund/icon_star1.png" class="star"></span>  
				        @endfor
				        @for($i=1;$i<=$iGray2;$i++)
				        <span><img src="/assets/images/mobile/fund/icon_star2.png" class="star"></span>  
				        @endfor
                    </div>
                </div>
                @else
                <div class="list2">
                    <div>专家讲课水平：</div>
                    <div>
                    	@for($i=1;$i<=5;$i++)
	                    <span><img src="/assets/images/mobile/fund/icon_star2.png" class="star"></span>
	                    @endfor 
                    </div>
                </div>
                @endif
                @if($oScoreLog && $oScoreLog->overall_score != 0)
                <div class="list03">
                    <div>整体综合评分：</div>
                    <div>
                        @for($i=1;$i<=$iBlue3;$i++)
				        <span><img src="/assets/images/mobile/fund/icon_star1.png" class="star"></span>  
				        @endfor
				        @for($i=1;$i<=$iGray3;$i++)
				        <span><img src="/assets/images/mobile/fund/icon_star2.png" class="star"></span>  
				        @endfor
                    </div>
                </div>
                @else
                <div class="list3">
                    <div>整体综合评分：</div>
                    <div>
                       @for($i=1;$i<=5;$i++)
	                   <span><img src="/assets/images/mobile/fund/icon_star2.png" class="star"></span>
	                   @endfor
                    </div>
                </div>
                @endif
            </div>
        </div>
        <div class="comment_box">
            <div class="comment_tit">评论</div>
            <div class="comment_cont">
            	@foreach($oComments as $k=>$v)
                <div class="comment_block">
                    <div class="comment_line">
                        <div class="comment_pic">
                            <img src="{{$v->user_thumb}}" style="width:36px;height:36px;"/>
                        </div>
                        <div class="comment_name">
                            <div>{{$v->user_nick}}</div>
                            <div>{{substr($v->created_at,0,10)}}</div>
                        </div>
                    </div>
                    <div class="comment_txt">
                        {{$v->comment}}
                    </div>
                    <div class="comment_infor">
                        <div class="" onclick="comment_reply('{{$v->user_nick}}')">回复</div>
                        <div class="" onclick="comment_zan({{$v->id}})">点赞（<span id="comment_zan_{{$v->id}}">{{$v->zan_count}}</span>）</div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="send_comm_box">
        <div><input type="text" class="send_txt" placeholder="写评论.." /></div>
    </div>
    <div class="cover" style="display:none;"></div>
    <div class="send_comm_box2" style="display:none;">
        <div>
            <textarea type="text" class="send_area" placeholder="写点什么吗？..."  id="comment"></textarea>
        </div>
        <div class="send_btn_block">
            <input class="send_btn" type="button" value="发表" id="send" onclick="submit_comment({{$oVideo->id}});"/>
        </div>
    </div>

    <script>
		function submit_video_zan(videoid){
			if(!videoid || videoid == 0){
				alert('参数错误！');return;
			}
			var url = '/mobile-aspirin-online/zan-video';
			var data = {videoid:videoid};
			$.post(url,data,function(msg){
				if(msg == 'success'){
					$('.white_heart').addClass("red_heart");
					var count = parseInt($('.white_heart').html());
					$('.white_heart').html(count+1);
				}else if(msg == 'again'){
					alert('您已经为当前视频点赞了哦');return;
				}else{
					alert('支持失败，请刷新当前页面重试！');return;
				}
			})
		}
		function submit_comment(videoid){
			if(!videoid || videoid == 0){
				alert('参数错误！');return;
			}
			var comment = $('#comment').val();
			if(!$.trim(comment)){
				 alert('评论内容不能为空');
				 return false;
			}
			var url = '/mobile-aspirin-online/comment';
			var data = {videoid:videoid,comment:comment};
			$.post(url,data,function(msg){
				if(msg == 'success'){
	                window.location.reload();
				}else{
					alert('提交错误！');return;
				}
			})
		}
		function comment_reply(usernick){
			if(!$.trim(usernick)){
				alert('未获取到昵称！');return;
			}
			$("#comment").html('回复'+usernick+"：");
			$('.cover').show();
            $('.send_comm_box').hide();
            $('.send_comm_box2').show();
		}
		function comment_zan(commentid){
			var url = '/mobile-aspirin-online/zan-comment';
			var data = {commentid,commentid};
			$.post(url,data,function(msg){
				if(msg == 'success'){
					var count = parseInt($("#comment_zan_"+commentid).html());
					$("#comment_zan_"+commentid).html(count+1);
				}else if(msg == 'again'){
					alert('您已经点赞了哦');return;
				}else{
					alert('提交错误！');return;
				}
			});
		}
		function submit_video_score(){
	    	var videoid = $('#videoid').val();
	    	var score1 = $('#score1').val();
	    	var score2 = $('#score2').val();
	    	var score3 = $('#score3').val();
	    	var url = '/mobile-aspirin-online/score';
	    	var data = {videoid:videoid,score1:score1,score2:score2,score3:score3};
	    	$.post(url,data,function(msg){
	    		if(msg == 'success'){
		    		alert('提交成功');
	    			return;
	    		}else if(msg == 'again'){
	    			alert('您已经点评过了！');
	    			return;
	    		}else{
	    			alert('提交错误！');return;
	    		}
	    	})
	    }
		$(function () {
            $('.send_txt').bind('focus',function(){
                $('.cover').show();
                $('.send_comm_box').hide();
                $('.send_comm_box2').show();
            });
        });
	</script>
	<script type="text/javascript">
		$('.evaluation .list01 span').click(function(){
			$('.evaluation .list01 span').unbind("click");
		});
		$('.evaluation .list02 span').click(function(){
	    	$('.evaluation .list02 span').unbind("click");
	    });
		$('.evaluation .list03 span').click(function(){
	    	$('.evaluation .list03 span').unbind("click");
	    });
	</script>
	<script type="text/javascript">
		$('.evaluation_box .evaluation .list span').click(function(){
	    	console.log($(this).index())
	    	var num = $(this).index()+1;
	    	$(this).parent().find('.star').each(function(){
	        	$(this).attr('src','/assets/images/mobile/fund/icon_star2.png')
	    	})
	        for(var i=0 ; i<num;i++){
	            $(this).parent().find('.star').eq(i).attr('src','/assets/images/mobile/fund/icon_star1.png')
	        }
	    	$('#score1').val(num);
	    	submit_video_score();
	    	$('.evaluation .list span').unbind("click");
	    });
	    $('.evaluation_box .evaluation .list2 span').click(function(){
	    	console.log($(this).index())
	    	var num = $(this).index()+1;
	    	$(this).parent().find('.star').each(function(){
	        	$(this).attr('src','/assets/images/mobile/fund/icon_star2.png')
	    	})
	        for(var i=0 ; i<num;i++){
	            $(this).parent().find('.star').eq(i).attr('src','/assets/images/mobile/fund/icon_star1.png')
	        }
	    	$('#score2').val(num);
	    	submit_video_score();
	    	$('.evaluation .list2 span').unbind("click");
	    });
	    $('.evaluation_box .evaluation .list3 span').click(function(){
	    	console.log($(this).index())
	    	var num = $(this).index()+1;
	    	$(this).parent().find('.star').each(function(){
	        	$(this).attr('src','/assets/images/mobile/fund/icon_star2.png')
	    	})
	        for(var i=0 ; i<num;i++){
	            $(this).parent().find('.star').eq(i).attr('src','/assets/images/mobile/fund/icon_star1.png')
	        }
	    	$('#score3').val(num);
	    	submit_video_score();
	    	$('.evaluation .list3 span').unbind("click");
	    });
	</script>
@stop