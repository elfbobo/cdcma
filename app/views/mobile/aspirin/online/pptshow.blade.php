@extends('mobile.common.layoutasp2')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金@stop
@section('content')
@include('mobile.common.stopforward')
	<div class="page_top_box" style="width:auto;">
		@if($iFlag == 1)
		<input type="button" class="btn_back" onclick="window.location.href='/mobile-aspirin-online/ppt/{{$oPpt->catid}}';"/>
		@else
    	<input type="button" class="btn_back" onclick="window.location.href='/mobile-aspirin-online/ppt-list/{{$oPpt->catid}}';"/>
    	@endif
        课件浏览
        <input type="button" class="btn_share" onclick="window.location.href='{{$oPpt->ppt_url}}';"/>
    </div>
    
    <div class="ppt_box">
        <div class="ppt_pic">            
            <div class="swiper-container">
                <div class="swiper-wrapper">
                	@foreach($oPptImage as $k=>$v)
                    <div class="swiper-slide">
                        <div class="banner_img"><img src="{{$v->image_url}}"/></div>
                    </div>
                    @endforeach
                </div>
                <div class="swiper-button-next"></div>
		        <div class="swiper-button-prev"></div>
            </div>            
        </div>
    </div>
    
    <script>
		$(document).ready(function(e) {
			var winW = document.documentElement.clientWidth;
			var winH = document.documentElement.clientHeight;
			
			$('.ppt_box').width(winW);
			$('.ppt_box').height(winH-45);
			
			if( winW < winH){
				//竖屏
				$('.ppt_pic img').width(winW);				
			} else {
				//横屏
				$('.ppt_pic img').height(winH-100);	;
			} 
			
			var mySwiper = new Swiper('.swiper-container', {
				//autoplay: 3000,
				pagination: '.swiper-pagination',
				loop:true,
				nextButton: '.swiper-button-next',
				prevButton: '.swiper-button-prev'
			})
        });
	</script>
@stop
