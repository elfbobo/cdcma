@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金 @stop
@section('keywords')阿司匹林专项基金 @stop
@section('content')
@include('mobile.common.stopforward')
<script>
function change_hospital(id){
    var pid = id-1;
	var hospital = $("#hospital"+pid).html();
	var hospitalid = $("#hospital"+pid).attr('name');
	console.log(hospital);
	
	var html_null = '请选择';
	if(pid==1){
		$('#user_province').val(hospitalid);
		$('#user_city').val(0);
		$('#user_county').val(0);
		$('#user_company').val(0);
		$("#hospital02").empty();
		$("#hospital03").empty();
		$("#hospital04").empty();
		$("#hospital"+2).html(html_null);
		$("#hospital"+3).html(html_null);
		$("#hospital"+4).html(html_null);
    }else if(pid==2){
    	$('#user_city').val(hospitalid);
    	$('#user_county').val(0);
		$('#user_company').val(0);
		$("#hospital03").empty();
		$("#hospital04").empty();
    	$("#hospital"+3).html(html_null);
		$("#hospital"+4).html(html_null);
    }else if(pid==3){
    	$('#user_county').val(hospitalid);
    	$('#user_company').val(0);
    	$("#hospital04").empty();
    	$("#hospital"+4).html(html_null);
    }else {
    	$('#user_company').val(hospitalid);
        return ;
    }
	var url = '/user/hospital-child/'+hospitalid;
	$.post(url,{},function(json){
		data = json;
		$("#hospital0"+id).empty();
    	if(data=='noinfo'){
			var html = '';
			$("#hospital0"+id).append(html);
		}else{
			var bShow = false;
			var html = '';
			for(var elem in data){
				bShow = true;
				html += '<div class="select_line"><a class="sele_item" href="javascript:void(0);" name="'+elem+'">'+data[elem]+'</a></div>';
			}
			if(bShow){
				$("#hospital0"+id).append(html);
			}
		}	
		},'json');
}
</script>
	<link rel="stylesheet" type="text/css" href="/assets/css/mobileaspirin/common.css"/>
	<div class="wrap">
		<div class="page_top_box">
	        <input type="button" class="btn_back" onclick="window.location.href='/mobile-aspirin-online/my-enter-list';"/>
	        <div class="page_top">查看/邀请医生</div>
	        <input type="button" class="btn_txt" value="邀请医生信息" style="visibility:hidden;"/>
	    </div>
	    <input type="hidden" id="user_province" name="user_province" value="0"/>
        <input type="hidden" id="user_city" name="user_city" value="0"/> 
        <input type="hidden" id="user_county" name="user_county" value="0"/>
        <input type="hidden" id="user_company" name="user_company" value="0"/>
        <input type="hidden" id="timeid" name="timeid" value="{{$iOnlineTimeId}}"/>
          
	    <div class="wrap sign_up_box">
	        <div class="sign_up">
		        <div class="input_box province">
	                <label for="hospital1">省份</label>
	                <div class="input_select province_txt" id="hospital1" name="0">请选择</div>
	            </div>
	            <div class="input_box city">
	                <label for="hospital2">城市</label>
	                <div class="input_select city_txt" id="hospital2" name="0">请选择</div>
	            </div>
	            <div class="input_box area">
	                <label for="hospital3">区/县</label>
	                <div class="input_select area_txt" id="hospital3" name="0">请选择</div>
	            </div>
	            <div class="input_box hosp">
	                <label for="hospital4">医院</label>
	                <div class="input_select hosp_txt" id="hospital4" name="0">请选择</div>
	            </div>
	            <div class="input_box">
	                <label for="choose_doctor">医生</label>
	                <div class="input_select" id="choose_doctor" onclick="choose_doctor();"></div>
	            </div>
	            <div class="invite_box mart_20">参会医生：</div>
	            @foreach($aInviteList as $k=>$v)
	            <div class="invite_box" id="invite_{{$v['invite_id']}}">
	                <span>{{sub_str($v['doc_hospital'],8)}}</span>
	                <span>{{$v['doc_name']}}</span>
	                @if($v['join_type'] == 1)
	                <span><img src="/assets/images/mobile/fund/x2.png" alt="" onclick="submit_doctor_del({{$v['invite_id']}});"></span>
	                @else
	                <span></span>
	                @endif
	            </div>
	            @endforeach
	        </div>
	<!--         <div class="signup_foot"> -->
	<!--             <button class="btn">确定</button> -->
	<!--         </div> -->
	    </div>
	</div>
	<!--选择省份-->
    <div class="select_box pop_province_box">
        <div class="select_top">
            <div class="cancel province_cancel">取消</div>
            <div class="sele_txt province_tit">省份</div>
            <div class="submit province_submit">完成</div>
        </div>
        <div class="select_cont">
        @foreach($aHosp as $k=>$v)
        <div class="select_line"><a class="sele_item" href="javascript:void(0);" name="{{$k}}">{{$v}}</a></div>
        @endforeach
        </div>
    </div>
	<!--选择城市-->
    <div class="select_box pop_city_box">
        <div class="select_top">
            <div class="cancel city_cancel">取消</div>
            <div class="sele_txt city_tit">城市</div>
            <div class="submit city_submit">完成</div>
        </div>
        <div class="select_cont" id="hospital02">
        </div>
    </div>
    <!--选择区县-->
    <div class="select_box pop_area_box">
        <div class="select_top">
            <div class="cancel area_cancel">取消</div>
            <div class="sele_txt area_tit">区县</div>
            <div class="submit area_submit">完成</div>
        </div>
        <div class="select_cont" id="hospital03">
        </div>
    </div>
    <!--选择医院-->
    <div class="select_box pop_hosp_box">
        <div class="select_top">
            <div class="cancel hosp_cancel">取消</div>
            <div class="sele_txt hosp_tit">医院</div>
            <div class="submit hosp_submit">完成</div>
        </div>
        <div class="select_cont" id="hospital04">
        </div>
    </div>
	<script type="text/javascript">
		function choose_doctor(){
			var timeid = $('#timeid').val();
			var iHospitalId = $('#user_company').val();
			if(!timeid ||timeid == 0 ||!iHospitalId || iHospitalId == 0){
				alert('请选择医院');return;
			}
			var url = "/mobile-aspirin-online/invite-doctor/"+timeid+'/'+iHospitalId;
			window.location.href = url;
		}
	    function submit_doctor_del(inviteid){
			if(inviteid == 0){
				alert('参数错误');return;
			}
			var url = '/mobile-aspirin-online/invite-delete';
			var data = {inviteid:inviteid};
			$.post(url,data,function(msg){
			 	if(msg == 'success'){
			 		$('#invite_'+inviteid).remove();
				}else{
					alert('请刷新重试!');
					return;
				}
			})
		}
	</script>
    <script type="text/javascript">
        $(document).ready(function () {
        	//选择省份
            $('.province').click(function () {
                var bottom = $('.pop_province_box').css("bottom");
                if( bottom == "0px"){
                    $('.pop_province_box').animate({bottom:"-100%"});
                    return;
                }
                $('.pop_province_box').animate({bottom:"0"});
                $('.pop_province_box .sele_item').click(function () {
                    $('.pop_province_box .sele_item').removeClass("sele_bg");
                    $(this).addClass("sele_bg");
                });

                $('.province_submit').click(function () {
                    $('.pop_province_box').animate({bottom:"-100%"});
                    if($('.pop_province_box .sele_item').hasClass("sele_bg")){
                        $('.province .province_txt').html($('.pop_province_box .sele_bg').html());
                        var itemname = $('.pop_province_box .sele_bg').attr('name');
                        $('.province .province_txt').attr('name',itemname);
                        //省份选中，筛选城市
                        change_hospital(2);
                    }
                });

                $('.province_cancel').click(function () {
                    $('.pop_province_box .sele_item').removeClass("sele_bg");
                    $('.pop_province_box').animate({bottom:"-100%"});
                });
            });
            //选择城市
            $('.city').click(function () {
                var bottom = $('.pop_city_box').css("bottom");
                if( bottom == "0px"){
                    $('.pop_city_box').animate({bottom:"-100%"});
                    return;
                }
                $('.pop_city_box').animate({bottom:"0"});

                $('.pop_city_box .sele_item').click(function () {
                    $('.pop_city_box .sele_item').removeClass("sele_bg");
                    $(this).addClass("sele_bg");
                });

                $('.city_submit').click(function () {
                    $('.pop_city_box').animate({bottom:"-100%"});
                    if($('.pop_city_box .sele_item').hasClass("sele_bg")){
                        $('.city .city_txt').html($('.pop_city_box .sele_bg').html());
                        var itemname = $('.pop_city_box .sele_bg').attr('name');
                        $('.city .city_txt').attr('name',itemname);
                        //城市选中，筛选区县
                        change_hospital(3);
                    }
                });

                $('.city_cancel').click(function () {
                    $('.pop_city_box .sele_item').removeClass("sele_bg");
                    $('.pop_city_box').animate({bottom:"-100%"});
                });
            });
            //选择区/县
            $('.area').click(function () {
                var bottom = $('.pop_area_box').css("bottom");
                if( bottom == "0px"){
                    $('.pop_area_box').animate({bottom:"-100%"});
                    return;
                }

                $('.pop_area_box').animate({bottom:"0"});

                $('.pop_area_box .sele_item').click(function () {
                    $('.pop_area_box .sele_item').removeClass("sele_bg");
                    $(this).addClass("sele_bg");
                });

                $('.area_submit').click(function () {
                    $('.pop_area_box').animate({bottom:"-100%"});
                    if($('.pop_area_box .sele_item').hasClass("sele_bg")){
                        $('.area .area_txt').html($('.pop_area_box .sele_bg').html());
                        var itemname = $('.pop_area_box .sele_bg').attr('name');
                        $('.area .area_txt').attr('name',itemname);
                        //区县选中，筛选医院
                        change_hospital(4);
                    }
                });

                $('.area_cancel').click(function () {
                    $('.pop_area_box .sele_item').removeClass("sele_bg");
                    $('.pop_area_box').animate({bottom:"-100%"});
                });
            });
            //选择医院
            $('.hosp').click(function () {
                //不选择区县一项时，根据城市id获取医院
                if($('#user_county').val() == 0){
                    var cityid = $('#user_city').val();
                    if(cityid == 0||!cityid){
                        alert('请选择城市');return;
                    }
                    var url = '/mobile-aspirin-online/hospital';
                	$.post(url,{cityid:cityid},function(json){
                		data = json;
                		$("#hospital04").empty();
                    	if(data=='noinfo'){
                			var html = '';
                			$("#hospital04").append(html);
                		}else{
                			var bShow = false;
                			var html = '';
                			for(var elem in data){
                				bShow = true;
                				html += '<div class="select_line"><a class="sele_item" href="javascript:void(0);" name="'+elem+'">'+data[elem]+'</a></div>';
                			}
                			if(bShow){
                				$("#hospital04").append(html);
                				var bottom = $('.pop_hosp_box').css("bottom");
                                if( bottom == "0px"){
                                    $('.pop_hosp_box').animate({bottom:"-100%"});
                                    return;
                                }
                                $('.pop_hosp_box').animate({bottom:"0"});

                                $('.pop_hosp_box .sele_item').click(function () {
                                    $('.pop_hosp_box .sele_item').removeClass("sele_bg");
                                    $(this).addClass("sele_bg");
                                });

                                $('.hosp_submit').click(function () {
                                    $('.pop_hosp_box').animate({bottom:"-100%"});
                                    if($('.pop_hosp_box .sele_item').hasClass("sele_bg")){
                                        $('.hosp .hosp_txt').html($('.pop_hosp_box .sele_bg').html());
                                        var itemname = $('.pop_hosp_box .sele_bg').attr('name');
                                        $('.hosp .hosp_txt').attr('name',itemname);
                                        //医院选中，筛选医生
                                        change_hospital(5);
                                    }
                                });

                                $('.hosp_cancel').click(function () {
                                    $('.pop_hosp_box .sele_item').removeClass("sele_bg");
                                    $('.pop_hosp_box').animate({bottom:"-100%"});
                                });
                			}
                		}	
                		},'json');
            		
                }else{
	                var bottom = $('.pop_hosp_box').css("bottom");
	                if( bottom == "0px"){
	                    $('.pop_hosp_box').animate({bottom:"-100%"});
	                    return;
	                }
	                $('.pop_hosp_box').animate({bottom:"0"});
	
	                $('.pop_hosp_box .sele_item').click(function () {
	                    $('.pop_hosp_box .sele_item').removeClass("sele_bg");
	                    $(this).addClass("sele_bg");
	                });
	
	                $('.hosp_submit').click(function () {
	                    $('.pop_hosp_box').animate({bottom:"-100%"});
	                    if($('.pop_hosp_box .sele_item').hasClass("sele_bg")){
	                        $('.hosp .hosp_txt').html($('.pop_hosp_box .sele_bg').html());
	                        var itemname = $('.pop_hosp_box .sele_bg').attr('name');
	                        $('.hosp .hosp_txt').attr('name',itemname);
	                        //医院选中，筛选医生
	                        change_hospital(5);
	                    }
	                });
	
	                $('.hosp_cancel').click(function () {
	                    $('.pop_hosp_box .sele_item').removeClass("sele_bg");
	                    $('.pop_hosp_box').animate({bottom:"-100%"});
	                });
                }
            });
        });
    </script>
@stop