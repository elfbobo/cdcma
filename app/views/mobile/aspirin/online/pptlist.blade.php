@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金@stop
@section('content')
@include('mobile.common.stopforward')
	<div class="wrap">
        <div class="page_top_box">
            <input type="button" class="btn_back" onclick="window.location.href='/mobile-aspirin-online/index';"/>
            <div class="page_top">查看课件</div>
            <input type="button" class="btn_search" style="visibility:hidden;"/>
        </div>
        <div class="highlights_tab style2">
            <span @if($iCatId == 1)class="sel" @endif onclick="window.location.href='/mobile-aspirin-online/ppt-list/1';">心内科</span>
            <span @if($iCatId == 2)class="sel" @endif onclick="window.location.href='/mobile-aspirin-online/ppt-list/2';">神内科</span>
        </div>
        @if($oPpt)
        <div class="wrap-style1 flex_content">
        @foreach($oPpt as $k=>$v)
            <div class="cont-list">
            	<a href="/mobile-aspirin-online/ppt-show/{{$v->id}}">
                <dl>
                    <dt><img src="{{$v->ppt_thumb}}" alt=""></dt>
                    <dd>
                        <p class="title">{{$v->ppt_title}}</p>
                        <p class="time">{{substr($v->created_at,0,10)}}</p>
                    </dd>
                </dl>
                </a>
            </div>
        @endforeach
        </div>
        @endif
    </div>
@stop
