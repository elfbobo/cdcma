@extends('mobile.common.layoutasp3')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金@stop
@section('content')
@include('mobile.common.stopforward')
	<link rel="stylesheet" type="text/css" href="/assets/css/mobileaspirin/index.css"/>
 	<!-- 1. Define some markup -->
 	<!-- 
	<button class="btn" data-clipboard-text="Just because you can doesn't mean you should — clipboard.js">
	    Copy to clipboard
	</button>
	 -->
    <!-- 2. Include library -->
    <script src="/assets/js/dist/clipboard.min.js"></script>

    <!-- 3. Instantiate clipboard -->
    <script>
    var clipboard = new Clipboard('#btn');

    clipboard.on('success', function(e) {
        console.log(e);
    });

    clipboard.on('error', function(e) {
        console.log(e);
    });
    </script>
	<div class="page_top_box">
    	<input type="button" class="btn_back" onclick="window.location.href='/mobile-user';"/>
        <div class="page_top">我的会议</div>
        <input type="button" class="btn_shopping" style="visibility:hidden;"/>
    </div>
    @if(isset($oInfo)&&$oInfo)
	<input type="hidden" id="catid" value="{{$oOnlineInfo->catid}}"/>
	<input type="hidden" id="timeid" value="{{$oInfo->id}}"/>
    <div class="page_cont">
        <div class="meeting_box2">
            <div class="meeting_tab">
                <a class="sele" href="#">主讲会议</a>
                <a href="/mobile-aspirin-online/my-attend-list">参加会议</a>
            </div>
            <div class="meeting_cont">
                <div class="list">
                    <div class="time">{{date('Y年m月d日',strtotime($oInfo->online_date))}} {{$oInfo->time_period}}</div>
                    <div class="theme">会议主题：{{$oPpt->ppt_title}}</div>
                    <!-- default:蓝色按钮可点；ban:灰色不可点；cancel:绿色； -->
                    <div class="expert_meeting">
                    	@if($iFlag == 0)
                        <a  class="default" onclick="$('#shade').show();$('#pre_notice_box').show();">取消会议</a>
                        <a href="javascript:void(0);" class="ban" onclick="alert('会议未开始！');">启动会议</a>
                    	<a href="/mobile-aspirin-online/invite-list/{{$oInfo->id}}" class="default">查看/邀请</a>
                    	@elseif($iFlag == 2)
                    	<a href="javascript:void(0);" class="ban">会议结束</a>
                    	<a href="javascript:void(0);" class="ban">启动会议</a>
                    	<a href="javascript:void(0);" class="ban">查看/邀请</a>
                    	@else
                    	<a href="javascript:void(0);" class="ban">取消会议</a>
                    	<a class="default" onclick="$('#shade').show();$('#meeting_notice_box').show();">启动会议</a>
                        <a href="javascript:void(0);" class="ban">查看/邀请</a>
                   		@endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="shade" id="shade" style="display:none;"></div>
    <div class="module-window" id="pre_notice_box" style="display:none;">
        <div class="txt">
            <p>您确认取消会议？</p>
        </div>
        <div class="btn_box">
            <button class="btn" onclick="$('#shade').hide();$('#pre_notice_box').hide();">再考虑一下</button>
            <button class="btn" onclick="submit_cancel({{$oInfo->id}});">确定</button>
        </div>
    </div>
    <div class="module-window2" id="submit_notice_box" style="display:none;">
        <div class="x_box"><img src="/assets/images/mobile/fund/x.png" alt="" onclick="window.location.reload();"></div>
        <div class="txt">
            <p class="text-align">您已成功取消报名</p>
        </div>
    </div>
    <div class="module-window" id="meeting_notice_box" style="display:none;">
        <div class="txt">
        	<p>温馨提示：</p>
            <p>会议时间不少于30分钟。</p>
            <p>幻灯分享链接:</p>
            <p>{{$sPptHtml}}进入会议后打开共享网页直接粘贴即可分享幻灯。</p>
        </div>
        <div class="btn_box">
            <button class="btn" onclick="$('#shade').hide();$('#meeting_notice_box').hide();">再考虑一下</button>
            <button class="btn" id="btn" data-clipboard-text="{{$sPptHtml}}" onclick="submit_open({{$oInfo->id}});">确定</button>
        </div>
    </div>
    @else
    <div class="page_cont">
        <div class="meeting_box2">
            <div class="meeting_tab">
                <a class="sele" href="#">主讲会议</a>
                <a href="/mobile-aspirin-online/my-attend-list">参加会议</a>
            </div>
            <div class="meeting_cont">
            </div>
        </div>
    </div>
    @endif
    <script type="text/javascript">
    function submit_cancel(timeid){
		if(timeid == 0){
			alert('会议参数错误');return;
		}
		var url = '/mobile-aspirin-online/speaker-cancel';
		var data = {timeid:timeid};
		$.post(url,data,function(msg){
		 	if(msg == 'success'){
		 		$('#pre_notice_box').hide();
				$('#submit_notice_box').show();
			}else{
				alert('请刷新重试!');
				return;
			}
		})
	}
    function submit_open(timeid){
		if(timeid == 0){
			alert('会议参数错误');return;
		}
		var url = '/mobile-aspirin-online/speaker-open';
		var data = {timeid:timeid};
		$.post(url,data,function(msg){
			if(msg.success){
				window.location.href = msg.url;
			}else{
				alert(msg.notice);
				$('#shade').hide();
				$('#meeting_notice_box').hide();
			}
		},'json');
	}
	</script>
@stop
