@extends('mobile.common.layoutasp3')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金@stop
@section('content')
@include('mobile.common.stopforward')
	<div class="page_top_box">
    	<input type="button" class="btn_back" onclick="window.location.href='/mobile-user';"/>
        <div class="page_top">我的会议</div>
        <input type="button" class="btn_shopping" style="visibility:hidden;"/>
    </div>
    <div class="page_cont">
        <div class="meeting_box2">
            <div class="meeting_tab">
                <a href="/mobile-aspirin-online/my-enter-list">主讲会议</a>
                <a class="sele" href="#">参加会议</a>
            </div>
            <div class="meeting_cont">
            	@foreach($aAttendInfo as $k=>$v)
                <div class="list meeting_list">
                    <div class="time">{{$v['online_date']}} {{$v['time_period']}}</div>
                    <div class="theme">会议主题：</div>
                    <div class="theme">{{$v['online_title']}}</div>
                    <div class="theme">授课专家：</div>
                    <div class="theme">{{$v['doc_name']}}    {{$v['doc_hospital']}}</div>
                    <div class="expert_meeting">
                        <a href="#" class=""></a>
                        @if($v['btn_flag'] == 0)
                        <a href="javascript:void(0);" class="default" onclick="submit_attend({{$v['speaker_id']}},{{$v['online_time_id']}})">参加会议</a>
                        @else
                        <a href="#" class="ban">会议结束</a>
                        @endif
                        <a href="#" class=""></a>
                    </div>
                </div>
                @endforeach
                @foreach($aInviteInfo as $k=>$v)
                <div class="list meeting_list">
                    <div class="intro">
                        <span>{{$v['doc_hospital']}}</span>
                        <span class="expert">{{$v['doc_name']}}</span>
                        <span>邀请您参加会议</span>
                    </div>
                    <div class="expert_meeting">
                    	@if($v['ignore_flag'] == 1)
                        <div><a href="#" class="ban">忽略</a></div>
                        <div><a href="#" class="ban">同意</a></div>
                        @else
                        <div><a href="javascript:void(0);" class="cancel" onclick="submit_ignore({{$v['speaker_id']}},{{$v['online_time_id']}});">忽略</a></div>
                        <div><a href="javascript:void(0);" class="agree" onclick="submit_agree({{$v['speaker_id']}},{{$v['online_time_id']}});">同意</a></div>
                        @endif
                    </div>
                    @if($v['ignore_flag'] == 1)
                    <div class="tips">您已忽略本次会议</div>
                    @endif
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <script type="text/javascript">
    function submit_attend(speakerid,timeid){
    	if(!speakerid || !timeid){
			alert('参数错误');return;
		}
		var url = '/mobile-aspirin-online/listener-attend';
		var data = {speakerid:speakerid,timeid:timeid};
		$.post(url,data,function(msg){
			if(msg.success){
				window.location.href = msg.url;
			}else{
				alert(msg.notice);return;
			}
		},'json')
    }
    function submit_agree(speakerid,timeid){
    	if(!speakerid || !timeid){
			alert('参数错误');return;
		}
		var url = '/mobile-aspirin-online/agree-attend';
		var data = {speakerid:speakerid,timeid:timeid};
		$.post(url,data,function(msg){
		 	if(msg == 'success'){
		 		window.location.reload();
			}else{
				alert('请刷新重试!');
				return;
			}
		})
	}
    function submit_ignore(speakerid,timeid){
    	if(!speakerid || !timeid){
			alert('参数错误');return;
		}
		var url = '/mobile-aspirin-online/ignore-attend';
		var data = {speakerid:speakerid,timeid:timeid};
		$.post(url,data,function(msg){
		 	if(msg == 'success'){
		 		window.location.reload();
			}else{
				alert('请刷新重试!');
				return;
			}
		})
	}
	</script>
@stop
