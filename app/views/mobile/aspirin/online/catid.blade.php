@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金@stop
@section('content')
@include('mobile.common.stopforward')
<div class="wrap">
    <div class="page_top_box">
	    <input type="button" class="btn_back" onclick="window.location.href='/mobile-aspirin-online/index';"/>
        <div class="page_top">在线会议</div>
        <input type="button" class="btn_search" style="visibility:hidden"/>
    </div>
    <div class="flex_content online-meeting_index">
    @if($iType == 1)
        <div class="index-module">
            <a href="/mobile-aspirin-online/ppt/1">
                <dl>
                    <dt><img src="/assets/images/mobile/fund/online-meeting_index1.png" alt=""></dt>
                </dl>
            </a>
            <a href="javascript:void(0);" style="visibility:hidden">
                <dl>
                    <dt><img src="/assets/images/mobile/fund/online-meeting_index1.png" alt=""></dt>
                </dl>
            </a>
        </div>
        <div class="index-module">
            <a href="javascript:void(0);" style="visibility:hidden">
                <dl>
                    <dt><img src="/assets/images/mobile/fund/online-meeting_index1.png" alt=""></dt>
                </dl>
            </a>
            <a href="/mobile-aspirin-online/ppt/2">
                <dl>
                    <dt><img src="/assets/images/mobile/fund/online-meeting_index2.png" alt=""></dt>
                </dl>
            </a>
        </div>
    @else
    	<div class="index-module">
            <a href="/mobile-aspirin-online/listener-list/1">
                <dl>
                    <dt><img src="/assets/images/mobile/fund/online-meeting_index1.png" alt=""></dt>
                </dl>
            </a>
            <a href="javascript:void(0);" style="visibility: hidden">
                <dl>
                    <dt><img src="/assets/images/mobile/fund/online-meeting_index1.png" alt=""></dt>
                </dl>
            </a>
        </div>
        <div class="index-module">
        	<a href="javascript:void(0);" style="visibility:hidden">
                <dl>
                    <dt><img src="/assets/images/mobile/fund/online-meeting_index1.png" alt=""></dt>
                </dl>
            </a>
            <a href="/mobile-aspirin-online/listener-list/2">
                <dl>
                    <dt><img src="/assets/images/mobile/fund/online-meeting_index2.png" alt=""></dt>
                </dl>
            </a>
        </div>
    @endif
    </div>
</div>
@stop