@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金 @stop
@section('content')
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
@if($oAspirinResearchApply->apply_type == 1)
	@if($isWxFlag)
	wx.config({
		debug:false,
		appId: "{{$wx_config['appId']}}",//必填
		timestamp:{{$wx_config['timestamp']}}, // 必填，生成签名的时间戳
		nonceStr: "{{$wx_config['nonceStr']}}", // 必填，生成签名的随机串
		signature: "{{$wx_config['signature']}}",// 必填，签名
		jsApiList: ['onMenuShareTimeline','onMenuShareAppMessage']// 必填，需要使用的JS接口列表，所有JS接口列表见附录2
	});
	wx.ready(function(){
		wx.onMenuShareTimeline({//分享到朋友圈
		    title:'专项基金科研培训',
		    link: '{{Config::get('app.url')}}/aspirinshare/research-result?iApplyId=1',
		    imgUrl: '{{Config::get('app.url')}}/assets/images/mobile/logo.png',
		    success: function () {  },
		    cancel: function () {  }
		});
		wx.onMenuShareAppMessage({//分享给朋友
		    title: '专项基金科研培训',
		    desc: '专项基金科研培训',
		    link: '{{Config::get('app.url')}}/aspirinshare/research-result?iApplyId=1',
		    imgUrl: '{{Config::get('app.url')}}/assets/images/mobile/logo.png',
		    type: '',
		    dataUrl: '',
		    success: function (){ },
		    cancel: function (){ }
		});
	});
	wx.error(function(res){ });
	@endif
@else
	@if($isWxFlag)
	wx.config({
		debug:false,
		appId: "{{$wx_config['appId']}}",//必填
		timestamp:{{$wx_config['timestamp']}}, // 必填，生成签名的时间戳
		nonceStr: "{{$wx_config['nonceStr']}}", // 必填，生成签名的随机串
		signature: "{{$wx_config['signature']}}",// 必填，签名
		jsApiList: ['hideOptionMenu']// 必填，需要使用的JS接口列表，所有JS接口列表见附录2
	});
	wx.ready(function(){
		wx.hideOptionMenu();
	});
	wx.error(function(res){ });
	@endif
@endif
</script>
<style>
.scientific_research_detail .fill_list .input_box label {width: 100px;}
</style>
	<div class="page_top_box">
        <a href="/aspirinfund/scientific-research"><input type="button" class="btn_back" /></a>
        <div class="page_top">科研培训</div>
        <input type="button" class="btn_txt" value="我要申请" style="visibility:hidden;"/>
    </div>
    <div class="wrap-style1 scientific_research_detail">
        <div class="cont-list">
            <div class="fill_list">
                <div class="input_box">
                    <label for="">姓名：</label>
                    <p style="font-size:13px;">{{$oAspirinResearchApply->apply_name}}</p>
                </div>
                <div class="input_box">
                    <label for="">城市：</label>
                    <p style="font-size:13px;">{{$oAspirinResearchApply->apply_city}}</p>
                </div>
                <div class="input_box">
                    <label for="">出生日期：</label>
                    <p style="font-size:13px;">{{date("Y-m-d",strtotime($oAspirinResearchApply->apply_birthday))}}</p>
                </div>
                <div class="input_box">
                    <label for="">职称：</label>
                    <p style="font-size:13px;">{{$oAspirinResearchApply->apply_title}}</p>
                </div>
                <div class="input_box">
                    <label for="">联系电话：</label>
                    <p style="font-size:13px;">{{$oAspirinResearchApply->apply_tel}}</p>
                </div>
                <div class="input_box">
                    <label for="">电子邮箱：</label>
                    <p style="font-size:13px;">{{$oAspirinResearchApply->apply_email}}</p>
                </div>
                <div class="input_box">
                    <label for="">单位名称：</label>
                    <p style="font-size:13px;">{{$oAspirinResearchApply->apply_company}}</p>
                </div>
                <div class="input_box">
                    <label for="">地址：</label>
                    <p style="font-size:13px;">{{$oAspirinResearchApply->apply_address}}</p>
                </div>
                <div class="input_box none">
                    <label for="">邮编：</label>
                    <p style="font-size:13px;">{{$oAspirinResearchApply->apply_postcode}}</p>
                </div>
                <div class="input_box none">
                    <label for="">教育背景：</label>
                    <p style="font-size:13px;">{{$oAspirinResearchApply->education_background}}</p>
                </div>
                <div class="input_box none">
                    <label for="">工作经历：</label>
                    @if(isset($oAspirinResearchApply->work_experience))
                    	 <p style="font-size:13px;">{{$oAspirinResearchApply->work_experience}}</p>
                    @else
                    	 <p style="font-size:13px;">无</p>
                    @endif
                </div>
                <div class="input_box none">
                    <label for="">既往科研经历：</label>
                    @if(isset($oAspirinResearchApply->research_experience))
                    	 <p style="font-size:13px;">{{$oAspirinResearchApply->research_experience}}</p>
                    @else
                    	 <p style="font-size:13px;">无</p>
                    @endif
                </div>
            </div>
            <div class="fill_box">
                <p style="font-size:13px;">方向一（心血管疾病一级预防）： 心血管疾病的一级预防主要是通过对不良生活方式的干预（运动、饮食、戒烟酒）、高危人群抗血小板治疗、综合管理血脂、血糖及血压来减少心血管疾病的发生，在这些措施中，您认为哪个（些）措施在实施过程中尚存在需要进一步解决的问题？主要的问题是什么？需要相应采取什么措施来解决？</p>
            </div>
            <div class="fill_list detail_page_answer">
                <p style="font-size:13px;">{{$oAspirinResearchApply->question1}}</p>
            </div>
            <div class="fill_box">
                <p style="font-size:13px;">方向二（心血管疾病二级预防）： 心血管疾病二级预防的诸多措施，如生活方式干预、规范的抗血小板治疗、综合管理血脂、血糖和血压，来降低心血管疾病的复发及死亡，在这些措施中，您认为哪个（些）措施在实施过程中尚存在进一步解决的问题？主要的问题是什么？需要相应采取什么措施来解决？</p>
            </div>
            <div class="fill_list detail_page_answer">
                <p style="font-size:13px;">{{$oAspirinResearchApply->question2}}</p>
            </div>
            <div class="pbtm" style="font-size:13px;color:red;">
                <p>书写要求（请结合您认为需要解决的问题，简要陈述科研设想的背景、科研假说、科研目的，以及预期要得出的结论）</p>
            </div>
            </br>
            <div class="fill_box">
                <p style="font-size:13px;">针对您提到的关于心血管预防的问题，陈述相应的解决方案？</p>
            </div>
            <div class="fill_list detail_page_answer">
                <p style="font-size:13px;">{{$oAspirinResearchApply->question3}}</p>
            </div>
        </div>
    </div>
@stop
