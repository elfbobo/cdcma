@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金 @stop
@section('keywords')阿司匹林专项基金 @stop
@section('content')
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
@if($isWxFlag)
wx.config({
	debug:false,
	appId: "{{$wx_config['appId']}}",//必填
	timestamp:{{$wx_config['timestamp']}}, // 必填，生成签名的时间戳
	nonceStr: "{{$wx_config['nonceStr']}}", // 必填，生成签名的随机串
	signature: "{{$wx_config['signature']}}",// 必填，签名
	jsApiList: ['onMenuShareTimeline','onMenuShareAppMessage']// 必填，需要使用的JS接口列表，所有JS接口列表见附录2
});
wx.ready(function(){
	wx.onMenuShareTimeline({//分享到朋友圈
	    title:'专项基金科研培训',
	    link: '{{Config::get('app.url')}}/aspirinshare/research-apply',
	    imgUrl: '{{Config::get('app.url')}}/assets/images/mobile/logo.png',
	    success: function () {  },
	    cancel: function () {  }
	});
	wx.onMenuShareAppMessage({//分享给朋友
	    title: '专项基金科研培训',
	    desc: '专项基金科研培训',
	    link: '{{Config::get('app.url')}}/aspirinshare/research-apply',
	    imgUrl: '{{Config::get('app.url')}}/assets/images/mobile/logo.png',
	    type: '',
	    dataUrl: '',
	    success: function (){ },
	    cancel: function (){ }
	});
});
wx.error(function(res){ });
@endif
</script>
	<div class="page_top_box">
        <a href="/aspirinfund/scientific-research"><input type="button" class="btn_back" /></a>
        <div class="page_top">科研培训</div>
        <input type="button" class="btn_txt" value="我要申请" style="visibility:hidden;"/>
    </div>
    <div class="wrap-style1 scientific_research_fill">
    <input type="hidden" value="{{$iUid}}" id="user_id">
        <div class="cont-list">
            <div class="fill_list">
                <div class="input_box">
                    <label for="">姓名：</label>
                    <input type="text" class="input_text" id="apply_name" name="apply_name">
                </div>
                <div class="input_box">
                    <label for="">城市：</label>
                    <input type="text" class="input_text" id="apply_city" name="apply_city">
                </div>
                <div class="input_box">
                    <label for="">出生日期：</label>
                    <input type="text" class="input_text" id="apply_birthday" name="apply_birthday">
                </div>
                <div class="input_box">
                    <label for="">职称：</label>
                    <input type="text" class="input_text" id="apply_title" name="apply_title">
                </div>
                <div class="input_box">
                    <label for="">联系电话：</label>
                    <input type="text" class="input_text" id="apply_tel" name="apply_tel">
                </div>
                <div class="input_box">
                    <label for="">电子邮箱：</label>
                    <input type="text" class="input_text" id="apply_email" name="apply_email">
                </div>
                <div class="input_box">
                    <label for="">单位名称：</label>
                    <input type="text" class="input_text" id="apply_company" name="apply_company">
                </div>
                <div class="input_box">
                    <label for="">地址：</label>
                    <input type="text" class="input_text" id="apply_address" name="apply_address">
                </div>
                <div class="input_box none">
                    <label for="">邮编：</label>
                    <input type="text" class="input_text" id="apply_postcode" name="apply_postcode">
                </div>
            </div>
            <div class="fill_list textarea_box">
                <div class="input_box">
                    <label for="">教育背景：</label>
                </div>
                <textarea name="education_background" id="education_background"></textarea>
            </div>
            <div class="fill_list textarea_box">
                <div class="input_box">
                    <label for="">工作经历：</label>
                </div>
                <textarea name="work_experience" id="work_experience"></textarea>
            </div>
            <div class="fill_list textarea_box">
                <div class="input_box">
                    <label for="">既往科研经历：</label>
                </div>
                <textarea name="research_experience" id="research_experience"></textarea>
            </div>
            
            <div class="fill_box">
            	<p style="font-size:13px;">您可以选择下列两个方向中的任意一个 做科研设想及申请：</p>
                <p style="font-size:13px;">方向一（心血管疾病一级预防）： 心血管疾病的一级预防主要是通过对不良生活方式的干预（运动、饮食、戒烟酒）、高危人群抗血小板治疗、综合管理血脂、血糖及血压来减少心血管疾病的发生，在这些措施中，您认为哪个（些）措施在实施过程中尚存在需要进一步解决的问题？主要的问题是什么？需要相应采取什么措施来解决？</p>
            </div>
            <div class="fill_list textarea_box">
                <textarea name="question1" id="question1" class="no_space"></textarea>
            </div>
                       <div class="fill_box">
                <p  style="font-size:13px;">方向二（心血管疾病二级预防）： 心血管疾病二级预防的诸多措施，如生活方式干预、规范的抗血小板治疗、综合管理血脂、血糖和血压，来降低心血管疾病的复发及死亡，在这些措施中，您认为哪个（些）措施在实施过程中尚存在进一步解决的问题？主要的问题是什么？需要相应采取什么措施来解决？</p>
            </div>
            <div class="fill_list textarea_box">
                <textarea name="question2" id="question2" class="no_space"></textarea>
            </div>
            <div class="pbtm" style="font-size:13px;color:red;">
                	<p>书写要求（请结合您认为需要解决的问题，简要陈述科研设想的背景、科研假说、科研目的，以及预期要得出的结论）</p>
            </div>
            </br>
            <div class="fill_box">
                <p style="font-size:13px;">针对您提到的关于心血管预防的问题，陈述相应的解决方案？</p>
                <p style="font-size:13px;">（请简述拟采用的科研方法/目标人群/干预方法/主要观察指标/次要观察指标等）</p>
            </div>
            <div class="fill_list textarea_box">
                <textarea name="question3" id="question3" class="no_space"></textarea>
            </div>
            <div class="fill_btn">
            	 <button class="btn" onclick="check()">提交申请</button>
            </div>
        </div>
    </div>
<script>
function check(){
	var uid = $("#user_id").val();
	var apply_name = $("#apply_name").val();
	var apply_city = $("#apply_city").val();
	var apply_birthday = $("#apply_birthday").val();
	var apply_title = $("#apply_title").val();
	var apply_tel = $("#apply_tel").val();
	var apply_email = $("#apply_email").val();
	var apply_company = $("#apply_company").val();
	var apply_address = $("#apply_address").val();
	var apply_postcode = $("#apply_postcode").val();
	var education_background = $("#education_background").val();
	var work_experience = $("#work_experience").val();
	var research_experience = $("#research_experience").val();
	var question1 = $("#question1").val();
	var question2 = $("#question2").val();
	var question3 = $("#question3").val();
	if(apply_name.length < 1){
		alert("请填写姓名");
		return false;
	}
	if(apply_city.length < 1){
		alert("请填写城市");
		return false;
	}
	if(apply_birthday.length < 1){
		alert("请填写出生日期");
		return false;
	}
	if(apply_title.length < 1){
		alert("请填写职称");
		return false;
	}
	if(apply_tel.length < 1){
		
		alert("请填写联系电话");
		return false;
	}else{
		if(!(/^1[34578]\d{9}$/).test(apply_tel)){
			alert('手机格式不正确！');return false;
		}
	}
	if(apply_email.length < 1){
		alert("请填写电子邮箱");
		return false;
	}else{
		if(!(/^(\w-*\.*)+@(\w-?)+(\.\w{2,})+$/).test(apply_email)){
			alert('邮箱格式不正确！');return false;
		}
	}
	if(apply_company.length < 1){
		alert("请填写单位名称");
		return false;
	}
	if(apply_address.length < 1){
		alert("请填写地址");
		return false;
	}
	if(apply_postcode.length < 1){
		alert("请填写邮编");
		return false;
	}
	if(education_background.length < 1){
		alert("请填写教育背景");
		return false;
	}
	if(work_experience.length < 1){
		alert("请填写工作经历");
		return false;
	}
	if(research_experience.length < 1){
		alert("请填写科研经历");
		return false;
	}
	if(question1.length < 1 && question2.length < 1){
		alert("请在心血管疾病一级预防和二级预防中选择一个作答");
		return false;
	}
	
	if(question3.length < 1){
		alert("请陈述您的解决方案");
		return false;
	}
	
	
	var url = '/aspirinfund/apply-save';
	var data = {'user_id':uid,'apply_name':apply_name,'apply_city':apply_city,'apply_birthday':apply_birthday,
			'apply_title':apply_title,'apply_tel':apply_tel,'apply_email':apply_email,'apply_company':apply_company,
			'apply_address':apply_address,'apply_postcode':apply_postcode,'education_background':education_background,'work_experience':work_experience,
			'research_experience':research_experience,'question1':question1,'question2':question2,'question3':question3,
			};
	$.post(url,data,function(msg){
		if(msg.success){
			alert('提交资料已在审核中客服人员会与持续跟进');
			window.location.href="/aspirinfund/application-researchlist";
			return;
		}else{
			alert(msg.mes);
			return;
		}
	  },'json')
}
</script>
@stop
