@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金 @stop
@section('content')
@include('mobile.common.stopforward')
<div class="page_top_box">
    <a href="/aspirinfund/application-result"><input type="button" class="btn_back" /></a>
    <div class="page_top">科研培训</div>
    <input type="button" class="btn_txt" value="我要申请" style="visibility:hidden;"/>
</div>
<script>

//滑屏加载
//**********************下拉加载S**********************
var _isRun = true;
function cl1(flag){
	if(_isRun){
		_isRun = false;
		var ceil = $('#ceil').val();
		if(ceil==0&&(($(document).scrollTop()>=($(document).height()-$(window).height()-10))||flag==1)){
			$("#container").append('<div id="loading" style="color:#00a0e9;font-size: 0.9rem;margin-left: 40%;margin-top:2%;margin-bottom:5%;">加载中...</div> ');
			var page = parseInt($('#page').val());
			page++;
			var searchkeyword = document.getElementById('searchkeyword').value;
			var url = '/aspirinfund/applicationsuccess-add';
			var data = {'page':page,'keyword':searchkeyword};
			$.post(url,data,function(msg){
				var json = eval('('+msg+')');
				if(json.ceil==1){
					$('#ceil').val(1);
				}
				
				json.ceil = null;
				$('#page').val(page);
				var i=0;
				var n=(page-1)*10+1;
				$("#container").children('#loading').remove();
				for(i in json){
					if(json[i]){
					
						var str = '<p><a href="/aspirinfund/applicationsuccess-message/'+json[i].applyid+'" style="color:#666666;font-size:13px;">'+json[i].applykey+''+json[i].applyname+''+json[i].applytitle+''+json[i].applycompany +'</a></p>';
						$("#container").append(str);
					}	
					n++;
					i++;
				}
				_isRun = true;
				if($('#ceil').val()==1){
					$("#container").children('#loading').remove();
					var res = '<div id="loading" style="color:#00a0e9;font-size: 0.9rem;margin-left: 40%;margin-top:2%;margin-bottom:5%;">已加载全部</div>';
		      		$("#container").append(res);
					document.getElementById("footer").style.display="block";
					$('#ceil').val(2);
					_isRun = true;
				}
			});
		}else if(ceil==1){
			$("#container").children('#loading').remove();
			$("#container").append('<div id="loading" style="color:#00a0e9;font-size: 0.9rem;margin-left: 40%;margin-top:2%;margin-bottom:5%;">已加载全部</div>');
			$('#ceil').val(2);
			_isRun = true;
		}else{
			_isRun = true;
		}
	}
}

//**********************下拉加载E**********************
function searchapply()
{
	var searchkeyword = document.getElementById("searchkeyword").value;
	if(searchkeyword.length > 0){
		window.location.href="/aspirinfund/search-applicationsuccess/"+searchkeyword;
	}
}
</script>
 <input type="hidden" id="page" value="1"/> 
 <input type="hidden" id="ceil" value="0"/> 
 <div class="result_search">
        <input type="text" class="search_input" id="searchkeyword"  @if(!empty($sSearchKeyWord)) value="{{$sSearchKeyWord}}" @endif>
        <button class="btn" onclick="searchapply()">搜索</button>
    </div>
    <div class="wrap-style1 result_page">
        <div class="result_list" id="container">
        @if(count($oAspirinResearchApply) > 0)
	        @foreach($oAspirinResearchApply as $key=>$value)
	            <p style="color:#666666;font-size:13px;" ><a href="/aspirinfund/applicationsuccess-message/{{$value->id}}" style="color:#666666;">{{$key+1}}. {{$value->apply_name}} {{$value->apply_title}} {{$value->apply_company}}</a></p>
	        @endforeach
	    @endif
        </div>
    </div>
  <script>

//绑定事件
function changeOpen(e){
	$("#container").on('mousedown touchstart',page_touchstart);
	$(window).scroll(function(){
		page_touchstart();
	});
//	$(document).on('mousedown touchstart',page_touchstart);
};

//开启事件绑定滑动
changeOpen();

//触摸（鼠标按下）开始函数
function page_touchstart(e){
	cl1(0);
};

</script>
@stop
