@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金 @stop
@section('content')
@include('mobile.common.stopforward')
<script>
//滑屏加载
//**********************下拉加载S**********************
var _isRun = true;
function cl1(flag){
	if(_isRun){
		_isRun = false;
		var ceil = $('#ceil').val();
		if(ceil==0&&(($(document).scrollTop()>=($(document).height()-$(window).height()-10))||flag==1)){
			$("#container").append('<div id="loading" style="color:#00a0e9;font-size: 0.9rem;margin-left: 40%;margin-top:2%;margin-bottom:5%;">加载中...</div> ');
			var page = parseInt($('#page').val());
			page++;
			var url = '/aspirinfund/application-researchadd/'+page;
			var data = {};
			$.post(url,data,function(msg){
				var json = eval('('+msg+')');
				if(json.ceil==1){
					$('#ceil').val(1);
				}
				
				json.ceil = null;
				$('#page').val(page);
				var i=0;
				var n=(page-1)*10+1;
				$("#container").children('#loading').remove();
				for(i in json){
					if(json[i]){
					
						var str = '<a href="/aspirinfund/applicationsuccess-message/'+json[i].applyid+'">'+'<div class="apply_list">'+
		                '<div class="name" style="font-size:13px;">'+json[i].applyname+'</div>'+
		                '<div class="state_box">'+
		                    '<div class="time">申请时间：'+json[i].applydate+'</div>'+
		                    '<div class="state success">'+json[i].applytype+'</div>'+
		                '</div>'+
		            '</div></a>';
						$("#container").append(str);
					}	
					n++;
					i++;
				}
				_isRun = true;
				if($('#ceil').val()==1){
					$("#container").children('#loading').remove();
					var res = '<div id="loading" style="color:#00a0e9;font-size: 0.9rem;margin-left: 40%;margin-top:2%;margin-bottom:5%;">已加载全部</div>';
		      		$("#container").append(res);
					document.getElementById("footer").style.display="block";
					$('#ceil').val(2);
					_isRun = true;
				}
			});
		}else if(ceil==1){
			$("#container").children('#loading').remove();
			$("#container").append('<div id="loading" style="color:#00a0e9;font-size: 0.9rem;margin-left: 40%;margin-top:2%;margin-bottom:5%;">已加载全部</div>');
			$('#ceil').val(2);
			_isRun = true;
		}else{
			_isRun = true;
		}
	}
}

//**********************下拉加载E**********************
function skiptoapplypage()
{
	window.location.href="/aspirinfund/apply-research-add";
}
</script>
 <input type="hidden" id="page" value="1"/> 
 <input type="hidden" id="ceil" value="0"/> 
 <div class="page_top_box">
        <a href="/aspirinfund/scientific-research"><input type="button" class="btn_back" /></a>
        <div class="page_top">科研培训</div>
        @if(!Input::get('from'))
        <input type="button" class="btn_txt" value="我要申请" onclick="skiptoapplypage()"/>
        @else
        <input type="button" class="btn_txt" value="我要申请" style="visibility:hidden;"/>
        @endif
    </div>
<div class="wrap-style1 scientific_research"  >
        <div class="cont-list" id="container">
            @foreach($oAspirinResearchApply as $value)
            <a href="/aspirinfund/applicationsuccess-message/{{$value->id}}">
	            <div class="apply_list">
	                <div class="name" style="font-size:13px;">{{$value->apply_name}}</div>
	                <div class="state_box">
	                    <div class="time">申请时间：{{date("Y-m-d",strtotime($value->created_at))}}</div>
	                    @if($value->apply_type == 0)
	                    <div class="state success">未审核</div>
					    @endif
					    @if($value->apply_type == 1)
					    <div class="state success">审核通过</div>
					    @endif
					    @if($value->apply_type == 2)
					    <div class="state success">审核失败</div>
					    @endif
	                </div>
	            </div>
	           </a>
           @endforeach 
        </div>
</div>
<script>

//绑定事件
function changeOpen(e){
	$("#container").on('mousedown touchstart',page_touchstart);
	$(window).scroll(function(){
		page_touchstart();
	});
//	$(document).on('mousedown touchstart',page_touchstart);
};

//开启事件绑定滑动
changeOpen();

//触摸（鼠标按下）开始函数
function page_touchstart(e){
	cl1(0);
};

</script>
@stop
