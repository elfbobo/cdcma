@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金 @stop
@section('keywords')阿司匹林专项基金 @stop
@section('content')
@include('mobile.common.stopforward')
<div class="wrap">
	<div class="page_top_box">
    	<input type="button" class="btn_back" onclick="window.location.href='/mobile-aspirin';"/>
        <div class="page_top">科研培训</div>
        <input type="button" class="btn_search" style="visibility:hidden;"/>
    </div>
 	<div class="wrap-index wrap-style3 wrap-style4">
        <div class="index-cont">
            <div class="online_meeting">
                <div class="index-module">
                    <a href="/aspirinshare/research-brief" class="green  fixed" style="width:143px;height:143px;">
                        <dl>
                            <dt><img src="/assets/images/mobile/fund/scientific_research_icon1.png" alt=""></dt>
                            <dd>科研培训简介</dd>
                        </dl>
                    </a>
                    
                </div>
                <div class="index-module">
                	@if($oUser->role_id == 3 && $oUser->card_auth_flag == 1)
                    <a href="javascript::void(0)" onclick="alert('医师认证审核通过后方可参与')" class="red  fixed" style="width:143px;height:143px;">
                    @else
                    <a href="/aspirinfund/application-researchlist" class="red  fixed" style="width:143px;height:143px;">
                    @endif
                        <dl>
                            <dt><img src="/assets/images/mobile/fund/scientific_research_icon2.png" alt=""></dt>
                            <dd>科研培训</dd>
                        </dl>
                    </a>
                    <!-- /aspirinfund/application-result 结果公示连接 -->
                    <a href="javascript::void(0)" onclick="alert('开发中')" class="skyblue  fixed" style="width:143px;height:143px;">
                        <dl>
                            <dt><img src="/assets/images/mobile/fund/scientific_research_icon3.png" alt=""></dt>
                            <dd>结果公示</dd>
                        </dl>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
