@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金 @stop
@section('content')
@include('mobile.common.stopforward')
<div class="wrap">
	<div class="page_top_box">
    	<input type="button" class="btn_back" onclick="window.location.href='/aspirinfund/scientific-research';"/>
        <div class="page_top">科研培训</div>
        <input type="button" class="btn_search" style="visibility:hidden;"/>
    </div>
    <div class="wrap-index wrap-style2">
        <div class="index-cont">
            <div class="result_box">
            @if($iAspirinResearchApply >= 1)
                <p class="add">恭喜您</p>
                <p class="following">入围科研项目</p>
            @else
            	<p class="add"></p>
                <p class="following"></p>
            @endif
                <div class="btn_box">
                    <a href="/aspirinfund/application-list"><button class="btn">查看入围名单</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
