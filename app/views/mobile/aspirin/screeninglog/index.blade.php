@extends('mobile.common.layoutasp2')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金 @stop
@section('content')
<<table>
  <tr>
    <th>时间</th>
    <th>查看</th>
  </tr>
  @foreach($oScreenings as $value)
	  <tr>
	    <td>{{date("Y-m-d H:i:s",strtotime($value->created_at))}}</td>
	    <td><a href="/aspirinfund/screening-detail/{{$value->id}}">查看</a></td>
	  </tr>
 @endforeach
</table>

@stop
