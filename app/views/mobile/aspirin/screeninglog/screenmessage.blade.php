@extends('mobile.common.layoutasp2')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金 @stop
@section('content')
<table>
	<tr>
    	<td>性别</td>
     @if($oScreening->sex == 0)
    	<td>女</td>
      @else
    	<td>男</td>
      @endif
	</tr>
	<tr>
		 <td>年龄</td>
		 <td>{{$oScreening->age}}</td>
	 </tr>
	 <tr>
		 <td>体重</td>
		 <td>{{$oScreening->weight}}KG</td>
	 </tr>
	 <tr>
		 <td>身高</td>
		 <td>{{$oScreening->height}}</td>
	 </tr>
	 <tr>
		 <td>您是否吸烟</td>
		 @if($oScreening->question1 == 1)
		 	<td>否</td>
		 @else
		 	<td>是</td>
		 @endif
	 </tr>
	  @if($oScreening->sex == 0)
  	  <tr>
		 <td>您是否已经绝经</td>
		 @if($oScreening->question0 == 1)
		 	<td>否</td>
		 @else
		 	<td>是</td>
		 @endif
	  </tr>
      @endif
	 <tr>
		 <td>您是否有早发心脑血管疾病家族史？</td>
		 @if($oScreening->question2 == 1)
		 	<td>否</td>
		 @else
		 	<td>是</td>
		 @endif
	 </tr>
	 <tr>
		 <td>您是否已经有已确诊的糖尿病？</td>
		 @if($oScreening->question3 == 1)
		 	<td>否</td>
		 @elseif($oScreening->question3 == 2)
		 	<td>是</td>
		 @else
		 	<td>不清楚</td>
		 @endif
		 
		 
		 @if($oScreening->question3 == 2)
		 	<td>您是否在服用降糖病？</td>
		 	 @if($oScreening->question31 == 1)
		 	<td>否</td>
			 @else
		 	<td>是</td>
		 	@endif
		 @endif
	 </tr>
	 <tr>
		 <td>您是否有已诊断的高血脂 </td>
		  @if($oScreening->question4 == 1)
		 	<td>否</td>
		  @elseif($oScreening->question4 == 2)
		 	<td>是</td>
		  @else
		  	<td>不清楚</td>
		  @endif
		 
		 
		 @if($oScreening->question4 == 2 ||$oScreening->question4 == 3)
		 <td>您是否在服用降脂药 </td>
		 		 @if($oScreening->question41 == 1)
			 	<td>否</td>
				 @else
			 	<td>是</td>
			 	@endif
		 @endif
		 @if($oScreening->question4 == 2)
				 <td>治疗前总胆固醇水平  </td>
				  <td>{{$oScreening->question42}}</td>
		 @endif
	 </tr>
	 <tr>
		 <td>您是否有已诊断的高血压 </td>
		 @if($oScreening->question5 == 1)
		 	<td>否</td>
		 @elseif($oScreening->question5 == 2)
		 	<td>是</td>
		 @else
			 <td>不清楚</td>
		 @endif
		 
		 
		 @if($oScreening->question5 == 2 || $oScreening->question5 == 3)
			 <td>您是否在服用降压药 </td>
			 @if($oScreening->question51 == 1 )
			 	 <td>否</td>
			 @else
			 	 <td>是</td>
			 @endif
		 @endif
		@if($oScreening->question5 == 2)
			 <td>治疗前收缩压（高压） </td>
			 <td>{{$oScreening->question52}}</td>
			 <td>治疗前舒张压（低压）  </td>
			 <td>{{$oScreening->question53}}</td>
			 <td>治疗后收缩压（高压）  </td>
			 <td>{{$oScreening->question54}}</td>
		@endif
	 </tr>
	 <tr>
		 <td>您是否有已诊断的冠心病或心肌梗死病史</td>
		 @if($oScreening->question6 == 1)
		 	<td>否</td>
		 @else
		 	<td>是</td>
		 @endif
	 </tr>
	 <tr>
		 <td>您是否有已诊断的脑卒中病史</td>
		 @if($oScreening->question7 == 1)
		 	<td>否</td>
		 @else
		 	<td>是</td>
		 @endif
	 </tr>
	 <tr>
		 <td>您现在是否患有胃肠道出血/溃疡等疾病 </td>
		 @if($oScreening->question8 == 1)
		 	<td>否</td>
		 @elseif($oScreening->question8 == 2)
		 	<td>是</td>
		 @else
		 	<td>不清楚</td>
		 @endif
	 </tr>
	 <tr>
		 <td>您是否正在服用抗血小板药物 </td>
		 @if($oScreening->question9 == 1)
		 	<td>否</td>
		 @else
		 	<td>是</td>
		 	<td>抗血小板药物名称（可多选） </td>
		 	<td>{{$oScreening->question91}}</td>
		 @endif
		 
	 </tr>
</table>
@stop
