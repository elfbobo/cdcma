@extends('mobile.common.layoutasp2')
@section('title')国卫健康云—筛查 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
	<div class="page_top_box" style="display:none;">
    	<input type="button" class="btn_back" style="display:none;"/>
        筛查
        <input type="button" class="btn_share" style="display:none;"/>
    </div>
    <div class="page_cont_box">
        <div class="results_box">
            <div @if($aResult['sex'] == 0) class="results_block woman" @else class="results_block" @endif>
                <div class="wid100">
                	@if($aResult['sex'] == 0)
                    <img class="wid100" src="/assets/images/aspirin/scr_woman_01.jpg"/>
                    @else
                    <img class="wid100" src="/assets/images/aspirin/scr_man_01.jpg"/>
                    @endif
                </div>
                <div class="results_cont">
                    <div class="results_line">
                        <div class="results_txt01">
                            年龄：
                        </div>
                        <div class="results_txt02">
                            {{$aResult['age']}}岁
                        </div>
                        <div class="results_txt01">
                            身高：
                        </div>
                        <div class="results_txt02">
                            {{$aResult['height']}}cm
                        </div>
                    </div>
                    <div class="results_line">
                        <div class="results_txt01">
                            体重：
                        </div>
                        <div class="results_txt02">
                            {{$aResult['weight']}}kg
                        </div>
                        <div class="results_txt01">
                            BMI：
                        </div>
                        <div class="results_txt02">
                            {{$aResult['bmi']}}
                        </div>
                    </div>
                </div>
                <div class="wid100">
                	@if($aResult['sex'] == 0)
                    <img class="wid100" src="/assets/images/aspirin/scr_woman_02.jpg"/>
                    @else
                    <img class="wid100" src="/assets/images/aspirin/scr_man_02.jpg"/>
                    @endif
                </div>
            </div>
            <div class="results_txt03">
                测试结果<span>（本结果仅供参考，最终以医生诊断为准）</span>
            </div>
            <div class="results_block2">
                <div class=" results_block2_left">
                    您目前的心血管风险
                </div>
                <div class=" results_block2_right c_red">
                    @if($aResult['flag'] == 2)
                    <img src="/assets/images/aspirin/scr_results03.png" />
                    <img src="/assets/images/aspirin/scr_results03.png" />
                    <img src="/assets/images/aspirin/scr_results04.png" />
                    @elseif($aResult['flag'] == 3)
                    <img src="/assets/images/aspirin/scr_results03.png" />
                    <img src="/assets/images/aspirin/scr_results03.png" />
                    <img src="/assets/images/aspirin/scr_results03.png" />
                	@else
                    <img src="/assets/images/aspirin/scr_results03.png"/>
                    <img src="/assets/images/aspirin/scr_results04.png"/>
                    <img src="/assets/images/aspirin/scr_results04.png"/>
                    @endif
                    <span>（{{$aResult['flag_text']}}）</span>
                </div>
            </div>
            @if($aResult['danger_element'])
            <div class="results_block3">
                <div>存在以下心血管危险因素</div>
                <div class=" clearfix">
                @foreach($aResult['danger_element'] as $v)
                    <div class="red">{{$v}}</div>
                @endforeach
                </div>
            </div>
            @endif
        </div>

        <div class="results_box">
            <div class="results_txt03">
                危害及防治建议
            </div>
            <div class="results_block4">
                {{$aResult['suggestion']}}
            </div>
        </div>
        <div class="results_share_txt03">
                <a href="/aspirinfund/screening-message/{{$aResult['id']}}">扫描（长按识别）下方二维码查看结果</a>
        </div>
        <div class="results_share_pic2">
        	<img class="wid100" src="{{$aResult['ewm_url']}}"/>
        </div>
    </div> 
@stop
