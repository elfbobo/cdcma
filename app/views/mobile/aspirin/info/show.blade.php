@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金@stop
@section('content')
@include('mobile.common.stopforward')
<style>
.cont img{max-width:100%;}
.wrap0 {
  width: 100%;
/*  height: 100%;*/
  display: flex;
  display: -webkit-flex;
  flex-direction: column;
  -webkit-flex-direction: column;
  overflow: hidden;
}
</style>
	<div class="wrap0">
		<div class="page_top_box">
	    	<input type="button" class="btn_back" onclick="window.location.href='/mobile-aspirin/info-list/{{$oInfo->group_id}}';"/>
	        <div class="page_top">{{mb_substr($sGroupTitle,0,10)}}</div>
	        <input type="button" class="btn_share" style="visibility:hidden;"/>
	    </div>
		<div class="wrap-style1">
	        <div class="cont-detail">
	            <div class="detail_title">
	                {{$sGroupTitle}}:{{$oInfo->info_title}}
	            </div>
	            @if(!empty($oInfo->info_subtitle))
	            <div class="detail_subtitle">
	                ——{{$oInfo->info_subtitle}}
	            </div>
	            @endif
	            @if(!empty($oInfo->info_video))
	            <div class="img"><video src="{{$oInfo->info_video}}" poster="{{$oInfo->info_video_thumb}}" controls style="width:100%"></video></div>
	            @endif
	            <div class="cont">
	            {{$oInfo->info_content}}
	            </div>
	        </div>
	    </div>
	</div>
@stop
