@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金@stop
@section('content')
@include('mobile.common.stopforward')
<style>
.wrap0 {
  width: 100%;
/*  height: 100%;*/
  display: flex;
  display: -webkit-flex;
  flex-direction: column;
  -webkit-flex-direction: column;
  overflow: hidden;
}
</style>
	<div class="wrap0">
		<div class="page_top_box">
	    	<input type="button" class="btn_back" onclick="window.location.href='/mobile-aspirin';"/>
	        <div class="page_top">{{mb_substr($sGroupTitle,0,10)}}</div>
	        <input type="button" class="btn_share" style="visibility:hidden;"/>
	    </div>
		<div class="wrap-style1">
	        <div class="cont-list">
	        @foreach($oInfo as $k=>$v)
	        <a href="/mobile-aspirin/info-show/{{$v->id}}">
	            <dl>
	                <dt><img src="{{$v->info_thumb}}" alt=""></dt>
	                <dd>
	                    <p class="title">{{$v->info_title}}</p>
	                    <p class="time">{{substr($v->created_at,0,10)}}</p>
	                </dd>
	            </dl>
	        </a>
	        @endforeach   
	        </div>
	    </div>
	</div>
@stop
