@extends('mobile.common.layoutasp2')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金 @stop
@section('content')
	<div class="page_top_box" style="display:none;">
    	<input type="button" class="btn_back" />
        	风险筛查
        <input type="button" class="btn_share" />
    </div>
    <div class="page_cont_box">
        <div class="results_share_box">
            <div class="results_share_txt01">
                <p>随着环境污染、生活及工作压力增加大</p>
                <p>我们离心血管疾病已经越来越近了</p>
            </div>
            <div class="results_share_pic1">
                <img class="wid100" src="/assets/images/mobile/fund/heart.png" />
            </div>
            <div class="results_share_txt02">
                <p>别担心，心血管疾病完成</p>
                <p>可以提前筛查、早期预防哦</p>
            </div>
            <div class="results_share_txt03">
                扫描（长按识别）下方二维码进行筛查
            </div>
            <div class="results_share_pic2">
                <img class="wid100" src="{{$sEwmUrl}}"/>
            </div>
        </div>
    </div>
    <div class="btm_pic">
        <img class="wid100" src="/assets/images/mobile/fund/btm_pic.png"/>
    </div>
     
@stop
