@extends('mobile.common.layoutasp2')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金 @stop
@section('keywords')阿司匹林专项基金 @stop
@section('content')
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
@if($isWxFlag)
wx.config({
	debug:false,
	appId: "{{$wx_config['appId']}}",//必填
	timestamp:{{$wx_config['timestamp']}}, // 必填，生成签名的时间戳
	nonceStr: "{{$wx_config['nonceStr']}}", // 必填，生成签名的随机串
	signature: "{{$wx_config['signature']}}",// 必填，签名
	jsApiList: ['onMenuShareTimeline','onMenuShareAppMessage']// 必填，需要使用的JS接口列表，所有JS接口列表见附录2
});
wx.ready(function(){
	wx.onMenuShareTimeline({//分享到朋友圈
	    title:'专项基金风险筛查表',
	    link: '{{Config::get('app.url')}}/aspirinshare/screening-ewm?iUid={{$iUid}}',
	    imgUrl: '{{Config::get('app.url')}}/assets/images/mobile/logo.png',
	    success: function () {  },
	    cancel: function () {  }
	});
	wx.onMenuShareAppMessage({//分享给朋友
	    title: '专项基金风险筛查表',
	    desc: '专项基金风险筛查表',
	    link: '{{Config::get('app.url')}}/aspirinshare/screening-ewm?iUid={{$iUid}}',
	    imgUrl: '{{Config::get('app.url')}}/assets/images/mobile/logo.png',
	    type: '',
	    dataUrl: '',
	    success: function (){ },
	    cancel: function (){ }
	});
});
wx.error(function(res){ });
@endif
</script>
	<div class="page_top_box" style="width:auto;display:none;">
    	<input type="button" class="btn_back" style="visibility:hidden;"/>
         	风险筛查
        <input type="button" class="btn_share" style="visibility:hidden;"/>
    </div>
    <input type="hidden" id="iId" name="iId" value="{{$iId}}">
    <div class="page_cont_box">
        <div class="select_block">
            <div class="select_tit">
                您是否有已诊断的高血脂
            </div>
            <div class="select clearfix">
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio11" name="question4" value="2" onclick="secondInfo(2);">
                    <label class="sele_radio_lable" for="radio11">是</label>
                </div>
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio12" name="question4" value="1" onclick="secondInfo(1);">
                    <label class="sele_radio_lable" for="radio12">否</label>
                </div>
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio13" name="question4" value="3" onclick="secondInfo(3);">
                    <label class="sele_radio_lable" for="radio13">不清楚</label>
                </div>
                <div class="clear"></div>
            </div>
            <div class="select_block" id="second_box" style="display:none;">
                <div class="select_tit">
                    您是否在服用降脂药
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio14" name="question41" value="2">
                        <label class="sele_radio_lable" for="radio14">是</label>
                    </div>
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio15" name="question41" value="1">
                        <label class="sele_radio_lable" for="radio15">否</label>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="select_block border_top" id="third_box" style="display:none;">
                <div class="select_tit">
                    治疗前总胆固醇水平
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio16" name="question42" value="1">
                        <label class="sele_radio_lable" for="radio16"><4.14mmol/L(<160mg/dl)</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio17" name="question42" value="2">
                        <label class="sele_radio_lable" for="radio17">4.15-5.17mmol/L(<160-199mg/dl)</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio18" name="question42" value="3">
                        <label class="sele_radio_lable" for="radio18">5.18-6.21mmol/L(<200-239mg/dl)</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio19" name="question42" value="4">
                        <label class="sele_radio_lable" for="radio19">6.22-7.24mmol/L(<240-279mg/dl)</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio20" name="question42" value="5">
                        <label class="sele_radio_lable" for="radio20">≥7.25mmol/L(<≥280mg/dl)</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio21" name="question42" value="6">
                        <label class="sele_radio_lable" for="radio21">不清楚</label>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="select_block">
            <div class="select_tit">
                您是否有已诊断的高血压
            </div>
            <div class="select clearfix">
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio51" name="question5" value="2" onclick="thirdInfo(2);">
                    <label class="sele_radio_lable" for="radio51">是</label>
                </div>
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio52" name="question5" value="1" onclick="thirdInfo(1);">
                    <label class="sele_radio_lable" for="radio52">否</label>
                </div>
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio53" name="question5" value="3" onclick="thirdInfo(3);">
                    <label class="sele_radio_lable" for="radio53">不清楚</label>
                </div>
                <div class="clear"></div>
            </div>
            <div class="select_block" id="second_box2" style="display:none;">
                <div class="select_tit">
                    您是否在服用降压药
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio54" name="question51" value="2">
                        <label class="sele_radio_lable" for="radio54">是</label>
                    </div>
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio55" name="question51" value="1">
                        <label class="sele_radio_lable" for="radio55">否</label>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="select_block border_top" id="third_box2" style="display:none;">
                <div class="select_txt">
                    <div class="select_left">
                        治疗前收缩压（高压）：
                    </div>
                    <div class="select_right">
                        <input class="input_txt" type="text" placeholder="请输入" name="question52" id="question52"/>
                    </div>
                </div>
                <div>
                    mmHg
                </div>
            </div>
            <div class="select_block border_top" id="fourth_box2" style="display:none;">
                <div class="select_tit">
                    治疗前舒张压（低压）
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio56" name="question53" value="1">
                        <label class="sele_radio_lable" for="radio56">小于80mmHg</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio57" name="question53" value="2">
                        <label class="sele_radio_lable" for="radio57">80-84mmHg</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio58" name="question53" value="3">
                        <label class="sele_radio_lable" for="radio58">85-89mmHg</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio59" name="question53" value="4">
                        <label class="sele_radio_lable" for="radio59">90-99mmHg</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio60" name="question53" value="5">
                        <label class="sele_radio_lable" for="radio60">大于等于100mmHg</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio61" name="question53" value="6">
                        <label class="sele_radio_lable" for="radio61">不清楚</label>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="select_block border_top" id="fifth_box2" style="display:none;">
                <div class="select_tit">
                    治疗后收缩压（高压）
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio70" name="question54" value="1">
                        <label class="sele_radio_lable" for="radio70">小于等于140mmHg</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio71" name="question54" value="2">
                        <label class="sele_radio_lable" for="radio71">大于等于140mmHg</label>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>

    </div>
    <div class="page_btn_box">
    @if($iCardAuthFlag == 2)
        <input class="send_btn" type="button" value="继续" onclick="check();"/>
    @else
    	 <input class="send_btn" type="button" value="继续" onclick="alert('您还没有通过医师认证')"/>
    @endif
    </div>
      <script type="text/javascript">
	 function secondInfo(flag){
	     if(flag == 2){
	   	    $("#second_box").show();
	   		$("#third_box").show();
	     }else if(flag == 1){
	   	    $("#second_box").hide();
	   	 	$("#third_box").hide();
	     }else if(flag == 3){
	     	$("#second_box").show();
	    	$("#third_box").hide();
		 }
	 }
	 function thirdInfo(flag){
	     if(flag == 2){
	   	    $("#second_box2").show();
	   		$("#third_box2").show();
	   		$("#fourth_box2").show();
	   		$("#fifth_box2").show();
	     }else if(flag == 1){
	     	$("#second_box2").hide();
		   	$("#third_box2").hide();
		   	$("#fourth_box2").hide();
		   	$("#fifth_box2").hide();
	     }else if(flag == 3){
	    	$("#second_box2").show();
	    	$("#third_box2").hide();
		   	$("#fourth_box2").hide();
		   	$("#fifth_box2").hide();
		 }
	 }
	</script>
    <script type="text/javascript">
	function check(){
		var iId = $("#iId").val();
		var question4 = $(':radio[name="question4"]:checked').val();
		var question41 = $(':radio[name="question41"]:checked').val();
		var question42 = $(':radio[name="question42"]:checked').val();
		var question5 = $(':radio[name="question5"]:checked').val();
		var question51 = $(':radio[name="question51"]:checked').val();
		var question52 = $.trim($("#question52").val());
		var question53 = $(':radio[name="question53"]:checked').val();
		var question54 = $(':radio[name="question54"]:checked').val();
		if(!question4){
			alert("请选择是否有已诊断的高血脂");
			return false;
		}
		if(question4 != 1 && !question41){
			alert("请选择是否在用降脂药");
			return false;
		}
		if(question4 == 2 && !question42){
			alert("请选择治疗前总胆固醇水平");
			return false;
		}
		if(!question5){
			alert("请选择是否有已诊断的高血压");
			return false;
		}
		if(question5 != 1 && !question51){
			alert("请选择是否在用降压药");
			return false;
		}
		if(question5 == 2 && !question52){
			alert("请填写治疗前收缩压（高压）");
			return false;
		}
		if(question5 == 2 && !question53){
			alert("请选择治疗前舒张压（低压）");
			return false;
		}
		if(question5 == 2 && !question54){
			alert("请选择治疗后收缩压（高压）");
			return false;
		}
		var url = '/aspirinfund/screening-submit3';
		var data = {'iId':iId,
					'question4':question4,
					'question41':question41,
					'question42':question42,
					'question5':question5,
					'question51':question51,
					'question52':question52,
					'question53':question53,
					'question54':question54
		};
		$.post(url,data,function(msg){
			if(msg.success){
				window.location.href= '/aspirinfund/screening-form4?id='+iId;
			}else{
				alert('请重新提交');
				return;
			}
		  },'json')
	}
	</script> 

@stop
