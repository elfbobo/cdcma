@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金  @stop
@section('description')阿司匹林专项基金 @stop
@section('keywords')阿司匹林专项基金 @stop
@section('content')
@include('mobile.common.stopforward')
<div class="page_top_box">
    	<div><input type="button" class="btn_back" onclick="window.location.href='/mobile-aspirin';"/></div>
    	<div style="width:70%;">风险筛查</div>  
        <div><input type="button" class="btn_share" style="visibility:hidden;"/></div>
</div>
<div class="wrap-index wrap-style3 wrap-style4">
        <div class="index-cont">
            <div class="online_meeting">
                <div class="index-module">
                    <a href="/aspirinfund/screening-form" class="single green">
                        <dl>
                            <dt><img src="/assets/images/mobile/fund/risk_screening-icon1.png" alt=""></dt>
                            <dd>筛选工具</dd>
                        </dl>
                    </a>
                </div>
                <div class="index-module">
                    <a href="javascript::void(0)" class="single red">
                        <dl>
                            <dt><img src="/assets/images/mobile/fund/risk_screening-icon2.png" alt=""></dt>
                            <dd>筛选记录</dd>
                        </dl>
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop
