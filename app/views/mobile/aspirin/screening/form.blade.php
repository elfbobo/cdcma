@extends('mobile.common.layoutasp2')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金 @stop
@section('keywords')阿司匹林专项基金 @stop
@section('content')
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
@if($isWxFlag)
wx.config({
	debug:false,
	appId: "{{$wx_config['appId']}}",//必填
	timestamp:{{$wx_config['timestamp']}}, // 必填，生成签名的时间戳
	nonceStr: "{{$wx_config['nonceStr']}}", // 必填，生成签名的随机串
	signature: "{{$wx_config['signature']}}",// 必填，签名
	jsApiList: ['onMenuShareTimeline','onMenuShareAppMessage']// 必填，需要使用的JS接口列表，所有JS接口列表见附录2
});
wx.ready(function(){
	wx.onMenuShareTimeline({//分享到朋友圈
	    title:'专项基金风险筛查表',
	    link: '{{Config::get('app.url')}}/aspirinshare/screening-ewm?iUid={{$iUid}}',
	    imgUrl: '{{Config::get('app.url')}}/assets/images/mobile/logo.png',
	    success: function () {  },
	    cancel: function () {  }
	});
	wx.onMenuShareAppMessage({//分享给朋友
	    title: '专项基金风险筛查表',
	    desc: '专项基金风险筛查表',
	    link: '{{Config::get('app.url')}}/aspirinshare/screening-ewm?iUid={{$iUid}}',
	    imgUrl: '{{Config::get('app.url')}}/assets/images/mobile/logo.png',
	    type: '',
	    dataUrl: '',
	    success: function (){ },
	    cancel: function (){ }
	});
});
wx.error(function(res){ });
@endif
</script>
	<div class="page_top_box" style="width:auto;">
    	<a href="/mobile-aspirin"><input type="button" class="btn_back"/></a>
         	风险筛查
        <input type="button" class="btn_share" style="visibility:hidden;"/>
    </div>
    <div class="page_cont_box">
        <div class="screening2_box">
            <div class="screening2_block sex">
                <div class="screening2_left">
                    性别
                </div>
                <div class="screening2_right sex_txt">
                    请选择
                </div>
            </div>
            <div class="screening2_block age">
                <div class="screening2_left">
                    年龄
                </div>
                <div class="screening2_right age_txt">
                    请选择
                </div>
            </div>
            <div class="screening2_block weight">
                <div class="screening2_left">
                    体重
                </div>
                <div class="screening2_right weight_txt">
                    请选择体重
                </div>
                <div class="screening2_label">
                    千克/公斤
                </div>
            </div>
            <div class="screening2_block height">
                <div class="screening2_left">
                    身高
                </div>
                <div class="screening2_right height_txt">
                    请选择身高
                </div>
                <div class="screening2_label">
                    厘米
                </div>
            </div>
        </div>
    </div>
    <div class="page_btn_box">
    @if($iCardAuthFlag == 2)
        <input class="send_btn" type="button" onclick="check();" value="继续"/>
    @else
    	<input class="send_btn" type="button" onclick="alert('您还没有通过医师认证')" value="继续"/>
    @endif
    </div>


    <!--选择性别-->
    <div class="select_box pop_sex_box">
        <div class="select_top">
            <div class="cancel sex_cancel">取消</div>
            <div class="sele_txt age_tit">性别</div>
            <div class="submit sex_submit">完成</div>
        </div>
        <div class="select_cont">
        </div>
    </div>


    <!--选择年龄-->
    <div class="select_box pop_age_box">
        <div class="select_top">
            <div class="cancel age_cancel">取消</div>
            <div class="sele_txt">年龄</div>
            <div class="submit age_submit">完成</div>
        </div>
          <div class="flex-between">
            <div class="select_cont year">
            </div>
            <div class="select_cont month">
            </div>
        </div>
    </div>

    <!--选择体重-->
    <div class="select_box pop_weight_box">
        <div class="select_top">
            <div class="cancel weight_cancel">取消</div>
            <div class="sele_txt">体重</div>
            <div class="submit weight_submit">完成</div>
        </div>
        <div class="select_cont">
        </div>
    </div>

    <!--选择身高-->
    <div class="select_box pop_height_box">
        <div class="select_top">
            <div class="cancel height_cancel">取消</div>
            <div class="sele_txt">身高</div>
            <div class="submit height_submit">完成</div>
        </div>
        <div class="select_cont">
        </div>
    </div>

<script type="text/javascript">
	function check(){
		var uid = $("#iUid").val();
		var sex = $.trim($('.sex .sex_txt').html());
		var age = $.trim($('.age .age_txt').html());
		var weight = $.trim($('.weight .weight_txt').html());
		var height = $.trim($('.height .height_txt').html());
	
		if(sex == "请选择"){
			alert("请选择性别");
			return false;
		}
		if(age == "请选择"){
			alert("请选择年龄");
			return false;
		}
		if(weight == "请选择体重"){
			alert("请选择体重");
			return false;
		}
		if(height == "请选择身高"){
			alert("请选择身高");
			return false;
		}
		var url = '/aspirinfund/screening-submit';
		var data = {'uid':uid,'sex':sex,'age':age,'weight':weight,'height':height};
		$.post(url,data,function(msg){
			if(msg.success){
				var sourceid = msg.id;
				window.location.href= '/aspirinfund/screening-form2?id='+sourceid;
			}else{
				alert('请重新提交');
				return;
			}
		  },'json')
	}
</script> 



<script type="text/javascript">
    $(document).ready(function () {

        //选择性别
         $('.sex').click(function () {
			 
			 
             var bottom = $('.pop_sex_box').css("bottom");

             if( bottom == "0px"){
                 $('.pop_sex_box').animate({bottom:"-100%"});
                 $('.pop_sex_box .select_cont').empty();
                 return;
             }

			 $('.pop_age_box').animate({bottom:"-100%"});
			 $('.pop_age_box .select_cont').empty();
			 $('.pop_weight_box').animate({bottom:"-100%"});
			 $('.pop_weight_box .select_cont').empty();
			 $('.pop_height_box').animate({bottom:"-100%"});			 
			 $('.pop_height_box .select_cont').empty();
             $('.pop_sex_box').animate({bottom:"0"});
             var sexTxt = ["男","女"];
             for ( var i =0; i< sexTxt.length; i++ ) {
                console.log(sexTxt[i]);
                $('.pop_sex_box .select_cont').append("<div class='select_line'><a class='sele_item' href='javascript:void(0);'>" + sexTxt[i] + "</a></div>");
             }

             $('.pop_sex_box .sele_item').click(function () {
                 $('.pop_sex_box .sele_item').removeClass("sele_bg");
                 $(this).addClass("sele_bg");
             });

             $('.sex_submit').click(function () {
                 $('.pop_sex_box').animate({bottom:"-100%"});
                 if($('.pop_sex_box .sele_item').hasClass("sele_bg")){
                     $('.sex .sex_txt').html($('.pop_sex_box .sele_bg').html());
                 }
                 $('.pop_sex_box .select_cont').empty();
             });

             $('.sex_cancel').click(function () {
                 $('.pop_sex_box .sele_item').removeClass("sele_bg");
                 $('.pop_sex_box').animate({bottom:"-100%"});
                 $('.pop_sex_box .select_cont').empty();
             });
         });


       //选择年龄
        $('.age').click(function () {
            var bottom = $('.pop_age_box').css("bottom");
            if( bottom == "0px"){
                $('.pop_age_box').animate({bottom:"-100%"});
                $('.pop_age_box .select_cont').empty();
                return;
            }
			$('.pop_age_box').animate({bottom:"0"});
			$('.pop_weight_box').animate({bottom:"-100%"});
			$('.pop_weight_box .select_cont').empty();
			$('.pop_height_box').animate({bottom:"-100%"});			 
			$('.pop_height_box .select_cont').empty();
            $('.pop_sex_box').animate({bottom:"-100%"});
			$('.pop_sex_box .select_cont').empty();
            var yearBefor = 1950;
            var yearEnd = 2050;
            var string;
            for ( var i = yearBefor; i<= yearEnd; i++ ) {
                    $('.pop_age_box .select_cont.year').append("<div class='select_line'><a class='sele_item year' href='javascript:void(0);'>" + i+"年" + "</a></div>");
            };
            for(var j = 1; j<= 12; j++) {
                $('.pop_age_box .select_cont.month').append("<div class='select_line'><a class='sele_item month' href='javascript:void(0);'>" + j+"月" + "</a></div>");
            }
            $('.pop_age_box .sele_item.year').click(function () {
                $('.sele_item.year').removeClass("sele_bg");
                $(this).addClass("sele_bg");
            });
            $('.pop_age_box .sele_item.month').click(function () {
                $('.sele_item.month').removeClass("sele_bg");
                $(this).addClass("sele_bg");
            });

            $('.age_submit').click(function () {
                $('.pop_age_box').animate({bottom:"-100%"});
                if($('.pop_age_box .year .sele_item').hasClass("sele_bg")){
                    if($('.pop_age_box .month .sele_item').hasClass("sele_bg")){
                        $('.age .age_txt').html($('.pop_age_box .year .sele_bg').html()+$('.pop_age_box .month .sele_bg').html());
                    }
                }
                $('.pop_age_box .select_cont').empty();
            });
            $('.age_cancel').click(function () {
                $('.pop_age_box .sele_item').removeClass("sele_bg");
                $('.pop_age_box').animate({bottom:"-100%"});
                $('.pop_age_box .select_cont').empty();
            });
        });



        //选择体重
        $('.weight').click(function () {
            var bottom = $('.pop_weight_box').css("bottom");
            if( bottom == "0px"){
                $('.pop_weight_box').animate({bottom:"-100%"});
                $('.pop_weight_box .select_cont').empty();
                return;
            }
			
			$('.pop_age_box').animate({bottom:"-100%"});
			$('.pop_age_box .select_cont').empty();
			$('.pop_weight_box').animate({bottom:"0"});
			$('.pop_height_box').animate({bottom:"-100%"});			 
			$('.pop_height_box .select_cont').empty();
            $('.pop_sex_box').animate({bottom:"-100%"});
			$('.pop_sex_box .select_cont').empty();
			
            var startW = 30;
            var endW = 300;
            var string;
            for ( var i = startW; i<= endW; i++ ) {
                $('.pop_weight_box .select_cont').append("<div class='select_line'><a class='sele_item' href='javascript:void(0);'>" + i+"公斤" + "</a></div>");
            }
            $('.pop_weight_box .sele_item').click(function () {
                $('.sele_item').removeClass("sele_bg");
                $(this).addClass("sele_bg");
            });

            $('.weight_submit').click(function () {
                $('.pop_weight_box').animate({bottom:"-100%"});
                if($('.pop_weight_box .sele_item').hasClass("sele_bg")){
                    var w = $('.pop_weight_box .sele_bg').html().replace(/[^0-9]/ig,"");
                    $('.weight .weight_txt').html(w);
                }
                $('.pop_weight_box .select_cont').empty();
            });
            $('.weight_cancel').click(function () {
                $('.pop_weight_box .sele_item').removeClass("sele_bg");
                $('.pop_weight_box').animate({bottom:"-100%"});
                $('.pop_weight_box .select_cont').empty();
            });
        });

        //选择身高
        $('.height').click(function () {
            var bottom = $('.pop_height_box').css("bottom");
			
            if( bottom == "0px"){
                $('.pop_height_box').animate({bottom:"-100%"});
                $('.pop_height_box .select_cont').empty();
                return;
            }
			$('.pop_age_box').animate({bottom:"-100%"});
			$('.pop_age_box .select_cont').empty();
			$('.pop_weight_box').animate({bottom:"-100%"});
			$('.pop_weight_box .select_cont').empty();
			$('.pop_height_box').animate({bottom:"0"});			 
            $('.pop_sex_box').animate({bottom:"-100%"});
			$('.pop_sex_box .select_cont').empty();
			
            var startH = 100;
            var endH = 300;
            var string;
            for ( var i = startH; i<= endH; i++ ) {
                $('.pop_height_box .select_cont').append("<div class='select_line'><a class='sele_item' href='javascript:void(0);'>" + i+"cm" + "</a></div>");
            }
            $('.pop_height_box .sele_item').click(function () {
                $('.sele_item').removeClass("sele_bg");
                $(this).addClass("sele_bg");
            });

            $('.height_submit').click(function () {
                $('.pop_height_box').animate({bottom:"-100%"});
                if($('.pop_height_box .sele_item').hasClass("sele_bg")){
                    var w = $('.pop_height_box .sele_bg').html().replace(/[^0-9]/ig,"");
                    $('.height .height_txt').html(w);
                }
                $('.pop_height_box .select_cont').empty();
            });
            $('.height_cancel').click(function () {
                $('.pop_height_box .sele_item').removeClass("sele_bg");
                $('.pop_height_box').animate({bottom:"-100%"});
                $('.pop_height_box .select_cont').empty();
            });
        });
    })
</script>
@stop
