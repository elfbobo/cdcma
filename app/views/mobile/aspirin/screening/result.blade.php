@extends('mobile.common.layoutasp2')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金 @stop
@section('keywords')阿司匹林专项基金 @stop
@section('content')
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
@if($isWxFlag)
wx.config({
	debug:false,
	appId: "{{$wx_config['appId']}}",//必填
	timestamp:{{$wx_config['timestamp']}}, // 必填，生成签名的时间戳
	nonceStr: "{{$wx_config['nonceStr']}}", // 必填，生成签名的随机串
	signature: "{{$wx_config['signature']}}",// 必填，签名
	jsApiList: ['onMenuShareTimeline','onMenuShareAppMessage']// 必填，需要使用的JS接口列表，所有JS接口列表见附录2
});
wx.ready(function(){
	wx.onMenuShareTimeline({//分享到朋友圈
	    title:'专项基金风险筛查',
	    link: '{{Config::get('app.url')}}/aspirinshare/screening-result?id={{$iId}}',
	    imgUrl: '{{Config::get('app.url')}}/assets/images/mobile/logo.png',
	    success: function () {  },
	    cancel: function () {  }
	});
	wx.onMenuShareAppMessage({//分享给朋友
	    title: '专项基金风险筛查',
	    desc: '专项基金风险筛查',
	    link: '{{Config::get('app.url')}}/aspirinshare/screening-result?id={{$iId}}',
	    imgUrl: '{{Config::get('app.url')}}/assets/images/mobile/logo.png',
	    type: '',
	    dataUrl: '',
	    success: function (){ },
	    cancel: function (){ }
	});
});
wx.error(function(res){ });
@endif
</script>
	<div class="page_top_box" style="width:auto;">
    	<input type="button" class="btn_back" onclick="window.location.href='/mobile-aspirin';"/>
         	风险筛查
        <input type="button" class="btn_share" style="visibility:hidden;"/>
    </div>
    <div class="page_cont_box">
        <div class="results_box">
            <div   @if($aResult['sex'] == 0) class="results_block woman" @else class="results_block" @endif>
                <div class="wid100">
                	@if($aResult['sex'] == 0)
                	<img class="wid100" src="/assets/images/mobile/fund/scr_woman_01.jpg"/>
                    @else
					<img class="wid100" src="/assets/images/mobile/fund/scr_man_01.jpg"/>
                    @endif
                </div>
                <div class="results_cont">
                    <div class="results_line">
                        <div class="results_txt01">
                            年龄：
                        </div>
                        <div class="results_txt02">
                            {{$aResult['age']}}岁
                        </div>
                        <div class="results_txt01">
                            身高：
                        </div>
                        <div class="results_txt02">
                            {{$aResult['height']}}cm
                        </div>
                    </div>
                    <div class="results_line">
                        <div class="results_txt01">
                            体重：
                        </div>
                        <div class="results_txt02">
                            {{$aResult['weight']}}kg
                        </div>
                        <div class="results_txt01">
                            BMI：
                        </div>
                        <div class="results_txt02">
                            {{$aResult['bmi']}}
                        </div>
                    </div>
                </div>
                <div class="wid100">
                	@if($aResult['sex'] == 0)
                	<img class="wid100" src="/assets/images/aspirin/scr_woman_02.jpg"/>
                    @else
                    <img class="wid100" src="/assets/images/mobile/fund/scr_man_02.jpg"/>
                    @endif
                </div>
            </div>
            <div class="results_txt03">
                测试结果<span>（本结果仅供参考，最终以医生诊断为准）</span>
            </div>
            <div class="results_block2">
                <div class=" results_block2_left">
                    您目前的心血管风险
                </div>
                <div @if($aResult['sex'] == 0) class="results_block2_right c_red" @else class="results_block2_right"  @endif>
                @if($aResult['sex'] == 0)
                	 @if($aResult['flag'] == 2)
                    <img src="/assets/images/aspirin/scr_results03.png" />
                    <img src="/assets/images/aspirin/scr_results03.png" />
                    <img src="/assets/images/aspirin/scr_results04.png" />
                    @elseif($aResult['flag'] == 3)
                    <img src="/assets/images/aspirin/scr_results03.png" />
                    <img src="/assets/images/aspirin/scr_results03.png" />
                    <img src="/assets/images/aspirin/scr_results03.png" />
                	@else
                    <img src="/assets/images/aspirin/scr_results03.png"/>
                    <img src="/assets/images/aspirin/scr_results04.png"/>
                    <img src="/assets/images/aspirin/scr_results04.png"/>
                    @endif
                    <span>（{{$aResult['flag_text']}}）</span>
                @else
                	 @if($aResult['flag'] == 2)
                    <img src="/assets/images/aspirin/scr_man_03.png" />
                    <img src="/assets/images/aspirin/scr_man_03.png" />
                    <img src="/assets/images/aspirin/scr_man_04.png" />
                    @elseif($aResult['flag'] == 3)
                    <img src="/assets/images/aspirin/scr_man_03.png" />
                    <img src="/assets/images/aspirin/scr_man_03.png" />
                    <img src="/assets/images/aspirin/scr_man_03.png" />
                	@else
                    <img src="/assets/images/aspirin/scr_man_03.png"/>
                    <img src="/assets/images/aspirin/scr_man_04.png"/>
                    <img src="/assets/images/aspirin/scr_man_04.png"/>
                    @endif
                    <span>（{{$aResult['flag_text']}}）</span>
                @endif
                </div>
            </div>
            @if($aResult['danger_element'])
            <div class="results_block3">
                <div>存在以下心血管危险因素</div>
                <div class=" clearfix">
                  @foreach($aResult['danger_element'] as $v)
                    <div class="red">{{$v}}</div>
                  @endforeach
                </div>
            </div>
            @endif
        </div>

        <div class="results_box">
            <div class="results_txt03">
                危害及防治建议
            </div>
            <div class="results_block4">
                {{$aResult['suggestion']}}
            </div>
        </div>
        
        
    </div>    
    
@stop
