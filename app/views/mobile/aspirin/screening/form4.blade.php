@extends('mobile.common.layoutasp2')
@section('title')阿司匹林专项基金@stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金 @stop
@section('content')
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
@if($isWxFlag)
wx.config({
	debug:false,
	appId: "{{$wx_config['appId']}}",//必填
	timestamp:{{$wx_config['timestamp']}}, // 必填，生成签名的时间戳
	nonceStr: "{{$wx_config['nonceStr']}}", // 必填，生成签名的随机串
	signature: "{{$wx_config['signature']}}",// 必填，签名
	jsApiList: ['onMenuShareTimeline','onMenuShareAppMessage']// 必填，需要使用的JS接口列表，所有JS接口列表见附录2
});
wx.ready(function(){
	wx.onMenuShareTimeline({//分享到朋友圈
	    title:'专项基金风险筛查表',
	    link: '{{Config::get('app.url')}}/aspirinshare/screening-ewm?iUid={{$iUid}}',
	    imgUrl: '{{Config::get('app.url')}}/assets/images/mobile/logo.png',
	    success: function () {  },
	    cancel: function () {  }
	});
	wx.onMenuShareAppMessage({//分享给朋友
	    title: '专项基金风险筛查表',
	    desc: '专项基金风险筛查表',
	    link: '{{Config::get('app.url')}}/aspirinshare/screening-ewm?iUid={{$iUid}}',
	    imgUrl: '{{Config::get('app.url')}}/assets/images/mobile/logo.png',
	    type: '',
	    dataUrl: '',
	    success: function (){ },
	    cancel: function (){ }
	});
});
wx.error(function(res){ });
@endif
</script>
	<div class="page_top_box" style="width:auto;display:none;">
    	<input type="button" class="btn_back" style="visibility:hidden;"/>
         	风险筛查
        <input type="button" class="btn_share" style="visibility:hidden;"/>
    </div>
    <input type="hidden" id="iId" name="iId" value="{{$iId}}">
    <div class="page_cont_box">
        <div class="select_block">
            <div class="select_tit">
                您是否有已诊断的冠心病或心肌梗死病史
            </div>
            <div class="select clearfix">
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio01" name="question6" value="2">
                    <label class="sele_radio_lable" for="radio01">是</label>
                </div>
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio02" name="question6" value="1">
                    <label class="sele_radio_lable" for="radio02">否</label>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="select_block">
            <div class="select_tit">
                您是否有已诊断的脑卒中病史
            </div>
            <div class="select clearfix">
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio03" name="question7" value="2">
                    <label class="sele_radio_lable" for="radio03">是</label>
                </div>
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio04" name="question7" value="1">
                    <label class="sele_radio_lable" for="radio04">否</label>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="select_block">
            <div class="select_tit">
                您现在是否患有胃肠道出血/溃疡等疾病
            </div>
            <div class="select clearfix">
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio07" name="question8" value="2">
                    <label class="sele_radio_lable" for="radio07">是</label>
                </div>
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio08" name="question8" value="1">
                    <label class="sele_radio_lable" for="radio08">否</label>
                </div>
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio09" name="question8" value="3">
                    <label class="sele_radio_lable" for="radio09">不清楚</label>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="select_block">
            <div class="select_tit">
                您是否正在服用抗血小板药物
            </div>
            <div class="select clearfix">
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio05" name="question9" value="2" onclick="secondInfo(2);">
                    <label class="sele_radio_lable" for="radio05">是</label>
                </div>
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio06" name="question9" value="1" onclick="secondInfo(1);">
                    <label class="sele_radio_lable" for="radio06">否</label>
                </div>
                <div class="clear"></div>
            </div>
            <div class="select_block" id="second_box" style="display:none;">
                <div class="select_tit">
                    抗血小板药物名称（<span class="red">可多选</span>）
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_check" type="checkbox" id="check01" name="question91" value="1">
                        <label class="sele_check_lable" for="check01">阿司匹林</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_check" type="checkbox" id="check02" name="question91" value="2">
                        <label class="sele_check_lable" for="check02">氯吡格雷</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_check" type="checkbox" id="check03" name="question91" value="3">
                        <label class="sele_check_lable" for="check03">其它</label>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>


    </div>
    <div class="page_btn_box">
    @if($iCardAuthFlag == 2)
        <input class="send_btn" type="button" value="生成报告" onclick="check();"/>
    @else
    	<input class="send_btn" type="button" value="生成报告" onclick="alert('您还没有通过医师认证')"/>
    @endif
    </div>
  <script type="text/javascript">
	 function secondInfo(flag){
	     if(flag == 2){
	   	  $("#second_box").show();
	     }else if(flag == 1){
	   	  $("#second_box").hide();
	     }
	 }
	</script>
    <script type="text/javascript">
	function check(){
		var iId = $("#iId").val();
		var question6 = $(':radio[name="question6"]:checked').val();
		var question7 = $(':radio[name="question7"]:checked').val();
		var question8 = $(':radio[name="question8"]:checked').val();
		var question9 = $(':radio[name="question9"]:checked').val();
		var question91 = '';
		$("input:checkbox[name='question91']:checked").each(function() {
			question91 += $(this).val()+',';
		});
		
		if(!question6){
			alert("请选择是否有已诊断的冠心病或心肌梗死病史");
			return false;
		}
		if(!question7){
			alert("请选择是否有已诊断的脑卒中病史");
			return false;
		}
		if(!question8){
			alert("请选择是否患有胃肠道出血/溃疡等疾病");
			return false;
		}
		if(!question9){
			alert("请选择是否正在服用抗血小板药物");
			return false;
		}
		if(question9 == 2 && !question91){
			alert("请选择抗血小板药物名称");
			return false;
		}
		
		var url = '/aspirinfund/screening-submit4';
		var data = {'iId':iId,
					'question6':question6,
					'question7':question7,
					'question8':question8,
					'question9':question9,
					'question91':question91
		};
		$.post(url,data,function(msg){
			if(msg.success){
				window.location.href= '/aspirinfund/screening-result?id='+iId;
			}else{
				alert('请重新提交');
				return;
			}
		  },'json')
	}
	</script> 
@stop
