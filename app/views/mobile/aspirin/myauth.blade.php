@extends('mobile.common.layoutasp')
@section('title')我的认证 @stop
@section('description')我的认证@stop
@section('keywords')我的认证 @stop
@section('content')
@include('mobile.common.stopforward')
	<script src="http://libs.baidu.com/jquery/1.7.2/jquery.js"></script>
	<script src="/assets/js/ajaxfileupload.js"></script>
    <div class="page_top_box">
        <input type="button" class="btn_back" onclick="WeixinJSBridge.call('closeWindow');" style="visibility:hidden;"/>
        <div class="page_top">我的认证</div>
        <input type="button" value="关闭" onclick="window.location.href='/mobile-aspirin/index/1';" style="width:60px;height:45px;display:block;cursor:pointer;font-size:18px;color:#fff;"/>
    </div>
    <div class="wrap-style1">
        <div class="wrap_box">
            <div class="input_box">
                <label for="">姓名：</label>
                <input type="text" class="input_text" value="{{$oUser->user_name}}" disabled='disabled'>
            </div>
            <div class="input_box">
                <label for="">医院：</label>
                <input type="text" class="input_text"  value="{{$sHospitalName}}" disabled='disabled'>
            </div>
            <div class="input_box">
                <label for="">科室：</label>
                <input type="text" class="input_text"  value="{{$oUser->user_department}}" disabled='disabled'>
            </div>
           <?php 
                        	$aPos = User::getPosition();
                        ?>  
            <div class="input_box">
                <label for="">职称：</label>
                <select name="" id="user_position" class="input_select" >
                    @foreach($aPos as $k=>$v)
					        <option value="{{$v}}" @if($oUser->user_position==$v) selected @endif>{{$v}}</option>
					@endforeach
                </select>
            </div>
            <div class="input_box">
                <label for="">医师证号：</label>
                <input type="text" class="input_text" id="card_number" value="{{$oUser->card_number}}">
            </div>
            <div class="File_box">
                <label for="">上传证件照片：</label><span style="color:red;font-size:1.2rem;">请上传您的医师执业证书照片</span>
                <div class="flie_box">
                    <dl >
                        <dt><img src="{{$oUser->card_thumb}}" id ="user_thumb" alt=""></dt>
                        <dd>&nbsp;</dd>
                    </dl>
                    @if($oUser->card_auth_flag != 2)
                    <label for="file-pic" class="file-pic"></label>
                    <input type="file" class="input_file" id="file-pic" name="file-pic"  onchange="saveThumb()">
                    @endif
                    <br/>
                </div>
            </div>
            <div class="btn_box" @if($oUser->card_auth_flag == 2) style="display:none;" @endif>
                <button class="btn" onclick="authuser()">立即认证</button>
            </div>
        </div>
    </div>
    <div class="shade" style="display:none ;" id="shade"></div>
    <div class="module-window" style="display:none ;" id ="module_window">
        <div class="txt">
            <p>您已提交成功</p>
            <p>我们将在3个工作日内进行审核</p>
            <p>请您耐心等待审核</p>
        </div>
        <div class="btn_box">
            <button class="btn" onclick="window.location.href='/mobile-aspirin/index'">知道了</button>
        </div>
    </div>
    <script>
	function saveThumb() {
		$.ajaxFileUpload({
			url: "/mobile-aspirin/upload-user-thumb",
			secureuri: false,
			fileElementId: "file-pic",
			dataType: "json",
			success: function(data, status) {
				document.getElementById("user_thumb").src = data.user_thumb;
			}
		})
	}
	function authuser()
	{
		var user_position = $("#user_position").val();
		var card_number = $("#card_number").val();
		var thumb = document.getElementById("user_thumb").attributes["src"].nodeValue;
		if(user_position.length < 1){
			alert("请选择职称");
			return false;
		}
		if(card_number.length < 1){
			alert("请填写医师证号");
			return false;
		}
		if(thumb.length < 1){
			alert("请上传证件照片");
			return false;
		}
		var url = '/mobile-aspirin/auth-save';
		var data = {'user_position':user_position,'card_number':card_number,'thumb':thumb};
		$.post(url,data,function(msg){
			if(msg.success){
				var shade = document.getElementById('shade');
				shade.style.display ="block";
				var modulewindow = document.getElementById('module_window');
				modulewindow.style.display ="block";
				return;
			}else{
				alert('请重新提交');
				return;
			}
		  },'json')
	}
	</script>
@stop