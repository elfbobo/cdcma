@extends('mobile.common.layoutasp')
@section('title')阿司匹林专项基金 @stop
@section('description')阿司匹林专项基金@stop
@section('keywords')阿司匹林专项基金@stop
@section('content')
@include('mobile.common.stopforward')
	<div class="wrap">
		<div class="page_top_box">
	    	<input type="button" class="btn_back" onclick="WeixinJSBridge.call('closeWindow');"/>
	        <div class="page_top">专项基金</div>
	        <input type="button" class="btn_share" style="visibility:hidden;"/>
	    </div>
		<div class="wrap-index wrap-style2">
	        <div class=""  style="width: 100%">
	            <!-- Swiper -->
	            <div class="swiper-container">
	                <div class="swiper-wrapper">
	                	@foreach($oGroup as $k=>$v)
	                	@if($k == 0)
	                    <div class="swiper-slide">
	                    <a href="/aspirinshare/info-brief">
	                    <img src="{{$v->info_group_thumb}}" style="height:147px;">
	                    <div class="tip_title">{{$v->info_group_title}}</div>
	                    </a>
	                    </div>
	                    @else
	                    <div class="swiper-slide">
	                    <a href="/mobile-aspirin/info-list/{{$v->id}}">
	                    <img src="{{$v->info_group_thumb}}" style="height:147px;">
	                    <div class="tip_title">{{$v->info_group_title}}</div>
	                    </a>
	                    </div>
	                    @endif
	                    @endforeach
	                </div>
	                <!-- Add Pagination -->
	                <div class="swiper-pagination"></div>
	            </div>
	            <!-- Initialize Swiper -->
	            <script>
	                var swiper = new Swiper('.swiper-container', {
	                    pagination: '.swiper-pagination'
	                });
	            </script>
	           
            	<div class="index-cont" style="margin: 5% auto 0">
                <div class="index-module">
                    <a href="/mobile-aspirin-online/index" class="center">
                        <dl>
                            <dt><img src="/assets/images/mobile/fund/online_meeting.png" alt=""></dt>
                            <dd>在线会议</dd>
                        </dl>
                    </a>
                </div>
                <div class="index-module">
                    <a href="/aspirinfund/scientific-research">
                        <dl>
                            <dt><img src="/assets/images/mobile/fund/scientific_research.png" alt=""></dt>
                            <dd>科研培训</dd>
                        </dl>
                    </a>
                    <a onclick="geturl();" style="cursor:pointer;">
                        <dl>
                            <dt><img src="/assets/images/mobile/fund/risk_screening.png" alt=""></dt>
                            <dd>风险筛查</dd>
                        </dl>
                    </a>
                </div>
            	</div>
	        </div>
	    </div>
    </div>
    @if($oUser->card_auth_flag == 3)
    <div class="shade" id="shade" style="display:block;"></div>
    <div class="module-window" id="notice_box" style="display:block;">
        <div class="txt">
            <p>温馨提示：</p>
            <p>您的认证信息有误</p>
            <p>请到个人中心我的认证进行修改</p>
        </div>
        <div class="btn_box">
            <button class="btn" onclick="$('#shade').hide();$('#notice_box').hide();">知道了</button>
        </div>
    </div>
    @endif
    <script type="text/javascript">
	function geturl()
	{
		var url = '/mobile-aspirin/screening-hit';
		$.post(url,{},function(msg){
			if(msg.success){
				window.location.href=msg.url;
			}
		  },'json')
	}
	</script>
@stop
