@extends('mobile.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
@if($isWxFlag)
wx.config({
	debug:false,
	appId: "{{$wx_config['appId']}}",//必填
	timestamp:{{$wx_config['timestamp']}}, // 必填，生成签名的时间戳
	nonceStr: "{{$wx_config['nonceStr']}}", // 必填，生成签名的随机串
	signature: "{{$wx_config['signature']}}",// 必填，签名
	jsApiList: ['scanQRCode']// 必填，需要使用的JS接口列表，所有JS接口列表见附录2
});

	function saoyisao(){
		wx.scanQRCode({
		    needResult: 0, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果，
		    scanType: ["qrCode","barCode"], // 可以指定扫二维码还是一维码，默认二者都有
		    success: function (res) {
		    var result = res.resultStr; // 当needResult 为 1 时，扫码返回的结果
		}
		});
	}
@else
	function saoyisao(){
		alert('请在微信浏览器使用');
	}
	@endif
</script>
<div class="bg">
    <div class="top">
        <img class="wid100" src="/assets/images/weixin/top2.jpg" />
    </div>
    <div class="page_nav_box" style="top:25%;">
    	<div class="nav_block"  style="visibility:hidden;">
        	<a href="javascript:void(0);">
            	<div class="nav_img">
                    <img class="wid100" src="/assets/images/weixin/nav_icon01.png">
                </div>
                <div class="">
                        空中课堂
                </div>
        	</a>
        </div>
        <div class="nav_block clearfix">
            @if($iRoleId == 2)
            <a href="/mobile-mytrain" class="left">
                <div class="nav_img">
                    <img class="wid100" src="/assets/images/weixin/nav_icon002.png">
                </div>
                <div class="">
                        我的培训
                </div>
            </a>
            @else
            <a href="javascript:void(0)" onclick="saoyisao()" class="left">
                <div class="nav_img">
                    <img class="wid100" src="/assets/images/weixin/nav_icon02.png">
                </div>
                <div class="">
                        签到调研
                </div>
            </a>
            @endif
            <a href="/mobile-docface" class="right">
                <div class="nav_img">
                    <img class="wid100" src="/assets/images/weixin/nav_icon01.png">
                </div>
                <div class="">
                         空中课堂
                </div>
            </a>
            <!--  
            <a href="/mobile-user" class="right">
                <div class="nav_img">
                    <img class="wid100" src="/assets/images/weixin/nav_icon03.png">
                </div>
                <div class="">
                        个人中心
                </div>
            </a>
            -->
    	</div>
    </div>		
    <div class="page_btm_box">
        <div>
            主办单位：北京医卫健康公益基金会
        </div>
<!--         <div> -->
<!--             赞助单位：拜耳医药保健有限公司 -->
<!--         </div> -->
        <div>
            网站所有权归北京医卫健康公益基金会所有
        </div>
        <div class="red" @if($iRoleId == 2) style="color:#0090d2;" @endif>
            如遇APP使用问题，请拨打咨询电话<?php echo HOTLINE;?><br/>（工作时间09:00-18:00）
        </div>
        <div>
            如需报告使用拜耳产品后出现的不适或产品质量问题，可与您的医生药师或本地药品监管部门进行联系；也可通过以下方式直接联系我们。邮箱：pv.china@bayer.com
        </div>
    </div>
</div>    
@stop