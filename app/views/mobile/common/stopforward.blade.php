<?php 
$isWxFlag = user_agent_is_weixin();
$wx_config = array();
if ($isWxFlag){
	include_once  app_path().'/include/jssdk.class.php';  //先把类包含进来
	$oJssdk = new jssdk();
	$wx_config = $oJssdk->getSignPackage();
}
?>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
@if($isWxFlag)
wx.config({
	debug:false,
	appId: "{{$wx_config['appId']}}",//必填
	timestamp:{{$wx_config['timestamp']}}, // 必填，生成签名的时间戳
	nonceStr: "{{$wx_config['nonceStr']}}", // 必填，生成签名的随机串
	signature: "{{$wx_config['signature']}}",// 必填，签名
	jsApiList: ['hideOptionMenu']// 必填，需要使用的JS接口列表，所有JS接口列表见附录2
});
wx.ready(function(){
	wx.hideOptionMenu();
});
wx.error(function(res){ });
@endif
</script>

