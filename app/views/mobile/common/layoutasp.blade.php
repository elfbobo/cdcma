<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>@yield('title')</title>
<meta name="description" content="@yield('description')"/>
<meta name="keywords" content="@yield('keywords')"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="/assets/js/jquery-3.0.0.min.js"></script>
<link rel="stylesheet" href="/assets/css/mobileaspirin/swiper-3.3.1.min.css">
<link rel="stylesheet" type="text/css" href="/assets/css/mobileaspirin/index.css?v=1"/>
<script src="/assets/js/swiper-3.3.1.jquery.min.js"></script>

<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?b4026242a28685ebffeb34a7817c8461";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
</head>
<body>
@yield('content')
</body>
</html>