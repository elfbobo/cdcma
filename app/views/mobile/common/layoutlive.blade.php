<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>@yield('title')</title>
<meta name="description" content="@yield('description')"/>
<meta name="keywords" content="@yield('keywords')"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<!-- <link rel="stylesheet" type="text/css" href="/assets/css/weixin.css?r=20170415" /> -->
<script src="http://libs.baidu.com/jquery/1.7.2/jquery.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="/assets/css/lkwapstyle.css">
<link rel="stylesheet" type="text/css" media="screen" href="/assets/css/swiper.min.css">
<script>window.FETCHER_SERVER_URL = "ws://fetcher.mudu.tv:8088";</script>
<script src="//static.mudu.tv/fetcher/bundle.6d7aca164d2389e8bea6.js"></script>
<script src="//static.mudu.tv/static/websdk/sdk.js"></script>
<script language="javascript" type="text/javascript" src="/assets/js/swiper.min.js"></script>
<script type="text/javascript" src="/assets/js/jquery-ui-1.10.4.custom.min.js"></script>
<script language="javascript" type="text/javascript" src="/assets/js/jquery.ui.touch-punch.min.js"></script>

<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?b4026242a28685ebffeb34a7817c8461";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
</head>
<body>
@yield('content')
</body>
</html>