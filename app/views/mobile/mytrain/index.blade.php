@extends('mobile.common.layout')
@section('title')我的培训-国卫健康云 @stop
@section('description')我的培训-国卫健康云@stop
@section('keywords')我的培训-国卫健康云 @stop
@section('content')
<div class="page_top_box">
    <input type="button" class="btn_back" onclick="window.location.href='/mobile'"/>
    <div class="page_top">我的培训</div>
    <input type="button" class="btn_share" style="visibility:hidden;"/>
</div>
<div class="theme_box">
	@if($oTrain)
	@foreach($oTrain as $k=>$v)
		<div class="theme_block">
            <div class="theme_tit">
                培训主题：{{$v->train_title}}
            </div>
            <div class="theme_time">
                培训时间：{{substr($v->start_time,0,16)}}—{{substr($v->end_time,11,5)}}
            </div>
            <div class="theme_btn">
                @if(($v->start_time<date('Y-m-d H:i:s',(time()+300)))&&($v->end_time>date('Y-m-d H:i:s')))
		        <input type="button" class="btn" value="参加培训" onclick="window.location.href='/mobile-mytrain/enter-live/{{$v->id}}'" />
		        @elseif($v->end_time<date('Y-m-d H:i:s'))
		                
		        @if($v->train_type==2)
		        <input type="button" class="btn" value="观看录播" onclick="window.location.href='/mobile-mytrain/enter-review/{{$v->id}}'">
		        @else
		        <input type="button" class="btn" value="参加培训" onclick="alert('培训已过期')">
		        @endif
		                
		        @else
		        <input type="button" class="btn" value="参加培训" onclick="alert('未到会议时间')">
		        @endif
            </div>
        </div>
	@endforeach
	@endif    
</div>	
@stop
