@extends('mobile.common.layout')
@section('title')我的培训-国卫健康云 @stop
@section('description')我的培训-国卫健康云@stop
@section('keywords')我的培训-国卫健康云 @stop
@section('content')
<script src="/assets/js/video.js"></script>
<div class="page_top_box">
    <input type="button" class="btn_back" onclick="window.location.href='/mobile'"/>
    <div class="page_top">我的培训</div>
    <input type="button" class="btn_share" style="visibility:hidden;"/>
</div>
<div class="infor_box">
	<div class="in_infor_tit">
    	{{$oTrain->train_title}}
	</div>
    <div class="video_box" id="a1">
    </div>
    <div class="infor_block clearfix">
    	<div class="right">
        	<a class="in_thumb_num" href="javascript:void(0);" onclick="docface_video_zan({{$oTrain->id}},{{Session::get('userid')}},3)" id="docface_video_zan">{{$oTrain->train_support}}</a>
        </div>
    </div>
</div>
<?php 
   $sVideoUrl = '';
   if (!empty($oTrain->train_url)){
        $sVideoUrl = Config::get("config.cdn_url").$oTrain->train_url;
   }
?>
<script type="text/javascript" src="/assets/js/player/ckplayer/ckplayer.js" charset="utf-8"></script>
<script type="text/javascript">
	var __flg; 
	var flashvars={
		f:'{{$sVideoUrl}}',
		c:0,
		p:2,
	    b:0,
	    loaded:'loadedHandler',
		};
	var video=['{{$sVideoUrl}}','{{$sVideoUrl}}','{{$sVideoUrl}}'];
	CKobject.embed('/assets/js/player/ckplayer/ckplayer.swf','a1','ckplayer_a1','100%','219',false,flashvars,video);
	function loadedHandler(){
	    if(CKobject.getObjectById('ckplayer_a1').getType()){
	    	//添加监听handler
	        CKobject.getObjectById('ckplayer_a1').addListener('play',playHandler);
	        CKobject.getObjectById('ckplayer_a1').addListener('pause',pauseHandler);
	    }
	    else{
	        //添加监听handler
	        CKobject.getObjectById('ckplayer_a1').addListener('play','playHandler');
	        CKobject.getObjectById('ckplayer_a1').addListener('pause','pauseHandler');
	    }
	}
	
	function playHandler(){
		//用户积分
		clearInterval(__flg);
		__flg = self.setInterval("addUserMinute({{$oTrain->id}},{{Session::get('userid')}})",18000);
	}

	function addUserMinute(videoid,userid){
		var time_second = Math.round(CKobject.getObjectById('ckplayer_a1').getStatus().time);
		var time_minute = Math.round(time_second/60);
		//console.log(time_second);
		var url = "/mobile-mytrain/video-view/"+videoid+"/"+userid+"/"+time_minute;
		$.post(url,{},function(msg){
			if(msg=='success'){
				console.log('success');
			}
		})
	}
	
	function ckplayer_status(str){
		//播放结束
		if(str == 'ended'){
			clearInterval(__flg);
			addUserMinute({{$oTrain->id}},{{Session::get('userid')}});
		}
	}
</script>
@stop