@extends('mobile.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
<script type="text/javascript">
function login_sub(){
	  var is_aspirin = $.trim($('#is_aspirin').val());
	  var user_nick_login = $.trim($('#user_nick_login').val());
	  var password_login = $.trim($('#password_login').val());
	  if(!user_nick_login||!password_login){
		  $('#codeimg').click();
		  alert('用户名和密码不能为空！');
		  return;
	  }
	  $.post('/login',{'user_nick':user_nick_login,'password':password_login,'is_aspirin':is_aspirin},function(msg){
		data = eval("("+msg+")");//转换为json对象 
		if( data.success =='agreeterms'){
			var mobile_cover_agree_caluese = document.getElementById("mobile_cover_agree_caluese");
			var mobile_pop_up_caluese      = document.getElementById("mobile_pop_up_caluese");
			var old_user_id  = document.getElementById("old_user_id");
			mobile_cover_agree_caluese.style.display ="block";
			mobile_pop_up_caluese.style.display ="block";
			old_user_id.value = data.userid;
		}else if(data.success=='success'){
			if(data.url != ''){
				var str = '';
				str += '<div class="wechat_box">'+
					    	'<p>截图保存以下二维码邀请医生注册，</p>'+
				    		'<p>参与专项基金项目</p>'+
				    		'<img src="'+data.url+'" alt="">'+
		                '</div>';
				$('#login_box').empty();
				$('#login_box').append(str);
			}else{
				if(data.data!=''){
					alert('登陆成功,您的用户编码为'+data.data);
					window.location.reload();
					return;
				}
				window.location.reload();
			}
		}else{
			alert('请输入正确的用户名和密码');
		}
	  })
}

function layoutagreewithcalues()
{
	  var old_user_id = document.getElementById("old_user_id").value;
	  $.post('/agree-disclaimer',{'old_user_id':old_user_id},function(msg){
			data = eval("("+msg+")");//转换为json对象 
			if(data.success=='success'){
				var mobile_cover_agree_caluese = document.getElementById("mobile_cover_agree_caluese");
				var mobile_pop_up_caluese      = document.getElementById("mobile_pop_up_caluese");
				mobile_cover_agree_caluese.style.display ="none";
				mobile_pop_up_caluese.style.display ="none";
			}
		  })

	  var is_aspirin = $.trim($('#is_aspirin').val());
	  var user_nick_login = $.trim($('#user_nick_login').val());
	  var password_login = $.trim($('#password_login').val());
	  if(!user_nick_login||!password_login){
		  $('#codeimg').click();
		  alert('用户名和密码不能为空！');
		  return;
	  }
	  $.post('/login',{'user_nick':user_nick_login,'password':password_login,'is_aspirin':is_aspirin},function(msg){
		data = eval("("+msg+")");//转换为json对象 
		if( data.success =='agreeterms'){
			var mobile_cover_agree_caluese = document.getElementById("mobile_cover_agree_caluese");
			var mobile_pop_up_caluese      = document.getElementById("mobile_pop_up_caluese");
			var old_user_id  = document.getElementById("old_user_id");
			mobile_cover_agree_caluese.style.display ="block";
			mobile_pop_up_caluese.style.display ="block";
			old_user_id.value = data.userid;
		}else if(data.success=='success'){
			if(data.url != ''){
				var str = '';
				str += '<div class="wechat_box">'+
					    	'<p>截图保存以下二维码邀请医生注册，</p>'+
				    		'<p>参与专项基金项目</p>'+
				    		'<img src="'+data.url+'" alt="">'+
		                '</div>';
				$('#login_box').empty();
				$('#login_box').append(str);
			}else{
				if(data.data!=''){
					alert('登陆成功,您的用户编码为'+data.data);
					window.location.reload();
					return;
				}
				window.location.reload();
			}
		}else{
			alert('请输入正确的用户名和密码');
		}
	  })
	  
}
function bayoutclosegreedbox()
{
	var mobile_cover_agree_caluese = document.getElementById("mobile_cover_agree_caluese");
	var mobile_pop_up_caluese      = document.getElementById("mobile_pop_up_caluese");
	mobile_cover_agree_caluese.style.display ="none";
	mobile_pop_up_caluese.style.display ="none";
}
</script>
	<input type="hidden" id="is_aspirin" name="is_aspirin" value="{{$iAspirinFlag}}"/>
	
	<div class="bg">
        <div class="top">
            <img class="wid100" src="/assets/images/weixin/top.jpg" />
        </div>
        <div id="login_box">
    	<div class="login_box0">
            <div class="login_tit">
                国卫健康云
            </div>
            <input type="hidden" id="old_user_id">
            <div class="login_block">
                <div class="login_left">
                    用户名:
                </div>
                <div class="login_right">
                    <input class="login_txt" type="text" id="user_nick_login" name="user_nick_login" placeholder="请输入手机/邮箱/用户名"/>
                </div>
            </div>
            <div class="login_block">
                <div class="login_left">
                    密　码:
                </div>
                <div class="login_right">
                    <input class="login_txt" type="password" id="password_login" name="password_login" placeholder="请输入密码"/>
                </div>
            </div>
            <div class="login_btn_block">
                <input class="login_btn" type="button" value="登录" onclick="login_sub()"/>
            </div>
            <div class="registered_block">
            	<a href="/forget">忘记密码？</a></br>
                没有账号？去<a class="registered" href="/mobile/register">注册</a>>
            </div>
		    <div class="instructions_box">
		        供医疗卫生专业人士使用
		    </div>
        </div>
        </div>
        <div class="btm_pic">
	        <img src="/assets/images/weixin/btm2.jpg" />
	    </div>
	</div>
	
	
	  
    <div class="cover" style="display:none;" id="mobile_cover_agree_caluese"></div>
    <div class="pop-up" style="display:none; top: 50%;" id="mobile_pop_up_caluese"> 
        <div class="head-top">个人信息披露知情同意</div>
        <div class="content">
            <p>本文件旨在向您告知有关国卫健康云在收集、使用和保护个人信息方面的政策，并且，一旦签署，您将允许主办方：《北京医卫健康公益基金会》使用您的个人信息。请仔细阅读本知情同意。</p>
            <h1>收集个人信息：</h1>
            <p>您已被告知，并且您已同意北京医卫健康公益基金会将收集和使用您所提供的如下个人信息，包括：① 您的姓名，② 您工作所在的医疗机构，③ 您的电子邮箱地址，④ 您的手机号码等相关个人信息。</p>
            <h1>使用个人信息：</h1>
            <p>基于您愿意与北京医卫健康公益基金会开展合作，以接受北京医卫健康公益基金会提供的科学信息和产品信息，并参加相关意见调查，北京医卫健康公益基金会将为以下目的使用您的个人信息：① 北京医卫健康公益基金会与您之间的学术交流；② 邀请您参加学术会议；③ 邀请您参加意见调查；④ 发送会议通知、文件或材料。</p>
            <h1>向第三方披露个人信息：</h1>
            <p>北京医卫健康公益基金会将向国内或国外的北京医卫健康公益基金会关联方或北京医卫健康公益基金会所委托的第三方披露您的个人信息，并且该等关联方或第三方将为本知情同意中所规定之目的使用您的个人信息。</p>
            <h1> 个人信息保护：</h1>
            <p>北京医卫健康公益基金会已制定了相关政策并采取了相关安全措施，以保护您的个人信息免受未经授权的访问、篡改、泄露、毁损或丢失。北京医卫健康公益基金会将与接收您个人信息的任何授权第三方订立书面合同，确保该第三方承担有关采取安全措施保护您个人信息的合同义务。</p>
        </div>
        <div class="foot-agree">
            <div class="checkbox">
                <input type="radio" name="agree" onclick="layoutagreewithcalues()">
                <label for="">我同意</label>
            </div>
            <div class="checkbox">
                <input type="radio" name="agree" onclick="bayoutclosegreedbox()">
                <label for="">我不同意</label>
            </div>
        </div>
    </div>
    <script>
        $('.pop-up').css({
            'height':$(window).height()*0.8,
            'margin-top':-$(window).height()*0.8*0.5
        })
        $('.pop-up .content').css('height',$('.pop-up').height()*0.8)
   </script>
@stop