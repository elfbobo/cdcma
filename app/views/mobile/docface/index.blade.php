@extends('mobile.common.layout')
@section('title')空中课堂-国卫健康云 @stop
@section('description')空中课堂-国卫健康云@stop
@section('keywords')空中课堂-国卫健康云 @stop
@section('content')
<script src="/assets/js/mobilelist.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/js/swiper/swiper.min.css" />
<script type="text/javascript" src="/assets/js/swiper/swiper.min.js"></script>
<div class="page_top_box">
    	<input type="button" class="btn_back" onclick="window.location.href='/mobile'"/>
        <div class="page_top">空中课堂</div>
        <input type="button" class="btn_share" style="visibility:hidden;"/>
    </div>
    <div class="page_cont_box">
        <div class="video_box">
        	@if($oFaceVideoAdvance)
        	<?php 
            $aDepart = explode('|',$oFaceVideoAdvance->department_id); 
            ?>
            @if($oFaceVideoAdvance->start_time>date('Y-m-d H:i:s'))
            <!-- 倒计时 -->
            @if((strtotime($oFaceVideoAdvance->start_time)-60*20)<time())
            <a href="/mobile-docface/enter-live/{{$oFaceVideoAdvance->id}}">
            @else
            <a href="/mobile-docface/doc-show/{{$oFaceVideoAdvance->doc_id}}">
            @endif
            	@if(in_array(5, $aDepart))
            		<img class="wid100" src="/assets/images/weixin/img001.jpg" />
	            @else
	            	<img class="wid100" src="/assets/images/weixin/img01.jpg" />
	            @endif
            </a>
            <div class="countdown_box">
            	<div class="countdown">
                	<span class="icon">距离开讲</span>
                	<?php 
					$time = strtotime($oFaceVideoAdvance->start_time)-time();	
					?>
					<input type="hidden" id="timestamp_input" value="{{$time}}"/>
                	<span id="day_show" class="time">00</span>天<span class="time" id="hour_show">00</span>:<span class="time"id="minute_show">00</span>:<span class="time" id="second_show">00</span>
                </div>
                <div class="title">
                	<span>{{$oFaceVideoAdvance->doc_info->doc_name}}</span>
                    {{$oFaceVideoAdvance->video_title}}
                    <br/>
                    <div style="color:red;">直播会议开始前20分钟可点击倒计时页面进行视频、声音的测试</div>
                </div>
            </div>
            @elseif(($oFaceVideoAdvance->start_time<date('Y-m-d H:i:s'))&&($oFaceVideoAdvance->end_time>date('Y-m-d H:i:s')))
            <!-- 直播开始 -->
            <a href="/mobile-docface/enter-live/{{$oFaceVideoAdvance->id}}">
            	@if(in_array(5, $aDepart))
            		<img class="wid100" src="/assets/images/weixin/img001.jpg" />
	            @else
	            	<img class="wid100" src="/assets/images/weixin/img01.jpg" />
	            @endif
            </a>
            <div class="countdown_box" onclick="window.location.href='/mobile-docface/enter-live/{{$oFaceVideoAdvance->id}}'">
            	<div class="countdown_txt">
                    直播正在进行中
                </div>
            </div>
            @else
            <!-- 敬请期待 -->
            <a href="javascript:void(0)">
            	<img class="wid100" src="/assets/images/weixin/img01.jpg" />
            </a>
            <div class="countdown_box">
	            <div class="countdown_txt">
	                    敬请期待
	            </div>
	        </div>
            @endif
            @endif
        </div>
        @if($oFaceVideoAdvance1)
        <div class=" infor_box">
        	<div class=" infor_tit">
            	精彩预告
            </div>
            <div class="infor_cont">
            
            	<a class="article_link" href="/mobile-docface/doc-show/{{$oFaceVideoAdvance->doc_id}}">
                	<div class="article">
                    	<div class="dot"></div>
                        <div class="article_cont">
                        	<div class="art_tit">
                            	<span>主题：</span>{{$oFaceVideoAdvance1->video_title}}
                            </div>
                            <div class="art_txt">
                            	<div class="art_author">
                                	{{$oFaceVideoAdvance1->doc_info->doc_name}} {{$oFaceVideoAdvance1->doc_info->doc_hospital}}
                                </div>
                                <div class="art_time">
                                	{{substr($oFaceVideoAdvance1->start_time,0,16)}}-{{substr($oFaceVideoAdvance1->end_time,11,5)}}
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                
            </div>
        </div>
        @endif
        <div class=" infor_box blue_bg">
        	<div class=" infor_tit">
            	精彩回顾
            </div>
            <input type="hidden" id="department_id" value="0"/>
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <a class="department sele" href="javascript:void(0);" onclick="departmentGroup(0);">全部</a>
                    </div>
                    <div class="swiper-slide">
                        <a class="department" href="javascript:void(0);" onclick="departmentGroup(1);"><span>●</span>内分泌科</a>
                    </div>
                    <div class="swiper-slide">
                        <a class="department" href="javascript:void(0);" onclick="departmentGroup(2);"><span>●</span>心血管内科</a>
                    </div>
                    <div class="swiper-slide">
                        <a class="department" href="javascript:void(0);" onclick="departmentGroup(3);"><span>●</span>神经内科</a>
                    </div>
                    <div class="swiper-slide">
                        <a class="department" href="javascript:void(0);" onclick="departmentGroup(4);"><span>●</span>肾病内科</a>
                    </div>
                    <div class="swiper-slide">
                        <a class="department" href="javascript:void(0);" onclick="departmentGroup(5);"><span>●</span>社区</a>
                    </div>
                </div>
            </div>
            <div class="infor_cont" id="container">
            @foreach($oFaceVideoReview as $k=>$v)
                <a class="article_link" href="/mobile-docface/review-show/{{$v->id}}">
                	<div class="article">
                    	<div class="dot"></div>
                        <div class="article_cont">
                        	<div class="art_tit">
                            	<span>主题：</span>{{$v->video_title}}
                            </div>
                            <div class="art_txt">
                            	<div class="art_author">
                                	{{$v->doc_name}} {{str_cut_cms($v->doc_hospital,28)}} 
                                </div>
                                <div class="art_time">
                                	{{substr($v->start_time,0,16)}}-{{substr($v->end_time,11,5)}}
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach    
            </div>
        </div>
    </div> 
@stop