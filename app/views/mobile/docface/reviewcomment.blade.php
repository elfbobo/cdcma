@extends('mobile.common.layout')
@section('title')空中课堂-国卫健康云 @stop
@section('description')空中课堂-国卫健康云@stop
@section('keywords')空中课堂-国卫健康云 @stop
@section('content')
<script src="/assets/js/video.js"></script>
<script>
$(function(){
	//隐藏评论提示
	$('.reply_txt').focus( function(){
		$('.prompt').hide();
	});
	
	//显示评论提示
	$('.reply_txt').blur( function(){
		if($('.reply_txt').val().length == 0){
			$('.prompt').show();
		} else {
			$('.prompt').hide();
		}
	});
	
}); 
</script>
<div class="page_top_box">
    	<input type="button" class="btn_back" onclick="window.location.href='/mobile-docface/review-show/{{$iReviewId}}'"/>
        <div class="page_top">空中课堂评论</div>
        <input type="button" class="btn_share" style=" visibility:hidden" />
    </div>
    <div class="page_cont_box">
        <div class="comment_box">
        @foreach($oComments as $oComment)
            <div class="comment_block">
            	<div class="com_pic">
                	<img style="width:46px;height:46px;" src="{{$oComment->user_thumb}}" />
                </div>
                <div class="com_cont">
                	<div class="com_tit">
                    	<div class="com_name">
                        	{{$oComment->user_nick}}
                        </div>
                        <div class="com_tit">
                        	{{substr($oComment->created_at,0,10)}}
                        </div>
                    </div>
                    <div class="com_infor">
                    	{{$oComment->comment}}
                    </div>
                    <div class="com_click">
                    	<a class="com_num" href="javascropt:void(0);" onclick="comment_zan({{$oComment->id}},1)">
                    		（<span id="comment_zan_{{$oComment->id}}">{{$oComment->zan_count}}</span>）
                    	</a>
                        <a class="com_reply" href="javascropt:void(0);" onclick="video_reply_mobile({{$oComment->user_id}},'{{$oComment->user_nick}}')">
                        	回复
                        </a>
                    </div>
                </div>
            </div>
         @endforeach   
        </div>
    </div>
    <div class="reply_box">
    	<div class="reply_txt_block">
        	<div class="reply">
            	<div class="prompt">写评论</div>
            	<input class="reply_txt" type="text" id="content"/>	
            </div>            
        </div>
        <div class="reply_btn_block">
        	<input class="reply_btn" type="button" value="发表" onclick="reply_mobile({{$iReviewId}},1);"/>
        </div>
    </div> 
    

@stop
