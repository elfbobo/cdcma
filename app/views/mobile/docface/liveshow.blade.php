@extends('mobile.common.layoutlive')
@section('title')空中课堂-国卫健康云 @stop
@section('description')空中课堂-国卫健康云@stop
@section('keywords')空中课堂-国卫健康云 @stop
@section('content')
<?php if($oFaceVideo['channel_type'] == 2){ ?>
    <div class="page_top_box"><!--这里调用原来他们网站的头部--></div>
    <div class="lk-video-box">
        <div id="mudulive"></div>
        <img style="display: block;" src="http://cdcma.bizconf.cn{{$oFaceVideo->video_thumb}}" width="100%">
        <div class="zbz">直播中</div>
    </div>
    <div class="lk-video-menu">
        <div class="titleBox">
            <a href="javascript:;" class="active"><span>文档</span><i></i></a>
            <a href="javascript:;"><span>问答</span></a>
        </div>
        <div class="lk-video-menu-box">
            <div class="tags-div lk-ppt-div" style="display: block;">
                <img src="http://cdcma.bizconf.cn{{$oFaceVideo->video_thumb}}" width="100%">
                <a href="javascript:;" class="fdicon"><img src="/assets/images/mobile/fdicon.png" width="100%" height="100%"></a>
            </div>
            <div class="tags-div lk-qa-div" style="display:none;">
                <ul>
                </ul>
                <div style="height:44px;"></div>
                <div class="lk-shuru-input">
                    <input class="btn" type="button" id="sendBtn" value="发送">
                    <div class="inputDiv"><input class="txt" type="text" id="sendmsg"></div>
                </div>
            </div>
        </div>
    </div> 
    <script type="text/javascript">
    $(function(){
        var bodyHeight = $(window).height()>480?$(window).height():480;
        var commentsHeight = bodyHeight-$(".page_top_box").height()-$(".lk-video-box").height()-$(".lk-video-menu .titleBox").height();
        $(".lk-video-menu-box").css("height",commentsHeight);
        $(window).resize(function(){
            var bodyHeight = $(window).height()>480?$(window).height():480;
            var commentsHeight = bodyHeight-$(".page_top_box").height()-$(".lk-video-box").height()-$(".lk-video-menu .titleBox").height();
            $(".lk-video-menu-box").css("height",commentsHeight);
        })

        $(".lk-ppt-div .fdicon").click(function(){
            $(".lk-video-box").toggle();
            $(".lk-ppt-div").toggleClass("lk-ppt-div-big");
        })

        $(".lk-video-menu .titleBox a").click(function(){
            var i = $(this).index();
            $(this).addClass("active").siblings("a").removeClass("active");
            $(".lk-video-menu .lk-video-menu-box .tags-div").hide().eq(i).show();
        })
    })
    </script>
    <!--弹出层-->
    <div id="pinjiaFloat" class="floatmain pie" style="display:none;">
        <table class="floatboxs">
            <tr>
                <td valign="middle" height="100%">
                    <div class="tcDivBox">
                        <a href="javascript:;" class="closeBtn closeA"></a>
                        <div class="tcTitleBox">评分</div>      
                        <div class="kdymain">
                            <div class="kdybox">
                                <div class="kdy_bz">
                                    <div class="kdy_bz_t">
                                        <div class="kdy_bz_d">
                                            <div class="ktitle">备课充分内容熟练</div>
                                            <div class="knumber"><span id="score1">0</span>分</div>
                                            <div class="ktuodong">
                                                <div class="kbg"></div>
                                                <div class="kzhizhen"><span></span></div>
                                            </div>                                            
                                        </div>
                                        <div class="kdy_bz_d">
                                            <div class="ktitle">逻辑清晰案例精彩</div>
                                            <div class="knumber"><span id="score2">0</span>分</div>
                                            <div class="ktuodong">
                                                <div class="kbg"></div>
                                                <div class="kzhizhen"><span></span></div>
                                            </div>                                            
                                        </div>
                                        <div class="kdy_bz_d">
                                            <div class="ktitle">授课内容专业正确</div>
                                            <div class="knumber"><span id="score3">0</span>分</div>
                                            <div class="ktuodong">
                                                <div class="kbg"></div>
                                                <div class="kzhizhen"><span></span></div>
                                            </div>                                            
                                        </div>
                                        <div class="kdy_bz_d">
                                            <div class="ktitle">讲课生动风趣</div>
                                            <div class="knumber"><span id="score4">0</span>分</div>
                                            <div class="ktuodong">
                                                <div class="kbg"></div>
                                                <div class="kzhizhen"><span></span></div>
                                            </div>                                            
                                        </div>
                                        <div class="kdy_bz_d">
                                            <div class="ktitle">课程效果好</div>
                                            <div class="knumber"><span id="score5">0</span>分</div>
                                            <div class="ktuodong">
                                                <div class="kbg"></div>   
                                                <div class="kzhizhen"><span></span></div>   
                                            </div>                                            
                                        </div>
                                    </div>            
                                </div>
                                <div class="allFenshu">综合评分：<span id="score6">0</span>分</div>
                            </div>    
                        </div>
                        <input type="hidden" id="videoid" value="{{$oFaceVideo->id}}"/>
                        <input type="hidden" id="userid" value="{{$iUserId}}"/>
                        <div class="promptBtn"><a href="javascript:;" class="dpbtn btn">确认</a></div>          
                    </div>   
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="topicpage" value="1">
    <script>
        $(".dpbtn").click(function(){
            var videoid = $('#videoid').val();
            var userid = $('#userid').val();
            var score1 = $('#score1').html();
            var score2 = $('#score2').html();
            var score3 = $('#score3').html();
            var score4 = $('#score4').html();
            var score5 = $('#score5').html();
            var score6 = $('#score6').html();
            var url = '/docface/live_score';
            var data = {videoid:videoid,userid:userid,score1:score1,score2:score2,score3:score3,score4:score4,score5:score5,score6:score6,device:<?php if($token){ echo "2,token:'{$token}'"; }else{ echo 3; } ?>};
            $.post(url,data,function(msg){
                if(msg == 'success'){
                    // alert('评分成功！');
                    $("#pinjiaFloat").remove();
                    return;
                }else{
                    alert('您已经点评过了！');
                    return;
                }
            })
        })
        $(function(){
            $(".floatmain .closeBtn").click(function(){
                $(this).parents(".floatmain").hide();
            })
            $(".kzhizhen").draggable({drag:function(){
                    var kdiv = $(this).parents(".kdy_bz_d");
                    ksz(kdiv);
                },containment: "parent",axis:"x"});  
            //$("#drag").draggable({containment: "div#main",axis:"x"}); //只允许横向拖动
            //$("#drag").draggable({containment: "div#main",axis:"y"}); //只允许竖向拖动
            $(".kdy_bz_d").resizable({
            });
            $(".ktuodong").click(function(e){
                var ktuodongwidth = $(".ktuodong").width()-18;
               var divx = $(".ktuodong").offset().left-$(document).scrollLeft();
                //("#drag").offset().left;
                //这里可得到鼠标X坐标
                var pointX = e.pageX;
                var x = pointX - divx;
                console.log(x);
                if(x<=ktuodongwidth){$(this).find(".kzhizhen").css("left",x);}
                else{$(this).find(".kzhizhen").css("left",ktuodongwidth);}
                var kdiv = $(this).parents(".kdy_bz_d");
                ksz(kdiv);
            });
        })
        function ksz(kdiv){
            var ktuodongwidth = $(".ktuodong").width()-18;
            var kleft = parseInt(kdiv.find(".kzhizhen").css("left"));
            var n = Math.round(100*kleft/ktuodongwidth);
            kdiv.find(".ktuodong .kbg").css("width",kleft);
            kdiv.find(".knumber span").html(Math.round(n/5));
            var allfenshu = 0;
            $(".kdymain .kdybox .knumber").each(function(){
                allfenshu = allfenshu + Number($(this).find("span").text());
            })
            $(".kdymain .kdybox .allFenshu span").text(allfenshu);
        }
        var player;
        Mudu.Init({{$oFaceVideo->video_id}}, function(){
            //初始化播放器
            player = new Mudu.Player({
                // 播放器容器ID，播放器会被添加到该DOM元素中
                containerId: 'mudulive',
                // 播放器播放类型：支持`live`和`vod`两个值，live为直播播放，vod为点播播放
                type: 'live',
                // 播放器视频播放地址
                src: Mudu.Room.GetPlayAddr(),
                image: 'http://cdcma.bizconf.cn{{$oFaceVideo->video_thumb}}',
                // 播放器是否自动播放
                autoplay: true,
                // 播放器是否显示控制条
                controls: true,
                // 播放器是否循环播放, 默认为false
                repeat: false,
                // 播放器宽度，单位为像素，默认为480
                width: $(window).width(),
                // 播放器高度，单位为像素，默认为270
                height: $(window).width()/320*230
            });
            $(".lk-video-box").find('img').remove();
            Mudu.Room.User.Assign('{{$sUserName}}', '{{$sUserthumb}}','{{$iUserId}}');
            //开始播放 player.play();
            var state = player.getState()
            //暂停播放 player.pause();
            // 返回直播间名字，类型为string
            var roomName = Mudu.Room.GetName();
            // 返回直播状态，类型为number: `1`为正在直播，`0`为不在直播
            var roomLiveStatus = Mudu.Room.GetLiveStatus();
            // 返回直播间浏览量，类型为number整数
            var roomViewNum = Mudu.Room.GetViewNum();
            // 返回直播间视频地址，类型为string
            var roomPlayAddr = Mudu.Room.GetPlayAddr();
            Mudu.MsgBus.On(
                // 事件名，值为Room.StreamEvent
                'Room.StreamEvent',
                // 事件处理函数，参数类型为object
                function (data) {
                    data = JSON.parse(data);
                    if(data.event == 1){
                        player.play();
                    }else{
                        player.stop();
                        $("#pinjiaFloat").show();
                    }
                }
            );
            //获取第一页话题
            gettopic();
            //接受新话题
            Mudu.MsgBus.On('Topic.New',function (topic) {
                topic = JSON.parse(topic);
                if(topic.checked == 1){//判断是否已审核
                    $(".insertTopic").find(".infor").each(function(){
                        if($(this).html() == topic.message){
                            $(this).parent().remove();
                        }
                    });
                    html = '<li id="'+topic.id+'"><div class="name">'+topic.username+'</div><div class="time">'+topic.updated_at+'</div><div class="infor">'+topic.message+'</div></li>';
                    $(".lk-qa-div ul").prepend(html);
                }
            })
            Mudu.MsgBus.On('Topic.Reply.New',function (reply) {
                reply = JSON.parse(reply);
                html = '<li><div class="name">'+reply.username+'</div><div class="time">'+reply.updated_at+'</div><div class="infor">'+reply.message+'</div></li>';
                $("#"+reply.belong_to).after(html);
            })
            //ppt
            var isPptOpen = Mudu.Room.PPT.IsOpen();
            if(isPptOpen){
                var pptUrl = Mudu.Room.PPT.GetUrl();
                $(".lk-ppt-div").find("img").attr("src", pptUrl);
                Mudu.MsgBus.On( "PPT.Changed", function (data) {
                    data = JSON.parse(data);
                    // console.log('新的ppt图片地址为：', data.url);
                    $(".lk-ppt-div").find("img").attr("src", data.url);
                })
            }
        });
        //获取话题互动
        function gettopic(){
            var page = $("#topicpage").val();
            if(page != 0){
                Mudu.Room.Topic.Get(page,function (response) {
                 // response格式为: {status: 'y', flag: 100, topics: [topicItem1, topicItem2, ...]}
                    response = JSON.parse(response);
                    if(response.topics && response.topics.length>0){
                        var html = '';
                        for (var i = 0; i < response.topics.length; i++) {
                            html += '<li id="'+response.topics[i].id+'"><div class="name">'+response.topics[i].username+'</div><div class="time">'+response.topics[i].updated_at+'</div><div class="infor">'+response.topics[i].message+'</div></li>';
                            if(response.topics[i].replies){
                                for (var k = 0; k < response.topics[i].replies.length; k++) {
                                    html += '<li><div class="name">'+response.topics[i].replies[k].username+'</div><div class="time">'+response.topics[i].replies[k].updated_at+'</div><div class="infor">'+response.topics[i].replies[k].message+'</div></li>';
                                }
                            }
                        }
                        $(".lk-qa-div ul").append(html);
                        page=parseInt(page)+1;
                        $("#topicpage").val(page)
                        gettopic();
                    }else{
                        $("#topicpage").val(0);
                    }
                })
            }
            return false;   
        }

        $("#sendBtn").click(function(){
            var sendmsg = $("#sendmsg").val();
            if(sendmsg){
                Mudu.Room.Topic.SendTopic({
                    msg: sendmsg
                },
                function (response) {
                    response = JSON.parse(response);
                    if(response.status == 'y'){
                        html = '<li class="insertTopic"><div class="name">{{$sUserName}}</div><div class="time"><?php echo date('Y-m-d H:i:s'); ?></div><div class="infor">'+sendmsg+'</div></li>';
                        $(".lk-qa-div ul").prepend(html);
                        $("#sendmsg").val('');
                    }           
                })
            }
        })
        $(function(){
            setInterval("addUserMinuteLive({{$oFaceVideo->id}},{{$iUserId}})",180000);
        }) 
        function addUserMinuteLive(videoid,userid){
            var timestamp = Date.parse(new Date());
            var time_second = timestamp/1000;
            var url = "/docface/video_view_live/"+videoid+"/"+userid+"/"+time_second+"/mobile";
            $.post(url,{},function(msg){
                if(msg=='success'){
                    console.log('success');
                }
            })
        }
    </script>
@stop
<?php }else{ ?>
    <iframe width="100%" height="100%" src="{{$oFaceVideo->video_url}}?nickName={{$sUserName}}"></iframe>
    <div id="pinjiaFloat" class="floatmain pie" style="display:none;">
        <table class="floatboxs">
            <tr>
                <td valign="middle" height="100%">
                    <div class="tcDivBox">
                        <a href="javascript:;" class="closeBtn closeA"></a>
                        <div class="tcTitleBox">评分</div>      
                        <div class="kdymain">
                            <div class="kdybox">
                                <div class="kdy_bz">
                                    <div class="kdy_bz_t">
                                        <div class="kdy_bz_d">
                                            <div class="ktitle">备课充分内容熟练</div>
                                            <div class="knumber"><span id="score1">0</span>分</div>
                                            <div class="ktuodong">
                                                <div class="kbg"></div>
                                                <div class="kzhizhen"><span></span></div>
                                            </div>                                            
                                        </div>
                                        <div class="kdy_bz_d">
                                            <div class="ktitle">逻辑清晰案例精彩</div>
                                            <div class="knumber"><span id="score2">0</span>分</div>
                                            <div class="ktuodong">
                                                <div class="kbg"></div>
                                                <div class="kzhizhen"><span></span></div>
                                            </div>                                            
                                        </div>
                                        <div class="kdy_bz_d">
                                            <div class="ktitle">授课内容专业正确</div>
                                            <div class="knumber"><span id="score3">0</span>分</div>
                                            <div class="ktuodong">
                                                <div class="kbg"></div>
                                                <div class="kzhizhen"><span></span></div>
                                            </div>                                            
                                        </div>
                                        <div class="kdy_bz_d">
                                            <div class="ktitle">讲课生动风趣</div>
                                            <div class="knumber"><span id="score4">0</span>分</div>
                                            <div class="ktuodong">
                                                <div class="kbg"></div>
                                                <div class="kzhizhen"><span></span></div>
                                            </div>                                            
                                        </div>
                                        <div class="kdy_bz_d">
                                            <div class="ktitle">课程效果好</div>
                                            <div class="knumber"><span id="score5">0</span>分</div>
                                            <div class="ktuodong">
                                                <div class="kbg"></div>   
                                                <div class="kzhizhen"><span></span></div>   
                                            </div>                                            
                                        </div>
                                    </div>            
                                </div>
                                <div class="allFenshu">综合评分：<span id="score6">0</span>分</div>
                            </div>    
                        </div>
                        <input type="hidden" id="videoid" value="{{$oFaceVideo->id}}"/>
                        <input type="hidden" id="userid" value="{{$iUserId}}"/>
                        <div class="promptBtn"><a href="javascript:;" class="dpbtn btn">确认</a></div>          
                    </div>   
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        var s1 = setInterval("addUserMinuteLive({{$oFaceVideo->id}},{{$iUserId}})",180000);
        var s2 = setInterval(test,1000);
        function test(){
            var timestamp = Date.parse(new Date());
            if(timestamp >= <?php echo strtotime($oFaceVideo->end_time)*1000; ?>){
                $("iframe").remove();
                $("#pinjiaFloat").show();
                clearInterval(s2);
            }
        }
        function addUserMinuteLive(videoid,userid){
            var timestamp = Date.parse(new Date());
            var time_second = timestamp/1000;
            var url = "/docface/video_view_live/"+videoid+"/"+userid+"/"+time_second+"/mobile";
            $.post(url,{},function(msg){
                if(msg=='success'){
                    console.log('success');
                }
            })
        }
        $(".dpbtn").click(function(){
            var videoid = $('#videoid').val();
            var userid = $('#userid').val();
            var score1 = $('#score1').html();
            var score2 = $('#score2').html();
            var score3 = $('#score3').html();
            var score4 = $('#score4').html();
            var score5 = $('#score5').html();
            var score6 = $('#score6').html();
            var url = '/docface/live_score';
            var data = {videoid:videoid,userid:userid,score1:score1,score2:score2,score3:score3,score4:score4,score5:score5,score6:score6,device:<?php if($token){ echo "2,token:'{$token}'"; }else{ echo 3; } ?>};
            $.post(url,data,function(msg){
                if(msg == 'success'){
                    // alert('评分成功！');
                    $("#pinjiaFloat").remove();
                    return;
                }else{
                    alert('您已经点评过了！');
                    return;
                }
            })
        })
        $(function(){
            $(".floatmain .closeBtn").click(function(){
                $(this).parents(".floatmain").hide();
            })
            $(".kzhizhen").draggable({drag:function(){
                    var kdiv = $(this).parents(".kdy_bz_d");
                    ksz(kdiv);
                },containment: "parent",axis:"x"});  
            //$("#drag").draggable({containment: "div#main",axis:"x"}); //只允许横向拖动
            //$("#drag").draggable({containment: "div#main",axis:"y"}); //只允许竖向拖动
            $(".kdy_bz_d").resizable({
            });
            $(".ktuodong").click(function(e){
                var ktuodongwidth = $(".ktuodong").width()-18;
               var divx = $(".ktuodong").offset().left-$(document).scrollLeft();
                //("#drag").offset().left;
                //这里可得到鼠标X坐标
                var pointX = e.pageX;
                var x = pointX - divx;
                console.log(x);
                if(x<=ktuodongwidth){$(this).find(".kzhizhen").css("left",x);}
                else{$(this).find(".kzhizhen").css("left",ktuodongwidth);}
                var kdiv = $(this).parents(".kdy_bz_d");
                ksz(kdiv);
            });
        })
        function ksz(kdiv){
            var ktuodongwidth = $(".ktuodong").width()-18;
            var kleft = parseInt(kdiv.find(".kzhizhen").css("left"));
            var n = Math.round(100*kleft/ktuodongwidth);
            kdiv.find(".ktuodong .kbg").css("width",kleft);
            kdiv.find(".knumber span").html(Math.round(n/5));
            var allfenshu = 0;
            $(".kdymain .kdybox .knumber").each(function(){
                allfenshu = allfenshu + Number($(this).find("span").text());
            })
            $(".kdymain .kdybox .allFenshu span").text(allfenshu);
        }
    </script>
@stop
<?php } ?>