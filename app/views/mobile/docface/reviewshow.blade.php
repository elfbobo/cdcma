@extends('mobile.common.layout')
@section('title')空中课堂-国卫健康云 @stop
@section('description')空中课堂-国卫健康云@stop
@section('keywords')空中课堂-国卫健康云 @stop
@section('content')
<!-- <script src="/assets/js/video.js"></script> -->
<?php 
if($oScoreLog){
	$iBlue1 = $oScoreLog->content_degree;
	$iGray1 = 5-$iBlue1;
	$iBlue2 = $oScoreLog->class_level;
	$iGray2 = 5-$iBlue2;
	$iBlue3 = $oScoreLog->overall_score;
	$iGray3 = 5-$iBlue3;
}
?>
<div class="page_top_box">
    	<input type="button" class="btn_back" onclick="window.location.href='/mobile-docface'"/>
        <div class="page_top">空中课堂详情</div>
        <input type="button" class="btn_comment" value="评论" onclick="window.location.href='/mobile-docface/review-comment/{{$oReview->id}}'" />
    </div>
    <div class="page_cont_box">
    	<div class="title_box">
        	{{$oReview->video_title}}
        </div>
        <div class="video2_box">
        	<div class="video2" id="a1">
            <?php if($oReview->channel_type != 2){ ?>
                <a href="javascript:void(0);">
                    <video id="media" style="width:100%;" controls="controls" poster="/assets/images/front/web/img04.jpg" src="{{Config::get("config.cdn_url").$oReview->video_url}}"></video>
                </a>
            <?php }else{ ?>
            <img src="/assets/images/front/web/img04.jpg" width="100%">
            <?php } ?>
            </div>
            <div class="thumb_num_box evaluation_box">
            	<div class="evaluation">
            		<!-- 判断是否已经提交过星级评分，每人限评一次，评过之后不再可点（3个分开判断） -->
					@if($oScoreLog && $oScoreLog->content_degree != 0)
						<div class="list01">
				            <span>内容丰富程度：</span>
				            @for($i=1;$i<=$iBlue1;$i++)
				            <span><img src="/assets/images/weixin/icon_star1.png" class="star"></span>  
				            @endfor
				            @for($i=1;$i<=$iGray1;$i++)
				            <span><img src="/assets/images/weixin/icon_star2.png" class="star"></span>  
				            @endfor
				        </div>
				    @else
	                    <div class="list">
	                        <span>内容丰富程度：</span>
	                        @for($i=1;$i<=5;$i++)
	                        <span><img src="/assets/images/weixin/icon_star2.png" class="star"></span>
	                        @endfor  
	                    </div>
                    @endif
                    @if($oScoreLog && $oScoreLog->class_level != 0)
						<div class="list02">
				            <span>专家讲课水平：</span>
				            @for($i=1;$i<=$iBlue2;$i++)
				            <span><img src="/assets/images/weixin/icon_star1.png" class="star"></span>  
				            @endfor
				            @for($i=1;$i<=$iGray2;$i++)
				            <span><img src="/assets/images/weixin/icon_star2.png" class="star"></span>  
				            @endfor
				        </div>
					@else
	                    <div class="list2">
	                        <span>专家讲课水平：</span>
	                        @for($i=1;$i<=5;$i++)
	                        <span><img src="/assets/images/weixin/icon_star2.png" class="star"></span>
	                        @endfor 
	                    </div>
                    @endif
                    @if($oScoreLog && $oScoreLog->overall_score != 0)
						<div class="list03">
				            <span>整体综合评分：</span>
				            @for($i=1;$i<=$iBlue3;$i++)
				            <span><img src="/assets/images/weixin/icon_star1.png" class="star"></span>  
				            @endfor
				            @for($i=1;$i<=$iGray3;$i++)
				            <span><img src="/assets/images/weixin/icon_star2.png" class="star"></span>  
				            @endfor
				        </div>
					@else	
	                    <div class="list3">
	                        <span>整体综合评分：</span>
	                        @for($i=1;$i<=5;$i++)
	                        <span><img src="/assets/images/weixin/icon_star2.png" class="star"></span>
	                        @endfor 
	                    </div>
                    @endif
                </div>
            	<a class="thumb_num" onclick="docface_video_zan({{$oReview->id}},{{$iUserId}},1)"><span class=""  id="docface_video_zan">{{$oReview->video_support}}</span></a>
            </div>
        </div>
        <div class="doc_details_box2">
            <div class="doc_details_tit">
                本期专家
            </div>
        	<div class="doc_details_top">
            	<div class="doc_pic">
                   	<img src="{{$oReview->doc_info->doc_thumb}}" />
                </div>
                <div class="doc_txt">
                	<p>专家：{{$oReview->doc_info->doc_name}}</p>
                    <p>职称: {{$oReview->doc_info->doc_position}}</p>
                    <p>科室: {{$oReview->doc_info->doc_department}}</p>
                    <p>医院: {{$oReview->doc_info->doc_hospital}}</p>
                </div>
            </div>
            <div class="doc_details_cont">
                <div>
                	{{$oReview->doc_info->doc_introduction}}
                </div>
            </div>
        </div>
    </div>
<?php 
	$sVideoUrl = '';
   	if (!empty($oReview->video_url)){
   		if (user_agent_is_mobile()){
        	$sVideoUrl = Config::get("config.cdn_url").$oReview->video_url;
   		}else{
   			$sVideoUrl = Config::get("config.cdn_url").str_replace(".mp4",".flv",$oReview->video_url);
   		}
   	}
   	$sVideoId = '';
   	if (!empty($oReview->video_id)){
   		$sVideoId = Config::get("config.cdn_url").$oReview->video_id;
   	}
?>
<?php if($oReview->channel_type == 2){ ?>
<!-- 目睹	 -->
<script>window.FETCHER_SERVER_URL = "ws://fetcher.mudu.tv:8088";</script>
<script src="//static.mudu.tv/fetcher/bundle.6d7aca164d2389e8bea6.js"></script>
<script src="//static.mudu.tv/static/websdk/sdk.js"></script>
<script>
Mudu.Init( {{$sVideoId}}, function(){
    //初始化播放器
    var player = new Mudu.Player({
        // 播放器容器ID，播放器会被添加到该DOM元素中
        containerId: 'a1',
        // 播放器播放类型：支持`live`和`vod`两个值，live为直播播放，vod为点播播放
        type: 'live',
        // 播放器视频播放地址
        src: "{{$sVideoUrl}}",
        image: 'http://cdcma.bizconf.cn{{$oReview->video_thumb}}',
        // 播放器是否自动播放
        autoplay: true,
        // 播放器是否显示控制条
        controls: true,
        // 播放器是否循环播放, 默认为false
        repeat: false,
        // 播放器宽度，单位为像素，默认为480
        width: $(".video2_box").width(),
        // 播放器高度，单位为像素，默认为270
        height: $(".video2_box").width()/375*217
    });
    //开始播放 player.play();
    var state = player.getState()
    //暂停播放 player.pause();
    // 返回直播间名字，类型为string
    var roomName = Mudu.Room.GetName();
    // 返回直播状态，类型为number: `1`为正在直播，`0`为不在直播
    var roomLiveStatus = Mudu.Room.GetLiveStatus();
    // 返回直播间浏览量，类型为number整数
    var roomViewNum = Mudu.Room.GetViewNum();
    // 返回直播间视频地址，类型为string
    var roomPlayAddr = Mudu.Room.GetPlayAddr();
    //Room.StreamEvent 事件
    //Room.StreamEvent事件会在直播流状态改变时(通常是后台开始直播或者关闭直播)被触发
    Mudu.MsgBus.On(
        // 事件名，值为Room.StreamEvent
        'Room.StreamEvent',
        // 事件处理函数，参数类型为object
        function (data) {
            data = JSON.parse(data);
            if(data.event == 1){
            	player.play();
            }else{
            	player.pause();
            }
            //console.log(msg);
        }
    );
});
</script>
<?php } ?>
<input type="hidden" id="score1" value="0"/>
<input type="hidden" id="score2" value="0"/>
<input type="hidden" id="score3" value="0"/>
<input type="hidden" id="videoid" value="{{$oReview->id}}"/>
<input type="hidden" id="userid" value="{{$iUserId}}"/>
<script type="text/javascript">
	$('.evaluation .list01 span').click(function(){
		$('.evaluation .list01 span').unbind("click");
	});
	$('.evaluation .list02 span').click(function(){
    	$('.evaluation .list02 span').unbind("click");
    });
	$('.evaluation .list03 span').click(function(){
    	$('.evaluation .list03 span').unbind("click");
    });
</script>
<script type="text/javascript">
	$('.evaluation_box .evaluation .list span').click(function(){
    	console.log($(this).index())
    	$(this).index()
    	$(this).parent().find('.star').each(function(){
        	$(this).attr('src','/assets/images/weixin/icon_star2.png')
    	})
        for(var i=0 ; i<$(this).index();i++){
            $(this).parent().find('.star').eq(i).attr('src','/assets/images/weixin/icon_star1.png')
        }
    	$('#score1').val($(this).index());
    	docface_video_score(3);
    	$('.evaluation .list span').unbind("click");
    });
    $('.evaluation_box .evaluation .list2 span').click(function(){
    	console.log($(this).index())
    	$(this).index()
    	$(this).parent().find('.star').each(function(){
        	$(this).attr('src','/assets/images/weixin/icon_star2.png')
    	})
        for(var i=0 ; i<$(this).index();i++){
            $(this).parent().find('.star').eq(i).attr('src','/assets/images/weixin/icon_star1.png')
        }
    	$('#score2').val($(this).index());
    	docface_video_score(3);
    	$('.evaluation .list2 span').unbind("click");
    });
    $('.evaluation_box .evaluation .list3 span').click(function(){
    	console.log($(this).index())
    	$(this).index()
    	$(this).parent().find('.star').each(function(){
        	$(this).attr('src','/assets/images/weixin/icon_star2.png')
    	})
        for(var i=0 ; i<$(this).index();i++){
            $(this).parent().find('.star').eq(i).attr('src','/assets/images/weixin/icon_star1.png')
        }
    	$('#score3').val($(this).index());
    	docface_video_score(3);
    	$('.evaluation .list3 span').unbind("click");
    });
</script>
<script type="text/javascript">
	//统计录播观看时长加积分
	window.__flg = self.setInterval("addUserMinute({{$oReview->id}},{{$iUserId}})",180000);
	window.Media = document.getElementById("media"); 
	
	function addUserMinute(videoid,userid){
		if(window.Media.ended){
			self.clearInterval(window.__flg);
			return ;
		}
		var time_second = Math.round(window.Media.currentTime);
		var time_minute = Math.round(time_second/60);
		var url = "/docface/video_view/"+videoid+"/"+userid+"/"+time_minute+"/mobile"
		$.post(url,{},function(msg){
			if(msg=='success'){
				console.log('success');
			}
		})
	}
</script>
<script type="text/javascript">
function docface_video_score(device){
	var videoid = $('#videoid').val();
	var userid = $('#userid').val();
	var score1 = $('#score1').val();
	var score2 = $('#score2').val();
	var score3 = $('#score3').val();
	var url = '/docface/video_score';
	var data = {videoid:videoid,userid:userid,score1:score1,score2:score2,score3:score3,device:device};
	$.post(url,data,function(msg){
		if(msg == 'success'){
// 			alert('评分成功！');
			return;
		}else{
			alert('您已经点评过了！');
			return;
		}
	})
}
</script>
@stop
