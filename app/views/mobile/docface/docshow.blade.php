@extends('mobile.common.layout')
@section('title')空中课堂-国卫健康云 @stop
@section('description')空中课堂-国卫健康云@stop
@section('keywords')空中课堂-国卫健康云 @stop
@section('content')
    <?php if($type != 1){ ?>
	<div class="page_top_box">
    	<input type="button" class="btn_back" onclick="window.location.href='/mobile-docface'"/>
        <div class="page_top">专家风采</div>
        <input type="button" class="btn_share" style="visibility:hidden;"/>
    </div>
    <?php } ?>
    <div class="page_cont_box">
    	<div class="doc_details_box">
        	<div class="doc_details_top">
            	<div class="doc_pic">
                   	<img src="{{$oDoc->doc_thumb}}" />
                </div>
                <div class="doc_txt">
                	<p>{{$oDoc->doc_name}}</p>
                    <p>职称: {{$oDoc->doc_position}}</p>
                    <p>科室: {{$oDoc->doc_department}}</p>
                    <p>医院: {{$oDoc->doc_hospital}}</p>
                </div>
            </div>
            <div class="doc_details_cont">
            	<div class="tit">
                	专家简介
                </div>
                <div class="">
                	{{$oDoc->doc_introduction}}
                </div>
            </div>
        </div>
    </div>
@stop
