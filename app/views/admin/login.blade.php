<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap.css" rel="stylesheet">
<link href="/asset/css/font-awesome.css" rel="stylesheet">
<link href="/asset/css/custom.css" rel="stylesheet">
<script src="http://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.3.0/js/bootstrap.js"></script>
<script src="/asset/js/ueditor/ueditor.config.js" type="text/javascript"></script>
<script src="/asset/js/ueditor/ueditor.all.js" type="text/javascript"></script>
<title>国卫健康云后台登录</title>
</head>
<body>
<div>
	<div class="page-header">
	  <h1> <small>国卫健康云系统后台登录</small></h1>
	</div>
	<form class="form-horizontal" style="margin-top:200px;width:400px;margin-left:500px;" method="post" action="/admlogin">
	  <div class="form-group" style="">
	    <label for="user_nick" class="col-sm-2 control-label" style="width:100px;">用户名：</label>
	    <div class="col-sm-10" style="width:300px;">
	      <input type="text" class="form-control" id="user_nick" name="user_nick" placeholder="请输入用户名">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="password" class="col-sm-2 control-label" style="width:100px;">密码：</label>
	    <div class="col-sm-10" style="width:300px;">
	      <input type="password" class="form-control" id="password" name="password" placeholder="请输入密码">
	    </div>
	  </div>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default" style="margin-left:27px;">登录</button>
	    </div>
	  </div>
	</form>
</div>
</body>
</html>