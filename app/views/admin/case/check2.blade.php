@extends('admin.common.layout')
@section('content')
<style>
<!--
.form-group p select{margin-left:24px;}
-->
</style>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>病例审核</h3>
	</div>
	
	<form class="form-horizontal"  method="post" action="/admcase/check/{{$oCaseInfo->id}}">
	  <div class="form-group">
	    <h4><label class="col-sm-2 control-label">病例基本信息</label></h4>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">病例名称</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" value="{{$oCaseInfo->case_name}}" readonly/>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">医生姓名</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" value="{{$oCaseInfo->doctor_name}}" readonly/>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">电子邮箱</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" value="{{$oCaseInfo->email}}" readonly/>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">科　　室</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" value="{{$oCaseInfo->department}}" readonly/>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">医　　院</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" value="{{$oCaseInfo->hospital}}" readonly/>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">联系电话</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" value="{{$oCaseInfo->contact_num}}" readonly/>
	    </div>
	  </div>
	  <div class="form-group">
	    <h4><label class="col-sm-2 control-label">患者基本资料</label></h4>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">年　　龄</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" value="{{$oCaseInfo->patient_age}}" readonly/>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">性　　别</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" value="{{$oCaseInfo->patient_sex}}" readonly/>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label"></label>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">主　　诉</label>
	    <div class="col-sm-10">
	      <textarea class="form-control" readonly>{{$oCaseInfo->self_reported}}</textarea>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">现病史</label>
	    <div class="col-sm-10">
	      <textarea class="form-control" readonly>{{$oCaseInfo->hpi}}</textarea>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">既往史</label>
	    <div class="col-sm-10">
	      <textarea class="form-control" readonly>{{$oCaseInfo->pmh}}</textarea>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">实验室检查</label>
	    <div class="col-sm-10">
	      <textarea class="form-control" readonly>{{$oCaseInfo->lab_exam_text}}</textarea>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">辅助检查（24小时动态血压监测等）</label>
	    <div class="col-sm-10">
	      <textarea class="form-control" readonly>{{$oCaseInfo->sub_exam_text}}</textarea>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">诊　　断</label>
	    <div class="col-sm-10">
	      <textarea class="form-control" readonly>{{$oCaseInfo->diagnose}}</textarea>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">治　　疗</label>
	    <div class="col-sm-10">
	      <textarea class="form-control" readonly>{{$oCaseInfo->cure}}</textarea>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">随　　访</label>
	    <div class="col-sm-10">
	      <textarea class="form-control" readonly>{{$oCaseInfo->follow_view}}</textarea>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">请提供化验单等辅助检查报告复印件（实验室检查、心电图等）</label>
	    <div class="col-sm-10">
	      <img alt=""  src="{{$oCaseInfo->sub_exam_fileUrl1}}" width='400px;'/>
	      <img alt=""  src="{{$oCaseInfo->sub_exam_fileUrl2}}" width='400px;'/>
	      <img alt=""  src="{{$oCaseInfo->sub_exam_fileUrl3}}" width='400px;'/>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">请说明该病例的血压特点或疑难问题</label>
	    <div class="col-sm-10">
	      <textarea class="form-control" readonly>{{$oCaseInfo->dis_qustion}}</textarea>
	    </div>
	  </div>
	  <div class="form-group">
	    <h4><label class="col-sm-2 control-label">病例审核</label></h4>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">审核</label>
	    <div class="col-sm-10">
	      <input type="radio" name="status" value="0"   onclick="checkCase('0');" 	@if($oCaseInfo->status=='0') checked @endif />未审核
	      <input type="radio" name="status" value="1"   onclick="checkCase('1');"	@if($oCaseInfo->status=='1') checked @endif />审核中
	      <input type="radio" name="status" value="2"   onclick="checkCase('2');"	@if($oCaseInfo->status=='2') checked @endif />审核通过
	      <input type="radio" name="status" value="-1"  onclick="checkCase('-1');"	@if($oCaseInfo->status=='-1') checked @endif />审核未通过
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">病例亮点</label>
	    <div class="col-sm-10">
	      <textarea class="form-control" id="case_lightspot" name="case_lightspot" @if($oCaseInfo->status!='2') readonly @endif>{{$oCaseInfo->case_lightspot}}</textarea>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">审核未通过原因</label>
	    <div class="col-sm-10">
	      <textarea class="form-control" id="nopass_reason" name="nopass_reason" @if($oCaseInfo->status!='-1') readonly @endif>{{$oCaseInfo->nopass_reason}}</textarea>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_position" class="col-sm-2 control-label">参与投票</label>
	    <div class="col-sm-10">
			<input type="checkbox" name="voteable" id="voteable" value="1" @if($oCaseInfo->voteable == 1) checked @endif/>
	    </div>
	  </div>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">确定</button>
	    </div>
	  </div>
	</form>
<script>
//病例审核 
function checkCase(type){
	switch(type){
		case '0':
			$('#nopass_reason').val('');
			$('#case_lightspot').val('');
			$('#case_lightspot').attr('readonly',true);
			$('#nopass_reason').attr('readonly',true);
			break;
		case '1': 
			$('#nopass_reason').val('');
			$('#case_lightspot').val('');
			$('#case_lightspot').attr('readonly','true');
			$('#nopass_reason').attr('readonly','true');
			break;
		case '2':
			$('#case_lightspot').attr('readonly',false);
			$('#nopass_reason').val('');
			$('#nopass_reason').attr('readonly',true);
			break;
		case '-1':
			$('#case_lightspot').val('');
			$('#case_lightspot').attr('readonly',true);
			$('#nopass_reason').attr('readonly',false);
			break;
	}
}
</script>
@stop


