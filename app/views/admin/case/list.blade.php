@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>病例征集</h3>
	</div>
	<div>
	<?php 
	$oUserAdminCase2  = UserAdminCase::where('user_id',Auth::User()->id)->where('type',2)->first();
	?>
		@if(Auth::User()->role_id==1||$oUserAdminCase2)
		<div style="float:right;">
			<button type="button" class="btn btn-primary" onclick="$('#downzip').val(1);myform.submit();">下载在线上传病例</button>
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admcase/downAllCase'">导出全部病例</button>
		</div>
		@endif
	</div>
	<div>
	<form class="navbar-form navbar-left"  method="get" action="/admcase/list" name="myform">
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  <br><br>
	  <input type="hidden" name="downzip" value="0" id="downzip"/>
	  <div class="form-group">
	  	<span>病例名称：</span>
	    <input type="text" class="form-control" name="case_name" style="width:400px;" value="{{Input::get('case_name','')}}">
	  </div>
	  &nbsp;&nbsp;&nbsp;
	  <div class="form-group">
	  	<span>状态：</span>
	   	<select name="status" class="form-control">  
	   	  <option value ="4" selected = "selected" >=全部=</option> 
		  <option value="0" @if(Input::get('status')=='0')  selected = "selected" @endif>未审核</option>  
		  <option value="1" @if(Input::get('status')=='1')  selected = "selected" @endif>审核中</option>
		  <option value="2" @if(Input::get('status')=='2')  selected = "selected" @endif>审核通过</option> 
		  <option value="-1" @if(Input::get('status')=='-1')  selected = "selected" @endif>审核未通过</option> 
		</select> 
	  </div>
	  <div class="form-group">
	  	<span>上传方式：</span>
	   	<select name="uploadType" class="form-control">  
	   	  <option value="0" selected = "selected" >=全部=</option> 
		  <option value="1" @if(Input::get('uploadType')=='1')  selected = "selected" @endif>在线上传</option>
		  <option value="2" @if(Input::get('uploadType')=='2')  selected = "selected" @endif>在线填写</option> 
		</select> 
	  </div>
	  <div class="form-group">
	  	<span>参与投票：</span>
	   	<select name="voteable" class="form-control">  
	   	  <option value="2" selected = "selected" >=全部=</option> 
		  <option value="1" @if(Input::get('voteable')=='1')  selected = "selected" @endif>是</option>
		  <option value="0" @if(Input::get('voteable')=='0')  selected = "selected" @endif>否</option> 
		</select> 
	  </div>
	  <button type="button" class="btn btn-default" style="margin-left:77px;" onclick="$('#downzip').val(0);myform.submit();">检索</button>
	</form>
	</div>
	
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>病例名称</th>
          <th>上传者姓名</th>
          <th>上传者电话</th>
          <th>审核状态</th>
          <th>上传方式</th>
          <th>参与投票</th>
          <th>是否参与投票<th>
          <th>投票数</th>
          <th>上传时间</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oCase as $k=>$v)
        <tr>
          <td>{{$v->id}}</td>
          <td>{{$v->case_name}}</td>
          <td>{{$v->user_name}}</td>
          <td>{{$v->user_tel}}</td>
          <td>{{$statusArr[$v->status]}}</td>
          <td>{{$v->uploadType==1?'方式一：在线上传':'方式二：在线填写'}}</td>
          <td>@if($v->voteable==1) 是  @else 否  @endif</td>
          <td><a href="/admcase/change/{{$v->id}}">@if($v->voteable==1) 取消投票  @else 参与投票  @endif</a></td>
          <td></td>
          <td>{{$v->votes}}</td>
          <td>{{$v->created_at}}</td>
          <td>
			<a href="/admcase/check/{{$v->id}}">审核</a>
		  </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div>{{$oCase->appends($aLink)->links()}}</div>
@stop
