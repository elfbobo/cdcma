@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>e-learning--ppt</h3>
	</div>
	<div style="float:right;"><button type="button" class="btn btn-primary" onclick="window.location.href='/admelearning/ppt-add'">新增ppt</button></div>
	
	<table class="table table-striped">
      <thead>
        <tr>
          <th width="10%">id</th>
          <th width="40%">标题</th>
          <th width="20%">缩略图</th>
          <th width="15%">点击量</th>
          <th width="20%">操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oList as $k=>$v)
        <tr>
          <th scope="row">{{$v->id}}</th>
          <td>{{$v->ppt_title}}</td>
          <td><img src="{{$v->ppt_thumb}}" style="height:50px;"/></td>
          <td>{{$v->ppt_hits}}</td>
          <td>
          		<a href="/admelearning/ppt-edit/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
          		&nbsp;&nbsp;&nbsp;
          		<a href="#" onclick="delUser('/admelearning/ppt-del/{{$v->id}}')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oList->links()}}
    <script>
		function delUser(url){
			if(confirm('确定要删除该条记录吗？')){
				window.location.href = url;
			}
		}	
	</script>
@stop
