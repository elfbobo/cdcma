@extends('admin.common.layout')
@section('content')
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/elearning.js"></script>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>编辑视频</h3>
	</div>
	<br><br><br><br>
	<form class="form-horizontal"  method="post" action="/admelearning/video-edit-do/{{$oVideo->id}}">
	  <div class="form-group">
	    <label for="user_name" class="col-sm-2 control-label"><span style="color:red;">*</span>标题</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="video_title" name="video_title" value="{{$oVideo->video_title}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_nick" class="col-sm-2 control-label"><span style="color:red;">*</span>视频地址</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="video_url" name="video_url" value="{{$oVideo->video_url}}" >
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="upload_file" class="col-sm-2 control-label"><span style="color:red;">*</span>缩略图</label>
	    <div class="col-sm-10">
	    	<input  id="upload_file" type="file" name="upload_file" onchange="saveThumb()" />
	      	<input type="hidden" class="form-control" id="thumb_url" name="thumb_url" value="{{$oVideo->video_thumb}}">
	      	<img style="height:200px;" alt="" id="thumb" src="{{$oVideo->video_thumb}}"/>
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">确定</button>
	    </div>
	  </div>
	</form>
@stop


