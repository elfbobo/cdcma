@extends('admin.common.layout')
@section('content')
<!-- 配置文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="/assets/js/ueditor/lang/zh-cn/zh-cn.js"></script>
<!-- date -->
<link type="text/css" href="/assets/js/date/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-1.8.17.custom.min.js"></script>
<style type="text/css">
<!--
.checkboxs_choose_boxs label { padding-left:0px !important; }
.itemsBoxs { background:#F1F1F1;border-radius:8px;padding:10px 15px;font-size:16px;font-weight:bold;margin-bottom:20px; }
.txt-left { text-align:left !important;font-weight:20; }
-->
</style>
<!-- date -->
<div class="app_content_div" id="app_content_div_301Index" style="margin-bottom:20px;"><h3>系统设置</h3></div>
<form class="form-horizontal"  method="post" action="/admsys/">
	<input type="hidden" name="action" id="action" value="save">
	<div class="col-sm-12 itemsBoxs">视频录制时长<span style="color:red;">（注：达到或超过设定的时长时，视频自动通过审核。）</span></div>
	<div class="form-group">
		<label for="video_audit_times" class="col-sm-3 control-label"><span style="color:red;">*</span> 视频录制时长</label>
		<div class="col-sm-3">
			<input type="text" class="form-control col-sm-5" id="video_audit_times" name="video_audit_times" value="{{@$oSysinfo->video_audit_times?$oSysinfo->video_audit_times:20}}">
		</div>
		<label class="col-sm-2 control-label txt-left">分钟</label>
	</div>
	<div class="col-sm-12 itemsBoxs">国卫健康云积分规则</div>
	<div class="form-group">
		<label for="video_integral" class="col-sm-3 control-label"><span style="color:red;">*</span> 健康教育视频观看赠送</label>
		<div class="col-sm-3">
			<input type="text" class="form-control col-sm-5" id="video_integral" name="video_integral" value="{{@$oSysinfo->video_integral?$oSysinfo->video_integral:0}}">
		</div>
		<label class="col-sm-2 control-label txt-left">积分</label>
	</div>
	<div class="form-group">
		<label for="live_integral" class="col-sm-3 control-label"><span style="color:red;">*</span> 参会系统赠送</label>
		<div class="col-sm-3">
			<input type="text" class="form-control col-sm-5" id="live_integral" name="live_integral" value="{{@$oSysinfo->live_integral?$oSysinfo->live_integral:0}}">
		</div>
		<label class="col-sm-2 control-label txt-left">积分</label>
	</div>
	<div class="form-group">
		<label for="view_video_deduct_integral" class="col-sm-3 control-label"><span style="color:red;">*</span> 观看精品课程扣除</label>
		<div class="col-sm-3">
			<input type="text" class="form-control col-sm-5" id="view_video_deduct_integral" name="view_video_deduct_integral" value="{{@$oSysinfo->view_video_deduct_integral?$oSysinfo->view_video_deduct_integral:0}}">
		</div>
		<label class="col-sm-2 control-label txt-left">积分</label>
	</div>

	<div class="form-group">
		<label for="cmc_use_integral" class="col-sm-3 control-label"><span style="color:red;">*</span> 兑换好医生CME课程消耗</label>
		<div class="col-sm-3">
			<input type="text" class="form-control col-sm-5" id="cmc_use_integral" name="cmc_use_integral" value="{{@$oSysinfo->cmc_use_integral?$oSysinfo->cmc_use_integral:0}}">
		</div>
		<label class="col-sm-2 control-label txt-left">积分</label>
	</div>
	<div class="form-group">
		<label for="kypx_use_integral" class="col-sm-3 control-label"><span style="color:red;">*</span> 兑换科研培训消耗</label>
		<div class="col-sm-3">
			<input type="text" class="form-control col-sm-5" id="kypx_use_integral" name="kypx_use_integral" value="{{@$oSysinfo->kypx_use_integral?$oSysinfo->kypx_use_integral:0}}">
		</div>
		<label class="col-sm-2 control-label txt-left">积分</label>
	</div>
	<div class="form-group">
		<label for="integral_tel" class="col-sm-3 control-label"><span style="color:red;">*</span> 热线电话</label>
		<div class="col-sm-3">
			<input type="text" class="form-control" id="integral_tel" name="integral_tel" value="{{@$oSysinfo->integral_tel?$oSysinfo->integral_tel:'010-85879900-8067'}}">
		</div>
	</div>
	<div class="form-group">
		<label for="exchange_rule_integral" class="col-sm-3 control-label"><span style="color:red;">*</span> 积分兑换规则说明</label>
		<div class="col-sm-4"><textarea class="form-control" id="exchange_rule_integral" name="exchange_rule_integral">{{@$oSysinfo->exchange_rule_integral}}</textarea></div>
	</div>
	<div class="col-sm-12 itemsBoxs">系统协议内容</div>
	<!-- <div class="form-group">
		<label for="video_title" class="col-sm-2 control-label"><span style="color:red;">*</span> 讲师讲课协议</label>
		<div class="col-sm-7"><textarea class="form-control" id="teach_agreement" style="height:220px;" name="teach_agreement">{{@$oSysinfo->teach_agreement}}</textarea></div>
		<label class="col-sm-2 control-label txt-left">讲师报名成功后，参与者满6人（含讲者），提醒讲者签署协议。</label>
	</div>
	<div class="form-group">
		<label for="video_title" class="col-sm-2 control-label"><span style="color:red;">*</span> 讲师视频观看协议</label>
		<div class="col-sm-7"><textarea class="form-control" id="view_teacher_video_agreement" style="height:220px;" name="view_teacher_video_agreement">{{@$oSysinfo->view_teacher_video_agreement}}</textarea></div>
		<label class="col-sm-2 control-label txt-left">讲师录制的视频发布并审核通过后，累计观看人数达到10人，提醒讲者签署协议。</label>
	</div> -->
	<div class="form-group">
		<label for="video_title" class="col-sm-2 control-label"><span style="color:red;">*</span> 医生认证协议</label>
		<div class="col-sm-7"><textarea class="form-control" id="doc_agreement" style="height:220px;" name="doc_agreement">{{@$oSysinfo->doc_agreement}}</textarea></div>
		<label class="col-sm-2 control-label txt-left">医生提交认证签，签署的医生认证协议。</label>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-7"><button type="submit" class="btn btn-default">确定</button></div>
	</div>
</form>
@stop


