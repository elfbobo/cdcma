@extends('admin.common.layout')
@section('content')
<script src="{{ asset('assets/js/wxueditor/ueditor.config.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/js/wxueditor/ueditor.all.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/js/ajaxfileupload.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/js/information.js')}}" type="text/javascript"></script>
<div class="app_content_div" id="app_content_div_301Index">
	<h3>编辑资讯</h3>
	<h6>
	<span style="color:red;">注：红色*为必填项，其他不作要求；内容一项仅限上传jpg和png格式的图片；如果修改内容时图片无法上传，更新缩略图封面再重试即可</span>
	</h6>
</div>
<form class="form-horizontal" action="/admmaterial/edit-do/{{$oMaterialNews->id}}" method="post" id="myform">
		<div class="form-group">
			<label class="col-sm-2 control-label"><span style="color:red;">*</span>资讯标题</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" style="width:70%;" id="title" name="title" value="{{$oNews->title}}">
		    </div>
		</div>
		<div class="form-group">
		    <label for="file_upload" class="col-sm-2 control-label"><span style="color:red;">*</span>缩略图/封面</label>
		    <div class="col-sm-10">
		    	<input  id="file_upload" name="file_upload"  type="file" onchange="saveMaterialImage({{$oNews->news_type}})"/>
		      	<input type="hidden" class="material_id" name="material_id" id="material_id" value="{{$oNews->material_id}}">
				<input type="hidden" class="media_id" name="media_id" id="media_id" value="{{$oNews->media_id}}">
				<img style="width:200px;height:140px;" style="display:block;" src="{{$oNews->path}}" id="thumb">
		    </div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label"><span style="color:red;">*</span>简介</label>
		    <div class="col-sm-10">
		    	<textarea name="digest" id="digest" style="width:550px;height:85px;">{{$oNews->digest}}</textarea>
		    </div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">作者</label>
		    <div class="col-sm-10">
		    	<input type="text" name="author" id="author" style="width:280px;" value="{{$oNews->author}}"/>
		    </div>
		</div>
		<div class="form-group" id="show_cover_pic">
			<label class="col-sm-2 control-label">正文中显示图片：</label>
		    <div class="col-sm-10">
		    	<input type="radio" name="show_cover_pic" style="vertical-align: middle;margin:0 10px;" value="0" @if($oNews->show_cover_pic==0) checked @endif/>否
				<input type="radio" name="show_cover_pic" style="vertical-align: middle;margin:0 10px;" value="1" @if($oNews->show_cover_pic==1) checked @endif/>是
		    </div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">原文链接：</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" style="width:70%;" id="content_source_url" name="content_source_url" value="{{$oNews->content_source_url}}">
		    </div>
		</div>
		<div class="form-group">
	    	<label class="col-sm-2 control-label"><span style="color:red;">*</span>内容：</label>
	    	<div class="col-sm-10">
				<textarea class="" style="margin-left:60px;" name="content0" id="content0">{{$oNews->content}}</textarea>
				<script type="text/javascript">
					UE.getEditor("content0",{
						zIndex : 1,
						toolbars: [
								   ["source",
									"undo",
								    "redo",
							        "bold",
							        "italic",
									"forecolor",
									"simpleupload",
									"insertimage",
									"link",
									"fontfamily",
									"fontsize",
									//"emotion",';
									"|",
									"attachment",
									"justifyleft",
									"justifyright",
									"justifycenter",
									"justifyjustify",
									"charts",
									]],
									autoFloatEnabled:true,
									autoHeightEnabled:false, 
									scaleEnabled:false,           
									initialFrameWidth : 600,
									initialFrameHeight: 290,
									compressSide:1
					})
				</script>
		    </div>
		</div>
		<div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <button type="submit" class="btn btn-default" onclick="check();" isClick="false">确定</button>
		    </div>
		</div> 
</form>
<script>
	function check(){
		var title = $("#title").val();
		var material_id = $("#material_id").val();
		var digest = $("#digest").val();
		var content0 = $("#content0").val();
		
		flag = true;
		if(!$.trim(title)){
			alert('图文标题不能为空');
			flag = false;
			return false;
		}
		if(!material_id){
			alert('图文封面不能为空');
			flag = false;
			return false;
		}
		if(!$.trim(digest)){
			alert('图文简介不能为空');
			flag = false;
			return false;
		}
// 		if(!content0){
// 			alert('图文内容不能为空');
// 			flag = false;
// 			return false;
// 		}
		if(!flag)return false;
		$('#myform').submit();
	}
</script>
@stop