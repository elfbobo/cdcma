@extends('admin.common.layout')
@section('content')
<script src="{{ asset('assets/js/blockui/jquery.blockUI.js') }}"></script>
<div class="app_content_div" id="app_content_div_301Index">
	@if($type == 1)
	<h3>慢性疾病与转化医学资讯列表</h3>
	@elseif($type == 2)
	<h3>心内空间资讯列表</h3>
	@elseif($type == 3)
	<h3>呼吸科空间资讯列表</h3>
	@endif
</div>
<form action="/admmaterial/list/{{$type}}" method="get">
	 资讯标题:<input type="text" id="title" name="title">
	<button type="submit">搜索</button>
</form>
<br></br>
<p style="color:red;">注意：图文素材有3天的有效期，过期后要重新发布;</br>
为防止异常，认证订阅号在一天内，只能对全体用户群发一次，</br>
或者在公众平台官网群发（不管本次群发是对全体还是对某个分组）一次。</br>
以避免一天内有2条群发进入历史消息列表;</p>
<div style="float:right;">
	<button type="button" class="btn btn-primary" onclick="window.location.href='/admmaterial/add/{{$type}}'">新增资讯</button>
</div>
	<table class="table table-striped">
    	<thead>
    	<tr>
        	<th>序号</th>
            <th>标题</th>
            <th>所属板块</th>
            <th>图片素材id</th>
            <th>更新时间</th>
            <th>修改</th>
            <th>删除</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        @foreach($oMaterialNews as $k=>$v)
        <tr>
        	<td>{{$v->id}}</td>
            <td>{{$v->title}}</td>
            <td>
            @if($v->news_type == 1)
            	慢性疾病与转化医学
            @elseif($v->news_type == 2)
            	心内空间
            @elseif($v->news_type == 3)
            	呼吸科空间
            @endif
            </td>
            <td>{{$v->media_id}}</td>
            <td>{{$v->updated_at}}</td>
            <td><a href="/admmaterial/edit/{{$v->id}}"><img src="/assets/images/admin/icon2.jpg" /></a></td>
            <td>
	            <a href="/admmaterial/delete/{{$v->id}}" onclick="return confirm('确定删除吗');">
	            <img src="/assets/images/admin/icon3.jpg"/>
	            </a>
            </td>
            <td>
				<a href="/admmaterial/preview/{{$v->id}}/{{$v->news_type}}">
                	<input type="button" value="预览">
                </a> | 
                <a href="#" onclick="confirm_push('{{$v->id}}','{{$v->news_type}}')">
                	<input type="button" value="群发" id="sub_button" isClick="false">
                </a>
            </td>
        </tr>
        @endforeach
        </tbody>
	</table>
    {{$oMaterialNews->links()}}
<script>
		function confirm_push(id,type){
			if(confirm('确定要向所有用户推送消息吗？')){
				
				var obj = $("#sub_button");
				if(obj.attr('isClick')=='true'){
					return false;
				}
				obj.attr('isClick','true')
				var url = '/admmaterial/push';
				$.post(url,{id:id,type:type},function(data){
					if(data.success){
						alert('推送成功');
						obj.attr('isClick','false')
						window.location.href='/admmaterial/list/'+type;
						return false;
					}else{
						alert(data.msg);
						obj.attr('isClick','false')
						return false;
					}
				},'json');
			}
		}	
</script>
@stop