@extends('admin.common.layout')
@section('content')
<div class="app_content_div" id="app_content_div_301Index">
	@if($iType == 1)
	<h3>慢性疾病与转化医学</h3>
	@elseif($iType == 2)
	<h3>心内空间</h3>
	@elseif($iType == 3)
	<h3>呼吸科空间</h3>
	@endif
</div>
<div class="content">
	<div class="course_manage mb20">
    	<div  class="checked_box_right">
        	<div class="div" style="clear:both;">
		    	<span style="color:red;">请填写接收预览消息的微信号：</span><br></br>
		        <input type="text" name="wxnumber" id="wxnumber" style="width:280px;" />
		    </div>
		</div>
		<div class="clear"></div>   	
	    <input class="btn" type="button" value="预览" id="sub_button" onclick="confirm_preview('{{$iId}}','{{$iType}}')" isClick="false"/>
	</div>
</div>
<script>
		function confirm_preview(id,type){
			var wxnumber = $("#wxnumber").val();
			if(!$.trim(wxnumber)){
				alert('请填写微信号');
				return false;
			}
			var obj = $("#sub_button");
			if(obj.attr('isClick')=='true'){
				return false;
			}
			obj.attr('isClick','true')
			var url = '/admmaterial/preview-do';
			$.post(url,{id:id,type:type,wxnumber:wxnumber},function(data){
				if(data.success){
					alert('请在微信预览');
					obj.attr('isClick','false')
					window.location.href='/admmaterial/list/'+type;
					return false;
				}else{
					alert(data.msg);
					obj.attr('isClick','false')
					return false;
				}
			},'json');
		}	
</script>
@stop