@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>分组列表</h3>
	</div>
	<div>
	</div>
	<div>
		<div style="float:right;">
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admuser/add-group'">添加组</button>
		</div>
	</div>
	
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>分组名称</th>
          <th>邀请注册链接</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oUsers as $k=>$v)
        <tr>
          <th scope="row">{{$v->id}}</th>
          <td>{{$v->user_name}}</td>
          <td>http://{{ $_SERVER['SERVER_NAME']}}/mobile/register/{{$v->invite_code}}</td>
          <td>
          		<a href="/admuser/edit-group/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
          		&nbsp;&nbsp;&nbsp;
          		<a href="#" onclick="delUser('/admuser/del-group/{{$v->id}}')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <script>
		function delUser(url){
			if(confirm('确定要删除当前组吗？')){
				window.location.href = url;
			}
		}
	</script>
@stop
