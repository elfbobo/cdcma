@extends('admin.common.layouts')
@section('content')
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>用户名</th>
          <th>注册编码</th>
          <th>角色</th>
          <th>CWID</th>
          <th>用户姓名</th>
          <th>手机号码</th>
          <th>邮箱</th>
          <th>所属代表姓名</th>
          <th>所属代表编码</th>
          <th>讲课费用</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oUsers as $k=>$v)
        <tr>
          <th scope="row">{{$v->id}}</th>
          <td>{{$v->user_nick}}</td>
          <td>{{$v->invite_code}}</td>
          <td>{{$aRole[$v->role_id]}}</td>
          <td>{{$v->user_cwid}}</td>
          <td>{{$v->user_name}}</td>
          <td>{{$v->user_tel}}</td>
          <td>{{$v->user_email}}</td>
          <td>@if(!empty($aRepName[$v->link_rep_id])) {{$aRepName[$v->link_rep_id]}} @endif</td>
          <td>@if(!empty($aRepCode[$v->link_rep_id])) {{$aRepCode[$v->link_rep_id]}} @endif</td>
          <td>{{$v->user_fee}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
@stop
