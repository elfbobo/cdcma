@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>线下会议列表</h3>
	</div>
	<div>
		<div style="float:right;">
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admuser/offline-add'">添加线下会议</button>
		</div>
	</div>
	
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>签到地址</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oOffline as $k=>$v)
        <tr>
          <th scope="row">{{$v->meeting_id}}</th>
          <td>http://{{ $_SERVER['SERVER_NAME']}}/docface/survey-pre/{{$v->meeting_id}}</td>
          <td>
          		<a href="javascript:void(0)" onclick="window.location.href='/admdocface/sign-in-log-offline/{{$v->meeting_id}}'">导出签到记录</a></span>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oOffline->links()}}
@stop
