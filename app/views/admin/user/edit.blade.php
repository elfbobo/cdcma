@extends('admin.common.layout')
@section('content')
<style>
<!--
.form-group p select{margin-left:24px;}
-->
</style>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>编辑用户</h3>
	</div>
	
	<form class="form-horizontal"  method="post" action="/admuser/edit-do/{{$oUser->id}}">
	  <!--<div class="form-group">
	    <label for="role_id" class="col-sm-2 control-label"><span style="color:red;">*</span>角色</label>
	    <div class="col-sm-10">
	      	<label class="radio-inline">
			  <input type="radio" name="role_id" id="role_id1" value="2" @if($oUser->role_id==2) checked @endif> 代表
			</label>
			<label class="radio-inline">
			  <input type="radio" name="role_id" id="role_id2" value="3" @if($oUser->role_id==3) checked @endif> 医生
			</label>
	    </div>
	  </div>
	  -->
	  <div class="form-group">
	    <label for="user_cwid" class="col-sm-2 control-label">CWID(<span style="color:red;">若角色为代表请填写该字段</span>)</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="user_cwid" name="user_cwid" value="{{$oUser->user_cwid}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_name" class="col-sm-2 control-label"><span style="color:red;">*</span>姓名</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="user_name" name="user_name" value="{{$oUser->user_name}}" >
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_nick" class="col-sm-2 control-label"><span style="color:red;">*</span>用户名</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="user_nick" name="user_nick"  value="{{$oUser->user_nick}}" >
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_fee" class="col-sm-2 control-label">讲课费用</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" style="width:300px;display:inline-block;" id="user_fee" name="user_fee"  value="{{$oUser->user_fee}}" > 元
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_sex" class="col-sm-2 control-label">性别</label>
	    <div class="col-sm-10">
	      	<label class="radio-inline">
			  <input type="radio" name="user_sex" id="user_sex1" value="1" @if($oUser->user_sex==1) checked @endif> 男
			</label>
			<label class="radio-inline">
			  <input type="radio" name="user_sex" id="user_sex2" value="2" @if($oUser->user_sex==2) checked @endif> 女
			</label>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_tel" class="col-sm-2 control-label"><span style="color:red;">*</span>手机</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="user_tel" name="user_tel"  value="{{$oUser->user_tel}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_email" class="col-sm-2 control-label"><span style="color:red;">*</span>邮箱</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="user_email" name="user_email"  value="{{$oUser->user_email}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_address" class="col-sm-2 control-label">地址</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="user_address" name="user_address"  value="{{$oUser->user_address}}">
	    </div>
	  </div>
	  <!--<div class="form-group">
	    <label for="user_city" class="col-sm-2 control-label"><span style="color:red;">*</span>城市</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="user_city" name="user_city"  value="{{$oUser->user_city}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_department" class="col-sm-2 control-label"><span style="color:red;">*</span>科室</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="user_department" name="user_department"  value="{{$oUser->user_department}}">
	    </div>
	  </div>
	  --><div class="form-group">
	    <label for="user_position" class="col-sm-2 control-label">职称</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="user_position" name="user_position"  value="{{$oUser->user_position}}">
	    </div>
	  </div><!--
	  <div class="form-group">
	    <label for="user_company" class="col-sm-2 control-label"><span style="color:red;">*</span>单位</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="user_company" name="user_company"  value="{{$oUser->user_company}}">
	    </div>
	  </div>
	  -->
	  <input type="hidden" id="user_province" name="user_province" value="{{$oUser->user_province}}"/>
      <input type="hidden" id="user_city" name="user_city" value="{{$oUser->user_city}}"/> 
      <input type="hidden" id="user_county" name="user_county" value="{{$oUser->user_county}}"/>
      <input type="hidden" id="user_company" name="user_company" value="{{$oUser->user_company}}"/>   
      <?php 
      $aHosp = Hospital::getChildren();
      ?>
	  @if($oUser->role_id==3)
	  <div class="form-group" style="margin-left:135px;">
		<p>
    	<label><span style="color:red;">*</span>省份</label>
        	<select id="hospital1" style="width:150px;margin-top:5px;" onchange="change_hospital(2)">
	            <option value="0">=请选择=</option>
	            @foreach($aHosp as $k=>$v)
	            <option value="{{$k}}" @if($k==$oUser->user_province) selected @endif>{{$v}}</option>
	            @endforeach
	        </select>
        </p>
        <p>
            	<label><span style="color:red;">*</span>城市</label>
            	<select id="hospital2" style="width:150px;margin-top:5px;" onchange="change_hospital(3)">
	            <option value="0">=请选择=</option>
	           <?php 
	            if($oUser->user_province!=0){
	            	$aCity = Hospital::getChildren($oUser->user_province);
	            }else{
	            	$aCity = array();
	            }
	            	?>
	            @foreach($aCity as $k=>$v)
	            <option value="{{$k}}" @if($k==$oUser->user_city) selected @endif>{{$v}}</option>
	            @endforeach
	            </select>
        </p>
        <p>
            	<label><span style="color:red;">*</span>区/县</label>
            	<select id="hospital3" style="width:150px;margin-top:5px;" onchange="change_hospital(4)">
	            <option value="0">=请选择=</option>
	             <?php $aCounty = Hospital::getChildren($oUser->user_city);?>
	            @foreach($aCounty as $k=>$v)
	            <option value="{{$k}}" @if($k==$oUser->user_county) selected @endif>{{$v}}</option>
	            @endforeach
	            </select>
        </p>
        <p style="width:400px;">
        	<label><span style="color:red;">*</span>医院</label>
        	<select id="hospital4" style="width:150px;margin-top:5px;" onchange="change_hospital(5)">
	            <option value="0">=请选择=</option>
	            <?php $aHospital = Hospital::getChildren($oUser->user_county);?>
	            @foreach($aHospital as $k=>$v)
	            <option value="{{$k}}" @if($k==$oUser->user_company) selected @endif>{{$v}}</option>
	            @endforeach
	        </select>
	        若没有，请<a style="cursor:pointer;color:#254797;" onclick="$('#user_company_div').show();">手动填写</a>
        </p>
        <p style="display:none;" id="user_company_div">
        	<label></label>
        	<input type="text" id="user_company_name" name="user_company_name" value="{{$oUser->user_company_name}}"/>  
        </p>
                                <?php 
    	$aDep = User::getDepartment();
    ?>
        <p>
        	<label><span style="color:red;">*</span>科室</label>
        	<select id="user_dep_select" style="width:150px;margin-top:5px;" onchange="$('#user_department').val($('#user_dep_select  option:selected').val());">
            <option value="0">=请选择=</option>
            @foreach($aDep as $k=>$v)
            <option value="{{$v}}" @if($v==$oUser->user_department) selected @endif>{{$v}}</option>
            @endforeach
            </select>
            <input type="hidden" name="user_department" id="user_department" value="{{$oUser->user_department}}"/> 
        </p>
        </div>
@else
<div class="form-group" style="margin-left:135px;">
<p>
    	<label><span style="color:red;">*</span>省份</label>
        	<select id="hospital21" style="width:150px;margin-top:5px;" onchange="change_hospital2(2)">
	            <option value="0">=请选择=</option>
	            @foreach($aHosp as $k=>$v)
	            <option value="{{$k}}" @if($k==$oUser->user_province) selected @endif>{{$v}}</option>
	            @endforeach
	        </select>
        </p>
        <p>
            	<label><span style="color:red;">*</span>城市</label>
            	<select id="hospital22" style="width:150px;margin-top:5px;" onchange="change_hospital2(3)">
	            <option value="0">=请选择=</option>
	            <?php 
	            if($oUser->user_province!=0){
	            	$aCity = Hospital::getChildren($oUser->user_province);
	            }else{
	            	$aCity = array();
	            }
	            	?>
	            @foreach($aCity as $k=>$v)
	            <option value="{{$k}}" @if($k==$oUser->user_city) selected @endif>{{$v}}</option>
	            @endforeach
	            </select>
        </p>
        <p>
        <?php 
		$sRegin = User::getReginCache();
		$aRegin = json_decode($sRegin);
		?>
        	<label><span style="color:red;">*</span>大区</label>
        	<select id="regin" style="width:150px;margin-top:5px;" onchange="change_regin()">
            <option value="0">=请选择=</option>
            @foreach($aRegin as $k=>$v)
            <option value="{{$k}}" @if($k==$oUser->user_regin) selected @endif>{{$v}}</option>
            @endforeach
            </select>
            <input type="hidden" name="user_regin" id="user_regin" value="{{$oUser->user_regin}}"/>  
        </p>
        <p>
        	<label><span style="color:red;">*</span>地区</label>
        	<select id="area" style="width:150px;margin-top:5px;" onchange="$('#user_area').val($('#area  option:selected').val());">
            <option value="0">=请选择=</option>
             <?php 
	            	$aArea = DB::table('area')->where('regin_id',$oUser->user_regin)->lists('area_name','id');
	            	?>
	            @foreach($aArea as $k=>$v)
	            <option value="{{$k}}" @if($k==$oUser->user_area) selected @endif>{{$v}}</option>
	            @endforeach
            </select>
        	<input type="hidden" name="user_area" id="user_area" value="{{$oUser->user_area}}"/>  
        </p>
	  </div>
	  @endif
	  <div class="form-group">
	    <label class="col-sm-2 control-label"><span style="color:red;"></span><span style="color:red;">非目标</span>用户邀请注册链接</label>
	    <div class="col-sm-10">
	      <input type="text" disabled class="form-control" value="@if(empty($oUser->invite_code)) 无  @else http://cdcma.bizconf.cn/mobile/register/{{$oUser->invite_code}}/2 @endif" >
	    </div>
	  </div>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">确定</button>
	    </div>
	  </div>
	</form>
@stop


