@extends('admin.common.layout')
@section('content')
<div class="app_content_div">
          <h2 class="sub-header">相关数据及缓存</h2>
          <div class="table-responsive">
          <table class="table table-striped">
           </table>
            <table class="table table-striped">
              <thead>
              </thead>
              <tbody>
              
              	<tr>
                  	<td>清除整站缓存</td>
                	<td><a href="/flush-all"><button type="button" class="btn btn-primary">点击清除</button></a></td>
                </tr>
                <tr>
                  	<td>更新大区缓存</td>
                	<td><a href="/admuser/update-regin"><button type="button" class="btn btn-primary">点击更新</button></a></td>
                </tr>
                <tr>
                  	<td>更新地区缓存</td>
                	<td><a href="/admuser/update-area"><button type="button" class="btn btn-primary">点击更新</button></a></td>
                </tr>
                <tr>
                  	<td>更新医院缓存</td>
                	<td><a href="/admuser/update-hospital"><button type="button" class="btn btn-primary">点击更新</button></a></td>
                </tr>
                <tr>
                  	<td>更新代表的大区地区id</td>
                	<td><a href="/admuser/update-user-regin-area"><button type="button" class="btn btn-primary">点击进入</button></a></td>
                </tr>
                <tr>
                  	<td>专家大会诊相关用户信息导出</td>
                	<td><a href="/admexpert/userinfo"><button type="button" class="btn btn-primary">导出</button></a></td>
                </tr>
                
                <tr>
                  	<td>西区ME用户积分情况导出</td>
                	<td><a href="/admuser/xqmeexpory"><button type="button" class="btn btn-primary">导出</button></a></td>
                </tr>
                
                <tr>
                  	<td>更新医生用户关联代表--20160307</td>
                	<td><a href="/admuser/update-doc-link-rep"><button type="button" class="btn btn-primary">点击进入</button></a></td>
                </tr>
                
                <tr style="display: none;">
                  	<td>为未关联代表id的医生用户关联上代表id（有问题咨询开发）</td>
                	<td><a href="/admuser/user_rep_relate"><button type="button" class="btn btn-primary">确定</button></a></td>
                </tr>
                
                <tr style="display: none;">
                  	<td>本次上线第一步：为已导入医脉通的用户关联医脉通id--20151223</td>
                	<td><a href="/admuser/user_medlive_relate"><button type="button" class="btn btn-primary">确定</button></a></td>
                </tr>
                <tr style="display: none;">
                  	<td>本次上线第二步：为未关联医脉通id的用户关联医脉通id--20151223</td>
                	<td><a href="/admuser/user_medlive_reg"><button type="button" class="btn btn-primary">确定</button></a></td>
                </tr>
                <!--  
                <tr>
                  	<td>线下会议签到记录-20161115</td>
                	<td><a href="/admuser/offline-list"><button type="button" class="btn btn-primary">点击进入</button></a></td>
                </tr>
                -->
                <?php 
                $num = 1;
                $oLog = OfflineSignInLog::orderby('meeting_code','desc')->first();
                if($oLog && $oLog->meeting_code != 0){
                	$num = $oLog->meeting_code;
                	$num = $num+1;
                }
                ?>
                <tr>
                  	<td>线下会议签到记录-20170502   <span style="color:red;">注：线下会议签到地址最新可用的为：http://cdcma.bizconf.cn/docface/offline-sign/{{$num}}</span></td>
                	<td><a href="/admdocface/export-offline-sign-in-log"><button type="button" class="btn btn-primary">导出</button></a></td>
                </tr>
                <tr>
                  	<td>移动端用户直播录播次数-20170102</td>
                	<td><a href="/admadmdocface/h5user-log"><button type="button" class="btn btn-primary">点击进入</button></a></td>
                </tr>
                <tr>
                  	<td>更新商务标签用户数据</td>
                	<td><a href="/admuser/update-shangwu-user"><button type="button" class="btn btn-primary">点击更新</button></a></td>
                </tr>
              </tbody>
            </table>
          </div>
</div>
@stop


