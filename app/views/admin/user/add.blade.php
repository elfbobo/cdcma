@extends('admin.common.layout')
@section('content')
<style>
<!--
.form-group p select{margin-left:24px;}
-->
</style>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>新增用户（由于用户积分关联问题，新增用户只能新增代表）</h3>
	</div>
	
	<form class="form-horizontal"  method="post" action="/admuser/add-do">
	<!--<div class="form-group">
	    <label for="role_id" class="col-sm-2 control-label"><span style="color:red;">*</span>角色</label>
	    <div class="col-sm-10">
	      	<label class="radio-inline">
			  <input type="radio" name="role_id" id="role_id1" value="2" checked> 代表
			</label>
			<label class="radio-inline">
			  <input type="radio" name="role_id" id="role_id2" value="3"> 医生
			</label>
	    </div>
	  </div>
	  -->
	  <div class="form-group">
	    <label for="user_cwid" class="col-sm-2 control-label">CWID</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="user_cwid" name="user_cwid" >
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_name" class="col-sm-2 control-label"><span style="color:red;">*</span>姓名</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="user_name" name="user_name" >
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_nick" class="col-sm-2 control-label"><span style="color:red;">*</span>用户名</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="user_nick" name="user_nick" >
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_fee" class="col-sm-2 control-label">讲课费用</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" style="width:300px;display:inline-block;" id="user_fee" name="user_fee" > 元
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_sex" class="col-sm-2 control-label">性别</label>
	    <div class="col-sm-10">
	      	<label class="radio-inline">
			  <input type="radio" name="user_sex" id="user_sex1" value="1" checked> 男
			</label>
			<label class="radio-inline">
			  <input type="radio" name="user_sex" id="user_sex2" value="2"> 女
			</label>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_tel" class="col-sm-2 control-label"><span style="color:red;">*</span>手机</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="user_tel" name="user_tel" >
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_email" class="col-sm-2 control-label"><span style="color:red;">*</span>邮箱</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="user_email" name="user_email" >
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_address" class="col-sm-2 control-label">地址</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="user_address" name="user_address" >
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_position" class="col-sm-2 control-label">职称</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="user_position" name="user_position" >
	    </div>
	  </div>
		<input type="hidden" id="user_province" name="user_province" value="0"/>
		<input type="hidden" id="user_city" name="user_city" value="0"/> 
		<input type="hidden" id="user_county" name="user_county" value="0"/>
		<input type="hidden" id="user_company" name="user_company" value="0"/> 
		<?php 
		$aHosp = Hospital::getChildren();
		?>
		<div class="form-group">
			<label for="hospital21" class="col-sm-2 control-label"><span style="color:red;">*</span>省份</label>
			<div class="col-sm-10">
				<select id="hospital21" class="form-control" style="width:150px;margin-top:5px;" onchange="change_hospital2(2)">
		            <option value="0">=请选择=</option>
		            @foreach($aHosp as $k=>$v)
		            <option value="{{$k}}">{{$v}}</option>
		            @endforeach
		        </select>
			</div>
		</div>
		<div class="form-group">
			<label for="hospital22" class="col-sm-2 control-label"><span style="color:red;">*</span>城市</label>
			<div class="col-sm-10">
				<select id="hospital22" class="form-control" style="width:150px;margin-top:5px;" onchange="change_hospital2(3)">
	            	<option value="0">=请选择=</option>
	            </select>
			</div>
		</div>
        <?php 
		$sRegin = User::getReginCache();
		$aRegin = json_decode($sRegin);
		?>
		<div class="form-group">
			<label for="regin" class="col-sm-2 control-label"><span style="color:red;">*</span>大区</label>
			<div class="col-sm-10">
				<select id="regin" class="form-control" style="width:150px;margin-top:5px;" onchange="change_regin()">
	            	<option value="0">=请选择=</option>
		            @foreach($aRegin as $k=>$v)
		            <option value="{{$k}}" >{{$v}}</option>
		            @endforeach
	            </select>
	            <input type="hidden" name="user_regin" id="user_regin" value=""/>  
			</div>
		</div>
		<div class="form-group">
			<label for="area" class="col-sm-2 control-label"><span style="color:red;">*</span>地区</label>
			<div class="col-sm-10">
				<select id="area" class="form-control" style="width:150px;margin-top:5px;" onchange="$('#user_area').val($('#area  option:selected').val());">
	            	<option value="0">=请选择=</option>
	            </select>
	        	<input type="hidden" name="user_area" id="user_area" value=""/> 
			</div>
		</div>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">确定</button>
	    </div>
	  </div>
	</form>
@stop


