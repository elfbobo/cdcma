@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>导入用户(只能导入代表)</h3>
	</div>
	<div class="page_center_right right">
        <div class="recommend_pd">
	        
	        <form id="form1" name="form1" enctype="multipart/form-data" method="post" action="/admuser/import-do">
	        <div class="Navigation_subtext">
				<table width="98%" border="0" align="center" style="margin-top:20px; border:1px solid #9abcde;">
				   <tr>
				      <td width="18%" height="50">&nbsp;选择你要导入的数据表</td>
				      <td width="82%">
				        <label>
				             <input name="file" type="file" id="file" size="50" />
				        </label>
				        <label>
				             <input name="button" type="submit" class="nnt_submit" id="button" value="开始导入" />
				        </label>&nbsp;
				      </td>
				    </tr>
				</div>
			</form>
				    
				    <tr>
				    <td colspan="2">
				    <label style="color: red">
				   注意：导入文件格式必须为：csv<br>
				   <strong>第一行必须如模板所示。</strong>从第二行开始为要导入的数据(*标记的字段必填)<br>
				    </label>
				    <br>
				    <a href="/assets/template/bayer.csv">模板下载</a>
				    </td>
				    </tr>
				</table>
			</div>
	        
        </div>
      </div>
	
@stop


