@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>更新医生关联代表信息</h3>
	</div>
	<div class="page_center_right right">
        <div class="recommend_pd">
        	<div class="form-group">
			    <label for="user_cwid" class="col-sm-2 control-label">现代表id</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="rep_id" name="rep_id" >
			    </div>
			</div>
			<br><br><br>
			<div class="form-group">
			    <label class="col-sm-2 control-label">医生/原代表id<br><span style="color:red">(多个请用英文逗号分割)</span></label>
			    <div class="col-sm-10">
			      <textarea id="doc_ids" name="doc_ids" class="form-control" >
			      </textarea>
			      <br>
			      <span style="color:red">注：您填写的医生/代表邀请的所有医生都会归属到您填写的代表，请谨慎填写并做好数据备份</span>
			    </div>
			</div>
			
			<div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="button" class="btn btn-default" onclick="update()">确定</button>
			    </div>
			</div>
        </div>
    </div>
    <script>
	function update(){
		var rep_id = $.trim($('#rep_id').val());
		var doc_ids = $.trim($('#doc_ids').val());
		if(!rep_id){
			alert('请填写代表id');
			return;
		}
		if(!doc_ids){
			alert('请填写医生id');
			return;
		}
		var url = "/admuser/update-doc-link-rep-do";
		var data = {'rep_id':rep_id,'doc_ids':doc_ids};
		$.post(url,data,function(json){
			if(!json.success){
				alert(json.msg);
			}else{
				alert(json.msg);
			}
		},'json');
	}
	</script>
	
@stop


