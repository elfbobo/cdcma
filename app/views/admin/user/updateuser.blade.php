@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>更新代表大区地区信息</h3>
	</div>
	<div class="page_center_right right">
        <div class="recommend_pd">
	        
	        <form id="form1" name="form1" enctype="multipart/form-data" method="post" action="/admuser/update-user-regin-area-do">
	        <div class="Navigation_subtext">
				<table width="98%" border="0" align="center" style="margin-top:20px; border:1px solid #9abcde;">
				   <tr>
				      <td width="18%" height="50">&nbsp;选择你要导入的数据表</td>
				      <td width="82%">
				        <label>
				             <input name="file" type="file" id="file" size="50" />
				        </label>
				        <label>
				             <input name="button" type="submit" class="nnt_submit" id="button" value="开始更新" />
				        </label>&nbsp;
				      </td>
				    </tr>
				</div>
			</form>
				    
				    <tr>
				    <td colspan="2">
				    <label style="color: red">
				   注意：导入文件格式必须为：csv<br>
				   第一列为代表的CWID，第二列为代表的大区id，第三列为代表的地区id<br>
				    </label>
				    <br>
				    </td>
				    </tr>
				</table>
			</div>
	        
        </div>
      </div>
	
@stop


