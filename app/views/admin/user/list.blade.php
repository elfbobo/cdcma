@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>用户列表</h3>
	</div>
	<div>
	<form class="navbar-form navbar-left"  method="get" action="/admuser">
	  
	  <div class="form-group">
	  	<span>用户类型：</span>
	   	<select name="user_type" class="form-control">  
	   	  <option value ="0">=用户类型=</option> 
		  <option value ="1" @if(Input::has('user_type')&&Input::get('user_type')==1)  selected = "selected"  @endif>拜新同用户</option>  
		  <option value="2" @if(Input::has('user_type')&&Input::get('user_type')==2)  selected = "selected" @endif>拜阿用户</option> 
		  <option value="3" @if(Input::has('user_type')&&Input::get('user_type')==3)  selected = "selected" @endif>拜新同和拜阿</option> 
		</select> 
	  </div>
	  &nbsp;&nbsp;&nbsp;
	   <div class="form-group">
	  	<span>角色：</span>
	   	<select name="role_id" class="form-control">  
	   	  <option value ="0">=全部=</option> 
		  <option value ="2" @if(Input::has('role_id')&&Input::get('role_id')==2)  selected = "selected"  @endif>代表</option>  
		  <option value="3" @if(Input::has('role_id')&&Input::get('role_id')==3)  selected = "selected" @endif>医生</option>  
		</select> 
	  </div>
	  <br><br>
	  <div class="form-group">
	  	<span>用户姓名：</span>
	    <input type="text" class="form-control" name="user_name" value="{{Input::get('user_name','')}}">
	  </div>
	  &nbsp;&nbsp;&nbsp;
<!--	  <div class="form-group">-->
<!--	  	<span>用户类型：</span>-->
<!--	    <input type="text" class="form-control" name="user_type" value="{{Input::get('user_type','')}}">-->
<!--	  </div>-->
	  &nbsp;&nbsp;&nbsp;
	  <div class="form-group">
	  	<span>CWID：</span>
	    <input type="text" class="form-control" name="user_cwid" value="{{Input::get('user_cwid','')}}">
	  </div>
	  &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
	  <div class="form-group">
	  	<span>用户编码：</span>
	    <input type="text" class="form-control" name="invite_code" value="{{Input::get('invite_code','')}}">
	  </div>
	  <br><br>
	  &nbsp;&nbsp;&nbsp;
	  <div class="form-group">
	  	<span>用户名：</span>
	    <input type="text" class="form-control" name="user_nick" value="{{Input::get('user_nick','')}}">
	  </div>
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  <div class="form-group">
	  	<span>手机：</span>
	    <input type="text" class="form-control" name="user_tel" value="{{Input::get('user_tel','')}}">
	  </div>
	  &nbsp;&nbsp;&nbsp;
	  <div class="form-group">
	  	<span>邮箱：</span>
	    <input type="text" class="form-control" name="user_email" value="{{Input::get('user_email','')}}">
	  </div>
	  <br><br>
	  &nbsp;&nbsp;&nbsp;
	  <div class="form-group">
	  	<span>所属代表id：</span>
	    <input type="text" class="form-control" name="link_rep_id" value="{{Input::get('link_rep_id','')}}">
	  </div>
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  <div class="form-group">
	  	<span>所属上级id：</span>
	    <input type="text" class="form-control" name="link_user_id" value="{{Input::get('link_user_id','')}}">
	  </div>
	  <br><br>
	  <div class="form-group">
	  	<span>病例征集权限：</span>
	    <input type="checkbox" class="form-control" name="case_checkbox" @if(Input::has('case_checkbox')) checked @endif>
	  </div>
	  <br><br>
	  <button type="submit" class="btn btn-default" style="margin-left:77px;">检索</button>
	</form>
	</div>
	<div>
		<div style="float:right;">
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admuser/add-user'">添加用户</button>
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admuser/list-group'">查看分组</button>
			<!-- 
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admuser/export-user'">导出用户关联信息</button>
			 -->
			 <div class="form-group" style="float:right;">
			   	<select name="num" class="form-control"> 
			   	@foreach($aTotalNum as $k=>$v)
			   	  <option value ="{{$v}}">{{$v}}条</option> 
			   	@endforeach
				</select> 
			  </div>
			<button type="button" class="btn btn-primary" onclick="exportUserTel()">导出用户及电话信息</button>
			<button type="button" class="btn btn-primary" onclick="exportUserInfos()">导出列表数据</button>
			<!-- <button type="button" class="btn btn-primary" onclick="window.location.href='/admuser/export-user-tel/2'">导出用户及电话信息2</button> -->
		</div>
	</div>
	
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>用户名</th>
          <th>注册编码</th>
          <th>角色</th>
          <th>CWID</th>
          <th>用户姓名</th>
          <th>手机号码</th>
          <th>邮箱</th>
          <!-- <th>用户类型</th> -->
          <th>所属代表姓名</th>
          <th>所属代表编码</th>
<!--          <th>积分</th>-->
<!--          <th>直播积分</th>-->
<!--          <th>录播积分</th>-->
          <th>讲课费用</th>
          <th>重置密码</th>
          <th>病例管理权限</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oUsers as $k=>$v)
        <tr>
          <th scope="row">{{$v->id}}</th>
          <td>{{$v->user_nick}}</td>
          <td>{{$v->invite_code}}</td>
          <td>{{$aRole[$v->role_id]}}</td>
          <td>{{$v->user_cwid}}</td>
          <td>{{$v->user_name}}</td>
          <td>{{$v->user_tel}}</td>
          <td>{{$v->user_email}}</td>
          <!-- <td>{{$v->user_type}}</td> -->
          <td>@if(!empty($aRepName[$v->link_rep_id])) {{$aRepName[$v->link_rep_id]}} @endif</td>
          <td>@if(!empty($aRepCode[$v->link_rep_id])) {{$aRepCode[$v->link_rep_id]}} @endif</td>
<!--          <td>{{$v->user_score}}</td>-->
<!--          <td>{{$v->user_live_score}}</td>-->
<!--          <td>{{$v->user_review_score}}</td>-->
          <td>{{$v->user_fee}}</td>
          <td><a style="cursor:pointer;" onclick="cheng_psw({{$v->id}})">重置密码</a></td>
          <td>
          	@if(in_array($v->id,$aUserAdminCase1))
          		<a style="cursor:pointer;color:red;" onclick="user_admin_case({{$v->id}},1)">取消编辑审核</a>
          	@else
          		<a style="cursor:pointer;" onclick="user_admin_case({{$v->id}},1)">编辑审核权限</a>
          	@endif
          	<br>
          	@if(in_array($v->id,$aUserAdminCase2))
          		<a style="cursor:pointer;color:red;" onclick="user_admin_case({{$v->id}},2)">取消所有权限</a>
          	@else
          		<a style="cursor:pointer;" onclick="user_admin_case({{$v->id}},2)">所有权限</a>
          	@endif
          	
          	</td>
          <td>
          		<a href="/admuser/edit-user/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
          		&nbsp;&nbsp;&nbsp;
          		<a href="#" onclick="delUser('/admuser/del-user/{{$v->id}}')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oUsers->appends($aLink)->links()}}
    <script>
    	function exportUserTel(){
    		var num = $('select[name="num"]').val();
    		window.location.href='/admuser/export-user-tel/'+num;
    	}
    	function exportUserInfos(){
    		var num = $('select[name="num"]').val();
    		// window.open('/admuser/export-user-info/'+num, "", "");
    		window.location.href='/admuser/export-user-info/'+num;
    	}

		function delUser(url){
			if(confirm('确定要删除当前用户吗？')){
				window.location.href = url;
			}
		}
		function cheng_psw(id){
			 var str=prompt("您即将重置id为"+id+"的用户密码为：","123456");
		    if(str)
		    {
		        $.post('/admuser/change-psw/'+id+'/'+str,{},function(msg){
					if(msg=='success'){
						alert('密码重置成功');
					}else{
						alert('密码重置失败');
					}
			    })
		    }
		}	

		function user_admin_case(uid,type){
			$.post('/admuser/user-admin-case/'+uid+'/'+type,{},function(msg){
				if(msg=='success'){
					alert('成功');
					window.location.reload();
				}else{
					alert('失败');
					window.location.reload();
				}
		    })
		}
	</script>
@stop
