@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>代表培训列表</h3>
	</div>
	<div>
	</div>
	<div>
		<div style="float:right;">
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admuser/train-add'">新增培训</button>
		</div>
	</div>
	
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>培训主题</th>
          <th>类型</th>
          <th>培训开始时间</th>
          <th>培训结束时间</th>
          <th>操作</th>
          <th>视频观看情况导出</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oTrain as $k=>$v)
        <tr>
          <th scope="row">{{$v->id}}</th>
          <td>{{$v->train_title}}</td>
          <td>@if($v->train_type==1) 直播 @else 录播 @endif</td>
          <td>{{$v->start_time}}</td>
          <td>{{$v->end_time}}</td>
          <td>
          		<a href="/admuser/train-edit/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
          		&nbsp;&nbsp;
          		<a href="#" onclick="delTrain('/admuser/train-del/{{$v->id}}')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
          </td>
          <td>
                 &nbsp;&nbsp;&nbsp;&nbsp; <a href="/admuser/train-export/{{$v->id}}" ><button type="button" class="btn btn-primary">导出</button></a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oTrain->links()}}
    <script>
		function delTrain(url){
			if(confirm('确定要删除当前培训预告吗？')){
				window.location.href = url;
			}
		}
	</script>
@stop
