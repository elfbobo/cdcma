@extends('admin.common.layout')
@section('content')
<!-- 配置文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="/assets/js/ueditor/lang/zh-cn/zh-cn.js"></script>

<!-- date -->
<script src="/assets/js/DatePicker/WdatePicker.js" type="text/javascript"></script>
<link type="text/css" href="/assets/js/date/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<link type="text/css" href="/assets/js/date/css/jquery-ui-timepicker-addon.css" rel="stylesheet" />
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-timepicker-zh-CN.js"></script>
<script type="text/javascript">
    $(function () {
        $(".ui_timepicker").datetimepicker({
            showSecond: true,
            timeFormat: 'hh:mm:ss',
            stepHour: 1,
            stepMinute: 1,
            stepSecond: 1
        })
    })
</script>
<!-- date -->

	<div class="app_content_div" id="app_content_div_301Index">
		<h3>编辑培训</h3>
	</div>
	
	<form class="form-horizontal"  method="post" action="/admuser/train-edit-do/{{$oTrain->id}}">
	  <div class="form-group">
	    <label for="train_title" class="col-sm-2 control-label"><span style="color:red;">*</span>培训主题</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="train_title" name="train_title" value="{{$oTrain->train_title}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="train_type" class="col-sm-2 control-label"><span style="color:red;">*</span>类型</label>
	      <label class="radio-inline">
			  <input type="radio" name="train_type" id="train_type1" value="1" @if($oTrain->train_type==1) checked @endif> 直播
			</label>
			<label class="radio-inline">
			  <input type="radio" name="train_type" id="train_type2" value="2" @if($oTrain->train_type==2) checked @endif> 录播
			</label>
	  </div>
	  <div class="form-group">
	    <label for="train_url" class="col-sm-2 control-label"><span style="color:red;">*</span>直播或录播视频路径</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="train_url" name="train_url"  value="{{$oTrain->train_url}}">
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <label for="doc_department" class="col-sm-2 control-label"><span style="color:red;">*</span>培训开始时间</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control ui_timepicker" id="start_time" name="start_time" value="{{$oTrain->start_time}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;">*</span>培训结束时间</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control ui_timepicker" id="end_time" name="end_time" value="{{$oTrain->end_time}}" >
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <label for="train_download_url" class="col-sm-2 control-label">录播视频下载地址</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="train_download_url" name="train_download_url"  value="{{$oTrain->train_download_url}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="train_download_psw" class="col-sm-2 control-label">录播视频下载密码</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="train_download_psw" name="train_download_psw"  value="{{$oTrain->train_download_psw}}">
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">确定</button>
	    </div>
	  </div>
	</form>
@stop


