@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>预约专家</h3>
	</div>
	<div>
	</div>
	<div>
		<div style="float:right;">
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admuser/export-order'">导出预约信息</button>
		</div>
	</div>
	
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>排行榜排名</th>
          <th>代表姓名</th>
          <th>代表cwid</th>
          <th>专家</th>
          <th>科室</th>
          <th>预约时间</th>
          <th>类型</th>
          <th>创建时间</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oUserOrder as $k=>$v)
        <tr>
          <th scope="row">{{$v->id}}</th>
          <td>{{isset($aUserOrder[$v->user_id])?$aUserOrder[$v->user_id]:''}}</td>
          <td>{{$v->user_name}}</td>
          <td>{{$v->user_cwid}}</td>
          <td>{{$v->doc_name}}</td>
          <td>{{$v->doc_department}}</td>
          <td>{{$v->order_time}}</td>
          <td>@if($v->user_order_doc_id==0) 旧版本数据 @endif</td>
          <td>{{$v->created_at}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oUserOrder->links()}}
@stop
