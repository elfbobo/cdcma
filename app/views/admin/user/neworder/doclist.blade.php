@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>我的预约--可预约专家列表</h3>
	</div>
	<div>
	
	</div>
	<div>
		<div style="float:right;">
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admuser/neworder/adddoc'">添加可预约医生</button>
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admuser/order'">查看专家预约情况</button>
		</div>
	</div>
	
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>姓名</th>
          <th>医院</th>
          <th>科室</th>
          <th>预约时间</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oUserOrderDoc as $k=>$v)
        <tr>
          <th scope="row">{{$v->id}}</th>
          <td>{{$v->doc_name}}</td>
          <td>{{$v->doc_hospital}}</td>
          <td>{{$v->doc_department}}</td>
          <td>{{substr($v->start_time,0,16)}}—{{substr($v->end_time,11,5)}}</td>
          <td>
          		<a href="/admuser/neworder/editdoc/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
          		&nbsp;&nbsp;&nbsp;
          		<a href="#" onclick="delUser('/admuser/del-order-doc/{{$v->id}}')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <script>
		function delUser(url){
			if(confirm('确定要删除当前专家吗？')){
				window.location.href = url;
			}
		}
	</script>
@stop
