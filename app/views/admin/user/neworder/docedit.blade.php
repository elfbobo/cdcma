@extends('admin.common.layout')
@section('content')
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/assets/js/doc.js"></script>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>我的预约编辑可预约专家</h3>
	</div>
	
	<form class="form-horizontal"  method="post" action="/admuser/neworder/editdocdo/{{$oUserOrderDoc->id}}">
	  <div class="form-group">
	    <label for="user_name" class="col-sm-2 control-label"><span style="color:red;">*</span>姓名</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_name" name="doc_name" value="{{$oUserOrderDoc->doc_name}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_nick" class="col-sm-2 control-label"><span style="color:red;">*</span>医院</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_hospital" name="doc_hospital"  value="{{$oUserOrderDoc->doc_hospital}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_tel" class="col-sm-2 control-label"><span style="color:red;">*</span>科室</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_department" name="doc_department"  value="{{$oUserOrderDoc->doc_department}}">
	    </div>
	  </div>
	 <div class="form-group">
	    <label for="upload_file" class="col-sm-2 control-label"><span style="color:red;">*</span>缩略图</label>
	    <div class="col-sm-10">
	    	<input  id="upload_file" name="upload_file"  type="file" onchange="saveThumb()"/>
	      	<input type="hidden" class="form-control" id="doc_thumb" name="doc_thumb"  value="{{$oUserOrderDoc->doc_thumb}}">
	      	<img style="width:160px;height:200px;" alt="" id="thumb" src="{{$oUserOrderDoc->doc_thumb}}"/>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_department" class="col-sm-2 control-label"><span style="color:red;">*</span>预约时间开始</label>
	    <div class="col-sm-10">
	      <input type="text" name="start_time" value="{{$oUserOrderDoc->start_time}}" onFocus="WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="txt short_txt" />
                         <a onclick="$('#start_time').focus();" class="time_icon"></a>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;">*</span>预约时间结束</label>
	    <div class="col-sm-10">
	      <input type="text" name="end_time" value="{{$oUserOrderDoc->end_time}}"  onFocus="WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="txt short_txt" />
                         <a onclick="$('#end_time').focus();" class="time_icon"></a>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;">*</span>可预约代表大区（不选表示所有积分排行前50名的代表都可以预约）</label>
	    <div class="col-sm-10">
	    @foreach($oRegin as $k=>$v)
	      <div style="width:185px;float:left;"><input type="checkbox" name="user_regin[]" value="{{$k}}" @if(strstr($oUserOrderDoc->user_regin,'|'.$k.'|')) checked @endif/>{{$v}}</div>
	    @endforeach
	    </div>
	  </div>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">确定</button>
	    </div>
	  </div>
	</form>
@stop


