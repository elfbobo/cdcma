@extends('admin.common.layout')
@section('content')
<!-- 配置文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="/assets/js/ueditor/lang/zh-cn/zh-cn.js"></script>
<script src="/assets/js/ajaxfileupload.js"></script>
<!-- date -->
<script src="/assets/js/DatePicker/WdatePicker.js" type="text/javascript"></script>
<link type="text/css" href="/assets/js/date/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<link type="text/css" href="/assets/js/date/css/jquery-ui-timepicker-addon.css" rel="stylesheet" />
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-timepicker-zh-CN.js"></script>
<script type="text/javascript">
    $(function () {
        $(".ui_timepicker").datetimepicker({
            showSecond: true,
            timeFormat: 'hh:mm:ss',
            stepHour: 1,
            stepMinute: 1,
            stepSecond: 1
        })
    })
</script>
<!-- date -->

	<div class="app_content_div" id="app_content_div_301Index">
		<h3>新增线下会议</h3>
	</div>
	
	<form class="form-horizontal"  method="post" action="/admaspirinoffline/add-do">
		<div class="form-group">
		    <label for="offline_title" class="col-sm-2 control-label"><span style="color:red;">*</span>会议主题</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" id="offline_title" name="offline_title" >
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="offline_place" class="col-sm-2 control-label"><span style="color:red;">*</span>会议地点</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" id="offline_place" name="offline_place" >
		    </div>
	  	</div>
	    <div class="form-group">
		    <label for="offline_start_time" class="col-sm-2 control-label"><span style="color:red;">*</span>会议开始时间</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control ui_timepicker" id="offline_start_time" name="offline_start_time" onchange="endTimeChange()">
		    </div>
	    </div>
	    <div class="form-group">
		    <label for="offline_end_time" class="col-sm-2 control-label"><span style="color:red;">*</span>会议结束时间</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control ui_timepicker" id="offline_end_time" name="offline_end_time" >
		    </div>
	    </div>
	    <div class="form-group">
		    <label for="apply_start_time" class="col-sm-2 control-label"><span style="color:red;">*</span>报名开始时间</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control ui_timepicker" id="apply_start_time" name="apply_start_time">
		    </div>
	    </div>
	    <div class="form-group">
		    <label for="apply_end_time" class="col-sm-2 control-label"><span style="color:red;">*</span>报名结束时间</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control ui_timepicker" id="apply_end_time" name="apply_end_time" >
		    </div>
	    </div>
	    <div class="form-group">
		    <label for="offline_least_gold" class="col-sm-2 control-label"><span style="color:red;">*</span>麦粒限制</label>
		    <div class="col-sm-10">
		    	<input type="text"  id="offline_least_gold" name="offline_least_gold" placeholder="0">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="offline_count" class="col-sm-2 control-label"><span style="color:red;">*</span>报名人数限制</label>
		    <div class="col-sm-10">
		    	<input type="text"  id="offline_count" name="offline_count" placeholder="0">
		    </div>
	  	</div>
	    <div class="form-group" >
		    <label for="offline_content" class="col-sm-2 control-label"><span style="color:red;">*</span>会议简介</label>
		    <div class="col-sm-10">
			    <script id="offline_content" name="offline_content" type="text/plain"></script>
				<script type="text/javascript">
				    var editor = UE.getEditor('offline_content')
				</script>
		    </div>
	    </div>
	  	<div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		    	<button type="submit" class="btn btn-default">确定</button>
		    </div>
	  	</div>
	</form>
	
<script type="text/javascript">
function endTimeChange(){
	var start = $('#offline_start_time').val();
	$('#offline_end_time').val(start);
// 	$('#apply_start_time').val(start);
}
</script>
@stop


