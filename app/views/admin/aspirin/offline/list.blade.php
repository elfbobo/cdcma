@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>线下会议列表</h3>
	</div>
	<form class="navbar-form navbar-left"  method="get" action="/admaspirinoffline/list">
		<div class="form-group">
	  		<span>会议主题：</span>
	    	<input type="text" class="form-control" id="title" name="title">
	  	</div>
	  	<button type="submit" class="btn btn-default" style="margin-left:77px;">检索</button>
	</form>
	<div style="float:right;">
		<button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirinoffline/add'">新增会议</button>
	</div>
	<table class="table table-striped">
      <thead>
        <tr>
        	<th>序号</th>
          	<th>会议主题</th>
          	<th>会议开始时间</th>
          	<th>报名截止时间</th>
          	<th>麦粒限制</th>
          	<th>报名人数限制</th>
          	<th>创建时间</th>
          	<th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oOffline as $k=>$v)
        <tr>
        	<td>{{$v->id}}</td>
          	<td>{{$v->offline_title}}</td>
          	<td>{{$v->offline_start_time}}</td>
          	<td>{{$v->apply_end_time}}</td>
          	<td>{{$v->offline_least_gold}}</td>
          	<td>{{$v->offline_count}}</td>
          	<td>{{$v->created_at}}</td>
			<td>
	            <a href="/admaspirinoffline/edit/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
	          	&nbsp;&nbsp;&nbsp;
	          	<a href="/admaspirinoffline/delete/{{$v->id}}" onclick="return confirm('确定删除吗');" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
	            &nbsp;&nbsp;&nbsp;
	            <a href="/admaspirinoffline/apply-user/{{$v->id}}">查看报名列表</a>
          	</td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oOffline ->links()}}
@stop
