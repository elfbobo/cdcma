@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>报名参与线下会议的人员名单</h3>
	</div>
	<table class="table table-striped">
      <thead>
        <tr>
        	<th>序号</th>
          	<th>用户id</th>
          	<th>用户CWID</th>
          	<th>用户姓名</th>
          	<th>创建时间</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oApplyInfo as $k=>$v)
        <tr>
        	<td>{{$v->id}}</td>
          	<td>{{$v->user_id}}</td>
          	<td>{{$v->user_cwid}}</td>
          	<td>{{$v->user_name}}</td>
          	<td>{{$v->created_at}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oApplyInfo->links()}}
@stop
