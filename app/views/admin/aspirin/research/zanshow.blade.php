@extends('admin.common.layout')
@section('content')
<script src="/assets/js/ajaxfileupload.js"></script>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>集赞审核</h3>
	</div>
	<form class="form-horizontal" method="post" action="/admaspirinresearch/zan-check"> 
		<input type="hidden" class="form-control" id="iId" name="iId" value="{{$iId}}"> 
		<div class="form-group">
		    <label class="col-sm-2 control-label">上传的集赞照片</label>
		    <div class="col-sm-10">
		      	<img id="image_url" src="{{$oZan->image_url}}" onmouseover="bigger()" onmouseout="smaller()"/>
		    </div>
	  	</div>		
	  	<div class="form-group">
		    <label class="col-sm-2 control-label"><span style="color:red;">*</span>允许通过审核</label>
		    <div class="col-sm-10">
				<label class="radio-inline">
					<input type="radio" name="zan_type" id="zan_type" value="1" onclick="zan_box(1);" @if($oZan->zan_type==1 ||$oZan->zan_type==0) checked @endif > 是
				</label>
		      	<label class="radio-inline">
					<input type="radio" name="zan_type" id="zan_type" value="2" onclick="zan_box(2);" @if($oZan->zan_type==2) checked @endif > 否
				</label>
		    </div>
	    </div>
	  	<div class="form-group">
		    <label for="fail_reason" class="col-sm-2 control-label">审核失败原因</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" id="fail_reason" name="fail_reason" value="{{$oZan->fail_reason}}" @if($oZan->zan_type==1 ||$oZan->zan_type==0) disabled @endif>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		    	<button type="submit" class="btn btn-default">确定</button>
		    </div>
	  	</div>
	</form>
<script type="text/javascript">
	function zan_box(flag)
	{
		if(flag == 2){
			$('#fail_reason').attr("disabled",false); 
			document.getElementById("fail_reason").focus().select(); 
		}else if(flag == 1){
			$('#fail_reason').val('');
			document.getElementById("fail_reason").setAttribute('disabled','disabled');
		}
	}
</script>
<script type="text/javascript">
	var img = document.getElementById('image_url');
 	function bigger(){
		img.style.width = '400px';
  		img.style.height = '400px';
 	}
 
 	function smaller(){
  		img.style.width = '200px';
  		img.style.height = '200px';
 	}
</script>
@stop