@extends('admin.common.layout')
@section('content')
<script src="/assets/js/ajaxfileupload.js"></script>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>科研申请详情</h3>
	</div>
	<form class="form-horizontal">
		<div class="form-group">
		    <label class="col-sm-2 control-label">姓名</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="apply_name" disabled value="{{$oApply->apply_name}}">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">城市</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="apply_city" disabled value="{{$oApply->apply_city}}">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">出生日期</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="apply_birthday" disabled value="{{$oApply->apply_birthday}}">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">职称</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="apply_title" disabled value="{{$oApply->apply_title}}">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">电话</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="apply_tel" disabled value="{{$oApply->apply_tel}}">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">邮箱</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="apply_email" disabled value="{{$oApply->apply_email}}">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">单位名称</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="apply_company" disabled value="{{$oApply->apply_company}}">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">地址</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="apply_address" disabled value="{{$oApply->apply_address}}">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">邮编</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="apply_postcode" disabled value="{{$oApply->apply_postcode}}">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">教育背景</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="education_background" disabled value="{{$oApply->education_background}}">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">工作经历</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="work_experience" disabled value="{{$oApply->work_experience}}">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">科研经历</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="research_experience" disabled value="{{$oApply->research_experience}}">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">问题1</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="question1" disabled value="{{$oApply->question1}}">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">问题2</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="question2" disabled value="{{$oApply->question2}}">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">问题3</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="question3" disabled value="{{$oApply->question3}}">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">问题4</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="question4" disabled value="{{$oApply->question4}}">
		    </div>
	  	</div>
	</form>
	<form class="form-horizontal" method="post" action="/admaspirinresearch/apply-check">  		
	  	<input type="hidden" class="form-control" id="iApplyId" name="iApplyId" value="{{$iApplyId}}">
	  	<div class="form-group">
		    <label class="col-sm-2 control-label"><span style="color:red;">*</span>允许通过审核</label>
		    <div class="col-sm-10">
				<label class="radio-inline">
					<input type="radio" name="apply_type" id="apply_type" value="1" onclick="apply_box(1);" @if($oApply->apply_type==1 ||$oApply->apply_type==0) checked @endif > 是
				</label>
		      	<label class="radio-inline">
					<input type="radio" name="apply_type" id="apply_type" value="2" onclick="apply_box(2);" @if($oApply->apply_type==2) checked @endif > 否
				</label>
		    </div>
	    </div>
	  	<div class="form-group">
		    <label for="fail_reason" class="col-sm-2 control-label">审核失败原因</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" id="fail_reason" name="fail_reason" value="{{$oApply->fail_reason}}" @if($oApply->apply_type==1 ||$oApply->apply_type==0) disabled @endif>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		    	<button type="submit" class="btn btn-default">确定</button>
		    </div>
	  	</div>
	</form>
<script type="text/javascript">
	function apply_box(flag)
	{
		if(flag == 2){
			$('#fail_reason').attr("disabled",false); 
			document.getElementById("fail_reason").focus().select(); 
		}else if(flag == 1){
			$('#fail_reason').val('');
			document.getElementById("fail_reason").setAttribute('disabled','disabled');
		}
	}
</script>
@stop
