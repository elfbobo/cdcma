@extends('admin.common.layout')
@section('content')
<div class="app_content_div">
	<h2 class="sub-header">科研申请板块</h2>
    <div class="table-responsive">
    	<table class="table table-striped"></table>
        <table class="table table-striped">
        	<thead></thead>
            <tbody>
            	<tr>
                  	<td>科研申请审核</td>
                	<td><a href="/admaspirinresearch/apply-list"><button type="button" class="btn btn-primary">点击进入</button></a></td>
                </tr>
                <tr>
                  	<td>集赞照片审核</td>
                	<td><a href="/admaspirinresearch/zan-list"><button type="button" class="btn btn-primary">点击进入</button></a></td>
                </tr>
                <tr>
                  	<td>科研培训课程</td>
                	<td><a href="/admaspirinresearch/train-list"><button type="button" class="btn btn-primary">点击进入</button></a></td>
                </tr>
            </tbody>
        </table>
	</div>
</div>
@stop


