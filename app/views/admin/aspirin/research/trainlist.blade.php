@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>培训课程列表</h3>
	</div>
	<form class="navbar-form navbar-left"  method="get" action="/admaspirinresearch/train-list">
		<div class="form-group">
	  		<span>培训主题：</span>
	    	<input type="text" class="form-control" id="title" name="title">
	  	</div>
	  	<button type="submit" class="btn btn-default" style="margin-left:77px;">检索</button>
	</form>
	<div style="float:right;">
		<button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirinresearch/train-add/'">新增课程</button>
	</div>
	<table class="table table-striped">
      <thead>
        <tr>
        	<th>序号</th>
          	<th>培训主题</th>
          	<th>点击量</th>
          	<th>创建时间</th>
          	<th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oTrain as $k=>$v)
        <tr>
        	<td>{{$v->id}}</td>
        	<td>{{$v->train_title}}</td>
          	<td>{{$v->train_hits}}</td>
          	<td>{{$v->created_at}}</td>
			<td>
	            <a href="/admaspirinresearch/train-edit/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
	          	&nbsp;&nbsp;&nbsp;
	          	<a href="/admaspirinresearch/train-delete/{{$v->id}}" onclick="return confirm('确定删除吗');" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
	            &nbsp;&nbsp;&nbsp;
          	</td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oTrain->links()}}
@stop
