@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>集赞照片信息列表</h3>
	</div>
	<form class="navbar-form navbar-left"  method="get" action="/admaspirinresearch/zan-list">
		<div class="form-group">
	  		<span>用户姓名：</span>
	    	<input type="text" class="form-control" id="name" name="name">
	  	</div>
	  	<button type="submit" class="btn btn-default" style="margin-left:77px;">检索</button>
	</form>
	<br /><br /><br />
	<p style="color:red;">*注：同一用户审核通过一张集赞照片即可获得观看科研培训课程视频的权限</p>
	<table class="table table-striped">
      <thead>
        <tr>
        	<th>序号</th>
          	<th>用户id</th>
          	<th>用户cwid</th>
          	<th>用户姓名</th>
          	<th>审核状态</th>
          	<th>创建时间</th>
          	<th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oZan as $k=>$v)
        <tr>
        	<td>{{$v->id}}</td>
          	<td>{{$v->user_id}}</td>
          	<td>{{$v->user_cwid}}</td>
          	<td>{{$v->user_name}}</td>
          	<td>@if($v->zan_type == 0) 未审核 @elseif($v->zan_type == 1) 审核通过 @else 审核未通过 @endif</td>
          	<td>{{$v->created_at}}</td>
			<td>
	            <a href="/admaspirinresearch/zan-show/{{$v->id}}">查看详情</a></span>
          	</td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oZan->links()}}
@stop
