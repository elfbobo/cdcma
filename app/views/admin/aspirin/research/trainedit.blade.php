@extends('admin.common.layout')
@section('content')
<!-- 配置文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="/assets/js/ueditor/lang/zh-cn/zh-cn.js"></script>
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/groupinfo.js"></script>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>修改课程内容</h3>
	</div>
	
	<form class="form-horizontal"  method="post" action="/admaspirinresearch/train-edit-do">
		<input type="hidden" class="form-control" id="iId" name="iId" value="{{$iId}}">
		<div class="form-group">
		    <label for="train_title" class="col-sm-2 control-label"><span style="color:red;">*</span>标题</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" id="train_title" name="train_title" value="{{$oTrain->train_title}}">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="upload_file" class="col-sm-2 control-label"><span style="color:red;">*</span>课程缩略图</label>
		    <div class="col-sm-10">
		    	<input  id="upload_file" name="upload_file"  type="file" onchange="saveTrainThumb()"/>
		      	<input type="hidden" class="form-control" id="train_thumb" name="train_thumb" value="{{$oTrain->train_thumb}}">
		      	<img style="width:200px;height:160px;" id="thumb" src="{{$oTrain->train_thumb}}"/>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">视频背景图</label>
		    <div class="col-sm-10">
		    	<input  id="upload_file0" name="upload_file0"  type="file" onchange="saveVideoThumb()"/>
		      	<input type="hidden" class="form-control" id="video_thumb" name="video_thumb" value="{{$oTrain->video_thumb}}">
		      	<img style="width:200px;height:160px;" alt="" id="videothumb" src="{{$oTrain->video_thumb}}"/>
		    </div>
	  	</div>
	    <div class="form-group">
		    <label for="video_url" class="col-sm-2 control-label">视频路径</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" id="video_url" name="video_url" value="{{$oTrain->video_url}}" placeholder="请填写完整视频链接地址">
		    </div>
	    </div>
	    <div class="form-group" >
		    <label for="train_content" class="col-sm-2 control-label"><span style="color:red;">*</span>图文内容</label>
		    <div class="col-sm-10">
			    <script id="train_content" name="train_content" type="text/plain">{{$oTrain->train_content}}</script>
				<script type="text/javascript">
				    var editor = UE.getEditor('train_content')
				</script>
		    </div>
	    </div>
	  	<div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		    	<button type="submit" class="btn btn-default">确定</button>
		    </div>
	  	</div>
	</form>
@stop


