@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>科研申请列表</h3>
	</div>
	<form class="navbar-form navbar-left"  method="get" action="/admaspirinresearch/apply-list">
		<div class="form-group">
	  		<span>姓名：</span>
	    	<input type="text" class="form-control" id="name" name="name">
	  	</div>
	  	<button type="submit" class="btn btn-default" style="margin-left:77px;">检索</button>
	</form>
	<!--  
	<div style="float:right;">
		<button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirinresearch/export-research'">导出申请记录</button>
	</div>
	-->
	<table class="table table-striped">
      <thead>
        <tr>
        	<th>序号</th>
          	<th>用户id</th>
          	<th>姓名</th>
          	<th>城市</th>
          	<th>单位</th>
          	<th>审核状态</th>
          	<th>创建时间</th>
          	<th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oApply as $k=>$v)
        <tr>
        	<td>{{$v->id}}</td>
          	<td>{{$v->user_id}}</td>
          	<td>{{$v->apply_name}}</td>
          	<td>{{$v->apply_city}}</td>
          	<td>{{$v->apply_company}}</td>
          	<td>@if($v->apply_type == 0) 未审核 @elseif($v->apply_type == 1) 审核通过 @else 审核未通过 @endif</td>
          	<td>{{$v->created_at}}</td>
			<td>
	            <a href="/admaspirinresearch/apply-show/{{$v->id}}">查看详情</a></span>
          	</td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oApply->links()}}
@stop
