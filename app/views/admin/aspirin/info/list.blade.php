@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>轮播图系列主题资讯列表</h3>
	</div>
	<form class="navbar-form navbar-left"  method="get" action="/admaspirininfo/list/{{$iGroupId}}">
		<div class="form-group">
	  		<span>标题：</span>
	    	<input type="text" class="form-control" id="title" name="title">
	  	</div>
	  	<button type="submit" class="btn btn-default" style="margin-left:77px;">检索</button>
	</form>
	<div style="float:right;">
		<button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirininfo/add/{{$iGroupId}}'">新增资讯</button>
	</div>
	<table class="table table-striped">
      <thead>
        <tr>
        	<th>序号</th>
          	<th>主题id</th>
          	<th>标题</th>
          	<th>是否有视频</th>
          	<th>点击量</th>
          	<th>创建时间</th>
          	<th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oInfo as $k=>$v)
        <tr>
        	<td>{{$v->id}}</td>
        	<td>{{$v->group_id}}</td>
          	<td>{{$v->info_title}}</td>
          	<td>@if($v->info_video == '') 无 @else 有 @endif</td>
          	<td>{{$v->info_hits}}</td>
          	<td>{{$v->created_at}}</td>
			<td>
	            <a href="/admaspirininfo/edit/{{$iGroupId}}/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
	          	&nbsp;&nbsp;&nbsp;
	          	<a href="/admaspirininfo/delete/{{$iGroupId}}/{{$v->id}}" onclick="return confirm('确定删除吗');" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
	            &nbsp;&nbsp;&nbsp;
          	</td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oInfo ->links()}}
@stop
