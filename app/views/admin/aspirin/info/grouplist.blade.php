@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>轮播图系列主题</h3>
	</div>
	<div style="float:right;">
		<button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirininfo/group-add'">新增主题</button>
	</div>
	<p style="color:red;">注：上传的第一张banner图片为专项基金简介缩略图（点击图片跳转链接），一经上传只可修改，不可删除或添加列表</p>
	<table class="table table-striped">
      <thead>
        <tr>
        	<th>序号</th>
          	<th>主题</th>
          	<th>创建时间</th>
          	<th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oGroup as $k=>$v)
        <tr @if($v->id == 1) style="color:red;" @else @endif>
        	<td>{{$v->id}}</td>
          	<td>{{$v->info_group_title}}</td>
          	<td>{{$v->created_at}}</td>
          	@if($v->id == 1)
          	<td>
          		<a href="/admaspirininfo/group-edit/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
          	</td>
          	@else
			<td>
	            <a href="/admaspirininfo/group-edit/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
	          	&nbsp;&nbsp;&nbsp;
	          	<a href="/admaspirininfo/group-delete/{{$v->id}}" onclick="return confirm('确定删除吗');" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
	            &nbsp;&nbsp;&nbsp;
	            <a href="/admaspirininfo/list/{{$v->id}}">查看列表</a>
          	</td>
          	@endif
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oGroup ->links()}}
@stop
