@extends('admin.common.layout')
@section('content')
<!-- 配置文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="/assets/js/ueditor/lang/zh-cn/zh-cn.js"></script>
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/groupinfo.js"></script>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>新增资讯</h3>
	</div>
	
	<form class="form-horizontal"  method="post" action="/admaspirininfo/add-do">
		<input type="hidden" class="form-control" id="iGroupId" name="iGroupId" value="{{$iGroupId}}">
		<div class="form-group">
		    <label for="info_title" class="col-sm-2 control-label"><span style="color:red;">*</span>标题</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" id="info_title" name="info_title" >
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="info_subtitle" class="col-sm-2 control-label">副标题</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" id="info_subtitle" name="info_subtitle" >
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="upload_file" class="col-sm-2 control-label"><span style="color:red;">*</span>缩略图</label>
		    <div class="col-sm-10">
		    	<input  id="upload_file" name="upload_file"  type="file" onchange="saveInfoThumb()"/>
		      	<input type="hidden" class="form-control" id="info_thumb" name="info_thumb" >
		      	<img style="width:160px;height:160px;" alt="" id="thumb" src=""/>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="upload_file0" class="col-sm-2 control-label">视频缩略图</label>
		    <div class="col-sm-10">
		    	<input  id="upload_file0" name="upload_file0"  type="file" onchange="saveInfoVideoThumb()"/>
		      	<input type="hidden" class="form-control" id="info_video_thumb" name="info_video_thumb" >
		      	<img style="width:200px;height:160px;" alt="" id="videothumb" src=""/>
		    </div>
	  	</div>
	    <div class="form-group">
		    <label for="info_video" class="col-sm-2 control-label">视频路径</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" id="info_video" name="info_video" placeholder="请填写完整视频链接地址">
		    </div>
	    </div>
	    <div class="form-group" >
		    <label for="info_content" class="col-sm-2 control-label"><span style="color:red;">*</span>图文内容</label>
		    <div class="col-sm-10">
			    <script id="info_content" name="info_content" type="text/plain"></script>
				<script type="text/javascript">
				    var editor = UE.getEditor('info_content')
				</script>
		    </div>
	    </div>
	  	<div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		    	<button type="submit" class="btn btn-default">确定</button>
		    </div>
	  	</div>
	</form>
@stop


