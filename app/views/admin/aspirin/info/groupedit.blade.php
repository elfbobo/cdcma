@extends('admin.common.layout')
@section('content')
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/groupinfo.js"></script>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>修改主题</h3>
	</div>
	
	<form class="form-horizontal"  method="post" action="/admaspirininfo/group-edit-do">
		<input type="hidden" class="form-control" id="iId" name="iId" value="{{$iId}}">
		<div class="form-group">
		    <label for="info_group_title" class="col-sm-2 control-label"><span style="color:red;">*</span>标题</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" id="info_group_title" name="info_group_title" value="{{$oGroup->info_group_title}}">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="upload_file" class="col-sm-2 control-label"><span style="color:red;">*</span>图片</label>
		    <div class="col-sm-10">
		    	<input  id="upload_file" name="upload_file"  type="file" onchange="saveGroupThumb()"/>
		      	<input type="hidden" class="form-control" id="info_group_thumb" name="info_group_thumb" value="{{$oGroup->info_group_thumb}}">
		      	<img style="width:200px;height:160px;" alt="" id="thumb" src="{{$oGroup->info_group_thumb}}"/>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		    	<button type="submit" class="btn btn-default">确定</button>
		    </div>
	  	</div>
	</form>
@stop


