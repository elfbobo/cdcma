@extends('admin.common.layout')
@section('content')
<script src="/assets/js/ajaxfileupload.js"></script>
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirineducation/list">健康教育</a>>>新增PPT</h4>
	</div>
	<br><br><br>
	<form class="form-horizontal"  method="post" action="/admaspirineducation/add-ppt-do">
		<div class="form-group">
		    <label for="ppt_title" class="col-sm-1 control-label"><span style="color:red;">*</span>主题</label>
		    <div class="col-sm-9">
		    	<input type="text" class="form-control" id="ppt_title" name="ppt_title" >
		    </div>
	  	</div>
	  	<div class="form-group">
			<label class="col-sm-1 control-label"><span style="color:red;">*</span>所属科室分类</label>
		    <div class="col-sm-9">
		    	<label class="radio-inline">
					<input type="radio" name="catid" id="catid1" value="1"> 心内科
				</label>
				<label class="radio-inline">
				    <input type="radio" name="catid" id="catid2" value="2"> 神内科
				</label>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="upload_file" class="col-sm-1 control-label"><span style="color:red;">*</span>缩略图</label>
		    <div class="col-sm-9">
		    	<input  id="upload_file" name="upload_file"  type="file" onchange="saveThumb()"/>
		      	<input type="hidden" class="form-control" id="ppt_thumb" name="ppt_thumb" >
		      	<img style="width:200px;height:160px;margin-top:20px;" alt="" id="thumb" src=""/>
		    </div>
	  	</div>
	  
	  	<div class="form-group">
		    <div class="col-sm-offset-1 col-sm-10">
		    	<button type="submit" class="btn btn-primary">确定</button>
		    </div>
	  	</div>
	</form>
</div>	
<script>
	function saveThumb() {
		$.ajaxFileUpload({
			url: "/admaspirineducation/upload-ppt-thumb",
			secureuri: false,
			fileElementId: "upload_file",
			dataType: "json",
			success: function(data, status) {
				$("#ppt_thumb").val(data.video_thumb);
				$("#thumb").attr("src", data.video_thumb);
			}
		})
	}
</script>
@stop