@extends('admin.common.layout')
@section('content')
<!-- 配置文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="/assets/js/ueditor/lang/zh-cn/zh-cn.js"></script>
<script src="/assets/js/ajaxfileupload.js"></script>
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirineducation/list">健康教育</a>>>新增视频</h4>
	</div>
	<br><br><br>
	<form class="form-horizontal"  method="post" action="/admaspirineducation/edu-add-do">
		<div class="form-group">
		    <label for="ppt_title" class="col-sm-1 control-label"><span style="color:red;">*</span>主题</label>
		    <div class="col-sm-9">
		    	<input type="text" class="form-control" id="ppt_title" name="ppt_title" >
		    </div>
	  	</div>
	  	<div class="form-group">
			<label class="col-sm-1 control-label"><span style="color:red;">*</span>所属科室分类</label>
		    <div class="col-sm-9">
		    	<label class="radio-inline">
					<input type="radio" name="ppt_catid" id="ppt_catid1" value="1"> 心内科
				</label>
				<label class="radio-inline">
				    <input type="radio" name="ppt_catid" id="ppt_catid2" value="2"> 神内科
				</label>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="upload_file" class="col-sm-1 control-label"><span style="color:red;">*</span>缩略图</label>
		    <div class="col-sm-9">
		    	<input  id="upload_file" name="upload_file"  type="file" onchange="saveThumb()"/>
		      	<input type="hidden" class="form-control" id="ppt_thumb" name="ppt_thumb" >
		      	<img style="width:200px;height:160px;margin-top:20px;" alt="" id="thumb" src=""/>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="video_url" class="col-sm-1 control-label"><span style="color:red;">*</span>视频地址</label>
		    <div class="col-sm-9">
		    	<input type="text" class="form-control" id="video_url" name="video_url">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="education_content" class="col-sm-1 control-label"><span style="color:red;">*</span>会议简介</label>
		    <div class="col-sm-9">
			    <script id="education_content" name="education_content" type="text/plain"></script>
				<script type="text/javascript">
				    var editor = UE.getEditor('education_content')
				</script>
		    </div>
	    </div>
	    
	  	<div class="form-group">
		    <div class="col-sm-offset-1 col-sm-10">
		    	<button type="submit" class="btn btn-primary">确定</button>
		    </div>
	  	</div>
	</form>
</div>	
<script>
	function saveThumb() {
		$.ajaxFileUpload({
			url: "/admaspirineducation/upload-ppt-thumb",
			secureuri: false,
			fileElementId: "upload_file",
			dataType: "json",
			success: function(data, status) {
				$("#ppt_thumb").val(data.video_thumb);
				$("#thumb").attr("src", data.video_thumb);
			}
		})
	}
</script>
@stop