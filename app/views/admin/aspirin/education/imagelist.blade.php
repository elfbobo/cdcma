@extends('admin.common.layout')
@section('content')
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirineducation/list">健康教育</a>>>PPT图片列表</h4>
	</div>
	
	<div>
	<div style="float:right;margin-right:50px;margin-bottom:15px;margin-top:20px;">
		<button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirineducation/add-ppt-image/{{$iPptId}}'">批量新增图片</button>
	</div>
	</div>
	
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>ppt图片</th>
          <th>删除</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oImage as $k=>$v)
        <tr>
          <td>{{$v->id}}</td>
          <td><img src="{{$v->image_url}}" style="height:50px;"/></td>
          <td>
          	<a href="#" onclick="del('/admaspirineducation/del-ppt-image/{{$v->id}}/{{$iPptId}}')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
		  </td>
        </tr>
        @endforeach
      </tbody>
    </table>
</div>    
    <script>
    function del(url){
		if(confirm('您确定要删除该条记录吗？')){
			window.location.href = url;
		}
	}
    </script>
@stop
