@extends('admin.common.layout')
@section('content')
<script>
function search(){
	var type_id = $("#type_id").val(); 
	if(type_id!=-1){
		window.location.href="/admaspirineducation/video-list/"+type_id;
	}else{
		window.location.href="/admaspirineducation/video-list";
	}
}
</script>
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirineducation/list">健康教育</a>>>视频课件列表</h4>
	</div>
	<form class="navbar-form navbar-left" action="/admaspirineducation/video-list" method="get" style="margin-top:30px;margin-bottom:30px;">
	   <div class="form-group">
	  	<span>会议状态：</span>
	   	<select name="type_id" id="type_id" class="form-control">  
            <option value ="-1">=全部=</option>
            <option value ="0" @if($iTypeId==0)  selected = "selected"  @endif>未审核</option>
            <option value ="1" @if($iTypeId==1)  selected = "selected"  @endif>审核通过</option>
            <option value ="2" @if($iTypeId==2)  selected = "selected"  @endif>审核未通过</option>
            <option value ="3" @if($iTypeId==3)  selected = "selected"  @endif>待签署讲课协议</option>
            <option value ="4" @if($iTypeId==4)  selected = "selected"  @endif>推送讲课协议</option>
        </select>
	  </div>
        <div class="form-group" style="padding-left: 10px;">
            <span>关键字：</span>
            <input type="text" class="form-control" name="q" value="{{Input::get('q','')}}" placeholder="作者或医院">
        </div>
	  <button type="submit" class="btn btn-default">检索</button>
    <button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirineducation/exportpd/'">CHINA项目导出</button>
    <button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirineducation/exportpd/?showtypes=mcm'">MCM项目导出</button>
    <button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirineducation/exportsign'">导出签名</button>
	</form>
	<table class="table table-striped">
      <thead>
        <tr>
          <th style="text-align:center;">id</th>
          <th>主题</th>
          <th style="text-align:center;">作者</th>
          <th>医院</th>
          <th style="text-align:center;">项目类型</th>
          <th style="text-align:center;">上传时间</th>
          <!-- <th style="text-align:center;">31日内观看次数</th> -->
          <th style="text-align:center;">审核状态</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oEducation as $k=>$v)
        <tr>
          <td style="text-align:center;">{{$v->id}}</td>
          <td>{{$v->ppt_title}}</td>
          <td style="text-align:center;">{{$v->user_name}}</td>
          <td>{{empty($v->hospital_name)?$v->user_company_name:$v->hospital_name}}</td>
          <td style="text-align:center;">{{$v->video_types}}项目</td>
          <td style="text-align:center;">{{$v->created_at}}</td>
          <!-- <td style="text-align:center;">{{$v->hits?$v->hits:'--'}}</td> -->
          <td style="text-align:center;">
          @if($v->education_type==1)
          <span style="color:green;">审核通过</span>
          @elseif($v->education_type==2)
          <span style="color:red;">审核未通过</span>
          @else
          <span style="color:blue;">未审核</span>
          @endif
          </td>
          <td>
            @if($v->is_signed==1)
            <a href="{{$v->sign_icon}}" target="_blank" style="color:red;margin-right:8px;">导出签名</a>
            @else
              <?php
              //本月已查看次数
              /*$thismonthviewcount = VideoViewLog::whereBetween("created_at", array(date("Y-m-01 00:00:00"), date("Y-m-d 23:59:59")))
                ->where("platform",'qrcode')
                ->where("video_id",$v->id)
                ->groupBy("ip")
                ->get()->count();*/
              $thismonthviewcount = DB::table('view_qrcode_video')->where('video_id', $v->id)->pluck('share_hits');
              // echo $thismonthviewcount;
              $pushed = AspirinUserNotice::where("user_id", $v->user_id)
                ->where("notice_type", 150)
                ->where("detail_id", $v->id)
                ->get()->count();
              if($v->education_type==1 && $thismonthviewcount>=10){
                if(!$pushed){
                  echo '<a href="/admaspirineducation/push-notice/'.$v->id.'" target="_blank" style="color:blue;">推送协议</a>';
                }else{
                  echo '<a href="javascript:;" style="color:orange;">协议待签署</a>';
                }
              }
              ?>
            @endif
            <a href="/admaspirineducation/video-show/{{$v->id}}">审核</a>
            <!-- @if($v->education_type!=1)
            <a href="/admaspirineducation/video-show/{{$v->id}}">审核</a>
            @else
            <span style="color:#999">/</span>
            @endif -->
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div>{{$oEducation->appends($params)->links()}}</div>
</div>    
@stop
