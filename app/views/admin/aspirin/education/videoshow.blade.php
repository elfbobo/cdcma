@extends('admin.common.layout')
@section('content')
<link href="http://vjs.zencdn.net/5.9.2/video-js.css" rel="stylesheet">
  <script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirineducation/list">健康教育</a>>>
		<a style="color:white;" href="/admaspirineducation/video-list">会议列表</a>>>
		视频课件审核</h4>
	</div>
	<br/><br/>
	<form class="form-horizontal" method="post" action="/admaspirineducation/video-check/{{$oEducation->id}}">
	<div class="form-group">
	    <label for="user_cwid" class="col-sm-1 control-label">主题</label>
	    <div class="col-sm-9">
	      <input type="text" disabled class="form-control" value="{{$oEducation->ppt_title}}" >
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <label for="user_cwid" class="col-sm-1 control-label">作者</label>
	    <div class="col-sm-9">
	      <input type="text" disabled class="form-control" value="{{$oUser->user_name}}" >
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <label for="upload_file" class="col-sm-1 control-label">缩略图</label>
	    <div class="col-sm-9">
	      	<img style="height:200px;margin-top:20px;" src="{{$oEducation->education_thumb?$oEducation->education_thumb:$oUser->user_thumb}}"/>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_cwid" class="col-sm-1 control-label">简介</label>
	    <div class="col-sm-9">
	      <textarea disabled class="form-control">{{$oEducation->education_content}}</textarea>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_cwid" class="col-sm-1 control-label">视频预览</label>
	    <div class="col-sm-9">
	    	<video id="my-video" class="video-js" controls preload="auto" width="508" height="310"
			   data-setup="{}">
			    <source src="{{$oEducation->video_url}}" type='video/mp4'>
			    <p class="vjs-no-js">
			      To view this video please enable JavaScript, and consider upgrading to a web browser that
			      <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
			    </p>
			  </video>
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <label for="user_cwid" class="col-sm-1 control-label">审核</label>
	    <div class="col-sm-9">
	      	<label class="radio-inline">
			  <input type="radio" name="education_type" value="0" @if($oEducation->education_type==0) checked @endif> 未审核
			</label>
			<label class="radio-inline">
			  <input type="radio" name="education_type" value="1" @if($oEducation->education_type==1) checked @endif> 审核通过
			</label>
			<label class="radio-inline">
			  <input type="radio" name="education_type" value="2" @if($oEducation->education_type==2) checked @endif> 审核不通过
			</label>
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <label for="user_cwid" class="col-sm-1 control-label">审核不通过原因</label>
	    <div class="col-sm-9">
	      <input type="text" class="form-control" name="fail_reason" value="{{$oEducation->fail_reason}}" >
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <div class="col-sm-offset-1 col-sm-9">
	      <button type="submit" class="btn btn-primary" onclick="submitCheck()">提交审核结果</button>
	    </div>
	  </div>
	  
	  <br/><br/>
    </form>
</div>
@stop
