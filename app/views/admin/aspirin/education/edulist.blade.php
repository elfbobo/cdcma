@extends('admin.common.layout')
@section('content')
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4>健康教育>>视频上传</h4>
	</div>
	<div>
		<div style="float:right;margin-right:50px;margin-bottom:20px;margin-top:30px;">
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirineducation/edu-add'">新增视频</button>
		</div>
	</div>
	<table class="table table-striped ">
      <thead>
        <tr>
          <th>id</th>
          <th>题目</th>
          <th>所属科室</th>
          <th>总点击量</th>
          <th>点赞量</th>
          <th>上传时间</th>
          <th>编辑</th>
          <th>删除</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oEdu as $k=>$v)
        <tr>
          <td>{{$v->id}}</td>
          <td>{{$v->ppt_title}}</td>
          <td>@if($v->ppt_catid == 1) 心内科 @else 神内科 @endif</td>
          <td>{{$v->education_hits}}</td>
          <td>{{$v->support_count}}</td>
          <td>{{$v->created_at}}</td>
          <td>
			<a href="/admaspirineducation/edu-edit/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
          </td>
          <td>
          	<a href="#" onclick="del('/admaspirineducation/edu-del/{{$v->id}}')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
		  </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div>{{$oEdu->links()}}</div>
</div>    
<script>
    function del(url){
		if(confirm('您确定要删除该条记录吗？')){
			window.location.href = url;
		}
	}
</script>
@stop
