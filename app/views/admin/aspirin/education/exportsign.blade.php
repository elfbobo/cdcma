@extends('admin.common.layout')
@section('content')
<script src="/assets/js/ajaxfileupload.js"></script>
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirineducation/list">健康教育</a>>>导出签名</h4>
	</div>
	<br><br><br>
	<form class="form-horizontal"  method="post" action="/admaspirineducation/exportsign">
		<div class="form-group">
	    	<label class="col-sm-1 control-label">开始时间</label>
	    	<div class="col-sm-9">
	      		<input type="date" class="form-control" id="start" name="start" value="" >
	    	</div>
	  	</div>
		<div class="form-group">
	    	<label class="col-sm-1 control-label">结束时间</label>
	    	<div class="col-sm-9">
	      		<input type="date" class="form-control" id="end" name="end" value="" >
	    	</div>
	  	</div>
		<div class="form-group">
	    	<label class="col-sm-1 control-label"></label>
	    	<div class="col-sm-9">
				<p>如果不选择时间，则导出所有数据</p>
	    	</div>
	  	</div>

	  
	  	<div class="form-group">
		    <div class="col-sm-offset-1 col-sm-10">
		    	<button type="submit" class="btn btn-primary">导出</button>
		    </div>
	  	</div>
	</form>
</div>	
<script>
	function saveThumb() {
		$.ajaxFileUpload({
			url: "/admaspirineducation/upload-ppt-thumb",
			secureuri: false,
			fileElementId: "upload_file",
			dataType: "json",
			success: function(data, status) {
				$("#ppt_thumb").val(data.video_thumb);
				$("#thumb").attr("src", data.video_thumb);
			}
		})
	}
</script>
@stop


