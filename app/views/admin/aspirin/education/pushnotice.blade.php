@extends('admin.common.layout')
@section('content')
    <?php
    $agreement_txt = '服务协议

甲方：中关村国卫健康大数据研究院                	乙方：{#user_name#}
联系地址：北京市西城区天桥南大街1号B座409   	证件号码：{#idcode#}
联系人： 於墰春                                 联系地址：{#address#}
联系电话： 010-83197787                       	联系电话：{#telephone#}

为更好地开展心血管患者教育工作，改善治疗效果，甲乙双方经协商一致，根据《中华人民共和国民法通则》、《中华人民共和国合同法》相关规定，自愿订立医生在线录制健康教育音频，并用于线上心脑血管领域科普宣教的服务协议，以资共同遵守。

第一条合规道德规范
甲方高度重视与乙方的合作关系与专业关系，但没有任何关系可以与道德和声誉的重要性相提并论。乙方了解甲方的合规与道德规范，在与甲方合作期间，乙方自愿接受该规范及相应国家地区的法律法规约束.

第二条声明
甲乙双方对于本协议及协议项下内容的理解正确而充分，本协议项下的任何内容或任何支付均不构成任何商业性的暗示或明示。
本协议项下的患者健康教育系专业、严肃、单纯的患教活动，旨在传授专业知识，双方无必要也无义务帮助对方借此谋取、转换任何的商业机会。
该项声明永久有效，甲乙双方均应严格遵守。

第三条 协议内容
（一)甲方权利义务
1、甲方有权要求乙方按时按质完成本协议项下约定的患者教育活动。
2、甲方应按时向乙方支付相应的劳务报酬。
（二）乙方权利义务
1、乙方应甲方邀请，录制并发布时间累计不短于20分钟的患者教育视频，每个患教视频在录制发布一个月内至少10名或者以上患者观看后方可支付讲课费。
2、乙方有权要求甲方按约定支付劳动报酬，如甲方延迟或拒不支付本协议项下约定的劳动报酬，乙方有权追究甲方的违约责任。

第四条 协议期限
    本协议自专家完成患教视频录制并发布日起生效，至30个工作日后终止。

第五条 支付方式
1. 本协议项下费用基于市场公允价值，并根据甲方讲课费支付管理办法产生。
2. 协议费用总额：___{#fee#}___。
3．支付方式：乙方完成相应服务后，经甲乙双方签字确认后，在2-4周内以银行转帐形式支付。
乙方帐号信息如下：
银行账号：{#bank_code#}
开户银行详细地址：{#bank_address#}
4．乙方个人所得税缴纳方式： □自行申报   代扣代缴。

第六条 协议的终止,变更与解除
1.本协议期满，协议自然终止。
2.甲乙双方经协商一致，可以书面变更本协议。
3.甲乙双方经协商一致可以解除本协议。
4、任何一方单方面解除本协议的，须在教育培训活动预订开始日期前提前一月通知对方，因未及时通知造成对方损失的，解约方应赔偿对方损失。

第七条 其他
1、因本协议引起的或与本协议有关的任何争议，双方均应友好协商解决，协商不成的，双方均有权向甲方所在地人民法院提起诉讼。
2、本协议经甲方项目负责人及乙方签字生效，协议一式二份，双方各执一份。
[以下无正文，为签字/盖章页]

甲方（盖章）：	乙方（签字）:


日期：{#date#}		日期：';
    $find = array("{#user_name#}","{#idcode#}","{#address#}","{#telephone#}","{#fee#}","{#bank_code#}","{#bank_address#}","{#date#}");
    $replace = array($noticeInfo['user_name'], $noticeInfo['card_number'], $noticeInfo['user_address'], $noticeInfo['user_tel'], $noticeInfo['user_fee'], $noticeInfo['bank_account'], $noticeInfo['bank_address'], $noticeInfo['date']);
    $agreement_txt = str_replace($find, $replace, $agreement_txt);
    ?>
	<div class="panel panel-primary" style="min-height: 680px;">
		<div class="panel-heading">
			<h4>
				<a style="color:white;" href="/admaspirineducation/list">健康教育</a>>>
				<a style="color:white;" href="/admaspirineducation/video-list">会议列表</a>>>
				视频课件 >> 推送签署协议提醒
			</h4>
		</div>
		<br/><br/>
		<form class="form-horizontal" method="post" action="/admaspirineducation/push-notice-check/{{$oEducation->id}}">
			<div class="form-group">
				<label for="user_cwid" class="col-sm-2 control-label">主题</label>
				<div class="col-sm-9">
					<input type="text" disabled class="form-control" value="{{$oEducation->ppt_title}}" >
				</div>
			</div>

			<div class="form-group">
				<label for="user_cwid" class="col-sm-2 control-label">作者</label>
				<div class="col-sm-3">
					<input type="text" disabled class="form-control" value="{{$oUser->user_name}}" >
				</div>
			</div>

			<div class="form-group">
				<label for="user_cwid" class="col-sm-2 control-label">标题</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="notice_content" value="您发布的健康教育视频课件30天累计观看量已达到10次，特此邀您签署《健康教育服务协议》">
				</div>
			</div>
			
			<div class="form-group">
				<label for="user_cwid" class="col-sm-2 control-label">协议内容</label>
				<div class="col-sm-9">
					<textarea name="agreement_txt" style="display: none" name="agreement_txt"><?php echo $agreement_txt; ?></textarea>
					<input type="hidden" name="agreement_img" value="{{$signUrl}}">
					<img src="{{$signUrl}}" width="100%" alt="" style="border: #ddd solid 1px;">
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-9">
					<input type="hidden" name="user_id" value="{{$oEducation->user_id}}">
					<input type="hidden" name="notice_type" value="150">
					<input type="hidden" name="detail_id" value="{{$oEducation->id}}">
					<button type="submit" class="btn btn-primary" onclick="submitCheck()">确认提醒</button>
					<button type="button" class="btn btn-danger" onclick="modifyInfo()">修改协议内容</button>
				</div>
			</div>

			<br/><br/>
		</form>
	</div>

    <script type="text/javascript">
        var data = {{$jUserInfo}};
        function modifyInfo(){
            location.href = '/admaspirineducation/push-notice-data/{{$oEducation->id}}';
            return;

            var form = document.createElement('form');
            form.action='/admaspirineducation/push-notice-data/{{$oEducation->id}}';
            form.method='post';
            form.style.display = 'none';
            for (var i in data) {
                var input = document.createElement('input');
                input.name = i;
                input.value = data[i];
            }
            document.getElementsByTagName('body')[0].appendChild(form);
            form.submit();
        }
    </script>
@stop
