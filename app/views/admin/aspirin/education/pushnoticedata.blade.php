@extends('admin.common.layout')
@section('content')
	<div class="panel panel-primary" style="min-height: 680px;">
		<div class="panel-heading">
			<h4>
				<a style="color:white;" href="/admaspirineducation/list">健康教育</a>>>
				<a style="color:white;" href="/admaspirineducation/video-list">会议列表</a>>>
				视频课件 >> 推送签署协议内容
			</h4>
		</div>
		<br/><br/>
		<form class="form-horizontal" method="post" action="/admaspirineducation/push-notice-data/{{$oEducation->id}}">
			<div class="form-group">
				<label for="user_cwid" class="col-sm-2 control-label">姓名</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="user_name" value="{{$noticeInfo['user_name']}}">
				</div>
			</div>
			<div class="form-group">
				<label for="user_cwid" class="col-sm-2 control-label">证件号码</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="card_number" value="{{$noticeInfo['card_number']}}">
				</div>
			</div>
			<div class="form-group">
				<label for="user_cwid" class="col-sm-2 control-label">联系地址</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="user_address" value="{{$noticeInfo['user_address']}}">
				</div>
			</div>
			<div class="form-group">
				<label for="user_cwid" class="col-sm-2 control-label">联系电话</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="user_tel" value="{{$noticeInfo['user_tel']}}">
				</div>
			</div>
			<div class="form-group">
				<label for="user_cwid" class="col-sm-2 control-label">协议费用总额</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="user_fee" value="{{$noticeInfo['user_fee']}}">
				</div>
			</div>
			<div class="form-group">
				<label for="user_cwid" class="col-sm-2 control-label">银行账号</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="bank_account" value="{{$noticeInfo['bank_account']}}">
				</div>
			</div>
			<div class="form-group">
				<label for="user_cwid" class="col-sm-2 control-label">开户银行详细地址</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="bank_address" value="{{$noticeInfo['bank_address']}}">
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-9">
					<input type="hidden" name="video_id" value="{{$noticeInfo['video_id']}}">
					<button type="submit" class="btn btn-primary">确认保存</button>
					<a href="/admaspirineducation/push-notice/{{$oEducation->id}}" class="btn btn-default">返回</a>
				</div>
			</div>

			<br/><br/>
		</form>
	</div>
@stop
