@extends('admin.common.layout')
@section('content')
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4>健康教育</h4>
	</div>
	<div>
		<div style="float:right;margin-right:50px;margin-bottom:20px;margin-top:30px;">
		<button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirineducation/edu-list'">上传视频列表</button>
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirineducation/add-ppt'">新增PPT</button>
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirineducation/video-list'">视频审核</button>
		</div>
	</div>
	
	<table class="table table-striped ">
      <thead>
        <tr>
          <th>id</th>
          <th>主题</th>
          <th>所属科室</th>
          <th>缩略图</th>
          <th>上传时间</th>
          <th>查看ppt图片</th>
          <th>编辑</th>
          <th>删除</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oList as $k=>$v)
        <tr>
          <td>{{$v->id}}</td>
          <td>{{$v->ppt_title}}</td>
          <td>@if($v->catid == 1) 心内科 @else 神内科 @endif</td>
          <td><img src="{{$v->ppt_thumb}}" style="height:50px;"/></td>
          <td>{{$v->created_at}}</td>
          <td>
            <a href="/admaspirineducation/ppt-image/{{$v->id}}"><span class="glyphicon glyphicon-picture" aria-hidden="true"></a></span>
          </td>
          <td>
			<a href="/admaspirineducation/edit-ppt/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
          </td>
          <td>
          	<a href="#" onclick="del('/admaspirineducation/del-ppt/{{$v->id}}')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
		  </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div>{{$oList->links()}}</div>
</div>    
    <script>
    function del(url){
		if(confirm('您确定要删除该条记录吗？')){
			window.location.href = url;
		}
	}
    </script>
@stop
