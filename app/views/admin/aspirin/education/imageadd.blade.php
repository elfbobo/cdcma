@extends('admin.common.layout')
@section('content')
<script type="text/javascript" src="/assets/js/uploadpics/jquery.uploadify-3.1.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/js/uploadpics/uploadify.css"/>
<script src="/assets/js/ajaxfileupload.js"></script>
<script type="text/javascript">
var img_id_upload=new Array();//初始化数组，存储已经上传的图片名
var i=0;//初始化数组下标
$(function() {
    $('#file_upload_pics').uploadify({
    	'auto'     : false,//关闭自动上传
    	'debug'		: false,
    	'removeTimeout' : 1,//文件队列上传完成1秒后删除
        'swf'      : '/assets/js/uploadpics/uploadify.swf',
        'uploader' : '/admaspirineducation/upload-ppt-image/{{$iPptId}}',
        'method'   : 'post',//方法，服务端可以用$_POST数组获取数据
		'buttonText' : '选择图片',//设置按钮文本
        'multi'    : true,//允许同时上传多张图片
        'uploadLimit' : 10,//一次最多只允许上传10张图片
        'fileTypeDesc' : 'Image Files',//只允许上传图像
        'fileTypeExts' : '*.gif; *.jpg; *.png',//限制允许上传的图片后缀
        'fileSizeLimit' : '20000KB',//限制上传的图片不得超过200KB 
        'onUploadSuccess' : function(file, data, response) {//每次成功上传后执行的回调函数，从服务端返回数据到前端
               img_id_upload[i]=data;
               i++;
        },
        'onQueueComplete' : function(queueData) {//上传队列全部完成后执行的回调函数
           // if(img_id_upload.length>0)
           // alert('成功上传的文件有：'+encodeURIComponent(img_id_upload));
        }  
        // Put your options here
    });
});

</script>
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirineducation/list">健康教育</a>>>
		<a style="color:white;" href="/admaspirineducation/ppt-image/{{$iPptId}}">图片列表</a>>>
		批量上传PPT图片</h4>
	</div>
	<div class="panel-body">
		<br/><br/>
		<p class="orange">注：支持jpg,png格式图片，大小在5M以内</p>
		
		<input type="file" name="file_upload_pics" id="file_upload_pics" />
		<br/>
		<p><a href="javascript:$('#file_upload_pics').uploadify('settings', 'formData', {'typeCode':document.getElementById('id_file').value,'_token':$('#_token').val()});$('#file_upload_pics').uploadify('upload','*')">上传</a>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="javascript:$('#file_upload_pics').uploadify('cancel','*')">重置上传队列</a>
		</p>
		<input type="hidden" value="1215154" name="tmpdir" id="id_file">
		<input type="hidden" name="_token" id="_token" value="{{Session::token()}}"/>
	</div>
</div>   
@stop
