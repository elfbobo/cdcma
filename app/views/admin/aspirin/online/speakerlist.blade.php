@extends('admin.common.layout')
@section('content')
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirinonline/online-list">在线会议</a>>>会议报名讲者信息列表</h4>
	</div>
	<br />
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>讲者id</th>
          <th>讲者姓名</th>
          <th>讲者医院</th>
          <th style="text-align:center;">参会人(限10人)</th>
          <th style="text-align:center;">状态</th>
          <th style="text-align:center;">视频浏览量</th>
          <th style="text-align:center;">视频点赞数</th>
          <th style="text-align:center;">创建时间</th>
          <th style="text-align:center;">讲者报名详情</th>
          <th style="text-align:center;">听者列表</th>
          <th style="text-align:center;">导出签名</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oOnlineSpeaker as $k=>$v)
        <tr>
          <td>{{$v->id}}</td>
          <td>{{$v->speaker_id}}</td>
          <td>{{$v->doc_name}}</td>
          <td>{{$v->doc_hospital}}</td>
          <td style="text-align:center;">{{$v->listener_count}}</td>
          <td style="text-align:center;">@if($v->open_flag == 1)已开启 @elseif($v->cancel_flag == 1) 已取消 @else 待开启 @endif</td>
          <td style="text-align:center;">{{$v->video_hits}}</td>
          <td style="text-align:center;">{{$v->video_zan}}</td>
          <td style="text-align:center;">{{$v->created_at}}</td>
          <td style="text-align:center;"><a href="/admaspirinonline/speaker-show/{{$v->online_time_id}}/{{$v->id}}">查看</a></td>
          <td style="text-align:center;"><a href="/admaspirinonline/listener-list/{{$v->online_time_id}}/{{$v->speaker_id}}">查看</a></td>
          <th style="text-align:center;">
            @if($v->is_signed==1)
            <a href="{{$v->sign_icon}}" target="_blank" style="color:red;margin-right:8px;">导出</a>
            @else
            <?php
            $applycount = AspirinOnlineListener::where("online_time_id",$v->online_time_id)->get()->count();
            // $log = DB::getQueryLog();
            // print_r($log);die;
            $pushed = AspirinUserNotice::where("user_id", $v->speaker_id)
              ->where("notice_type", 160)
              ->where("detail_id", $v->online_time_id)
              ->get()->count();
            // $log = DB::getQueryLog();
            // print_r($log);die;
            if($applycount>=5){
              if(!$pushed){
                echo '<a href="/admaspirinonline/push-notice/'.$v->online_time_id.'" target="_blank" style="color:blue;">推送协议</a>';
              }else{
                echo '<a href="javascript:;" style="color:orange;">协议待签署</a>';
              }
            } ?>
            @endif
          </th>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div>{{$oOnlineSpeaker->links()}}</div>
</div>  
@stop
