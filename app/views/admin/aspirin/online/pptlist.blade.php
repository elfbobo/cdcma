@extends('admin.common.layout')
@section('content')
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirinonline/online-list">在线会议</a>>>PPT图片列表</h4>
	</div>
	<div>
		<div style="float:right;margin-right:50px;margin-bottom:20px;margin-top:20px;">
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirinonline/ppt-add/{{$iPptId}}'">批量新增图片</button>
		</div>
	</div>
	
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>ppt图片</th>
          <th>删除</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oPptImage as $k=>$v)
        <tr>
          <td>{{$v->id}}</td>
          <td><img src="{{$v->image_url}}" style="weight:50px;height:50px;"/></td>
          <td>
          	<a href="#" onclick="del('/admaspirinonline/ppt-delete/{{$v->id}}/{{$iPptId}}')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
		  </td>
        </tr>
        @endforeach
      </tbody>
    </table>
</div>    
<script>
    function del(url){
		if(confirm('您确定要删除该条记录吗？')){
			window.location.href = url;
		}
	}
</script>
@stop
