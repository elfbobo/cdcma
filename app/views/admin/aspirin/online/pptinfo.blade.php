@extends('admin.common.layout')
@section('content')
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirinonline/online-list">在线会议</a>>>会议主题课件</h4>
	</div>
	<div>
		<div style="float:right;margin-right:50px;margin-bottom:20px;margin-top:30px;">
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirinonline/pptinfo-add'">新增课件</button>
		</div>
	</div>
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>课件标题</th>
          <th>所属科室</th>
          <th>创建时间</th>
          <th>查看ppt图片</th>
          <th>编辑</th>
          <th>删除</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oPpt as $k=>$v)
        <tr>
          <td>{{$v->id}}</td>
          <td>{{$v->ppt_title}}</td>
          <td>@if($v->catid == 1) 心内科 @elseif($v->catid == 2) 神内科  @elseif($v->catid == 3) 全科  @elseif($v->catid == 4) 神外科 @endif</td>
          <td>{{substr($v->created_at,0,10)}}</td>
          <td>
            <a href="/admaspirinonline/ppt-list/{{$v->id}}"><span class="glyphicon glyphicon-picture" aria-hidden="true"></a></span>
          </td>
          <td>
			<a href="/admaspirinonline/pptinfo-edit/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
          </td>
          <td>
          	<a href="#" onclick="del('/admaspirinonline/pptinfo-delete/{{$v->id}}')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
		  </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div>{{$oPpt->links()}}</div>
</div>
<script>
    function del(url){
		if(confirm('您确定要删除该信息吗？')){
			window.location.href = url;
		}
	}
</script>    
@stop
