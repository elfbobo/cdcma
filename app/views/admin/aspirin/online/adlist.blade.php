@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>阿司匹林自助会议广告</h3>
	</div>
    <div>
        <form class="navbar-form navbar-left"  method="get" action="/admaspirinonline/ad-list">

        </form>
        <a href="/admaspirinonline/ad-add" class="btn btn-primary" style="margin-left:15px; float: right">上传广告</a>
        <div style="clear: both"></div>
    </div>

	<div>
	<table class="table table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>名称</th>
          <th>索引</th>
          <th style="width: 150px">创建时间</th>
          <th width="190" style="text-align: center">操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oVideos as $k=>$v)
        <tr>
          <td>{{$v->id}}</td>
          <td>{{$v->title}}</td>
            <td>{{$v->index}}</td>
            <td>{{$v->created_at}}</td>
          <td>
            <a href="{{$v->filepath}}" style="margin-left: 10px" target="_blank">预览</a>
            <a href="/admaspirinonline/ad-edit?id={{$v->id}}" style="margin-left: 10px">编辑</a>
            <a href="/admaspirinonline/ad-delete?id={{$v->id}}" onclick="return confirm('你确定要删除该视频吗?');" style="margin-left: 10px">删除</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
	{{$oVideos->appends($aLink)->links()}}
     
@stop
