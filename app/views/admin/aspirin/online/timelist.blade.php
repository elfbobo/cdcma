@extends('admin.common.layout')
@section('content')
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirinonline/online-list">在线会议</a>>>会议时间信息</h4>
	</div>
	<br />
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>主题id</th>
          <th>日期</th>
          <th>时间段</th>
          <th>报名讲课人数</th>
          <th>创建时间</th>
          <th>讲者列表</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oOnlineTime as $k=>$v)
        <tr>
          <td>{{$v->id}}</td>
          <td>{{$v->online_id}}</td>
          <td>{{$v->online_date}}</td>
          <td>{{$v->time_period}}</td>
          <td>{{$v->speaker_count}}</td>
          <td>{{substr($v->created_at,0,10)}}</td>
          <td>
			<a href="/admaspirinonline/speaker-list/{{$v->id}}">查看</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div>{{$oOnlineTime->links()}}</div>
</div>  
@stop
