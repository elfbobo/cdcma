@extends('admin.common.layout')
@section('content')
<script src="/assets/js/ajaxfileupload.js"></script>
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirinonline/online-list">在线会议</a>>>讲者申报会议详情（除视频地址外，讲者申报信息修改无效）</h4>
	</div>
	<br /><br />
	<form class="form-horizontal">
	  <div class="form-group">
	  	<label class="col-sm-1 control-label"><span style="color:red;">*</span>申报姓名</label>
	    <div class="col-sm-9">
	    	<input type="text" class="form-control" name="declare_name" value="{{$oOnlineSpeaker->declare_name}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-1 control-label"><span style="color:red;">*</span>身份证号</label>
	    <div class="col-sm-9">
	    	<input type="text" class="form-control" name="declare_id_number" value="{{$oOnlineSpeaker->declare_id_number}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-1 control-label"><span style="color:red;">*</span>开户行</label>
	    <div class="col-sm-9">
	    	<input type="text" class="form-control" name="declare_bank" value="{{$oOnlineSpeaker->declare_bank}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-1 control-label"><span style="color:red;">*</span>银行账号</label>
	    <div class="col-sm-9">
	    	<input type="text" class="form-control" name="declare_bank_number" value="{{$oOnlineSpeaker->declare_bank_number}}">
	    </div>
	  </div>
	  <div class="form-group">
		<label class="col-sm-1 control-label"><span style="color:red;">*</span>会议开启标志</label>
		<div class="col-sm-9">
			<label class="radio-inline">
				<input type="radio" name="open_flag"  value="0" @if($oOnlineSpeaker->open_flag==0) checked @endif> 未开启
			</label>
		    <label class="radio-inline">
				<input type="radio" name="open_flag"  value="1" @if($oOnlineSpeaker->open_flag==1) checked @endif> 已开启
			</label>
		</div>
	  </div>
	  <div class="form-group">
		<label class="col-sm-1 control-label"><span style="color:red;">*</span>取消会议标志</label>
		<div class="col-sm-9">
			<label class="radio-inline">
				<input type="radio" name="cancel_flag"  value="0" @if($oOnlineSpeaker->cancel_flag==0) checked @endif> 未取消
			</label>
		    <label class="radio-inline">
				<input type="radio" name="cancel_flag"  value="1" @if($oOnlineSpeaker->cancel_flag==1) checked @endif> 已取消
			</label>
		</div>
	  </div>
	</form>
	
	<form class="form-horizontal"  method="post" action="/admaspirinonline/speaker-edit">
	  <input type="hidden" class="form-control" id="iId" name="iId" value="{{$iId}}">
	  <input type="hidden" class="form-control" id="iTimeId" name="iTimeId" value="{{$oOnlineSpeaker->online_time_id}}">
	  <div class="form-group">
	    <label class="col-sm-1 control-label"><span style="color:red;">*</span>视频地址</label>
	    <div class="col-sm-9">
	    	<input type="text" class="form-control" id="video_url" name="video_url" value="{{$oOnlineSpeaker->video_url}}">
	    	<br /><span style="color:red;">注：此处链接为会畅返回的会议地址，用于精彩回顾视频，请填写完整链接</span>
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <div class="col-sm-offset-1 col-sm-10">
	        <button type="submit" class="btn btn-primary">确定</button>
	  	</div>
	  </div>
	</form>
</div>	
@stop


