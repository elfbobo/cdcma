@extends('admin.common.layout')
@section('content')
<script src="/assets/js/ajaxfileupload.js"></script>
<!-- date -->
<script src="/assets/js/DatePicker/WdatePicker.js" type="text/javascript"></script>
<link type="text/css" href="/assets/js/date/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<link type="text/css" href="/assets/js/date/css/jquery-ui-timepicker-addon.css" rel="stylesheet" />
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-timepicker-zh-CN.js"></script>
<script type="text/javascript">
    $(function () {
        $(".ui_timepicker").datetimepicker({
            showSecond: false,
            showMinute:false,
            showHour:false,
            timeFormat: '',
            stepHour: 1,
            stepMinute: 1,
            stepSecond: 1
        })
    })
</script>
<!-- date -->

<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirinonline/online-list">在线会议</a>>>修改会议主题</h4>
	</div>
	<br><br><br>
	
	<form class="form-horizontal"  method="post" action="/admaspirinonline/online-edit">
	  <input type="hidden" class="form-control" id="iId" name="iId" value="{{$iId}}">
	  <div class="form-group">
	  	<label class="col-sm-1 control-label"><span style="color:red;">*</span>主题</label>
	    <div class="col-sm-9">
	    	<input type="text" class="form-control" id="online_title" name="online_title" value="{{$oOnline->online_title}}">
	    </div>
	  </div>
	  <div class="form-group">
		<label class="col-sm-1 control-label"><span style="color:red;">*</span>所属科室分类</label>
		<div class="col-sm-9">
		    <label class="radio-inline">
				<input type="radio" name="catid" id="catid1" value="1" @if($oOnline->catid==1) checked @else disabled @endif> 心内科
			</label>
			<label class="radio-inline">
				<input type="radio" name="catid" id="catid2" value="2" @if($oOnline->catid==2) checked @else disabled @endif> 神内科
			</label>
			<label class="radio-inline">
				<input type="radio" name="catid" id="catid1" value="3" @if($oOnline->catid==3) checked @else disabled @endif> 全科
			</label>
			<label class="radio-inline">
				<input type="radio" name="catid" id="catid2" value="4" @if($oOnline->catid==4) checked @else disabled @endif> 神外科
			</label>
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-1 control-label"><span style="color:red;">*</span>简述</label>
	    <div class="col-sm-9">
	    	<input type="text" class="form-control" id="online_des" name="online_des" value="{{$oOnline->online_des}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-1 control-label"><span style="color:red;">*</span>banner图</label>
	    <div class="col-sm-9">
	    	<input  id="upload_file" name="upload_file"  type="file" onchange="saveOnlineBanner()"/>
	      	<input type="hidden" class="form-control" id="online_banner" name="online_banner" value="{{$oOnline->online_banner}}">
	      	<img style="width:200px;height:150px;margin-top:20px;" alt="" id="banner" src="{{$oOnline->online_banner}}"/>
	    </div>
	  </div>
	  <div class="form-group">
		<label class="col-sm-1 control-label"><span style="color:red;">*</span>是否在app展示标题和描述</label>
		<div class="col-sm-9">
			<label class="radio-inline">
				<input type="radio" name="show_flag" id="show_flag0" value="0" @if($oOnline->show_flag==0) checked @endif> 是
			</label>
		    <label class="radio-inline">
				<input type="radio" name="show_flag" id="show_flag1" value="1" @if($oOnline->show_flag==1) checked @endif> 否
			</label>
		</div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-1 control-label"><span style="color:red;">*</span>缩略图</label>
	    <div class="col-sm-9">
	    	<input  id="upload_file0" name="upload_file0"  type="file" onchange="saveOnlineThumb()"/>
	      	<input type="hidden" class="form-control" id="online_thumb" name="online_thumb" value="{{$oOnline->online_thumb}}">
	      	<img style="width:150px;height:150px;margin-top:20px;" alt="" id="thumb" src="{{$oOnline->online_thumb}}"/>
	    	<br /><span style="color:red;">注：缩略图为精彩回顾列表页所用</span>
	    </div>
	  </div>
	  <div class="form-group">
		<label class="col-sm-1 control-label"><span style="color:red;">*</span>展示开始时间</label>
		<div class="col-sm-9">
			<input type="text" class="form-control ui_timepicker" id="show_start_time" name="show_start_time" value="{{substr($oOnline->show_start_time,0,10)}}">
	  		<br /><span style="color:red;">注：此处录入的时间段规定了会议展示的始末时间，每月的展示开始时间请以月初第一个整周的周一开始</span>
		</div>
	  </div>
	  <div class="form-group">
		<label class="col-sm-1 control-label"><span style="color:red;">*</span>展示结束时间</label>
		<div class="col-sm-9">
			<input type="text" class="form-control ui_timepicker" id="show_end_time" name="show_end_time" value="{{substr($oOnline->show_end_time,0,10)}}">
		</div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-1 control-label"><span style="color:red;">*</span>会议时间段</label>
	    <div class="col-sm-9">
	    	<input type="text" class="form-control" id="time_period" name="time_period" value="{{$sTimePeriod}}">
	    	<br /><span style="color:red;">注：多个时间段以英文格式逗号‘,’隔开，填写格式：11:00-12:30,15:00-16:00</span>
	    </div>
	  </div>
	  <div class="form-group">
	    <div class="col-sm-offset-1 col-sm-10">
	    	<button type="submit" class="btn btn-primary">确定</button>
	    </div>
	  </div>
	</form>
</div>	
<script>
	function saveOnlineBanner(){
		$.ajaxFileUpload({
			url: "/admaspirinonline/upload-online-banner",
			secureuri: false,
			fileElementId: "upload_file",
			dataType: "json",
			success: function(data, status) {
				$("#online_banner").val(data.online_banner);
				$("#banner").attr("src", data.online_banner);
			}
		})
	}
	function saveOnlineThumb(){
		$.ajaxFileUpload({
			url: "/admaspirinonline/upload-online-thumb",
			secureuri: false,
			fileElementId: "upload_file0",
			dataType: "json",
			success: function(data, status) {
				$("#online_thumb").val(data.online_thumb);
				$("#thumb").attr("src", data.online_thumb);
			}
		})
	}
</script>
@stop