@extends('admin.common.layout')
@section('content')
<style>
<!--
.form-group p select{margin-left:24px;}
-->
</style>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>编辑自助会议广告</h3>
	</div>
	<br/>
	<form class="form-horizontal"  method="post" action="/admaspirinonline/ad-edit">

	  <div class="form-group">
	    <label for="user_nick" class="col-sm-2 control-label"><span style="color:red;">*</span>广告名称</label>
	    <div class="col-sm-9">
	      <input type="text" class="form-control" name="title" required value="{{$oInfo->title}}" >
	    </div>
	  </div>

		<div class="form-group">
			<label for="user_nick" class="col-sm-2 control-label"><span style="color:red;">*</span>广告索引</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" name="index" required value="{{$oInfo->index}}" >
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">广告视频(不上传时不修改)</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" name="filepath" >
				<input id="upload_file" name="upload_file" type="file" onchange="saveMaterial()"/>
			</div>
		</div>

		<div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
			<input type="hidden" name="id" value="{{$id}}">
	      <button type="submit" class="btn btn-default"> 提交 </button>
	    </div>
	  </div>
	</form>

<script src="/assets/js/ajaxfileupload.js"></script>
<script>
    function saveMaterial(){
        $.ajaxFileUpload({
            url: "/admaspirinonline/ad-upload",
            secureuri: false,
            fileElementId: "upload_file",
            dataType: "json",
            success: function(data, status) {
                if (data.status) {
                    if ($('input[name="title"]').val() == '') {
                        $('input[name="title"]').val(data.file_name);
                    }
                    $('input[name="filepath"]').val(data.file_url);
                    alert('上传成功');
                } else {
                    alert('上传失败');
				}
            }
        })
    }
</script>
@stop


