@extends('admin.common.layout')
@section('content')
<script src="/assets/js/ajaxfileupload.js"></script>
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirinonline/online-list">在线会议</a>>>新增首页banner</h4>
	</div>
	<br><br><br>
	
	<form class="form-horizontal"  method="post" action="/admaspirinonline/banner-add">
	  <div class="form-group">
	  	<label class="col-sm-1 control-label"><span style="color:red;">*</span>主题</label>
	    <div class="col-sm-9">
	    	<input type="text" class="form-control" id="banner_title" name="banner_title" >
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-1 control-label"><span style="color:red;">*</span>简述</label>
	    <div class="col-sm-9">
	    	<input type="text" class="form-control" id="banner_des" name="banner_des" >
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-1 control-label"><span style="color:red;">*</span>图片</label>
	    <div class="col-sm-9">
	    	<input  id="upload_file" name="upload_file"  type="file" onchange="saveBannerThumb()"/>
	      	<input type="hidden" class="form-control" id="banner_thumb" name="banner_thumb" >
	      	<img style="width:200px;height:160px;margin-top:20px;" alt="" id="thumb" src=""/>
	    </div>
	  </div>
	  <div class="form-group">
		<label class="col-sm-1 control-label"><span style="color:red;">*</span>是否在app展示标题和描述</label>
		<div class="col-sm-9">
			<label class="radio-inline">
				<input type="radio" name="show_flag" id="show_flag0" value="0" checked> 是
			</label>
		    <label class="radio-inline">
				<input type="radio" name="show_flag" id="show_flag1" value="1"> 否
			</label>
		</div>
	  </div>
	  <div class="form-group">
	    <div class="col-sm-offset-1 col-sm-10">
	    	<button type="submit" class="btn btn-primary">确定</button>
	    </div>
	  </div>
	</form>
</div>	
<script>
	function saveBannerThumb(){
		$.ajaxFileUpload({
			url: "/admaspirinonline/upload-banner",
			secureuri: false,
			fileElementId: "upload_file",
			dataType: "json",
			success: function(data, status) {
				$("#banner_thumb").val(data.banner_thumb);
				$("#thumb").attr("src", data.banner_thumb);
			}
		})
	}
</script>
@stop