@extends('admin.common.layout')
@section('content')
<script src="/assets/js/ajaxfileupload.js"></script>
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirinonline/online-list">在线会议</a>>>修改会议课件</h4>
	</div>
	<br><br><br>
	<form class="form-horizontal"  method="post" action="/admaspirinonline/pptinfo-edit-do/{{$oPpt->id}}">
		<div class="form-group">
		    <label for="ppt_title" class="col-sm-1 control-label"><span style="color:red;">*</span>课件标题</label>
		    <div class="col-sm-9">
		    	<input type="text" class="form-control" id="ppt_title" name="ppt_title" value="{{$oPpt->ppt_title}}">
		    </div>
	  	</div>
	  	<div class="form-group">
			<label class="col-sm-1 control-label"><span style="color:red;">*</span>所属科室分类</label>
		    <div class="col-sm-9">
		    	<label class="radio-inline">
					<input type="radio" name="catid" id="catid1" value="1" @if($oPpt->catid==1) checked @else disabled @endif> 心内科
				</label>
				<label class="radio-inline">
				    <input type="radio" name="catid" id="catid2" value="2" @if($oPpt->catid==2) checked @else disabled @endif> 神内科
				</label>
				<label class="radio-inline">
				    <input type="radio" name="catid" id="catid2" value="2" @if($oPpt->catid==3) checked @else disabled @endif> 全科
				</label>
				<label class="radio-inline">
				    <input type="radio" name="catid" id="catid2" value="2" @if($oPpt->catid==4) checked @else disabled @endif> 神外科
				</label>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="upload_file" class="col-sm-1 control-label"><span style="color:red;">*</span>缩略图</label>
		    <div class="col-sm-9">
		    	<input  id="upload_file0" name="upload_file0"  type="file" onchange="saveThumb()"/>
		      	<input type="hidden" class="form-control" id="ppt_thumb" name="ppt_thumb" value="{{$oPpt->ppt_thumb}}">
		      	<img style="width:200px;height:160px;margin-top:20px;" alt="" id="thumb" src="{{$oPpt->ppt_thumb}}"/>
		    </div>
	  	</div>
	    <div class="form-group">
		    <label class="col-sm-1 control-label"><span style="color:red;">*</span>上传ppt</label>
		    <div class="col-sm-9">
		    	<input  id="upload_file_ppt" name="upload_file_ppt" type="file" onchange="savePpt()"/>
		    </div>
	    </div>
	    <div class="form-group">
		    <label class="col-sm-1 control-label"><span style="color:red;">*</span>ppt地址</label>
		    <div class="col-sm-9">
		        <input type="text" class="form-control" id="ppt_url" name="ppt_url" value="{{$oPpt->ppt_url}}">
		    </div>
	    </div>
	  	<div class="form-group">
		    <div class="col-sm-offset-1 col-sm-10">
		    	<button type="submit" class="btn btn-primary">确定</button>
		    </div>
	  	</div>
	</form>
</div>	
<script>
	function saveThumb() {
		$.ajaxFileUpload({
			url: "/admaspirinonline/upload-online-thumb",
			secureuri: false,
			fileElementId: "upload_file0",
			dataType: "json",
			success: function(data, status) {
				$("#ppt_thumb").val(data.online_thumb);
				$("#thumb").attr("src", data.online_thumb);
			}
		})
	}
	function savePpt() {
		$.ajaxFileUpload({
			url: "/admaspirinonline/upload-ppt",
			secureuri: false,
			fileElementId: "upload_file_ppt",
			dataType: "json",
			success: function(data, status) {
				$("#ppt_url").val(data.url);
				alert("上传成功");
			}
		})
	}
</script>
@stop