@extends('admin.common.layout')
@section('content')
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirinonline/online-list">在线会议</a>>>会议报名听者信息列表</h4>
	</div>
	<br />
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>听者id</th>
          <th>听者姓名</th>
          <th>听者所在医院</th>
          <th>是否已参加</th>
          <th>参会类型</th>
          <th>会议邀请状态</th>
          <th>创建时间</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oOnlineListener as $k=>$v)
        <tr>
          <td>{{$v->id}}</td>
          <td>{{$v->listener_id}}</td>
          <td>{{$v->doc_name}}</td>
          <td>{{$v->doc_hospital}}</td>
          <td>@if($v->has_attend == 0)未参加 @else 已参加过 @endif</td>
          <td>@if($v->join_type == 0)报名参加 @else 被邀请 @endif</td>
          <td>@if($v->is_agree == 1)已同意 @elseif($v->is_agree == 2) 已忽略 @else 未处理 @endif</td>
          <td>{{$v->created_at}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div>{{$oOnlineListener->links()}}</div>
</div>  
@stop
