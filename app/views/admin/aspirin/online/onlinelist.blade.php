@extends('admin.common.layout')
@section('content')
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirinonline/online-list">在线会议</a>>>会议主题列表</h4>
	</div>
	<div>
		<div style="float:right;margin-right:50px;margin-bottom:20px;margin-top:30px;">
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirinonline/banner-list'">在线会议首页banner信息</button>
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirinonline/ad-list'">会议广告</button>
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirinonline/online-add'">新增会议主题</button>
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirinonline/ppt'">查看全部课件</button>
		</div>
	</div>
	<br />
	<p style="color:red;margin-top:40px;">注：为方便会议信息展示，请在每个月末及时添加下个月会议信息</p>
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>主题</th>
          <th>所属科室</th>
          <th>展示时间段</th>
          <th>创建时间</th>
          <th>编辑</th>
          <th>删除</th>
          <th>会议时间列表</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oOnlineList as $k=>$v)
        <tr>
          <td>{{$v->id}}</td>
          <td>{{$v->online_title}}</td>
          <td>@if($v->catid == 1) 心内科 @elseif($v->catid == 2) 神内科  @elseif($v->catid == 3) 全科  @elseif($v->catid == 4) 神外科 @endif</td>
          <td>{{substr($v->show_start_time,0,10)}}至{{substr($v->show_end_time,0,10)}}</td>
          <td>{{substr($v->created_at,0,10)}}</td>
          <td>
			<a href="/admaspirinonline/online-edit/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
          </td>
          <td>
          	<a href="#" onclick="del('/admaspirinonline/online-delete/{{$v->id}}')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
		  </td>
          <td>
			<a href="/admaspirinonline/time-list/{{$v->id}}">查看</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div>{{$oOnlineList->links()}}</div>
</div>
<script>
    function del(url){
		if(confirm('您确定要删除该信息吗？')){
			window.location.href = url;
		}
	}
</script>    
@stop
