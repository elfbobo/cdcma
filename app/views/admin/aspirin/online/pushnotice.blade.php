@extends('admin.common.layout')
@section('content')
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirinonline/online-list">在线会议</a>>>会议报名讲者信息列表>>推送讲者协议</h4>
	</div>
	<br/><br/>
	<form class="form-horizontal" method="post" action="/admaspirineducation/push-notice-check/{{$oOnline->online_time_id}}">
		<div class="form-group">
			<label for="user_cwid" class="col-sm-2 control-label">讲者姓名</label>
			<div class="col-sm-3">
				<input type="text" disabled class="form-control" value="{{$oUser->user_name}}" >
			</div>
		</div>

	  	<div class="form-group">
	    	<label for="user_cwid" class="col-sm-2 control-label">标题</label>
		    <div class="col-sm-9">
		    	<input type="hidden" name="user_id" value="{{$oOnline->speaker_id}}">
		    	<input type="hidden" name="notice_type" value="160">
		    	<input type="hidden" name="detail_id" value="{{$oOnline->id}}">
				<input type="text" class="form-control" name="notice_content" value="已有6人报名参加了您于<?php echo date("Y年m月d日 ", strtotime($oOnlineTime->online_date))."{$oOnlineTime->time_period}"; ?>的课程，特此邀您签署《讲师讲课服务协议》">
		    </div>
	  	</div>

	  	<div class="form-group">
	    	<label for="user_cwid" class="col-sm-2 control-label">协议内容</label>
		    <div class="col-sm-9"><textarea class="form-control" name="agreement_txt" style="height:400px;" name="agreement_txt"></textarea></div>
	  	</div>
	  	<div class="form-group">
		    <div class="col-sm-offset-2 col-sm-9">
		    	<button type="submit" class="btn btn-primary" onclick="submitCheck()">确认提醒</button>
		    </div>
	  	</div>
	  
	  <br/><br/>
    </form>
</div>
@stop
