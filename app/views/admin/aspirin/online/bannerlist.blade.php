@extends('admin.common.layout')
@section('content')
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirinonline/online-list">在线会议</a>>>首页banner信息</h4>
	</div>
	<div>
		<div style="float:right;margin-right:50px;margin-bottom:20px;margin-top:30px;">
			<button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirinonline/banner-add'">新增banner</button>
		</div>
	</div>
	<br />
	<p style="color:red;margin-top:40px;">注：在线会议首页banner只展示最新一条信息</p>
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>主题</th>
          <th>简述</th>
          <th>图片</th>
          <th>创建时间</th>
          <th>编辑</th>
          <th>删除</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oBannerList as $k=>$v)
        <tr>
          <td>{{$v->id}}</td>
          <td>{{$v->banner_title}}</td>
          <td>{{$v->banner_des}}</td>
          <td><img src="{{$v->banner_thumb}}" style="weight:100px;height:50px;"/></td>
          <td>{{substr($v->created_at,0,10)}}</td>
          <td>
			<a href="/admaspirinonline/banner-edit/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
          </td>
          <td>
          	<a href="#" onclick="del('/admaspirinonline/banner-delete/{{$v->id}}')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
		  </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div>{{$oBannerList->links()}}</div>
</div>
<script>
    function del(url){
		if(confirm('您确定要删除该banner信息吗？')){
			window.location.href = url;
		}
	}
</script>    
@stop
