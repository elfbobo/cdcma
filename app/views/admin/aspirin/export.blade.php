@extends('admin.common.layout')
@section('content')
<div class="app_content_div">
	<h2 class="sub-header">专项基金周报告导出</h2>
	<div class="table-responsive">
		<table class="table table-striped"></table>
		<table class="table table-striped">
			<thead></thead>
			<tbody>
                <tr>
                  	<td style="width:90%;">专项基金入组用户信息导出(不包含参与过在线会议的用户)</td>
                	<td><a href="/admaspirin/export-user"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td>在线会议详情导出</td>
                	<td><a href="/admaspirin/export-online"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td>健康教育录制详情导出(录制时长为0的可根据视频地址手动统计,新版app会加入时长一项)</td>
                	<td><a href="/admaspirin/export-education"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td>科研申请和筛查记录导出(累计数据)</td>
                	<td><a href="/admaspirin/export-apply-screening"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td>科研申请和筛查记录导出(上周数据)</td>
                	<td><a href="/admaspirin/export-apply-screening/1"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <!--  
				<tr>
                  	<td>科研申请记录详情导出</td>
                	<td><a href="/admaspirinresearch/export-research"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td>筛查记录详情导出</td>
                	<td><a href="/admaspirin/export-screening"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                -->
			</tbody>
		</table>
	</div>
</div>
@stop