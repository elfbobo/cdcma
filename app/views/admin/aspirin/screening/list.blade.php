@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>心血管风险筛查报告统计</h3>
	</div>
	<form class="navbar-form navbar-left"  method="get" action="/admaspirinscreening/list">
		<div class="form-group">
	  		<span>用户CWID：</span>
	    	<input type="text" class="form-control" id="name" name="name">
	  	</div>
	  	<button type="submit" class="btn btn-default" style="margin-left:77px;">检索</button>
	</form>
	<div style="float:right;">
		<button type="button" class="btn btn-primary" onclick="window.location.href='/admaspirinscreening/export-screening'">导出筛查记录</button>
	</div>
	<table class="table table-striped">
      <thead>
        <tr>
        	<th>序号</th>
          	<th>用户id</th>
          	<th>用户CWID</th>
          	<th>用户姓名</th>
          	<th>app上使用次数</th>
          	<th>分享使用次数</th>
          	<th>创建时间</th>
          	<th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oScreeningLog as $k=>$v)
        <tr>
        	<td>{{$v->id}}</td>
          	<td>{{$v->user_id}}</td>
          	<td>{{$v->user_cwid}}</td>
          	<td>{{$v->user_name}}</td>
          	<td>{{$v->app_use_count}}</td>
          	<td>{{$v->share_use_count}}</td>
          	<td>{{$v->created_at}}</td>
          	<td><a href="/admaspirinscreening/show/{{$v->user_id}}">查看详情</a></td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oScreeningLog ->links()}}
@stop
