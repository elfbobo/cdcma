@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>心血管风险筛查报告统计详情</h3>
	</div>
	
	<table class="table table-striped">
      <thead>
        <tr>
        	<th>序号</th>
          	<th>性别</th>
          	<th>年龄</th>
          	<th>身高(cm)</th>
          	<th>体重(kg)</th>
          	<th>身体质量指数</th>
          	<th>风险评分</th>
          	<th>风险百分比</th>
          	<th>风险等级</th>
          	<th>使用设备</th>
          	<th>创建时间</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oScreening as $k=>$v)
        <tr>
        	<td>{{$v->id}}</td>
          	<td>@if($v->sex == 1)男 @else 女 @endif</td>
          	<td>{{$v->age}}</td>
          	<td>{{$v->height}}</td>
          	<td>{{$v->weight}}</td>
          	<td>{{$v->bmi}}</td>
          	<td>{{$v->score}}</td>
          	<td>{{$v->percent}}</td>
          	<td>@if($v->flag == 3) 高 @elseif($v->flag == 2) 中 @else 低 @endif</td>
          	<td>@if($v->device_type == 0) app @else 分享页 @endif</td>
          	<td>{{substr($v->created_at,0,10)}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oScreening->links()}}
@stop
