@extends('admin.common.layout')
@section('content')
<style type="text/css">
<!--
.itemsBoxs { background:#F1F1F1;border-radius:8px;padding:10px 15px;font-size:16px;font-weight:bold;margin-bottom:20px; }
.txt-left { text-align:left !important;font-weight:20; }
-->
</style>
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4><a style="color:white;" href="/admaspirinuser/list">医师认证用户列表</a></h4>
	</div>
	<br/><br/>
	<form class="form-horizontal" method="post" action="/admaspirinuser/check/{{$oUser->id}}">
		<div class="form-group">
		    <label class="col-sm-2 control-label">医生姓名</label>
		    <div class="col-sm-9">
		    	<input type="text" disabled class="form-control" value="{{$oUser->user_name}}" >
		    </div>
	  	</div>
	  	<div class="form-group">
	    	<label class="col-sm-2 control-label">省份</label>
	    	<div class="col-sm-9">
	        	<input type="text" disabled class="form-control" value="{{$sProvince}}" >
	        </div>
	    </div>
	    <div class="form-group">
	    	<label class="col-sm-2 control-label">城市</label>
	    	<div class="col-sm-9">
	        	<input type="text" disabled class="form-control" value="{{$sCity}}" >
	        </div>
	    </div>
	    <div class="form-group">
	    	<label class="col-sm-2 control-label">区县</label>
	    	<div class="col-sm-9">
	        	<input type="text" disabled class="form-control" value="{{$sCounty}}" >
	        </div>
	    </div>
	    <div class="form-group">
	    	<label class="col-sm-2 control-label">医院</label>
	    	<div class="col-sm-9">
	        	<input type="text" disabled class="form-control" value="{{$sHospital}}" >
	        </div>
	    </div>
	  	<div class="form-group">
	    	<label class="col-sm-2 control-label">科室</label>
	    	<div class="col-sm-9">
	        	<input type="text" disabled class="form-control" value="{{$oUser->user_department}}" >
	        </div>
	    </div>
	    <div class="form-group">
	    	<label class="col-sm-2 control-label">职称</label>
	    	<div class="col-sm-9">
	        	<input type="text" disabled class="form-control" value="{{$oUser->user_position}}" >
	        </div>
	    </div>
	    <div class="form-group">
	    	<label class="col-sm-2 control-label">手机号码</label>
	    	<div class="col-sm-9">
	        	<input type="text" disabled class="form-control" value="{{$oUser->user_mobile}}" >
	        </div>
	    </div>
	    <div class="form-group">
	    	<label class="col-sm-2 control-label">证件号</label>
	    	<div class="col-sm-9">
	        	<input type="text" disabled class="form-control" value="{{$oUser->card_number}}" >
	        </div>
	    </div>
	    <div class="form-group">
		    <label for="upload_file" class="col-sm-2 control-label">证件照</label>
		    <div class="col-sm-9">
		    	<img style="width:250px;margin-top:5px;border:1px solid #CCC;" src="{{@$oUser->card_thumb?$oUser->card_thumb:''}}"/>
		    </div>
	    </div>
		<div class="col-sm-11 itemsBoxs" style="margin:20px;">付款信息</div>
		<div class="form-group">
		    <label class="col-sm-2 control-label">真实姓名</label>
		    <div class="col-sm-9">
		    	<input type="text" disabled class="form-control" value="{{@$sBankinfo->bank_realname}}" >
		    </div>
	  	</div>
	  	<div class="form-group">
	    	<label class="col-sm-2 control-label">身份证号码</label>
	    	<div class="col-sm-9">
	        	<input type="text" disabled class="form-control" value="{{@$sBankinfo->bank_idcard_code}}" >
	        </div>
	    </div>
	    <div class="form-group">
	    	<label class="col-sm-2 control-label">开户行名称</label>
	    	<div class="col-sm-9">
	        	<input type="text" disabled class="form-control" value="{{@$sBankinfo->bank_name}}" >
	        </div>
	    </div>
	    <div class="form-group">
	    	<label class="col-sm-2 control-label">用户账号</label>
	    	<div class="col-sm-9">
	        	<input type="text" disabled class="form-control" value="{{@$sBankinfo->bank_account}}" >
	        </div>
	    </div>
	    <div class="col-sm-11 itemsBoxs" style="margin:20px;">审核操作</div>
	    <div class="form-group" style="margin-bottom:30px;">
		    <label for="upload_file" class="col-sm-2 control-label">协议签名图片</label>
		    <div class="col-sm-9">
		      	<img style="width:250px;margin-top:5px;border:1px solid #CCC;" src="{{@$oUser->sign_icon?$oUser->sign_icon:''}}"/>
		    </div>
	    </div>
	    <div class="form-group">
		  	<label class="col-sm-2 control-label">规范该用户所在医院：</label>
		   	<select name="change_hospital" id="change_hospital" class="form-control col-sm-9" style="width:300px;">  
		   	  <option value ="0">=请选择=</option>
		   	  @foreach($aHospital as $k=>$v) 
			  <option value ="{{$k}}">{{$v}}</option>
			  @endforeach 
			</select>
			<label style="color:red;margin-left:10px;" class="control-label">注：如果用户医院信息无需规范修改，请将此选择框中的内容置为"=请选择="状态！</label> 
	    </div>
	    <div class="form-group">
		    <label for="card_auth_flag" class="col-sm-2 control-label">审核</label>
		    <div class="col-sm-9">
				<label class="radio-inline">
				    <input type="radio" name="card_auth_flag" value="2" @if($oUser->card_auth_flag==2) checked @endif> 审核通过
				</label>
				<label class="radio-inline">
				    <input type="radio" name="card_auth_flag" value="3" @if($oUser->card_auth_flag==3) checked @endif> 审核不通过
				</label>
		    </div>
	    </div>
	    <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-9">
		        <button type="submit" class="btn btn-primary">提交审核结果</button>
		    </div>
	    </div>
    </form>
</div>
@stop
