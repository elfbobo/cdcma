@extends('admin.common.layout')
@section('content')
<div class="panel panel-primary" style="min-height: 680px;">
	<div class="panel-heading">
		<h4>医师认证用户列表</h4>
	</div>
    <form class="navbar-form navbar-left" action="" method="get" style="margin-top:30px;margin-bottom:30px;">
      <div class="form-group">
        <span>姓名：</span>
        <input type="text" class="form-control" name="user_name" value="{{Input::get('user_name','')}}" placeholder="">
      </div>
      <div class="form-group" style="padding-left: 10px;">
        <span>手机号：</span>
        <input type="text" class="form-control" name="user_tel" value="{{Input::get('user_tel','')}}" placeholder="">
      </div>
      <div class="form-group" style="padding-left: 10px;">
        <span>医院：</span>
        <input type="text" class="form-control" name="user_company" value="{{Input::get('user_company','')}}" placeholder="">
      </div>
      <button type="submit" class="btn btn-default">检索</button>
    </form>
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>用户名</th>
          <th>注册编码</th>
          <th>用户姓名</th>
          <th>省份</th>
          <th>城市</th>
          <th>区县</th>
          <th>科室</th>
          <th>职称</th>
          <th>手机号码</th>
          <!--<th>医院</th>-->
          <th>审核状态</th>
          <th>审核</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oUser as $k=>$v)
        <tr>
          <th scope="row">{{$v->id}}</th>
          <td>{{$v->user_nick}}</td>
          <td>{{$v->invite_code}}</td>
          <td>{{$v->user_name}}</td>
          <td>{{$v->user_province}}</td>
          <td>{{$v->user_city}}</td>
          <td>{{$v->user_county}}</td>
          <td>{{$v->user_department}}</td>
          <td>{{$v->user_position}}</td>
          <td>{{$v->user_mobile?$v->user_mobile:'--'}}</td>
          <!--<td>{{$v->hospital_name?$v->hospital_name:'--'}}</td>-->
          <td>@if($v->card_auth_flag == 1)
            未审核
          @elseif($v->card_auth_flag == 2)
            审核通过
          @elseif($v->card_auth_flag == 3)
            审核不通过
          @else
          @endif</td>	
          <td>
              @if($v->sign_icon && $v->card_auth_flag == 2)
              <a href="{{$v->sign_icon}}" target="_blank" style="color:red;margin-right:8px;">导出签名</a>
              @endif
            <a href="/admaspirinuser/detail/{{$v->id}}" >审核</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div>{{$oUser->appends($params)->links()}}</div>
</div>
@stop
