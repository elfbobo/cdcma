@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>空中课堂--{{$sVideoTitle}}--调研列表</h3>
	</div>
	<div style="float:right;">
		<button type="button" class="btn btn-primary" onclick="window.location.href='/admdocfacerec/add-survey/{{$iVid}}'">新增题目</button>
	</div>
	<div class="form-group">
	    <label for="title" class="col-sm-2 control-label">调研链接地址<span style="color:red;">*</span></label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" readonly value="http://cdcma.bizconf.cn/docface/survey-pre/{{$iVid}}">
	   		<span style="color:red">(请拷贝当前地址并生成相应二维码)</span>
	    </div>
	</div>
	<br>
	<br><br><br>
	@if(!in_array(Auth::User()->id,Config::get("config.docface")))
	<div>
		<button type="button" class="btn btn-primary" onclick="window.location.href='/admdocfacerec/sign-in-log/{{$iVid}}'">导出签到记录</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		
		<!--<button type="button" class="btn btn-primary" onclick="window.location.href='/admdocfacerec/survey-log/{{$iVid}}'">导出调研记录</button>
	--></div>
	@endif
	<table class="table table-striped">
	              <thead>
	                <tr>
			<th>题号</th>
			<th>题目标题</th>
			<th>题目类型</th>
			<th>正确答案</th>
			<th>排序位</th>
			<th>添加时间</th>
			<th>编辑/删除</th>
		</tr>
		 </thead>
	              <tbody>
		@foreach($oQuestions as $k=>$v)
		<tr id="list_{{$v->id}}">
			<td>{{$k+1}}</td>
			<td style="width:700px;">{{$v->title}}</td>
			<td>@if($v['type']==1) 单选 @else 多选  @endif</td>
			<td>{{$v->result}}</td>
			<td>{{$v->listorder}}</td>
			<td>{{$v->created_at}}</td>
			<td>
	  			<a href="/admdocfacerec/edit-survey/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> </a>&nbsp;&nbsp;&nbsp;
				<a href="/admdocfacerec/del-survey/{{$v->id}}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> </a>
			</td>
		</tr>
		@endforeach
		</tbody>
	</table>
@stop
