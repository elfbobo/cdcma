@extends('admin.common.layout')
@section('content')
<!-- 配置文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="/assets/js/ueditor/lang/zh-cn/zh-cn.js"></script>
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/video.js"></script>
<!-- date -->
<script src="/assets/js/DatePicker/WdatePicker.js" type="text/javascript"></script>
<link type="text/css" href="/assets/js/date/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<link type="text/css" href="/assets/js/date/css/jquery-ui-timepicker-addon.css" rel="stylesheet" />
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-timepicker-zh-CN.js"></script>
<script type="text/javascript">
$(function () {
    $(".ui_timepicker").datetimepicker({
        showSecond: true,
        timeFormat: 'hh:mm:ss',
        stepHour: 1,
        stepMinute: 1,
        stepSecond: 1
    })
})
</script>
<style type="text/css">
<!--
.checkboxs_choose_boxs label { padding-left:0px !important; }
-->
</style>
<!-- date -->
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>编辑音频</h3>
	</div>
	
	<form class="form-horizontal"  method="post" action="/admdocfacerec/edit-audio-do/{{$oFaceVideo->id}}">
	  <div class="form-group">
	    <label for="video_title" class="col-sm-2 control-label"><span style="color:red;">*</span>标题</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="video_title" name="video_title" value="{{$oFaceVideo->video_title}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="channel_type" class="col-sm-2 control-label"><span style="color:red;">*</span>音频来源</label>
	    <div class="col-sm-10">
			<label class="radio-inline">
			  <input type="radio" name="channel_type" id="channel_type2" value="2" disabled @if($oFaceVideo->channel_type==2) checked @endif> 目睹
			</label>
	      	<label class="radio-inline">
			  <input type="radio" name="channel_type" id="channel_type1" value="1"  @if($oFaceVideo->channel_type==1) checked @endif> 展视
			</label>
			<label class="radio-inline">
			  <input type="radio" name="channel_type" id="channel_type3" value="3" disabled @if($oFaceVideo->channel_type==3) checked @endif> 会畅返回地址
			</label>
	    </div>
	  </div>
	  <div class="form-group" style="display:none;">
	    <label for="video_type" class="col-sm-2 control-label">音频类型</label>
	    <div class="col-sm-10">
	      	<label class="radio-inline">
			  <input type="radio" name="video_type" id="video_type1" value="1" @if($oFaceVideo->video_type==1) checked @endif> 录播
			</label>
			<label class="radio-inline">
			  <input type="radio" name="video_type" id="video_type2" value="2" @if($oFaceVideo->video_type==2) checked @endif> 直播
			</label>
			<label class="radio-inline">
			  <input type="radio" name="video_type" id="video_type3" value="3" @if($oFaceVideo->video_type==3) checked @endif> 音频
			</label>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="department_id" class="col-sm-2 control-label">科室</label>
	    <div class="col-sm-10 checkboxs_choose_boxs">
	      	<!-- <label class="radio-inline">
			  <input type="checkbox" name="department_id[]" id="departmeny_0" value="0" @if($oFaceVideo->department_id==0) checked @endif> 默认科室
			</label> -->
			<?php 
			$aDepartment = Config::get('config.department');
			?>
			@foreach($aDepartment as $k=>$v)
			<label class="radio-inline">
			  <input type="checkbox" name="department_id[]" id="departmeny_{{$k}}" value="{{$k}}" @if(strpos($oFaceVideo->department_id, $k.'|') !== false) checked @endif> {{$v}}
			</label>
			@endforeach
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="video_permission" class="col-sm-2 control-label"><span style="color:red;">*</span>音频观看权限</label>
	    <div class="col-sm-10">
	      	<label class="radio-inline">
			  <input type="radio" name="video_permission" id="video_permission1" value="0" @if($oFaceVideo->video_permission==0) checked @endif> 所有人可见
			</label>
			<label class="radio-inline">
			  <input type="radio" name="video_permission" id="video_permission2" value="1" @if($oFaceVideo->video_permission==1) checked @endif> 仅代表可见
			</label>
	    </div>
	  </div>	  
	  <div class="form-group" style="display: none;">
	    <label for="video_id" class="col-sm-2 control-label"><span style="color:red;">*</span>目睹直播地址id</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="video_id" name="video_id" value="{{$oFaceVideo->video_id}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="video_url" class="col-sm-2 control-label">音频路径</label>
		<div class="col-sm-10">
			<div class="row">
				<div class="col-sm-8">
					<input type="text" class="form-control" id="video_url" name="video_url" value="{{$oFaceVideo->video_url}}">
				</div>
				<div class="col-sm-3" style="padding: 0">
					<input id="upload_file" name="upload_file" type="file" style="display: none;" onchange="saveAudio()"/>
					<button type="button" class="btn btn-primary" onclick="document.getElementById('upload_file').click()">上传音频</button>
				</div>
			</div>
		</div>
	  </div>
	  <div class="form-group">
	    <label for="video_download_url" class="col-sm-2 control-label">音频下载地址</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="video_download_url" name="video_download_url" value="{{$oFaceVideo->video_download_url}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="video_download_psw" class="col-sm-2 control-label">音频下载密码</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="video_download_psw" name="video_download_psw" value="{{$oFaceVideo->video_download_psw}}" >
	    </div>
	  </div>
	  <div class="form-group" style="display: none">
	    <label for="upload_file" class="col-sm-2 control-label"><span style="color:red;">*</span>缩略图</label>
	    <div class="col-sm-10">
	    	<input  id="upload_file" name="upload_file"  type="file" onchange="saveThumb()"/>
	      	<input type="hidden" class="form-control" id="video_thumb" name="video_thumb" value="{{$oFaceVideo->video_thumb}}" >
	      	<img style="width:320px;height:200px;" alt="" id="thumb" src="{{$oFaceVideo->video_thumb}}"/>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="upload_file" class="col-sm-2 control-label">Banner設置</label>
	    <div class="col-sm-10">
	    	<input id="upload_banner" name="upload_banner" type="file" onchange="saveBanner()"/>
	      	<input type="hidden" class="form-control" id="video_banner" name="video_banner" value="{{$oFaceVideo->video_banner}}">
	      	<img style="width:320px;height:200px;" id="banner" alt="" src="{{$oFaceVideo->video_banner}}" />
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_position" class="col-sm-2 control-label"><span style="color:red;">*</span>关联专家</label>
	    <div class="col-sm-10">
	      <select class="col-md-4 form-control" id="doc_id" name="doc_id" >
	      	@foreach($oDocs as $k=>$v)
	      		<option value="{{$k}}" @if($oFaceVideo->doc_id==$k) selected @endif>{{$v}}</option>
	      	@endforeach
	      </select>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_department" class="col-sm-2 control-label"><span style="color:red;">*</span>直播开始时间</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control ui_timepicker" id="start_time" name="start_time" value="{{$oFaceVideo->start_time}}" onchange="endTimeChange()">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;">*</span>直播结束时间</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control ui_timepicker" id="end_time" name="end_time" value="{{$oFaceVideo->end_time}}" >
	    </div>
	  </div>
	  <div class="form-group" >
	    <label for="video_introduce" class="col-sm-2 control-label"><span style="color:red;">*</span>课程简介</label>
	    <div class="col-sm-10">
	    <script id="video_introduce" name="video_introduce" type="text/plain">{{$oFaceVideo->video_introduce}}</script>
		<script type="text/javascript">
		    var editor = UE.getEditor('video_introduce')
		</script>
	    </div>
	  </div>
	  <div class="form-group" style="display:none;">
	    <label for="video_rec" class="col-sm-2 control-label redtxt">精品课堂</label>
	    <div class="col-sm-10 checkboxs_choose_boxs">
	      	<label class="radio-inline">
			  <input type="checkbox" name="video_rec" id="video_rec" value="1" checked> 推荐至精品课堂
			</label>
	    </div>
	  </div>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">确定</button>
	    </div>
	  </div>
	</form>
<script>
    function saveAudio() {
        $.ajaxFileUpload({
            url: "/admdocfacerec/upload-video-file",
            secureuri: false,
            fileElementId: "upload_file",
            dataType: "json",
            success: function(data, status) {
                $("#video_url").val(data.video_url);
                $("#video_download_url").val(data.video_url);
                alert("上传成功")
            }
        })
    }
$("#video_id").blur(function(){
	var channeltype = $("input[name='channel_type']:checked").val();
	var videotype = $("input[name='video_type']:checked").val();
	if(videotype == 1 && channeltype == 2){
		var video_id = $("#video_id").val();
		var reg = new RegExp("^[0-9]*$");
		if(reg.test(video_id)){
	        var url = '/admdocfacerec/get-video-url';
			var data = {videoid:video_id};
			$.post(url,data,function(o){
				o = JSON.parse(o);
				if(o.isok){
					if(o.videourl){
						$("#video_url").val(o.videourl);
					}
				}
			})
	    }
	}	
})
$("input[name='video_type']").change(function(){
	var channeltype = $("input[name='channel_type']:checked").val();
	var video_id = $("#video_id").val();
	if(video_id && channeltype == 2){
		var videotype = $("input[name='video_type']:checked").val();
		if(videotype == 1){
			var reg = new RegExp("^[0-9]*$");
			if(reg.test(video_id)){
		        var url = '/admdocfacerec/get-video-url';
				var data = {videoid:video_id};
				$.post(url,data,function(o){
					o = JSON.parse(o);
					if(o.isok){
						if(o.videourl){
							$("#video_url").val(o.videourl);
						}
					}
				})
		    }
		}
	}
})
</script>
@stop


