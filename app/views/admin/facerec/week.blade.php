@extends('admin.common.layout')
@section('content')
<div class="app_content_div">
          <h2 class="sub-header">KPI报告</h2>
          <div class="table-responsive">
          本周报告包含的直播为：<br>
          @foreach($oVideo as $k=>$v)
          <span style="color:blue;">{{$v->video_title}};</span><br>
          @endforeach
          <table class="table table-striped">
           </table>
            <table class="table table-striped">
              <thead>
              </thead>
              <tbody>
                @for($i=1;$i<=$oUserNum;$i++)
                <tr>
                  	<td style="width:90%;">导出本周总明细表{{$i}}</td>
                	<td><a href="/admdocface/export-week-log/{{$id}}/{{$i}}"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                @endfor
                <!-- <tr>
                  	<td style="width:90%;">导出本周总明细表02</td>
                	<td><a href="/admdocface/export-week-log/{{$id}}/2"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td style="width:90%;">导出本周总明细表03</td>
                	<td><a href="/admdocface/export-week-log/{{$id}}/3"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td style="width:90%;">导出本周总明细表04</td>
                	<td><a href="/admdocface/export-week-log/{{$id}}/4"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td style="width:90%;">导出本周总明细表05</td>
                	<td><a href="/admdocface/export-week-log/{{$id}}/5"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr> -->
                <tr>
                  	<td>导出本周Cluster排名</td>
                	<td><a href="/admdocface/week-cluster/{{$id}}"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td>导出本周大区观看医生数排行</td>
                	<td><a href="/admdocface/week-regin-docs/{{$id}}"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td>导出本周地区观看医生数排行</td>
                	<td><a href="/admdocface/week-area-docs/{{$id}}"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td>导出本周代表观看医生数排行</td>
                	<td><a href="/admdocface/week-rep-docs/{{$id}}"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td>导出本周大医生组织观看医生数排行</td>
                	<td><a href="/admdocface/week-bigdoc-docs/{{$id}}"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
              </tbody>
            </table>
          </div>
</div>
@stop


