@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>空中课堂--直播评分列表</h3>
	</div>
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>用户</th>
          <th>备课充分内容熟练</th>
          <th>逻辑清晰案例精彩</th>
          <th>授课内容专业正确</th>
          <th>讲课生动风趣</th>
          <th>课程效果好</th>
          <th>总评分</th>
          <th>访问来源</th>
        </tr>
      </thead>
      <tbody>
      <?php 
			$aDepartment = Config::get('config.department');
			?>
      	@foreach($oVideos as $k=>$v)
        <tr>
          <th scope="row">{{$v->id}}</th>
          <td>{{$oDocs[$v->user_id]}}</td>
          <td>{{$v->course_skilled}}</td>
          <td>{{$v->course_logic}}</td>
          <td>{{$v->course_correct}}</td>
          <td>{{$v->course_vivid}}</td>
          <td>{{$v->course_effect}}</td>
          <td>{{$v->course_all}}</td>
          <td>@if($v->device_type==1) 电脑 @elseif($v->device_type==2) app @else 微信 @endif</td>
        </tr>
        @endforeach
      </tbody>
    </table>
@stop
