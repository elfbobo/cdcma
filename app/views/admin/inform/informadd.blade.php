@extends('admin.common.layout')
@section('content')
<!-- 配置文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="/assets/js/ueditor/lang/zh-cn/zh-cn.js"></script>
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/information.js"></script>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>新增资讯</h3>
	</div>
	
	<form class="form-horizontal"  method="get" action="/adminformation/do-information-add">
	  <div class="form-group">
	    <label for="doc_name" class="col-sm-2 control-label"><span style="color:red;">*</span>资讯标题</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_name" name="doc_name" >
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="upload_file" class="col-sm-2 control-label"><span style="color:red;">*</span>缩略图</label>
	    <div class="col-sm-10">
	    	<input  id="upload_file" name="upload_file"  type="file" onchange="saveThumb()"/>
	      	<input type="hidden" class="form-control" id="doc_thumb" name="doc_thumb" >
	      	<img style="width:160px;height:200px;" alt="" id="thumb" src=""/>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_position" class="col-sm-2 control-label"><span style="color:red;">*</span>文章链地址</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_position" name="doc_position" >
	    </div>
	  </div>
<!--	  <div class="form-group">-->
<!--	    <label for="doc_department" class="col-sm-2 control-label"><span style="color:red;">*</span>科室</label>-->
<!--	    <div class="col-sm-10">-->
<!--	      <input type="text" class="form-control" id="doc_department" name="doc_department" >-->
<!--	    </div>-->
<!--	  </div>-->
<!--	  <div class="form-group">-->
<!--	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;">*</span>医院</label>-->
<!--	    <div class="col-sm-10">-->
<!--	      <input type="text" class="form-control" id="doc_hospital" name="doc_hospital" >-->
<!--	    </div>-->
<!--	  </div>-->
<!--	  <div class="form-group" >-->
<!--	    <label for="doc_introduction" class="col-sm-2 control-label"><span style="color:red;">*</span>简介</label>-->
<!--	    <div class="col-sm-10">-->
<!--	    <script id="doc_introduction" name="doc_introduction" type="text/plain"></script>-->
<!--		<script type="text/javascript">-->
<!--		    var editor = UE.getEditor('doc_introduction')-->
<!--		</script>-->
<!--	    </div>-->
<!--	  </div>-->
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">确定</button>
	    </div>
	  </div>
	</form>
@stop


