@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>资讯列表</h3>
	</div>
	<form action="/adminformation/information-list">
	 资讯标题:<input type="text" name='title' >
	 <button type="submit">搜索</button>
    </form>
	<div style="float:right;">
	
	<button type="button" class="btn btn-primary" onclick="window.location.href='/adminformation/add-information'">新增资讯</button></div>
<!--	<div style="float:left;">-->
<!--	-->
<!--	<button type="button" class="btn btn-primary" onclick="window.location.href='/admsurvey/survey-result'">导出调研结果</button></div>-->
	<table class="table table-striped">
      <thead>
        <tr>
          <th>咨讯标题</th>
          <th>缩列图</th> 
          <th>文章链接地址</th>
          <th>点击量</th> 
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oInfromation as $k=>$v)
        <tr>
          <td>{{$v->info_title}}</td>
          <td><img width="30px" src="{{$v->info_thumb}}"></img></td>
           
          <td>{{$v->info_url}}</td>
          <td>{{$v->info_hits}}</td>
           
          <td>

          		<a href="/adminformation/do-edit/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
          		&nbsp;&nbsp;&nbsp;
          		<a href="/adminformation/do-dele/{{$v->id}}" onclick="return confirm('确定删除吗');" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
                &nbsp;&nbsp;&nbsp;
          		 
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oInfromation ->links()}}
     
@stop
