<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap.css" rel="stylesheet">
<link href="/assets/css/font-awesome.css" rel="stylesheet">
<script src="http://libs.baidu.com/jquery/1.7.2/jquery.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.3.0/js/bootstrap.js"></script>
<link href="/assets/css/custom.css" rel="stylesheet">
<title>国卫健康云后台</title>
</head>
<body>
   <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12 main" >
			<div class="col-md-12" id="content" >
			   @yield('content')
			</div>
        </div>
      </div>
    </div>
<script>
function change_hospital(id){
    var pid = id-1;
	var hospital = $("#hospital"+pid+"  option:selected").val();
	console.log(hospital);
	var html_null = '<option value="0">=请选择=</option>';
	if(pid==1){
		$('#user_province').val(hospital);
		$("#hospital"+2).html(html_null);
		$("#hospital"+3).html(html_null);
		$("#hospital"+4).html(html_null);
    }else if(pid==2){
    	$('#user_city').val(hospital);
    	$("#hospital"+3).html(html_null);
		$("#hospital"+4).html(html_null);
    }else if(pid==3){
    	$('#user_county').val(hospital);
    	$("#hospital"+4).html(html_null);
    }else {
    	$('#user_company').val(hospital);
        return ;
    }
	var url = '/user/hospital-child/'+hospital;
	$.post(url,{},function(json){
		data = json;
    	if(data=='noinfo'){
			var html = '<option value="0">=请选择=</option>';
			$("#hospital"+id).html(html);
			
		}else{
			var bShow = false;
			var html = '<option value="0">=请选择=</option>';
			for(var elem in data){
				bShow = true;
				html += '<option value="' + elem + '">' + data[elem] + '</option>';
			}
			if(bShow){
				$("#hospital"+id).html(html);
			}
		}	
	},'json');
}

function change_hospital2(id){
    var pid = id-1;
	var hospital2 = $("#hospital2"+pid+"  option:selected").val();
	if(pid==1){
		$('#user_province').val(hospital2);
    }else if(pid==2){
    	$('#user_city').val(hospital2);
    	return ;
    }
	var url = '/user/hospital-child/'+hospital2;
	$.post(url,{},function(json){
		data = json;
    	if(data=='noinfo'){
			var html = '<option value="0">=请选择=</option>';
			$("#hospital2"+id).html(html);
			
		}else{
			var bShow = false;
			var html = '<option value="0">=请选择=</option>';
			for(var elem in data){
				bShow = true;
				html += '<option value="' + elem + '">' + data[elem] + '</option>';
			}
			if(bShow){
				$("#hospital2"+id).html(html);
			}
		}
	},'json');
}

function change_regin(){
	var regin = $("#regin  option:selected").val();
	$('#user_regin').val(regin);
	var url = '/user/area-info/'+regin;
	$.post(url,{},function(json){
		data = json;
    	if(data=='noinfo'){
			var html = '<option value="0">=请选择=</option>';
			$("#area").html(html);
			
		}else{
			var bShow = false;
			var html = '<option value="0">=请选择=</option>';
			for(var elem in data){
				bShow = true;
				html += '<option value="' + elem + '">' + data[elem] + '</option>';
			}
			if(bShow){
				$("#area").html(html);
			}
		}
	},'json');
}
</script>
</body>
</html>
