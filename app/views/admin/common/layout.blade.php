<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap.css" rel="stylesheet">
<link href="/assets/css/font-awesome.css" rel="stylesheet">
<script src="http://libs.baidu.com/jquery/1.7.2/jquery.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.3.0/js/bootstrap.js"></script>
<link href="/assets/css/custom.css" rel="stylesheet">

<title>国卫健康云后台</title>
</head>
<body>
<div class="navbar navbar-default">
	<div class="navbar-header">
		<a href="##" class="navbar-brand" >国卫健康云后台管理系统</a>
	</div>
   <p class="navbar-text navbar-right" style="margin-right:30px;">
		{{Auth::User()->user_nick}},欢迎您！
		<a href="/admsys/">系统设置</a> | 
		<a href="/admlogout">退出</a>
   </p>
</div>


   <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
			<div class="accordion" id="accordion-11">
			@if(in_array(Auth::User()->id,Config::get("config.docface")) || Auth::User()->role_id==1)
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" href="/kyadmin">
					首页
					</a>
				</div>
			</div>
			@endif
			@if(Auth::User()->role_id==1)
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" href="/admuser">
					用户
					</a>
				</div>
			</div>
			@endif
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" href="/admcmeorder">好医生兑换记录</a>
				</div>
			</div>
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" href="/admkypxorder/">科研培训兑换记录</a>
				</div>
			</div>
			@if(Auth::User()->role_id==1)
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" style="cursor:pointer;" contenteditable="false" data-parent="#accordion-aspirin" data-toggle="collapse" onclick="$('#accordion-element-aspirin').toggle()">
					阿司匹林专项基金
					</a>
				</div>
				<div id="accordion-element-aspirin" style="height: 280px; @if(getRouteName()!='admaspirin'&&getRouteName()!='admaspirininfo'&&getRouteName()!='admaspirinonline'&&getRouteName()!='admaspirinresearch'&&getRouteName()!='admaspirinscreening'&&getRouteName()!='admaspirinoffline'&&getRouteName()!='admaspirineducation'&&getRouteName()!='admaspirinuser') display:none; @endif">
					<div class="accordion-inner" >         
					  <ul class="nav nav-sidebar">
					    <li><a href="/admaspirininfo/group-list">轮播图</a></li>
			            <li><a href="/admaspirinonline/online-list">在线会议</a></li>
			            <li><a href="/admaspirinresearch/apply-list">科研申请</a></li>
			            <li><a href="/admaspirineducation/list">健康教育</a></li>
<!-- 			            <li><a href="/admaspirinscreening/list">筛查</a></li> -->
			            <li><a href="/admaspirinoffline/list">线下会议</a></li>
			            <li><a href="/admaspirinuser/list">医师认证审核</a></li>
			            <li><a href="/admaspirin/index">专项基金周报导出</a></li>
			           </ul>
		            </div>
				</div>
			</div>
			@endif
			@if(in_array(Auth::User()->id,Config::get("config.docface")) || Auth::User()->role_id==1)
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" href="/admdocface/doc">
					专家
					</a>
				</div>
			</div>
			@endif
			@if(Auth::User()->role_id==1)
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" href="/admuser/train-list">
					我的培训
					</a>
				</div>
			</div>
			@endif
			@if(in_array(Auth::User()->id,Config::get("config.docface")) || Auth::User()->role_id==1)
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" href="/admdocface/video">
					空中课堂
					</a>
				</div>
			</div>
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" href="/admdocfacerec/video">
					精品课堂
					</a>
				</div>
			</div>
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" href="/admtrainvideo/video">
						科研培训
					</a>
				</div>
			</div>
			@endif
			@if(Auth::User()->role_id==1)
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" href="/admuser/neworder">
					我的预约
					</a>
				</div>
			</div>
			
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" href="/admdocface/month">
					月报告相关导出
					</a>
				</div>
			</div>
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" href="/admuser/other">
					相关数据及缓存
					</a>
				</div>
			</div>
			
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" contenteditable="false" data-parent="#accordion-22" data-toggle="collapse" onclick="$('#accordion-element-99').toggle()">
					专家大会诊
					</a>
				</div>
			
				<div id="accordion-element-99" style="height: 160px; @if(getRouteName()!='admexpert') display:none; @endif">
					<div class="accordion-inner" >         
					  <ul class="nav nav-sidebar">
			            <li><a href="/admexpert/list/1">跨领域学术交流</a></li>
			          	<li><a href="/admexpert/list/2">科室与专家大会诊</a></li>
			          	<li><a href="/admexpert/list/3">疑难病例讨论</a></li>
			          	<li><a href="/admexpert/list/5">专家连线</a></li>
			          	<li><a href="/admexpert/list/4">往期回顾</a></li>
			           </ul>
		            </div>
				</div>
			</div>
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" href="/admsurvey/question-list">
					调研题目管理
					</a>
				</div>
			</div>
			@endif
		 	@if(Auth::User()->role_id==1||User::isCaseAdmin())
				<div class="accordion-group">
					<div class="accordion-heading">
						<a class="accordion-toggle" href="/admcase/list">
						病例征集
						</a>
					</div>
				</div>
			@endif
			@if(in_array(Auth::User()->id,Config::get("config.information")) || Auth::User()->role_id==1)
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" contenteditable="false" data-parent="#accordion-22" data-toggle="collapse" onclick="$('#accordion-element-999').toggle()">
					资讯管理
					</a>
				</div>
				<div id="accordion-element-999" style="height: 160px; @if(getRouteName()!='adminformation'&& getRouteName()!='admmaterial') display:none; @endif">
					<div class="accordion-inner" >         
					  <ul class="nav nav-sidebar">
			            <li><a href="/adminformation/information-list">拜耳医汇</a></li>
			          	<li><a href="/admmaterial/list/1">慢性疾病与转化医学</a></li>
			          	<li><a href="/admmaterial/list/2">心内空间</a></li>
			          	<li><a href="/admmaterial/list/3">呼吸科空间</a></li>
			           </ul>
		            </div>
				</div>
			</div>
			@endif
			@if(Auth::User()->role_id==1)
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" href="/admmeeting">
					自助会议平台
					</a>
				</div>
			</div>
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" href="/admmeeting-manager">
						会议管理
					</a>
				</div>
			</div>
			@endif
			@if(in_array(Auth::User()->id,Config::get("config.research")) || Auth::User()->role_id==1)
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" href="/admresearch/research-list">
					 小调研
					</a>
				</div>
			</div>
			@endif
			@if(Auth::User()->role_id==1)
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" href="/admapp/version">
					app更新
					</a>
				</div>
			</div>
			@endif
			
			
			<div class="accordion-group" style="display:none;">
				<div class="accordion-heading">
					<a class="accordion-toggle" contenteditable="false" data-parent="#accordion-22" data-toggle="collapse" onclick="$('#accordion-element-e').toggle()">
					E-Learning
					</a>
				</div>
			
				<div id="accordion-element-e" style="height: 120px; @if(getRouteName()!='admelearning') display:none; @endif">
					<div class="accordion-inner" >         
					  <ul class="nav nav-sidebar">
			            <li><a href="/admelearning/video-list">video</a></li>
			          	<li><a href="/admelearning/ppt-list">ppt</a></li>
			          	<li><a href="/admelearning/pdf-list">pdf</a></li>
			           </ul>
		            </div>
				</div>
			</div>
			
			</div>
			</div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" >
			<div class="col-md-12" id="content" >
			   @yield('content')
			   

			</div>
        </div>
      </div>
    </div>
<script>
                    function change_hospital(id){
                        var pid = id-1;
                    	var hospital = $("#hospital"+pid+"  option:selected").val();
                    	console.log(hospital);
                    	var html_null = '<option value="0">=请选择=</option>';
                    	if(pid==1){
							$('#user_province').val(hospital);
							$("#hospital"+2).html(html_null);
							$("#hospital"+3).html(html_null);
							$("#hospital"+4).html(html_null);
                        }else if(pid==2){
                        	$('#user_city').val(hospital);
                        	$("#hospital"+3).html(html_null);
							$("#hospital"+4).html(html_null);
                        }else if(pid==3){
                        	$('#user_county').val(hospital);
                        	$("#hospital"+4).html(html_null);
                        }else {
                        	$('#user_company').val(hospital);
                            return ;
                        }
		            	var url = '/user/hospital-child/'+hospital;
		            	$.post(url,{},function(json){
		            		data = json;
		                	if(data=='noinfo'){
		        				var html = '<option value="0">=请选择=</option>';
		        				$("#hospital"+id).html(html);
		        				
		        			}else{
		        				var bShow = false;
		        				var html = '<option value="0">=请选择=</option>';
		        				for(var elem in data){
		        					bShow = true;
		        					html += '<option value="' + elem + '">' + data[elem] + '</option>';
		        				}
		        				if(bShow){
		        					$("#hospital"+id).html(html);
		        				}
		        			}	
		        			},'json');
                    }

                    function change_hospital2(id){
                        var pid = id-1;
                    	var hospital2 = $("#hospital2"+pid+"  option:selected").val();
                    	if(pid==1){
							$('#user_province').val(hospital2);
                        }else if(pid==2){
                        	$('#user_city').val(hospital2);
                        	return ;
                        }
		            	var url = '/user/hospital-child/'+hospital2;
		            	$.post(url,{},function(json){
		            		data = json;
		                	if(data=='noinfo'){
		        				var html = '<option value="0">=请选择=</option>';
		        				$("#hospital2"+id).html(html);
		        				
		        			}else{
		        				var bShow = false;
		        				var html = '<option value="0">=请选择=</option>';
		        				for(var elem in data){
		        					bShow = true;
		        					html += '<option value="' + elem + '">' + data[elem] + '</option>';
		        				}
		        				if(bShow){
		        					$("#hospital2"+id).html(html);
		        				}
		        			}
		            	},'json');
                    }

                    function change_regin(){
		            	var regin = $("#regin  option:selected").val();
		            	$('#user_regin').val(regin);
		            	var url = '/user/area-info/'+regin;
		            	$.post(url,{},function(json){
		            		data = json;
		                	if(data=='noinfo'){
		        				var html = '<option value="0">=请选择=</option>';
		        				$("#area").html(html);
		        				
		        			}else{
		        				var bShow = false;
		        				var html = '<option value="0">=请选择=</option>';
		        				for(var elem in data){
		        					bShow = true;
		        					html += '<option value="' + elem + '">' + data[elem] + '</option>';
		        				}
		        				if(bShow){
		        					$("#area").html(html);
		        				}
		        			}
		            	},'json');
		            }

</script>
</body>
</html>
