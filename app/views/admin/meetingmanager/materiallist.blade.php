@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>会议管理资料</h3>
	</div>
	<form class="navbar-form navbar-left"  method="get" action="/admmeeting-manager/material">
	  <div class="form-group">
	  	<span>标题：</span>
	    <input type="text" class="form-control" name="title" value="{{Input::get('title','')}}">
	  </div>
	  <button type="submit" class="btn btn-default" style="margin-left:50px;">检索</button>
	  <a href="/admmeeting-manager/upload" class="btn btn-primary" style="margin-left:15px;">上传资料</a>
	</form>
	<div>
	<table class="table table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>类型</th>
          <th>资料名称</th>
          <th style="width: 150px">创建时间</th>
          <th width="190" style="text-align: center">操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oMaterial as $k=>$v)
        <tr>
          <td>{{$v->id}}</td>
          <td>{{$v->type == 1 ? '视频' : 'ppt'}}</td>
          <td>{{$v->title}}</td>
          <td>{{$v->created_at}}</td>
          <td>
            <a href="{{$v->filepath}}" style="margin-left: 10px" target="_blank">下载/预览</a>
            <a href="/admmeeting-manager/edit-material/{{$v->id}}" style="margin-left: 10px">编辑</a>
            <a href="/admmeeting-manager/delete-material/{{$v->id}}" onclick="return confirm('你确定要删除该资料吗?');" style="margin-left: 10px">删除</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
	{{$oMaterial->appends($aLink)->links()}}
     
@stop
