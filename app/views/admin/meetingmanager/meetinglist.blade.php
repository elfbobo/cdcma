@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>会议管理模块</h3>
	</div>
	<form class="navbar-form navbar-left"  method="get" action="/admmeeting-manager">
	  <div class="form-group">
		  <span>会议类型：</span>
		  <select class="form-control" name="type">
			  <option value="">全部</option>
			  <option value="0" {{Input::get('type','') === '0' ? 'selected' : ''}}>院内会</option>
			  <option value="1" {{Input::get('type','') === '1' ? 'selected' : ''}}>科室会</option>
		  </select>
	  </div>
	  &nbsp;&nbsp;&nbsp;
	  <div class="form-group">
	  	<span>标题：</span>
	    <input type="text" class="form-control" name="title" value="{{Input::get('title','')}}">
	  </div>
	  <button type="submit" class="btn btn-default" style="margin-left:50px;">检索</button>
	  <a href="/admmeeting-manager/create" class="btn btn-primary" style="margin-left:15px;">创建会议</a>
	  <a href="/admmeeting-manager/material" class="btn btn-danger" style="margin-left:15px;">材料管理</a>
	</form>
	<div>
	<table class="table table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>会议类型</th>
          <th>标题</th>
          <th>报名人数</th>
          <th>开始日期</th>
          <th>会议时长</th>
          <th style="width: 150px">创建时间</th>
          <th width="170" style="text-align: center">操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oMeeting as $k=>$v)
        <tr>
          <td>{{$v->id}}</td>
          <td>{{$v->type == 1 ? '科室会' : '院内会'}}</td>
          <td>{{$v->title}}</td>
          <td>{{$v->sign_num}}人</td>
          <td>{{$v->start_date}}</td>
          <td>{{$v->date_len}} 小时</td>
          <td>{{$v->created_at}}</td>
          <td>
            <a href="/admmeeting-manager/sign-list/{{$v->id}}">报名详情</a>
            <a href="/admmeeting-manager/edit/{{$v->id}}" style="margin-left: 10px">编辑</a>
            <a href="/admmeeting-manager/delete/{{$v->id}}" onclick="return confirm('你确定要删除该会议吗?');" style="margin-left: 10px">删除</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
	{{$oMeeting->appends($aLink)->links()}}
     
@stop
