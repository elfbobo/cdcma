@extends('admin.common.layout')
@section('content')
<style>
<!--
.form-group p select{margin-left:24px;}
-->
</style>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>上传会议资料</h3>
	</div>
	
	<form class="form-horizontal"  method="post" action="/admmeeting-manager/edit-material-do/{{$id}}">
	  <div class="form-group">
	    <label for="user_name" class="col-sm-2 control-label"><span style="color:red;">*</span>资料类型</label>
	    <div class="col-sm-3">
			<select name="type" class="form-control">
				<option value="0" {{$oInfo->type == 0 ?'selected':''}}>ppt</option>
				<option value="1" {{$oInfo->type == 1 ?'selected':''}}>视频</option>
			</select>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_nick" class="col-sm-2 control-label"><span style="color:red;">*</span>资料名称</label>
	    <div class="col-sm-9">
	      <input type="text" class="form-control" name="title" required value="{{$oInfo->title}}" >
	    </div>
	  </div>

		<div class="form-group">
			<label class="col-sm-2 control-label">资料文件(不上传时不修改)</label>
			<div class="col-sm-9">
				<input type="hidden" class="form-control" name="filepath" >
				<input id="upload_file" name="upload_file" type="file" onchange="saveMaterial()"/>
			</div>
		</div>

		<div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default"> 提交 </button>
	    </div>
	  </div>
	</form>

<script src="/assets/js/ajaxfileupload.js"></script>
<script>
    function saveMaterial(){
        $.ajaxFileUpload({
            url: "/admmeeting-manager/upload-material",
            secureuri: false,
            fileElementId: "upload_file",
            dataType: "json",
            success: function(data, status) {
                if (data.status) {
                    if ($('input[name="title"]').val() == '') {
                        $('input[name="title"]').val(data.file_name);
                    }
                    $('input[name="filepath"]').val(data.file_url);
                    alert('上传成功');
                } else {
                    alert('上传失败');
				}
            }
        })
    }
</script>
@stop


