@extends('admin.common.layout')
@section('content')
<style>
<!--
.form-group p select{margin-left:24px;}
-->
</style>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>编辑会议</h3>
	</div>
	
	<form class="form-horizontal"  method="post" action="/admmeeting-manager/edit-do/{{$oInfo->id}}">
	  <div class="form-group">
	    <label for="user_name" class="col-sm-2 control-label"><span style="color:red;">*</span>会议类型</label>
	    <div class="col-sm-3">
			<select name="type" class="form-control">
				<option value="0" {{$oInfo->type == 0 ?'selected':''}}>院内会</option>
				<option value="1" {{$oInfo->type == 1 ?'selected':''}}>科室会</option>
			</select>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user_nick" class="col-sm-2 control-label">标题</label>
	    <div class="col-sm-9">
	      <input type="text" class="form-control" name="title" value="{{$oInfo->title}}" >
	    </div>
	  </div>
		<div class="form-group">
			<label for="user_fee" class="col-sm-2 control-label"><span style="color:red;">*</span>会议开始时间</label>
			<div class="col-sm-10">
				<input type="text" name="start_date" class="form-control ui_timepicker" required style="width:300px;display:inline-block;" value="{{$oInfo->start_date}}" >
			</div>
		</div>
		<div class="form-group">
			<label for="user_fee" class="col-sm-2 control-label"><span style="color:red;">*</span>会议时长</label>
			<div class="col-sm-10">
				<input type="text" name="end_date" class="form-control" required style="width:300px;display:inline-block;" value="{{$dateLen}}" >
				（小时）
			</div>
		</div>

	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">确定</button>
	    </div>
	  </div>
	</form>

<!-- date -->
<script src="/assets/js/DatePicker/WdatePicker.js" type="text/javascript"></script>
<link type="text/css" href="/assets/js/date/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<link type="text/css" href="/assets/js/date/css/jquery-ui-timepicker-addon.css" rel="stylesheet" />
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-timepicker-zh-CN.js"></script>
<script type="text/javascript">
    $(function () {
        $(".ui_timepicker").datetimepicker({
            showSecond: true,
            timeFormat: 'hh:mm',
            stepHour: 1,
            stepMinute: 1,
            stepSecond: 0
        })
    })
</script>
<!-- date -->

@stop


