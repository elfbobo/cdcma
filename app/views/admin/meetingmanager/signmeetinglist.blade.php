@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>会议管理报名信息</h3>
	</div>
	<form class="navbar-form navbar-left"  method="get" action="/admmeeting-manager/sign-list/{{$id}}">
	  <div class="form-group">
	  	<span>姓名：</span>
	    <input type="text" class="form-control" name="title" value="{{Input::get('title','')}}">
	  </div>
	  <button type="submit" class="btn btn-default" style="margin-left:50px;">检索</button>
	  <a href="/admmeeting-manager" class="btn btn-default" style="margin-left:10px;">返回</a>
	</form>
	<div>
	<table class="table table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>会议地点</th>
          <th>报名者</th>
          <th>报名时间</th>
          <th>医院</th>
          <th>主席</th>
          <th>创建时间</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oMeeting as $k=>$v)
        <tr>
          <td>{{$v->id}}</td>
          <td>{{$v->province_name}} {{$v->city_name}}</td>
          <td>{{$v->user_name}}</td>
          <td>{{$v->datetime}}</td>
          <td>{{$v->hospital}}</td>
          <td>{{$v->chairman}}</td>
          <td>{{$v->created_at}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
	{{$oMeeting->appends($aLink)->links()}}
     
@stop
