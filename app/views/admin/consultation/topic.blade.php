@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>专家大会诊--话题</h3>
	</div>
	
	<script>
	        	function addOption(){
		        	var arrOption = ["1","2","3","4","5","6","7","8","9","10"];
		        	var itemNum = 10;
        	        var optionnum=$("#optionnum").val();
	        		if(parseInt(optionnum)>parseInt(itemNum)){
	        			return false;
	        		}
	        		$("#optiondiv").append("<div class='form-group' id='option"+optionnum+"'><label  class='col-sm-2 control-label'>话题"+arrOption[optionnum-1]+"<strong>:</strong></label><div class='col-sm-8'><input type='text' class='form-control' id='optionvalue"+optionnum+"' name='optionvalue"+arrOption[optionnum-1]+"'></div></div>");
	        		if(parseInt(optionnum)<=parseInt(itemNum)){
	        			optionnum++;
	        		}		
	        		$("#optionnum").val(optionnum);
	        	}
	        	function delOption(){
	        		var optionnum=$("#optionnum").val();
	        		var option=optionnum-1;
	        		$("#option"+option).remove();
	        		if(parseInt(optionnum)>parseInt(1)){
	        			optionnum--;
	        		}
	        		$("#optionnum").val(optionnum);
	        	}
        	</script>
<form action="/admexpert/add-topic/{{$id}}" method="post" class="form-horizontal" role="form">

	<div class="form-group">
		<label for="type" class="col-sm-2 control-label">&nbsp;</label>
		<button class="btn btn-primary" type="button" onclick="addOption()">添加</button>
		<button class="btn btn-primary" type="button" onclick="delOption()">删除</button>
         <input type="hidden" value="{{count($oTopic)+1}}" id="optionnum" name="optionnum">
		 
	</div> 
	<div id="optiondiv">
	@foreach($oTopic as $k=>$v)
		<div class="form-group" id="option{{$k+1}}">
		    <label class="col-sm-2 control-label">话题{{$k+1}}<strong>:</strong></label>
		    <div class="col-sm-8">
		      <input type="text" class="form-control" id="optionvalue{{$k+1}}" name="optionvalue{{$k+1}}" value="{{$v->topic_title}}">
		    </div>
		</div>
	@endforeach
	</div>
	<div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">确定</button>
	    </div>
  	</div>

</form>
@stop
