@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>专家大会诊--列表页</h3>
	</div>
	<div style="float:right;"><button type="button" class="btn btn-primary" onclick="window.location.href='/admexpert/add/{{$type}}'">新增</button></div>
	
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>主题</th>
          <th>专家</th>
          @if($type!=4)
          <th>邀请码</th>
          <th>会议开始时间</th>
          <th>会议结束时间</th>
          @else
          <th>点击量</th>
          <th>支持数</th>
          @endif
          @if($type==1)
          <th>已报名</th>
          <th>设置报名人数</th>
          <th>话题列表</th>
          <th>报名情况</th>
          @endif
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oVideos as $k=>$v)
        <tr>
          <th scope="row">{{$v->id}}</th>
          <td>{{$v->meeting_title}}</td>
          <td>{{$oDocs[$v->expert_id]}}</td>
          @if($type!=4)
          <td>{{$v->meeting_code}}</td>
          <td>{{$v->meeting_starttime}}</td>
          <td>{{$v->meeting_endtime}}</td>
          @else
          <td>{{$v->meeting_hits}}</td>
          <td>{{$v->meeting_support}}</td>
          @endif
          @if($type==1)
           <td>{{$v->reg_num}}</td>
          <td>{{$v->reg_setmax_nums}}</td>
          <td><a href="/admexpert/topic/{{$v->id}}">话题列表</a></td>
          <td><a href="/admexpert/apply/{{$v->id}}">报名情况</a></td>
          @endif
          <td>
          		<a href="/admexpert/edit/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
          		&nbsp;&nbsp;&nbsp;
          		<a href="#" onclick="delUser('/admexpert/del/{{$v->id}}')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oVideos->links()}}
    <script>
		function delUser(url){
			if(confirm('确定要删除该条记录吗？')){
				window.location.href = url;
			}
		}	
	</script>
@stop
