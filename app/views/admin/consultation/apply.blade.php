@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>跨领域学术交流--直播报名</h3>
	</div>
	<form class="form-horizontal" role="form">
	@foreach($oTopic as $k=>$v)
		<div class="form-group" id="option{{$k+1}}">
		    <label class="col-sm-2 control-label">话题{{$k+1}}<strong>:</strong></label>
		    <div class="col-sm-8">
		      <input type="text" disabled class="form-control" id="optionvalue{{$k+1}}" name="optionvalue{{$k+1}}" value="{{$v->topic_title}}">
		    </div>
		</div>
	@endforeach
	</form>
	<div style="float:right;"><button type="button" class="btn btn-primary" onclick="window.location.href='/admexpert/export-apply/{{$iId}}'">导出报名信息</button></div>
	
	
	<table class="table table-striped">
      <thead>
        <tr>
        <th>编号</th>
          <th>用户名</th>
          <th>用户姓名</th>
          <th>邮箱</th>
          <th>手机号码</th>
          <!--<th>大区</th>
          <th>地区</th>-->
         @foreach($oTopic as $k=>$v)
         	<th>话题{{$k+1}}</th>
         @endforeach
         <th>报名时间</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oConUserReg as $k=>$v)
        <tr>
          <th scope="row">{{$k+1}}</th>
          @if($v->user_info)
          <td>{{$v->user_info->user_nick}}</td>
           <td>{{$v->user_info->user_name}}</td>
           <td>{{$v->user_info->user_email}}</td>
           <td>{{$v->user_info->user_tel}}</td>
            <!--<td>{{$v->user_info->user_regin}}</td>
             <td>{{$v->user_info->user_area}}</td>-->
             @else
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          @endif
          
          @foreach($aTopic as $key=>$val)
          	<?php $value = $v->user_answer;?>
         	@if(isset($value[$key]))
         		<td>{{$value[$key]}}</td>
         	@else
         		<td></td>
         	@endif
         @endforeach
          
          <td>{{$v->created_at}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
	{{$oConUserReg->links()}}
@stop
