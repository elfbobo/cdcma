@extends('admin.common.layout')
@section('content')
<!-- 配置文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="/assets/js/ueditor/lang/zh-cn/zh-cn.js"></script>
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/video.js"></script>

<!-- date -->
<script src="/assets/js/DatePicker/WdatePicker.js" type="text/javascript"></script>
<link type="text/css" href="/assets/js/date/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<link type="text/css" href="/assets/js/date/css/jquery-ui-timepicker-addon.css" rel="stylesheet" />
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-timepicker-zh-CN.js"></script>
<script type="text/javascript">
    $(function () {
        $(".ui_timepicker").datetimepicker({
            showSecond: true,
            timeFormat: 'hh:mm:ss',
            stepHour: 1,
            stepMinute: 1,
            stepSecond: 1
        })
    })
</script>
<!-- date -->

	<div class="app_content_div" id="app_content_div_301Index">
		<h3>新增专家大会诊</h3>
	</div>
	
	<form class="form-horizontal"  method="post" action="/admexpert/add-do/{{$type}}">
	  <div class="form-group">
	    <label for="video_title" class="col-sm-2 control-label"><span style="color:red;">*</span>会议主题</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="meeting_title" name="meeting_title" >
	    </div>
	  </div>
	  @if($type==4)
	  <div class="form-group">
	    <label for="video_url" class="col-sm-2 control-label"><span style="color:red;">*</span>视频路径</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="meeting_url" name="meeting_url" >
	    </div>
	  </div>
	  @else
	  <div class="form-group">
	    <label for="video_url" class="col-sm-2 control-label"><span style="color:red;">*</span>直播路径</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="meeting_url" name="meeting_url" >
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="video_url" class="col-sm-2 control-label"><span style="color:red;">*</span>互动会路径</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="meeting_video_url" name="meeting_video_url" >
	    </div>
	  </div>
	  @endif
	  @if($type==4 || $type==3)
	  <div class="form-group">
	    <label for="upload_file" class="col-sm-2 control-label"><span style="color:red;">*</span>缩略图</label>
	    <div class="col-sm-10">
	    	<input  id="upload_file" name="upload_file"  type="file" onchange="saveThumb()"/>
	      	<input type="hidden" class="form-control" id="video_thumb" name="meeting_thumb" >
	      	<img style="width:320px;height:200px;" alt="" id="thumb" src=""/>
	    </div>
	  </div>
	  @endif
	  <div class="form-group">
	    <label for="doc_position" class="col-sm-2 control-label"><span style="color:red;">*</span>关联专家</label>
	    <div class="col-sm-10">
	      <select id="expert_id" name="expert_id" >
	      		@if($type==3||$type==4)
	      		<option value="0">无</option>
	      		@endif
	      	@foreach($oDocs as $k=>$v)
	      		<option value="{{$k}}">{{$v}}</option>
	      	@endforeach
	      </select>
	    </div>
	  </div>
	  @if($type!=4)
	  <div class="form-group">
	    <label for="doc_department" class="col-sm-2 control-label"><span style="color:red;">*</span>直播开始时间</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control ui_timepicker" id="start_time" name="meeting_starttime" onchange="endTimeChange('start_time12','end_time1')">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_department" class="col-sm-2 control-label"><span style="color:red;">*</span>直播结束时间</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control ui_timepicker" id="end_time1" name="meeting_endtime">
	    </div>
	  </div>
	  @endif
	  @if($type==1)
	  <div class="form-group">
	    <label for="doc_department" class="col-sm-2 control-label"><span style="color:red;">*</span>报名开始时间</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control ui_timepicker" id="start_time2" name="reg_starttime" onchange="endTimeChange('start_time2','end_time2')">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;">*</span>报名结束时间</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control ui_timepicker" id="end_time2" name="reg_endtime" >
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;">*</span>报名总人数(填写数字)</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="reg_setmax_nums" name="reg_setmax_nums" >
	    </div>
	  </div>
	  @endif
	  <div class="form-group" >
	    <label for="video_introduce" class="col-sm-2 control-label"><span style="color:red;">*</span>课程简介</label>
	    <div class="col-sm-10">
	    <script id="meeting_description" name="meeting_description" type="text/plain"></script>
		<script type="text/javascript">
		    var editor = UE.getEditor('meeting_description')
		</script>
	    </div>
	  </div>
	  @if($type==4)
	  <div class="form-group">
	    <label class="col-sm-2 control-label">病例提交者</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="case_submiter" name="case_submiter" >
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="col-sm-2 control-label">会议时间</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="meeting_time" name="meeting_time" >
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_position" class="col-sm-2 control-label">栏目选择</label>
	    <div class="col-sm-10">
	      <select id="column" name="column" >
	      		<option value="1">全部</option>
	      		<option value="2">专家大会诊</option>
	      		<option value="3">病例征集</option>
	      </select>
	    </div>
	  </div>
	  @endif
	  @if($type==3)
	  <div class="form-group">
	    <label for="doc_position" class="col-sm-2 control-label">推荐到病例征集</label>
	    <div class="col-sm-10">
			<input type="checkbox" name="recommend" id="recommend" value="1"/>
	    </div>
	  </div>
	  @endif
	  
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">确定</button>
	    </div>
	  </div>
	</form>
@stop


