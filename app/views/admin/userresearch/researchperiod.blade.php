@extends('admin.common.layout')
@section('content')
<script>
  function delete(id){
      if(confirm('你确定要删除该调研吗')){
                 window.location.href="/admresearch/delete-research/"+id;
          }
   
	  }
</script>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>调研期列表</h3>
	</div>
<!--	<form action="/admsurvey/question-list">-->
<!--	 调研期:<input type="text" name='phase' >-->
<!--	 <button type="submit">搜索</button>-->
<!--    </form>-->
	<div style="float:right;">
	<button type="button" class="btn btn-primary" onclick="window.location.href='/admresearch/add-period'">新增调研</button></div>
<!--	<div style="float:left;">-->
	
<!--	<button type="button" class="btn btn-primary" onclick="window.location.href='/admsurvey/survey-result'">导出调研结果</button></div>-->
	<table class="table table-striped">
      <thead>
        <tr>
          <th>调研期号</th>
          <th>调研期标题</th>
          <th>调研编码图</th>
          <th>调研路径</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oResearchPeriod as $k=>$v)
        <tr>
          <td>{{$v->id}}</td>
          <td>{{$v->research_title}}</td>
          <td><div><img style="width:15px;" src="{{$v->research_imges}}"></div></td>
          <td>http://{{$sName}}/survey/question/{{$v->id}}</td>
          <td>
          		<a href="/admresearch/edit-research/{{$v->id}}"><span   class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
          		&nbsp;&nbsp;&nbsp;
          		<a href="/admresearch/delete-research/{{$v->id}}" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
                 &nbsp;&nbsp;&nbsp;
          		<a href="/admresearch/result-list/{{$v->id}}" >查看结果</a></span>
                &nbsp;&nbsp;&nbsp;
          		<a href="/admresearch/question-list/{{$v->id}}" >题目列表</a></span>
          		
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oResearchPeriod ->links()}}
     
@stop
