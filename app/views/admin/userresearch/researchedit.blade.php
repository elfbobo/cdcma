@extends('admin.common.layout')
@section('content')
<!-- 配置文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="/assets/js/ueditor/lang/zh-cn/zh-cn.js"></script>
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/research.js"></script>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>空中课堂--编辑调研</h3>
	</div>
	
	<script>
	        	function addOption(){
		        	var arrOption = ["A","B","C","D","E","F","G","H","I","J"];
		        	var itemNum = 10;
        	        var optionnum=$("#optionnum").val();
	        		if(parseInt(optionnum)>parseInt(itemNum)){
	        			return false;
	        		}
	        		$("#optiondiv").append("<div class='form-group' id='option"+optionnum+"'><label  class='col-sm-2 control-label'>选项"+arrOption[optionnum-1]+"<strong>:</strong></label><div class='col-sm-8'><input type='text' class='form-control' id='optionvalue"+optionnum+"' name='optionvalue"+arrOption[optionnum-1]+"'></div></div>");
	        		if(parseInt(optionnum)<=parseInt(itemNum)){
	        			optionnum++;
	        		}		
	        		$("#optionnum").val(optionnum);
	        	}
	        	function delOption(){
	        		var optionnum=$("#optionnum").val();
	        		var option=optionnum-1;
	        		$("#option"+option).remove();
	        		if(parseInt(optionnum)>parseInt(1)){
	        			optionnum--;
	        		}
	        		$("#optionnum").val(optionnum);
	        	}
        	</script>
<form action="/admresearch/do-add" method="get" class="form-horizontal" role="form">
     <input type="hidden" class="form-control" id="doc_name" name="iId" value="{{$oResearchPeriodQuestion -> id}}">
	 <input type="hidden" class="form-control" id="doc_hospital" name="iPeriod" value="{{$oResearchPeriodQuestion->research_period_id}}">
	<div class="form-group">
	    <label for="title" class="col-sm-2 control-label">调研题目<span style="color:red;">*</span></label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" id="title" name="q_title" value="{{$oResearchPeriodQuestion->research_title}}">
	    </div>
	</div>
	<div class="form-group">
	    <label for="type" class="col-sm-2 control-label">题目类型<span style="color:red;">*</span></label>
	    <div class="col-sm-2">
		     <div class="input-group">
			      <span class="input-group-addon">
			        <input type="radio" name="q_type" value="1" @if($oResearchPeriodQuestion->research_type == 1) checked @endif>
			      </span>
			      <input type="text" class="form-control" value="单选" style="width:70px;">
			      <span class="input-group-addon">
			        <input type="radio" name="q_type" value="2" @if($oResearchPeriodQuestion->research_type == 2) checked @endif>
			      </span>
			      <input type="text" class="form-control" value="多选" style="width:70px;">
		     </div>
	    </div>
	</div>
	<div class="form-group">
	    <label for="result" class="col-sm-2 control-label">正确答案<span style="color:red;"></span></label>
	    <div class="col-sm-6">
	      <input type="text" class="form-control" style="width:100px;" id="result" name="q_result" value="{{$oResearchPeriodQuestion->research_result}}">不区分大小写（例如：A，AB）
	    </div>
	</div>
	<div class="form-group">
	    <label for="upload_file" class="col-sm-2 control-label"><span style="color:red;"></span>缩略图</label>
	    <div class="col-sm-10">
	    	<input  id="upload_file" name="upload_file"  type="file" onchange="saveThumb()"/>
	      	<input type="hidden" class="form-control" id="doc_thumb" name="doc_thumb" value="{{$oResearchPeriodQuestion->research_img}}">
	      	<img style="width:160px;" alt="" id="thumb" src="{{$oResearchPeriodQuestion->research_img}}"/>
	    </div>
	  </div>
	<div class="form-group">
		<label for="type" class="col-sm-2 control-label">&nbsp;</label>
		<button class="btn btn-primary" type="button" onclick="addOption()">添加选项</button>
		<button class="btn btn-primary" type="button" onclick="delOption()">删除选项</button>
        <input type="hidden" value="{{count($oUserSurveyItem)+1}}" id="optionnum" name="optionnum">
	</div> 
	<div id="optiondiv">
	@foreach($oUserSurveyItem as $k=>$v)
		<div class="form-group" id="option{{$k+1}}">
		    <label class="col-sm-2 control-label">选项{{$v->research_option}}<strong>:</strong></label>
		    <div class="col-sm-8">
		      <input type="text" class="form-control" id="optionvalue{{$k+1}}" name="optionvalue{{$v->research_option}}" value="{{$v->research_content}}">
		    </div>
		</div>
	@endforeach
	</div>
	<div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">确定</button>
	    </div>
  	</div>

</form>
@stop
