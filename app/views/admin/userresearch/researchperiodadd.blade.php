@extends('admin.common.layout')
@section('content')
<!-- 配置文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="/assets/js/ueditor/lang/zh-cn/zh-cn.js"></script>
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/research.js"></script>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>新增调研期</h3>
	</div>
	
	<form class="form-horizontal"  method="get" action="/admresearch/do-period-add">
<!--	  <div class="form-group">-->
<!--	    <label for="doc_position" class="col-sm-2 control-label"><span style="color:red;">*</span>调研期号</label>-->
<!--	    <div class="col-sm-10">-->
<!--	      <input type="text"   class="form-control" id="doc_position" name="research_period" >-->
<!--	    </div>-->
<!--	  </div>-->
	  <div class="form-group">
	    <label for="doc_name" class="col-sm-2 control-label"><span style="color:red;">*</span>调研器标题</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_name" name="research_title" >
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="upload_file" class="col-sm-2 control-label"><span style="color:red;">*</span>调研编码图</label>
	    <div class="col-sm-10">
	    	<input  id="upload_file" name="upload_file"  type="file" onchange="saveThumb()"/>
	      	<input type="hidden" class="form-control" id="doc_thumb" name="doc_thumb" >
	      	<img style="width:160px;" alt="" id="thumb" src=""/>
	    </div>
	  </div>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">确定</button>
	    </div>
	  </div>
	</form>
@stop


