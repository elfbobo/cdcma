@extends('admin.common.layout')
@section('content')
<!-- 配置文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="/assets/js/ueditor/lang/zh-cn/zh-cn.js"></script>
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/research.js"></script>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>调研试题</h3>
	</div>
	
	<script>
	        	function addOption(){
		        	var arrOption = ["A","B","C","D","E","F","G","H","I","J"];
		        	var itemNum = 10;
        	        var optionnum=$("#optionnum").val();
	        		if(parseInt(optionnum)>parseInt(itemNum)){
	        			return false;
	        		}
	        		$("#optiondiv").append("<div class='form-group' id='option"+optionnum+"'><label  class='col-sm-2 control-label'>选项"+arrOption[optionnum-1]+"<strong>:</strong></label><div class='col-sm-8'><input type='text' class='form-control' id='optionvalue"+optionnum+"' name='optionvalue"+arrOption[optionnum-1]+"'></div></div>");
	        		if(parseInt(optionnum)<=parseInt(itemNum)){
	        			optionnum++;
	        		}		
	        		$("#optionnum").val(optionnum);
	        	}
	        	function delOption(){
	        		var optionnum=$("#optionnum").val();
	        		var option=optionnum-1;
	        		$("#option"+option).remove();
	        		if(parseInt(optionnum)>parseInt(1)){
	        			optionnum--;
	        		}
	        		$("#optionnum").val(optionnum);
	        	}
        	</script>
<form action="/admresearch/do-adds" method="get" class="form-horizontal" role="form">
   <input type="hidden" value="{{$iPeriod}}" name="iPeriod">
	<div class="form-group">
	    <label for="title" class="col-sm-2 control-label">调研题目<span style="color:red;">*</span></label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" id="title" name="q_title" value="">
	    </div>
	</div>
	<div class="form-group">
	    <label for="type" class="col-sm-2 control-label">类型<span style="color:red;">*</span></label>
	    <div class="col-sm-2">
		     <div class="input-group">
			      <span class="input-group-addon">
			        <input type="radio" name="q_type" value="1">
			      </span>
			      <input type="text" class="form-control" value="单选" style="width:70px;">
			      <span class="input-group-addon">
			        <input type="radio" name="q_type" value="2">
			      </span>
			      <input type="text" class="form-control" value="多选" style="width:70px;">
		     </div>
	    </div>
	</div>
	<div class="form-group">
	    <label for="result" class="col-sm-2 control-label">正确答案<span style="color:red;"></span></label>
	    <div class="col-sm-6">
	      <input type="text" class="form-control" style="width:100px;" id="result" name="q_result" value="">不区分大小写（例如：A，AB）
	    </div>
	</div>
	<div class="form-group">
	    <label for="upload_file" class="col-sm-2 control-label"><span style="color:red;"></span>题目插图</label>
	    <div class="col-sm-10">
	    	<input  id="upload_file" name="upload_file"  type="file" onchange="saveThumb()"/>
	      	<input type="hidden" class="form-control" id="doc_thumb" name="doc_thumb" >
	      	<img style="width:160px;" alt="" id="thumb" src=""/>
	    </div>
	  </div>
	<div class="form-group">
		<label for="type" class="col-sm-2 control-label">&nbsp;</label>
		<button class="btn btn-primary" type="button" onclick="addOption()">添加选项</button>
		<button class="btn btn-primary" type="button" onclick="delOption()">删除选项</button>
         <input type="hidden" value="1" id="optionnum" name="optionnum">
		 
	</div> 
	<div id="optiondiv"></div>
	<div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">确定</button>
	    </div>
  	</div>

</form>
@stop
