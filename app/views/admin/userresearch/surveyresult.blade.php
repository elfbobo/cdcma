@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>{{$sTitle}}</h3>
	</div>
<!--	<div style="float:right;">-->
<!--	-->
<!--	<button type="button" class="btn btn-primary" onclick="window.location.href='/admresearch/add-question/{{$iId}}'">新增调研题目</button></div>-->
	<div style="float:right;">
	
	<button type="button" class="btn btn-primary" onclick="window.location.href='/admresearch/research_result/{{$iId}}'">导出调研结果</button></div>
	<table class="table table-striped">
      <thead>
        <tr>
          <th>期号</th>
          <th>题目</th>
          <th>A</th>
          <th>B</th>
          <th>C</th>
          <th>D</th>
           
        </tr>
      </thead>
      <tbody>
      	@foreach($oResearchPeriodQuestion as $k=>$v)
        <tr>
          <td>{{$v->research_period_id}}</td>
          <td>{{$v->research_title}}</td>
          <td>{{isset($aQuestionRadio[$v->id]['A'])?$aQuestionRadio[$v->id]['A']:0}}人|{{isset($apercent[$v->id]['A'])?$apercent[$v->id]['A']:0}}% </td>
          <td> {{isset($aQuestionRadio[$v->id]['B'])?$aQuestionRadio[$v->id]['B']:0}}人|{{isset($apercent[$v->id]['B'])?$apercent[$v->id]['B']:0}}%</td>
          <td> {{isset($aQuestionRadio[$v->id]['C'])?$aQuestionRadio[$v->id]['C']:0}}人|{{isset($apercent[$v->id]['C'])?$apercent[$v->id]['C']:0}}%</td>
          <td> {{isset($aQuestionRadio[$v->id]['D'])?$aQuestionRadio[$v->id]['D']:0}}人|{{isset($apercent[$v->id]['D'])?$apercent[$v->id]['D']:0}}%</td>
        </tr>
        
        @endforeach
      </tbody>
    </table>
    {{$oResearchPeriodQuestion ->links()}}
     
@stop
