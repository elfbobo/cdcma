@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>调研题目列表</h3>
	</div>
<!--	<form action="/admsurvey/question-list">-->
<!--	 调研期:<input type="text" name='phase' >-->
<!--	 <button type="submit">搜索</button>-->
<!--    </form>-->
	<div style="float:right;">
	
	<button type="button" class="btn btn-primary" onclick="window.location.href='/admresearch/add-question/{{$iId}}'">新增调研题目</button></div>
<!--	<div style="float:left;">-->
<!--	-->
<!--	<button type="button" class="btn btn-primary" onclick="window.location.href='/admresearch/research_result/{{$iId}}'">导出调研结果</button></div>-->
	<table class="table table-striped">
      <thead>
        <tr>
          <th>期号</th>
          <th>题目</th>
          <th>题目类型</th>
          <th>正确结果</th>
          <th>插图</th>
          <th>本期标题</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oResearchPeriodQuestion as $k=>$v)
        <tr>
          <td>{{$v->research_period_id}}</td>
          <td>{{$v->research_title}}</td>
          <td>@if($v->research_type == 1) 单选 @else 多选 @endif</td>
          <td>{{$v->research_result}}</td>
          <td><div>@if($v->research_img)<img src="{{$v->research_img}}"  style="width:25px;">@else 无图@endif</div></td>
          <td>{{$aResearchPeriod[$v->research_period_id]}}</td>
          <td>
          		<a href="/admresearch/edit-survey/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
          		&nbsp;&nbsp;&nbsp;
          		<a href="/admresearch/delete-survey/{{$v->id}}/{{$v->research_period_id}}" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
                &nbsp;&nbsp;&nbsp;
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oResearchPeriodQuestion ->links()}}
     
@stop
