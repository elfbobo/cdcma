@extends('admin.common.layout')
@section('content')
<!-- 配置文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="/assets/js/ueditor/lang/zh-cn/zh-cn.js"></script>
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/video.js"></script>
<!-- date -->
<script src="/assets/js/DatePicker/WdatePicker.js" type="text/javascript"></script>
<link type="text/css" href="/assets/js/date/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<link type="text/css" href="/assets/js/date/css/jquery-ui-timepicker-addon.css" rel="stylesheet" />
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/assets/js/date/js/jquery-ui-timepicker-zh-CN.js"></script>
<script type="text/javascript">
$(function () {
    $(".ui_timepicker").datetimepicker({
        showSecond: true,
        timeFormat: 'hh:mm:ss',
        stepHour: 1,
        stepMinute: 1,
        stepSecond: 1
    })
})
</script>
<style type="text/css">
<!--
.checkboxs_choose_boxs label { padding-left:0px !important; }
-->
</style>
<!-- date -->
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>编辑视频</h3>
	</div>
	
	<form class="form-horizontal"  method="post" action="/admtrainvideo/edit-video-do/{{$oFaceVideo->id}}">
	  <div class="form-group">
	    <label for="video_title" class="col-sm-2 control-label"><span style="color:red;">*</span>标题</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="video_title" name="video_title" value="{{$oFaceVideo->video_title}}">
	    </div>
	  </div>

	  <div class="form-group">
	    <label for="department_id" class="col-sm-2 control-label">科室</label>
	    <div class="col-sm-10 checkboxs_choose_boxs">
	      	<!-- <label class="radio-inline">
			  <input type="checkbox" name="department_id[]" id="departmeny_0" value="0" @if($oFaceVideo->department_id==0) checked @endif> 默认科室
			</label> -->
			<?php 
			$aDepartment = Config::get('config.department');
			?>
			@foreach($aDepartment as $k=>$v)
			<label class="radio-inline">
			  <input type="checkbox" name="department_id[]" id="departmeny_{{$k}}" value="{{$k}}" @if(in_array($k, explode(',', $oFaceVideo->department_id)) !== false) checked @endif> {{$v}}
			</label>
			@endforeach
	    </div>
	  </div>

	  <div class="form-group">
	    <label for="video_url" class="col-sm-2 control-label">视频路径</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="video_url" name="video_url" value="{{$oFaceVideo->video_url}}">
			<input  id="upload_video" name="upload_video"  type="file" onchange="saveVideo()"/>
	    </div>
	  </div>

	  <div class="form-group">
	    <label for="upload_file" class="col-sm-2 control-label"><span style="color:red;">*</span>缩略图</label>
	    <div class="col-sm-10">
	    	<input  id="upload_file" name="upload_file"  type="file" onchange="saveThumb()"/>
	      	<input type="hidden" class="form-control" id="video_thumb" name="video_thumb" value="{{$oFaceVideo->video_thumb}}" >
	      	<img style="width:320px;height:200px;" alt="" id="thumb" src="{{$oFaceVideo->video_thumb}}"/>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="upload_file" class="col-sm-2 control-label">Banner設置</label>
	    <div class="col-sm-10">
	    	<input id="upload_banner" name="upload_banner" type="file" onchange="saveBanner()"/>
	      	<input type="hidden" class="form-control" id="video_banner" name="video_banner" value="{{$oFaceVideo->video_banner}}">
	      	<img style="width:320px;height:200px;" id="banner" alt="" src="{{$oFaceVideo->video_banner}}" />
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_position" class="col-sm-2 control-label"><span style="color:red;">*</span>关联专家</label>
	    <div class="col-sm-10">
	      <select class="col-md-4 form-control" id="doc_id" name="doc_id" >
	      	@foreach($oDocs as $k=>$v)
	      		<option value="{{$k}}" @if($oFaceVideo->doc_id==$k) selected @endif>{{$v}}</option>
	      	@endforeach
	      </select>
	    </div>
	  </div>

	  <div class="form-group" >
	    <label for="video_introduce" class="col-sm-2 control-label"><span style="color:red;">*</span>课程简介</label>
	    <div class="col-sm-10">
	    <script id="video_introduce" name="video_introduce" type="text/plain">{{$oFaceVideo->video_introduce}}</script>
		<script type="text/javascript">
		    var editor = UE.getEditor('video_introduce')
		</script>
	    </div>
	  </div>

	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">确定</button>
	    </div>
	  </div>
	</form>
<script>
    function saveVideo() {
        $.ajaxFileUpload({
            url: "/admtrainvideo/upload-video-file",
            secureuri: false,
            fileElementId: "upload_video",
            dataType: "json",
            success: function(data, status) {
                $("#video_url").val(data.video_url);
                alert("上传成功")
            }
        })
    }
</script>
@stop


