@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index" style="margin-bottom:8px;">
    <h3 style="display:inline-table;">科研培训--视频列表</h3>
    <div style="float:right;margin-top:10px;">
      <button type="button" class="btn btn-primary" onclick="window.location.href='/admtrainvideo/add-video'">新增视频</button>
    </div>
  </div>
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>标题</th>
          <th>科室</th>
          <th>专家</th>
          <th>直播评分列表</th>
          <!--<th>积分导出</th> -->
		      @if(!in_array(Auth::User()->id,Config::get("config.docface")))
          <th>周报导出</th>
          @endif
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
      <?php 
          $aDepartment = Config::get('config.department');
          ?>
      	@foreach($oVideos as $k=>$v)
        <tr>
          <th scope="row">{{$v->id}}</th>
          <td>{{str_cut_cms($v->video_title,30)}}</td>
          <td>
          @foreach($aDepartment as $kdep=>$vdep)
          @if(strpos($v->department_id, $kdep.'|') !== false)
          {{$vdep}}<br>
          @endif
          @endforeach
          </td>
          <td><?php  echo $oDocs[$v->doc_id]; ?></td>
          <td><a href="/admtrainvideo/live-score/{{$v->id}}" >直播评分列表</a></td>
          <!--<td><a href="/admdocfacerec/export-log/{{$v->id}}" >积分导出</a></td>-->
		      @if(!in_array(Auth::User()->id,Config::get("config.docface")))
          <td><a href="/admtrainvideo/week-log/{{$v->id}}" >周报导出</a></td>
          @endif
          <td>
      		<a href="/admtrainvideo/edit-video/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
      		&nbsp;&nbsp;&nbsp;
      		<a href="#" onclick="delUser('/admtrainvideo/del-video/{{$v->id}}')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oVideos->links()}}
    <script>
		function delUser(url){
			if(confirm('确定要删除该条记录吗？')){
				window.location.href = url;
			}
		}	
	</script>
@stop
