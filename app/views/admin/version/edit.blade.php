@extends('admin.common.layout')
@section('content')
<!-- 配置文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="/assets/js/ueditor/lang/zh-cn/zh-cn.js"></script>
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/doc.js"></script>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>编辑版本</h3>
	</div>
	
	<form class="form-horizontal"  method="post" action="/admapp/edit-version-do/{{$oVersion->id}}">
	  
	  <div class="form-group">
	    <label for="number" class="col-sm-2 control-label"><span style="color:red;">*</span>版本号</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="number" name="number" value="{{$oVersion->number}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="types" class="col-sm-2 control-label"><span style="color:red;">*</span>类型</label>
	    <div class="col-sm-10">
	      	<label class="radio-inline">
			  <input type="radio" name="types" id="types1" value="ios" @if($oVersion->types=='ios') checked @endif> IOS
			</label>
			<label class="radio-inline">
			  <input type="radio" name="types" id="types2" value="android" @if($oVersion->types=='android') checked @endif> 安卓
			</label>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="url" class="col-sm-2 control-label"><span style="color:red;">*</span>安装包路径</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="url" name="url"  value="{{$oVersion->url}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="content" class="col-sm-2 control-label"><span style="color:red;">*</span>更新内容</label>
	    <div class="col-sm-10">
	      <textarea class="form-control" name="content" id="content" style="height:100px;">{{$oVersion->content}}</textarea>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="forces" class="col-sm-2 control-label">是否强制更新</label>
	    <div class="col-sm-10">
	      	<label class="radio-inline">
			  <input type="radio" name="forces" id="forces1" value="1" @if($oVersion->forces==1) checked @endif> 是
			</label>
			<label class="radio-inline">
			  <input type="radio" name="forces" id="forces2" value="0" @if($oVersion->forces==0) checked @endif> 否
			</label>
	    </div>
	  </div>
	  <!-- <div class="form-group">
	    <label for="is_push" class="col-sm-2 control-label">是否推送</label>
	    <div class="col-sm-10">
	      	<label class="radio-inline">
			  <input type="radio" name="is_push" id="is_push1" value="1" @if($oVersion->is_push==1) checked @endif> 是
			</label>
			<label class="radio-inline">
			  <input type="radio" name="is_push" id="is_push2" value="2" @if($oVersion->is_push==2) checked @endif> 否
			</label>
	    </div> 
	  </div>-->
	  <div class="form-group">
	    <label for="isopen" class="col-sm-2 control-label">状态</label>
	    <div class="col-sm-10">
	      	<label class="radio-inline">
			  <input type="radio" name="isopen" id="isopen1" value="1" @if($oVersion->isopen==1) checked @endif> 开启
			</label>
			<label class="radio-inline">
			  <input type="radio" name="isopen" id="isopen2" value="0" @if($oVersion->isopen==0) checked @endif> 关闭
			</label>
	    </div>
	  </div>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">确定</button>
	    </div>
	  </div>
	</form>
@stop