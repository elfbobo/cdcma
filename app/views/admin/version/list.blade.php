@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>App --版本列表</h3>
	</div>
	<div style="float:right;"><button type="button" class="btn btn-primary" onclick="window.location.href='/admapp/add-version'">新增版本</button></div>
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>版本号</th>
          <th>安装包类型</th>
          <th>安装包路径</th>
          <th>是否推送</th>
          <th>是否开启</th>
          <th>上传时间</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oDocs as $k=>$v)
        <tr>
          <th scope="row">{{$v->id}}</th>
          <td>{{$v->number}}</td>
          <td>{{$v->types}}</td>
          <td>{{$v->url}}</td>
          <td><?php echo ($v->is_push == 1)?'是':'否'; ?></td>
          <td><?php echo ($v->isopen == 1)?'是':'否'; ?></td>
          <td>{{$v->created_at}}</td>
          <td>
          		<a href="/admapp/edit-version/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
          		&nbsp;&nbsp;&nbsp;
          		<a href="#" onclick="delVersion('/admapp/del-version/{{$v->id}}')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oDocs->links()}}
    <script>
		function delVersion(url){
			if(confirm('确定要删除该版本吗？')){
				window.location.href = url;
			}
		}	
	</script>
@stop
