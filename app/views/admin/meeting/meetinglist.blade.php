@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>自助会议平台</h3>
	</div>
	<form class="navbar-form navbar-left"  method="get" action="/admmeeting">
	  <div class="form-group">
	  	<span>专家姓名：</span>
	    <input type="text" class="form-control" name="expert_name" value="{{Input::get('expert_name','')}}">
	  </div>
	  &nbsp;&nbsp;&nbsp;
	  <div class="form-group">
	  	<span>CWID：</span>
	    <input type="text" class="form-control" name="user_cwid" value="{{Input::get('user_cwid','')}}">
	  </div>
	  &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
	  <div class="form-group">
	  	<span>用户编码：</span>
	    <input type="text" class="form-control" name="invite_code" value="{{Input::get('invite_code','')}}">
	  </div>
	  <br><br>
	  &nbsp;&nbsp;&nbsp;
	  <div class="form-group">
	  	<span>创建人名称：</span>
	    <input type="text" class="form-control" name="creator_name" value="{{Input::get('creator_name','')}}">
	  </div>
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  <div class="form-group">
	  	<span>创建人手机：</span>
	    <input type="text" class="form-control" name="creator_tel" value="{{Input::get('creator_tel','')}}">
	  </div>
	  &nbsp;&nbsp;&nbsp;
	  <div class="form-group">
	  	<span>创建人邮箱：</span>
	    <input type="text" class="form-control" name="creator_email" value="{{Input::get('creator_email','')}}">
	  </div>
	  <br><br>
	  <button type="submit" class="btn btn-default" style="margin-left:77px;">检索</button>
	</form>
	<div>
	<table class="table table-striped">
      <thead>
        <tr>
          <th>会议序号</th>
          <th>专家单位</th> 
          <th>创建人姓名</th>
          <th>会议开始时间</th>
          <th>会议主题</th>
          <th>是否开启会议</th>
          <th>会议详情</th>
          <th>导出信息</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oMeeting as $k=>$v)
        <tr>
          <td>{{$v->id}}</td>
          <td>{{$v->expert_company}}</td>
          <td>{{$v->creator_name}}</td>
          <td>{{$v ->meeting_start_time}}</td>
          <td>{{$v->meeting_title}}</td>
           <td>
             @if($v->status == 1)
                <a href="/admmeeting/open-meeting/{{$v->id}}"> 点击开启会议 </a>
             @else
              <a href="/admmeeting/open-meeting/{{$v->id}}"> 点击关闭会议 </a>
             @endif
           </td>
           <td>
              <a href ="/admmeeting/meeting-detail/{{$v->id}}">详情</a>
           <td>
           <td>
              <a href ="/admmeeting/meeting-message/{{$v->id}}">导出</a>
           <td>
        </tr>
        @endforeach
      </tbody>
    </table>
        {{$oMeeting->appends($aLink)->links()}}
     
@stop
