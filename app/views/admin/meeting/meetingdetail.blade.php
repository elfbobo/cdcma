@extends('admin.common.layout')
@section('content')
<!-- 配置文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="/assets/js/ueditor/lang/zh-cn/zh-cn.js"></script>
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/information.js"></script>
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>会议详情</h3>
	</div>
	
	<form class="form-horizontal"  method="get" action="/adminformation/do-information-edit">
	  <div class="form-group">
	    <label for="doc_name" class="col-sm-2 control-label"><span style="color:red;">*</span>会议标题</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_name" name="doc_name" disabled value="{{$oMeeting -> meeting_title}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_position" class="col-sm-2 control-label"><span style="color:red;">*</span>专家姓名</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_position" name="doc_position" disabled value="{{$oMeeting -> expert_name}}">
	    </div>
	  </div>
	   <div class="form-group">
	    <label for="doc_name" class="col-sm-2 control-label"><span style="color:red;">*</span>专家单位</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_name" name="doc_name" disabled value="{{$oMeeting -> expert_company}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_position" class="col-sm-2 control-label"><span style="color:red;">*</span>创建人姓名</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_position" name="doc_position" disabled value="{{$oMeeting -> creator_name}}">
	    </div>
	  </div>
	   <div class="form-group">
	    <label for="doc_name" class="col-sm-2 control-label"><span style="color:red;">*</span>创建人手机号</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_name" name="doc_name" disabled  value="{{$oMeeting -> creator_tel}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_position" class="col-sm-2 control-label"><span style="color:red;">*</span>创建人邮箱</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_position" name="doc_position" disabled value="{{$oMeeting -> creator_email}}">
	    </div>
	  </div>
	   <div class="form-group">
	    <label for="doc_position" class="col-sm-2 control-label"><span style="color:red;">*</span>预约该会议的人数</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_position" name="doc_position" disabled value="{{$oMeeting -> order_count}}">
	    </div>
	  </div>
	  
	   <div class="form-group">
	    <label for="doc_position" class="col-sm-2 control-label"><span style="color:red;">*</span>会议状态</label>
	    <div class="col-sm-10">
	    @if($oMeeting->meeting_type_id == 0)
	      <input type="text" class="form-control" id="doc_position" name="doc_position" disabled value="会议未确认">
	    @else
	       <input type="text" class="form-control" id="doc_position" name="doc_position" disabled value="会议已确认">
	    @endif
	    </div>
	  </div>
<!--	  <div class="form-group">-->
<!--	    <div class="col-sm-offset-2 col-sm-10">-->
<!--	      <button type="submit" class="btn btn-default">确定</button>-->
<!--	    </div>-->
<!--	  </div>-->
	</form>
@stop


