@extends('admin.common.layout')
@section('content')
    <div class="app_content_div" style="margin-bottom: 25px;">
        <h3>好医生兑换审核</h3>
    </div>

    <form class="form-horizontal"  method="post" action="">
        <input type="hidden" class="form-control" name="id" value="{{$info->id}}">
        <div class="form-group">
            <label for="doc_name" class="col-sm-2 control-label">姓名</label>
            <div class="col-sm-10">
                <p class="form-control-static">{{$info->name}}</p>
            </div>
        </div>
        <div class="form-group">
            <label for="doc_name" class="col-sm-2 control-label">工作单位</label>
            <div class="col-sm-10">
                <p class="form-control-static">{{$info->workplace}}</p>
            </div>
        </div>
        <div class="form-group">
            <label for="doc_name" class="col-sm-2 control-label">科室</label>
            <div class="col-sm-10">
                <p class="form-control-static">{{$info->dept}}</p>
            </div>
        </div>
        <div class="form-group">
            <label for="doc_name" class="col-sm-2 control-label">职称</label>
            <div class="col-sm-10">
                <p class="form-control-static">{{$info->professional}}</p>
            </div>
        </div>
        <div class="form-group">
            <label for="doc_name" class="col-sm-2 control-label">职务</label>
            <div class="col-sm-10">
                <p class="form-control-static">{{$info->position}}</p>
            </div>
        </div>
        <div class="form-group">
            <label for="doc_name" class="col-sm-2 control-label">联系电话</label>
            <div class="col-sm-10">
                <p class="form-control-static">{{$info->telephone}}</p>
            </div>
        </div>
        <div class="form-group">
            <label for="doc_name" class="col-sm-2 control-label">区号</label>
            <div class="col-sm-10">
                <p class="form-control-static">{{$info->areacode}}</p>
            </div>
        </div>
        <div class="form-group">
            <label for="doc_name" class="col-sm-2 control-label">邮政编码</label>
            <div class="col-sm-10">
                <p class="form-control-static">{{$info->postalcode}}</p>
            </div>
        </div>

        <div class="form-group">
            <label for="doc_position" class="col-sm-2 control-label">审核状态</label>
            <div class="col-sm-10">
                <label class="radio-inline"><input type="radio" name="status" {{$info->status ? 'checked' : ''}} value="1"> 通过审核</label>
                <label class="radio-inline"><input type="radio" name="status" {{!$info->status ? 'checked' : ''}} value="0"> 审核中</label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" name="submit" class="btn btn-default">确定</button>
            </div>
        </div>
    </form>
@stop
