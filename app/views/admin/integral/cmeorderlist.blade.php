@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>好医生CME课程兑换列表</h3>
	</div>
	<div>
	<form class="navbar-form navbar-left"  method="get" action="/admcmeorder">
	  <div class="form-group">
	  	<span>用户姓名：</span>
	    <input type="text" class="form-control" name="user_name" value="{{Input::get('user_name','')}}">
	  </div>
	  <button type="submit" class="btn btn-default" style="margin-left:17px;">检索</button>
	  <a href="/admcmeexport" class="export-excel btn btn-primary" target="_blank" style="margin-left:10px;">导出未兑换记录</a><br /><br />
	</form>
	</div>
	<table class="table table-striped">
      <thead>
        <tr>
          <th style="text-align:center;">id</th>
          <th style="text-align:center;">用户姓名</th>
          <th style="text-align:center;">消耗积分</th>
          <th>备注</th>
          <th>兑换时间</th>
          <th>状态</th>
          <th>审核时间</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oLists as $k=>$v)
        <tr>
          <th scope="row" style="text-align:center;">{{$v->id}}</th>
          <td style="text-align:center;">{{$v->user_name}}</td>
          <td style="text-align:center;">{{$v->number}}</td>
          <td>{{$v->obj_title}}</td>
          <td>{{$v->created_at}}</td>
          <td>{{$v->status ? '已兑换': '审核中'}}</td>
          <td>{{$v->status?$v->check_time:''}}</td>
          <td><a href="/admcmecheck?id={{$v->id}}">确认审核</a></td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oLists->appends($aLink)->links()}}
@stop
