@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>空中课堂--编辑调研</h3>
	</div>
	
	<script>
	        	function addOption(){
		        	var arrOption = ["A","B","C","D","E","F","G","H","I","J"];
		        	var itemNum = 10;
        	        var optionnum=$("#optionnum").val();
	        		if(parseInt(optionnum)>parseInt(itemNum)){
	        			return false;
	        		}
	        		$("#optiondiv").append("<div class='form-group' id='option"+optionnum+"'><label  class='col-sm-2 control-label'>选项"+arrOption[optionnum-1]+"<strong>:</strong></label><div class='col-sm-8'><input type='text' class='form-control' id='optionvalue"+optionnum+"' name='optionvalue"+arrOption[optionnum-1]+"'></div></div>");
	        		if(parseInt(optionnum)<=parseInt(itemNum)){
	        			optionnum++;
	        		}		
	        		$("#optionnum").val(optionnum);
	        	}
	        	function delOption(){
	        		var optionnum=$("#optionnum").val();
	        		var option=optionnum-1;
	        		$("#option"+option).remove();
	        		if(parseInt(optionnum)>parseInt(1)){
	        			optionnum--;
	        		}
	        		$("#optionnum").val(optionnum);
	        	}
        	</script>
<form action="/admsurvey/do-add" method="get" class="form-horizontal" role="form">
     <input type="hidden" class="form-control" id="doc_name" name="iId" value="{{$oUserSurveyQuestion -> id}}">
	 <input type="hidden" class="form-control" id="doc_hospital" name="phase" value="{{$oUserSurveyQuestion->phase_id}}">
	<div class="form-group">
	    <label for="title" class="col-sm-2 control-label">调研题目<span style="color:red;">*</span></label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" id="title" name="q_title" value="{{$oUserSurveyQuestion->q_title}}">
	    </div>
	</div>
	<div class="form-group">
	    <label for="type" class="col-sm-2 control-label">题目类型<span style="color:red;">*</span></label>
	    <div class="col-sm-2">
		     <div class="input-group">
			      <span class="input-group-addon">
			        <input type="radio" name="q_type" value="1" @if($oUserSurveyQuestion->q_type == 1) checked @endif>
			      </span>
			      <input type="text" class="form-control" value="单选" style="width:70px;">
			      <span class="input-group-addon">
			        <input type="radio" name="q_type" value="2" @if($oUserSurveyQuestion->q_type == 2) checked @endif>
			      </span>
			      <input type="text" class="form-control" value="多选" style="width:70px;">
		     </div>
	    </div>
	</div>
	<div class="form-group">
	    <label for="result" class="col-sm-2 control-label">正确答案<span style="color:red;">*</span></label>
	    <div class="col-sm-6">
	      <input type="text" class="form-control" style="width:100px;" id="result" name="q_result" value="{{$oUserSurveyQuestion->q_result}}">不区分大小写（例如：A，AB）
	    </div>
	</div>
	<div class="form-group">
	    <label for="listorder" class="col-sm-2 control-label">题目题号<span style="color:red;">*</span></label>
	    <div class="col-sm-1">
	      <input type="text" class="form-control" id="listorder" name="q_order" value="{{$oUserSurveyQuestion->q_order}}">
	    </div>
	</div>
	<div class="form-group">
		<label for="type" class="col-sm-2 control-label">&nbsp;</label>
		<button class="btn btn-primary" type="button" onclick="addOption()">添加选项</button>
		<button class="btn btn-primary" type="button" onclick="delOption()">删除选项</button>
        <input type="hidden" value="{{count($oUserSurveyItem)+1}}" id="optionnum" name="optionnum">
	</div> 
	<div id="optiondiv">
	@foreach($oUserSurveyItem as $k=>$v)
		<div class="form-group" id="option{{$k+1}}">
		    <label class="col-sm-2 control-label">选项{{$v->item}}<strong>:</strong></label>
		    <div class="col-sm-8">
		      <input type="text" class="form-control" id="optionvalue{{$k+1}}" name="optionvalue{{$v->item}}" value="{{$v->item_title}}">
		    </div>
		</div>
	@endforeach
	</div>
	<div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">确定</button>
	    </div>
  	</div>

</form>
@stop
