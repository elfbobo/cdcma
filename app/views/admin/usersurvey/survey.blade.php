@extends('admin.common.layout')
@section('content')
<!-- 配置文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/assets/js/ueditor/ueditor.all.js"></script>
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script type="text/javascript" src="/assets/js/ueditor/lang/zh-cn/zh-cn.js"></script>
<script src="/assets/js/ajaxsurveyload.js"></script>
<script src="/assets/js/surveyadd.js"></script>
	<div class="app_content_div" id="app_content_div_301Index">
		<h5>添加选项</h5>
	</div>
	
	<form class="form-horizontal"  method="get" action="/admsurvey/do-add-choice">
	   
	  <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;"></span></label>
	    <div class="col-sm-10">
	      <hr/>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;"></span></label>
	    <div class="col-sm-10">
	      <input type="hidden" class="form-control" id="doc_hospital" name="iId" value="{{$aChoice['iId']}}">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;"></span></label>
	    <div class="col-sm-10">
	      <input type="hidden" class="form-control" id="doc_hospital" name="phase" value="{{$aChoice['phase']}}">
	    </div>
	  </div>
	     
	                   
	  <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;">*</span>A</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_hospital" name="A" value="@if(!empty($aChoice['A'])){{$aChoice['A']}}@endif">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;">*</span>B</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_hospital" name="B" value="@if(!empty($aChoice['B'])){{$aChoice['B']}}@endif ">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;">*</span>C</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_hospital" name="C" value="@if(!empty($aChoice['C'])){{$aChoice['C']}}@endif ">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;">*</span>D</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_hospital" name="D" value="@if(!empty($aChoice['D'])){{$aChoice['D']}}@endif ">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;">*</span>E</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_hospital" name="E" value="@if(!empty($aChoice['E'])){{$aChoice['E']}}@endif ">
	    </div>
	   </div>
	   <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;">*</span>F</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_hospital" name="F" value="@if(!empty($aChoice['F'])){{$aChoice['F']}}@endif ">
	    </div>
	   </div>
	   <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;">*</span>G</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_hospital" name="G" value="@if(!empty($aChoice['G'])){{$aChoice['G']}}@endif ">
	    </div>
	   </div>
	   <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;">*</span>H</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_hospital" name="H" value="@if(!empty($aChoice['H'])){{$aChoice['H']}}@endif ">
	    </div>
	   </div>
	   <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;">*</span>I</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_hospital" name="I" value="@if(!empty($aChoice['I'])){{$aChoice['I']}}@endif ">
	    </div>
	   </div>
	   <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;">*</span>J</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="doc_hospital" name="J" value="@if(!empty($aChoice['J'])){{$aChoice['J']}}@endif ">
	    </div>
	   </div>
	   <div class="form-group">
	    <label for="doc_hospital" class="col-sm-2 control-label"><span style="color:red;"></span></label>
	    <div class="col-sm-10">
	      <hr/>
	    </div>
	   </div>
<!--	  <div class="form-group" >-->
<!--	    <label for="doc_introduction" class="col-sm-2 control-label"><span style="color:red;">*</span>简介</label>-->
<!--	    <div class="col-sm-10">-->
<!--	    <script id="doc_introduction" name="doc_introduction" type="text/plain"></script>-->
<!--		<script type="text/javascript">-->
<!--		    var editor = UE.getEditor('doc_introduction')-->
<!--		</script>-->
<!--	    </div>-->
<!--	  </div>-->
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">确定</button>
	    </div>
	  </div>
	</form>
@stop


