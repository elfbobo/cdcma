@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>调研题目列表</h3>
	</div>
<!--	<form action="/admsurvey/question-list">-->
<!--	 调研期:<input type="text" name='phase' >-->
<!--	 <button type="submit">搜索</button>-->
<!--    </form>-->
	<div style="float:right;">
	
	<button type="button" class="btn btn-primary" onclick="window.location.href='/admsurvey/add-question'">新增调研题目</button></div>
	<div style="float:left;">
	
	<button type="button" class="btn btn-primary" onclick="window.location.href='/admsurvey/survey-result'">导出调研结果</button></div>
	<table class="table table-striped">
      <thead>
        <tr>
          <th>期号</th>
          <th>题目题号</th> 
          <th>题目</th>
          <th>类型</th>
          <th>正确答案</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oUserSurveyQuestion as $k=>$v)
        <tr>
          <td>{{$v->phase_id}}</td>
          <td>{{$v->q_order}}</td>
           
          <td>{{$v->q_title}}</td>
          <td>@if($v->q_type == '1') 单选@else 多选 @endif</td>
          <td>{{$v->q_result}}</td>
           
          
          <td>
<!--               <a href="/admsurvey/has-choice-or-not/{{$v->id}}/{{$v->phase_id}}" >选项 </a></span>-->
<!--          		&nbsp;&nbsp;&nbsp;-->
          		<a href="/admsurvey/edit-survey/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
          		&nbsp;&nbsp;&nbsp;
          		<a href="/admsurvey/delete-survey/{{$v->id}}" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
                &nbsp;&nbsp;&nbsp;
          		 
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oUserSurveyQuestion ->links()}}
     
@stop
