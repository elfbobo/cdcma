@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>空中课堂--新增调研</h3>
	</div>
	
	<script>
	        	function addOption(){
		        	var arrOption = ["A","B","C","D","E","F","G","H","I","J"];
		        	var itemNum = 10;
        	        var optionnum=$("#optionnum").val();
	        		if(parseInt(optionnum)>parseInt(itemNum)){
	        			return false;
	        		}
	        		$("#optiondiv").append("<div class='form-group' id='option"+optionnum+"'><label  class='col-sm-2 control-label'>选项"+arrOption[optionnum-1]+"<strong>:</strong></label><div class='col-sm-8'><input type='text' class='form-control' id='optionvalue"+optionnum+"' name='optionvalue"+arrOption[optionnum-1]+"'></div></div>");
	        		if(parseInt(optionnum)<=parseInt(itemNum)){
	        			optionnum++;
	        		}		
	        		$("#optionnum").val(optionnum);
	        	}
	        	function delOption(){
	        		var optionnum=$("#optionnum").val();
	        		var option=optionnum-1;
	        		$("#option"+option).remove();
	        		if(parseInt(optionnum)>parseInt(1)){
	        			optionnum--;
	        		}
	        		$("#optionnum").val(optionnum);
	        	}
        	</script>
<form action="/admdocface/add-survey/{{$iVid}}" method="post" class="form-horizontal" role="form">

	<div class="form-group">
	    <label for="title" class="col-sm-2 control-label">题目<span style="color:red;">*</span></label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" id="title" name="title" value="">
	    </div>
	</div>
	<div class="form-group">
	    <label for="type" class="col-sm-2 control-label">类型<span style="color:red;">*</span></label>
	    <div class="col-sm-2">
		     <div class="input-group">
			      <span class="input-group-addon">
			        <input type="radio" name="type" value="1">
			      </span>
			      <input type="text" class="form-control" value="单选" style="width:70px;">
			      <span class="input-group-addon">
			        <input type="radio" name="type" value="2">
			      </span>
			      <input type="text" class="form-control" value="多选" style="width:70px;">
		     </div>
	    </div>
	</div>
	<div class="form-group">
	    <label for="result" class="col-sm-2 control-label">正确答案<span style="color:red;">*</span></label>
	    <div class="col-sm-6">
	      <input type="text" class="form-control" style="width:100px;" id="result" name="result" value="">不区分大小写（例如：A，AB）
	    </div>
	</div>
	<div class="form-group">
	    <label for="listorder" class="col-sm-2 control-label">排序位<span style="color:red;">*</span></label>
	    <div class="col-sm-1">
	      <input type="text" class="form-control" id="listorder" name="listorder" value="0">
	    </div>
	</div>
	<div class="form-group">
		<label for="type" class="col-sm-2 control-label">&nbsp;</label>
		<button class="btn btn-primary" type="button" onclick="addOption()">添加选项</button>
		<button class="btn btn-primary" type="button" onclick="delOption()">删除选项</button>
         <input type="hidden" value="1" id="optionnum" name="optionnum">
		 
	</div> 
	<div id="optiondiv"></div>
	<div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">确定</button>
	    </div>
  	</div>

</form>
@stop
