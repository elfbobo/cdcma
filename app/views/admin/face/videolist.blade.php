@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>空中课堂--视频列表</h3>
	</div>
	<div style="float:right;"><button type="button" class="btn btn-primary" onclick="window.location.href='/admdocface/add-video'">新增视频</button></div>
    <div style="float: left">
        <form class="navbar-form navbar-left" action="" method="get">
            <div class="form-group">
                <span>专家：</span>
                <input type="text" class="form-control" name="name" value="{{Input::get('name','')}}" placeholder="">
            </div>
            <div class="form-group" style="padding-left: 10px;">
                <span>医院：</span>
                <input type="text" class="form-control" name="hospital" value="{{Input::get('hospital','')}}" placeholder="">
            </div>
            <button type="submit" class="btn btn-default">检索</button>
        </form>
    </div>
    <div style="clear: both"></div>
	@if(!in_array(Auth::User()->id,Config::get("config.docface")))
	<div style="float:right;"><button type="button" class="btn btn-primary" onclick="window.location.href='/admdocface/review-log'">导出录播观看情况</button></div>
	@endif
	截止当前时间累计观看直播医生数为：<span id="live_count" style="color:red;">统计中...</span><br>
	截止当前时间累计观看录播医生数为：<span id="review_count" style="color:red;">统计中...</span>
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>标题</th>
          <th>科室</th>
          <th>专家</th>
          <th nowrap="">类型</th>
          <th>直播开始时间</th>
          <th>直播结束时间</th>
          <th>更新签到积分</th>
          <th>试题列表</th>
          <th>直播评分列表</th>
          <!--<th>积分导出</th> -->
		      @if(!in_array(Auth::User()->id,Config::get("config.docface")))
          <th>周报导出</th>
          @endif
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
      <?php 
			$aDepartment = Config::get('config.department');
			?>
      	@foreach($oVideos as $k=>$v)
        <tr>
          <th scope="row">{{$v->id}}</th>
          <td>{{str_cut_cms($v->video_title,30)}}</td>
          <td>
          @foreach($aDepartment as $kdep=>$vdep)
          @if(strpos($v->department_id, $kdep.'|') !== false)
          {{$vdep}}<br>
          @endif
          @endforeach
          </td>
          <td><?php  echo $oDocs[$v->doc_id]; ?></td>
          <td><?php if($v->video_type==1){ echo '录播'; }else{ echo '直播'; } ?></td>
          <td>{{$v->start_time}}</td>
          <td>{{$v->end_time}}</td>
          <td><a class="updateSignin" href="javascript:;" data-url="/admdocface/update-sign-in/{{$v->id}}" >更新签到积分</a></td>
          <td><a href="/admdocface/survey/{{$v->id}}" >试题列表</a></td>
          <td><a href="/admdocface/live-score/{{$v->id}}" >直播评分列表</a></td>
          <!--<td><a href="/admdocface/export-log/{{$v->id}}" >积分导出</a></td>-->
		      @if(!in_array(Auth::User()->id,Config::get("config.docface")))
          <td><a href="/admdocface/week-log/{{$v->id}}" >周报导出</a></td>
          @endif
          <td>
      		<a href="/admdocface/edit-video/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
      		&nbsp;&nbsp;&nbsp;
      		<a href="#" onclick="delUser('/admdocface/del-video/{{$v->id}}')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oVideos->appends($params)->links()}}
    <script>
    $(function(){
      // 获取统计数
      $.get('', {count:1}, function(data){
        console.log(data);
        $('#live_count').text(data.data.live_count);
        $('#review_count').text(data.data.review_count);

      });

      $(".updateSignin").on("click",function(){
        var url = $(this).attr("data-url");
        $.get(url,{},function(o){
          console.log(o);
          if(o=="success"){
            alert("恭喜您，更新成功！");
            location.reload();
          }
        });
      });
    });
		function delUser(url){
			if(confirm('确定要删除该条记录吗？')){
				window.location.href = url;
			}
		}	
	</script>
@stop
