@extends('admin.common.layout')
@section('content')
<script>
function export_detail(count){
	var month = window.prompt("请输入要导出的月份（如：2015-05）");
    if( month)  //当用户输入内容，点确定后；
    {
        window.location.href="/admdocface/mounth-log/"+month+"/"+count;
    }                
}
function export_detail_regin(regin){
	var month = window.prompt("请输入要导出的月份（如：2015-05）");
    if( month)  //当用户输入内容，点确定后；
    {
        window.location.href="/admdocface/mounth-log-regin/"+month+"/"+regin;
    }                
}
function cluster(url){
	var month = window.prompt("请输入要导出的月份（如：2015-05）");
    if( month)  //当用户输入内容，点确定后；
    {
        window.location.href=url+month;
    } 
}
		
</script>
<div class="app_content_div">
          <h2 class="sub-header">KPI报告</h2>
          <div class="table-responsive">
          <table class="table table-striped">
           </table>
            <table class="table table-striped">
              <thead>
              </thead>
              <tbody>
              
                <tr>
                  	<td style="width:90%;">导出月总明细表01</td>
                	<td><a onclick="export_detail(1);"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td style="width:90%;">导出月总明细表02</td>
                	<td><a onclick="export_detail(2);"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td style="width:90%;">导出月总明细表03</td>
                	<td><a onclick="export_detail(3);"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td style="width:90%;">导出月总明细表04</td>
                	<td><a onclick="export_detail(4);"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td style="width:90%;">导出月总明细表05</td>
                	<td><a onclick="export_detail(5);"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td style="width:90%;">导出月总明细表--ME北大区</td>
                	<td><a onclick="export_detail_regin(17);"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td>导出月Cluster排名</td>
                	<td><a onclick="cluster('/admdocface/cluster/');"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td>导出月大区观看医生数排行</td>
                	<td><a onclick="cluster('/admdocface/regin-docs/');"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td>导出月地区观看医生数排行</td>
                	<td><a onclick="cluster('/admdocface/area-docs/');"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td>导出月代表观看医生数排行</td>
                	<td><a onclick="cluster('/admdocface/rep-docs/');"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
                <tr>
                  	<td>导出月大医生组织观看医生数排行</td>
                	<td><a onclick="cluster('/admdocface/bigdoc-docs/');"><button type="button" class="btn btn-primary">导出csv</button></a></td>
                </tr>
              </tbody>
            </table>
          </div>
</div>
@stop


