@extends('admin.common.layout')
@section('content')
	<div class="app_content_div" id="app_content_div_301Index">
		<h3>空中课堂--专家列表</h3>
	</div>
	<div style="float:right;"><button type="button" class="btn btn-primary" onclick="window.location.href='/admdocface/add-doc'">新增专家</button></div>
	<div style="float:left;">
        <form class="navbar-form navbar-left" action="" method="get">
            <div class="form-group">
                <span>姓名：</span>
                <input type="text" class="form-control" name="user_name" value="{{Input::get('user_name','')}}" placeholder="">
            </div>
            <div class="form-group" style="padding-left: 10px;">
                <span>医院：</span>
                <input type="text" class="form-control" name="hospatil" value="{{Input::get('hospatil','')}}" placeholder="">
            </div>
            <button type="submit" class="btn btn-default">检索</button>
        </form>
    </div>
	<table class="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>姓名</th>
          <th>职称</th>
          <th>科室</th>
          <th>医院</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
      	@foreach($oDocs as $k=>$v)
        <tr>
          <th scope="row">{{$v->id}}</th>
          <td>{{$v->doc_name}}</td>
          <td>{{$v->doc_position}}</td>
          <td>{{$v->doc_department}}</td>
          <td>{{$v->doc_hospital}}</td>
          <td>
          		<a href="/admdocface/edit-doc/{{$v->id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>
          		&nbsp;&nbsp;&nbsp;
          		<a href="#" onclick="delUser('/admdocface/del-doc/{{$v->id}}')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$oDocs->appends($params)->links()}}
    <script>
		function delUser(url){
			if(confirm('确定要删除该专家吗？')){
				window.location.href = url;
			}
		}	
	</script>
@stop
