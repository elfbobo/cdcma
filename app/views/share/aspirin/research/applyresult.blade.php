@extends('share.aspirin.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
	<div class="page_top_box" style="display:none;">
    	<input type="button" class="btn_back" style="display:none;"/>
        科研申请
        <input type="button" class="btn_share" style="display:none;" />
    </div>
    <div class="page_cont_box">
    	<div class="download_app_box">
        	<div class="download_app_txt">
            	如需进行科研申请，请下载国卫健康云APP。
            </div>
            <div class="download_app_link">
            	<a href="/app">点击下载</a>
            </div>
        </div>
        <div class=" infor_box">
            <div class="infor_cont">
            	<div class="infor_line">
                	<div class="infor_label">
                    	姓名：
                    </div>
                    <div class="infor_txt">
                    	{{$oApply->apply_name}}
                    </div>
                </div>
                <div class="infor_line">
                	<div class="infor_label">
                    	城市：
                    </div>
                    <div class="infor_txt">
                    	{{$oApply->apply_city}}
                    </div>
                </div>
                <div class="infor_line">
                	<div class="infor_label">
                    	出生日期：
                    </div>
                    <div class="infor_txt">
                    	{{$oApply->apply_birthday}}
                    </div>
                </div>
                <div class="infor_line">
                	<div class="infor_label">
                    	职称：
                    </div>
                    <div class="infor_txt">
                    	{{$oApply->apply_title}}
                    </div>
                </div>
                <div class="infor_line">
                	<div class="infor_label">
                    	联系电话：
                    </div>
                    <div class="infor_txt">
                    	{{$oApply->apply_tel}}
                    </div>
                </div>
                <div class="infor_line">
                	<div class="infor_label">
                    	电子邮箱：
                    </div>
                    <div class="infor_txt">
                    	{{$oApply->apply_email}}
                    </div>
                </div>
                <div class="infor_line">
                	<div class="infor_label">
                    	单位名称：
                    </div>
                    <div class="infor_txt">
                    	{{$oApply->apply_company}}
                    </div>
                </div>
                <div class="infor_line">
                	<div class="infor_label">
                    	地址：
                    </div>
                    <div class="infor_txt">
                    	{{$oApply->apply_address}}
                    </div>
                </div>
                <div class="infor_line">
                	<div class="infor_label">
                    	邮编：
                    </div>
                    <div class="infor_txt">
                    	{{$oApply->apply_postcode}}
                    </div>
                </div>
                <div class="infor_line">
                	<div class="infor_label">
                    	教育背景：
                    </div>
                    <div class="infor_txt">
                    	{{$oApply->education_background}}
                    </div>
                </div>
                <div class="infor_line">
                	<div class="infor_label">
                    	工作经历：
                    </div>
                    <div class="infor_txt">
                    	{{$oApply->work_experience}}
                    </div>
                </div>
                <div class="infor_line">
                	<div class="infor_label" style="width:84px;">
                    	既往科研经历：
                    </div>
                    <div class="infor_txt">
                    	{{$oApply->research_experience}}
                    </div>
                </div>                
                <div class="pbtm blue">
               		 您可以选择下列两个方向中的任意一个 做科研设想及申请：
                </div>
                <div class="pbtm blue">
					方向一（心血管疾病一级预防）： 心血管疾病的一级预防主要是通过对不良生活方式的干预（运动、饮食、戒烟酒）、高危人群抗血小板治疗、综合管理血脂、血糖及血压来减少心血管疾病的发生，在这些措施中，您认为哪个（些）措施在实施过程中尚存在需要进一步解决的问题？主要的问题是什么？需要相应采取什么措施来解决？
                </div>
                <div class="pbtm">
                	{{$oApply->question1}}
                </div>
                <div class="pbtm blue">
                	方向二（心血管疾病二级预防）： 心血管疾病二级预防的诸多措施，如生活方式干预、规范的抗血小板治疗、综合管理血脂、血糖和血压，来降低心血管疾病的复发及死亡，在这些措施中，您认为哪个（些）措施在实施过程中尚存在进一步解决的问题？主要的问题是什么？需要相应采取什么措施来解决？ 
                </div>
                <div class="pbtm">
                	{{$oApply->question2}}
                </div>
                <div class="pbtm blue" style="font-size:13px;color:red;">
                	书写要求（请结合您认为需要解决的问题，简要陈述科研设想的背景、科研假说、科研目的，以及预期要得出的结论）
                </div>
                </br>
                <div class="pbtm blue">
                	针对您提到的关于心血管预防的问题，陈述相应的解决方案？
                </div>
                <div class="pbtm blue">
                	（请简述拟采用的科研方法/目标人群/干预方法/主要观察指标/次要观察指标等）
                </div>
                <div class="pbtm">
                	{{$oApply->question3}}
                </div>
            </div>
        </div>
    </div>
@stop
