@extends('share.aspirin.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
<?php 
$isWxFlag = user_agent_is_weixin();
$wx_config = array();
if ($isWxFlag){
	include_once  app_path().'/include/jssdk.class.php';  //先把类包含进来
	$oJssdk = new jssdk();
	$wx_config = $oJssdk->getSignPackage();
}
?>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
@if($isWxFlag)
wx.config({
	debug:false,
	appId: "{{$wx_config['appId']}}",//必填
	timestamp:{{$wx_config['timestamp']}}, // 必填，生成签名的时间戳
	nonceStr: "{{$wx_config['nonceStr']}}", // 必填，生成签名的随机串
	signature: "{{$wx_config['signature']}}",// 必填，签名
	jsApiList: ['onMenuShareTimeline','onMenuShareAppMessage']// 必填，需要使用的JS接口列表，所有JS接口列表见附录2
});
wx.ready(function(){
	wx.onMenuShareTimeline({//分享到朋友圈
	    title:'专项基金科研培训简介',
	    link: '{{Config::get('app.url')}}/aspirinshare/research-brief',
	    imgUrl: '{{Config::get('app.url')}}/assets/images/mobile/logo.png',
	    success: function () {  },
	    cancel: function () {  }
	});
	wx.onMenuShareAppMessage({//分享给朋友
	    title: '专项基金科研培训简介',
	    desc: '专项基金科研培训简介',
	    link: '{{Config::get('app.url')}}/aspirinshare/research-brief',
	    imgUrl: '{{Config::get('app.url')}}/assets/images/mobile/logo.png',
	    type: '',
	    dataUrl: '',
	    success: function (){ },
	    cancel: function (){ }
	});
});
wx.error(function(res){ });
@endif
</script>
<div class="page_top_box" style="display:none;">
    <input type="button" class="btn_back" style="display:none;"/>
        科研培训简介
    <input type="button" class="btn_share" style="display:none;"/>
</div>
<div class="page_cont_box">
    <div>
        <img class="wid100" src="/assets/images/aspirin/img03.jpg" />
    </div>
    <div class=" infor_box">
        <div class=" infor_tit">
            背景介绍
        </div>
        <div class=" infor_cont">
            <p class="indent">
                随着我国人口老龄化的加剧以及心血管疾病危险因素的流行，心脑血管疾病的患病率及死亡率不断上升，及早采取积极的干预措施并加强心血管疾病的防治工作刻不容缓。
            </p>
            <p class="indent">
                为增加医生对心血管疾病预防理念的理解及重视，增强医生对阿司匹林在心血管疾病预防中的认识及规范使用；加强我国心血管医生与国际上的合作与沟通，提高临床医生在心血管疾病预防领域的科研工作能力，心馨心血管健康（苏州工业园区）基金会特开展阿司匹林专项基金科研培训项目。
            </p>
        </div>
    </div>
    <div class=" infor_box">
        <div class=" infor_tit clear"style="background-color:#75b437;">
            培训介绍
        </div>
        <div class=" infor_cont">
            <p style="margin-bottom: 20px">
                <span class="bold">活动发起：</span>心馨心血管健康（苏州工业园区）基金会2017年4月启动该项目，向心内科医生发起科研培训项目介绍以及报名通知，在心血管预防科研领域有经验及意向者可以下载申请表填写并发到基金会的专用邮箱: aspirin@ccahouse.org ，申请表收集时限为2017年4月至7月底。
            </p>
            <p style="margin-bottom: 20px">
                <span class="bold">申请人筛选及科研课程培训：</span>1)网络培训课程：心馨心血管健康（苏州工业园区）基金会7月底收集整理所有项目申请人资料，预计申请人数100-150人，面向所有参与者，8月份由专业科研机构Medsci公司结合心血管预防领域的科研工作提供2场线上基础班网络培训课程，课程内容涉及心血管领域，内容浅显易懂。 2)学员筛选与初级班科研培训：心鑫心血管健康（苏州工业园区）基金会收集整理所有参与者的项目申请书，并在8月份收集整理所有申请者资料，按照评分标准，初步筛选出能够入围初级班科研培训的优秀申请人，预计60人，在8-9月份开展2场线下面对面培训课程（地点初定上海及成都），课程内容涉及心血管领域，针对科研项目实施中的问题进行专业培训。初级班优秀申请人接受培训后，可以按照培训课程的收获对其申请资料进行更新。 3)高级班学员筛选与培训：心馨心血管健康（苏州工业园区）基金会2017年9月份对初级班申请人更新后的申请书进行综合测评，邀请10名专家委员组按照评分标准，对候选人申请资料进行二次评分，每位专家测评30份申请表，筛选出分数最高的前30位申请人参加10月份全国高级班科研培训课程，针对申请人在心血管领域科研项目实施过程中的共性问题，进行深入探讨及培训。活动结束后，提供专家工作量的报告,并给予学员指导性建议及点评。 高级班面对面培训会议之后，针对个体化的需求，为30位优秀申请人每人提供3次电话培训（共计90次）。
            </p>
            <p style="margin-bottom: 20px">
                <span class="bold">科研培训内容：</span>心馨心血管健康（苏州工业园区）基金会预计在8-9月召开面对面培训会议（会议地点需要排除典型旅游及度假圣地，初定上海及成都）。 上午集中授课：由专业科研机构Medsci公司结合心血管预防领域的科研工作进行授课，上午内容涉及但不局限于：1、心血管领域临床研究如何选题，2、如何选择合适的研究类型，3、心血管领域研究设计要点等，其中的科研举例均以阿司匹林既往相关的研究为例。 下午实战演练及专家面对面： 20-30名学员分成2组，由Medsci培训师以及2名心血管领域科研经验丰富的专家指导。每位专家及培训师负责10-15位被培训人，对被培训人面对面培训，各组结合临床科研情况，可以阿司匹林为例，进行案例分享及讨论。每组选取一名候选人代表各组发言，由专家及培训师进行点评指导。
            </p>
            <p style="margin-bottom: 20px">
                <span class="bold">电话培训具体内容：</span>专业医学培训公司针对30位优秀申请人进行一对一电话培训，每人3次课程，可根据但不局限于所列出的课程安排进行选择，具体包括临床研究类型及研究思维设计，国自然标书撰写，临床研究数据库的建立与管理等，培训过程可以阿司匹林既往已经发表的文献为例。（培训结束后提供电话培训的相关证明材料作为支持，包括培训时间、培训内容、被培训人、培训师及电话时长）
            </p>
            <div class="qr_code_box"><img src="/assets/images/aspirin/qr_code.png" alt=""><p>了解更多专项基金详情内容请扫描二维码查看</p></div>
        </div>
    </div>
</div>
@stop
