@extends('share.aspirin.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
	<div class="page_top_box" style="display:none;">
    	<input type="button" class="btn_back" style="display:none;"/>
        科研申请
        <input type="button" class="btn_share" style="display:none;" />
    </div>
    <div class="page_cont_box">
    	<div class="download_app_box">
        	<div class="download_app_txt">
            	如需进行科研申请，请下载国卫健康云APP。
            </div>
            <div class="download_app_link">
            	<a href="/app">点击下载</a>
            </div>
        </div>
        <div class=" infor_box">
            <div class="fill_infor_box">
            	<div class="fill_block">
                	<div class="fill_line">
                    	<label class="fill_label">姓名：</label>
                        <input class="fill_txt" type="text" onclick="showbox();"/>
                    </div>
                    <div class="fill_line">
                    	<label class="fill_label">城市：</label>
                        <input class="fill_txt" type="text" onclick="showbox();"/>
                    </div>
                    <div class="fill_line">
                    	<label class="fill_label">出生日期：</label>
                        <input class="fill_txt" type="text" onclick="showbox();"/>
                    </div>
                    <div class="fill_line">
                    	<label class="fill_label">职称：</label>
                        <input class="fill_txt" type="text" onclick="showbox();"/>
                    </div>
                    <div class="fill_line">
                    	<label class="fill_label">联系电话：</label>
                        <input class="fill_txt" type="text" onclick="showbox();"/>
                    </div>
                    <div class="fill_line">
                    	<label class="fill_label">电子邮箱：</label>
                        <input class="fill_txt" type="text" onclick="showbox();"/>
                    </div>
                    <div class="fill_line">
                    	<label class="fill_label">单位名称：</label>
                        <input class="fill_txt" type="text" onclick="showbox();"/>
                    </div>
                    <div class="fill_line">
                    	<label class="fill_label">地址：</label>
                        <input class="fill_txt" type="text" onclick="showbox();"/>
                    </div>
                    <div class="fill_line">
                    	<label class="fill_label">邮编：</label>
                        <input class="fill_txt" type="text" onclick="showbox();"/>
                    </div>
                </div>
                <div class="fill_block">
                	<div>教育背景：</div>
                    <div class="">
                    	<textarea class="fill_area" onclick="showbox();"></textarea>
                    </div>
                </div>
                <div class="fill_block">
                	<div>工作经历：</div>
                    <div class="">
                    	<textarea class="fill_area" onclick="showbox();"></textarea>
                    </div>
                </div>
                <div class="fill_block">
                	<div>既往科研经历：</div>
                    <div class="">
                    	<textarea class="fill_area" onclick="showbox();"></textarea>
                    </div>
                </div>
                <div class="pbtm">
                	<p>您可以选择下列两个方向中的任意一个 做科研设想及申请：</p>
					<p>方向一（心血管疾病一级预防）： 心血管疾病的一级预防主要是通过对不良生活方式的干预（运动、饮食、戒烟酒）、高危人群抗血小板治疗、综合管理血脂、血糖及血压来减少心血管疾病的发生，在这些措施中，您认为哪个（些）措施在实施过程中尚存在需要进一步解决的问题？主要的问题是什么？需要相应采取什么措施来解决？</p>
                </div>
                <div class="fill_block">
                    <div class="">
                    	<textarea class="fill_area" onclick="showbox();"></textarea>
                    </div>
                </div>
                <div class="pbtm">
                	<p>方向二（心血管疾病二级预防）： 心血管疾病二级预防的诸多措施，如生活方式干预、规范的抗血小板治疗、综合管理血脂、血糖和血压，来降低心血管疾病的复发及死亡，在这些措施中，您认为哪个（些）措施在实施过程中尚存在进一步解决的问题？主要的问题是什么？需要相应采取什么措施来解决？ </p>
                </div>
                <div class="fill_block">
                    <div class="">
                    	<textarea class="fill_area" onclick="showbox();"></textarea>
                    </div>
                </div>
                <div class="pbtm" style="font-size:13px;color:red;">
                	<p>书写要求（请结合您认为需要解决的问题，简要陈述科研设想的背景、科研假说、科研目的，以及预期要得出的结论）</p>
                </div>
                </br>
                <div class="pbtm">
                	<p>针对您提到的关于心血管预防的问题，陈述相应的解决方案？</p>
                    <p>（请简述拟采用的科研方法/目标人群/干预方法/主要观察指标/次要观察指标等）</p>
                </div>
                <div class="fill_block">
                    <div class="">
                    	<textarea class="fill_area" onclick="showbox();"></textarea>
                    </div>
                </div>
                <div class="screening_btn_block">
                	<input class="screening_btn" type="button" value="提交申请" onclick="showbox();"/>
                </div>
            </div>
        </div>
    </div>
    <script>
	function showbox()
	{
		alert('如需进行科研申请，请下载国卫健康云APP');
	}
	</script>
@stop
