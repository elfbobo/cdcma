@extends('share.aspirin.common.layout')
@section('title')国卫健康云-筛查 @stop
@section('description')国卫健康云-筛查@stop
@section('keywords')国卫健康云-筛查 @stop
@section('content')
    <div class="page_top_box" style="display:none;">
    	<input type="button" class="btn_back" style="display:none;"/>
        筛查
        <input type="button" class="btn_share" style="display:none;"/>
    </div>
    <div class="page_cont_box">
        <div class="results_share_box">
            <div class="results_share_txt01">
                <p>随着环境污染、生活及工作压力增加大</p>
                <p>我们离心血管疾病已经越来越近了</p>
            </div>
            <div class="results_share_pic1">
                <img class="wid100" src="/assets/images/aspirin/heart.png" />
            </div>
            <div class="results_share_txt02">
                <p>别担心，心血管疾病完成</p>
                <p>可以提前筛查、早期预防哦</p>
            </div>
            <div class="results_share_txt03">
                扫描（长按识别）下方二维码进行筛查
            </div>
            <div class="results_share_pic2">
                <img class="wid100" src="{{$sEwmUrl}}"/>
            </div>
        </div>
    </div>
    <div class="btm_pic">
        <img class="wid100" src="/assets/images/aspirin/btm_pic.png"/>
    </div>
@stop
