@extends('share.aspirin.common.layout')
@section('title')国卫健康云—筛查 @stop
@section('description')国卫健康云—筛查@stop
@section('keywords')国卫健康云—筛查 @stop
@section('content')
    <div class="page_top_box" style="display:none;">
    	<input type="button" class="btn_back" style="display:none;"/>
        筛查
        <input type="button" class="btn_search" style="display:none;"/>
    </div>
    <input type="hidden" id="iId" name="iId" value="{{$iId}}">
    <input type="hidden" id="sex" name="sex" value="{{$sex}}">
    <div class="page_cont_box">
        <div class="select_block">
            <div class="select_tit">
                您是否吸烟？
            </div>
            <div class="select clearfix">
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio01" name="question1" value="2">
                    <label class="sele_radio_lable" for="radio01">是</label>
                </div>
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio02" name="question1" value="1">
                    <label class="sele_radio_lable" for="radio02">否</label>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        @if($sex == 0)
        <div class="select_block">
            <div class="select_tit">
                您是否已经绝经？
            </div>
            <div class="select clearfix">
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio03" name="question0" value="2">
                    <label class="sele_radio_lable" for="radio03">是</label>
                </div>
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio04" name="question0" value="1">
                    <label class="sele_radio_lable" for="radio04">否</label>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        @else
        @endif
        <div class="select_block">
            <div class="select_tit">
                您是否有早发心脑血管疾病家族史？<span>（直系亲属中有男<55岁或女性<65岁的心血管疾病并发史）</span>
            </div>
            <div class="select clearfix">
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio05" name="question2" value="2">
                    <label class="sele_radio_lable" for="radio05">是</label>
                </div>
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio06" name="question2" value="1">
                    <label class="sele_radio_lable" for="radio06">否</label>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="select_block">
            <div class="select_tit">
                您是否已经有已确诊的糖尿病？
            </div>
            <div class="select clearfix">
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio07" name="question3" value="2" onclick="secondInfo(2);">
                    <label class="sele_radio_lable" for="radio07">是</label>
                </div>
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio08" name="question3" value="1" onclick="secondInfo(1);">
                    <label class="sele_radio_lable" for="radio08">否</label>
                </div>
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio09" name="question3" value="3" onclick="secondInfo(3);">
                    <label class="sele_radio_lable" for="radio09">不清楚</label>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="select_block" id="second_box" style="display:none;">
            <div class="select_tit">
                您是否在服用降糖病？
            </div>
            <div class="select clearfix">
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio010" name="question31" value="2">
                    <label class="sele_radio_lable" for="radio010">是</label>
                </div>
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio011" name="question31" value="1">
                    <label class="sele_radio_lable" for="radio011">否</label>
                </div>
                <div class="clear"></div>
            </div>
        </div>

    </div>
    <div class="page_btn_box">
        <input class="send_btn" type="button" value="继续" onclick="check();"/>
    </div>
    <script type="text/javascript">
	 function secondInfo(flag){
	     if(flag == 2){
	   	  $("#second_box").show();
	     }else if(flag == 1 || flag == 3){
	   	  $("#second_box").hide();
	     }
	 }
	</script>
    <script type="text/javascript">
	function check(){
		var iId = $("#iId").val();
		var sex = $("#sex").val();
		var question0 = $(':radio[name="question0"]:checked').val();
		var question1 = $(':radio[name="question1"]:checked').val();
		var question2 = $(':radio[name="question2"]:checked').val();
		var question3 = $(':radio[name="question3"]:checked').val();
		var question31 = $(':radio[name="question31"]:checked').val();
		if(!question1){
			alert("请选择是否吸烟");
			return false;
		}
		if(sex == 0 && !question0){
			alert("请选择是否已绝经");
			return false;
		}
		if(!question2){
			alert("请选择是否有早发心脑血管疾病家庭史");
			return false;
		}
		if(!question3){
			alert("请选择是否有确诊的糖尿病");
			return false;
		}
		if(question3 == 2 && !question31){
			alert("请选择是否在服用降糖药");
			return false;
		}

		var url = '/aspirinshare/screening-submit2';
		var data = {'iId':iId,
					'question0':question0,
					'question1':question1,
					'question2':question2,
					'question3':question3,
					'question31':question31
		};
		$.post(url,data,function(msg){
			if(msg.success){
				window.location.href= '/aspirinshare/screening-form3?id='+iId;
			}else{
				alert('请重新提交');
				return;
			}
		  },'json')
		
	}
	</script> 
@stop
