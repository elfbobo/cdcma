@extends('share.aspirin.common.layout')
@section('title')国卫健康云—筛查 @stop
@section('description')国卫健康云—筛查@stop
@section('keywords')国卫健康云—筛查@stop
@section('content')
	<div class="page_top_box" style="display:none;">
    	<input type="button" class="btn_back" style="display:none;"/>
        筛查
        <input type="button" class="btn_search" style="display:none;"/>
    </div>
    <input type="hidden" id="iUid" name="iUid" value="{{$iUid}}">
    <div class="page_cont_box">
        <div class="screening2_box">
            <div class="screening2_block sex">
                <div class="screening2_left">
                    性别
                </div>
                <div class="screening2_right sex_txt">
                    请选择
                </div>
            </div>
            <div class="screening2_block age">
                <div class="screening2_left">
                    年龄
                </div>
                <div class="screening2_right age_txt">
                    请选择
                </div>
            </div>
            <div class="screening2_block weight">
                <div class="screening2_left">
                    体重
                </div>
                <div class="screening2_right weight_txt">
                    请选择体重
                </div>
                <div class="screening2_label">
                    千克/公斤
                </div>
            </div>
            <div class="screening2_block height">
                <div class="screening2_left">
                    身高
                </div>
                <div class="screening2_right height_txt">
                    请选择身高
                </div>
                <div class="screening2_label">
                    厘米
                </div>
            </div>
        </div>
    </div>
    <div class="page_btn_box">
        <input class="send_btn" type="button" value="继续" onclick="check();"/>
    </div>
	<ul class="sex_list" style="display: none;">
    <li>男</li>
    <li>女</li>
	</ul>
	<ul class="weight_list" style="display: none;"></ul>
	<ul class="height_list" style="display: none;"></ul>
	
    
<script type="text/javascript">
	function check(){
		var uid = $("#iUid").val();
		var sex = $.trim($('.sex .sex_txt').html());
		var age = $.trim($('.age .age_txt').html());
		var weight = $.trim($('.weight .weight_txt').html());
		var height = $.trim($('.height .height_txt').html());
	
		if(sex == "请选择"){
			alert("请选择性别");
			return false;
		}
		if(age == "请选择"){
			alert("请选择年龄");
			return false;
		}
		if(weight == "请选择体重"){
			alert("请选择体重");
			return false;
		}
		if(height == "请选择身高"){
			alert("请选择身高");
			return false;
		}
		var url = '/aspirinshare/screening-submit';
		var data = {'uid':uid,'sex':sex,'age':age,'weight':weight,'height':height};
		$.post(url,data,function(msg){
			if(msg.success){
				var sourceid = msg.id;
				window.location.href= '/aspirinshare/screening-form2?id='+sourceid;
			}else{
				alert('请重新提交');
				return;
			}
		  },'json')
	}
</script> 
<script type="text/javascript">
    $(document).ready(function () {

        //选择年龄
        var myDate = new Date();
        var age =$('.age').mobiscroll().date({
            theme: "android-ics light",
            lang: "zh",
            display: 'bottom',
            startYear:1950,
           	endYear:myDate.getFullYear(),
            cancelText: null,
            cancelText: '清空',//清空按钮名称
            setText: '确定', //确认按钮名称
            dateFormat: 'yy年m月', //返回结果格式化为年月格式
            // wheels:[], 设置此属性可以只显示年月，此处演示，就用下面的onBeforeShow方法,另外也可以用treelist去实现
            onBeforeShow: function (inst) { inst.settings.wheels[0].length>2?inst.settings.wheels[0].pop():null; }, //弹掉“日”滚轮
            headerText: function (valueText) { return "选择出生年月"; },
            onSelect:function(valueText,inst){
                $(this).find('.age_txt').html(valueText);
                //console.log(valueText);
            }
        });

        //选择性别
        var sex_list = $(".sex_list").mobiscroll().treelist({
            theme: "android-ics light",
            lang: "zh",
            display: 'bottom',
            defaultValue: [Math.floor($('.sex_list li').length/2)],
            cancelText: null,
            inputClass:'mob_input',
            cancelText: '清空',//清空按钮名称
            setText: '确定', //确认按钮名称
            headerText: function (valueText) { return "选择性别"; },
            onSelect: function (valueText) {
                var m = $(this).find("li").eq(valueText).html();
                //console.log(m);
                $('.sex .screening2_right').html(m);            }
        });

        $('.sex').click(function () {
            sex_list.mobiscroll('show');
        });


        //创建体重list
        var startW = 30;  //最小体重
        var endW = 130;  //最大体重
        var string;
        for ( var i = startW; i<= endW; i++ ) {
            $('.weight_list').append("<li>" + i + "</li>");
            //console.log(i);
        }
        //选择体重
        var weight_list = $(".weight_list").mobiscroll().treelist({
            theme: "android-ics light",
            lang: "zh",
            display: 'bottom',
            defaultValue: [Math.floor($('.weight_list li').length/2)],
            cancelText: null,
            inputClass:'mob_input',
            cancelText: '清空',//清空按钮名称
            setText: '确定', //确认按钮名称
            headerText: function (valueText) { return "选择体重"; },
            onSelect: function (valueText) {
                var m = $(this).find("li").eq(valueText).html();
                //console.log(m);
                $('.weight .screening2_right').html(m);            }
        });

        $('.weight').click(function () {
            weight_list.mobiscroll('show');
        });


        //创建身高list
        var startH = 100;  //最小身高
        var endH = 220;  //最高身高
        for ( var i = startH; i<= endH; i++ ) {
            $('.height_list').append("<li>" + i + "</li>");
            //console.log(i);
        }
        //选择身高
        var height_list = $(".height_list").mobiscroll().treelist({
            theme: "android-ics light",
            lang: "zh",
            display: 'bottom',
            defaultValue: [Math.floor($('.height_list li').length/2)],
            cancelText: null,
            inputClass:'mob_input',
            cancelText: '清空',//清空按钮名称
            setText: '确定', //确认按钮名称
            headerText: function (valueText) { return "选择身高"; },
            onSelect: function (valueText) {
                var m = $(this).find("li").eq(valueText).html();
                //console.log(m);
                $('.height .screening2_right').html(m);            }
        });

        $('.height').click(function () {
            height_list.mobiscroll('show');
        });

    })
</script>
@stop
