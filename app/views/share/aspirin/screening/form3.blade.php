@extends('share.aspirin.common.layout')
@section('title')国卫健康云—筛查 @stop
@section('description')国卫健康云—筛查@stop
@section('keywords')国卫健康云—筛查 @stop
@section('content')
	<div class="page_top_box" style="display:none;">
    	<input type="button" class="btn_back" style="display:none;"/>
        筛查
        <input type="button" class="btn_search" style="display:none;"/>
    </div>
    <input type="hidden" id="iId" name="iId" value="{{$iId}}">
    <div class="page_cont_box">
        <div class="select_block">
            <div class="select_tit">
                您是否有已诊断的高血脂
            </div>
            <div class="select clearfix">
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio11" name="question4" value="2" onclick="secondInfo(2);">
                    <label class="sele_radio_lable" for="radio11">是</label>
                </div>
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio12" name="question4" value="1" onclick="secondInfo(1);">
                    <label class="sele_radio_lable" for="radio12">否</label>
                </div>
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio13" name="question4" value="3" onclick="secondInfo(3);">
                    <label class="sele_radio_lable" for="radio13">不清楚</label>
                </div>
                <div class="clear"></div>
            </div>
            <div class="select_block" id="second_box" style="display:none;">
                <div class="select_tit">
                    您是否在服用降脂药
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio14" name="question41" value="2">
                        <label class="sele_radio_lable" for="radio14">是</label>
                    </div>
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio15" name="question41" value="1">
                        <label class="sele_radio_lable" for="radio15">否</label>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="select_block border_top" id="third_box" style="display:none;">
                <div class="select_tit">
                    治疗前总胆固醇水平
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio16" name="question42" value="1">
                        <label class="sele_radio_lable" for="radio16"><4.14mmol/L(<160mg/dl)</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio17" name="question42" value="2">
                        <label class="sele_radio_lable" for="radio17">4.15-5.17mmol/L(<160-199mg/dl)</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio18" name="question42" value="3">
                        <label class="sele_radio_lable" for="radio18">5.18-6.21mmol/L(<200-239mg/dl)</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio19" name="question42" value="4">
                        <label class="sele_radio_lable" for="radio19">6.22-7.24mmol/L(<240-279mg/dl)</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio20" name="question42" value="5">
                        <label class="sele_radio_lable" for="radio20">≥7.25mmol/L(<≥280mg/dl)</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio21" name="question42" value="6">
                        <label class="sele_radio_lable" for="radio21">不清楚</label>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="select_block">
            <div class="select_tit">
                您是否有已诊断的高血压
            </div>
            <div class="select clearfix">
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio51" name="question5" value="2" onclick="thirdInfo(2);">
                    <label class="sele_radio_lable" for="radio51">是</label>
                </div>
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio52" name="question5" value="1" onclick="thirdInfo(1);">
                    <label class="sele_radio_lable" for="radio52">否</label>
                </div>
                <div class="left">
                    <input class="sele_radio" type="radio" id="radio53" name="question5" value="3" onclick="thirdInfo(3);">
                    <label class="sele_radio_lable" for="radio53">不清楚</label>
                </div>
                <div class="clear"></div>
            </div>
            <div class="select_block" id="second_box2" style="display:none;">
                <div class="select_tit">
                    您是否在服用降压药
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio54" name="question51" value="2">
                        <label class="sele_radio_lable" for="radio54">是</label>
                    </div>
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio55" name="question51" value="1">
                        <label class="sele_radio_lable" for="radio55">否</label>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="select_block border_top" id="third_box2" style="display:none;">
                <div class="select_txt">
                    <div class="select_left">
                        治疗前收缩压（高压）：
                    </div>
                    <div class="select_right">
                        <input class="input_txt" type="text" placeholder="请输入" name="question52" id="question52"/>
                    </div>
                </div>
                <div>
                    mmHg
                </div>
            </div>
            <div class="select_block border_top" id="fourth_box2" style="display:none;">
                <div class="select_tit">
                    治疗前舒张压（低压）
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio56" name="question53" value="1">
                        <label class="sele_radio_lable" for="radio56">小于80mmHg</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio57" name="question53" value="2">
                        <label class="sele_radio_lable" for="radio57">80-84mmHg</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio58" name="question53" value="3">
                        <label class="sele_radio_lable" for="radio58">85-89mmHg</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio59" name="question53" value="4">
                        <label class="sele_radio_lable" for="radio59">90-99mmHg</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio60" name="question53" value="5">
                        <label class="sele_radio_lable" for="radio60">大于等于100mmHg</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio61" name="question53" value="6">
                        <label class="sele_radio_lable" for="radio61">不清楚</label>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="select_block border_top" id="fifth_box2" style="display:none;">
                <div class="select_tit">
                    治疗后收缩压（高压）
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio70" name="question54" value="1">
                        <label class="sele_radio_lable" for="radio70">小于等于140mmHg</label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="select clearfix">
                    <div class="left">
                        <input class="sele_radio" type="radio" id="radio71" name="question54" value="2">
                        <label class="sele_radio_lable" for="radio71">大于等于140mmHg</label>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="page_btn_box">
        <input class="send_btn" type="button" value="继续" onclick="check();"/>
    </div>
    
    <script type="text/javascript">
	 function secondInfo(flag){
	     if(flag == 2){
	   	    $("#second_box").show();
	   		$("#third_box").show();
	     }else if(flag == 1){
	   	    $("#second_box").hide();
	   	 	$("#third_box").hide();
	     }else if(flag == 3){
	     	$("#second_box").show();
	    	$("#third_box").hide();
		 }
	 }
	 function thirdInfo(flag){
	     if(flag == 2){
	   	    $("#second_box2").show();
	   		$("#third_box2").show();
	   		$("#fourth_box2").show();
	   		$("#fifth_box2").show();
	     }else if(flag == 1){
	     	$("#second_box2").hide();
		   	$("#third_box2").hide();
		   	$("#fourth_box2").hide();
		   	$("#fifth_box2").hide();
	     }else if(flag == 3){
	    	$("#second_box2").show();
	    	$("#third_box2").hide();
		   	$("#fourth_box2").hide();
		   	$("#fifth_box2").hide();
		 }
	 }
	</script>
    <script type="text/javascript">
	function check(){
		var iId = $("#iId").val();
		var question4 = $(':radio[name="question4"]:checked').val();
		var question41 = $(':radio[name="question41"]:checked').val();
		var question42 = $(':radio[name="question42"]:checked').val();
		var question5 = $(':radio[name="question5"]:checked').val();
		var question51 = $(':radio[name="question51"]:checked').val();
		var question52 = $.trim($("#question52").val());
		var question53 = $(':radio[name="question53"]:checked').val();
		var question54 = $(':radio[name="question54"]:checked').val();
		if(!question4){
			alert("请选择是否有已诊断的高血脂");
			return false;
		}
		if(question4 != 1 && !question41){
			alert("请选择是否在用降脂药");
			return false;
		}
		if(question4 == 2 && !question42){
			alert("请选择治疗前总胆固醇水平");
			return false;
		}
		if(!question5){
			alert("请选择是否有已诊断的高血压");
			return false;
		}
		if(question5 != 1 && !question51){
			alert("请选择是否在用降压药");
			return false;
		}
		if(question5 == 2 && !question52){
			alert("请填写治疗前收缩压（高压）");
			return false;
		}
		if(question5 == 2 && !question53){
			alert("请选择治疗前舒张压（低压）");
			return false;
		}
		if(question5 == 2 && !question54){
			alert("请选择治疗后收缩压（高压）");
			return false;
		}
		var url = '/aspirinshare/screening-submit3';
		var data = {'iId':iId,
					'question4':question4,
					'question41':question41,
					'question42':question42,
					'question5':question5,
					'question51':question51,
					'question52':question52,
					'question53':question53,
					'question54':question54
		};
		$.post(url,data,function(msg){
			if(msg.success){
				window.location.href= '/aspirinshare/screening-form4?id='+iId;
			}else{
				alert('请重新提交');
				return;
			}
		  },'json')
	}
	</script> 
@stop
