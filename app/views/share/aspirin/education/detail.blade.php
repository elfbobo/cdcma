@extends('share.aspirin.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
    <div class="page_top_box">健康教育</div>
    <div class="page_cont_box">
        <div>
            <a href="javascript:void(0)">
                <video id="video-show" class="video-js vjs-default-skin vjs-big-play-centered" src="{{$oEducation->video_url}}" poster="{{$oEducation->ppt_thumb}}" controls="controls" width="100%" oncontextmenu="return false;">
                    您的浏览器不支持 video 标签。
                </video>
            </a>
        </div>
        <div class=" infor_box">
            <div class="bg">
                <div class="video2_box">
                    <div class="video2_line">
                        <div class="video2_tit">
                            {{$oEducation->ppt_title}}
                        </div>
                        <div class="video2_icon">
                            @if($iHasSupport)
                                <a class="white_heart red_heart" id="zan_button"><span id="zan_count">{{$oEducation->support_count}}</span></a>
                            @else
                                <a class="white_heart" id="zan_button" onclick="support({{$oEducation->id}})"><span id="zan_count">{{$oEducation->support_count}}</span></a>
                            @endif
                        </div>
                    </div>
                    <div class="video2_line">
                        <!--<div class="video2_txt">
                            浏览量：{{$oEducation->share_hits}}
                        </div>-->
                        <div class="video2_txt txt_right">
                            {{date('Y.m.d',strtotime($oEducation->created_at))}}
                        </div>
                    </div>
                </div>
                <div class="infor_cont">
                    <div class="intro_box clearfix">
                        @if(!empty($oEducation->education_thumb))
                            <img class="intro_pic" src="{{$oEducation->education_thumb}}" style="width:66px;height:66px;"/>
                        @else
                            <img class="intro_pic" src="{{$oUser->user_thumb}}" style="width:66px;height:66px;"/>
                        @endif
                        <div class="intro_name">
                            @if(isset($oUser->user_name))
                                <p>专家：{{$oUser->user_name}}</p>
                            @endif
                            @if(isset($oUser->user_position))
                                <p>职称：{{$oUser->user_position}}</p>
                            @endif
                            @if(isset($oUser->user_department))
                                <p>科室：{{$oUser->user_department}}</p>
                            @endif
                            @if(!empty($sUserCompany))
                                <p>医院：{{$sUserCompany}}</p>
                            @endif
                        </div>
                        <div class="clear"></div>
                        <div>
                            {{$oEducation->education_content}}
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <link href="/assets/js/videojs/video-js.min.css" rel="stylesheet">
    <script src="/assets/js/videojs/video.min.js"></script>
    <script type="text/javascript">
        // 播放索引
        var i = 0;
        // 播放列表
        var list = [{
            src: '/upload/ad/secend-adv.mp4',
            type: 'video/mp4'
        },{
            src: '{{$oEducation->video_url}}',
            type: 'video/mp4'
        }];
        $(function(){
            var player = videojs("video-show", {
                "width": document.body.clientWidth + "px",
                "height": (document.body.clientWidth * 0.75) + "px",
                "poster": "{{$oEducation->ppt_thumb}}",
                "autoplay": true,
                "controls": true,
                "sources": [{
                    src: list[i].src,
                    type: list[i].type
                }],
            }, function(){
                this.on('loadeddata',function(){

                }),
                this.on('ended',function(){
                    i++;
                    if (i < list.length) {
                        this.src({type: list[i].type, src: list[i].src});
                        this.play();
                    }
                });
            })
        });
    </script>
    <?php
    //微信扫一扫参与签到调研
    $isWxFlag = user_agent_is_weixin();
    ?>
    <script type="text/javascript">
        setInterval(function(){
            $.post('/aspirinshare/show-time-delay', {id:'{{$oVideoLog->id}}', time: 60}).done(function(data){
                console.log('show-time-delay ok');
            });
        }, 60 * 1000);
    </script>
    <script>
        function support(id) {
            $('#zan_button').addClass("red_heart");
            $('#zan_count').html((parseInt($('#zan_count').html()) + 1));
            $('#zan_button').attr('onclick', '');
            $.post('/aspirinshare/education-support/' + id, {}, function (msg) {

            })
        }
    </script>
@stop
