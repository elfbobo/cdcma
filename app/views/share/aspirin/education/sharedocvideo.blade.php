<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>国卫健康云</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" type="text/css" href="/assets/css/aspirin.css" />
<script src="http://libs.baidu.com/jquery/1.7.2/jquery.js"></script>
<script type="text/javascript" src="http://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="http://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
<script type="text/javascript" src="https://cdn.bootcss.com/html2canvas/0.5.0-beta4/html2canvas.min.js"></script>
<style>
<!--
.titleBoxs { padding:30px 20px 20px;display:table;color:#14C599; }
.titleBoxs h1 { font-size:24px;font-weight:600;width:100%;float:left;text-align:left;line-height:140%; }
.titleBoxs li { font-size:14px;padding:8px 0;display:inline-table;line-height:180%; }
.footerBoxs {
    position:fixed;bottom:0;left:0;right:0;height:30px;
    background:url('/public/assets/images/mobile/share_bottom_bg.jpg') no-repeat bottom right;
}
.contentBoxs { padding:0px 20px; }
.contentBoxs ul { width:48%;float:left;padding-right:2%; }
.contentBoxs li.photoBoxs img { width:133px;padding-top:8px; }
.contentBoxs li.infoBoxs { font-size:14px;line-height:180%;color:#14C599; }
.contentBoxs li.infoBoxs div { position:relative; }
.contentBoxs li.infoBoxs div p { text-align:left;display:inline-block;padding-left:50px; }
.contentBoxs li.infoBoxs div p.title {
    color:#000;font-weight:bold;
    width:50px !important;text-align:center !important;position:absolute;left:0px !important;padding:0px !important;
}
.contentBoxs li.qrcodeBoxs {
    width:148px;height:160px;background:url("/public/assets/images/mobile/share_qrcode_bg.png?t=<?php echo time(); ?>") no-repeat center;
    background-size: 133px 160px;position:relative;margin-bottom:15px;
}
.contentBoxs li.qrcodeBoxs img { width:94px;height:94px;position:absolute;top:48px;left:28px;border-radius:12px; }
.contentBoxs li.txtBoxs { font-size:14px;line-height:180%;text-align:center;color:#14C599; }
-->
</style>
</head>
<body style="background:#FFF;">
    <input type="hidden" id="education_id" value="{{$videoInfo->id}}">
    <div id="capture">
        <div style="width:100%;float:left"><img src="/public/assets/images/mobile/share_banner.jpg" width="100%"></div>
        <div class="titleBoxs">
            <ul>
                <!-- <h1>{{$videoInfo->ppt_title}}</h1> -->
                <h1><p>慢病管理，听听医生怎么说</p><p style="text-align:right;">在线讲座，随时随地看</p></h1>
                <li><p>主办方：国卫健康大数据（中关村）研究所</p><p>承办方：拜耳医药保健有限公司</p></li>
            </ul>
        </div>
        <div style="clear:both;"></div>
        <div class="contentBoxs">
            <ul class="doctorBoxs">
                <li class="photoBoxs"><img src="{{$domain}}{{$doctorInfo->user_thumb}}"></li>
                <li class="infoBoxs">
                    <div><p class="title">姓名：</p><p>{{$doctorInfo->user_name}}</p></div>
                    <div><p class="title">省市：</p><p>{{$p_c_str}}</p></div>
                    <div><p class="title">医院：</p><p>{{$doctorInfo->company?$doctorInfo->company:"--"}}</p></div>
                    <div><p class="title">科室：</p><p>{{$doctorInfo->user_department}}</p></div>
                    <div><p class="title">职称：</p><p>{{$doctorInfo->user_position}}</p></div>
                </li>
            </ul>
            <ul class="doctorBoxs">
                <li class="qrcodeBoxs"><img src="{{$sEwmUrl}}"></li>
                <li class="txtBoxs"><p>扫码观看专家视频</p><p>健康管理从自身做起</p></li>
            </ul>
        </div>
        <div style="height:40px;width:100%;display:table;"></div>
        <div class="footerBoxs" style="position:unset;"></div>
    </div>
<script  type="text/javascript" >
$(document).ready( function(){
    html2canvas(document.body, {
        allowTaint: true,
        taintTest: false,
        onrendered: function(canvas) {
            canvas.id = "capture";
            //生成base64图片数据
            var dataUrl = canvas.toDataURL();
            var newImg = document.createElement("img");
            newImg.src =  dataUrl;
            //html保存成图片，存储到数据库
            var id = $('#education_id').val();
            var url = '/aspirinshare/education-save-pic';
            var data = {'id':id,'picurl':dataUrl};
            $.post(url,data,function(msg){
            });
        }
    });  
}); 
</script>
</body>
</html>