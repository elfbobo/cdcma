<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>国卫健康云</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" type="text/css" href="/assets/css/aspirin.css" />
<script src="http://libs.baidu.com/jquery/1.7.2/jquery.js"></script>
<script type="text/javascript" src="http://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://html2canvas.hertzen.com/build/html2canvas.js"></script>
        <script  type="text/javascript" >
        $(document).ready( function(){
//                 $(".example1").on("click", function(event) {
//                         event.preventDefault();
                        html2canvas(document.body, {
                        allowTaint: true,
                        taintTest: false,
                        onrendered: function(canvas) {
                            canvas.id = "mycanvas";
                            //document.body.appendChild(canvas);
                            //生成base64图片数据
                            var dataUrl = canvas.toDataURL();
                            var newImg = document.createElement("img");
                            newImg.src =  dataUrl;
//                             document.body.appendChild(newImg);

							//html保存成图片，存储到数据库
							var id = $('#education_id').val();
                            var url = '/aspirinshare/education-save-pic';
                        	var data = {'id':id,'picurl':dataUrl};
                        	$.post(url,data,function(msg){
                        	});
                        }
                    });
//                 }); 
             
        });
         
        </script>
</head>
<body>
	<div class="page_top_box" style="display:none;">
        健康教育
    </div>
    <input type="hidden" id="education_id" value="{{$oEducation->id}}">
    <div class="page_cont_box">
        <div class=" infor_box">
            <div class="bg">
            	<div class="video2_box">
                    <div class="video2_line">
                        <div class="video2_tit">
                            {{$oEducation->ppt_title}}
                        </div>
                        <div class="video2_icon">
                            <a class="white_heart"><span>{{$oEducation->support_count}}</span></a>
                        </div>
                    </div>
                    <div class="video2_line">
                        <div class="video2_txt">
                            浏览量：{{$oEducation->education_hits}}
                        </div>
                        <div class="video2_txt txt_right">
                            {{date('Y.m.d',strtotime($oEducation->created_at))}}
                        </div>
                    </div>
                </div>
                <div class="infor_cont">
                    <div class="intro_box clearfix">
                    @if(!empty($oEducation->education_thumb))
					<img class="intro_pic" src="{{$oEducation->education_thumb}}" style="width:66px;height:66px;"/>
					@else
					<img class="intro_pic" src="{{$oUser->user_thumb}}" style="width:66px;height:66px;"/>
					@endif
                        <div class="intro_name">
	                        @if(isset($oUser->user_name))
							<p>专家：{{$oUser->user_name}}</p>
							@endif
							@if(isset($oUser->user_position))
							<p>职称：{{$oUser->user_position}}</p>
							@endif
							@if(isset($oUser->user_department))
							<p>科室：{{$oUser->user_department}}</p>
							@endif
							@if(!empty($sUserCompany))
							<p>医院：{{$sUserCompany}}</p>
							@endif
                        </div>
                        <div class="clear"></div>
                        <div>
                            {{$oEducation->education_content}}
                        </div>
                    </div>
                </div>
                <div class="download_box">
		        	<div class="download_txt">
		            	扫描二维码，观看详细健康教育视频
		            </div>
		            <div class="erweima_block">
		            	<img class="wid100" src="{{$sEwmUrl}}" style="width:200px;height:200px;"/>
		            </div>
		        </div>
            </div>
        </div>
    </div>
</body>
</html>