@extends('share.aspirin.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
	<div class="page_top_box">
        国卫健康云
        <input type="button" class="btn_share" style="display:none;" />
    </div>
    <div class="page_cont_box">
        
        <div class="download_box">
        	<div class="download_txt">
            	扫描二维码，观看详细健康教育视频
            </div>
            <div class="erweima_block">
            	<img class="wid100" src="{{$sEwmUrl}}" />
            </div>
        </div>
    </div>
    <div class="page_btm_pic">
    	<img class="wid100" src="res/images/btm.jpg" />
    </div>
    
 
@stop
