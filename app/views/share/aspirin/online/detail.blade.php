@extends('share.aspirin.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
	<div class="page_top_box" style="display:none;">
    	<input type="button" class="btn_back" />
        精彩回顾
        <input type="button" class="btn_share" />
    </div>
    <div class="page_cont_box">
    	<div class="title_box">
        	<p class="center">{{$aVideo['online_title']}}</p>
        	@if(!empty($aVideo['doc_hospital']) && !empty($aVideo['doc_name']))
            <p class="center font12">{{$aVideo['doc_hospital']}} &nbsp;&nbsp;&nbsp;&nbsp;{{$aVideo['doc_name']}}</p>
            @endif
        </div>
        <div class="video_box">
        	<a href="javascript:void(0)">
            	<video id="media" src="{{$aVideo['video_url']}}" poster="{{$aVideo['online_banner']}}" controls="controls" width="100%">
				您的浏览器不支持 video 标签。
				</video>
            </a>
        </div>
        
        <div class="video_infor_box">
        	<div class="video_infor_block">
            	浏览量：{{$aVideo['video_hits']}}
            </div>
            <div class="video_infor_block">
            	<a class="white_heart" onclick="changeIcon();">{{$aVideo['video_zan']}}</a>
            </div>
        </div>
        <div class="grade_box">
        	<div class="grade_line">
                <div class="grade_left">
                    内容丰富程度
                </div>
                <div class="grade_right">
                    <div class="star"><span></span></div>
                    <div class="star"><span></span></div>
                    <div class="star"><span></span></div>
                    <div class="star"><span></span></div>
                    <div class="star"><span></span></div>
                </div>
            </div>
            <div class="grade_line">
                <div class="grade_left">
                    专家讲课水平
                </div>
                <div class="grade_right">
                    <div class="star"><span></span></div>
                    <div class="star"><span></span></div>
                    <div class="star"><span></span></div>
                    <div class="star"><span></span></div>
                    <div class="star"><span></span></div>
                </div>
            </div>
            <div class="grade_line">
                <div class="grade_left">
                    整体综合评分
                </div>
                <div class="grade_right">
                    <div class="star"><span></span></div>
                    <div class="star"><span></span></div>
                    <div class="star"><span></span></div>
                    <div class="star"><span></span></div>
                    <div class="star"><span></span></div>
                </div>
            </div>
        </div>
        <div class="comment_box">
            <div class="comment_tit">
                评论
            </div>
            <div class="comment_cont">
            	@foreach($aVideo['comments'] as $v)
                <div class="comment_block">
                    <div class="comment_line">
                        <div class="comment_pic">
                            <img src="{{$v['user_thumb']}}" />
                        </div>
                        <div class="comment_name">
                            <div>{{$v['user_nick']}}</div>
                            <div>{{$v['created_at']}}</div>
                        </div>
                    </div>
                    <div class="comment_txt">
                        {{$v['comment']}}
                    </div>
                    <div class="comment_infor">
                        <div class="huifu">
                            回复
                        </div>
                        <div class="zan">
                            点赞（<span>{{$v['zan_count']}}</span>）
                        </div>
                    </div>
                </div>
               @endforeach 
            </div>
        </div>
    </div>
    <div class="send_comm_box">
        <div>
            <input type="text" class="send_txt" placeholder="写评论.." />
        </div>
    </div>

    <div id="cover" style="display:none;">
	    <div class="cover"></div>
	    <div class="pop_box">
	    	<div class="pop_top">
	        	<p>如需继续观看</p>
	            <p>请下载国卫健康云</p>
	        </div>
	        <div class="pop_btm">
	        	<input class="pop_btn" type="button" value="点击下载" onclick="window.location.href='/app'" />
	        </div>
	    </div>
    </div>
    <div id="attend_cover" style="display:none;">
	    <div class="cover"></div>
	    <div class="pop_box">
	    	<div class="pop_top">
	            <p>请下载国卫健康云</p>
	        	<p>参与评论点赞</p>
	        </div>
	        <div class="pop_btm">
	        	<input class="pop_btn" type="button" value="点击下载" onclick="window.location.href='/app'" />
	        </div>
	    </div>
    </div>
    <script>
	    function changeIcon() {
	    	$('#attend_cover').show();
	    }
		$(function () {
			$('.star').click(function(){
				$('#attend_cover').show();
            });
			$('.zan').click(function(){
				$('#attend_cover').show();
            });
			$('.huifu').click(function(){
				$('#attend_cover').show();
            });
            $('.send_txt').bind('focus',function(){
            	$('.send_txt').attr('disabled',true);
            	$('#attend_cover').show();
            });
        });
	</script>
    <script>
    Media = document.getElementById("media"); 
    window.setInterval(function(){
		var currentt = Media.currentTime;
		if(currentt>60){
			window.location.href='/aspirinshare/online-show-copy/{{$iVideoId}}';
		}
    },1000)
    </script>
@stop
