@extends('share.aspirin.common.layout')
@section('title')国卫健康云 @stop
@section('content')
<?php
$agent = strtolower ( $_SERVER ['HTTP_USER_AGENT'] );
$is_iphone = ((strpos ($agent, 'iphone' )) || (strpos ($agent, 'iph' )))? true : false;
$is_ipad = (strpos ($agent, 'ipad' )) ? true : false;
$is_android = ((strpos ($agent, 'android' )) || (strpos($agent, 'adr' )))? true : false;
?>
@if(user_agent_is_weixin())
<div class="cover" style=" display:block;"></div>
    <div class="prompt_box" style=" display:block;">
    	<div class="prompt_pic">
        	<img src="/assets/images/aspirin/arrow.png" />
        </div>
        <div class="prompt_txt">
        	@if($is_iphone || $is_ipad)
        	<p>戳右上角，选择“<span>在Safari中打开</span>”</p>
        	@else
        	<p>戳右上角，选择“<span>在浏览器中打开</span>”</p>
        	@endif
            <p>即可完成下载，等你哦！</p>
        </div>
    </div>
@endif
	<div class="page_top_box">
        国卫健康云
        <input type="button" class="btn_share" style="display:none;" />
    </div>
    <div class="page_cont_box">
        
        <div class="download_box">
        	<div class="invite_code">邀请码：{{$sCode}}</div>
            <div class="download_btn">
            @if($is_iphone || $is_ipad)
            <!--<A href="itms-services://?action=download-manifest&url=https://jnj.kydev.net:8443/plist/mbglxy_cdcma.plist">-->
            <A href="http://www.pgyer.com/Vw0U">
            	<input class="download_btn" type="button" value="下载国卫健康云APP" />
            </A>
            @else
            <!--<A display="cursor:pointer;" onclick="window.location.href='http://cdcma.bizconf.cn/upload/cdcma.apk';">-->
            <A href="http://www.pgyer.com/ZHNe">
            	<input class="download_btn" type="button" value="下载国卫健康云APP" />
            </A>
            @endif
            </div>
        </div>
        
    </div>
    
@stop
