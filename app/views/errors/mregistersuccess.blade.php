@extends('front.common.layoutm')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content3')
<div class="cover"></div>
<div class="pop_box">
	<div class="pop_txt">
	    <p>恭喜您已注册成功</p>
        <p class="red">您的邀请码为{{$code}}</p>
        <p>此邀请码可邀请其它医生注册本学院观看直播及进行学术交流</p>
    </div>
    <div class="btn_box">
    	<input type="button" class="btn5" value="确定" onclick="window.location.href='/docface/survey/{{Session::get('survey_video_id')}}'"/>
    </div>
</div>
@stop