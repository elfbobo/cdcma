<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>国卫健康云-提示信息</title>
<link rel="stylesheet" type="text/css" href="/assets/css/css.css"/>
</head>
<body>
	<div class="pop_logoin_box center">
	<div class="pop_cont2">
    	<p>恭喜您已注册成功</p>
        <p class="color">您的邀请码为{{$code}}</p>
        <p>此邀请码可邀请其它医生注册本学院观看直播及进行学术交流</p>
    </div>
    <div class="center">
    	<input class="save" type="button" value="确定" onclick="window.location.href='/'"/>
    </div>
</div>
</body>
</html>