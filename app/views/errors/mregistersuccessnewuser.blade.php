@extends('mobile.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content')
<div class="bg">
    <div class="top">
        <img class="wid100" src="/assets/images/weixin/top2.jpg" />
    </div>
    <div class="invite_code_box">
        <div>
            恭喜您已注册成功
        </div>
        <div class="red">
            您的邀请码为{{$code}}
        </div>
        <div>
            此邀请码可邀请其他医生注册本学院
        </div>
        <div>
            观看直播及进行学术交流
        </div>
        <div>
            长按下方二维码<span class="blue">关注订阅号</span>
        </div>
        <div>
            方便查看更多直播视频及学术交流
        </div>
        <div class="erweima">
            <img src="/assets/images/weixin/erweima.jpg" />
        </div>
    </div>
    <div class="page_btm_box">
        <div>
            主办单位：北京医卫健康公益基金会
        </div>
<!--         <div> -->
<!--             赞助单位：拜耳医药保健有限公司 -->
<!--         </div> -->
        <div>
            网站所有权归北京医卫健康公益基金会所有
        </div>
        <div class="red">
            如遇APP使用问题，请拨打咨询电话<?php echo HOTLINE;?>（工作时间09:00-18:00）
        </div>
    </div>
</div>
@stop