@extends('front.common.layout')
@section('title')专家大会诊-国卫健康云 @stop
@section('description')专家大会诊-国卫健康云@stop
@section('keywords')专家大会诊-国卫健康云 @stop
@section('content2')
<div class="p_sml_menu">
    	您当前位置：
    	<a href="">首页</a>
        >
        <a href="">专家大会诊</a>
        > 
        精彩回顾
    </div>
</div>
<div class="wid969">
	@foreach($oConsultation as $k=>$v)
    <div class="block_box left white_box auto_h">
        <div class="sml_tit"><a class="tit" href="/consultation/show/{{$v->id}}">{{$v->meeting_title}}</a></div>
        <div class="block_img">
            <a href="/consultation/show/{{$v->id}}"><img src="{{$v->meeting_thumb}}" /></a>
        </div>
        <div class="block_cont auto_h">
           {{$v->meeting_description}}            
        </div>            
    </div>
    @endforeach
	<div class="clear"></div>	
    
    <div class="p_number_box">{{$oConsultation->links()}}</div>
    
    
</div>


@stop
