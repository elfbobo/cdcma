@extends('front.common.layout')
@section('title')专家大会诊-国卫健康云 @stop
@section('description')专家大会诊-国卫健康云@stop
@section('keywords')专家大会诊-国卫健康云 @stop
@section('content2')

@section('content1')
<script type="text/javascript">
                	function block_text(text){
                		$('#pop_apply_success_box_html').html(text);
	                    $.blockUI({ message: $('#pop_apply_success_box')});
                    }
				    function submit_apply(){
				    	$.ajax({
				            cache: true,
				            type: "POST",
				            url:'/consultation/apply',
				            data:$('#apply_form').serialize(),// 你的formid
				            async: false,
				            error: function(request) {
				            	block_text("Connection error");
				            },
				            success: function(data) {
				                if(data=='repeat'){
//				                    alert('您已报名，无需重复报名，谢谢！');
				                   block_text('您已报名，无需重复报名，谢谢！');
				                }else if(data=='noanswer'){
//				                    alert('请至少选择2个话题提出您的观点阐述，谢谢！');
				                    block_text('请至少选择2个话题提出您的观点阐述，谢谢！');
				                }else if(data=='full'){
				                	block_text('报名人数已满，下次再来吧');
					            }else{
					                if(data=='success'){
//										alert('成功');
					                	$('#apply_num').html(parseInt($('#apply_num').html())+1);
					                	block_text('感谢您参与报名，我们会尽快通知您是否入选参与本期会议讨论');
					                }else{
//										alert('报名失败，请刷新页面后重新报名，谢谢！');
										block_text('报名失败，请刷新页面后重新报名，谢谢！');
					                }
				                }
				            }
				        });
				    }
				</script>
<div class="p_sml_menu">
    	您当前位置：
    	<a href="/">首页</a>
        >
        专家大会诊
    </div>
    @if($Consultation1)
    <div class="p_box">
    	<div class="p_box_top"></div>
        <div class="p_box_cont wid_auto">
        	<div class="countdown_left m0">
            	@if($Consultation1->meeting_starttime>date('Y-m-d H:i:s'))
                <a href="#"><img class="countdown_img" src="/assets/images/front/web/img04.jpg" /></a>
                <div class="countdown">
            	<?php 
					$time = strtotime($Consultation1->meeting_starttime)-time();
					$day = floor($time/(24*60*60));
					$day = str_pad($day,2,'0',STR_PAD_LEFT);
					
					$hour = floor(($time-$day*(24*60*60))/(60*60));
					$hour = str_pad($hour,2,'0',STR_PAD_LEFT);
					
					$minute = floor(($time-$day*(24*60*60)-$hour*(60*60))/(60));
					$minute = str_pad($minute,2,'0',STR_PAD_LEFT);
					
					$second = floor(($time-$day*(24*60*60)-$hour*(60*60)-$minute*60));
					$second = str_pad($second,2,'0',STR_PAD_LEFT);
				?>
					<div id="day">
	                <div class="countdown_num day1 num_{{$day[0]}}"></div>
	                <div class="countdown_num day2 num_{{$day[1]}}"></div>
	                </div>
	                <div id="hour">
	                <div class="countdown_num hour1 num_{{$hour[0]}}"></div>
	                <div class="countdown_num hour2 num_{{$hour[1]}}"></div>
	                </div>
	                <div id="minute">
	                <div class="countdown_num minute1 num_{{$minute[0]}}"></div>
	                <div class="countdown_num minute2 num_{{$minute[1]}}"></div>
	                </div>
	                <div id="second">
	                <div class="countdown_num second1 num_{{$second[0]}}"></div>
	                <div class="countdown_num second2 num_{{$second[1]}}"></div>
	                </div>
	                <input type="hidden" id="time" value="{{$time}}">
	                <script>
						var int=self.setInterval("clock()", 1000)
						function clock(){
							var time = $("#time").val();
							if(time<0){
								return false;
								//window.location.reload();
							}
							$("#time").val(time-1);
							var nDay=Math.floor(time/(60*60) / 24); //Math.floor(nMS/(60*60) / 24) % 31
							var nH=Math.floor(time/(60*60)) % 24;//折合小时 var nH=Math.floor(nMS/(60*60) / 24)*24+Math.floor(nMS/(60*60)) % 24;
							var nM=Math.floor(time/(60)) % 60;//分
							var nS=Math.floor(time) % 60;
							$('#second').find('div:eq(0)').attr('class','countdown_num second1 num_'+Math.floor(nS/10)).end().find('div:eq(1)').attr('class','countdown_num second2 num_'+Math.floor(nS%10));
							$('#minute').find('div:eq(0)').attr('class','countdown_num minute1 num_'+Math.floor(nM/10)).end().find('div:eq(1)').attr('class','countdown_num minute2 num_'+Math.floor(nM%10));
							$('#hour').find('div:eq(0)').attr('class','countdown_num hour1 num_'+Math.floor(nH/10)).end().find('div:eq(1)').attr('class','countdown_num hour2 num_'+Math.floor(nH%10));
							$('#day').find('div:eq(0)').attr('class','countdown_num day1 num_'+Math.floor(nDay/10)).end().find('div:eq(1)').attr('class','countdown_num day2 num_'+Math.floor(nDay%10));
						}
					</script>
					
	            </div>
	            @elseif(($Consultation1->meeting_starttime<date('Y-m-d H:i:s'))&&($Consultation1->meeting_endtime>date('Y-m-d H:i:s')))
	           <a href=""><img class="countdown_img" src="/assets/images/front/web/img04.jpg" /></a>
                <div class="countdown">
                    <div class="countdown_num day1 num_0"></div>
                    <div class="countdown_num day2 num_0"></div>
                    <div class="countdown_num hour1 num_0"></div>
                    <div class="countdown_num hour2 num_0"></div>
                    <div class="countdown_num minute1 num_0"></div>
                    <div class="countdown_num minute2 num_0"></div>
                    <div class="countdown_num second1 num_0"></div>
                    <div class="countdown_num second2 num_0"></div>
                </div>
	            @else
	             <a href="#"><img src="/assets/images/front/web/img04.jpg"></a>
                <div class="countdown expect">
            	</div>  
	            @endif
       		</div>
            <div class="meeting_box">
            	<div><span class="bold">会议主席：</span>{{$Consultation1->doc_name}}</div>
                <div><span class="bold">会议时间：</span>{{substr($Consultation1->meeting_starttime,0,16)}} - {{substr($Consultation1->meeting_endtime,11,5)}}</div>
                <div><span class="bold">本期主题：</span>{{$Consultation1->meeting_title}}</div>
                <div class="bold">本期话题：</div>
                @foreach($oTopic as $k=>$v)
                <div>
                	<div class="left" style="width:20px">{{$k+1}}.</div>
                    <div class="right" style="width:290px"><a>{{$v->topic_title}}</a></div>
                    <div class="clear"></div>                
                </div>
                @endforeach
                <div class="meetimg_btn_box">
                	@if(($Consultation1->meeting_starttime<date('Y-m-d H:i:s',(time()+600)))&&($Consultation1->meeting_endtime>date('Y-m-d H:i:s')))
                	<input class="meetimg_btn bg_green" type="button" value="观看直播" onclick="window.location.href='/consultation/enter/{{$Consultation1->id}}/1'"/>
                    <input class="meetimg_btn" type="button" value="参与会议" onclick="enter_meeting({{$Consultation1->id}},'{{$Consultation1->meeting_code}}')"/>
                    @else
                    <input class="meetimg_btn bg_green" type="button" value="观看直播" onclick="block_text('直播尚未开始或已结束')"/>
                    <input class="meetimg_btn" type="button" value="参与会议" onclick="block_text('直播尚未开始或已结束')"/>
                    @endif
                </div>
            </div>
        	<div class="clear"></div>
            <div class="apply_box">
            	<div class="apply_tit">跨领域学术交流报名</div>
                <div class="apply_cont">
                	<div class="left wid290">
                    	<p><span class="bold">尊敬的医生您好：欢迎您参加跨领域学术交流。</span></p>
                        <p><span class="bold">报名条件：</span>积分排行榜前100名。</p>
                        <p><span class="bold">报名时间：</span>{{$Consultation1->reg_starttime}}</p>
                        <p><span class="bold">截止时间：</span>{{$Consultation1->reg_endtime}}</p>
                    </div>
                    <div class="right">
                    	@if(Auth::User()->role_id<=2)
                    	<a class="apply" style="cursor:pointer;" onclick="block_text('仅限医生参与报名')">
                    	<span id="apply_num">{{$Consultation1->reg_num}}</span>/{{$Consultation1->reg_setmax_nums}}<br />立即报名</a>
                    	@else
                    	@if(strtotime($Consultation1->reg_starttime)<time()&&strtotime($Consultation1->reg_endtime)>time())
                    	<a class="apply" style="cursor:pointer;" @if($Consultation1->reg_num>=$Consultation1->reg_setmax_nums) onclick="block_text('报名人数已满，下次再来吧');"  @else @if($iRegFlag==2) onclick="block_text('您已报名，无需重复报名，谢谢！')" @elseif($iRegFlag==0) onclick="block_text('积分排行榜前100名才能报名哦！')" @elseif($iRegFlag==1) onclick="$('#view_point_box_div').removeClass('viewpoint_none');$.blockUI({ message: $('#pop_apply_success_box_right')});" @endif @endif>
                    	<span id="apply_num">{{$Consultation1->reg_num}}</span>/{{$Consultation1->reg_setmax_nums}}<br />立即报名</a>
                    	@else
                    	<a class="apply" style="cursor:pointer;" onclick="block_text('请在规定报名时间内报名')">
                    	<span id="apply_num">{{$Consultation1->reg_num}}</span>/{{$Consultation1->reg_setmax_nums}}<br />立即报名</a>
                    	@endif
                    	@endif
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="viewpoint_box viewpoint_none" id="view_point_box_div">
            	<div class="green">感谢您报名参与此次跨领域学术交流</div>
                <div class="green">请您对本期话题任选2-3个提出你的观点进行阐述（必填）</div>
                
                <form method="post" id="apply_form">
                <div class="left">
					<input type="hidden" name="meeting_id" id="meeting_id" value="{{$Consultation1->id}}"/>
                	@foreach($oTopic as $k=>$v)
                    <div>
                        <img src="/assets/images/front/web/viewpoint_icon.jpg" />话题{{$k+1}}
                        <input class="viewpoint" type="text" name="topic[{{$v->id}}]" />
                    </div>
                    @endforeach
                </div>
                <div class="right">
                	 <input class="meetimg_btn m0" type="button" value="确定" onclick="submit_apply()"/>
                </div>   
                </form> 
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            
        </div>
        <div class="p_box_btm"></div>
    </div>
</div>
@endif
@stop
@section('content2')
<div class="p_bar_bg">
	<div class="wid1025">
    	<div class="p_tit">精彩预告</div>
    </div>
</div>
<div class="wid1025">	
    <div class="p_box">
    	<div class="p_box_top" style="margin-top:-10px;"></div>
        <div class="p_box_cont wid_auto">               	
            <div class="apply_box wid875 mbtm15">
            	<div class="apply_tit color_green">科室与专家大会诊</div>
            	@if($Consultation2)
                <div class="apply_cont">
                	<div class="left wid130">
                    	<a href=""><img class="doc_pic" src="{{$Consultation2->doc_thumb}}" /></a>
                    </div>
                	<div class="left wid580">
                    	<p><span class="bold">时间：</span>{{substr($Consultation2->meeting_starttime,0,16)}} - {{substr($Consultation2->meeting_endtime,11,5)}}</p>
                        <p><span class="bold">专家：</span>{{$Consultation2->doc_name}}</p>
                        <p><span class="bold">主题：</span><a class="color8" href="#">{{$Consultation2->meeting_title}}</a></p>
                        <p class="bold">课程简介：</p>
                        <p>{{$Consultation2->meeting_description}}</p>
                    </div>
                    <div class="right">
                        @if(($Consultation2->meeting_starttime<date('Y-m-d H:i:s',(time()+600)))&&($Consultation2->meeting_endtime>date('Y-m-d H:i:s')))
	                	<div><input class="meetimg_btn bg_green" type="button" value="观看直播" onclick="window.location.href='/consultation/enter/{{$Consultation2->id}}/1'"/></div>
	                    <div class="ptop45"><input class="meetimg_btn" type="button" value="参与会议" onclick="enter_meeting({{$Consultation2->id}},'{{$Consultation2->meeting_code}}')"/></div>
	                    @else
	                    <div><input class="meetimg_btn bg_green" type="button" value="观看直播" onclick="block_text('直播尚未开始或已结束')"/></div>
	                    <div class="ptop45"><input class="meetimg_btn" type="button" value="参与会议" onclick="block_text('直播尚未开始或已结束')"/></div>
	                    @endif
                    </div>
                    <div class="clear"></div>
                </div>
                @endif
            </div> 
            <div class="apply_box wid875 mbtm15">
            	<div class="apply_tit color_green">疑难病例讨论</div>
            	@if($Consultation3)
                <div class="apply_cont">
                	<div class="left wid710">
                    	<p><span class="bold">时间：</span>{{substr($Consultation3->meeting_starttime,0,16)}} - {{substr($Consultation3->meeting_endtime,11,5)}}</p>
                        <p><span class="bold">主题：</span><a class="color8" href="#">{{$Consultation3->meeting_title}}</a></p>
                        <p class="bold">课程简介：</p>
                        <p>{{$Consultation3->meeting_description}}</p>
                    </div>
                    <div class="right">
                    	@if(($Consultation3->meeting_starttime<date('Y-m-d H:i:s',(time()+600)))&&($Consultation3->meeting_endtime>date('Y-m-d H:i:s')))
	                	<div><input class="meetimg_btn bg_green" type="button" value="观看直播" onclick="window.location.href='/consultation/enter/{{$Consultation3->id}}/1'"/></div>
	                    <div class="ptop45"><input class="meetimg_btn" type="button" value="参与会议" onclick="enter_meeting({{$Consultation3->id}},'{{$Consultation3->meeting_code}}')"/></div>
	                    @else
	                    <div><input class="meetimg_btn bg_green" type="button" value="观看直播" onclick="block_text('直播尚未开始或已结束')"/></div>
	                    <div class="ptop45"><input class="meetimg_btn" type="button" value="参与会议" onclick="block_text('直播尚未开始或已结束')"/></div>
	                    @endif
                    </div>
                    <div class="clear"></div>
                </div>
                @endif
            </div> 
            
            <div class="apply_box wid875 ">
            	<div class="apply_tit color_green">专家连线</div>
            	@if($Consultation5)
                <div class="apply_cont">
                	<div class="left wid130">
                    	<a href=""><img class="doc_pic" src="{{$Consultation5->doc_thumb}}" /></a>
                    </div>
                	<div class="left wid580">
                    	<p><span class="bold">时间：</span>{{substr($Consultation5->meeting_starttime,0,16)}} - {{substr($Consultation5->meeting_endtime,11,5)}}</p>
                        <p><span class="bold">专家：</span>{{$Consultation5->doc_name}}</p>
                        <p><span class="bold">主题：</span><a class="color8" href="#">{{$Consultation5->meeting_title}}</a></p>
                        <p class="bold">课程简介：</p>
                        <p>{{$Consultation5->meeting_description}}</p>
                    </div>
                    <div class="right">
                        @if(($Consultation5->meeting_starttime<date('Y-m-d H:i:s',(time()+600)))&&($Consultation5->meeting_endtime>date('Y-m-d H:i:s')))
	                	<div><input class="meetimg_btn bg_green" type="button" value="观看直播" onclick="window.location.href='/consultation/enter/{{$Consultation5->id}}/1'"/></div>
	                    <div class="ptop45"><input class="meetimg_btn" type="button" value="参与会议" onclick="enter_meeting({{$Consultation5->id}},'{{$Consultation5->meeting_code}}')"/></div>
	                    @else
	                    <div><input class="meetimg_btn bg_green" type="button" value="观看直播" onclick="block_text('直播尚未开始或已结束')"/></div>
	                    <div class="ptop45"><input class="meetimg_btn" type="button" value="参与会议" onclick="block_text('直播尚未开始或已结束')"/></div>
	                    @endif
                    </div>
                    <div class="clear"></div>
                </div>
                @endif
            </div> 
        </div>
        <div class="p_box_btm"></div>
    </div>
</div>
<div class="p_bar_bg bg_color1">
	<div class="wid1025">
    	<div class="p_tit">精彩回顾</div>
    </div>
</div>
<div class="wid1025">
	<div class="block_bg">
    	<div class="sml_menu_box">
           	<a class="a_more" href="/consultation/list">more</a>
            <div class="clear"></div>
        </div>
        <div class="block_line">
        	@foreach($Consultation4 as $k=>$v)
        	<div class="block_box1 left">
            	<div class="center">
            		<a href="/consultation/show/{{$v->id}}" class="sml_tit red mx_h">{{$v->meeting_title}}</a>
                </div>
                <div class="block_img">
            		<a href="/consultation/show/{{$v->id}}"><img src="{{$v->meeting_thumb}}" /></a>
            	</div>
                <div class="block_cont height72">
                	<p>专家：{{$v->doc_name}}</p>
                    <p class="theme2">医院： {{$v->doc_hospital}}</p>
                </div>
            </div>
            @endforeach
            <div class="clear"></div>
        </div>
    </div>
</div>



<div class="p_bar_bg bg_color2">
	<div class="wid1025">
    	<div class="p_tit">直播流程使用说明</div>
    </div>
</div>
<div class="wid1025 pbtm40">
	<div class="p_box">
    	<div class="p_box_top" style="margin-top:-10px;"></div>
        <div class="p_box_cont wid_auto">               	
            <div class="explain_box">
            	<p class="explain_txt" style="color:#888888">请初次参加会议互动用户点击如下链接下载插件并安装使用.</p>
                <p class="explain_txt"><a class="explain_link" style="color:#2b91d0" href="http://meeting.confcloud.cn/support/download">http://meeting.confcloud.cn/support/download</a></p>
                <p class="explain_txt" style="color:#888888">报名参加会议请点击“参与会议”按钮进入后输入会议密码及用户名即可参加此次会议互动。
</p><p class="explain_txt" style="color:#888888">观看会议讨论的人员可点击“观看直播”按钮进入，进入后输入用户名即可进入观看现场直播。
</p>
            </div>
        </div>
        <div class="p_box_btm"></div>
    </div>
</div>
<script>
function enter_meeting(id,code){
	$.blockUI({ message: $('#invited_code_box')});
	$('#invited_code_sub_btn').attr('onclick','invited_code_sub('+id+',"'+code+'")');
}

function invited_code_sub(id,code){
	var enter_code = $('#invited_code').val();
	if(enter_code.toUpperCase()==code.toUpperCase()){
		window.location.href='/consultation/enter/'+id+'/2'
	}else{
		$.unblockUI()
		alert('邀请码输入有误，请重新输入');
		return;
	}
		
}

</script>

				<div class="pop_logoin_box center" style="top:0;display:none;" id="pop_apply_success_box">
                    <div class="pop_cont2 pop_ptop60">
                        <p id="pop_apply_success_box_html">您已报名成功，感谢您的参与。</p>
                    </div>
                    <div class="center">
                        <input class="save" type="button" value="确定" onclick="$.unblockUI()"/>
                    </div>
                </div>
                
                <div class="pop_logoin_box center" id="invited_code_box" style="display:none;">
					<div class="pop_cont2">
				    	<p>请您输入参与会议的邀请码：</p>
				       
				        <p class="pop_ptop"><input class="pop_txt" type="text" id="invited_code"/></p>
				    </div>
				    <div class="center">
				    	<input class="save" id="invited_code_sub_btn" type="button" value="确定"/>
				        <input class="save mleft75" type="button" value="取消" onclick="$.unblockUI();"/>
				    </div>
				</div>
				
				<div class="pop_logoin_box center" style="top:0;display:none;" id="pop_apply_success_box_right">
                    <div class="pop_cont2 pop_ptop60">
                        <p>请在右侧选择2-3道话题进行简单阐述</p>
                    </div>
                    <div class="center">
                        <input class="save" type="button" value="知道了" onclick="$.unblockUI()"/>
                    </div>
                </div>
                
<div class="cover" id="cover_1" @if(empty($caseList['data'])) style="display:none;" @endif></div>
<div class="pop_vote_box" id="cover_2" @if(empty($caseList['data'])) style="display:none;" @endif>
	<div class="vote_top"></div>
    <div class="vote_body">
    	<div class="tx_l">
        	<img src="/assets/images/front/web/logo.jpg" />
        </div>
    	<h4>病例投票</h4>
        <div class="close_btn">
        	<input type="button" class="btn" onclick="cover(1);"/>
        </div>
        <div class="case_vote clearfix">
        	@foreach($caseList['data'] as $key => $v)
        	<div class="vote_one @if($key%2 == 1) mar_r0 @endif @if($key > 3) pb19 @endif">
            	<p class="mar_b20"><span>病例名称：</span>{{$v['case_name']}}</p>
                <p><span>病例亮点：</span>{{str_cut_cms($v['case_lightspot'],80)}}<a href="/case/show/{{$v['id']}}">【详情】</a></p>
                <p class="center">
                	<input type="button" class="p_btn" value="+投票" onclick="vote({{$v['id']}});"/>
                </p>
            </div>
        	@endforeach
        </div>
    </div>
    <div class="vote_btm"></div>
    <div class="pop_logoin_box center" id="cover_3" style="display:none;">
        <div class="pop_cont2 pop_ptop60">
            <p id="massage">恭喜您已投票成功，感谢您的参与</p>
        </div>
        <div class="center">
            <input class="save" type="button" value="确定" onclick="cover(2);"/>
        </div>
	</div>
</div>
<script>
function cover(type){
	if(type == 1){
		$('#cover_1').hide();
		$('#cover_2').hide();
	}else{
		$('#cover_3').hide();
	}
}
function vote(caseId){
	var url ="/case/vote";
	var data = {
		caseId:caseId
	}
	$.post(url,data,function(msg){
		if(msg == 1){
			$('#massage').html('恭喜您已投票成功，感谢您的参与');
			$("#cover_3").show();
		}else if(msg == 0){
			$('#massage').html('您已投票');
			$("#cover_3").show();
		}else {
			$('#massage').html('参数错误，投票失败');
			$("#cover_3").show();
		}
	});
}

</script>
@stop
