@extends('front.common.layout')
@section('title')专家大会诊-国卫健康云 @stop
@section('description')专家大会诊-国卫健康云@stop
@section('keywords')专家大会诊-国卫健康云 @stop
@section('content1')
<script src="/assets/js/video.js"></script>
	<div class="p_sml_menu">
    	您当前位置：
    	<a href="/">首页</a>
        >
        <a href="/consultation">专家大会诊</a>
        >
        <a href="/consultation/list">精彩回顾</a>
        >详细
    </div>
    <div class="p_left_box left">
    	<div class="detailed_box">
        	<div class="bold">{{$oReview->meeting_title}}</div>
            <div class="detailed_time">时间：{{$oReview->meeting_time}}</div>
            <div class="detailed_video" id="a1">
    		<br><br><br><br><br>
    			<span style="text-align:center;color:#717071;text-align:center;">浏览器版本过低，请您升级浏览器后观看</span>
            <br><br><br><br><br>	
            </div>
            <div class="detailed_good">
            	<a href="javascript:void(0)" class="good" onclick="docface_video_zan({{$oReview->id}},{{Auth::User()->id}},2)"></a>
                <a href="javascript:void(0)" class="good_txt" id="docface_video_zan">{{$oReview->meeting_support}}</a>
            </div>
        </div>
        </div>    
	    <div class="p_right_box">    	
	        <div class="txt_bg01">
	        	<div class="txt_bg01_top"></div>
	            <div class="txt_bg01_cont" style=" min-height: 360px;">
	                <div class="tit">本期简介</div>
	                <div>{{$oReview->meeting_description}}</div>
	        	</div>
	            <div class="txt_bg01_btm"></div>
	        </div>
	    </div>
	    <div class="clear"></div>
	</div>
       <div class="p_bar_bg bg_color2">
			<div class="wid1025">
		    	<div class="p_tit">医友评论</div>
		    </div>
		</div>
        <div class="wid1025 pbtm40">
		    <div class="p_box">
		        <div class="p_box_top" style="margin-top:-10px;"></div>
		        <div class="p_box_cont wid_auto p75">
        	@foreach($oComments as $oComment)
        	<div class="comment_line">
                <div class="left wid80">
                    <a href="#"><img class="pic" src="{{$oComment->user_thumb}}" /></a>
                </div>
                <div class="right wid405 wid790">
                    <div class="name_line"><a href="#" class="name">{{$oComment->user_nick}}</a></div>
                    <div class="">
                        <div class="left wid630">{{$oComment->comment}}</div>
                        <div class="right wid125">
                        	<a class="hand" href="javascript:void(0)" onclick="comment_zan({{$oComment->id}},2)">（<span id="comment_zan_{{$oComment->id}}">{{$oComment->zan_count}}
                        	</span>）</a>
                            <a class="reply" href="javascript:void(0)"  onclick="video_reply({{$oComment->user_id}},'{{$oComment->user_nick}}')">回复</a>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            @endforeach
            
            <div class="people_num">已有<span>{{count($oComments)}}</span>位医友发表了看法</div>
            <div class="comment_cont">
            	<div id='reply' value="" style='color:#666;position: relative;left:4px;top:1px;width:865px;height:22px;display:none;'>
				</div>
				<div name="content" id="content" contentEditable="true" style="outline:none;background-color: #FFFFFF; color: #888888;font: 14px/24px '微软雅黑';border: 1px solid #e0e0e0;height: 96px;overflow-y: auto;padding: 5px;resize: none;width: 865px;">
				</div>
            </div>
            <div class="comment_btn_box" style="position: relative;">
            	<a class="face" href="javascript:void(0);" id="showpic">插入表情</a>
            	<div  class="" style="background:#fff;position:absolute;left:8px;top:50px;display:none;height:89px;border:1px solid #D1D1D1;margin-left:14px;padding:5px 10px 5px 10px;" id="pic_list">{{$sImgLi}}
	                 <a href='javascript:void(0);' onclick='hidePic(0)' class='icon close1'></a>
	            </div>
                <input id="replay_box" class="comment_btn" type="button" value="发表评论" onclick="reply({{$oReview->id}},2);"/>
                <div class="clear"></div>
            </div>
        </div>
        <div class="p_box_btm"></div>
    </div>
   
 <script type="text/javascript" src="/assets/js/player/ckplayer/ckplayer.js" charset="utf-8"></script>
<script type="text/javascript">
	var __flg; 
	var flashvars={
		f:'{{Config::get("config.cdn_url").str_replace(".mp4",".flv",$oReview->meeting_url)}}',
		c:0,
		//lv:1,
		//b:1,
		h:1,
		loaded:'loadedHandler',
		};
	var video=['{{Config::get("config.cdn_url").str_replace(".mp4",".flv",$oReview->meeting_url)}}','{{Config::get("config.cdn_url").str_replace(".mp4",".flv",$oReview->meeting_url)}}','{{Config::get("config.cdn_url").str_replace(".mp4",".flv",$oReview->meeting_url)}}'];
	CKobject.embed('/assets/js/player/ckplayer/ckplayer.swf','a1','ckplayer_a1','548','329',false,flashvars,video);


</script>   
    <script>
	document.onclick=function(event){
		var e = event || window.event;
		var elem = e.srcElement||e.target;
		while(elem){
			if(elem.id == 'showpic' || elem.id == 'pic_list'){
				$("#pic_list").show();
				return;
			}else{
				$("#pic_list").hide();
				//return;
			}
			elem = elem.parentNode;
		}
	}
</script>
@stop
