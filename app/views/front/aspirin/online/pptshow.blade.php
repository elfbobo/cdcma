@extends('front.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<script src="/assets/js/jquery-1.8.3.min.js"></script>
<script src="/assets/js/jquery.SuperSlide.2.1.1.js"></script>
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/aspirin">专项基金</a>
    >
   在线会议
</div>
<div class="p_center_box fund_page">
	<div class="left">
    	<ul>
            <li><a href="/aspirin/index"><span style="background-position:left 4px center;" class="fund_icon1">专项基金</span></a></li>
            <li><a href="/aspirin/info-list"><span class="fund_icon2">阿司匹林</span></a></li>
            <li><a href="/aspirinonline/ppt/1" class="sele"><span class="fund_icon3_3">在线会议</span></a></li>
            <li><a href="/aspirinresearch/brief"><span class="fund_icon4">科研培训</span></a></li>
        </ul>
    </div>
    <div class="right fund_intro science online_meeting">
    	<div class="top_online_meeting">
        </div>
        <div class="risk_tool_tab">
            <div onclick="window.location.href='/aspirinonline/ppt/1';" style="cursor:pointer;">讲者报名</div>
            <div onclick="window.location.href='/aspirinonline/listener-list/1';" style="cursor:pointer;">报名参会</div>
            <div onclick="window.location.href='/aspirinonline/review-list/0';" style="cursor:pointer;">精彩回顾</div>
            <div onclick="window.location.href='/aspirinonline/ppt-list/1';" style="cursor:pointer;" class="sele">查看课件</div>
        </div>
        <div class="picScroll-left">
        	@if($oPptImage)
            <div class="hd">
                <a class="next"></a>
                <ul></ul>
                <a class="prev"></a>
            </div>
            <div class="bd">
                <ul class="picList">
                	@foreach($oPptImage as $k=>$v)
                    <li>
<!--                     	<div class="pic"><a href="#"><img src="/assets/images/front/web/ppt.png"/></a></div> -->
                        <div class="pic"><a href="#"><img src="{{$v->image_url}}"/></a></div>
                    </li>
                    @endforeach
                </ul>
            </div>
        	@endif
        </div>
        <div class="download_ppt">
            <button class="btn" onclick="window.location.href='{{$oPpt->ppt_url}}';">下载 PPT</button>
        </div>
    </div>
	<div class="clear"></div>
</div>
<script type="text/javascript">
    jQuery(".picScroll-left").slide({titCell:".hd ul",mainCell:".bd ul",autoPage:true,effect:"left",autoPlay:false,vis:1,trigger:"click"});
</script>
@include('front.common.contact')
@stop