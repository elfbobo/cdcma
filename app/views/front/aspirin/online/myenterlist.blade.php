@extends('front.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<!-- 1. Define some markup -->
<!-- 
<button class="btn" data-clipboard-text="Just because you can doesn't mean you should — clipboard.js">
	Copy to clipboard
</button>
-->
<!-- 2. Include library -->
<script src="/assets/js/dist/clipboard.min.js"></script>

<!-- 3. Instantiate clipboard -->
<script>
    var clipboard = new Clipboard('#btn');

    clipboard.on('success', function(e) {
        console.log(e);
    });

    clipboard.on('error', function(e) {
        console.log(e);
    });
</script>
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/user">个人中心</a>
    >
    我的会议
</div>

<div class="p_center_box fund_page">
    @include('front.common.ucleft',['nowpos'=>'myenterlist'])
	<!-- <div class="left">
            <ul>
                <li><a href="/user"><span class="personal_icon1">个人信息</span></a></li>
                <li><a href="/user/my-score"><span class="personal_icon2">我的积分</span></a></li>
                @if(Auth::User()->role_id==3)
                <li><a href="/aspirin/my-auth"><span class="personal_icon3">我的认证</span></a></li>
                @endif
                <li><a href="/aspirinonline/my-enter-list"  class="sele"><span class="personal_icon4_4">我的会议</span></a></li>
                <li><a href="/aspirinoffline"><span class="personal_icon5">线下会议</span></a></li>
                <li><a href=""><span class="personal_icon6">视频下载</span></a></li>
                <li><a href="/aspirinresearch/my-list"><span class="personal_icon7">科研培训</span></a></li>
                <li><a href="/aspirin/contact"><span class="personal_icon8">联系客服</span></a></li>
                @if(Auth::User()->role_id==2)
                <li><a href="/user/my-train"><span class="personal_icon9">我的培训</span></a></li>
                @endif
                @if(Auth::User()->role_id==2)
                <li><a href="/user/my-order"><span class="icon04">我的预约</span></a></li>
                @endif
                <li><a href="/user/caselist/1"><span class="icon06">我的病例</span></a></li>
                @if(Auth::User()->role_id==2)
                <li><a href="/user/mydoc"><span class="icon09">医生管理</span></a></li>
                @endif
            </ul>
        </div> -->
    <div class="right fund_intro science online_meeting">
    	<div class="kinds_tab clearfix">
            <div onclick="window.location.href='/aspirinonline/my-enter-list';" style="cursor:pointer;" class="sele">主讲会议</div>
            <div onclick="window.location.href='/aspirinonline/my-attend-list';" style="cursor:pointer;">参加会议</div>
            <div class="bottom_line"></div>
        </div>
        @if(isset($oInfo)&&$oInfo)
        <input type="hidden" id="catid" value="{{$oOnlineInfo->catid}}"/>
		<input type="hidden" id="timeid" value="{{$oInfo->id}}"/>
        <div class="online_meeting_box success-page">
            <div class="item-list">
                <div class="item-flex">
                    <div class="item-time">{{date('Y年m月d日',strtotime($oInfo->online_date))}} {{$oInfo->time_period}}
                    	<span style="margin-left: 20px">会议主题：{{sub_str($oPpt->ppt_title,12)}}</span>
                    </div>
                    <div class="clearfix">
                    	@if($iFlag == 0)
                        <div class="item-count" onclick="$('#shade').show();$('#pre_notice_box').show();" style="cursor:pointer;">取消会议</div>
                        <div class="item-count ban" onclick="alert('会议未开始！');" style="cursor:pointer;">启动会议</div>
                    	<div class="item-count" onclick="$('#shade').show();$('#invite_box').show();" style="cursor:pointer;">查看/邀请</div>
                    	@elseif($iFlag == 2)
                    	<div class="item-count ban">会议结束</div>
                    	<div class="item-count ban">启动会议</div>
                    	<div class="item-count ban">查看/邀请</div>
                    	@else
                    	<div class="item-count ban">取消会议</div>
                    	<div class="item-count" onclick="$('#shade').show();$('#meeting_notice_box').show();" style="cursor:pointer;">启动会议</div>
                        <div class="item-count ban">查看/邀请</div>
                   		@endif
                	</div>
            	</div>
        	</div>
        </div>
        @endif
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@if(isset($oInfo)&&$oInfo)
<div class="cover" id="shade" style="display:none;"></div>
<div class="modal_window style_one" id="pre_notice_box" style="display:none;">
    <div class="cont_txt" style="font-weight: bold;padding-bottom: 10px">
        <p>您确认取消会议？</p>
    </div>
    <div class="btn_box">
        <button class="btn" onclick="$('#shade').hide();$('#pre_notice_box').hide();">再考虑一下</button>
        <button class="btn" onclick="submit_cancel({{$oInfo->id}});">确定</button>
    </div>
</div>
<div class="modal_window style_one" id="submit_notice_box" style="display:none;">
    <div class="title_close">
        <span class="title"></span>
        <span class="close" style="padding: 10px 10px 0 0">
        	<img src="/assets/images/front/web/close_btn.jpg" alt="" onclick="window.location.reload();">
        </span>
    </div>
    <div class="cont_txt" style="padding: 20px 42px 50px ; font-size: 16px">
        <p>您已成功取消会议</p>
    </div>
</div>
<div class="modal_window style_one" id="meeting_notice_box" style="display:none;">
    <div class="cont_txt" style="padding: 20px 42px 50px ; font-size: 16px">
        <p>温馨提示：</p>
        <p>会议时间不少于30分钟。</p>
        <p>幻灯分享链接:</p>
        <p>{{$sPptHtml}}进入会议后打开共享网页直接粘贴即可分享幻灯。</p>
    </div>
    <div class="btn_box">
        <button class="btn" onclick="$('#shade').hide();$('#meeting_notice_box').hide();">再考虑一下</button>
        <button class="btn" id="btn" data-clipboard-text="{{$sPptHtml}}" onclick="submit_open({{$oInfo->id}});">确定</button>
    </div>
</div>
<div class="modal_window invitation" id="invite_box" style="display:none;">
    <div class="content">
        <div class="title_close">
            <span class="title">邀请医生：</span>
            <span class="close">
            	<img src="/assets/images/front/web/pop_close.png" onclick="$('#shade').hide();$('#invite_box').hide();" style="cursor:pointer;">
            </span>
        </div>
        <div class="clearfix">
            <div class="input_box">
                <label><span class="red">*</span>省份：</label>
                <select id="hospital1" class="input_select" onchange="change_hospital(2);">
                    <option value="0">=请选择=</option>
                    @foreach($aHosp as $k=>$v)
                    <option value="{{$k}}">{{$v}}</option>
                    @endforeach
                </select>
            </div>
            <div class="input_box">
                <label><span class="red">*</span>城市：</label>
                <select id="hospital2" class="input_select" onchange="change_hospital(3);get_hospital();">
                    <option value="0">=请选择=</option>
                </select>
            </div>
            <div class="input_box">
                <label>区/县：</label>
                <select id="hospital3" class="input_select" onchange="change_hospital(4);">
                    <option value="0">=请选择=</option>
                </select>
            </div>
            <div class="input_box">
                <label><span class="red">*</span>医院：</label>
                <select id="hospital4" class="input_select" style="width:200px;" onchange="change_hospital(5);get_doctor();">
                    <option value="0">=请选择=</option>
                </select>
            </div>
            <div class="input_box last">
                <label>医生：</label>
                <div class="flex-block clearfix"  id="doctor_box">
<!--                     <div> -->
<!--                         <input type="checkbox" class="input_checkbox"> -->
<!--                         <span>孙宁玲</span> -->
<!--                     </div> -->
                </div>
            </div>
        </div>
        <div class="item">
            <div class="item-header">您邀请的医生：</div>
            @foreach($aInviteList as $k=>$v)
            <div class="item-list" id="invite_{{$v['invite_id']}}">
                <div class="item-flex">
                    <div class="item-title">{{sub_str($v['doc_hospital'],8)}}</div>
                    <div class="item-sub">{{$v['doc_name']}} </div>
            		@if($v['join_type'] == 1)
                    <div class="item-count" onclick="submit_doctor_del({{$v['invite_id']}});" style="cursor:pointer;">删除</div>
            		@endif
                </div>
            </div>
            @endforeach
        </div>
        <div class="btn_box">
            <input type="button" class="btn" value="确定" onclick="submit_doctor();"/>
        </div>
    </div>
</div>
<input type="hidden" id="user_province" name="user_province" value="0"/>
<input type="hidden" id="user_city" name="user_city" value="0"/> 
<input type="hidden" id="user_county" name="user_county" value="0"/>
<input type="hidden" id="user_company" name="user_company" value="0"/>
@endif
<script type="text/javascript">
	function submit_cancel(timeid){
		if(timeid == 0){
			alert('会议参数错误');return;
		}
		var url = '/aspirinonline/speaker-cancel';
		var data = {timeid:timeid};
		$.post(url,data,function(msg){
		 	if(msg == 'success'){
		 		$('#pre_notice_box').hide();
				$('#submit_notice_box').show();
			}else{
				alert('请刷新重试!');
				return;
			}
		})
	}
	function submit_open(timeid){
		if(timeid == 0){
			alert('会议参数错误');return;
		}
		var url = '/aspirinonline/speaker-open';
		var data = {timeid:timeid};
		$.post(url,data,function(msg){
			if(msg.success){
				window.location.href = msg.url;
			}else{
				alert(msg.notice);
				$('#shade').hide();
				$('#meeting_notice_box').hide();
			}
		},'json');
	}
	function change_hospital(id){
        var pid = id-1;
    	var hospital = $("#hospital"+pid+"  option:selected").val();
    	console.log(hospital);
    	var html_null = '<option value="0">=请选择=</option>';
    	if(pid==1){
			$('#user_province').val(hospital);
			$("#hospital"+2).html(html_null);
			$("#hospital"+3).html(html_null);
			$("#hospital"+4).html(html_null);
        }else if(pid==2){
        	$('#user_city').val(hospital);
        	$("#hospital"+3).html(html_null);
			$("#hospital"+4).html(html_null);
			
        }else if(pid==3){
        	$('#user_county').val(hospital);
        	$("#hospital"+4).html(html_null);
        }else {
        	$('#user_company').val(hospital);
            return ;
        }
    	var url = '/user/hospital-child/'+hospital;
    	$.post(url,{},function(json){
    		data = json;
        	if(data=='noinfo'){
				var html = '<option value="0">=请选择=</option>';
				$("#hospital"+id).html(html);
			}else{
				var bShow = false;
				var html = '<option value="0">=请选择=</option>';
				for(var elem in data){
					bShow = true;
					html += '<option value="' + elem + '">' + data[elem] + '</option>';
				}
				if(bShow){
					$("#hospital"+id).html(html);
				}
			}	
			},'json');
    }
    function get_hospital(){
    	//不选择区县一项时，根据城市id获取医院
        var cityid = $('#user_city').val();
        if(cityid == 0||!cityid){
            alert('请选择城市');return;
        }
        var url = '/aspirinonline/hospital';
    	$.post(url,{cityid:cityid},function(json){
    		data = json;
        	if(data=='noinfo'){
        		var html = '<option value="0">=请选择=</option>';
				$("#hospital4").html(html);
    		}else{
    			var bShow = false;
				var html = '<option value="0">=请选择=</option>';
				for(var elem in data){
					bShow = true;
					html += '<option value="' + elem + '">' + data[elem] + '</option>';
				}
				if(bShow){
					$("#hospital4").html(html);
				}
    		}
    	},'json');
    }
    function get_doctor(){
    	var timeid = $('#timeid').val();
		var iHospitalId = $('#user_company').val();
		if(!timeid ||timeid == 0 ||!iHospitalId || iHospitalId == 0){
			alert('请选择医院');return;
		}
		var url = "/aspirinonline/invite-doctor";
		var data = {timeid:timeid,iHospitalId:iHospitalId};
		$.post(url,data,function(msg){
			if(msg.success){
				var aShow = false;
				var doc = '';
				for(var elem in msg.aDoctorInfo){
					var aShow = true;
					doc += '<div>';
					doc += '<input type="checkbox" class="input_checkbox" id="selected_doc_' + msg.aDoctorInfo[elem]['id'] + '" name="selected_doc" value="' + msg.aDoctorInfo[elem]['id'] + '">';
					doc += '<span>' + msg.aDoctorInfo[elem]['user_name'] + '</span>';
					doc += '</div>';
				}
				$('#doctor_box').append(doc);
			}else{
				alert('邀请失败');
				$('#shade').hide();
				$('#invite_box').hide();
			}
		},'json');
    }
  //获取多选框值
	function getCheckbox(name,s){
		if(!s){
			s = '|';
		}
		var obj = $("input[name="+name+"]:checked");
		var str = '';
		obj.each(function(){
			if(str){
				str += s ;
			}
			str += $(this).val();
		});
		return str;
	}
	function submit_doctor(){
		var doctorids = getCheckbox('selected_doc');
		if(!doctorids){
			alert("请选择要邀请的医生");
			return false;
		}
		var timeid = $('#timeid').val();
		if(!timeid ||timeid == 0){
			alert('参数错误');return;
		}
		var url = '/aspirinonline/invite-submit';
		var data = {timeid:timeid,doctorids:doctorids};
		$.post(url,data,function(msg){
            console.log(msg);
			if(msg.success){
				alert('邀请成功');
				window.location.reload();;
			}else{
				alert(msg.notice);
			}
		},'json');
	}
	function submit_doctor_del(inviteid){
		if(inviteid == 0){
			alert('参数错误');return;
		}
		var url = '/aspirinonline/invite-delete';
		var data = {inviteid:inviteid};
		$.post(url,data,function(msg){
		 	if(msg == 'success'){
		 		$('#invite_'+inviteid).remove();
			}else{
				alert('请刷新重试!');
				return;
			}
		})
	}
</script>
@stop