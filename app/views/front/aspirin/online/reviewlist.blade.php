@extends('front.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/aspirin">专项基金</a>
    >
    在线会议
</div>
<div class="p_center_box fund_page">
	<div class="left">
    	<ul>
            <li><a href="/aspirin/index"><span style="background-position:left 4px center;" class="fund_icon1">专项基金</span></a></li>
            <li><a href="/aspirin/info-list"><span class="fund_icon2">阿司匹林</span></a></li>
            <li><a href="/aspirinonline/ppt/1" class="sele"><span class="fund_icon3_3">在线会议</span></a></li>
            <li><a href="/aspirinresearch/brief"><span class="fund_icon4">科研培训</span></a></li>
        </ul>
    </div>
    <div class="right fund_intro science online_meeting">
    	<div class="top_online_meeting">
        </div>
        <div class="risk_tool_tab">
        	<div onclick="window.location.href='/aspirinonline/ppt/1';" style="cursor:pointer;">讲者报名</div>
            <div onclick="window.location.href='/aspirinonline/listener-list/1';" style="cursor:pointer;">报名参会</div>
            <div onclick="window.location.href='/aspirinonline/review-list/0';" style="cursor:pointer;" class="sele">精彩回顾</div>
            <div onclick="window.location.href='/aspirinonline/ppt-list/1';" style="cursor:pointer;" class="none">查看课件</div>
        </div>
        <div class="kinds_tab2 clearfix">
            <a href="/aspirinonline/review-list/0" @if($iCatId == 0) class="btn cur" @else class="btn" @endif>全部（{{$iCountAll}}）</a>
            <a href="/aspirinonline/review-list/1" @if($iCatId == 1) class="btn cur" @else class="btn" @endif>心内科（{{$iCount1}}）</a>
            <a href="/aspirinonline/review-list/2" @if($iCatId == 2) class="btn cur" @else class="btn" @endif>神内科（{{$iCount2}}）</a>
        </div>
        <div class="highlights-page clearfix">
        	@foreach($oVideoList as $k=>$v)
            <div @if($k%3 == 2) class="block_box left white_box last" @else class="block_box left white_box" @endif>
                <div class="sml_tit">
                	<a class="tit" href="/aspirinonline/review-show/{{$v->id}}">{{sub_str($v->ppt_title,14)}}</a>
                </div>
                <div class="block_img">
                    <a href="/aspirinonline/review-show/{{$v->id}}"><img src="{{$v->ppt_thumb}}"/></a>
                </div>
                <div class="block_cont">
                    <p>专家：{{$v->doc_name}}</p>
                    <p class="theme2">医院： {{sub_str($v->doc_hospital,12)}}</p>
                </div>
            </div>
	        @endforeach
	        <div class="p_number_box">
	        {{$oVideoList->links()}}
		    </div>
        </div>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop