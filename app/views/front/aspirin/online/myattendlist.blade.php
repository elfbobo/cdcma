@extends('front.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/user">个人中心</a>
    >
    我的会议
</div>
<div class="p_center_box fund_page">
    @include('front.common.ucleft',['nowpos'=>'myenterlist'])
	<!-- <div class="left">
            <ul>
                <li><a href="/user"><span style="background-position:left 4px center;" class="personal_icon1">个人信息</span></a></li>
                <li><a href="/user/my-score"><span class="personal_icon2">我的积分</span></a></li>
                @if(Auth::User()->role_id==3)
                <li><a href="/aspirin/my-auth"><span class="personal_icon3">我的认证</span></a></li>
                @endif
                <li><a href="/aspirinonline/my-enter-list" class="sele"><span class="personal_icon4_4">我的会议</span></a></li>
                <li><a href="/aspirinoffline"><span class="personal_icon5">线下会议</span></a></li>
                <li><a href=""><span class="personal_icon6">视频下载</span></a></li>
                <li><a href="/aspirinresearch/my-list"><span class="personal_icon7">科研培训</span></a></li>
                <li><a href="/aspirin/contact"><span class="personal_icon8">联系客服</span></a></li>
                @if(Auth::User()->role_id==2)
                <li><a href="/user/my-train"><span class="personal_icon9">我的培训</span></a></li>
                @endif
                @if(Auth::User()->role_id==2)
                <li><a href="/user/my-order"><span class="icon04">我的预约</span></a></li>
                @endif
                <li><a href="/user/caselist/1"><span class="icon06">我的病例</span></a></li>
                @if(Auth::User()->role_id==2)
                <li><a href="/user/mydoc"><span class="icon09">医生管理</span></a></li>
                @endif
            </ul>
        </div> -->
    <div class="right fund_intro science online_meeting">
        <div class="kinds_tab clearfix">
            <div onclick="window.location.href='/aspirinonline/my-enter-list';" style="cursor:pointer;">主讲会议</div>
            <div onclick="window.location.href='/aspirinonline/my-attend-list';" style="cursor:pointer;" class="sele">参加会议</div>
            <div class="bottom_line"></div>
        </div>
        <div class="online_meeting_box my-meeting-page">
        	@foreach($aAttendInfo as $k=>$v)
            <div class="item-list">
                <div class="item-flex">
                    <div class="item-head">
                        <p>{{$v['online_date']}} {{$v['time_period']}}</p>
                        <p>会议主题：{{$v['online_title']}}</p>
                        <p class="item-speaker">讲者：{{$v['doc_name']}}   {{$v['doc_hospital']}}</p>
                    </div>
                    <div class="clearfix">
                        <div class="item-count" onclick="submit_attend({{$v['speaker_id']}},{{$v['online_time_id']}})" style="cursor:pointer;">参加会议</div>
                    </div>
                </div>
            </div>
            @endforeach
            @foreach($aInviteInfo as $k=>$v)
            <div class="item-list">
                <div class="item-flex">
                    <div class="item-head">
                        <p>{{$v['online_date']}} {{$v['time_period']}}</p>
                        <p>会议主题：{{$v['online_title']}}</p>
                        <p class="item-speaker">讲者：{{$v['doc_name']}}   {{$v['doc_hospital']}}</p>
                        @if($v['ignore_flag'] == 1)
                        <p class="item-tip">您已忽略本次会议</p>
                        @else
                        <p class="item-tip">邀请您参加此次会议</p>
                        @endif
                    </div>
                    <div class="clearfix">
                    	@if($v['ignore_flag'] == 1)
                    	<div class="item-count ban">忽略</div>
                        <div class="item-count ban">同意</div>
                        @else
                    	<div class="item-count" onclick="submit_ignore({{$v['speaker_id']}},{{$v['online_time_id']}});" style="cursor:pointer;">忽略</div>
                        <div class="item-count color-red" onclick="submit_agree({{$v['speaker_id']}},{{$v['online_time_id']}});" style="cursor:pointer;">同意</div>
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
<script type="text/javascript">
function submit_attend(speakerid,timeid){
	if(!speakerid || !timeid){
		alert('参数错误');return;
	}
	var url = '/aspirinonline/listener-attend';
	var data = {speakerid:speakerid,timeid:timeid};
	$.post(url,data,function(msg){
		if(msg.success){
			window.location.href = msg.url;
		}else{
			alert(msg.notice);return;
		}
	},'json')
}
function submit_agree(speakerid,timeid){
	if(!speakerid || !timeid){
		alert('参数错误');return;
	}
	var url = '/aspirinonline/agree-attend';
	var data = {speakerid:speakerid,timeid:timeid};
	$.post(url,data,function(msg){
	 	if(msg == 'success'){
	 		window.location.reload();
		}else{
			alert('请刷新重试!');
			return;
		}
	})
}
function submit_ignore(speakerid,timeid){
	if(!speakerid || !timeid){
		alert('参数错误');return;
	}
	var url = '/aspirinonline/ignore-attend';
	var data = {speakerid:speakerid,timeid:timeid};
	$.post(url,data,function(msg){
	 	if(msg == 'success'){
	 		window.location.reload();
		}else{
			alert('请刷新重试!');
			return;
		}
	})
}
</script>
@stop