@extends('front.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/><!-- date -->
<link type="text/css" href="{{ asset('assets/js/date/css/jquery-ui-1.8.17.custom.css') }}" rel="stylesheet" />
<link type="text/css" href="{{ asset('assets/js/date/css/jquery-ui-timepicker-addon.css') }}" rel="stylesheet" />
<script type="text/javascript" src="{{ asset('assets/js/date/js/jquery-ui-1.8.17.custom.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/date/js/jquery-ui-timepicker-addon.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/date/js/jquery-ui-timepicker-zh-CN.js') }}"></script>
<style type="text/css">
<!--
.item-list { width:100%;float:left; }
.item-list h3 { color:#333;padding:0px 5px;margin-bottom:12px; }
.item-list ul { width:100%;float:left; }
.online_meeting .signup-page .item-list .item-count {
	margin-left:5px !important;width:auto !important;padding:0px 20px !important;background:#0090D3 !important;
}
.item-list ul span { width:auto;float:left;margin-left:25px;color:#888;font-size:14px;line-height:26px;margin-top:3px; }
.item-list ul span.dateBoxs { background:url('/assets/images/front/date_icon.png') no-repeat left;padding-left:25px;background-size:20px; }
.item-list ul span.timeBoxs { background:url('/assets/images/front/time_icon.png') no-repeat left;padding-left:26px;background-size:20px; }
.searchBoxs { width:100%;float:left;padding:20px 0;background:#FFF;border-bottom:1px solid #e5f4fb; }
.searchBoxs ul { padding:0px 20px; }
.searchBoxs ul li { width:auto;float:left;margin-right:10px; }
.searchBoxs ul li select, .searchBoxs ul li input { height:38px;line-height:33px;border:1px solid #CCC;border-radius:3px;color:#888; }
.searchBoxs ul li select { width:100px;padding-left:4px; }
.searchBoxs ul li input { height:36px;line-height:33px;text-indent:10px; }
.searchBoxs ul li input.dateInput {
	width:100px;background:url('/assets/images/front/date_icon.png') no-repeat 95px;padding-right:25px;background-size:20px;
}
.searchBoxs ul li input.keysInput { width:220px; }
.searchBtns { width:auto;float:left;padding:0px 25px;color:#FFF;background:#0090D3;height:36px;line-height:38px;border-radius:3px;font-size:14px; }
-->
</style>
<div class="p_sml_menu new_menu">您当前位置：<a href="/">首页</a> > <a href="/aspirin">专项基金</a> > 在线会议</div>
<div class="p_center_box fund_page">
	<div class="left">
    	<ul>
            <li><a href="/aspirin/index"><span style="background-position:left 4px center;" class="fund_icon1">专项基金</span></a></li>
            <li><a href="/aspirin/info-list"><span class="fund_icon2">阿司匹林</span></a></li>
            <li><a href="/aspirinonline/ppt/1" class="sele"><span class="fund_icon3_3">在线会议</span></a></li>
            <li><a href="/aspirinresearch/brief"><span class="fund_icon4">科研培训</span></a></li>
        </ul>
    </div>
    <div class="right fund_intro science online_meeting">
    	<div class="top_online_meeting">
        </div>
        <div class="risk_tool_tab">
            <div onclick="window.location.href='/aspirinonline/ppt/1';" style="cursor:pointer;">讲者报名</div>
            <div onclick="window.location.href='/aspirinonline/listener-list/1';" style="cursor:pointer;" class="sele">报名参会</div>
            <div onclick="window.location.href='/aspirinonline/review-list/0';" style="cursor:pointer;">精彩回顾</div>
            <div onclick="window.location.href='/aspirinonline/ppt-list/1';" style="cursor:pointer;" class="none">查看课件</div>
        </div>
		<!--  <div class="kinds_tab clearfix">
		<div onclick="window.location.href='/aspirinonline/listener-list/1';" style="cursor:pointer;" @if($iCatId == 1) class="sele" @endif>心内科</div>
		<div onclick="window.location.href='/aspirinonline/listener-list/2';" style="cursor:pointer;" @if($iCatId == 2) class="sele" @endif>神内科</div>
		<div class="bottom_line"></div>
		</div> -->
		<div class="searchBoxs clearfix">
			<ul>
				<li><select id="iCatId" name="iCatId">
					<option value="1"@if($iCatId == 1) selected @endif>心内科</option>
					<option value="2"@if($iCatId == 2) selected @endif>神内科</option>
				</select></li>
				<li><input type="text" class="dateInput" id="s_online_date" name="s_online_date" value="{{@$s_online_date}}" placeholder="日期"></li>
				<li><input type="text" class="keysInput" id="s_keys" name="s_keys" value="{{@$s_keys}}" placeholder="请输入主题关键字"></li>
				<a href="javascript:;" class="searchBtns">搜 索</a>
			</ul>
		</div>
        <!-- 	参会列表页：未报名的 小于5人是绿色，5到10人是蓝色，10是红色；已报名的显示绿色的“参加会议”，点击有相应提示；会议结束不显示 -->
        <div class="online_meeting_box signup-page" style="padding:0px 20px 30px;">
        	@foreach($aOnlineInfo as $k=>$v)
            <div class="item-list">
                <h3>{{$v['speaker_info'][0]['doc_name']}} 主题：{{$v['online_title']}}</h3>
                <ul>
                	@foreach($v['speaker_info'] as $key=>$val)
	                	@if($val['listener_count'] < 5)
	                    	@if($val['btn_flag'] == 2)
	                    	<a class="item-count color-skyblue" href="#" onclick="submit_attend({{$val['speaker_id']}},{{$v['online_time_id']}})">{{$val['doc_name']}} (0/10)</a><!-- 参加会议 -->
	                    	@elseif($val['btn_flag'] == 3)
	                    	<a class="item-count" style="background-color:#b4b4b4;" onclick="alert('会议未开始！');" href="#">报名 (0/10)</a>
	                    	@else
	                    	<a class="item-count color-green" onclick="pre_box({{$v['online_time_id']}},'{{$val['speaker_id']}}','{{$val['doc_name']}}','{{$val['doc_department']}}','{{$val['doc_hospital']}}');" href="#">
	                    	报名  ({{$val['listener_count']}}/{{$val['listener_limit']}})
	                    	</a>
	                    	@endif
	                	@elseif($val['listener_count'] >= 5 && $val['listener_count'] < 10)
	                		@if($val['btn_flag'] == 2)
	                    	<a class="item-count color-skyblue" onclick="submit_attend({{$val['speaker_id']}},{{$v['online_time_id']}})" href="#">参加会议</a>
	                    	@elseif($val['btn_flag'] == 3)
	                    	<a class="item-count" style="background-color:#b4b4b4;" onclick="alert('会议未开始！');" href="#">报名  (0/10)</a>
	                    	@else
	                    	<a class="item-count color-skyblue" onclick="pre_box({{$v['online_time_id']}},'{{$val['speaker_id']}}','报名 ','{{$val['doc_department']}}','{{$val['doc_hospital']}}');" href="#">
	                        报名  ({{$val['listener_count']}}/{{$val['listener_limit']}})
	                        </a>
	                        @endif
	                    @else
	                        <a class="item-count color-red" onclick="alert('报名人数已满');" href="#">
	                        报名  ({{$val['listener_count']}}/{{$val['listener_limit']}})
	                        </a>
	                    @endif
	                @endforeach
                	<span class="dateBoxs">{{$v['online_date']}}</span>
                	<span class="timeBoxs">{{$v['time_period']}}</span>
                </ul>
            </div>
	        @endforeach
        </div>
    </div>
	<div class="clear"></div>
</div>
<input type="hidden" id="speakerid" value="0">
<input type="hidden" id="timeid" value="0">
<div class="cover" id="shade" style="display:none;"></div>
<div class="modal_window style_one" id="pre_box" style="display:none;">
    <div class="cont_txt" style="padding-bottom: 10px" id="info"></div>
    <div class="btn_box">
        <button class="btn" onclick="$('#shade').hide();$('#pre_box').hide();">取消</button>
    	<button class="btn" onclick="enter_box();">确认报名</button>
    </div>
</div>
<div class="modal_window style_one" id="submit_notice_box" style="display:none;">
    <div class="title_close">
        <span class="title"></span>
        <span class="close" style="padding: 10px 10px 0 0">
        	<img src="/assets/images/front/web/close_btn.jpg" alt="" onclick="window.location.href='/aspirinonline/my-attend-list';">
        </span>
    </div>
    <div class="cont_txt" style="padding: 20px 42px 50px ;text-align: left">
        <p>如会议报名少于5人系统将自动取消此次会议，请您时时关注，谢谢。</p>
    </div>
</div>
@include('front.common.contact')
<script type="text/javascript">
$(function () {
    $(".dateInput").datetimepicker({
        showHour: false,
        showMinute: false,
        showSecond: false,
        showTime: false,
        timeFormat: '',
        stepHour: 1,
        stepMinute: 1,
        stepSecond: 1,
        // dateFormat: 'yy年mm月dd日' //格式化时间 
    });
    //搜索提交动作
    $(".searchBtns").on("click", function(){
    	var iCatId = $("#iCatId").val();
    	var s_online_date = $("#s_online_date").val();
    	var s_keys = $("#s_keys").val();
    	var gourl = "/aspirinonline/listener-list/"+iCatId+"/";
    	var surl, params = [];
    	if(s_online_date!="") params.push("s_online_date="+s_online_date.trim());
    	if(s_keys) params.push("s_keys="+encodeURIComponent(s_keys.trim()));
    	if(params!=""){
    		surl = gourl+"?"+params.join("&");
    	}else{
    		surl = gourl;
    	}
    	window.location.href = surl;
    });
});
function pre_box(timeid,speakerid,name,depart,hos){
	if(timeid == 0){
		alert('会议参数错误');return;
	}
	var url = '/aspirinonline/listener-enter';
	var data = {timeid:timeid};
	$.post(url,data,function(msg){
		if(msg == 'nocard'){
			alert('仅限通过医师认证的用户参与!');
			return;
		}
		if(msg == 'nextweek'){
			alert('请在本周日24点以后至会议开始前报名此会，谢谢！');
			return;
		}else if(msg == 'hasstart'){
 			alert('报名结束，会议已开始');
			return;
		}else if(msg == 'hasend'){
 			alert('会议已结束');
			return;
		}else if(msg == 'onlyone'){
 			alert('同一时间段只允许报名一位专家的会议，谢谢！');
			return;	
		}else if(msg == 'success'){
			$('#speakerid').val(speakerid);
			$('#timeid').val(timeid);
			var str = '';
			str += '<p>'+hos+'</p>'+
						'<p>'+depart+'</p>'+
			    		'<p>'+name+'</p>';
			$('#info').empty();
			$('#info').append(str);
			$('#shade').show();
			$('#pre_box').show();
		}else{
			alert('数据有误，请尝试重新进入!');
			return;
		}
	})
}
function enter_box(){
	var speakerid = $('#speakerid').val();
	var timeid = $('#timeid').val();
	if(speakerid == 0 || timeid == 0){
		alert('会议参数错误');return;
	}
	var url = '/aspirinonline/listener-submit';
	var data = {
			speakerid:speakerid,
			timeid:timeid
	};
	$.post(url,data,function(msg){
		if(msg == 'nocard'){
			alert('仅限通过医师认证的用户参与!');
			return;
		}
		if(msg == 'success'){
			$('#pre_box').hide();
			$('#submit_notice_box').show();
		}else if(msg == 'again'){
			alert('您已报名该讲者会议!');
			return;
		}else{
			alert('数据有误，请尝试重新进入!');
			return;
		}
	})
}
function submit_attend(speakerid,timeid){
	if(!speakerid || !timeid){
		alert('参数错误');return;
	}
	var url = '/aspirinonline/listener-attend';
	var data = {speakerid:speakerid,timeid:timeid};
	$.post(url,data,function(msg){
		if(msg.success){
			window.location.href = msg.url;
		}else{
			alert(msg.notice);return;
		}
	},'json')
}
</script>
@stop