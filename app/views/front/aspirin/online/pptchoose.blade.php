@extends('front.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
@include('front.aspirin.certification')
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/aspirin">专项基金</a>
    >
    在线会议
</div>
<div class="p_center_box fund_page">
	<div class="left">
    	<ul>
            <li><a href="/aspirin/index"><span style="background-position:left 4px center;" class="fund_icon1">专项基金</span></a></li>
            <li><a href="/aspirin/info-list"><span class="fund_icon2">阿司匹林</span></a></li>
            <li><a href="/aspirinonline/ppt/1" class="sele"><span class="fund_icon3_3">在线会议</span></a></li>
            <li><a href="/aspirinresearch/brief"><span class="fund_icon4">科研培训</span></a></li>
        </ul>
    </div>
    <div class="right fund_intro science online_meeting">
    	<div class="top_online_meeting">
	    	@if(isset($oOnline))
            <p class="s_title">{{$oOnline->online_title}}</p>
            <ul class="s_list">
                <li>• 累计举办讲座<span class="color-red">{{$iHasOpen}}场</span>；共<span class="color-red">{{$iTotalCount}}人</span>参与会议；</li>
                <li>• 即将开始讲座<span class="color-red">{{$iPreOpen}}场</span></li>
            </ul>
            @else
            <p class="s_title">暂无会议</p>
            @endif
        </div>
        <div class="risk_tool_tab">
            <div onclick="window.location.href='/aspirinonline/ppt/1';" style="cursor:pointer;" class="sele">讲者报名</div>
            <div onclick="window.location.href='/aspirinonline/listener-list/1';" style="cursor:pointer;">报名参会</div>
            <div onclick="window.location.href='/aspirinonline/review-list/0';" style="cursor:pointer;">精彩回顾</div>
            <div onclick="window.location.href='/aspirinonline/ppt-list/1';" style="cursor:pointer;" class="none">查看课件</div>
        </div>
        <div class="kinds_tab clearfix">
            <div onclick="window.location.href='/aspirinonline/ppt/1';" @if($iCatId == 1) class="sele" @endif>心内科</div>
            <div onclick="window.location.href='/aspirinonline/ppt/2';" @if($iCatId == 2) class="sele" @endif>神内科</div>
            <div class="bottom_line"></div>
        </div>
        <div class="world_heart">
            <div class="list_box choose-page">
            	@foreach($oPpt as $k=>$v)
                <div class="input-radio clearfix">
                    <input type="radio" class="left" name="ppt_box" value="{{$v->id}}">
                    <a href="/aspirinonline/ppt-show/{{$v->id}}/1" style="color:#0090d3;">
                    <label class="left">{{$v->ppt_title}}</label>
                    </a>
                </div>
                @endforeach
                <div class="btn_box">
                    <button class="btn" onclick="check_box({{$iCatId}});">确定</button>
                    <p class="tips-txt">点击主题查看课件</p>
                </div>
            </div>
        </div>
    </div>
	<div class="clear"></div>
</div>
<script>
function check_box(catid){
	var pptid = $("input[name='ppt_box']:checked").val();
	if(!pptid){
		alert('请先选择会议课件');return;
	}
    window.location.href="/aspirinonline/speaker-settime?iCatId={{$iCatId}}&iPptId="+pptid;
    // window.location.href="/aspirinonline/speaker-list/"+catid+"/"+pptid;
}
</script>
@include('front.common.contact')
@stop