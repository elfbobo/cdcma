@extends('front.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<script src="/assets/js/jquery-1.8.3.min.js"></script>
<script src="/assets/js/video.js"></script>
<?php 
if($oScoreLog){
	$iBlue1 = $oScoreLog->content_degree;
	$iGray1 = 5-$iBlue1;
	$iBlue2 = $oScoreLog->class_level;
	$iGray2 = 5-$iBlue2;
	$iBlue3 = $oScoreLog->overall_score;
	$iGray3 = 5-$iBlue3;
}
?>
<input type="hidden" id="score1" value="0"/>
<input type="hidden" id="score2" value="0"/>
<input type="hidden" id="score3" value="0"/>
<input type="hidden" id="videoid" value="{{$iVideoId}}"/>
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/aspirin">专项基金</a>
    >
    在线会议
</div>
<div class="p_center_box fund_page">
	<div class="left">
    	<ul>
            <li><a href="/aspirin/index"><span style="background-position:left 4px center;" class="fund_icon1">专项基金</span></a></li>
            <li><a href="/aspirin/info-list"><span class="fund_icon2">阿司匹林</span></a></li>
            <li><a href="/aspirinonline/ppt/1" class="sele"><span class="fund_icon3_3">在线会议</span></a></li>
            <li><a href="/aspirinresearch/brief"><span class="fund_icon4">科研培训</span></a></li>
        </ul>
    </div>
    <div class="right special_intro">
        <div class="highlights-page clearfix">
            <div class="clearfix">
                <div class="p_left_box left">
                    <div class="detailed_box">
                        <div class="bold">{{$oPpt->ppt_title}}</div>
                        <div class="detailed_time">时间：{{substr($oOnlineTime->online_date,0,10)}}</div>
                        <div class="detailed_video">
                        	<video src="{{$oVideo->video_url}}" controls style="width:548px;height:329px;"></video>
                        </div>
                        <div class="detailed_good">
                            <div class="share_box" style="padding-top: 5px">
                               <div class="bdsharebuttonbox">
<!--                                <a href="#" class="bds_more" data-cmd="more"></a> -->
                               <a href="#" class="bds_weixin" data-cmd="weixin" title="分享到微信"></a>
                               <a href="#" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博"></a>
                               </div>
							   <script>
							   var id = $('#videoid').val();
							   var url = 'http://cdcma.bizconf.cn/aspirinshare/online-show/'+id;
							   window._bd_share_config={
									   "common":{"bdSnsKey":{},"bdText":"","bdUrl":url,"bdMini":"3","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"24"},
									   "share":{},
									   "selectShare":{"bdContainerClass":null,"bdSelectMiniList":["weixin","tsina"]}
							   };
							   with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];
							   </script>
                            </div>
                            @if($iZan == 1)
                            <a onclick="alert('已经赞过了呦')" class="good"></a>
                            <a onclick="alert('已经赞过了呦')" class="good_txt">Like</a>
                            @else
                            <a onclick="submit_video_zan({{$oVideo->id}})" class="good" style="cursor:pointer;"></a>
                            <a onclick="submit_video_zan({{$oVideo->id}})" class="good_txt" style="cursor:pointer;">Like</a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="evaluation_box right">
                	<!-- 判断是否已经提交过星级评分，每人限评一次，评过之后不再可点（3个分开判断） -->
					@if($oScoreLog && $oScoreLog->content_degree != 0)
	                <div class="list01">
	                    <p>内容丰富程度：</p>
	                    <p class="star_box">
	                    	@for($i=1;$i<=$iBlue1;$i++)
					        <span><img src="/assets/images/front/web/icon_star3.png" class="star"></span>  
					        @endfor
					        @for($i=1;$i<=$iGray1;$i++)
					        <span><img src="/assets/images/front/web/icon_star4.png" class="star"></span>  
					        @endfor
	                    </p>
	                </div>
	                @else
	                <div class="list">
	                    <p>内容丰富程度：</p>
                        <p class="star_box">
	                    	@for($i=1;$i<=5;$i++)
		                    <span><img src="/assets/images/front/web/icon_star4.png" class="star"></span>
		                    @endfor
	                    </p>
	                </div>
	                @endif
                    @if($oScoreLog && $oScoreLog->class_level != 0)
                	<div class="list02">
	                    <p>专家讲课水平：</p>
	                    <p class="star_box">
	                    	@for($i=1;$i<=$iBlue2;$i++)
					        <span><img src="/assets/images/front/web/icon_star3.png" class="star"></span>  
					        @endfor
					        @for($i=1;$i<=$iGray2;$i++)
					        <span><img src="/assets/images/front/web/icon_star4.png" class="star"></span>  
					        @endfor
	                    </p>
	                </div>
	                @else
	                <div class="list2">
	                    <p>专家讲课水平：</p>
	                    <p class="star_box">
	                    	@for($i=1;$i<=5;$i++)
		                    <span><img src="/assets/images/front/web/icon_star4.png" class="star"></span>
		                    @endfor 
	                    </p>
	                </div>
	                @endif			
                    @if($oScoreLog && $oScoreLog->overall_score != 0)
	                <div class="list03">
	                    <p>整体综合评分：</p>
                        <p class="star_box">
	                        @for($i=1;$i<=$iBlue3;$i++)
					        <span><img src="/assets/images/front/web/icon_star3.png" class="star"></span>  
					        @endfor
					        @for($i=1;$i<=$iGray3;$i++)
					        <span><img src="/assets/images/front/web/icon_star4.png" class="star"></span>  
					        @endfor
	                    </p>
	                </div>
	                @else
	                <div class="list3">
	                    <p>整体综合评分：</p>
                        <p class="star_box">
	                       @for($i=1;$i<=5;$i++)
		                   <span><img src="/assets/images/front/web/icon_star4.png" class="star"></span>
		                   @endfor
	                    </p>
	                </div>
	                @endif
                    <div class="btn_box">
                        <button class="btn" onclick="submit_video_score();">提交</button>
                    </div>
                </div>
            </div>
            <div class="comment">
                <div class="p_bar_bg w_auto bg_color2">
                    <div class="p_tit">医友评论</div>
                </div>
                <div class="comment-list">
                    <div class="">
                        @foreach($oComments as $k=>$v)
                        <div class="comment_line">
                            <div class="left wid80">
                                <a ><img class="pic" src="{{$v->user_thumb}}"/></a>
                            </div>
                            <div class="right wid405">
                                <div class="name_line"><a class="name">{{$v->user_nick}}</a></div>
                                <div class="">
                                    <div class="left wid280">{{$v->comment}}</div>
                                    <div class="right wid125">
                                        <a class="hand" onclick="comment_zan({{$v->id}})" style="cursor:pointer;">（<span id="comment_zan_{{$v->id}}">{{$v->zan_count}}</span>）</a>
                                        <a class="reply" onclick="video_reply({{$v->user_id}},'{{$v->user_nick}}')" style="cursor:pointer;">回复</a>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
						@endforeach
                        <div class="people_num">已有<span>{{count($oComments)}}</span>位医友发表了看法</div>
                        <div class="comment_cont">
			            	<div id='reply' value="" style='color:#666;position: relative;left:4px;top:1px;height:22px;display:none;'>
							</div>
							<div name="content" id="content" contentEditable="true" style="margin:0 0 auto 0;outline:none;background-color: #FFFFFF; color: #888888;font: 14px/24px '微软雅黑';border: 1px solid #e0e0e0;height: 96px;overflow-y: auto;padding: 5px;resize: none;width: 482px;">
							</div>
			            </div>
			            <div class="comment_btn_box" style="position: relative;">
			            	<a class="face" href="javascript:void(0);" id="showpic">插入表情</a>
			            	<div  class="" style="background:#fff;position:absolute;left:8px;top:50px;display:none;height:89px;border:1px solid #D1D1D1;margin-left:14px;padding:5px 10px 5px 10px;" id="pic_list">{{$sImgLi}}
				                 <a href='javascript:void(0);' onclick='hidePic(0)' class='icon close1'></a>
				            </div>
			                <input id="replay_box" class="comment_btn" type="button" value="发表评论" onclick="submit_comment({{$oVideo->id}});"/>
			                <div class="clear"></div>
			            </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
	<div class="clear"></div>
</div>
<script>
function submit_video_zan(videoid){
	if(!videoid || videoid == 0){
		alert('参数错误！');return;
	}
	var url = '/aspirinonline/zan-video';
	var data = {videoid:videoid};
	$.post(url,data,function(msg){
		if(msg == 'success'){
			alert('点赞成功');return;
		}else if(msg == 'again'){
			alert('您已经为当前视频点赞了哦');return;
		}else{
			alert('支持失败，请刷新当前页面重试！');return;
		}
	})
}
function submit_video_score(){
	var videoid = $('#videoid').val();
	var score1 = $('#score1').val();
	var score2 = $('#score2').val();
	var score3 = $('#score3').val();
// 	if(score1 == 0 && score2 == 0 && score3 == 0){
// 		alert('您还未进行评分！');
// 		return;
// 	}
	var url = '/aspirinonline/score';
	var data = {videoid:videoid,score1:score1,score2:score2,score3:score3};
	$.post(url,data,function(msg){
		if(msg == 'success'){
			alert('提交成功');
			return;
		}else if(msg == 'again'){
			alert('您已经点评过了！');
			return;
		}else{
			alert('提交错误！');return;
		}
	})
}
function submit_comment(videoid){
	if(!videoid || videoid == 0){
		alert('参数错误！');return;
	}
	 var reply = $("#reply").html();
	 var content = $("#content").html();
	 if(!trim(content)){
		 alert('评论内容不能为空');
		 return false;
	 }
	 $('#replay_box').attr("disabled",true);
	 var comment = reply+content;
	 var url = '/aspirinonline/comment';
	 var data = {videoid:videoid,comment:comment};
	 $.post(url,data,function(msg){
		if(msg == 'success'){
            window.location.reload();
		}else{
			alert('提交错误！');return;
		}
	 })
}
function comment_zan(commentid){
	var url = '/aspirinonline/zan-comment';
	var data = {commentid,commentid};
	$.post(url,data,function(msg){
		if(msg == 'success'){
			var count = parseInt($("#comment_zan_"+commentid).html());
			$("#comment_zan_"+commentid).html(count+1);
		}else if(msg == 'again'){
			alert('您已经点赞了哦');return;
		}else{
			alert('提交错误！');return;
		}
	});
}
</script>
<script type="text/javascript">
	$('.evaluation_box .list01 span').click(function(){
		$('.evaluation_box .list01 span').unbind("click");
	});
	$('.evaluation_box .list02 span').click(function(){
    	$('.evaluation_box .list02 span').unbind("click");
    });
	$('.evaluation_box .list03 span').click(function(){
    	$('.evaluation_box .list03 span').unbind("click");
    });
</script>
<script type="text/javascript">
	$('.evaluation_box .list span').click(function(){
    	console.log($(this).index())
    	var num = $(this).index()+1;
    	$(this).parent().find('.star').each(function(){
        	$(this).attr('src','/assets/images/front/web/icon_star4.png')
    	})
        for(var i=0 ; i<num;i++){
            $(this).parent().find('.star').eq(i).attr('src','/assets/images/front/web/icon_star3.png')
        }
    	$('#score1').val(num);
//     	submit_video_score();
    	$('.evaluation_box .list span').unbind("click");
    });
    $('.evaluation_box .list2 span').click(function(){
    	console.log($(this).index())
    	var num = $(this).index()+1;
    	$(this).parent().find('.star').each(function(){
        	$(this).attr('src','/assets/images/front/web/icon_star4.png')
    	})
        for(var i=0 ; i<num;i++){
            $(this).parent().find('.star').eq(i).attr('src','/assets/images/front/web/icon_star3.png')
        }
    	$('#score2').val(num);
//     	submit_video_score();
    	$('.evaluation_box .list2 span').unbind("click");
    });
    $('.evaluation_box .list3 span').click(function(){
    	console.log($(this).index())
    	var num = $(this).index()+1;
    	$(this).parent().find('.star').each(function(){
        	$(this).attr('src','/assets/images/front/web/icon_star4.png')
    	})
        for(var i=0 ; i<num;i++){
            $(this).parent().find('.star').eq(i).attr('src','/assets/images/front/web/icon_star3.png')
        }
    	$('#score3').val(num);
//     	submit_video_score();
    	$('.evaluation_box .list3 span').unbind("click");
    });
</script>
<script>
	document.onclick=function(event){
		var e = event || window.event;
		var elem = e.srcElement||e.target;
		while(elem){
			if(elem.id == 'showpic' || elem.id == 'pic_list'){
				$("#pic_list").show();
				return;
			}else{
				$("#pic_list").hide();
				//return;
			}
			elem = elem.parentNode;
		}
	}
</script>
@include('front.common.contact')
@stop