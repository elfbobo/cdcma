@extends('front.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/aspirin">专项基金</a>
    >
    在线会议
</div>
<div class="p_center_box fund_page">
	<div class="left">
    	<ul>
            <li><a href="/aspirin/index"><span style="background-position:left 4px center;" class="fund_icon1">专项基金</span></a></li>
            <li><a href="/aspirin/info-list"><span class="fund_icon2">阿司匹林</span></a></li>
            <li><a href="/aspirinonline/ppt/1" class="sele"><span class="fund_icon3_3">在线会议</span></a></li>
            <li><a href="/aspirinresearch/brief"><span class="fund_icon4">科研培训</span></a></li>
        </ul>
    </div>
    <div class="right fund_intro science online_meeting">
    	<div class="top_online_meeting">
        </div>
        <div class="risk_tool_tab">
            <div onclick="window.location.href='/aspirinonline/ppt/1';" style="cursor:pointer;">讲者报名</div>
            <div onclick="window.location.href='/aspirinonline/listener-list/1';" style="cursor:pointer;">报名参会</div>
            <div onclick="window.location.href='/aspirinonline/review-list/0';" style="cursor:pointer;">精彩回顾</div>
            <div onclick="window.location.href='/aspirinonline/ppt-list/1';" style="cursor:pointer;" class="sele">查看课件</div>
        </div>
        <div class="kinds_tab clearfix">
            <div onclick="window.location.href='/aspirinonline/ppt-list/1';" @if($iCatId == 1) class="sele" @endif>心内科</div>
            <div onclick="window.location.href='/aspirinonline/ppt-list/2';" @if($iCatId == 2) class="sele" @endif>神内科</div>
            <div class="bottom_line"></div>
        </div>
        <div class="world_heart">
            <div class="list_box">
            	@if($oPpt)
            	@foreach($oPpt as $k=>$v)
                <div class="list" onclick="window.location.href='/aspirinonline/ppt-show/{{$v->id}}/0';" style="cursor:pointer;">
                    <div class="list-pic"><img src="{{$v->ppt_thumb}}"></div>
                    <div class="list-item">
                        <div class="list-item-title">{{$v->ppt_title}}</div>
                        <div class="list-item-intro">{{substr($v->created_at,0,10)}}</div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop