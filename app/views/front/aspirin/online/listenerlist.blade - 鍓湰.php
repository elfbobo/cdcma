@extends('front.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/aspirin">专项基金</a>
    >
    在线会议
</div>

<div class="p_center_box fund_page">
	<div class="left">
    	<ul>
            <li><a href="/aspirin/index"><span style="background-position:left 4px center;" class="fund_icon1">专项基金</span></a></li>
            <li><a href="/aspirin/info-list"><span class="fund_icon2">阿司匹林</span></a></li>
            <li><a href="/aspirinonline/ppt/1" class="sele"><span class="fund_icon3_3">在线会议</span></a></li>
            <li><a href="/aspirinresearch/brief"><span class="fund_icon4">科研培训</span></a></li>
        </ul>
    </div>
    <div class="right fund_intro science online_meeting">
    	<div class="top_online_meeting">
        </div>
        <div class="risk_tool_tab">
            <div onclick="window.location.href='/aspirinonline/ppt/1';" style="cursor:pointer;">讲者报名</div>
            <div onclick="window.location.href='/aspirinonline/listener-list/1';" style="cursor:pointer;" class="sele">报名参会</div>
            <div onclick="window.location.href='/aspirinonline/review-list/0';" style="cursor:pointer;">精彩回顾</div>
            <div onclick="window.location.href='/aspirinonline/ppt-list/1';" style="cursor:pointer;" class="none">查看课件</div>
        </div>
        <div class="kinds_tab clearfix">
            <div onclick="window.location.href='/aspirinonline/listener-list/1';" style="cursor:pointer;" @if($iCatId == 1) class="sele" @endif>心内科</div>
            <div onclick="window.location.href='/aspirinonline/listener-list/2';" style="cursor:pointer;" @if($iCatId == 2) class="sele" @endif>神内科</div>
            <div class="bottom_line"></div>
        </div>
        <!-- 	参会列表页：未报名的 小于5人是绿色，5到10人是蓝色，10是红色；已报名的显示绿色的“参加会议”，点击有相应提示；会议结束不显示 -->
        <div class="online_meeting_box signup-page">
        	@foreach($aOnlineInfo as $k=>$v)
            <div class="item-list">
                <div class="item-flex">
                    <div class="item-time width-300">
                        <p>{{sub_str($v['online_title'],20)}}</p>
                        <p>{{$v['online_date']}} {{$v['time_period']}}</p>
                    </div>
                    <div class="clearfix width-390">
                    	@foreach($v['speaker_info'] as $key=>$val)
	                    	@if($val['listener_count'] < 5)
		                    	@if($val['btn_flag'] == 2)
		                    	<div class="item-count color-skyblue" onclick="submit_attend({{$val['speaker_id']}},{{$v['online_time_id']}})" style="cursor:pointer;">参加会议</div>
		                    	@elseif($val['btn_flag'] == 3)
		                    	<div class="item-count" style="background-color:#b4b4b4;" onclick="alert('会议未开始！');" style="cursor:pointer;">参加会议</div>
		                    	@else
		                    	<div class="item-count color-green" onclick="pre_box({{$v['online_time_id']}},'{{$val['speaker_id']}}','{{$val['doc_name']}}','{{$val['doc_department']}}','{{$val['doc_hospital']}}');" style="cursor:pointer;">
		                    	{{$val['doc_name']}}({{$val['listener_count']}}/{{$val['listener_limit']}})
	                        	</div>
		                    	@endif
	                    	@elseif($val['listener_count'] >= 5 && $val['listener_count'] < 10)
	                    		@if($val['btn_flag'] == 2)
		                    	<div class="item-count color-skyblue" onclick="submit_attend({{$val['speaker_id']}},{{$v['online_time_id']}})" style="cursor:pointer;">参加会议</div>
		                    	@elseif($val['btn_flag'] == 3)
		                    	<div class="item-count" style="background-color:#b4b4b4;" onclick="alert('会议未开始！');" style="cursor:pointer;">参加会议</div>
		                    	@else
		                    	<div class="item-count color-skyblue" onclick="pre_box({{$v['online_time_id']}},'{{$val['speaker_id']}}','{{$val['doc_name']}}','{{$val['doc_department']}}','{{$val['doc_hospital']}}');" style="cursor:pointer;">
		                        {{$val['doc_name']}}({{$val['listener_count']}}/{{$val['listener_limit']}})
		                        </div>
		                        @endif
	                        @else
		                        <div class="item-count color-red" onclick="alert('报名人数已满');" style="cursor:pointer;">
		                        {{$val['doc_name']}}({{$val['listener_count']}}/{{$val['listener_limit']}})
		                        </div>
	                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
	        @endforeach
        </div>
    </div>
	<div class="clear"></div>
</div>
<input type="hidden" id="speakerid" value="0">
<input type="hidden" id="timeid" value="0">
<div class="cover" id="shade" style="display:none;"></div>
<div class="modal_window style_one" id="pre_box" style="display:none;">
    <div class="cont_txt" style="padding-bottom: 10px" id="info">
<!--         <p>北大人民医院心内科</p> -->
<!--         <p>孙宁玲</p> -->
    </div>
    <div class="btn_box">
        <button class="btn" onclick="$('#shade').hide();$('#pre_box').hide();">取消</button>
    	<button class="btn" onclick="enter_box();">确认报名</button>
    </div>
</div>
<div class="modal_window style_one" id="submit_notice_box" style="display:none;">
    <div class="title_close">
        <span class="title"></span>
        <span class="close" style="padding: 10px 10px 0 0">
        	<img src="/assets/images/front/web/close_btn.jpg" alt="" onclick="window.location.href='/aspirinonline/my-attend-list';">
        </span>
    </div>
    <div class="cont_txt" style="padding: 20px 42px 50px ;text-align: left">
        <p>如会议报名少于5人系统将自动取消此次会议，请您时时关注，谢谢。</p>
    </div>
</div>
@include('front.common.contact')
<script type="text/javascript">
	function pre_box(timeid,speakerid,name,depart,hos){
		if(timeid == 0){
			alert('会议参数错误');return;
		}
		var url = '/aspirinonline/listener-enter';
		var data = {timeid:timeid};
		$.post(url,data,function(msg){
			if(msg == 'nocard'){
				alert('仅限通过医师认证的用户参与!');
				return;
			}
			if(msg == 'nextweek'){
				alert('请在本周日24点以后至会议开始前报名此会，谢谢！');
				return;
			}else if(msg == 'hasstart'){
	 			alert('报名结束，会议已开始');
				return;
			}else if(msg == 'hasend'){
	 			alert('会议已结束');
				return;
			}else if(msg == 'onlyone'){
	 			alert('同一时间段只允许报名一位专家的会议，谢谢！');
				return;	
			}else if(msg == 'success'){
				$('#speakerid').val(speakerid);
				$('#timeid').val(timeid);
				var str = '';
				str += '<p>'+hos+'</p>'+
							'<p>'+depart+'</p>'+
				    		'<p>'+name+'</p>';
				$('#info').empty();
				$('#info').append(str);
				$('#shade').show();
				$('#pre_box').show();
			}else{
				alert('数据有误，请尝试重新进入!');
				return;
			}
		})
	}
	function enter_box(){
		var speakerid = $('#speakerid').val();
		var timeid = $('#timeid').val();
		if(speakerid == 0 || timeid == 0){
			alert('会议参数错误');return;
		}
		var url = '/aspirinonline/listener-submit';
		var data = {
				speakerid:speakerid,
				timeid:timeid
		};
		$.post(url,data,function(msg){
			if(msg == 'nocard'){
				alert('仅限通过医师认证的用户参与!');
				return;
			}
			if(msg == 'success'){
				$('#pre_box').hide();
				$('#submit_notice_box').show();
			}else if(msg == 'again'){
				alert('您已报名该讲者会议!');
				return;
			}else{
				alert('数据有误，请尝试重新进入!');
				return;
			}
		})
	}
	function submit_attend(speakerid,timeid){
		if(!speakerid || !timeid){
			alert('参数错误');return;
		}
		var url = '/aspirinonline/listener-attend';
		var data = {speakerid:speakerid,timeid:timeid};
		$.post(url,data,function(msg){
			if(msg.success){
				window.location.href = msg.url;
			}else{
				alert(msg.notice);return;
			}
		},'json')
	}
</script>
@stop