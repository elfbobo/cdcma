@extends('front.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<!-- 1. Define some markup -->
<!-- 
<button class="btn" data-clipboard-text="Just because you can doesn't mean you should — clipboard.js">
	Copy to clipboard
</button>
-->
<!-- 2. Include library -->
<script src="/assets/js/dist/clipboard.min.js"></script>

<!-- 3. Instantiate clipboard -->
<script>
    var clipboard = new Clipboard('#btn');

    clipboard.on('success', function(e) {
        console.log(e);
    });

    clipboard.on('error', function(e) {
        console.log(e);
    });
</script>
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/aspirin">专项基金</a>
    >
    在线会议
</div>

<div class="p_center_box fund_page">
	<div class="left">
    	<ul>
            <li><a href="/aspirin/index"><span style="background-position:left 4px center;" class="fund_icon1">专项基金</span></a></li>
            <li><a href="/aspirin/info-list"><span class="fund_icon2">阿司匹林</span></a></li>
            <li><a href="/aspirinonline/ppt/1" class="sele"><span class="fund_icon3_3">在线会议</span></a></li>
            <li><a href="/aspirinresearch/brief"><span class="fund_icon4">科研培训</span></a></li>
        </ul>
    </div>
    <div class="right fund_intro science online_meeting">
    	<div class="top_online_meeting">
    		@if(isset($oOnline) && $oOnline)
    		<?php $oPpt = AspirinOnlinePpt::find($iPptId);?>
    		@if($oPpt)
    		<p class="s_title">{{sub_str($oPpt->ppt_title,15)}}</p>
            @endif
            <ul class="s_list">
                <li>• 累计举办讲座<span class="color-red">{{$iHasOpen}}场</span>；共<span class="color-red">{{$iTotalCount}}人</span>参与会议；</li>
                <li>• 即将开始讲座<span class="color-red">{{$iPreOpen}}场</span></li>
            </ul>
            @else
            <p class="s_title">暂无会议</p>
            @endif
        </div>
        <div class="risk_tool_tab">
            <div onclick="window.location.href='/aspirinonline/ppt/1';" style="cursor:pointer;" class="sele">讲者报名</div>
            <div onclick="window.location.href='/aspirinonline/listener-list/1';" style="cursor:pointer;">报名参会</div>
            <div onclick="window.location.href='/aspirinonline/review-list/0';" style="cursor:pointer;">精彩回顾</div>
            <div onclick="window.location.href='/aspirinonline/ppt-list/1';" style="cursor:pointer;" class="none">查看课件</div>
        </div>
        <div class="online_meeting_box">
            <div class="item-header">会议时间（自选）：</div>
            @if(isset($oOnline) && $oOnline)
            @foreach($aOnlineTime as $k=>$v)
	        <div class="item-list">
            	<div class="item-flex">
                    <div class="item-time">{{$v['online_date']}} {{$v['time_period']}}</div>
                    @if($v['enter_flag'] == 0)
                    <div class="item-count ban">报名({{$v['speaker_count']}}/{{$v['limit_count']}})</div>
	                @elseif($v['enter_flag'] == 2)
                    <div class="item-count ban" onclick="alert('会议未开始！');" style="cursor:pointer;">启动会议</div>
                    @elseif($v['enter_flag'] == 3)
                    <div class="item-count" onclick="meeting_open({{$v['online_time_id']}});" style="cursor:pointer;">启动会议</div>
	                @else
	                <div class="item-count" onclick="pre_box({{$oOnline->id}},{{$v['online_time_id']}});"  style="cursor:pointer;">报名({{$v['speaker_count']}}/{{$v['limit_count']}})</div>
	                @endif
                </div>
            </div>
	        @endforeach
            <div class="item-footer">注：会议参与人员最低5人,如报名人数不满5人则本次会议将自动取消。</div>
            @endif
        </div>
    </div>
	<div class="clear"></div>
</div>
@if(isset($oOnline) && $oOnline)
<input type="hidden" id="onlineid" value="{{$oOnline->id}}">
<input type="hidden" id="timeid" value="0">
<input type="hidden" id="pptid" value="{{$iPptId}}">
@endif
<div class="cover" style="display:none;" id="shade"></div>
<div class="modal_window style_one" id="pre_notice_box" style="display:none;">
    <div class="cont_txt">
        <p>本月讲题仅限选择一场与医生在线交流，您是否确认报名该场在线会议?</p>
    </div>
    <div class="btn_box">
        <button class="btn" onclick="$('#shade').hide();$('#pre_notice_box').hide();">再考虑一下</button>
        <button class="btn" onclick="$('#pre_notice_box').hide();$('#submit_box').show();">确定</button>
    </div>
</div>
<div class="modal_window style_two" id="submit_box" style="display:none;">
    <div class="content">
        <p class="font_bold">请您填写以下信息，以便申报专家费用：</p>
        <div class="input_box">
            <label for="declare_name">姓名：</label>
            <input type="text" class="input_text" id="declare_name" @if($oUserInfo) value="{{$oUserInfo->declare_name}}" @endif>
        </div>
        <div class="input_box">
            <label for="declare_id_num">身份证号：</label>
            <input type="text" class="input_text" id="declare_id_num" @if($oUserInfo) value="{{$oUserInfo->declare_id_number}}" @endif>
        </div>
        <div class="input_box">
            <label for="declare_bank">开户行：</label>
            <input type="text" class="input_text" id="declare_bank" @if($oUserInfo) value="{{$oUserInfo->declare_bank}}" @endif>
        </div>
        <div class="input_box">
            <label for="declare_bank_num">银行账号：</label>
            <input type="text" class="input_text" id="declare_bank_num" @if($oUserInfo) value="{{$oUserInfo->declare_bank_number}}" @endif>
        </div>
        <div class="btn_box">
            <button class="btn" onclick="$('#shade').hide();$('#submit_box').hide();">再考虑一下</button>
            <button class="btn" id="enterbutton" onclick="enter_box();">确定</button>
        </div>
    </div>
</div>
<div class="modal_window style_one" id="submit_notice_box" style="display:none;">
    <div class="title_close">
        <span class="title"></span>
        <span class="close" style="padding: 10px 10px 0 0">
        <img src="/assets/images/front/web/close_btn.jpg" alt="" onclick="window.location.href='/aspirinonline/my-enter-list';">
        </span>
    </div>
    <div class="cont_txt" style="padding: 0 42px 30px ;text-align: left">
        <p>恭喜您已报名成功，如会议报名少于5人系统将自动取消此次会议，请您时时关注，谢谢。</p>
    </div>
</div>
<div class="modal_window style_one" id="meeting_notice_box" style="display:none;">
    <div class="cont_txt" style="padding: 20px 42px 50px ; font-size: 16px">
        <p>温馨提示：</p>
        <p>会议时间不少于30分钟。</p>
        <p>幻灯分享链接:</p>
        <p>{{$sPptHtml}}进入会议后打开共享网页直接粘贴即可分享幻灯。</p>
    </div>
    <div class="btn_box">
        <button class="btn" onclick="$('#shade').hide();$('#meeting_notice_box').hide();">再考虑一下</button>
        <button class="btn" id="btn" data-clipboard-text="{{$sPptHtml}}" onclick="submit_open();">确定</button>
    </div>
</div>
@include('front.common.contact')
<script type="text/javascript">
function pre_box(iOnlineId,timeid){
	if(iOnlineId == 0 || timeid == 0){
		alert('会议参数错误');return;
	}
	var url = '/aspirinonline/speaker-enter';
	var data = {iOnlineId:iOnlineId};
	$.post(url,data,function(msg){
		if(msg == 'nocard'){
			alert('仅限通过医师认证的副高级别以上用户参与!');
			return;
		}else if(msg == 'noauth'){
			alert('仅限副高以上职称用户报名!');
			return;
		}else if(msg == 'one'){
 			alert('每月只可报名一场会议!');
			return;
		}else if(msg == 'twice'){
 			alert('试运营期内限每位讲者只能参与两次!');
			return;	
		}else if(msg == 'again'){
 			alert('您已经申报过本月会议!');
			return;
		}else if(msg == 'success'){
			$('#onlineid').val(iOnlineId);
			$('#timeid').val(timeid);
			$('#shade').show();
			$('#pre_notice_box').show();
		}else if(msg == 'shangxian'){
 			alert('已达试点地区报名场次数上限!');
			return;
		}else{
			alert('数据有误，请尝试重新进入!');
			return;
		}
	})
}
function enter_box(){
	var iPptId = $('#pptid').val();
	var iOnlineId = $('#onlineid').val();
	var timeid = $('#timeid').val();
	var declare_name = $('#declare_name').val();
	var declare_id_num = $('#declare_id_num').val();
	var declare_bank = $('#declare_bank').val();
	var declare_bank_num = $('#declare_bank_num').val();
	if(iOnlineId == 0 || timeid == 0 || iPptId == 0){
		alert('会议参数错误');return;
	}
	if(!declare_name){
		alert('请输入申报姓名');return;
	}
	if(!declare_id_num){
		alert('请输入有效身份证号');return;
	}
	if(!declare_bank){
		alert('请输入开户行');return;
	}
	if(!declare_bank_num){
		alert('请输入有效银行账号');return;
	}
	var url = '/aspirinonline/speaker-submit';
	var data = {
			iPptId:iPptId,
			iOnlineId:iOnlineId,
			timeid:timeid,
			declare_name:declare_name,
			declare_id_num:declare_id_num,
			declare_bank:declare_bank,
			declare_bank_num:declare_bank_num
	};
	$.post(url,data,function(msg){
		$('#enterbutton').attr('disabled',true); 
		if(msg == 'nocard'){
			alert('仅限通过医师认证的副高级别以上用户参与!');
			return;
		}else if(msg == 'success'){
			$('#submit_box').hide();
			$('#submit_notice_box').show();
		}else if(msg == 'notime'){
			alert('该时间段已被占满!');
			return;
		}else if(msg == 'meetingerror'){
 			alert('获取会议链接失败!');
			return;
		}else if(msg == 'codeerror'){
			alert('未获取到会议编码，请重试!');
			return;
		}else if(msg == 'shangxian'){
 			alert('已达试点地区报名场次数上限!');
			return;
		}else{
			alert('数据有误，请尝试重新进入!');
			return;
		}
	})
}
function meeting_open(timeid){
	$('#timeid').val(timeid);
	$('#shade').show();
	$('#meeting_notice_box').show();
}
function submit_open(){
	var timeid = $('#timeid').val();
	if(timeid == 0){
		alert('会议参数错误');return;
	}
	var url = '/aspirinonline/speaker-open';
	var data = {timeid:timeid};
	$.post(url,data,function(msg){
		if(msg.success){
			window.location.href = msg.url;
		}else{
			alert(msg.notice);
			$('#shade').hide();
			$('#meeting_notice_box').hide();
		}
	},'json');
}
</script>
@stop