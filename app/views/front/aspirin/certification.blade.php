<?php 
$oUser = User::find(Session::get('userid'));
if($oUser->user_company != 0 && is_numeric($oUser->user_company)){
	$sHospitalName = Hospital::where('id',$oUser->user_company)->pluck('name');
}else{
	$sHospitalName = $oUser->user_company_name;
}
?>
@if($oUser->role_id==3 && $oUser->card_auth_flag == 0)
<script>
$(function(){
	$('#cover').show();
	$('#certification_box').show();
});
</script>
@endif
<div class="cover" style="display:none;" id="cover"></div>
<div class="modal_window certification" style="display:none;" id="certification_box">
    <div class="content">
        <span class="close" style="float:right;">
            <img src="/assets/images/front/web/pop_close.png" onclick="$('#cover').hide();$('#certification_box').hide();" style="cursor:pointer;">
        </span>
        <div class="title_close">
            <span class="title">请您先进行认证：</span>
            <span class="close"><img src="/assets/images/front/web/pop_close.png" alt="" style="display:none;"></span>
        </div>
        <div class="input_box">
            <label>姓名：</label>
            <input type="text" class="input_text" value="{{$oUser->user_name}}" disabled>
        </div>
        <div class="input_box">
            <label>医院：</label>
            <input type="text" class="input_text" value="{{sub_str($sHospitalName,12)}}" disabled>
        </div>
        <div class="input_box">
            <label>科室：</label>
            <input type="text" class="input_text" value="{{$oUser->user_department}}" disabled>
        </div>
        <?php 
        $aPos = User::getPosition();
        ?>  
        <div class="input_box">
            <label for="">职称：</label>
            <select name="" id="user_position" class="input_select">
                @foreach($aPos as $k=>$v)
			    <option value="{{$v}}" @if($oUser->user_position==$v) selected @endif>{{$v}}</option>
				@endforeach
            </select>
        </div>
        <div class="input_box">
            <label for="">医师证号：</label>
            <input type="text" class="input_text" placeholder="请输入您的医师执业证号" id="card_number" value="{{$oUser->card_number}}">
        </div>
        <div class="input_box">
            <label for="">上传证件照片：</label>
            <div class="file_box">
                <label for="file-pic"><img id="thumb" @if($oUser->card_thumb) src="{{$oUser->card_thumb}}" @else src="/assets/images/front/web/add.png" @endif style="width:96px;height:96px;"></label>
                <input type="file" class="input_file" id="file-pic" name="file-pic" onchange="saveCardThumb();">
            </div>
        </div>
        <div class="input_box" style="width:520px;">
			<span style="color:red;">注：请上传您的医师执业证书照片</span>
		</div>
		<input type="hidden" id="card_thumb" value="{{$oUser->card_thumb}}"/>
        <div class="btn_box">
            <input type="button" class="btn" value="立即认证" onclick="submitAuth();"/>
        </div>
    </div>
</div>
<div class="modal_window style_one" style="display:none;" id="submit_certification_box">
    <p class="font_bold">您已提交成功</p>
    <p>我们将在3个工作日内进行审核</p>
    <p>请您耐心等待审核</p>
    <div class="btn_box">
        <button class="btn" onclick="$('#cover').hide();$('#submit_certification_box').hide();">我知道了</button>
    </div>
</div>
<script>
	function saveCardThumb()
	{
		$.ajaxFileUpload({
			url: '/aspirin/upload-user-card',
			secureuri: false,
			fileElementId: "file-pic",
			dataType: "json",
			success: function(data, status) {
				$('#thumb').attr('src', data.card_thumb);
				$('#card_thumb').val(data.card_thumb);
			}
		})
	}
	function submitAuth()
	{
		var user_position = $('#user_position').val();
		var card_number = $('#card_number').val();
		var card_thumb = $('#card_thumb').val();
		if(!user_position){
			alert('请选择职称');
			return false;
		}
		if(!card_number){
			alert('请填写医师证号');
			return false;
		}
		if(!card_thumb){
			alert('请上传证件照片');
			return false;
		}
		var url = '/aspirin/submit-auth';
		var data = {'user_position':user_position,'card_number':card_number,'card_thumb':card_thumb};
		$.post(url,data,function(msg){
			if(msg.success){
				$('#certification_box').hide();
				$('#cover').show();
            	$('#submit_certification_box').show();
			}else{
				alert('请重新提交');
				return;
			}
		  },'json')
	}
</script>