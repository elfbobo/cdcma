@extends('front.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<style>
.detail_box .pic_box video {display: inline-block;}
.detail_box img{max-width: 95%;}
</style>
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/aspirin">专项基金</a>
    >
    <a href="/aspirin/info-list">阿司匹林</a>
    >
    详细页
</div>
<div class="p_center_box fund_page">
	<div class="left">
    	<ul>
            <li><a href="/aspirin/index"><span style="background-position:left 4px center;" class="fund_icon1">专项基金</span></a></li>
            <li><a href="/aspirin/info-list" class="sele"><span class="fund_icon2_2">阿司匹林</span></a></li>
            <li><a href="/aspirinonline/ppt/1"><span class="fund_icon3">在线会议</span></a></li>
            <li><a href="/aspirinresearch/brief"><span class="fund_icon4">科研培训</span></a></li>
        </ul>
    </div>
    <div class="right world_heart">
    	@if(count($oInfo))
        <div class="detail_box">
            <div class="detail_title">{{$oInfo->info_title}}</div>
            @if(!empty($oInfo->info_subtitle))
            <div class="detail_subtitle">——{{$oInfo->info_subtitle}}</div>
            @endif
            @if(!empty($oInfo->info_video))
	            <div class="pic_box"><video src="{{$oInfo->info_video}}" poster="{{$oInfo->info_video_thumb}}" controls style="width:562px;height:320px;"></video></div>
	        @endif
	        <p class="text-indent">{{$oInfo->info_content}}</p>
        </div>
        @endif
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop