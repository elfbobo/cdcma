@extends('front.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/aspirin">专项基金</a>
    >
    <a href="/aspirin/info-list">阿司匹林</a>
    >
    列表页
</div>
<div class="p_center_box fund_page">
	<div class="left">
    	<ul>
            <li><a href="/aspirin/index"><span style="background-position:left 4px center;" class="fund_icon1">专项基金</span></a></li>
            <li><a href="/aspirin/info-list" class="sele"><span class="fund_icon2_2">阿司匹林</span></a></li>
            <li><a href="/aspirinonline/ppt/1"><span class="fund_icon3">在线会议</span></a></li>
            <li><a href="/aspirinresearch/brief"><span class="fund_icon4">科研培训</span></a></li>
        </ul>
    </div>
    <div class="right world_heart">
        <div class="list_box">
        	@foreach($oInfo as $k=>$v)
            <div class="list">
                <div class="list-pic">
                	<a href="/aspirin/info-show/{{$v->id}}"><img src="{{$v->info_thumb}}" alt=""></a>
                </div>
                <div class="list-item">
                	<a href="/aspirin/info-show/{{$v->id}}">
                    	<div class="list-item-title">{{$v->info_title}}</div>
                    </a>
                    <div class="list-item-intro">{{substr($v->created_at,0,10)}}</div>
                </div>
            </div>
            @endforeach
            <div class="p_number_box">
	        {{$oInfo->links()}}
		    </div>
        </div>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop