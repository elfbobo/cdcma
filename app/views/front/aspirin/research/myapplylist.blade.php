@extends('front.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
@include('front.aspirin.certification')
<div class="p_sml_menu new_menu">
    您当前位置：
   <a href="/">首页</a>
    >
    <a href="/aspirin">专项基金</a>
    >
    科研培训
</div>
<div class="p_center_box fund_page">
	<div class="left">
    	<ul>
            <li><a href="/aspirin/index"><span style="background-position:left 4px center;" class="fund_icon1">专项基金</span></a></li>
            <li><a href="/aspirin/info-list"><span class="fund_icon2">阿司匹林</span></a></li>
            <li><a href="/aspirinonline/ppt/1"><span class="fund_icon3">在线会议</span></a></li>
            <li><a href="/aspirinresearch/brief" class="sele"><span class="fund_icon4_4">科研培训</span></a></li>
        </ul>
    </div>
    <div class="right fund_intro science">
    	<div class="top_banner"><img src="/assets/images/front/web/fund_intro_bg.png" alt=""></div>
        <div class="risk_tool_tab">
            <div onclick="window.location.href='/aspirinresearch/brief';">科研培训简介</div>
            <div class="sele" onclick="window.location.href='/aspirinresearch/my-apply-list';">科研培训</div>
            <div class="none" onclick="alert('暂未公示');">结果公示</div>
            <!--  
            <div class="none" onclick="window.location.href='/aspirinresearch/result';">结果公示</div>
        	-->
        </div>
        <div class="scientfic_research">
        	@if($oUser->role_id==3 && $oUser->card_auth_flag == 1)
        	<button class="btn" onclick="alert('医师认证审核通过以后方可参与');">申请科研</button>
        	@else
            <button class="btn" onclick="window.location.href='/aspirinresearch/apply';">申请科研</button>
            @endif
        </div>
        <div class="list-box">
        	@foreach($oApply as $k=>$v)
            <a href="/aspirinresearch/my-apply-show/{{$v->id}}">
                <div class="list-item">
                    <span class="list-item-time">{{substr($v->created_at,0,10)}}</span>
                    <span class="list-item-name">{{$v->apply_name}}</span>
                    @if($v->apply_type == 2)
                    <span class="list-item-state"><span style="color:red;border:1px solid red;">未通过审核</span></span>
                    @elseif($v->apply_type == 1)
                    <span class="list-item-state"><span class="success">审核通过</span></span>
                    @else
                    <span class="list-item-state"><span>审核中</span></span>
                    @endif
                </div>
            </a>
            @endforeach
        </div>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop