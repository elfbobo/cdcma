@extends('front.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/aspirin">专项基金</a>
    >
    科研申请
</div>
<div class="p_center_box fund_page">
	<div class="left">
    	<ul>
            <li><a href="/aspirin/index"><span style="background-position:left 4px center;" class="fund_icon1">专项基金</span></a></li>
            <li><a href="/aspirin/info-list"><span class="fund_icon2">阿司匹林</span></a></li>
            <li><a href="/aspirinonline/speaker-list/1"><span class="fund_icon3">在线会议</span></a></li>
            <li><a href="/aspirinresearch/brief" class="sele"><span class="fund_icon4_4">科研培训</span></a></li>
        </ul>
    </div>
    <div class="right fund_intro science">
    	<div class="top_banner"><img src="/assets/images/front/web/fund_intro_bg.png" alt=""></div>
        <div class="risk_tool_tab">
            <div onclick="window.location.href='/aspirinresearch/brief';">科研培训简介</div>
            <div class="sele" onclick="window.location.href='/aspirinresearch/my-apply-list';">科研培训</div>
            <div class="none" onclick="alert('暂未公示');">结果公示</div>
            <!--  
            <div class="none" onclick="window.location.href='/aspirinresearch/result';">结果公示</div>
        	-->
        </div>
        <div class="detail_content">
            <div class="item-box">
                <div class="item">
                    <div class="item-tit">姓名：</div>
                    <div class="input_box">
                        <input type="text" id="apply_name" name="apply_name">
                    </div>
                </div>
                <div class="item">
                    <div class="item-tit">城市：</div>
                    <div class="input_box">
                        <input type="text" id="apply_city" name="apply_city">
                    </div>
                </div>
                <div class="item">
                    <div class="item-tit">出生日期：</div>
                    <div class="input_box">
                        <input type="text" id="apply_birthday" name="apply_birthday">
                    </div>
                </div>
                <div class="item">
                    <div class="item-tit">职称：</div>
                    <div class="input_box">
                        <input type="text" id="apply_title" name="apply_title">
                    </div>
                </div>
                <div class="item">
                    <div class="item-tit">联系电话：</div>
                    <div class="input_box">
                        <input type="text" id="apply_tel" name="apply_tel">
                    </div>
                </div>
                <div class="item">
                    <div class="item-tit">电子邮箱：</div>
                    <div class="input_box">
                        <input type="text" id="apply_email" name="apply_email">
                    </div>
                </div>
                <div class="item">
                    <div class="item-tit">单位名称：</div>
                    <div class="input_box">
                        <input type="text" id="apply_company" name="apply_company">
                    </div>
                </div><div class="item">
                    <div class="item-tit">地址：</div>
                <div class="input_box">
                    <input type="text" id="apply_address" name="apply_address">
                </div>
                </div>
                <div class="item">
                    <div class="item-tit">邮编：</div>
                    <div class="input_box">
                        <input type="text" id="apply_postcode" name="apply_postcode">
                    </div>
                </div>
                <div class="item width_694">
                    <div class="item-tit">教育背景：</div>
                    <div class="item-msg">
                        <textarea name="education_background" id="education_background"></textarea>
                    </div>
                </div>
                <div class="item width_694">
                    <div class="item-tit">工作经历：</div>
                    <div class="item-msg">
                        <textarea name="work_experience" id="work_experience"></textarea>
                    </div>
                </div>
                <div class="item width_694">
                    <div class="item-tit">既往科研经历：</div>
                    <div class="item-msg">
                        <textarea name="research_experience" id="research_experience"></textarea>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="item-md">
                	<div class="item-tit">您可以选择下列两个方向中的任意一个 做科研设想及申请：</div>
                    <div class="item-tit">方向一（心血管疾病一级预防）： 心血管疾病的一级预防主要是通过对不良生活方式的干预（运动、饮食、戒烟酒）、高危人群抗血小板治疗、综合管理血脂、血糖及血压来减少心血管疾病的发生，在这些措施中，您认为哪个（些）措施在实施过程中尚存在需要进一步解决的问题？主要的问题是什么？需要相应采取什么措施来解决？</div>
                    <div class="item-msg">
                        <textarea name="question1" id="question1"></textarea>
                    </div>
                </div>
                <div class="item-md">
                    <div class="item-tit">方向二（心血管疾病二级预防）： 心血管疾病二级预防的诸多措施，如生活方式干预、规范的抗血小板治疗、综合管理血脂、血糖和血压，来降低心血管疾病的复发及死亡，在这些措施中，您认为哪个（些）措施在实施过程中尚存在进一步解决的问题？主要的问题是什么？需要相应采取什么措施来解决？</div>
                    <div class="item-msg">
                        <textarea name="question2" id="question2"></textarea>
                    </div>
                    <div class="item-tip">书写要求（请结合您认为需要解决的问题，简要陈述科研设想的背景、科研假说、科研目的，以及预期要得出的结论）</div>
                </div>
                <div class="item-md">
                    <div class="item-tit">针对您提到的关于心血管预防的问题，陈述相应的解决方案？
                        （请简述拟采用的科研方法/目标人群/干预方法/主要观察指标/次要观察指标等）</div>
                    <div class="item-msg">
                        <textarea name="question3" id="question3"></textarea>
                    </div>
                </div>
                <div class="btn_box">
                @if($oUser->card_auth_flag == 2 ||$oUser->role_id == 2)
                    <button class="btn" onclick="submit_apply();">提交申请</button>
	            @else
	            	<button class="btn" onclick="alert('医师认证审核通过后方可参与');">提交申请</button>
	            @endif
                </div>
            </div>
        </div>
    </div>
	<div class="clear"></div>
</div>
<script>
function submit_apply(){
	var apply_name = $.trim($('#apply_name').val());
	var apply_city = $.trim($('#apply_city').val());
	var apply_birthday = $.trim($('#apply_birthday').val());
	var apply_title = $.trim($('#apply_title').val());
	var apply_tel = $.trim($('#apply_tel').val());
	var apply_email = $.trim($('#apply_email').val());
	var apply_company = $.trim($('#apply_company').val());
	var apply_address = $.trim($('#apply_address').val());
	var apply_postcode = $.trim($('#apply_postcode').val());
	var education_background = $.trim($('#education_background').val());
	var work_experience = $.trim($('#work_experience').val());
	var research_experience = $.trim($('#research_experience').val());
	var question1 = $.trim($('#question1').val());
	var question2 = $.trim($('#question2').val());
	var question3 = $.trim($('#question3').val());
	if(!apply_name){
		alert('请填写姓名');
		return false;
	}
	if(!apply_city){
		alert('请填写城市');
		return false;
	}
	if(!apply_birthday){
		alert('请填写出生日期');
		return false;
	}
	if(!apply_title){
		alert('请填写职称');
		return false;
	}
	if(!apply_tel){
		alert('请填写联系电话');
		return false;
	}else{
		if(!(/^1[34578]\d{9}$/).test(apply_tel)){
			alert('手机格式不正确！');return false;
		}
	}
	if(!apply_email){
		alert('请填写电子邮箱');
		return false;
	}else{
		if(!(/^(\w-*\.*)+@(\w-?)+(\.\w{2,})+$/).test(apply_email)){
			alert('邮箱格式不正确！');return false;
		}
	}
	if(!apply_company){
		alert('请填写单位名称');
		return false;
	}
	if(!apply_address){
		alert('请填写地址');
		return false;
	}
	if(!apply_postcode){
		alert('请填写邮编');
		return false;
	}
	if(!education_background){
		alert('请填写教育背景');
		return false;
	}
	if(!work_experience){
		alert('请填写工作经历');
		return false;
	}
	if(!research_experience){
		alert('请填写既往科研经历');
		return false;
	}
	if(!question1 && !question2){
		alert('请选填任一方向的信息');
		return false;
	}
	if(!question3){
		alert('请陈述您的解决方案');
		return false;
	}
	var url = '/aspirinresearch/submit-apply';
	var data = {
			'apply_name':apply_name,
			'apply_city':apply_city,
			'apply_birthday':apply_birthday,
			'apply_title':apply_title,
			'apply_tel':apply_tel,
			'apply_email':apply_email,
			'apply_company':apply_company,
			'apply_address':apply_address,
			'apply_postcode':apply_postcode,
			'education_background':education_background,
			'work_experience':work_experience,
			'research_experience':research_experience,
			'question1':question1,
			'question2':question2,
			'question3':question3,
	};
	$.post(url,data,function(msg){
		if(msg == 'success'){
			alert('提交资料已在审核中,客服人员会持续跟进');
			window.location.href='/aspirinresearch/my-apply-list';
			return;
		}else{
			alert('提交失败，请重试');
			return;
		}
	})
}
</script>
@include('front.common.contact')
@stop