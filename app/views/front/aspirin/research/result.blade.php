@extends('front.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/aspirin">专项基金</a>
    >
    科研结果公示
</div>
<div class="p_center_box fund_page">
	<div class="left">
    	<ul>
            <li><a href="/aspirin/index"><span style="background-position:left 4px center;" class="fund_icon1">专项基金</span></a></li>
            <li><a href="/aspirin/info-list"><span class="fund_icon2">阿司匹林</span></a></li>
            <li><a href="/aspirinonline/ppt/1"><span class="fund_icon3">在线会议</span></a></li>
            <li><a href="/aspirinresearch/brief" class="sele"><span class="fund_icon4_4">科研培训</span></a></li>
        </ul>
    </div>
    <div class="right fund_intro science">
    	<div class="top_banner"><img src="/assets/images/front/web/fund_intro_bg.png" alt=""></div>
        <div class="risk_tool_tab">
            <div onclick="window.location.href='/aspirinresearch/brief';">科研培训简介</div>
            <div onclick="window.location.href='/aspirinresearch/my-apply-list';">科研培训</div>
            <div class="sele" onclick="window.location.href='/aspirinresearch/result';">结果公示</div>
        </div>
        <div class="announcement">
        	@if($iFlag == 1)
            <div class="announcement_title">恭喜您入围科研项目</div>
            @else
            <div class="announcement_title"></div>
            @endif
            <div class="btn_box">
                <button class="btn" onclick="window.location.href='/aspirinresearch/result-list';">查看入围名单</button>
            </div>
        </div>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop