@extends('front.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/aspirin">专项基金</a>
    >
    科研结果公示
</div>
<div class="p_center_box fund_page">
	<div class="left">
    	<ul>
            <li><a href="/aspirin/index"><span style="background-position:left 4px center;" class="fund_icon1">专项基金</span></a></li>
            <li><a href="/aspirin/info-list"><span class="fund_icon2">阿司匹林</span></a></li>
            <li><a href="/aspirinonline/ppt/1"><span class="fund_icon3">在线会议</span></a></li>
            <li><a href="/aspirinresearch/brief" class="sele"><span class="fund_icon4_4">科研培训</span></a></li>
        </ul>
    </div>
    <div class="right fund_intro science">
    	<div class="top_banner"><img src="/assets/images/front/web/fund_intro_bg.png" alt=""></div>
        <div class="risk_tool_tab">
            <div onclick="window.location.href='/aspirinresearch/brief';">科研培训简介</div>
            <div onclick="window.location.href='/aspirinresearch/my-apply-list';">科研培训</div>
            <div class="sele" onclick="window.location.href='/aspirinresearch/result';">结果公示</div>
        </div>
    	<div class="detail_content">
            <div class="item-box">
                <div class="item">
                    <div class="item-tit">姓名：</div>
                    <div class="item-msg">{{$oApply->apply_name}}</div>
                </div>
                <div class="item">
                    <div class="item-tit">城市：</div>
                    <div class="item-msg">{{$oApply->apply_city}}</div>
                </div>
                <div class="item">
                    <div class="item-tit">出生日期：</div>
                    <div class="item-msg">{{$oApply->apply_birthday}}</div>
                </div>
                <div class="item">
                    <div class="item-tit">职称：</div>
                    <div class="item-msg">{{$oApply->apply_title}}</div>
                </div>
                <div class="item">
                    <div class="item-tit">联系电话：</div>
                    <div class="item-msg">{{$oApply->apply_tel}}</div>
                </div>
                <div class="item">
                    <div class="item-tit">电子邮箱：</div>
                    <div class="item-msg">{{$oApply->apply_email}}</div>
                </div>
                <div class="item">
                    <div class="item-tit">单位名称：</div>
                    <div class="item-msg">{{$oApply->apply_company}}</div>
                </div><div class="item">
                    <div class="item-tit">地址：</div>
                    <div class="item-msg">{{$oApply->apply_address}}</div>
                </div>
                <div class="item">
                    <div class="item-tit">邮编：</div>
                    <div class="item-msg">{{$oApply->apply_postcode}}</div>
                </div>
                <div class="item">
                    <div class="item-tit">教育背景：</div>
                    <div class="item-msg">{{$oApply->education_background}}</div>
                </div>
                <div class="item">
                    <div class="item-tit">工作经历：</div>
                    <div class="item-msg">{{$oApply->work_experience}}</div>
                </div>
                <div class="item">
                    <div class="item-tit">既往科研经历：</div>
                    <div class="item-msg">{{$oApply->research_experience}}</div>
                </div>
                <div class="clear"></div>
                
                <div class="item-md">
                	<div class="item-tit">您可以选择下列两个方向中的任意一个 做科研设想及申请：</div>
                    <div class="item-tit">方向一（心血管疾病一级预防）： 心血管疾病的一级预防主要是通过对不良生活方式的干预（运动、饮食、戒烟酒）、高危人群抗血小板治疗、综合管理血脂、血糖及血压来减少心血管疾病的发生，在这些措施中，您认为哪个（些）措施在实施过程中尚存在需要进一步解决的问题？主要的问题是什么？需要相应采取什么措施来解决？</div>
                    <div class="item-msg">{{$oApply->question1}}</div>
                </div>
                <div class="item-md">
                    <div class="item-tit">方向二（心血管疾病二级预防）： 心血管疾病二级预防的诸多措施，如生活方式干预、规范的抗血小板治疗、综合管理血脂、血糖和血压，来降低心血管疾病的复发及死亡，在这些措施中，您认为哪个（些）措施在实施过程中尚存在进一步解决的问题？主要的问题是什么？需要相应采取什么措施来解决？</div>
                    <div class="item-msg">{{$oApply->question2}}</div>
                    <div class="item-tip">书写要求（请结合您认为需要解决的问题，简要陈述科研设想的背景、科研假说、科研目的，以及预期要得出的结论）</div>
                </div>
                <div class="item-md">
                    <div class="item-tit">针对您提到的关于心血管预防的问题，陈述相应的解决方案？
                        （请简述拟采用的科研方法/目标人群/干预方法/主要观察指标/次要观察指标等）</div>
                    <div class="item-msg">{{$oApply->question3}}</div>
                </div>
            </div>
        </div>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop