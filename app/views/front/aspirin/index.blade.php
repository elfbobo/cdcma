@extends('front.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<!-- @include('front.aspirin.certification') -->
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/aspirin">专项基金</a>
    >
    专项基金简介
</div>
<div class="p_center_box fund_page">
	<div class="left">
    	<ul>
            <li><a href="/aspirin/index" class="sele"><span style="background-position:left 4px center;" class="fund_icon1_1">专项基金</span></a></li>
            <li><a href="/aspirin/info-list"><span class="fund_icon2">阿司匹林</span></a></li>
            <li><a href="/aspirinonline/ppt/1"><span class="fund_icon3">在线会议</span></a></li>
            <li><a href="/aspirinresearch/brief"><span class="fund_icon4">科研培训</span></a></li>
        </ul>
    </div>
    <div class="right fund_intro">
    	<div class="top_banner"><img src="/assets/images/front/web/fund_intro_bg.png" alt=""></div>
        <div class="item-title bg-color1">背景介绍</div>
        <div class="item-intro">
            <p class="mar-btm_30">中国心血管健康联盟：中国心血管健康联盟由葛均波院士和霍勇教授倡议成立。联盟成立的主要任务是在在政府领导下建立中国心血管防治战线，实现中国心血管事件的拐点下降早日到来，从而达到每个中国人的心血管健康的愿景。</p>
            <p class="mar-btm_30">国际阿司匹林基金会:国际阿司匹林基金会成立40余年,由6名来自不同领域（心脑血管，消化，肿瘤，药学，流行病学等）的专家组成，每年定期召开委员组会议，每年设立不同的学术奖项用来嘉奖在阿司匹林研究中做出卓越贡献的研究人员。</p>
            <p class="">阿司匹林专项基金:基于以上背景，在葛均波院士和霍勇教授的倡导下，由联盟成立阿司匹林专项基金。目的一是增加医生对心血管疾病预防理念的理解及重视，增强医生对阿司匹林在心血管预防中的认识及规范使用；二是加强我国心血管医生与国际上的合作与沟通，提高临床医生在心血管预防领域的科研工作能力。</p>
        </div>
        <div class="item-title bg-color2">框架介绍</div>
        <div class="item-intro">
            <p>阿司匹林专项基金项目包括两部分内容：医学教育及科研培训。</p>
            <p><span class="font_bold">医学教育：</span>围绕心血管疾病一、二级预防的最新进展，采取全新的投票互动等形式，开展学术会议</p>
            <p><span class="font_bold">科研培训：</span></p>
            <p><span class="font_bold">项目特色：</span>与权威专家面对面结合实例的科研技能培训，一对一量身定做的英文文章撰写能力培训</p>
            <p><span class="font_bold">参与方式：</span>通过在线填写申请，由专家组进行评选，最终选择20位优秀候选人参加培训。</p>
        </div>
        <div class="item-title bg-color3">创始人简介</div>
        <div class="expert_list_box">
            <div class="list">
                <div class="list-pic"><img src="/assets/images/front/web/expert_img1.png" alt=""></div>
                <div class="list-item">
                    <div class="list-item-title">葛均波  院士</div>
                    <div class="list-item-intro">中国科学院院士。现为同济大学副校长，复旦大学附属中山医院教授，主任医师，博士生导师，心导管室主任，心内科主任，上海市心血管病研究所所长，长江学者计划特聘教授。 2000年加入九三学社，任九三学社第十二届中央委员会委员，上海市第十五届委员会常委。第十一届全国政协委员。上海市第九届青联常委。</div>
                </div>
            </div>
            <div class="list">
                <div class="list-pic"><img src="/assets/images/front/web/expert_img2.png" alt=""></div>
                <div class="list-item">
                    <div class="list-item-title">霍勇  院士</div>
                    <div class="list-item-intro">北京大学第一医院教授，主任医师，博士生导师，现任北京大学第一医院心内科及心脏中心主任，北京大学第一临床医学院学位分会委员、《中国介入心脏病学杂志》主编、《中国医学前沿杂志》主编、中华医学会心血管介入治疗培训中心主任、中华医学会心血管病学分会主任委员、中国医师协会心血管内科医师分会会长，卫生部医政司心血管疾病介入诊疗技术管理专家工作组组长，民进中央委员、民进中央科技医卫委员会副主任、民进市委常委、西城医卫支部主任、西城区政协委员等。</div>
                </div>
            </div>
            <div class="share_box">
                <div class="bdsharebuttonbox">
<!--                 	<a href="#" class="bds_more" data-cmd="more"></a> -->
                 	<a href="#" class="bds_weixin" data-cmd="weixin" title="分享到微信"></a>
                	<a href="#" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博"></a>
                </div>
			    <script>
				    var url = 'http://cdcma.bizconf.cn/aspirinshare/info-brief';
				    window._bd_share_config={
						   "common":{"bdSnsKey":{},"bdText":"","bdUrl":url,"bdMini":"3","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"24"},
						   "share":{},
						   "selectShare":{"bdContainerClass":null,"bdSelectMiniList":["weixin","tsina"]}
				    };
				    with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];
			    </script>
            </div>
        </div>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop