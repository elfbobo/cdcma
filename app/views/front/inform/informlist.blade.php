@extends('front.common.layout')
@section('title')资讯中心-国卫健康云 @stop
@section('description')资讯中心-国卫健康云@stop
@section('keywords')资讯中心-国卫健康云 @stop
@section('content2')
 <script type ="text/javascript">
 function addHots(id,urls){
		 
		var url = "/information/add/"+id;
		$.post(url,{},function(msg){
			if(msg=='success'){
				window.location.href=urls;
			}
		})
	}
</script>
    <div class="p_center_box">
        <div class="left">
            <ul class="wid156">
                <li style="display:none;"><a href="javascript:alert('开发中')">专家介绍</a></li>
                <li style="display:none;"><a href="javascript:alert('开发中')">期刊阅读</a></li>
                <li style="display:none;"><a href="javascript:alert('开发中')">订阅</a></li>
                <li><a class="sele" href="javascript:void(0)">拜耳医汇</a></li>
                <li style="display:none;"><a href="javascript:alert('开发中')">学科分类</a></li>
                <li><a href="/information/list/1">慢性疾病与转化医学</a></li>
                <li><a href="/information/list/2">心内空间</a></li>
                <li><a href="/information/list/3">呼吸科空间</a></li>
            </ul>
        </div>
        <div class="right infor_box">
        @foreach($oAllInformation as  $value)
	        	<div class="infor_line clearfix">
	            	<div class="infor_pic_block">
	                	<a href="javascript:void(0)"  onclick="addHots({{$value->id}},'{{$value->info_url}}')">
	                    	<img class="infor_pic" src="{{$value->info_thumb}}" />
	                    </a>
	                </div>
	                <div class="infor_txt_block">
	                	<div class="infor_link_block">
	                	  
	                    	<a class="infor_link"  href="javascript:void(0)" onclick="addHots({{$value->id}},'{{$value->info_url}}')">{{$value->info_title}}</a>
	                    </div>
	                    <div class="">
	                    	{{date("Y-m-d",strtotime(strval($value->created_at)))}}
	                    </div>
	                </div>
	                <div class="clear"> </div>
	            </div>
        @endforeach 
         <div class="p_number_box">
         {{$oAllInformation->links()}}
            </div>   
        </div>
        <div class="clear"></div>
    </div>
@stop