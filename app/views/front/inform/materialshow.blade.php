@extends('front.common.layout')
@section('title')资讯中心-国卫健康云 @stop
@section('description')资讯中心-国卫健康云@stop
@section('keywords')资讯中心-国卫健康云 @stop
@section('content2')
<div class="p_center_box">
        <div class="left">
            <ul class="wid156">
                <li><a href="/information">拜耳医汇</a></li>
                <li><a @if($oMaterialNews->news_type == 1) class="sele" @else @endif href="/information/list/1">慢性疾病与转化医学</a></li>
                <li><a @if($oMaterialNews->news_type == 2) class="sele" @else @endif href="/information/list/2">心内空间</a></li>
                <li><a @if($oMaterialNews->news_type == 3) class="sele" @else @endif href="/information/list/3">呼吸科空间</a></li>
            </ul>
		</div>
        <div class="right wid767_2">
        	<div class="meeting_detail_box">
            	<div class="meeting_detail_tit">
                	{{$oMaterialNews->title}}
                </div>
                <div class="meeting_detail_time">
                	时间：{{substr($oMaterialNews->created_at,0,10)}}
                </div>
                <div class="meeting_detail_txt">
                	{{$sNewContent}}
                </div>
            </div>
    	</div>
        <div class="clear"></div>
</div>
@stop