@extends('front.common.layout')
@section('title')资讯中心-国卫健康云 @stop
@section('description')资讯中心-国卫健康云@stop
@section('keywords')资讯中心-国卫健康云 @stop
@section('content2')
<div class="p_center_box">
	<div class="left">
    	<ul class="wid156">
        	<li><a href="/information">拜耳医汇</a></li>
            <li><a @if($type == 1) class="sele" @else @endif href="/information/list/1">慢性疾病与转化医学</a></li>
            <li><a @if($type == 2) class="sele" @else @endif href="/information/list/2">心内空间</a></li>
            <li><a @if($type == 3) class="sele" @else @endif href="/information/list/3">呼吸科空间</a></li>
    	</ul>
	</div>
	<div class="right infor_box">
        @foreach($oMaterialNews as $k=>$v)
	        <div class="infor_line clearfix">
	            <div class="infor_pic_block">
	                <a href="/information/show/{{$v->id}}">
	                    <img class="infor_pic" src="{{$v->path}}" />
	                </a>
	            </div>
	            <div class="infor_txt_block">
	                <div class="infor_link_block">
	                    <a class="infor_link"  href="/information/show/{{$v->id}}">{{$v->title}}</a>
	                </div>
	                <div class="">
	                    {{substr($v->created_at,0,10)}}
	                </div>
	            </div>
	            <div class="clear"> </div>
	       </div>
        @endforeach 
        <div class="p_number_box">
        {{$oMaterialNews->links()}}
        </div>   
	</div>
	<div class="clear"></div>
</div>
@stop