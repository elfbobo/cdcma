@extends('front.common.layoutm')
@section('title')国卫健康云@stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云@stop
@section('content3')
<?php
//微信扫一扫参与签到调研
$isWxFlag = user_agent_is_weixin();
$wx_config = array();
if ($isWxFlag){
	include_once  app_path().'/include/jssdk.class.php';  //先把类包含进来
	$oJssdk = new jssdk();
	$wx_config = $oJssdk->getSignPackage();
}
?>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
@if($isWxFlag)
wx.config({
	debug:false,
	appId: "{{$wx_config['appId']}}",//必填
	timestamp:{{$wx_config['timestamp']}}, // 必填，生成签名的时间戳
	nonceStr: "{{$wx_config['nonceStr']}}", // 必填，生成签名的随机串
	signature: "{{$wx_config['signature']}}",// 必填，签名
	jsApiList: ['scanQRCode']// 必填，需要使用的JS接口列表，所有JS接口列表见附录2
});

	function saoyisao(){
		wx.scanQRCode({
		    needResult: 0, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果，
		    scanType: ["qrCode","barCode"], // 可以指定扫二维码还是一维码，默认二者都有
		    success: function (res) {
		    var result = res.resultStr; // 当needResult 为 1 时，扫码返回的结果
		}
		});
	}
@else
	function saoyisao(){
		alert('请在微信浏览器使用');
	}
	@endif
</script>
<input type="hidden" id="meeting_code" value="{{$iMeetingCode}}">
<div class="phone_page">
	<div class="page_top">
    	<p>输入手机号</p>
    	<input class="page_top_left" type="button" style="visibility: hidden;"/>
    </div>
    <div class="page_content p40">
    	<div class="phone_text question_tit">非常感谢您的参与，请输入您的手机号码！</div>
        <div class="pt30 pb30" id="enter_box"><input class="phone_input" type="text" placeholder="请输入手机号码" id="sign_name"/></div>
    </div>
    <div class="btn_box">
    	<input type="button" class="btn5" value="提交" onclick="offline_sign_submit();"/>
    </div>
</div>
<div id="sign_success" style="display:none;">
	<div class="cover"></div>
	<div class="pop_box">
		<div class="pop_txt" id="sign_content">
		    签到成功
	    </div>
	    <div class="btn_box">
	    	<input type="button" class="btn5" value="确定" onclick="$('#sign_success').hide();window.location.reload();"/>
	    </div>
	</div>
</div>
<div id="mobile_nouser" style="display:none;">
		<div class="cover"></div>
		<div class="pop_box">
			<div class="pop_txt">
			    未注册
		    </div>
		    <div class="btn_box">
		    	<input type="button" class="btn5" value="确定" onclick="window.location.href='/docface/survey-registernewuser/TN5MYA/{{$iMeetingCode}}'"/>
		    </div>
		</div>
	</div>
<script type="text/javascript">
function offline_sign_submit(){
	var meeting_code = $.trim($('#meeting_code').val());
	var sign_name = $.trim($('#sign_name').val());
	var reg_mobile = /^1[34578][0-9]{9}$/;
	istel= reg_mobile.test(sign_name);
	if (!istel ) {
	    alert("手机号码格式不正确！");
	    return false;
	}
	var url = '/docface/offline-sign-do';
	var data = {sign_name:sign_name,meeting_code:meeting_code};
	$.post(url,data,function(msg){
		if(msg == 'success'){
			$('#sign_content').html('签到成功');
			$('#sign_success').show();
			return;
		}else{
			var mobile_nouser = document.getElementById("mobile_nouser");
			mobile_nouser.style.display ="block";
// 			var str = '</br>'+
// 				'<span style="font-size:14px;color:red;">您输入的手机号码还未注册，请重新输入已注册的手机号码'+
// 				'</br><span style="color:blue;" onclick="saoyisao();">点击此处扫描医学顾问专属二维码注册</span>'+
// 				'</span>';
// 			$('#enter_box').append(str);
			return;
		}
	})
}
</script>
@stop

