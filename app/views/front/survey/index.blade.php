@extends('front.common.layoutm')
@section('title'){{$sVideoTitle}} @stop
@section('description'){{$sVideoTitle}}@stop
@section('keywords'){{$sVideoTitle}}@stop
@section('content3')
<script type="text/javascript">

	
    function submit_survey(flg){
        if(flg==1){
            box_show('您已参与该调研，无需重复参加，谢谢！');return;
        }
    	$.ajax({
            cache: true,
            type: "POST",
            url:'/docface/survey/{{$iVid}}',
            data:$('#survey_form').serialize(),// 你的formid
            async: false,
            error: function(request) {
                box_show("Connection error");
            },
            success: function(data) {
                if(data=='repeat'){
                    box_show('您已参与该调研，无需重复参加，谢谢！');
                }else if(data=='noanswer'){
                    box_show('请选择您的答案后提交，谢谢！');
                }else{
	                if(data=='success'){
						//
						box_show('参与调研成功');
	                }else{
						box_show('参与调研失败，请刷新页面后重新参与，谢谢！');
	                }
                }
            }
        });
    }
</script>
@if(count($oQuestions))
<div class="phone_page">
	<div class="page_top">
    	<p>课程调研</p>
    	<input class="page_top_left" type="button" />
    </div>
   <div class="page_content">
		<form method="post" id="survey_form">
		<input type="hidden" name="survey_count" id="survey_count" value="{{count($oQuestions)}}"/>
			
			@foreach($oQuestions as $k=>$v)
			<div class="survey_text">
	            <p class="question_tit">{{$k+1}}、{{$v->title}}(@if($v['type']==1) 单选 @else 多选  @endif)</p>
	            <ul>
	            	@foreach($v->options as $key=>$val)
	            	@if($v['type']==1)
	                <li>
	                    <input type="radio" class="s_radio"  name="surveys[{{$v->id}}]" value="{{$val->option}}"/>{{$val->option}}、{{$val->option_title}}
	                </li>
	                @else
	                <li>
	                    <input type="checkbox" class="s_checkbox" name="surveys[{{$v->id}}][]" value="{{$val->option}}"/>{{$val->option}}、{{$val->option_title}}
	                </li>
	                @endif
	                @endforeach
	            </ul>
	        </div>
	        <div class="border"></div>
			@endforeach
			<div class="btn_box">
	        	<input type="button" class="btn5" value="提交" onclick="submit_survey(0)"/>
	        </div> 
		</form>
    </div>
</div>
@else
<div class="phone_page">
	<div class="page_top">
    	<p>拜耳医汇</p>
    	<input class="page_top_left" type="button" />
    </div>
    <div class="erweima_box">
    	<img class="wid100" src="/assets/images/mobile/erweima.jpg" />
    </div>
    <div class="page_content p40">
    	<div class="phone_text question_tit center">随时随地了解最新资讯<br />长按识别二维码，关注“拜耳医汇”微信客户端</div>
    </div>
    <div class="btn_box" style="display:none;">
    	<input type="button" class="btn5" value="关闭" />
    </div>
</div>
@endif
@stop

