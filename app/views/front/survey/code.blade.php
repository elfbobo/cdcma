@extends('front.common.layoutm')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content3')
<script>
function submit_code(){
	var register_code = $.trim($('#register_code').val());
	if(!register_code){
		box_show('请输入您的邀请码');return;
	}
	//根据邀请码获取用户id
	var url = '/register-code/'+register_code;
	$.post(url,{},function(msg){
		if(msg=='no_code'){
			box_show('该用户编码不存在，请核对您的用户编码是否正确！');
		}else{
			location.href='/docface/survey-register/'+register_code;
		}
	})
}
</script>
<div class="phone_page">
	<div class="page_top">
    	<p>注册</p>
    	<input class="page_top_left" type="button" onclick="window.location.href='/docface/survey/{{Session::get('survey_video_id')}}'"/>
    </div>
    <div class="page_content p40">
    	<div class="phone_text question_tit">请您输入邀请人的用户编码：</div>
        <div class="pt30 pb30"><input id="register_code" name="register_code" class="phone_input" type="text" placeholder="请您输入邀请人的用户编码" /></div>
        <div>
            <input type="button" class="btn5 left" value="提交" onclick="submit_code();"/>
            <input type="button" class="btn5 right" value="取消" onclick="window.history.go(-1);"/>
            <div class="clear"></div>
        </div>
        <div class="hint_box">
	    	<span class="red">温馨提示：</span>如果您无邀请码，请联系您的医学顾问。
   		</div>    
    </div>    
</div>


@stop