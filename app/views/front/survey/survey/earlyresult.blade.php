@extends('front.survey.survey.layout')
@section('title')早期达标之我见 @stop
@section('description')早期达标之我见@stop
@section('keywords')早期达标之我见@stop
@section('content')
<body onload="init()" onresize="init()">
	<div class="logo">
    	<img src="/assets/images/survey/logo.png" />
    </div>
    <div class="container" id="bg">
        <div class="survey_box">
            <h4>早期达标之我见</h4>
            <div class="ques_one">
                <p>
                    <span class="order">Q1.</span> 
                    <span class="info">您觉得血压早期达标的含义包括什么?（可多选）</span>
                </p>	
                <div class="survey_result">
                    <div class="progress">
                        <div @if(!empty($aChoose[1])&&stristr($aChoose[1],'A')) style="color:red;" @endif>A</div>
                        <div class="bar green"><span style="width:{{!empty($aResult[1]['A'])?$aResult[1]['A']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[1]['A'])?$aResult[1]['A']:0}}%</div>
                    </div>
                    <div class="progress">
                        <div @if(!empty($aChoose[1])&&stristr($aChoose[1],'B')) style="color:red;" @endif>B</div>
                        <div class="bar red"><span style="width:{{!empty($aResult[1]['B'])?$aResult[1]['B']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[1]['B'])?$aResult[1]['B']:0}}%</div>
                    </div>
                </div>
            </div>
            <div class="ques_one">
                <p>
                    <span class="order">Q2.</span> 
                    <span class="info">您认为在患者可耐受的前提下，一般高血压患者应该在什么时间内降压达标？</span>
                </p>	
                <div class="survey_result">
                    <div class="progress">
                        <div  @if(!empty($aChoose[2])&&stristr($aChoose[2],'A')) style="color:red;" @endif>A</div>
                        <div class="bar green"><span style="width:{{!empty($aResult[2]['A'])?$aResult[2]['A']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[2]['A'])?$aResult[2]['A']:0}}%</div>
                    </div>
                    <div class="progress">
                        <div @if(!empty($aChoose[2])&&stristr($aChoose[2],'B')) style="color:red;" @endif>B</div>
                        <div class="bar red"><span style="width:{{!empty($aResult[2]['B'])?$aResult[2]['B']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[2]['B'])?$aResult[2]['B']:0}}%</div>
                    </div>
                    <div class="progress">
                        <div @if(!empty($aChoose[2])&&stristr($aChoose[2],'C')) style="color:red;" @endif>C</div>
                        <div class="bar yellow"><span style="width:{{!empty($aResult[2]['C'])?$aResult[2]['C']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[2]['C'])?$aResult[2]['C']:0}}%</div>
                    </div>
                    <div class="progress">
                        <div @if(!empty($aChoose[2])&&stristr($aChoose[2],'D')) style="color:red;" @endif>D</div>
                        <div class="bar blue"><span style="width:{{!empty($aResult[2]['D'])?$aResult[2]['D']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[2]['D'])?$aResult[2]['D']:0}}%</div>
                    </div>
                </div>
            </div>
            <div class="ques_one">
                <p>
                    <span class="order">Q3.</span> 
                    <span class="info">您认为尽早血压达标有哪些临床意义？（可多选）</span>
                </p>	
                <div class="survey_result">
                    <div class="progress">
                        <div @if(!empty($aChoose[3])&&stristr($aChoose[3],'A')) style="color:red;" @endif>A</div>
                        <div class="bar green"><span style="width:{{!empty($aResult[3]['A'])?$aResult[3]['A']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[3]['A'])?$aResult[3]['A']:0}}%</div>
                    </div>
                    <div class="progress">
                        <div @if(!empty($aChoose[3])&&stristr($aChoose[3],'B')) style="color:red;" @endif>B</div>
                        <div class="bar red"><span style="width:{{!empty($aResult[3]['B'])?$aResult[3]['B']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[3]['B'])?$aResult[3]['B']:0}}%</div>
                    </div>
                    <div class="progress">
                        <div @if(!empty($aChoose[3])&&stristr($aChoose[3],'C')) style="color:red;" @endif>C</div>
                        <div class="bar yellow"><span style="width:{{!empty($aResult[3]['C'])?$aResult[3]['C']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[3]['C'])?$aResult[3]['C']:0}}%</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="btm">
    	<img src="/assets/images/survey/btm1.png" />
    </div>
</body>
@stop