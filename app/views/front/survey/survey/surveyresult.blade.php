@extends('front.survey.survey.layout')
@section('title'){{$oResearchPeriod ->research_title }} @stop
@section('description'){{$oResearchPeriod ->research_title }}@stop
@section('keywords'){{$oResearchPeriod ->research_title }}@stop
@section('content')
<body onload="init()" onresize="init()">
	<div class="logo">
    	<img src="/assets/images/survey/logo.png" />
    </div>
    <div class="container" id="bg">
        <div class="survey_box" style="z-index:99">
            <h4>{{$oResearchPeriod ->research_title }}</h4>
          @foreach($oResearchPeriodQuestion as $v)
            @if($v->research_type == 1)
		            <div class="ques_one">
		                <p>
		                    <span class="order">Q{{$aList[$v->id]}}.</span> 
		                    <span class="info">{{$v->research_title}}</span>
		                </p>	
		                <div class="survey_result">
		                @if(!empty($v->research_result))
		                  <div class="progress">
		                                                           正确答案{{$v->research_result}}
		                  </div>
		                @endif
		                @if(empty($apicture[$v->id]))
				                   @if(isset($arr[$v->id]['A']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'A')) style="color:red;" @endif>A</div>
				                        <div class="bar green"><span style="width:{{!empty($aResult[$v->id]['A'])?$aResult[$v->id]['A']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['A'])?$aResult[$v->id]['A']:0}}%</div>
				                    </div>
				                   @endif
				                      @if(isset($arr[$v->id]['B']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'B')) style="color:red;" @endif>B</div>
				                        <div class="bar red"><span style="width:{{!empty($aResult[$v->id]['B'])?$aResult[$v->id]['B']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['B'])?$aResult[$v->id]['B']:0}}%</div>
				                    </div>
				                     @endif
				                      @if(isset($arr[$v->id]['C']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'C')) style="color:red;" @endif>C</div>
				                        <div class="bar yellow"><span style="width:{{!empty($aResult[$v->id]['C'])?$aResult[$v->id]['C']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['C'])?$aResult[$v->id]['C']:0}}%</div>
				                    </div>
				                     @endif
				                      @if(isset($arr[$v->id]['D']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'D')) style="color:red;" @endif>D</div>
				                        <div class="bar blue"><span style="width:{{!empty($aResult[$v->id]['D'])?$aResult[$v->id]['D']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['D'])?$aResult[$v->id]['D']:0}}%</div>
				                    </div>
				                     @endif
				                      @if(isset($arr[$v->id]['E']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'E')) style="color:red;" @endif>E</div>
				                        <div class="bar blue"><span style="width:{{!empty($aResult[$v->id]['E'])?$aResult[$v->id]['E']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['E'])?$aResult[$v->id]['E']:0}}%</div>
				                    </div>
				                     @endif
				                      @if(isset($arr[$v->id]['F']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'F')) style="color:red;" @endif>F</div>
				                        <div class="bar blue"><span style="width:{{!empty($aResult[$v->id]['F'])?$aResult[$v->id]['F']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['F'])?$aResult[$v->id]['F']:0}}%</div>
				                    </div>
				                     @endif
				                      @if(isset($arr[$v->id]['G']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'G')) style="color:red;" @endif>G</div>
				                        <div class="bar blue"><span style="width:{{!empty($aResult[$v->id]['G'])?$aResult[$v->id]['G']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['G'])?$aResult[$v->id]['G']:0}}%</div>
				                    </div>
				                     @endif
				                      @if(isset($arr[$v->id]['H']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'H')) style="color:red;" @endif>H</div>
				                        <div class="bar blue"><span style="width:{{!empty($aResult[$v->id]['H'])?$aResult[$v->id]['H']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['H'])?$aResult[$v->id]['H']:0}}%</div>
				                    </div>
				                     @endif
				                      @if(isset($arr[$v->id]['I']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'I')) style="color:red;" @endif>I</div>
				                        <div class="bar blue"><span style="width:{{!empty($aResult[$v->id]['I'])?$aResult[$v->id]['I']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['I'])?$aResult[$v->id]['I']:0}}%</div>
				                    </div>
				                     @endif
				                      @if(isset($arr[$v->id]['J']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'J')) style="color:red;" @endif>J</div>
				                        <div class="bar blue"><span style="width:{{!empty($aResult[$v->id]['J'])?$aResult[$v->id]['J']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['J'])?$aResult[$v->id]['J']:0}}%</div>
				                    </div>
				                     @endif
		                      
		                 @else
		                          <img src="{{$apicture[$v->id]}}" />
		                 @endif
		                </div>
		            </div>
          @else
           
		            <div class="ques_one">
		                <p>
		                    <span class="order">Q{{$aList[$v->id]}}.</span> 
		                    <span class="info">{{$v->research_title}}（可多选）</span>
		                </p>
		                <div class="survey_result">
		                  @if(!empty($v->research_result))
		                  <div class="progress">
		                                                           正确答案{{$v->research_result}}
		                  </div>
		                 @endif
		                 @if(empty($apicture[$v->id]))
				                   @if(isset($arr[$v->id]['A']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'A')) style="color:red;" @endif>A</div>
				                        <div class="bar green"><span style="width:{{!empty($aResult[$v->id]['A'])?$aResult[$v->id]['A']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['A'])?$aResult[$v->id]['A']:0}}%</div>
				                    </div>
				                   @endif
				                    @if(isset($arr[$v->id]['B']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'B')) style="color:red;" @endif>B</div>
				                        <div class="bar red"><span style="width:{{!empty($aResult[$v->id]['B'])?$aResult[$v->id]['B']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['B'])?$aResult[$v->id]['B']:0}}%</div>
				                    </div>
				                     @endif
				                     @if(isset($arr[$v->id]['C']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'C')) style="color:red;" @endif>C</div>
				                        <div class="bar yellow"><span style="width:{{!empty($aResult[$v->id]['C'])?$aResult[$v->id]['C']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['C'])?$aResult[$v->id]['C']:0}}%</div>
				                    </div>
				                     @endif
				                     @if(isset($arr[$v->id]['D']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'D')) style="color:red;" @endif>D</div>
				                        <div class="bar blue"><span style="width:{{!empty($aResult[$v->id]['D'])?$aResult[$v->id]['D']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['D'])?$aResult[$v->id]['D']:0}}%</div>
				                    </div>
				                     @endif
				                      @if(isset($arr[$v->id]['E']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'E')) style="color:red;" @endif>E</div>
				                        <div class="bar blue"><span style="width:{{!empty($aResult[$v->id]['E'])?$aResult[$v->id]['E']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['E'])?$aResult[$v->id]['E']:0}}%</div>
				                    </div>
				                     @endif
				                      @if(isset($arr[$v->id]['F']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'F')) style="color:red;" @endif>F</div>
				                        <div class="bar blue"><span style="width:{{!empty($aResult[$v->id]['F'])?$aResult[$v->id]['F']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['F'])?$aResult[$v->id]['F']:0}}%</div>
				                    </div>
				                     @endif
				                      @if(isset($arr[$v->id]['G']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'G')) style="color:red;" @endif>G</div>
				                        <div class="bar blue"><span style="width:{{!empty($aResult[$v->id]['G'])?$aResult[$v->id]['G']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['G'])?$aResult[$v->id]['G']:0}}%</div>
				                    </div>
				                     @endif
				                      @if(isset($arr[$v->id]['H']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'H')) style="color:red;" @endif>H</div>
				                        <div class="bar blue"><span style="width:{{!empty($aResult[$v->id]['H'])?$aResult[$v->id]['H']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['H'])?$aResult[$v->id]['H']:0}}%</div>
				                    </div>
				                     @endif
				                      @if(isset($arr[$v->id]['I']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'I')) style="color:red;" @endif>I</div>
				                        <div class="bar blue"><span style="width:{{!empty($aResult[$v->id]['I'])?$aResult[$v->id]['I']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['I'])?$aResult[$v->id]['I']:0}}%</div>
				                    </div>
				                     @endif
				                      @if(isset($arr[$v->id]['J']))
				                    <div class="progress">
				                        <div @if(!empty($aChoose[$v->id])&&stristr($aChoose[$v->id],'J')) style="color:red;" @endif>J</div>
				                        <div class="bar blue"><span style="width:{{!empty($aResult[$v->id]['J'])?$aResult[$v->id]['J']:1}}%;"></span></div>
				                        <div class="num">{{!empty($aResult[$v->id]['J'])?$aResult[$v->id]['J']:0}}%</div>
				                    </div>
				                     @endif
		                   @else
		                          <img src="{{$apicture[$v->id]}}" />
		                   @endif
		                </div>
		            </div>
            @endif
            @endforeach
            
<!--            <div class="ques_one">-->
<!--                <p>-->
<!--                    <span class="order">Q4.</span> -->
<!--                    <span class="info">您觉得哪种剂型单次给药就能够达到24小时平稳血药浓度？<br/>正确答案C</span>-->
<!--                </p>	-->
<!--                <div class="survey_result">-->
<!--                    <img src="/assets/images/survey/chart.jpg" />-->
<!--                </div>-->
<!--            </div>-->
        </div>
    </div>
    <div class="btm">
       <img src="{{$oResearchPeriod -> research_imges}}" class="img"/>
    	<img src="/assets/images/survey/btm.png"/>
    </div>
</body>
@stop