@extends('front.survey.survey.layout')
@section('title'){{$oResearchPeriod->research_title}} @stop
@section('description'){{$oResearchPeriod->research_title}}@stop
@section('keywords'){{$oResearchPeriod->research_title}}@stop
@section('content')
<body onload="init()" onresize="init()">
	<div class="logo">
    	<img src="/assets/images/survey/logo.png" />
    </div>
    <div class="container">
        <div class="survey_box" style="z-index:99">
        	<form id="survey_form" action="/survey/submits/{{$oResearchPeriod->id}}" method="post">
            <h4>{{$oResearchPeriod->research_title}}</h4>
          @foreach($oResearchPeriodQuestion as $v) 
	           @if($v->research_type == 1)
	            <div class="ques_one">
	                <p>
	                    <span class="order">Q{{$aList[$v->id]}}.</span> 
	                    <span class="info">{{$v->research_title}} </span>
	                </p>
	                <div class="survey_result">
	                      @if(isset($arr[$v->id]['A']))
	                        <div>
                              <input name="survey[{{$v->id}}]" type="radio" value="{{$arr[$v->id]['A']}}"/>A. {{$aContent[$v->id]['A']}}
                           </div>
	                      @endif
	                      @if(isset($arr[$v->id]['B']))
	                       <div>
	                        <input name="survey[{{$v->id}}]" type="radio" value="{{$arr[$v->id]['B']}}"/>B.  {{$aContent[$v->id]['B']}}
	                       </div>
	                      @endif
	                      @if(isset($arr[$v->id]['C']))
	                        <div>
	                        <input name="survey[{{$v->id}}]" type="radio" value="{{$arr[$v->id]['C']}}"/>C.  {{$aContent[$v->id]['C']}}
	                        </div>
	                      @endif
	                      @if(isset($arr[$v->id]['D']))
	                         <div>
	                          <input name="survey[{{$v->id}}]" type="radio" value="{{$arr[$v->id]['D']}}"/>D.  {{$aContent[$v->id]['D']}}
	                          </div>
	                      @endif
	                      @if(isset($arr[$v->id]['E']))
	                        <div>
	                        <input name="survey[{{$v->id}}]" type="radio" value="{{$arr[$v->id]['E']}}"/>E.  {{$aContent[$v->id]['E']}}
	                         </div>
	                      @endif
	                      @if(isset($arr[$v->id]['F']))
	                         <div>
	                        <input name="survey[{{$v->id}}]" type="radio" value="{{$arr[$v->id]['F']}}"/>F.  {{$aContent[$v->id]['F']}}
	                         </div>
	                      @endif
	                      @if(isset($arr[$v->id]['G']))
	                         <div>
	                        <input name="survey[{{$v->id}}]" type="radio" value="{{$arr[$v->id]['G']}}"/>G.  {{$aContent[$v->id]['G']}}
	                         </div>
	                      @endif
	                      @if(isset($arr[$v->id]['H']))
	                         <div>
	                        <input name="survey[{{$v->id}}]" type="radio" value="{{$arr[$v->id]['H']}}"/>H.  {{$aContent[$v->id]['H']}}
	                         </div>
	                      @endif
	                      @if(isset($arr[$v->id]['I']))
	                         <div>
	                        <input name="survey[{{$v->id}}]" type="radio" value="{{$arr[$v->id]['I']}}"/>F.  {{$aContent[$v->id]['I']}}
	                         </div>
	                      @endif
	                 
	                </div>
	            </div>
	            @else
	              <div class="ques_one">
	                <p>
	                    <span class="order">Q{{$aList[$v->id]}}.</span> 
	                    <span class="info">{{$v->research_title}}（可多选）</span>
	                </p>
	                <div class="survey_result">
	                      @if(isset($arr[$v->id]['A']))
	                        <div>
                              <input name="survey[{{$v->id}}]" type="checkbox" value="{{$arr[$v->id]['A']}}"/>A. {{$aContent[$v->id]['A']}}
                           </div>
	                      @endif
	                      @if(isset($arr[$v->id]['B']))
	                       <div>
	                        <input name="survey[{{$v->id}}]" type="checkbox" value="{{$arr[$v->id]['B']}}"/>B.  {{$aContent[$v->id]['B']}}
	                       </div>
	                      @endif
	                      @if(isset($arr[$v->id]['C']))
	                        <div>
	                        <input name="survey[{{$v->id}}]" type="checkbox" value="{{$arr[$v->id]['C']}}"/>C.  {{$aContent[$v->id]['C']}}
	                        </div>
	                      @endif
	                      @if(isset($arr[$v->id]['D']))
	                         <div>
	                          <input name="survey[{{$v->id}}]" type="checkbox" value="{{$arr[$v->id]['D']}}"/>D.  {{$aContent[$v->id]['D']}}
	                          </div>
	                      @endif
	                      @if(isset($arr[$v->id]['E']))
	                        <div>
	                        <input name="survey[{{$v->id}}]" type="checkbox" value="{{$arr[$v->id]['E']}}"/>E.  {{$aContent[$v->id]['E']}}
	                         </div>
	                      @endif
	                      @if(isset($arr[$v->id]['F']))
	                         <div>
	                        <input name="survey[{{$v->id}}]" type="checkbox" value="{{$arr[$v->id]['F']}}"/>F.  {{$aContent[$v->id]['F']}}
	                         </div>
	                      @endif  
	                      @if(isset($arr[$v->id]['G']))
	                         <div>
	                        <input name="survey[{{$v->id}}]" type="checkbox" value="{{$arr[$v->id]['G']}}"/>F.  {{$aContent[$v->id]['G']}}
	                         </div>
	                      @endif  
	                      @if(isset($arr[$v->id]['H']))
	                         <div>
	                        <input name="survey[{{$v->id}}]" type="checkbox" value="{{$arr[$v->id]['H']}}"/>F.  {{$aContent[$v->id]['H']}}
	                         </div>
	                      @endif  
	                      @if(isset($arr[$v->id]['I']))
	                         <div>
	                        <input name="survey[{{$v->id}}]" type="checkbox" value="{{$arr[$v->id]['I']}}"/>F.  {{$aContent[$v->id]['I']}}
	                         </div>
	                      @endif  
	                     
	                </div>
	            </div>
	            @endif
          
           @endforeach 
            <div class="search_box">
                <div><img src="/assets/images/survey/search.png" /></div>
                <div><input type="button" class="btn" value="提交，查看结果" onclick="submit_survey()"/></div>
            </div>
            </form>
        </div>
    </div>
     <div class="btm">
    	<img src="/assets/images/survey/btm.png" />
    	<img src="{{$oResearchPeriod->research_imges}}" class="img"/>
    </div>
</body>
@stop