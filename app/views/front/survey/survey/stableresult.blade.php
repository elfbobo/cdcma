@extends('front.survey.survey.layout')
@section('title')平稳达标之我见 @stop
@section('description')平稳达标之我见@stop
@section('keywords')平稳达标之我见@stop
@section('content')
<body onload="init()" onresize="init()">
	<div class="logo">
    	<img src="/assets/images/survey/logo.png" />
    </div>
    <div class="container" id="bg">
        <div class="survey_box">
            <h4>平稳达标之我见</h4>
            <div class="ques_one">
                <p>
                    <span class="order">Q1.</span> 
                    <span class="info">血压管理以下几个指标，1 降压达标，2 平稳降压，3 器官保护，您认为的重要性排序是？</span>
                </p>	
                <div class="survey_result">
                    <div class="progress">
                        <div @if(!empty($aChoose[1])&&stristr($aChoose[1],'A')) style="color:red;" @endif>A</div>
                        <div class="bar green"><span style="width:{{!empty($aResult[1]['A'])?$aResult[1]['A']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[1]['A'])?$aResult[1]['A']:0}}%</div>
                    </div>
                    <div class="progress">
                        <div @if(!empty($aChoose[1])&&stristr($aChoose[1],'B')) style="color:red;" @endif>B</div>
                        <div class="bar red"><span style="width:{{!empty($aResult[1]['B'])?$aResult[1]['B']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[1]['B'])?$aResult[1]['B']:0}}%</div>
                    </div>
                    <div class="progress">
                        <div @if(!empty($aChoose[1])&&stristr($aChoose[1],'C')) style="color:red;" @endif>C</div>
                        <div class="bar yellow"><span style="width:{{!empty($aResult[1]['C'])?$aResult[1]['C']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[1]['C'])?$aResult[1]['C']:0}}%</div>
                    </div>
                    <div class="progress">
                        <div @if(!empty($aChoose[1])&&stristr($aChoose[1],'D')) style="color:red;" @endif>D</div>
                        <div class="bar blue"><span style="width:{{!empty($aResult[1]['D'])?$aResult[1]['D']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[1]['D'])?$aResult[1]['D']:0}}%</div>
                    </div>
                </div>
            </div>
            <div class="ques_one">
                <p>
                    <span class="order">Q2.</span> 
                    <span class="info">您觉得平稳降压能够带来哪些获益？（可多选）</span>
                </p>	
                <div class="survey_result">
                    <div class="progress">
                        <div @if(!empty($aChoose[2])&&stristr($aChoose[2],'A')) style="color:red;" @endif>A</div>
                        <div class="bar green"><span style="width:{{!empty($aResult[2]['A'])?$aResult[2]['A']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[2]['A'])?$aResult[2]['A']:0}}%</div>
                    </div>
                    <div class="progress">
                        <div @if(!empty($aChoose[2])&&stristr($aChoose[2],'B')) style="color:red;" @endif>B</div>
                        <div class="bar red"><span style="width:{{!empty($aResult[2]['B'])?$aResult[2]['B']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[2]['B'])?$aResult[2]['B']:0}}%</div>
                    </div>
                    <div class="progress">
                        <div @if(!empty($aChoose[2])&&stristr($aChoose[2],'C')) style="color:red;" @endif>C</div>
                        <div class="bar yellow"><span style="width:{{!empty($aResult[2]['C'])?$aResult[2]['C']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[2]['C'])?$aResult[2]['C']:0}}%</div>
                    </div>
                    <div class="progress">
                        <div @if(!empty($aChoose[2])&&stristr($aChoose[2],'D')) style="color:red;" @endif>D</div>
                        <div class="bar blue"><span style="width:{{!empty($aResult[2]['D'])?$aResult[2]['D']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[2]['D'])?$aResult[2]['D']:0}}%</div>
                    </div>
                </div>
            </div>
            <div class="ques_one">
                <p>
                    <span class="order">Q3.</span> 
                    <span class="info">您觉得高品质的控释剂型能否带来24小时平稳降压?</span>
                </p>	
                <div class="survey_result">
                    <div class="progress">
                        <div @if(!empty($aChoose[3])&&stristr($aChoose[3],'A')) style="color:red;" @endif>A</div>
                        <div class="bar green"><span style="width:{{!empty($aResult[3]['A'])?$aResult[3]['A']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[3]['A'])?$aResult[3]['A']:0}}%</div>
                    </div>
                    <div class="progress">
                        <div @if(!empty($aChoose[3])&&stristr($aChoose[3],'B')) style="color:red;" @endif>B</div>
                        <div class="bar red"><span style="width:{{!empty($aResult[3]['B'])?$aResult[3]['B']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[3]['B'])?$aResult[3]['B']:0}}%</div>
                    </div>
                    <div class="progress">
                        <div @if(!empty($aChoose[3])&&stristr($aChoose[3],'C')) style="color:red;" @endif>C</div>
                        <div class="bar yellow"><span style="width:{{!empty($aResult[3]['C'])?$aResult[3]['C']:1}}%;"></span></div>
                        <div class="num">{{!empty($aResult[3]['C'])?$aResult[3]['C']:0}}%</div>
                    </div>
                </div>
            </div>
            <div class="ques_one">
                <p>
                    <span class="order">Q4.</span> 
                    <span class="info">您觉得哪种剂型单次给药就能够达到24小时平稳血药浓度？<br/>正确答案C</span>
                </p>	
                <div class="survey_result">
                    <img src="/assets/images/survey/chart.jpg" />
                </div>
            </div>
        </div>
    </div>
    <div class="btm">
    	<img src="/assets/images/survey/btm2.png" />
    </div>
</body>
@stop