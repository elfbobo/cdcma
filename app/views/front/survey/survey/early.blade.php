@extends('front.survey.survey.layout')
@section('title')早期达标观点小调研@stop
@section('description')早期达标观点小调研@stop
@section('keywords')早期达标观点小调研@stop
@section('content')
<body onload="init()" onresize="init()">
	<div class="logo">
    	<img src="/assets/images/survey/logo.png" />
    </div>
    <div class="container" id="bg">
        <div class="survey_box" style="z-index:99">
        	<form id="survey_form" action="/survey/submit/2" method="post">
            <h4>早期达标观点小调研</h4>
            <div class="ques_one">
                <p>
                    <span class="order">Q1.</span> 
                    <span class="info">您觉得血压早期达标的含义包括什么?（可多选）</span>
                </p>
                <div class="survey_result">
                    <div>
                        <input name="survey[1]" type="checkbox" value="A" />A. 尽早启动降压治疗
                    </div>
                    <div>
                        <input name="survey[1]" type="checkbox" value="B" />B. 尽早血压达标
                    </div>
                </div>
            </div>
            <div class="ques_one">
                <p>
                    <span class="order">Q2.</span> 
                    <span class="info">您认为在患者可耐受的前提下，一般高血压患者应该在什么时间内降压达标？</span>
                </p>
                <div class="survey_result">
                    <div>
                        <input name="survey[2]" type="radio" value="A" />A. 1-2周
                    </div>
                    <div>
                        <input name="survey[2]" type="radio" value="B" />B. 2-4周
                    </div>
                    <div>
                        <input name="survey[2]" type="radio" value="C" />C. 1-2个月 
                    </div>
                    <div>
                        <input name="survey[2]" type="radio" value="D" />D. 2-3个月
                    </div>
                </div>
            </div>
            <div class="ques_one">
                <p>
                    <span class="order">Q3.</span> 
                    <span class="info">您认为尽早血压达标有哪些临床意义？（可多选）</span>
                </p>
                <div class="survey_result">
                    <div>
                        <input name="survey[3]" type="checkbox" value="A" />A. 尽早启动血管保护
                    </div>
                    <div>
                        <input name="survey[3]" type="checkbox" value="B" />B. 提高治疗依从性
                    </div>
                    <div>
                        <input name="survey[3]" type="checkbox" value="C" />C. 降低临床事件 
                    </div>
                </div>
            </div>
            <div class="search_box">
                <div><img src="/assets/images/survey/search.png" /></div>
                <div><input type="button" class="btn" value="提交，查看结果" onclick="submit_survey()"/></div>
            </div>
            </form>
        </div>
    </div>
    <div class="btm">
    	<img src="/assets/images/survey/btm1.png" />
    </div>
</body>
@stop