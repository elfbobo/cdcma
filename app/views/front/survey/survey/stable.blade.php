@extends('front.survey.survey.layout')
@section('title')平稳达标之我见 @stop
@section('description')平稳达标之我见@stop
@section('keywords')平稳达标之我见@stop
@section('content')
<body onload="init()" onresize="init()">
	<div class="logo">
    	<img src="/assets/images/survey/logo.png" />
    </div>
    <div class="container">
        <div class="survey_box" style="z-index:99">
        	<form id="survey_form" action="/survey/submit/1" method="post">
            <h4>平稳达标之我见</h4>
            <div class="ques_one">
                <p>
                    <span class="order">Q1.</span> 
                    <span class="info">血压管理以下几个指标，1 降压达标，2 平稳降压，3 器官保护您认为的重要性排序是？</span>
                </p>
                <div class="survey_result">
                    <div>
                        <input name="survey[1]" type="radio" value="A"/>A. 1，2，3
                    </div>
                    <div>
                        <input name="survey[1]" type="radio" value="B"/>B. 1，3，2
                    </div>
                    <div>
                        <input name="survey[1]" type="radio" value="C"/>C. 2，1，3 
                    </div>
                    <div>
                        <input name="survey[1]" type="radio" value="D"/>D. 2，3，1
                    </div>
                </div>
            </div>
            <div class="ques_one">
                <p>
                    <span class="order">Q2.</span> 
                    <span class="info">您觉得平稳降压能够带来哪些获益？（可多选）</span>
                </p>
                <div class="survey_result">
                    <div>
                        <input name="survey[2]" type="checkbox" value="A"/>A. 不良事件降低
                    </div>
                    <div>
                        <input name="survey[2]" type="checkbox" value="B"/>B. 冠心病事件降低
                    </div>
                    <div>
                        <input name="survey[2]" type="checkbox" value="C"/>C. 卒中事件降低
                    </div>
                    <div>
                        <input name="survey[2]" type="checkbox" value="D"/>D. 更好依从性
                    </div>
                </div>
            </div>
            <div class="ques_one">
                <p>
                    <span class="order">Q3.</span> 
                    <span class="info">您觉得高品质的控释剂型能否带来24小时平稳降压?</span>
                </p>
                <div class="survey_result">
                    <div>
                        <input name="survey[3]" type="radio" value="A"/>A. 能
                    </div>
                    <div>
                        <input name="survey[3]" type="radio" value="B"/>B. 不能
                    </div>
                    <div>
                        <input name="survey[3]" type="radio" value="C"/>C. 也许能 
                    </div>
                </div>
            </div>
            <div class="ques_one">
                <p>
                    <span class="order">Q4.</span> 
                    <span class="info">您觉得哪种剂型单次给药就能够达到24小时平稳血药浓度？</span>
                </p>
                <div class="survey_result">
                    <div>
                        <input name="survey[4]" type="radio" value="A"/>A. 分子长效普通片剂
                    </div>
                    <div>
                        <input name="survey[4]" type="radio" value="B"/>B. 高品质缓释剂型
                    </div>
                    <div>
                        <input name="survey[4]" type="radio" value="C"/>C. 高品质控释剂型 
                    </div>
                </div>
            </div>
            <div class="search_box">
                <div><img src="/assets/images/survey/search.png" /></div>
                <div><input type="button" class="btn" value="提交，查看结果" onclick="submit_survey()"/></div>
            </div>
            </form>
        </div>
    </div>
    <div class="btm">
    	<img src="/assets/images/survey/btm2.png" />
    </div>
</body>
@stop