@extends('front.common.layoutm')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content3')
<link rel="stylesheet" type="text/css" href="/assets/css/agreewith.css" />
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/jquery-1.8.3.min.js"></script>
<div class="phone_page">
	<div class="page_top">
    	<p>国卫健康云注册</p>
    	<input class="page_top_left" type="button" />
    </div>
    <div class="page_content">
    	<div class="page_tit">国卫健康云注册</div>
    	<form id="register_form"   method="post" action="/docface/survey-register/{{$code}}" onsubmit="return sub_reg();">
    	<div class="regist_text">
        	<div class="form"  style="z-index:100;">
            	<div class="login_txt">账号信息</div>
            	<div class="one_input">
                	<label><span class="red">*</span>用户名:</label>
                    <div class="the_input">
                		<input type="text" id="user_nick" name="user_nick" placeholder="建议使用真实姓名+数字"/>
                    </div>
                </div>
                <div class="one_input">
                	<label><span class="red">*</span>密　码:</label>
                    <div class="the_input">
                		<input type="password" id="password" name="password" placeholder="建议使用手机号码"/>
                    </div>
                </div>
                <div class="one_input">
                	<label style="width:76px;"><span class="red">*</span>确认密码:</label>
                    <div class="the_input">
                		<input type="password" id="password_r"/>
                    </div>
                </div>
                <div class="login_txt ptop10">个人信息</div>
                <div class="user_pic_box">
                	<img style="width:54px;height:54px;" id="thumb" name="thumb" src="/assets/images/mobile/pic.jpg" />
                    <input class="user_file" id="upload_file" name="upload_file"  type="file" onchange="saveThumb()" />                        
                    <input type="hidden" name="user_thumb" id="user_thumb" value="/assets/images/dafault/default_doc.jpg"/>
                </div>                
                <div class="one_input">
                	<label><span class="red">*</span>姓名：</label>
                    <div class="the_input">
                		<input type="text" id="user_name" name="user_name"/>
                    </div>
                </div>
                <div class="one_input">
                	<label><span class="red">*</span>性别：</label>
                    <div class="the_input">
                		<input type="radio" class="s_radio" name="user_sex" id="user_sex1" value="1" checked/>男
                        <input type="radio" class="s_radio mleft40" name="user_sex" id="user_sex2" value="2"/>女
                    </div>
                </div>
                <div class="one_input">
                	<label><span class="red">*</span>手机：</label>
                    <div class="the_input">
                		<input type="text" id="user_tel" name="user_tel"/>
                    </div>
                </div>
                <div class="one_input">
                	<label><span class="red">*</span>邮箱：</label>
                    <div class="the_input">
                		<input type="text" id="user_email" name="user_email"/>
                    </div>
                </div>
                <div class="one_input">
                	<label>地址：</label>
                    <div class="the_input">
                		<input type="text" id="user_address" name="user_address"/>
                    </div>
                </div>
                		<input type="hidden" id="user_province" name="user_province" value="0"/>
                        <input type="hidden" id="user_city" name="user_city" value="0"/> 
                        <input type="hidden" id="user_county" name="user_county" value="0"/>
                        <input type="hidden" id="user_company" name="user_company" value="0"/> 
                <!--<div class="one_input">
                	<label>城市：</label>
                    <div class="the_input">
                		<input type="text" id="user_city" name="user_city"/>
                    </div>
                </div>
                -->
                <?php 
                $aHosp = Hospital::getChildren();
                ?>
                <div class="one_input">
                	<label><span class="red">*</span>省份：</label>
                    <div class="the_input">
                    	<select id="hospital1" style="width:80%;margin-top:5px;margin-left:5%" onchange="change_hospital(2)">
						            <option value="0">=请选择=</option>
						            @foreach($aHosp as $k=>$v)
						            <option value="{{$k}}">{{$v}}</option>
						            @endforeach
						        </select>
                    </div>
                </div>
                 <div class="one_input">
                	<label><span class="red">*</span>城市：</label>
                    <div class="the_input">
		                        	<select id="hospital2" style="width:80%;margin-top:5px;margin-left:5%" onchange="change_hospital(3)">
						            <option value="0">=请选择=</option>
						            </select>
                    </div>
                </div>
                 <div class="one_input">
                	<label><span class="red">*</span>区/县：</label>
                    <div class="the_input">
                    	<select id="hospital3" style="width:80%;margin-top:5px;margin-left:5%" onchange="change_hospital(4)">
						            <option value="0">=请选择=</option>
						            </select>
                    </div>
                </div>
                 <div class="one_input">
                	<label><span class="red">*</span>医院：</label>
                    <div class="the_input">
                    	<select id="hospital4" style="width:80%;margin-top:5px;margin-left:5%" onchange="change_hospital(5)">
						            <option value="0">=请选择=</option>
						        </select>
                    </div>
                </div>
                <div style="margin-left:25%;">
                若没有，请<a style="cursor:pointer;color:#254797;" onclick="$('#user_company_div').show();">手动填写</a>
                </div>
	                        <div class="one_input" style="display:none;" id="user_company_div">
			                    <label>&nbsp;</label>
			                    <div class="the_input">
			                		<input type="text" id="user_company_name" name="user_company_name" value=""/>
			                    </div>
			                </div>
                <div class="one_input">
                <?php 
                        	$aDep = User::getDepartment();
                        ?>
                	<label><span class="red">*</span>科室：</label>
                    <div class="the_input">
                    	<select id="user_dep_select" style="width:80%;margin-top:5px;margin-left:5%">
					            <option value="0">=请选择=</option>
					            @foreach($aDep as $k=>$v)
					            <option value="{{$v}}">{{$v}}</option>
					            @endforeach
					    </select>
                		<input type="hidden" id="user_department" name="user_department"/>
                    </div>
                </div>
                <?php 
                	$aPos = User::getPosition();
                ?>
                <div class="one_input">
                	<label><span class="red">*</span>职称：</label>
                    <div class="the_input">
                    	<select id="user_pos_select" style="width:80%;margin-top:5px;margin-left:5%">
				            <option value="0">=请选择=</option>
				            @foreach($aPos as $k=>$v)
				            <option value="{{$v}}">{{$v}}</option>
				            @endforeach
					    </select>
					    <input type="hidden" id="user_position" name="user_position" value=""/>
                    </div>
                </div>
                 <div class="one_input">
                	<label style="font-size:14px ;"><span class="red"></span>医师证号：</label>
                    <div class="the_input">
                		<input type="text" id="card_number" name="card_number"/>
                    </div>
                </div>
                <div class="login_txt ptop10">上传医师证照</div>
                <div class="user_pic_box">
                	<img style="width:54px;height:54px;" id="thumb_certificate" name="thumb_certificate" src="/assets/images/mobile/pic.jpg" />
                    <input class="user_file" id="upload_file_certificate" name="upload_file_certificate"  type="file" onchange="saveThumbcertificate()" />                        
                    <input type="hidden" name="card_thumb" id="user_thumb_certificate" value=""/>
                </div> 
                 <div class="agree-deal clearfix">
	            <input type="checkbox" id="agreechange">
	            <label for="" onclick="displaayclause()">已阅读并同意声明条款</label>
	           </div>
                <!--<div class="one_input">
                	<label><span class="red">*</span>单位：</label>
                    <div class="the_input">
                		<input type="text" id="user_company" name="user_company"/>
                    </div>
                </div>
            --></div>
        </div>
        
        <div class="btn_box">
    		<input type="submit" class="btn5" value="提交" />
    	</div>
    	</form>
    </div>
</div>


<div class="cover" style="display:none;" id="mobile_cover_agree_caluese" style="z-index:999999;"></div>
    <div class="pop-up" style="display:none;top: 50%;" id="mobile_pop_up_caluese" style="z-index:999999;"> 
        <div class="head-top">个人信息披露知情同意</div>
        <div class="content">
            <p>本文件旨在向您告知有关国卫健康云在收集、使用和保护个人信息方面的政策，并且，一旦签署，您将允许主办方：《北京医卫健康公益基金会》使用您的个人信息。请仔细阅读本知情同意。</p>
            <h1>收集个人信息：</h1>
            <p>您已被告知，并且您已同意北京医卫健康公益基金会将收集和使用您所提供的如下个人信息，包括：① 您的姓名，② 您工作所在的医疗机构，③ 您的电子邮箱地址，④ 您的手机号码等相关个人信息。</p>
            <h1>使用个人信息：</h1>
            <p>基于您愿意与北京医卫健康公益基金会开展合作，以接受北京医卫健康公益基金会提供的科学信息和产品信息，并参加相关意见调查，北京医卫健康公益基金会将为以下目的使用您的个人信息：① 北京医卫健康公益基金会与您之间的学术交流；② 邀请您参加学术会议；③ 邀请您参加意见调查；④ 发送会议通知、文件或材料。</p>
            <h1>向第三方披露个人信息：</h1>
            <p>北京医卫健康公益基金会将向国内或国外的北京医卫健康公益基金会关联方或北京医卫健康公益基金会所委托的第三方披露您的个人信息，并且该等关联方或第三方将为本知情同意中所规定之目的使用您的个人信息。</p>
            <h1> 个人信息保护：</h1>
            <p>北京医卫健康公益基金会已制定了相关政策并采取了相关安全措施，以保护您的个人信息免受未经授权的访问、篡改、泄露、毁损或丢失。北京医卫健康公益基金会将与接收您个人信息的任何授权第三方订立书面合同，确保该第三方承担有关采取安全措施保护您个人信息的合同义务。</p>
        </div>
        <div class="foot-agree">
            <div class="checkbox">
                <input type="radio"  name="agree" id="agree" onclick="agreewiththecaluse()">
                <label for="">我同意</label>
            </div>
            <div class="checkbox">
                <input type="radio" onclick="hiddendiaplaayclause()" id="notagree">
                <label for="">我不同意</label>
            </div>
        </div>
 </div>
 <script>
        $('.pop-up').css({
            'height':$(window).height()*0.8,
            'margin-top':-$(window).height()*0.8*0.5
        })
         $('.pop-up .content').css('height',$('.pop-up').height()*0.8)
  </script>
	<script>
	function displaayclause()
	{
		var mobile_cover_agree_caluese = document.getElementById("mobile_cover_agree_caluese");
		var mobile_pop_up_caluese = document.getElementById("mobile_pop_up_caluese");
		mobile_cover_agree_caluese.style.display ="block";
		mobile_pop_up_caluese.style.display ="block";
	}
	function  hiddendiaplaayclause()
	{
		var mobile_cover_agree_caluese = document.getElementById("mobile_cover_agree_caluese");
		var mobile_pop_up_caluese = document.getElementById("mobile_pop_up_caluese");
		var notagree =  document.getElementById("notagree");
		var agree   = document.getElementById("agree");
		var agreechange  = document.getElementById("agreechange");
		notagree.checked =true;
		agree.checked = false;
		agreechange.checked =false;
		mobile_cover_agree_caluese.style.display ="none";
		mobile_pop_up_caluese.style.display ="none";
	}

	function agreewiththecaluse()
	{
		var mobile_cover_agree_caluese = document.getElementById("mobile_cover_agree_caluese");
		var mobile_pop_up_caluese = document.getElementById("mobile_pop_up_caluese");
		var notagree =  document.getElementById("notagree");
		var agree   = document.getElementById("agree");
		var agreechange  = document.getElementById("agreechange");
		notagree.checked =false;
		agree.checked = true;
		agreechange.checked =true;
		mobile_cover_agree_caluese.style.display ="none";
		mobile_pop_up_caluese.style.display ="none";
	}
	 function change_hospital(id){
         var pid = id-1;
     	var hospital = $("#hospital"+pid+"  option:selected").val();
     	console.log(hospital);
     	var html_null = '<option value="0">=请选择=</option>';
     	if(pid==1){
				$('#user_province').val(hospital);
				$("#hospital"+2).html(html_null);
				$("#hospital"+3).html(html_null);
				$("#hospital"+4).html(html_null);
         }else if(pid==2){
         	$('#user_city').val(hospital);
         	$("#hospital"+3).html(html_null);
				$("#hospital"+4).html(html_null);
         }else if(pid==3){
         	$('#user_county').val(hospital);
         	$("#hospital"+4).html(html_null);
         }else {
         	$('#user_company').val(hospital);
             return ;
         }
     	var url = '/user/hospital-child/'+hospital;
     	$.post(url,{},function(json){
     		data = json;
         	if(data=='noinfo'){
 				var html = '<option value="0">=请选择=</option>';
 				$("#hospital"+id).html(html);
 				
 			}else{
 				var bShow = false;
 				var html = '<option value="0">=请选择=</option>';
 				for(var elem in data){
 					bShow = true;
 					html += '<option value="' + elem + '">' + data[elem] + '</option>';
 				}
 				if(bShow){
 					$("#hospital"+id).html(html);
 				}
 			}	
 			},'json');
     }
	function sub_reg(){
		
		var user_name = $.trim($('#user_name').val());
		var user_tel = $.trim($('#user_tel').val());
		var user_email = $.trim($('#user_email').val());
		var user_address = $.trim($('#user_address').val());
		var user_address = $.trim($('#user_address').val());
		var user_city = $.trim($('#user_city').val());
		var user_department = $.trim($('#user_department').val());
		var user_company_name = $.trim($('#user_company_name').val());
		var user_nick = $.trim($('#user_nick').val());
		var password = $('#password').val();
		var password_r = $('#password_r').val();
		var user_sex = $("input[name='user_sex']:checked").val();
		var agreechange = document.getElementById("agreechange").checked;
		if(!agreechange){
			var mobile_cover_agree_caluese = document.getElementById("mobile_cover_agree_caluese");
			var mobile_pop_up_caluese = document.getElementById("mobile_pop_up_caluese");
			mobile_cover_agree_caluese.style.display ="block";
			mobile_pop_up_caluese.style.display ="block";
			return false;
		}
		if(!user_name){
			box_show('请输入您的姓名！');
			return false;
		}
		if(!user_tel){
			box_show('请输入您的手机号码！');
			return false;
		}
//		var reg_mobile=/^((\+?86)|(\(\+86\)))?(13[0123456789][0-9]{8}|15[0123456789][0-9]{8}|18[0123456789][0-9]{8}|147[0-9]{8}|1349[0-9]{7})$/;
//		istel= reg_mobile.test(user_tel);
//		  if (!istel ) {
//		    alert("手机号码格式不正确！");
//		    return false;
//		  }

		var reg_mobile = /^1[34578][0-9]{9}$/;
		istel= reg_mobile.test(user_tel);
		if (!istel ) {
		    alert("手机号码格式不正确！");
		    return false;
		  }
		if(!user_email){
			box_show('请输入您的邮箱！');
			return false;
		}
		var reg = /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
		  ismail= reg.test(user_email);
		  if (!ismail ) {
		    alert("邮箱格式不正确！");
		    return false;
		  }
//		if(!user_department){
//			box_show('请输入您的科室！');
//			return false;
//		}
//		if(!user_company){
//			box_show('请输入您的单位！');
//			return false;
//		}
		//医生职称必选
		var user_pos_select = $("#user_pos_select  option:selected").val();
		if(!user_pos_select||user_pos_select==0){
			alert('请选择您的职称！');
			return false;
		}
		$('#user_position').val(user_pos_select);
		  var user_dep_select = $("#user_dep_select  option:selected").val();
			if(!user_dep_select||user_dep_select==0){
				alert('请选择您的科室！');
				return false;
			}
			$('#user_department').val(user_dep_select);
			//省份、城市、区县、医院
			var user_province = $.trim($('#user_province').val());
			var user_city = $.trim($('#user_city').val());
			var user_county = $.trim($('#user_county').val());
			var user_company = $.trim($('#user_company').val());
			if(!user_province||user_province==0){
				alert('请选择您的省份！');
				return false;
			}
			if(!user_city||user_city==0){
				alert('请选择您的城市！');
				return false;
			}
			if(!user_county||user_county==0){
				alert('请选择您所在的区/县！');
				return false;
			}
			if(!user_company||user_company==0){
				if(!user_company_name){
					alert('请选择您的医院！');
					return false;
				}
			}
		if(!user_nick){
			box_show('请输入您的用户名！');
			return false;
		}
		if(!password){
			box_show('请输入您的密码！');
			return false;
		}
		if(!password_r){
			box_show('请重复输入密码！');
			return false;
		}
		if(password!=password_r){
			box_show('两次输入的密码不一致，请重新输入！');
			return false;
		}
		var url = '/user/register-check/'+user_nick+'/'+user_tel+'/'+user_email;
		$.post(url,{},function(msg){
			if(msg=='nick_exist'){
				//若该昵称存在
				box_show('该用户名已经被注册了，试试其他用户名吧！');
				return false;
			}else if(msg=='tel_exist'){
				box_show('该手机号码已经被注册了！');
				return false;
			}else if(msg=='email_exist'){
				box_show('该邮箱已经被注册了！');
				return false;
			}else{
				$('#register_form').attr('onsubmit','');
				$("#register_form").submit();
			}
		})
		return false;
	}
	function saveThumb() {
		$.ajaxFileUpload({
			url: "/user/upload-user-first-thumb",
			secureuri: false,
			fileElementId: "upload_file",
			dataType: "json",
			success: function(data, status) {
				$("#thumb").attr("src", data.user_thumb);
				$("#user_thumb").val( data.user_thumb);
			}
		})
	}
	function saveThumbcertificate() {
		$.ajaxFileUpload({
			url: "/user/upload-user-first-thumb_certificate",
			secureuri: false,
			fileElementId: "upload_file_certificate",
			dataType: "json",
			success: function(data, status) {
				$("#thumb_certificate").attr("src", data.user_thumb);
				$("#user_thumb_certificate").val( data.user_thumb);
			}
		})
	}
	</script>
@stop