@extends('front.common.layoutm')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content3')
<script>
function login_check(){
	var user_nick = $('#user_nick').val();
	var organizer_code = $('#organizer_code').val();
	if(!organizer_code){
		box_show('请填写正确的会议组织者用户编码。');
		return;
	}
	var url = "/login-mobile";
	$.post(url,{'user_nick':user_nick,'organizer_code':organizer_code},function(msg){
		if(msg=='success'){
			$('#mobile_success').show();
		}else if(msg=='repeat'){
			$('#mobile_success2').show();
		}else if(msg=='rep'){
			box_show('此签到为医生签到，代表无需签到。');
		}else if(msg=='code_err'){
			box_show('请填写正确的会议组织者用户编码。');
		}else if(msg=='live_sign_stop'){
			box_show('本期直播签到结束');
		}else{
			$('#mobile_nouser').show();
		}
	});
}
</script>
@if($iEndFlag==1)
<script type="text/javascript">
$(function(){
	box_show('本期直播签到结束');
})
</script>
@endif
	<div class="phone_page">
	<div class="page_top">
    	<p>签到</p>
    	<input class="page_top_left" type="button" onclick="history.go(-1);"/>
    </div>
    <div class="page_content p40">
    	<div class="phone_text question_tit">非常感谢您的参与，请输入以下信息进行签到！</div>
    	<div class="pt30 pb30"><input class="phone_input" id="organizer_code" name="organizer_code" type="text" placeholder="请输入会议组织者用户编码" /></div>
        <div class="pt30 pb30" style="padding-top:0px;"><input class="phone_input" id="user_nick" name="user_nick" type="text" placeholder="请输入用户名/手机号" /></div>
    </div>
    <div class="btn_box">
    	<input type="button" class="btn5" value="提交" onclick="login_check()" />
    </div>
	</div>
	
	<div id="mobile_success" style="display:none;">
		<div class="cover"></div>
		<div class="pop_box">
			<div class="pop_txt">
			    签到成功
		    </div>
		    <div class="btn_box">
		    	<input type="button" class="btn5" value="确定" onclick="window.location.reload()"/>
		    </div>
		</div>
	</div>
	
	<div id="mobile_nouser" style="display:none;">
		<div class="cover"></div>
		<div class="pop_box">
			<div class="pop_txt">
			    未注册
		    </div>
		    <div class="btn_box">
		    	<input type="button" class="btn5" value="确定" onclick="window.location.href='/docface/survey-register'"/>
		    </div>
		</div>
	</div>
	
	<div id="mobile_success2" style="display:none;">
		<div class="cover"></div>
		<div class="pop_box">
			<div class="pop_txt">
			    已签到
		    </div>
		    <div class="btn_box">
		    	<input type="button" class="btn5" value="确定" onclick="window.location.reload()"/>
		    </div>
		</div>
	</div>
@stop