@extends('front.common.layout')
@section('title')注册-国卫健康云 @stop
@section('description')注册-国卫健康云@stop
@section('keywords')注册-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>

<script src="/assets/js/ajaxfileupload.js"></script>
<script>
function displaayclause()
{
	var cover_display_clause = document.getElementById("cover_display_clause");
	var box_display_clause = document.getElementById("box_display_clause");
	cover_display_clause.style.display ="block";
	box_display_clause.style.display ="block";
}
function notgreedwithcaluse()
{
	var cover_display_clause = document.getElementById("cover_display_clause");
	var box_display_clause = document.getElementById("box_display_clause");
	cover_display_clause.style.display ="none";
	box_display_clause.style.display ="none";
	var greed_with_box = document.getElementById("greed_with_box");
	greed_with_box.checked = false;
	var greed_with_caluese = document.getElementById("greed_with_caluese");
	greed_with_caluese.checked =false;
	var no_greed_with_box = document.getElementById("no_greed_with_box");
	no_greed_with_box.checked = true;
}
function greedwithcaluse()
{
	var cover_display_clause = document.getElementById("cover_display_clause");
	var box_display_clause = document.getElementById("box_display_clause");
	cover_display_clause.style.display ="none";
	box_display_clause.style.display ="none";
	var greed_with_box = document.getElementById("greed_with_box");
	greed_with_box.checked = true;
	var greed_with_caluese = document.getElementById("greed_with_caluese");
	greed_with_caluese.checked =true;
	var no_greed_with_box = document.getElementById("no_greed_with_box");
	no_greed_with_box.checked = false;
}
function closegreedbox()
{
	var cover_display_clause = document.getElementById("cover_display_clause");
	var box_display_clause = document.getElementById("box_display_clause");
	cover_display_clause.style.display ="none";
	box_display_clause.style.display ="none";
}
</script>
<div class="p_bar_bg bg_color2">
	<div class="wid1025">
    	<div class="p_tit">用户注册</div>
    </div>
</div>
<div class="p_center_box">
<form id="register_form"   method="post" action="/register-do" onsubmit="return sub_reg();">
   <input type="hidden" id="link_user_id" name="link_user_id" value="0"/>
   <input type="hidden" id="rep_type" name="rep_type" value="0"/>
    <div class=" wid698 wid933">
        <div class="">
            <div class="center_block">
            	<div class="center_tit white_bg">个人信息</div>
                <div class="">
                    <div class="upload_box">
                        <div >
                            <img id="thumb" name="thumb" src="/assets/images/dafault/default_doc.jpg" />
                            <span class="chg_txt">更改头像</span>
                            <input class="upload_btn" id="upload_file" name="upload_file"  type="file" onchange="saveThumb()" />                        
                        	<input type="hidden" name="user_thumb" id="user_thumb" value="/assets/images/dafault/default_doc.jpg"/>
                        </div>
                    </div>
                    <div class="center_infor wid260">
                        <p><label><span class="red">*</span>性别：</label>
                        	<input type="radio" name="user_sex" id="user_sex1" value="1" checked/><span class="padding">男</span>
                            <input type="radio" name="user_sex" id="user_sex2" value="2"/><span class="padding">女</span>
                        </p>
                        <p>
                        	<label><span class="red">*</span>姓名：</label>
                        	<input type="text" id="user_name" name="user_name"/>
                        </p>
                        <p>
                        	<label><span class="red">*</span>手机：</label>
                        	<input type="text" id="user_tel" name="user_tel" />
                        </p>
                        <p>
                        	<label><span class="red">*</span>邮箱：</label>
                        	<input type="text" id="user_email" name="user_email"/>    
                        </p>
                        <p><label>地址：</label>
                        	<input type="text" id="user_address" name="user_address"/>
                        </p>
                        <?php 
                        	$aPos = User::getPosition();
                        ?>
                        <p id="pos_box">
                        	<label><span class="red">*</span>职称：</label>
                        	<select id="user_pos_select" style="width:197px;margin-top:5px;">
					            <option value="0">=请选择=</option>
					            @foreach($aPos as $k=>$v)
					            <option value="{{$v}}">{{$v}}</option>
					            @endforeach
					        </select>
					        <input type="hidden" id="user_position" name="user_position" value=""/>
                        </p>
                    </div>
                    <div class="center_infor wid260">
                        <div id="department_div">
	                        <p>
                        	<label><span class="red">*</span>省份：</label>
	                        	<select id="hospital1" style="width:150px;margin-top:5px;" onchange="change_hospital(2)">
						            <option value="0">=请选择=</option>
						            @foreach($aHosp as $k=>$v)
						            <option value="{{$k}}">{{$v}}</option>
						            @endforeach
						        </select>
	                        </p>
	                        <p>
		                        	<label><span class="red">*</span>城市：</label>
		                        	<select id="hospital2" style="width:150px;margin-top:5px;" onchange="change_hospital(3)">
						            <option value="0">=请选择=</option>
						            </select>
		                    </p>
		                    <p>
		                        	<label><span class="red">*</span>区/县：</label>
		                        	<select id="hospital3" style="width:150px;margin-top:5px;" onchange="change_hospital(4)">
						            <option value="0">=请选择=</option>
						            </select>
		                    </p>
	                        <p style="width:400px;">
	                        	<label><span class="red">*</span>医院：</label>
	                        	<select id="hospital4" style="width:150px;margin-top:5px;" onchange="change_hospital(5)">
						            <option value="0">=请选择=</option>
						        </select>
						        若没有，请<a style="cursor:pointer;color:#254797;" onclick="$('#user_company_div').show();">手动填写</a>
	                        </p>
	                        <p style="display:none;" id="user_company_div">
	                        	<label></label>
	                        	<input type="text" id="user_company_name" name="user_company_name" value=""/>  
	                        </p>
	                                                <?php 
                        	$aDep = User::getDepartment();
                        ?>
                        <input type="hidden" id="user_province" name="user_province" value="0"/>
                        <input type="hidden" id="user_city" name="user_city" value="0"/> 
                        <input type="hidden" id="user_county" name="user_county" value="0"/>
                        <input type="hidden" id="user_company" name="user_company" value="0"/>   
	                        <p>
	                        	<label><span class="red">*</span>科室：</label>
	                        	<select id="user_dep_select" style="width:150px;margin-top:5px;">
					            <option value="0">=请选择=</option>
					            @foreach($aDep as $k=>$v)
					            <option value="{{$v}}">{{$v}}</option>
					            @endforeach
					            </select>
					            <input type="hidden" name="user_department" id="user_department" value=""/> 
	                        </p>
	                        
	                        
                        </div>
                        <div id="regin_div" style="display:none;">
	                        <?php 
							$sRegin = User::getReginCache();
							$aRegin = json_decode($sRegin);
							?>
							<script>
							function change_regin(){
				            	var regin = $("#regin  option:selected").val();
				            	var url = '/user/area-info/'+regin;
				            	$.post(url,{},function(json){
				            		data = json;
				                	if(data=='noinfo'){
				        				var html = '<option value="0">=请选择=</option>';
				        				$("#area").html(html);
				        				
				        			}else{
				        				var bShow = false;
				        				var html = '<option value="0">=请选择=</option>';
				        				for(var elem in data){
				        					bShow = true;
				        					html += '<option value="' + elem + '">' + data[elem] + '</option>';
				        				}
				        				if(bShow){
				        					$("#area").html(html);
				        				}
				        			}
				            	},'json');
				            }
							</script>
							<p>
							<label><span class="red">*</span>省份：</label>
	                        	<select id="hospital21" style="width:150px;margin-top:5px;" onchange="change_hospital2(2)">
						            <option value="0">=请选择=</option>
						            @foreach($aHosp as $k=>$v)
						            <option value="{{$k}}">{{$v}}</option>
						            @endforeach
						        </select>
	                        </p>
	                        <p>
		                        	<label><span class="red">*</span>城市：</label>
		                        	<select id="hospital22" style="width:150px;margin-top:5px;" onchange="change_hospital2(3)" >
						            <option value="0">=请选择=</option>
						            </select>
		                    </p>
	                        <p>
	                        	<label><span class="red">*</span>大区：</label>
	                        	<select id="regin" style="width:150px;margin-top:5px;" onchange="change_regin()">
					            <option value="0">=请选择=</option>
					            @foreach($aRegin as $k=>$v)
					            <option value="{{$k}}">{{$v}}</option>
					            @endforeach
					            </select>
					            <input type="hidden" name="user_regin" id="user_regin" value=""/>  
	                        </p>
	                        <p>
	                        	<label><span class="red">*</span>地区：</label>
	                        	<select id="area" style="width:150px;margin-top:5px;" >
					            <option value="0">=请选择=</option>
					            </select>
	                        	<input type="hidden" name="user_area" id="user_area" value=""/>  
	                        </p>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                
            </div>
            <div class="center_block">
                <div class="center_tit white_bg">账号信息</div>
                <div class="center_infor3 center p0">
                    <p><label id="user_nick_label"><span class="red">*</span>用户名：</label><input class="infor_txt" type="text" id="user_nick" name="user_nick" placeholder="建议使用真实姓名+数字"/></p>
                    <p><label><span class="red">*</span>密　码：</label><input class="infor_txt" type="password" id="password" name="password" placeholder="建议使用手机号码"/></p>
                    <p><label><span class="red">*</span>确认密码：</label><input class="infor_txt" type="password" style="width:212px;" id="password_r"  /></p>
                </div>
            </div>
        </div>
          <div class="disclaimer">
            <input type="checkbox" id="greed_with_caluese" >
            <label for="">已阅读并同意<a href="javascript::void(0)" onclick="displaayclause()">声明条款</a></label>
        </div>
        <div class="center">
            <input class="save" type="submit" value="提交注册" />
        </div>
    </div>
    </form>
    
</div>
<div class="cover" style="display:none ;" id="cover_display_clause"></div>
<div class="pop-up" style="display:none ;" id="box_display_clause">
    <div class="close"><img src="/assets/images/front/web/close.png" alt="" onclick="closegreedbox()"></div>
    <div class="head-title">个人信息披露知情同意</div>
    <div class="box">
        <div class="content">
            <p class="text-indent">本文件旨在向您告知有关国卫健康云在收集、使用和保护个人信息方面的政策，并且，一旦签署，您将允许主办方：《北京医卫健康公益基金会》使用您的个人信息。请仔细阅读本知情同意。</p>
            <h1>收集个人信息：</h1>
            <p>您已被告知，并且您已同意北京医卫健康公益基金会将收集和使用您所提供的如下个人信息，包括：① 您的姓名，② 您工作所在的医疗机构，③ 您的电子邮箱地址，④ 您的手机号码等相关个人信息。</p>
            <h1>使用个人信息：</h1>
            <p>基于您愿意与北京医卫健康公益基金会开展合作，以接受北京医卫健康公益基金会提供的科学信息和产品信息，并参加相关意见调查，北京医卫健康公益基金会将为以下目的使用您的个人信息：① 北京医卫健康公益基金会与您之间的学术交流；② 邀请您参加学术会议；③ 邀请您参加意见调查；④ 发送会议通知、文件或材料。</p>
            <h1>向第三方披露个人信息：</h1>
            <p>北京医卫健康公益基金会将向国内或国外的北京医卫健康公益基金会关联方或北京医卫健康公益基金会所委托的第三方披露您的个人信息，并且该等关联方或第三方将为本知情同意中所规定之目的使用您的个人信息。</p>
            <h1> 个人信息保护：</h1>
            <p>北京医卫健康公益基金会已制定了相关政策并采取了相关安全措施，以保护您的个人信息免受未经授权的访问、篡改、泄露、毁损或丢失。北京医卫健康公益基金会将与接收您个人信息的任何授权第三方订立书面合同，确保该第三方承担有关采取安全措施保护您个人信息的合同义务。</p>
        </div>
        <div class="head-title">医疗卫生专业人士同意声明</div>
        <div class="content">
            <p class="text-indent">本人是依法具有执业资格的医疗卫生专业人士。本人已阅读本知情同意中的信息。本人同意提供本人的个人信息，以供北京医卫健康公益基金会根据本知情同意使用，并且同意北京医卫健康公益基金会可以根据本知情同意向经授权的第三方披露本人的个人信息。本人声明和保证，本人在向北京医卫健康公益基金会提供个人信息前，已获得本人所在单位的相关批准或许可（如需要），并且本人提供个人信息的行为不会违反适用的法律法规以及本人所在单位的规章制度。</p>
        </div>
    </div>
    <div class="foot-agree clearfix">
        <div class="checkbox">
            <input type="checkbox" id="greed_with_box" name="agree" onclick ="greedwithcaluse()">
            <label for="">我同意</label>
        </div>
        <div class="checkbox">
            <input type="checkbox" name="agree" id="no_greed_with_box" onclick="notgreedwithcaluse()">
            <label for="">我不同意</label>
        </div>
    </div>
</div>
	<script>
	$(function(){
		$.blockUI({ message: $('#register_box')});
	})
	function register(){
		var register_code = $.trim($('#register_code').val());
		if(!register_code){
			alert('请输入您的邀请码');return;
		}
		//根据邀请码获取用户id
		var url = '/register-code/'+register_code;
		$.post(url,{},function(msg){
			if(msg=='no_code'){
				alert('该用户编码不存在，请核对您的用户编码是否正确！');
			}else{
				if(msg=='BAYER1'){
					var str = "<label>职位：</label><input type='text' id='user_position' name='user_position'/> ";
					$('#pos_box').empty();
					$('#pos_box').append(str);  
					$('#department_div').hide();
					$('#regin_div').show();
					$('#user_nick_label').html('<span class="red">*</span>CWID：');
					document.getElementById("user_nick").setAttribute("placeholder","");
					document.getElementById("password").setAttribute("placeholder","");
					$('#rep_type').val(1);
				}else if(msg=='BAYER2'){
					var str = "<label>职位：</label><input type='text' id='user_position' name='user_position'/> ";
					$('#pos_box').empty();
					$('#pos_box').append(str);
					$('#department_div').hide();
					$('#regin_div').show();
					$('#user_nick_label').html('<span class="red">*</span>CWID：');
					document.getElementById("user_nick").setAttribute("placeholder","");
					document.getElementById("password").setAttribute("placeholder","");
					$('#rep_type').val(2);
				}else{
					$('#link_user_id').val(msg);
				}
				$.unblockUI();
			}
		})
	}


	function sub_reg(){
		var user_name = $.trim($('#user_name').val());
		var user_tel = $.trim($('#user_tel').val());
		var user_email = $.trim($('#user_email').val());
		var user_address = $.trim($('#user_address').val());
		var user_address = $.trim($('#user_address').val());
//		var user_city = $.trim($('#user_city').val());
		var user_department = $.trim($('#user_department').val());
		var user_company_name = $.trim($('#user_company_name').val());
		var user_nick = $.trim($('#user_nick').val());
		var password = $('#password').val();
		var password_r = $('#password_r').val();
		var user_sex = $("input[name='user_sex']:checked").val();
		var rep_type = $('#rep_type').val();

		if(user_sex != 1 && user_sex != 2){
			alert('请选择您的性别！');
			return false;
		}
		if(!user_name){
			alert('请输入您的姓名！');
			return false;
		}
		if(!user_tel){
			alert('请输入您的手机号码！');
			return false;
		}
//		var reg_mobile=/^((\+?86)|(\(\+86\)))?(13[0123456789][0-9]{8}|15[0123456789][0-9]{8}|18[0123456789][0-9]{8}|147[0-9]{8}|1349[0-9]{7})$/;
//		istel= reg_mobile.test(user_tel);
//		  if (!istel ) {
//		    alert("手机号码格式不正确！");
//		    return false;
//		  }
		var reg_mobile = /^1[34578][0-9]{9}$/;
		istel= reg_mobile.test(user_tel);
		if (!istel ) {
		    alert("手机号码格式不正确！");
		    return false;
		  }
		if(!user_email){
			alert('请输入您的邮箱！');
			return false;
		}
		  var reg = /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
		  ismail= reg.test(user_email);
		  if (!ismail ) {
		    alert("邮箱格式不正确！");
		    return false;
		  }
		if(rep_type==0){
			//医生职称必选
			var user_pos_select = $("#user_pos_select  option:selected").val();
			if(!user_pos_select||user_pos_select==0){
				alert('请选择您的职称！');
				return false;
			}
			$('#user_position').val(user_pos_select);
			
			var user_dep_select = $("#user_dep_select  option:selected").val();
			if(!user_dep_select||user_dep_select==0){
				alert('请选择您的科室！');
				return false;
			}
			$('#user_department').val(user_dep_select);
			//省份、城市、区县、医院
			var user_province = $.trim($('#user_province').val());
			var user_city = $.trim($('#user_city').val());
			var user_county = $.trim($('#user_county').val());
			var user_company = $.trim($('#user_company').val());
			if(!user_province||user_province==0){
				alert('请选择您的省份！');
				return false;
			}
			if(!user_city||user_city==0){
				alert('请选择您的城市！');
				return false;
			}
			if(!user_county||user_county==0){
				alert('请选择您所在的区/县！');
				return false;
			}
			if(!user_company||user_company==0){
				if(!user_company_name){
					alert('请选择您的医院！');
					return false;
				}
			}
		}else{
			var regin = $("#regin  option:selected").val();
        	var area = $("#area  option:selected").val();
//			var user_regin = $.trim($('#user_regin').val());
//			var user_area = $.trim($('#user_area').val());
			if(!regin||regin==0){
				alert('请选择您的大区！');
				return false;
			}
			if(!area||area==0){
				alert('请选择您的地区！');
				return false;
			}
			$('#user_regin').val(regin);
			$('#user_area').val(area);
			//省份、城市
			var user_province = $.trim($('#user_province').val());
			var user_city = $.trim($('#user_city').val());
			if(!user_province||user_province==0){
				alert('请选择您的省份！');
				return false;
			}
			if(!user_city||user_city==0){
				alert('请选择您的城市！');
				return false;
			}
		}
//		if(!user_company){
//			alert('请输入您的单位！');
//			return false;
//		}
		if(!user_nick){
			alert('请输入您的用户名或CWID！');
			return false;
		}
		if(!password){
			alert('请输入您的密码！');
			return false;
		}
		if(!password_r){
			alert('请重复输入密码！');
			return false;
		}
		if(password!=password_r){
			alert('两次输入的密码不一致，请重新输入！');
			return false;
		}
		var greed_with_caluese = document.getElementById("greed_with_caluese");
		var result = greed_with_caluese.checked;
		if(!result){
			var cover_display_clause = document.getElementById("cover_display_clause");
			var box_display_clause = document.getElementById("box_display_clause");
			cover_display_clause.style.display ="block";
			box_display_clause.style.display ="block";
			return false;
		}
		var url = '/user/register-check/'+user_nick+'/'+user_tel+'/'+user_email;
		$.post(url,{},function(msg){
			if(msg=='nick_exist'){
				//若该昵称存在
				alert('该用户名已经被注册了，试试其他用户名吧！');
				return false;
			}else if(msg=='tel_exist'){
				alert('该手机号码已经被注册了！');
				return false;
			}else if(msg=='email_exist'){
				alert('该邮箱已经被注册了！');
				return false;
			}else{
				$('#register_form').attr('onsubmit','');
				$("#register_form").submit();
			}
		})
		return false;
	}
	function saveThumb() {
		$.ajaxFileUpload({
			url: "/user/upload-user-first-thumb",
			secureuri: false,
			fileElementId: "upload_file",
			dataType: "json",
			success: function(data, status) {
				$("#thumb").attr("src", data.user_thumb);
				$("#user_thumb").val( data.user_thumb);
			}
		})
	}
	</script>
	<div class="pop_logoin_box center" id="register_box" style="display:none;">
		<div class="pop_cont2">
	    	<p>请您输入邀请人的用户编码：</p>
	       
	        <p class="pop_ptop"><input class="pop_txt" type="text" id="register_code"/></p>
	    </div>
	    <div class="center">
	    	<input class="save" type="button" value="确定" onclick="register()"/>
	        <input class="save mleft75" type="button" value="取消" onclick="$.unblockUI();$.blockUI({ message: $('#register_fail')});"/>
	    </div>
	    <div class="pop_prompt">
	    	<span class="color">温馨提示：</span>如果您无邀请码，请联系北京医卫健康公益基金会 <?php echo HOTLINE;?>。
	    </div>
	</div>
	
	<div class="pop_logoin_box center" id="register_fail" style="display:none;">
	<div class="pop_cont2 pop_ptop60">
    	<p>很抱歉, 本网站仅提供给医疗卫生专业人士</p>
    </div>
    <div class="center">
    	<input class="save" type="button" value="确定" onclick="$.unblockUI();window.location.href='/';"/>
    </div>
</div>
@stop