<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>@yield('title')</title>
<meta name="description" content="@yield('description')"/>
<meta name="keywords" content="@yield('keywords')"/>
<link rel="stylesheet" type="text/css" href="/assets/css/css.css"/>
<script src="http://libs.baidu.com/jquery/1.7.2/jquery.js"></script>
<script src="/assets/js/blockui/jquery.blockUI.js" type="text/javascript"></script>
<script src="/assets/js/common.js"></script>
<script src="/assets/js/ajaxfileupload.js"></script>
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?b4026242a28685ebffeb34a7817c8461";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
</head>

<body>
<div class="p_top_bg">
	<div class="logo_box">
<!--     	 <img class="logo left" src="/assets/images/front/web/logo1.png" /> -->
<!--     	 <img class="logo right" src="/assets/images/front/web/logo2.png" /> -->
    	 <div class="clear"></div>
    </div>
</div>
<div class="p_body">
    <div class="p_menu_bg">
        <ul class="left">
            <li><a id="index_banner" href="/" @if(getRouteName()=='')class="sele" @endif><span>首页</span></a></li>
            <li><a id="docface_banner" @if(getRouteName()=='docface')class="sele" @endif href="/docface"><span>空中课堂</span></a></li>
            <li><a @if(getRouteName()=='aspirin')class="sele" @endif href="/aspirin"><span>专项基金</span></a></li>
<!--            <li><a href="/consultation" @if(getRouteName()=='consultation')class="sele" @endif><span>专家大会诊</span></a></li>-->
            @if(Auth::check() && Auth::User()->role_id==2)
              <li><a href="/meeting/appoint-list" @if(getRouteName()=='meeting')class="sele" @endif ><span>自助会议平台</span></a></li>
            @endif
            <!-- 
            <li><a href="/case/rule" @if(getRouteName()=='case')class="sele" @endif><span>病例征集</span></a></li>
            <li><a href="/information" @if(getRouteName()=='information')class="sele" @endif ><span>资讯中心</span></a></li>
             -->
            <li><a href="/pmd" @if(getRouteName()=='pmd'||getRouteName()=='cnsearch')class="sele" @endif ><span>文献检索</span></a></li>
<!--            @if(Auth::check() && Auth::User()->role_id==2)-->
<!--            <li><a href="/meeting/appoint-list" @if(getRouteName()=='meeting')class="sele" @endif ><span>自助会议平台</span></a></li>-->
<!--            @endif-->
            <li><a class="b_right @if(getRouteName()=='user') sele @endif " href="/user"><span>个人中心</span></a></li>
        </ul>
        <ul class="p_login">
            @if(!Auth::check())
             <li><a href="/register" id="register_banner">注册</a></li>
            <li><a class="no_border" href="#" onclick="show_login_box()" id="login_banner">登录</a></li>
            @else
           
            
            
            
            @if(Auth::User()->role_id==2&&(!is_numeric(Auth::User()->user_city)))
            <script>
            $(function(){
            	$.blockUI({ message: $('#rep_new_city_box')});
            });
            function change_rep_city_info(){
            	var user_province = $.trim($('#user_province').val());
    			var user_city = $.trim($('#user_city').val());
    			if(!user_province||user_province==0){
    				alert('请选择您的省份！');
    				return false;
    			}
    			if(!user_city||user_city==0){
    				alert('请选择您的城市！');
    				return false;
    			}
            	var url = '/user/change-rep-city-info/'+user_province+'/'+user_city;
            	$.post(url,{},function(msg){
                	if(msg=='success'){
                		$.unblockUI();
                    	alert('更新标准省份及城市成功');
                	}else{
                    	alert('更新失败，请刷新后重试');
                	}
            	})
            }
            </script>
            @elseif(Auth::User()->role_id==3&&(!is_numeric(Auth::User()->user_city)||!is_numeric(Auth::User()->user_company)))
            <script>
            $(function(){
            	$.blockUI({ message: $('#doc_new_city_box')});
            });
            function change_doc_city_info(){
            	//省份、城市、区县、医院
    			var user_province = $.trim($('#user_province').val());
    			var user_city = $.trim($('#user_city').val());
    			var user_county = $.trim($('#user_county').val());
    			var user_company = $.trim($('#user_company').val());
    			var user_company_layout_name = $.trim($('#user_company_layout_name').val());
    			if(!user_province||user_province==0){
    				alert('请选择您的省份！');
    				return false;
    			}
    			if(!user_city||user_city==0){
    				alert('请选择您的城市！');
    				return false;
    			}
    			if(!user_county||user_county==0){
    				alert('请选择您所在的区/县！');
    				return false;
    			}
    			if(!user_company||user_company==0){
        			if(!user_company_layout_name){
	    				alert('请选择您的医院！');
	    				return false;
        			}
    			}
    			var user_dep_select = $("#user_dep_select  option:selected").val();
    			if(!user_dep_select||user_dep_select==0){
    				alert('请选择您的科室！');
    				return false;
    			}
    			if(user_company_layout_name){
            		var url = '/user/change-doc-city-info/'+user_province+'/'+user_city+'/'+user_county+'/'+user_company+'/'+user_dep_select+'/'+user_company_layout_name;
    			}else{
    				var url = '/user/change-doc-city-info/'+user_province+'/'+user_city+'/'+user_county+'/'+user_company+'/'+user_dep_select;
    			}
                $.post(url,{},function(msg){
                	if(msg=='success'){
                		$.unblockUI();
                    	alert('更新标准医院及科室成功');
                    	window.location.reload();
                	}else{
                    	alert('更新失败，请刷新后重试');
                	}
            	})
            }
            </script>
            @endif
            <?php
            //医生用户是否已更新职称    dll  add  20170408
			$aPos = User::getPosition();
			?>
	        @if(Auth::User()->role_id==3&&(!in_array(Auth::User()->user_position,$aPos)))
	        <script>
	            $(function(){
	            	$.blockUI({ message: $('#doc_new_pos_box')});
	            });
	            function change_doc_pos_info(){
	    			var user_pos_select = $("#user_pos_select  option:selected").val();
	    			if(!user_pos_select||user_pos_select==0){
	    				alert('请选择您的职称！');
	    				return false;
	    			}
	    			var url = '/user/change-doc-pos-info/'+user_pos_select;
	                $.post(url,{},function(msg){
	                	if(msg=='success'){
	                		$.unblockUI();
	                    	alert('更新标准职称成功');
	                    	window.location.reload();
	                	}else{
	                    	alert('更新失败，请刷新后重试');
	                	}
	            	})
	            }
	        </script>
	        @endif
             <?php 
            //已登录用户是否是代表
            ?>
            @if(Auth::User()->role_id==2&&(!is_numeric(Auth::User()->user_regin)||!is_numeric(Auth::User()->user_area)))
            <script>
            $(function(){
            	$.blockUI({ message: $('#rep_new_info_box')});
            });
            function change_rep_info(){
            	var regin = $("#regin  option:selected").val();
            	var area = $("#area  option:selected").val();
            	if(regin==0){
					alert('请选择大区');return;
                }
                if(area==0){
					alert('请选择地区');return;
                }
            	var url = '/user/change-rep-info/'+regin+'/'+area;
            	$.post(url,{},function(msg){
                	if(msg=='success'){
                		$.unblockUI();
                    	alert('更新标准大区及地区成功');
                    	window.location.reload();
                	}else{
                    	alert('更新失败，请刷新后重试');
                	}
            	})
            }
            function change_regin(){
            	var regin = $("#regin  option:selected").val();
            	var url = '/user/area-info/'+regin;
            	$.post(url,{},function(json){
            		data = json;
                	if(data=='noinfo'){
        				var html = '<option value="0">=请选择=</option>';
        				$("#area").html(html);
        				
        			}else{
        				var bShow = false;
        				var html = '<option value="0">=请选择=</option>';
        				for(var elem in data){
        					bShow = true;
        					html += '<option value="' + elem + '">' + data[elem] + '</option>';
        				}
        				if(bShow){
        					$("#area").html(html);
        				}
        			}
        			scroll(0,0);
            	},'json');
            }
            </script>
            @endif
            <li>您好，{{Auth::User()->user_nick}}<a class="no_border" href="/logout">退出</a></li>
            @endif
        </ul>
    </div>
    @yield('content1')
</div>
@yield('content2')
<?php 
$sRegin = User::getReginCache();
$aRegin = json_decode($sRegin);
?>
<div class="pop_logoin_box" style="display:none;" id="rep_new_info_box">
	<div class="pop_tit">请更新标准大区及地区名称</div>
    <div class="pop_cont" style="padding-top:20px;">
    	<div class="pop_line">
        	<div class="wid70 left">大区:</div>
            <div class="left">
            <select id="regin" style="width:150px;margin-top:5px;" onchange="change_regin()">
            <option value="0">=请选择=</option>
            @foreach($aRegin as $k=>$v)
            <option value="{{$k}}">{{$v}}</option>
            @endforeach
            </select>
            </div>
            <div class="clear"></div>
        </div>
        <div class="pop_line">
        	<div class="wid70 left">地区:</div>
            <div class="left">
			<select id="area" style="width:150px;margin-top:5px;" onchange="scroll(0,0);">
            <option value="0">=请选择=</option>
            </select>
			</div>
            <div class="clear"></div>
        </div>
		<div class="pop_btn_box" style="padding-top: 16px;">
        	<input type="button" value="确定" onclick="change_rep_info()"/>
        </div>
    </div>
</div>
<?php 
$aHosp = Hospital::getChildren();
?>
<div class="pop_logoin_box" style="display:none;" id="rep_new_city_box">
	<div class="pop_tit">请更新标准省份和城市名称</div>
    <div class="pop_cont" style="padding-top:20px;">
    	<div class="pop_line">
        	<div class="wid70 left">省份:</div>
            <div class="left">
            <select id="hospital21" style="width:150px;margin-top:5px;" onchange="change_hospital2(2)">
            <option value="0">=请选择=</option>
            @foreach($aHosp as $k=>$v)
            <option value="{{$k}}">{{$v}}</option>
            @endforeach
			</select>
            </div>
            <div class="clear"></div>
        </div>
        <div class="pop_line">
        	<div class="wid70 left">城市:</div>
            <div class="left">
			<select id="hospital22" style="width:150px;margin-top:5px;" onchange="change_hospital2(3)">
            <option value="0">=请选择=</option>
            </select>
			</div>
            <div class="clear"></div>
        </div>
		<div class="pop_btn_box" style="padding-top: 16px;">
        	<input type="button" value="确定" onclick="change_rep_city_info()"/>
        </div>
    </div>
</div>
						<input type="hidden" id="user_province" name="user_province" value="0"/>
                        <input type="hidden" id="user_city" name="user_city" value="0"/> 
                        <input type="hidden" id="user_county" name="user_county" value="0"/>
                        <input type="hidden" id="user_company" name="user_company" value="0"/>
<div class="pop_logoin_box2" style="display:none;" id="doc_new_city_box">
<script>
                    function change_hospital(id){
                        var pid = id-1;
                    	var hospital = $("#hospital"+pid+"  option:selected").val();
                    	console.log(hospital);
                    	var html_null = '<option value="0">=请选择=</option>';
                    	if(pid==1){
							$('#user_province').val(hospital);
							$("#hospital"+2).html(html_null);
							$("#hospital"+3).html(html_null);
							$("#hospital"+4).html(html_null);
                        }else if(pid==2){
                        	$('#user_city').val(hospital);
                        	$("#hospital"+3).html(html_null);
							$("#hospital"+4).html(html_null);
                        }else if(pid==3){
                        	$('#user_county').val(hospital);
                        	$("#hospital"+4).html(html_null);
                        }else {
                        	$('#user_company').val(hospital);
                            return ;
                        }
		            	var url = '/user/hospital-child/'+hospital;
		            	$.post(url,{},function(json){
		            		data = json;
		                	if(data=='noinfo'){
		        				var html = '<option value="0">=请选择=</option>';
		        				$("#hospital"+id).html(html);
		        				
		        			}else{
		        				var bShow = false;
		        				var html = '<option value="0">=请选择=</option>';
		        				for(var elem in data){
		        					bShow = true;
		        					html += '<option value="' + elem + '">' + data[elem] + '</option>';
		        				}
		        				if(bShow){
		        					$("#hospital"+id).html(html);
		        				}
		        			}	
		        			},'json');
                    }

                    function change_hospital2(id){
                        var pid = id-1;
                    	var hospital2 = $("#hospital2"+pid+"  option:selected").val();
                    	if(pid==1){
							$('#user_province').val(hospital2);
                        }else if(pid==2){
                        	$('#user_city').val(hospital2);
                        	return ;
                        }
		            	var url = '/user/hospital-child/'+hospital2;
		            	$.post(url,{},function(json){
		            		data = json;
		                	if(data=='noinfo'){
		        				var html = '<option value="0">=请选择=</option>';
		        				$("#hospital2"+id).html(html);
		        				
		        			}else{
		        				var bShow = false;
		        				var html = '<option value="0">=请选择=</option>';
		        				for(var elem in data){
		        					bShow = true;
		        					html += '<option value="' + elem + '">' + data[elem] + '</option>';
		        				}
		        				if(bShow){
		        					$("#hospital2"+id).html(html);
		        				}
		        			}
		            	},'json');
                    }

				            </script>
	<div class="pop_tit">请更新标准医院及科室名称</div>
    <div class="pop_cont" style="padding-top:20px;">
    	<div class="pop_line">
        	<div class="wid70 left">省份:</div>
            <div class="left">
            <select id="hospital1" style="width:150px;margin-top:5px;" onchange="change_hospital(2)">
            <option value="0">=请选择=</option>
            @foreach($aHosp as $k=>$v)
            <option value="{{$k}}">{{$v}}</option>
            @endforeach
			</select>
            </div>
            <div class="clear"></div>
        </div>
        <div class="pop_line">
        	<div class="wid70 left">城市:</div>
            <div class="left">
			<select id="hospital2" style="width:150px;margin-top:5px;" onchange="change_hospital(3)">
            <option value="0">=请选择=</option>
            </select>
			</div>
            <div class="clear"></div>
        </div>
        <div class="pop_line">
        	<div class="wid70 left">区/县:</div>
            <div class="left">
			<select id="hospital3" style="width:150px;margin-top:5px;" onchange="change_hospital(4)">
            <option value="0">=请选择=</option>
            </select>
			</div>
            <div class="clear"></div>
        </div>
        <div class="pop_line">
        	<div class="wid70 left">医院:</div>
            <div class="left">
			<select id="hospital4" style="width:150px;margin-top:5px;" onchange="change_hospital(5)">
	            <option value="0">=请选择=</option>
	        </select>
	         <a style="cursor:pointer;color:#254797;" onclick="$('#user_company_layout_div').show();">手动添加</a>
			</div>
            <div class="clear"></div>
        </div>
        <div  class="pop_line" style="display:none;" id="user_company_layout_div">
        	<div class="wid70 left">&nbsp;</div>
            <div class="left">
        		<input type="text" class="pop_txt" style="width:141px;height:17px;" id="user_company_layout_name" name="user_company_layout_name" value=""/>
        	</div>
            <div class="clear"></div>
        </div>
        <?php 
        $aDep = User::getDepartment();
        ?>
         <div class="pop_line">
        	<div class="wid70 left">科室:</div>
            <div class="left">
			<select id="user_dep_select" style="width:150px;margin-top:5px;">
					            <option value="0">=请选择=</option>
					            @foreach($aDep as $k=>$v)
					            <option value="{{$v}}">{{$v}}</option>
					            @endforeach
					            </select>
			</div>
            <div class="clear"></div>
        </div>
		<div class="pop_btn_box" style="padding-top: 16px;">
        	<input type="button" value="确定" onclick="change_doc_city_info()"/>
        </div>
    </div>
</div>
<?php 
$aPos = User::getPosition();
?>
<div class="pop_logoin_box" style="display:none;" id="doc_new_pos_box">
	<div class="pop_tit">请更新标准职称名称</div>
    <div class="pop_cont" style="padding-top:20px;">
    	<div class="pop_line">
        	<div class="wid70 left">职称:</div>
            <div class="left">
            	<select id="user_pos_select" style="width:150px;margin-top:5px;">
					<option value="0">=请选择=</option>
					@foreach($aPos as $k=>$v)
					<option value="{{$v}}">{{$v}}</option>
					@endforeach
				</select>
            </div>
            <div class="clear"></div>
        </div>
		<div class="pop_btn_box" style="padding-top: 16px;">
        	<input type="button" value="确定" onclick="change_doc_pos_info();"/>
        </div>
    </div>
</div>

<div class="pop_logoin_box" style="display:none;" id="login_box">
	<div class="pop_tit">登　录</div>
    <div class="pop_cont">
    	<div class="pop_line">
        	<div class="wid70 left">用户名:</div>
            <div class="left"><input class="pop_txt" type="text" id="user_nick_login" name="user_nick_login" placeholder="请输入手机/邮箱/用户名"/></div>
            <div class="clear"></div>
        </div>
        <input type="hidden" id="old_user_id" value="">
        <div class="pop_line">
        	<div class="wid70 left">密码:</div>
            <div class="left"><input class="pop_txt" type="password" id="password_login" name="password_login" placeholder="请输入密码"/></div>
            <div class="clear"></div>
        </div><!--
        <div class="pop_line">
        	<div class="wid70 left">验证码:</div>
            <div class="left"><input class="pop_txt wid140" type="text" id="verifi_code" name="verifi_code"/></div>
            <div class="left">
            	<a href="#" id="codeimg_banner"><img class="check" src="/auth-code" title="点击刷新" id="codeimg" onclick="this.src='/code?'+Math.random();"/></a>
            </div>
            <div class="clear"></div>
        </div>
        --><div class="pop_btn_box" style="padding-top: 20px;width:320px;">
        	<input type="button" value="登录" onclick="login_sub()"/>
            <input type="button" value="取消" onclick="$.unblockUI();"/>
            <a id="forget_password_a_tag" href="/forget" style="font-size:12px; color:red;">忘记密码？</a>
        </div>
    </div>
</div>
<script>
  function login_sub(){
	  var user_nick_login = $.trim($('#user_nick_login').val());
	  var password_login = $.trim($('#password_login').val());
	  var verifi_code = $.trim($('#verifi_code').val());
	  if(!user_nick_login||!password_login){
		  $('#codeimg').click();
		  alert('用户名和密码不能为空！');
		  return;
	  }
	  $.post('/login',{'user_nick':user_nick_login,'password':password_login,'verifi_code':verifi_code},function(msg){
		data = eval("("+msg+")");//转换为json对象 
		if(data.success=='code_err'){
			$('#codeimg').click();
			alert('请输入正确的验证码');
		}else if(data.success== 'agreeterms'){
			var old_user_id = document.getElementById("old_user_id");
			var layout_agree_with_cover = document.getElementById("layout_agree_with_cover");
			var layout_agree_with_pop_box = document.getElementById("layout_agree_with_pop_box");
			$.unblockUI();
			layout_agree_with_pop_box.style.display ="block";
			layout_agree_with_cover.style.display ="block";
			old_user_id.value = data.userid;
			//老用户医生需要同意免责条款才能登陆
		}else if(data.success=='success'){
			if(data.data!=''){
				alert('登陆成功,您的用户编码为'+data.data);
				window.location.href='/';
				return;
			}
			window.location.reload();
		}else{
			$('#codeimg').click();
			alert('请输入正确的用户名和密码');
		}
	  })
  }
  function show_login_box(){
		$.blockUI({ message: $('#login_box')});
		document.getElementById("forget_password_a_tag").setAttribute("onclick","");
		document.getElementById("forget_password_a_tag").setAttribute("href","/forget");
//		$('#codeimg').click();
	}
  function bayoutclosegreedbox()
  {
	  var layout_agree_with_cover = document.getElementById("layout_agree_with_cover");
	  var layout_agree_with_pop_box = document.getElementById("layout_agree_with_pop_box");
	  layout_agree_with_pop_box.style.display ="none";
	  layout_agree_with_cover.style.display ="none";
  }
  function layoutagreewithcalues()
  {
	  var old_user_id = document.getElementById("old_user_id").value;
	  $.post('/agree-disclaimer',{'old_user_id':old_user_id},function(msg){
			data = eval("("+msg+")");//转换为json对象 
			if(data.success=='success'){
				var layout_agree_with_cover = document.getElementById("layout_agree_with_cover");
				var layout_agree_with_pop_box = document.getElementById("layout_agree_with_pop_box");
				layout_agree_with_pop_box.style.display ="none";
				layout_agree_with_cover.style.display ="none";
			}
		  })
		  var user_nick_login = $.trim($('#user_nick_login').val());
	  var password_login = $.trim($('#password_login').val());
	  var verifi_code = $.trim($('#verifi_code').val());
	  if(!user_nick_login||!password_login){
		  $('#codeimg').click();
		  alert('用户名和密码不能为空！');
		  return;
	  }
	  $.post('/login',{'user_nick':user_nick_login,'password':password_login,'verifi_code':verifi_code},function(msg){
		data = eval("("+msg+")");//转换为json对象 
		if(data.success=='code_err'){
			$('#codeimg').click();
			alert('请输入正确的验证码');
		}else if(data.success== 'agreeterms'){
			var old_user_id = document.getElementById("old_user_id");
			var layout_agree_with_cover = document.getElementById("layout_agree_with_cover");
			var layout_agree_with_pop_box = document.getElementById("layout_agree_with_pop_box");
			$.unblockUI();
			layout_agree_with_pop_box.style.display ="block";
			layout_agree_with_cover.style.display ="block";
			old_user_id.value = data.userid;
			//老用户医生需要同意免责条款才能登陆
		}else if(data.success=='success'){
			if(data.data!=''){
				alert('登陆成功,您的用户编码为'+data.data);
				window.location.href='/';
				return;
			}
			window.location.reload();
		}else{
			$('#codeimg').click();
			alert('请输入正确的用户名和密码');
		}
	  })
	  
  }

  function layoutnotagreewithcalues()
  {
	  var layout_agree_with_cover = document.getElementById("layout_agree_with_cover");
	  var layout_agree_with_pop_box = document.getElementById("layout_agree_with_pop_box");
	 layout_agree_with_pop_box.style.display ="none";
	 layout_agree_with_cover.style.display ="none";
	 $.blockUI({ message: $('#login_box')});
	 document.getElementById("forget_password_a_tag").setAttribute("onclick","");
	 document.getElementById("forget_password_a_tag").setAttribute("href","/forget");
  }
</script>
<link rel="stylesheet"  type="text/css" href="/assets/css/common.css"/>
<div class="cover"  style="display:none ;"  id="layout_agree_with_cover"></div>
<div class="pop-up"  style="display:none ;" id="layout_agree_with_pop_box">
    <div class="close"><img src="/assets/images/front/web/close.png" alt="" onclick="bayoutclosegreedbox()"></div>
    <div class="head-title">个人信息披露知情同意</div>
     <div class="box">
        <div class="content">
            <p class="text-indent">本文件旨在向您告知有关国卫健康云在收集、使用和保护个人信息方面的政策，并且，一旦签署，您将允许主办方：《北京医卫健康公益基金会》使用您的个人信息。请仔细阅读本知情同意。</p>
            <h1>收集个人信息：</h1>
            <p>您已被告知，并且您已同意北京医卫健康公益基金会将收集和使用您所提供的如下个人信息，包括：① 您的姓名，② 您工作所在的医疗机构，③ 您的电子邮箱地址，④ 您的手机号码等相关个人信息。</p>
            <h1>使用个人信息：</h1>
            <p>基于您愿意与北京医卫健康公益基金会开展合作，以接受北京医卫健康公益基金会提供的科学信息和产品信息，并参加相关意见调查，北京医卫健康公益基金会将为以下目的使用您的个人信息：① 北京医卫健康公益基金会与您之间的学术交流；② 邀请您参加学术会议；③ 邀请您参加意见调查；④ 发送会议通知、文件或材料。</p>
            <h1>向第三方披露个人信息：</h1>
            <p>北京医卫健康公益基金会将向国内或国外的北京医卫健康公益基金会关联方或北京医卫健康公益基金会所委托的第三方披露您的个人信息，并且该等关联方或第三方将为本知情同意中所规定之目的使用您的个人信息。</p>
            <h1> 个人信息保护：</h1>
            <p>北京医卫健康公益基金会已制定了相关政策并采取了相关安全措施，以保护您的个人信息免受未经授权的访问、篡改、泄露、毁损或丢失。北京医卫健康公益基金会将与接收您个人信息的任何授权第三方订立书面合同，确保该第三方承担有关采取安全措施保护您个人信息的合同义务。</p>
        </div>
        <div class="head-title">医疗卫生专业人士同意声明</div>
        <div class="content">
            <p class="text-indent">本人是依法具有执业资格的医疗卫生专业人士。本人已阅读本知情同意中的信息。本人同意提供本人的个人信息，以供北京医卫健康公益基金会根据本知情同意使用，并且同意北京医卫健康公益基金会可以根据本知情同意向经授权的第三方披露本人的个人信息。本人声明和保证，本人在向北京医卫健康公益基金会提供个人信息前，已获得本人所在单位的相关批准或许可（如需要），并且本人提供个人信息的行为不会违反适用的法律法规以及本人所在单位的规章制度。</p>
        </div>
    </div>
    <div class="foot-agree clearfix">
        <div class="checkbox">
            <input type="checkbox" name="agree" onclick="layoutagreewithcalues()">
            <label for="">我同意</label>
        </div>
        <div class="checkbox">
            <input type="checkbox" name="agree" onclick="layoutnotagreewithcalues()">
            <label for="">我不同意</label>
        </div>
    </div>
</div>


<div id="common_pop_box" style="display:none;">
	<div class="pop_logoin_box">
		<div class="pop_tit2" id="common_pop_box_title">
	    	提示
	    </div>
	    <div class=" pop_cont3" id="common_pop_box_content">
	    	您已提交成功
	    </div>
	    <div class="center">
	    	<input type="button" id="common_pop_box_submit" class="question_btn" value="确定" onclick="$.unblockUI();"/>
	    </div>
	</div>
</div>
</body>
</html>