<div class="p_bottom">
    <div class="p_bottom_body">
	    <div class="bottom_infor" style="padding-left:120px;">
	    	<div class="host">
                <p>主办单位：北京医卫健康公益基金会</p>
<!--                 <p>赞助单位：拜耳医药保健有限公司</p>   -->
                <p>网站所有权归北京医卫健康公益基金会所有</p>   
            </div>
            <div class="warning">
                <p>
                    如遇APP使用问题，请拨打咨询电话<?php echo HOTLINE;?>
                    <br/>（工作时间09:00-18:00）
                </p>
                <p style="color:#888888;font-weight:normal;">
                   如需报告使用拜耳产品后出现的不适或产品质量问题，可与您的医生药师或本地药品监管部门进行联系；也可通过以下方式直接联系我们。邮箱：pv.china@bayer.com
                </p>
            </div>
        </div>
        <div class="bottom_infor">
            <div class="">
                <img src="/assets/images/front/web/erweima.jpg" style="width:72px;height:72px;"/>
            </div>
            <div class="erweima">
                                    扫描二维码
            </div>
            <div class="erweima">
                                下载国卫健康云APP
            </div>
        </div> 
        <div class="bottom_infor">
         <div class="">
                <img src="/assets/images/weixin/erweima.jpg" style="width:72px;height:72px;"/>
            </div>
            <div class="erweima">
                                    扫描二维码
            </div>
            <div class="erweima">
           	 关注“空中课堂综合管理”微信公众号
            </div>
        </div>
        <div class="clear"></div>   
    </div>
</div>