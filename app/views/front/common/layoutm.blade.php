<body>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>@yield('title')</title>
<meta name="description" content="@yield('description')"/>
<meta name="keywords" content="@yield('keywords')"/>
<link rel="stylesheet" type="text/css" href="/assets/css/mobile.css" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<script src="http://libs.baidu.com/jquery/1.7.2/jquery.js"></script>
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?b4026242a28685ebffeb34a7817c8461";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>

</head>
<script type="text/javascript">
function box_show(str){
	var top = document.body.scrollTop;
	$('#box_pop_div').css('top',top+90)
	$('#box_content').html(str);
	$('#mobile_box').show();
}
</script>
<body>
    @yield('content3')
    <div id="mobile_box" style="display:none;z-index:100000;">
		<div class="cover" style="z-index: 100000;"></div>
		<div class="pop_box" style="z-index: 100001;" id="box_pop_div">
			<div class="pop_txt" id="box_content">
			    签到成功
		    </div>
		    <div class="btn_box">
		    	<input type="button" class="btn5" value="确定" onclick="$('#mobile_box').hide()"/>
		    </div>
		</div>
	</div>
    
</body>
</html>