<div id="survey_box">
	<div class="cover"></div>
	<div class="pop_info_box">
	   	<a href="javascript:void(0)" class="pop_info_close" onclick="pop_survey_close();"></a>
		<div class="pop_info_top"></div>
	    <div class="pop_info_cont">
	    	<div class="pop_info_tit">
	        	调研
	        </div>
	        <form id="survey_form" action="/user/survey-ajax-submit" method="post">
	        @foreach($aQuestion as $k=>$v)
		        <div class="question_block">
		        	<div class="question_tit">
		            	{{$v['q_order']}}.{{$v['q_title']}}
		            </div>
		            <div class="question_cont">
						@foreach($v['items'] as $kitem=>$vitem)
							@if($v['q_type']==1)
							<p><input class="question_pad" type="radio" name="survey[{{$v['id']}}]" value="{{$kitem}}"/>{{$vitem}}</p>
							@else
							<p><input class="question_pad" type="checkbox" name="survey[{{$v['id']}}][]" value="{{$kitem}}" />{{$vitem}}</p>
							@endif
						@endforeach
					</div>
				</div>
			@endforeach
	        <div class="question_btn_box">
	        	<input type="button" class="question_btn" value="提交" onclick="submit_survey()"/>
	        </div>  
	        </form>      
	    </div>
	    <div class="pop_info_btm"></div>
	</div>
</div>
	
<div id="survey_submit_success_box" style="display:none;">
	<div class="cover"></div>
	<div class="pop_logoin_box">
		<div class="pop_tit2">
	    	提示
	    </div>
	    <div class=" pop_cont3">
	    	您已提交成功
	    </div>
	    <div class="center">
	    	<input type="button" class="question_btn" value="确定" onclick="survey_submit_box({{$iSurveySet}})"/>
	    </div>
	</div>
</div>
@if($iSurveySet==0)
<div id="survey_success_box" style="display:none;">
	<div class="cover"></div>
	<div class="pop_info_box">
	   	<a href="javascript:void(0)" class="pop_info_close" onclick="agree_medlive_reg(3)"></a>
		<div class="pop_info_top"></div>
	    <div class="pop_info_cont">
	    	<div class="pop_info_tit font24">
	        	消息提醒
	        </div>
	        <div class="remind_box ">
	        	<div class="remind_tit">尊敬的会员您好，感谢您参与本次调研，同时获得医脉通{{Config::get('config.survey_gold')}}麦粒</div>
	            <div class="remind_block">
	            	<div class="bold">
	                	医脉通用户福利：
	                </div>
	                <div class="remind_txt">
	                	<div>
	                    	全国的临床指南库；1万份指南免费下载；
	                    </div>
	                    <div>
	                    	麦粒兑换礼品；
	                    </div>
	                    <div>
	                    	海量医学资源；
	                    </div>
	                    <div>
	                    	6000多份经典病例及诊疗经验；
	                    </div>
	                    <div>
	                    	专业学术软件——文献检索、论文写作、外文翻译、药品图鉴9大软件等；
	                    </div>
	                    <div>
	                    	麦粒兑换礼品（麦粒只能在医脉通网站使用，以下为部分可兑换礼品，点击兑换礼品即可查看）
	                    </div>
	                    <div class="gift_box">
	                    	<img src="http://www.medlive.cn/upload/gift/thumb/1446026901661.jpg" />
	                        <img src="http://www.medlive.cn/upload/gift/thumb/1445508132891.jpg" />
	                        <img src="http://www.medlive.cn/upload/gift/thumb/1444992230639.jpg" />
	                        <img src="http://www.medlive.cn/upload/gift/thumb/1298966592118.jpg" />
	                        <img src="http://www.medlive.cn/upload/gift/thumb/1298967176712.jpg" />
	                        <img class="mright0" src="http://www.medlive.cn/upload/gift/thumb/1446027263052.jpg" />
	                    </div>
	                </div>
	            </div>
	            <div class="remind_txt2">
	            	<div class="">
	                	我已阅读 <a href="http://www.medlive.cn/service/disclaimer" target="_blank" ><span class="green">《医脉通网络服务协议》</span></a>
	                </div>
	                <div class="remind_pleft1">
	                	<p><input type="radio" id="agree_medlive_reg1" onclick="agree_medlive_reg(1)" /><label class="pleft10">我同意成为医脉通用户</label></p>
	                    <p class="remind_pleft2" id="agree_medlive_message" style="display:none;">正在为您注册医脉通请稍后...</p>
	                    <p><input type="radio" id="agree_medlive_reg2" onclick="agree_medlive_reg(2)" /><label class="pleft10">以后再考虑;（温馨提示：医脉通麦粒只能在医脉通积分商城使用，故非医脉通用户暂不能使用；）</label></p>
	                </div>
	            </div>
	 			 <div class="question_btn_box">
	                <input type="button" class="question_btn" style="background-color:#ababab;"id="submit_reg_medlive" disabled onclick="window.location.href='/user/agree-reg-medlive-submit'" value="确定并兑换礼品" />
	            </div>  
	            <div class="center">
	                注：活动时间：2016年1月1日-2016年1月12日
	            </div>
	        </div>             
	    </div>
	    <div class="pop_info_btm"></div>
	</div>
</div>
@endif
<script>
//同意注册医脉通（flag=1），或者以后考虑(flag=2)，或者直接关闭弹框（flag=3）
function agree_medlive_reg(flag){
	if(flag==1){
		//同意注册
		$('#agree_medlive_reg2').attr('checked',false);
		$('#agree_medlive_message').show();
		$('#agree_medlive_reg2').attr("disabled", true);	//以后再考虑单选框不可以点
	}else if(flag==2){
		//以后考虑
		$('#agree_medlive_reg1').attr('checked',false);
		$('#submit_reg_medlive').attr("disabled", true);	//确定按钮不可以点
		$('#submit_reg_medlive').css('background-color','#ababab');
		$('#agree_medlive_message').hide();					//隐藏掉同意下面的字
	}else if(flag == 3){
		//用户点击的是关闭
		$('#survey_success_box').hide();
	}
	url = "/user/agree-medlive-reg/"+flag;
	$.post(url,{},function(msg){
		if(flag==1){
			//用户信息回显
			$('#agree_medlive_message').html(msg);
			//确定按钮可以点
			$('#submit_reg_medlive').attr("disabled", false);
			$('#submit_reg_medlive').css('background-color','#0090d3');
			$('#agree_medlive_reg2').attr("disabled", false);	//以后再考虑单选框可以点
		}
	});
}
//调研提交
function submit_survey(){
	$('#survey_form').submit();
}
//为调研绑定提交事件
$(document).ready(function(){
    $('#survey_form').bind('submit', function(){
        ajaxSubmit(this, function(json){
            data = eval("("+json+")");
            if(data.success==false){
                alert(data.msg);
                return;
            }else if(data.success==true){
            	$('#survey_submit_success_box').show();
				$('#survey_box').hide();
            }else{
                alert('网络错误，请刷新当前页面重试，如有问题可拨打电话联系我们');
                return;
            }
        });
        return false;
    });
});

//提交调研后的确认弹框,有麦粒奖励
function survey_submit_box(iSet){
	$('#survey_submit_success_box').hide();
	if(iSet==0){
		$('#survey_success_box').show();
	}
}

//用户直接关闭调研弹框
function pop_survey_close(){
	$('#survey_box').hide();
	url = "/user/survey-close";
	$.post(url,{},function(){
		
	});
}
//将form转为AJAX提交
function ajaxSubmit(frm, fn) {
    var dataPara = getFormJson(frm);
    $.ajax({
        url: frm.action,
        type: frm.method,
        data: dataPara,
        success: fn
    });
}

//将form中的值转换为键值对。
function getFormJson(frm) {
    var o = {};
    var a = $(frm).serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });

    return o;
}
</script>