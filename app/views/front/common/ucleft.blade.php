<div class="left">
    <ul>
        <li><a href="/user"@if($nowpos=='index') class="sele"@endif>
            <span class="@if($nowpos=='index') personal_icon1_1 @else personal_icon1 @endif">个人信息</span>
        </a></li>
        <li><a href="/user/my-score"@if($nowpos=='myscore') class="sele"@endif>
            <span class="@if($nowpos=='myscore') personal_icon2_2 @else personal_icon2 @endif">我的积分</span>
        </a></li>
        <li><a href="/user/my-health-score"@if($nowpos=='myhealthscore') class="sele"@endif>
            <span class="@if($nowpos=='myhealthscore') personal_icon10_10 @else personal_icon10 @endif">健康云积分</span>
        </a></li>
        @if(Auth::User()->role_id==3)
        <!-- <li><a href="/aspirin/my-auth"@if($nowpos=='myauth') class="sele"@endif> -->
        <li><a href="javascript:alert('请前往国卫健康云APP进行认证');"@if($nowpos=='myauth') class="sele"@endif>
            <span class="@if($nowpos=='myauth') personal_icon3_3 @else personal_icon3 @endif">我的认证</span>
        </a></li>
        @endif
        <li><a href="/aspirinonline/my-enter-list"@if($nowpos=='myenterlist') class="sele"@endif>
            <span class="@if($nowpos=='myenterlist') personal_icon4_4 @else personal_icon4 @endif">我的会议</span>
        </a></li>
        <li><a href="/aspirinoffline"@if($nowpos=='aspirinoffline') class="sele"@endif>
            <span class="@if($nowpos=='aspirinoffline') personal_icon5_5 @else personal_icon5 @endif">线下会议</span>
        </a></li>
        <!-- <li><a href=""><span class="personal_icon6">视频下载</span></a></li> -->
        <li><a href="/aspirinresearch/my-list"@if($nowpos=='aspirinresearchlist') class="sele"@endif>
            <span class="@if($nowpos=='aspirinresearchlist') personal_icon7_7 @else personal_icon7 @endif">科研培训</span>
        </a></li>
        <li><a href="/aspirin/contact"@if($nowpos=='contact') class="sele"@endif>
            <span class="@if($nowpos=='contact') personal_icon8_8 @else personal_icon8 @endif">联系客服</span>
        </a></li>
        @if(Auth::User()->role_id==2)
        <li><a href="/user/my-train"@if($nowpos=='mytrain') class="sele"@endif>
            <span class="@if($nowpos=='mytrain') personal_icon9_9 @else personal_icon9 @endif">我的培训</span></a></li>
        @endif
        <!--  
        @if(Auth::User()->role_id==2)
        <li><a href="/user/my-order"><span class="icon04">我的预约</span></a></li>
        @endif
        <li><a href="/user/caselist/1"><span class="icon06">我的病例</span></a></li>
        -->
        @if(Auth::User()->role_id==2)
        <li><a href="/user/mydoclist"@if($nowpos=='mydoclist') class="sele"@endif><span class="icon09">我的医生</span></a></li>
        @endif
    </ul>
</div>