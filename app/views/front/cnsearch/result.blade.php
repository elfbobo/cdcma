@extends('front.common.layout')
@section('title')文献检索-国卫健康云 @stop
@section('description')文献检索-国卫健康云@stop
@section('keywords')文献检索-国卫健康云 @stop
@section('content2')
<input type="hidden" id="term_one" value="{{$term_one}}"/>
<input type="hidden" id="keyword_one" value="{{$keyword_one}}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pubmed/basic.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pubmed/pubmed.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pubmed/med_search.css') }}" />
<script src="{{ asset('assets/js/blockui/jquery.blockUI.js') }}"></script>
<script src="{{ asset('assets/js/pubmed/cnsearch_function.js') }}"></script>
<script src="{{ asset('assets/js/pubmed/cn_search.js') }}"></script>
<script>
	var cnSearch = {
	    load: function() {
	        cnSearch.load_cnsearch()
	    },
	    load_cnsearch: function() {
	        var q = "";
	        var term_one = decodeURIComponent($("#term_one").val());
	        var keyword_one = decodeURIComponent($("#keyword_one").val());
	        var myDate = new Date();
	    	var endYear = myDate.getFullYear();
	    	var page = $("#page").val();
	        var db = "wf_qk";
	        showPropWindow();
	        var data = {
	                type: "ajax",
	                action: "cnsearch",
	                db: db,
	                page: page,
	                term_one: term_one,
	                dark_exact_one: 'dark',
	                keyword_one: keyword_one,
	                yhf_one:'*',
	                endYear:endYear,
	                db: db
	        };
	        $.post(ajaxCnSearchUrl, data,
	        function(html) {
	            op();
	            $("#result").html(html);
	            $("#pagination").find("a").click(function() {
	                $("#page").val(this.rel);
	                cnSearch.load_cnsearch()
	            })
	        })
	    }
	};
	var ajaxCnSearchUrl = "/cnsearch/ajax";
	$(document).ready(function() {
	    cnSearch.load()
	});
</script>
	<div class="p_sml_menu">
    	您当前位置：
    	<a href="/">首页</a>
        >
        文献检索
    </div>    
    <div class="wid1025 pbtm40">	
    	<div class="p_box">
    		<div class="p_countdown_box" style="background: url('/assets/images/front/web/box_cont.png') repeat-y scroll 0 0;padding: 0 10px;">
	            <input type="hidden" id='page' value="1"/>
				<div id="result" class="page_content clearfix"></div>
			    <div style="display:none" id="runMsg" onclick="closeLoading();">
				  <div class="box_message">
					<div class="box_message_title clearfix">
						<div class="left_line left"></div>
						<div class="title_box left">
							<div class="left">提示：(点击关闭窗口)</div>
							<a id="close_loading"></a>
						</div>
						<div class="right_line right"></div>
					</div>
					<div class="box_message_bd">
			          <img src="/assets/images/pubmed/load.gif"/>&nbsp;&nbsp;加载中...</div>
					</div>
				 </div>
	        </div>
    	</div>
    	<div class="p_box_btm"></div>
	</div>
@stop