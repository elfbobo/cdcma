@extends('front.common.layout')
@section('title')文献检索-国卫健康云 @stop
@section('description')文献检索-国卫健康云@stop
@section('keywords')文献检索-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pubmed/basic.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pubmed/pubmed.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pubmed/med_search.css') }}" />
<script src="{{ asset('assets/js/blockui/jquery.blockUI.js') }}"></script>
<script src="{{ asset('assets/js/pubmed/cnsearch_function.js') }}"></script>
<script src="{{ asset('assets/js/pubmed/cn_search.js') }}"></script>
<div class="p_sml_menu">
    	您当前位置：
    	<a href="/">首页</a>
        >
        文献检索
    </div>    
    
    <div class="wid1025 pbtm40">	
    	<div class="p_box">
    		<div class="p_countdown_box" style="background: url('/assets/images/front/web/box_cont.png') repeat-y scroll 0 0;padding: 0 10px;">
        	<div class="top_line3" id="nav_pubmed">
            	<div class="docum_btn_left">
                	<label><a href='/pmd' style="text-decoration: none; color:black;">PubMed</a></label>
                </div>
                <div class="docum_btn_right current">
                	<label>中文文献</label>
                </div>
                <div class="clear"></div>
            </div>
            <div id="wrap" style="width:987px;">
            <div class="list_body">
            <form accept-charset="utf-8">
		     <!-- //中文 检索一条件 START -->	
		      <div class="pubmed_search_content" id="advanceQueryDiv_0">
		        <div class="restrictDiv">        
		         <div class="title">
		           <div class="btn_box">
		               <button type="button" onclick="doClearDiv('cnFieldDiv');">清空</button>
		            </div>
		         </div>
		         <div id="cnFieldDiv"> 
					<table cellspacing="0" cellpadding="0" width="98%" style="margin:0;">
						<tbody>
							
							<tr>
								<td>
								  <select class="selectBox_long" id="term_one" name="term_one">
                                      {{$selOptionTerm}}
								  </select>
								</td>
								<td><select class="selectBox" id="dark_exact_one" name="dark_exact_one">{{$selOptionDE}}</select></td>
								<td><input type="text" id="keyword_one" name="keyword_one" value="" /></td>
								<td><select class="selectBox" id="yhf_one" name="yhf_one">{{$selOptionYHF}}</select></td>
							</tr>
							
							<tr>
								<td>
								  <select  class="selectBox_long" id="term_two" name="term_two">
									  {{$selOptionTerm}}
								  </select>
								</td>
								<td><select class="selectBox" id="dark_exact_two" name="dark_exact_two">{{$selOptionDE}}</select></td>
								<td><input type="text" id="keyword_two" name="keyword_two" value="" /></td>
								<td><select class="selectBox" id="yhf_two" name="yhf_two">{{$selOptionYHF}}</select></td>
							</tr>
							
							<tr>
								<td>
								  <select  class="selectBox_long" id="term_three" name="term_three">
									{{$selOptionTerm}}
								  </select>
								</td>
								<td><select class="selectBox" id="dark_exact_three" name="dark_exact_three">{{$selOptionDE}}</select></td>
								<td><input type="text" id="keyword_three" name="keyword_three" value="" /></td>
								<td><select class="selectBox" id="yhf_three" name="yhf_three">{{$selOptionYHF}}</select></td>
							</tr>
							
							<tr>
								<td>
								  <select  class="selectBox_long" id="term_four" name="term_four">
									{{$selOptionTerm}}
								  </select>
								</td>
								<td><select class="selectBox" id="dark_exact_four" name="dark_exact_four">{{$selOptionDE}}</select></td>
								<td><input type="text" id="keyword_four" name="keyword_four" value="" /></td>
								<td><select class="selectBox" id="yhf_four" name="yhf_four">{{$selOptionYHF}}</select></td>
							</tr>
		
							<tr>
								<td>
								  <select  class="selectBox_long" id="term_five" name="term_five">
									{{$selOptionTerm}}
								  </select>
								</td>
								<td><select class="selectBox" id="dark_exact_five" name="dark_exact_five">{{$selOptionDE}}</select></td>
								<td><input type="text" id="keyword_five" name="keyword_five" value="" /></td>
								<td><select class="selectBox" id="yhf_five" name="yhf_five">{{$selOptionYHF}}</select></td>
							</tr>
							
							<tr>
								<td>
								  <select  class="selectBox_long" id="term_six" name="term_six">
									{{$selOptionTerm}}
								  </select>
								</td>
								<td><select class="selectBox" id="dark_exact_six" name="dark_exact_six">{{$selOptionDE}}</select></td>
								<td><input type="text" id="keyword_six" name="keyword_six" value="" /></td>
								<td><select class="selectBox" id="yhf_six" name="yhf_six">{{$selOptionYHF}}</select></td>
							</tr>
							
							<tr>
								<td>
								    <input type="checkbox" checked="checked" id="checkboxYear" name="checkboxYear"/>
							     	<select class="selectBox" id="startYear" name="startYear">{{$selOptionStartYear}}</select>
							     	&nbsp;-&nbsp;
							     	<select class="selectBox" id="endYear" name="endYear">{{$selOptionEndYear}}</select>
								</td>
							</tr>
							
						</tbody>
					  </table>
		              <p class="center">
		                <input type="button"  class="btn"
							name="cn_search" id="cn_search" value="我要检索"
							style="cursor: pointer" />
		               </p>
		             </div>   
		             </div>  
		            </div> 
            <!-- //中文文献 检索一条件 END -->	
		      
			</form>
          	</div>
            <div class="bottom_line"></div>
            </div>
            <input type="hidden" id='page' value="1"/>
			<div id="result" class="page_content clearfix"  style="display: none;"></div>
		    <div style="display:none" id="runMsg" onclick="closeLoading();">
			  <div class="box_message">
				<div class="box_message_title clearfix">
					<div class="left_line left"></div>
					<div class="title_box left">
						<div class="left">提示：(点击关闭窗口)</div>
						<a id="close_loading"></a>
					</div>
					<div class="right_line right"></div>
				</div>
				<div class="box_message_bd">
		          <img src="/assets/images/pubmed/load.gif"/>&nbsp;&nbsp;加载中...</div>
				</div>
			 </div>
        </div>
    </div>
    <div class="p_box_btm"></div>
</div>
@stop