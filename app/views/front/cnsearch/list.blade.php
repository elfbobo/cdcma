<div class="page_content clearfix" style="background:#fff;">
	<div class="content_area layout_r290">
		<div class="auto_box_cyan literature_topBlock pubmed_search" style="margin-bottom:0;">
			<div class="abox_body">
				<div class="abox_body_innerbox">
					<p>
						<input id="cn_all" type="checkbox" onclick="select_all('cn')">全选本页
						<button class="btn" onclick="selectCancel('cn')" type="button">清空选项</button>
						@if(Config::get('config.is_pub_email_open'))
						<button class="btn" onclick="help_text('cn_num')" type="button">批量求助</button>
						@endif
					</p>
					
					<div class="topic">
					检索表达式：{{$q}}&nbsp;&nbsp;
					共命中<font color="red">{{$pageNum}}</font>页<font color="red">{{$total}}</font>条记录
						<div class="right">
							<a href="javascript:goSearch()" style="cursor: pointer;">修改检索条件</a>
						</div>
					</div>
					
					<div class="content" style="background:#fff;">
						<div class="hd">
							<div id="div_data">
								<input name="sel_pmids" id="sel_pmids" type="hidden" value="" />
								@if ($results)
								 @foreach ($results as $key=>$val)
								    @if (is_numeric($key))
								        <div class="list clearfix">
										<input class="left" name="cn_num" type="checkbox"
											value="{{$val['helptext']}}" />
										<div class="info">
											<h3>
												<span>  
												@if($total>$pageSize)
													{{++$key+(intval($page)-1)*$pageSize}}
												@else
													{{++$key}}
												@endif
												 . </span> 			
												<strong>{{$val['title']}}</strong>
											</h3>
											
										 @if (isset($val['source']))		
											<p>
												<span class="bold">出处：</span> 
												<span> {{$val['source']}}</span>
											</p>
										 @endif	
										 	
										 @if (isset($val['author']))	
											<p>
												<span class="bold">作者：</span> <span class="gray"> {{$val['author']}}
												</span>
											</p>
										 @endif	
										 
										 @if (isset($val['description']))
											<p>
											  <span class="bold">摘要：</span><span>{{$val['description']}}</span>
											</p>
										 @endif	
										 
										 @if (isset($val['keywords']))	
											<p>
											   <span class="bold">关键词：</span><span>{{$val['keywords']}}</span>
											</p>
										 @endif
										 @if(Config::get('config.is_pub_email_open'))
										  <p>
											<input onclick="javascript:help_one_text('{{$val['helptext']}}')"
												style="cursor: pointer;" type="button" value="全文求助" />
										  </p>
										 @endif
										</div>
									</div>
								    @endif
								 @endforeach
								<div class="p_number_box">
            						<div id="pagination">{{$pages}}</div>
								</div>
								@else
									<div class="list clearfix">
										<div class="info">
											未命中记录
										</div>
									</div>
								@endif
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>