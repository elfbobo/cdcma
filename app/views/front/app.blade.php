<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>国卫健康云</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" type="text/css" href="/assets/css/aspirin.css" />
<script src="http://libs.baidu.com/jquery/1.7.2/jquery.js"></script>
</head>
<body>
<?php
$agent = strtolower ( $_SERVER ['HTTP_USER_AGENT'] );
$is_iphone = ((strpos ($agent, 'iphone' )) || (strpos ($agent, 'iph' )))? true : false;
$is_ipad = (strpos ($agent, 'ipad' )) ? true : false;
$is_android = ((strpos ($agent, 'android' )) || (strpos($agent, 'adr' )))? true : false;
?>
@if(user_agent_is_weixin())
<div class="cover" style=" display:block;"></div>
    <div class="prompt_box" style=" display:block;">
    	<div class="prompt_pic">
        	<img src="/assets/images/aspirin/arrow.png" />
        </div>
        <div class="prompt_txt">
        	@if($is_iphone || $is_ipad)
        	<p>戳右上角，选择“<span>在Safari中打开</span>”</p>
        	@else
        	<p>戳右上角，选择“<span>在浏览器中打开</span>”</p>
        	@endif
            <p>即可完成下载，等你哦！</p>
        </div>
    </div>
@endif
	<div class="page_top_box">
        国卫健康云
        <input type="button" class="btn_share" style="display:none;" />
    </div>
    <div class="page_cont_box">
        
        <div class="download_box">
        	<div class="invite_code">&nbsp;</div>
            <div class="download_btn">
            @if($is_iphone || $is_ipad)
            <A href="https://fir.im/nlg6">
            	<input class="download_btn" type="button" value="下载国卫健康云APP" />
            </A>
            @else
            <A display="cursor:pointer;" onclick="window.location.href='http://cdcma.bizconf.cn/upload/cdcma.apk';">
            	<input class="download_btn" type="button" value="下载国卫健康云APP" />
            </A>
            @endif
            </div>
        </div>
        
    </div>
</body>
</html>