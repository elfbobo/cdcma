@extends('front.common.layout')
@section('title')文献检索-国卫健康云 @stop
@section('description')文献检索-国卫健康云@stop
@section('keywords')文献检索-国卫健康云 @stop
@section('content2')
<script src="{{ asset('assets/js/pubmed/pubmed_function.js') }}"></script>
<script src="{{ asset('assets/js/pubmed/pubmed.js') }}"></script>
<div class="p_sml_menu">
    	您当前位置：
    	<a href="/">首页</a>
        >
        文献检索
    </div>    
    <div class="wid1025 pbtm40">	
    	<div class="p_box">
    		<div class="p_countdown_box" style="background: url('/assets/images/front/web/box_cont.png') repeat-y scroll 0 0;padding: 0 10px;">
        	<div class="top_line2"></div>
            <div class="list_body pb20">
            	<div class="docum_title">{{$result['title']}}</div>
                <div class="docum_quarry">
	                {{$result['journalabbrev']}}
	                {{$result['year']}}
	                {{$result['month']}}
	                {{$result['day']}};
	                ({{$result['issue']}});
	                {{$result['pages']}}
	                <a href="{{PUBMED_SHOW_URL}}{{$result['pmid']}}" target="_blank">PMID:{{$result['pmid']}}<span class="icon"></span>
					</a>
                </div>
                <div class="docum_quarry">
                       {{$result['authors']}}
                </div>
                <div class="docum_quarry docum_hui">{{$result['affiliation']}} </div>
                <div class="docum_mold">
                	<p><span>文献类型：</span>其他</p>
                    <p>
                     <span>主要主题词：
                         {{$result['keywords']}}
                    </span></p>
                </div>
                @if($result['abstract'])
                <div class="docum_digest">
                	<div class="digest_title">摘要</div>
                	<p>{{$result['abstract']}}</p>
                </div>
                @endif
                @if(Config::get('config.is_pub_email_open'))
                <a class="docum_download mt20" style="cursor: pointer;" onclick="javascript:help_one_text('{{$result['helptext']}}')"></a>
          		@endif
          	</div>
            <div class="bottom_line"></div>
        </div>
        <div class="p_box_btm"></div>
    </div>
</div>
@stop