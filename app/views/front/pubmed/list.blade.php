<div class="page_content clearfix">
	<div class="content_area layout_r290">
		<div class="auto_box_cyan literature_topBlock pubmed_search" style="margin-bottom:0;">
			<div class="abox_body" style=" background:#fff">
				<div class="abox_body_innerbox">
					<div class="topic">
						<div class="left" id="queryTermDiv">
							<span class="field">[检索策略]</span> <span> {{$term}} </span>
						</div>
						<div class="right">
							<a href="javascript:goSearch()" style="cursor: pointer;">修改检索条件</a>
						</div>
					</div>
					<p>
						<input id="pubmid_all" type="checkbox"
							onclick="select_all('pubmid')">全选本页
						<button class="btn" onclick="selectCancel('pubmid')" type="button">清空选项</button>
						@if(Config::get('config.is_pub_email_open'))
						<button class="btn" onclick="help_text('pubmid_num')" type="button">批量求助</button>
						@endif
					</p>
					<div class="content" style="background:#fff;">
						<div class="hd">
							<div class="title_area">
								<div id="li_all" class="title">
									<div class="head"></div>
									<div class="fill">
										<a id="count_all" title="点击浏览所有查询结果"
											style="text-decoration: none" href="javascript:void(0);">所有:
											@if($total) {{$total}} @else 0 @endif </a>
									</div>
									<div class="tail"></div>
								</div>
							</div>
							<input type="hidden" id='hidden_term' value="{{$term}}">
							<div id="div_data">
								<input name="sel_pmids" id="sel_pmids" type="hidden" value="" />
								
								@if(!empty($results))
								@foreach($results as $key=>$val)
								<div class="list clearfix">
									<input class="left" name="pubmid_num" type="checkbox"
										value="{{$val['helptext']}}" />
									<div class="info">
										<h3>
											<span> @if($total>$pageSize)
												{{++$key+(intval($page)-1)*$pageSize}} @else {{++$key}}
												@endif . </span> 
												<a target="_blank" class="en" href="{{URL::to('/')}}/pmd/show/{{$val['pmid']}}/{{time()}}">
												   {{$val['title']}} 
												</a>
										</h3>
										<p>
											<span class="bold">出处：</span> <span> {{$val['journal']}}
												{{$val['year']}} {{$val['month']}}; {{$val['volume']}}
												@if($val['issue']) ({{$val['issue']}}): @endif
												{{$val['pages']}} </span>
										</p>

										<p>
											<span class="bold">作者：</span> <span class="gray">
												{{$val['authors']}} </span>
										</p>
										@if(Config::get('config.is_pub_email_open'))
										<p>
											<input onclick="javascript:help_one_text('{{$val['helptext']}}')"
												style="cursor: pointer;" type="button" value="全文求助" />
										</p>
										@endif
									</div>
								</div>
								@endforeach
								<div class="p_number_box">
            						<div id="pagination">{{$pages}}</div>
								</div>
								@else
								   	<div class="list clearfix">
										<div class="info">
											未命中记录
										</div>
									</div>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>