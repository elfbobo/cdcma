@extends('front.common.layout')
@section('title')文献检索-国卫健康云 @stop
@section('description')文献检索-国卫健康云@stop
@section('keywords')文献检索-国卫健康云 @stop
@section('content2')
<script src="{{ asset('assets/js/DatePicker/WdatePicker.js')}}" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pubmed/basic.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pubmed/pubmed.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pubmed/med_search.css') }}" />
<script src="{{ asset('assets/js/pubmed/pubmed_function.js') }}"></script>
<script src="{{ asset('assets/js/pubmed/pubmed.js') }}"></script>
<script src="{{ asset('assets/js/blockui/jquery.blockUI.js') }}"></script>
        <div class="p_sml_menu">
    	您当前位置：
    	<a href="/">首页</a>
        >
        文献检索
    </div>    
    <div class="wid1025 pbtm40">	
    	<div class="p_box">
    		<div class="p_countdown_box" style="background: url('/assets/images/front/web/box_cont.png') repeat-y scroll 0 0;padding: 0 10px;">
        	<div class="top_line3" id="nav_pubmed">
            	<div class="docum_btn_left current">
                	<label>PubMed</label>
                </div>
                <div class="docum_btn_right">
                	<label><a href='/cnsearch' style="text-decoration: none; color:black;">中文文献</a></label>
                </div>
                <div class="clear"></div>
            </div>
            <div id="wrap" style="width:987px;" >
            <div class="list_body"  >
            <form accept-charset="utf-8">
            
		      <!-- //pubmed 检索一条件 START -->	
		      <div class="pubmed_search_content" id="advanceQueryDiv_0" style="padding-top:10px;">
		        <div class="restrictDiv">
		        <div class="title">
		           <div class="txt_change">
		               <span id="span0" style="cursor:pointer" onclick="togglePmdDiv(this,'fieldDiv');exsimpleDivClick(this);" class="minus"></span>
		               <span>高级检索_检索字段、日期等限定</span>
		           </div>
		           <div class="btn_box">
		               <button onclick="doClearDiv('fieldDiv');" type="button">清空</button>
		            </div>
		         </div>
		         <div id="fieldDiv">
					<table cellspacing="0" cellpadding="0" width="98%" >
						<tbody>
							<tr>
								<td width="10%">关联符</td>
								<td width="30%">检索字段</td>
								<td width="60%">检索词</td>
							</tr>
							<tr>
								<td><select class="selectBox" id="r_f0" name="ra0">{{$sChar}}</select></td>
								<td>
								  <select class="selectBox_long" id="fi_f0" name="fi_f0" onChange="changeSuggestDic('fi_f0','keyword_fi0')">
                                 {{$sel0}}
								  </select>
								</td>
								<td><input type="text" id="keyword_fi0" name="keyword_fi0" value="" sgDict="keyword" autoComplete="off" class="keywordSuggest"/></td>
							</tr>
							<tr>
								<td><select class="selectBox" id="r_f1" name="ra1">{{$sChar}}</select></td>
								<td>
								  <select  class="selectBox_long" id="fi_f1" name="fi_f1" changeSuggestDic('fi_f1','keyword_fi1')">
                                     {{$sel1}}
								  </select>
								</td>
								<td><input type="text" id="keyword_fi1" name="keyword_fi1" value="" sgDict="jrz" autoComplete="off" class="keywordSuggest"/></td>
							</tr>
							<tr>
								<td><select class="selectBox" id="r_f2" name="ra2">{{$sChar}}</select></td>
								<td>
								  <select  class="selectBox_long" id="fi_f2" name="fi_f2" onChange="changeSuggestDic('fi_f2','keyword_fi2')">
									 {{$sel2}}
								  </select>
								</td>
								<td><input type="text" id="keyword_fi2" name="keyword_fi2" value="" sgDict="keyword" autoComplete="off" class="keywordSuggest"/></td>
							</tr>
							<tr>
								<td><select class="selectBox" id="r_f3" name="ra3">{{$sChar}}</select></td>
								<td>
								  <select  class="selectBox_long" id="fi_f3" name="fi_f3" onChange="changeSuggestDic('fi_f3','keyword_fi3')">
									  {{$sel3}}
								  </select>
								</td>
								<td><input type="text" id="keyword_fi3" name="keyword_fi3" value="" sgDict="keyword" autoComplete="off" class="keywordSuggest"/></td>
							</tr>
		
							<tr>
								<td><select class="selectBox" id="r_f4" name="ra4">{{$sChar}}</select></td>
								<td>
								  <select  class="selectBox_long" id="fi_f4" name="fi_f4" onChange="changeSuggestDic('fi_f4','keyword_fi4')">
                                      {{$sel4}}
								  </select>
								</td>
								<td><input type="text" id="keyword_fi4" name="keyword_fi4" value="" sgDict="auf" autoComplete="off" class="keywordSuggest"/></td>
							</tr>
						</tbody>
					</table>
					<div class="search_condition">
		             <table cellspacing="0" cellpadding="0" style="margin-left:20px;">
		                  <tbody>
		                      <tr style="disply:block; margin-left:10px;">
		                        <td width="112px" valign="top">
		                           <label>中国期刊中文名称</label>
		                        </td>
		                        <td width="430px">
		                           <input type="text" style=" width:308px;width:299px\9;  " class="keywordSuggest txt_input" autocomplete="off" sgdict="keyword_jourcn" id="jour_cn" name="jour_cn"/>
		                           <p class="center">例如：<span class="blue bold">第一军医大学学报</span></p>
		                        </td>
		                        <td width="112px" valign="top">
		                          <label>中国作者中文名称</label>
		                        </td>
		                        <td width=" ">
		                          <input type="text" class="txt_input" autocomplete="off" id="auth_cn" name="auth_cn" style="width:181px;width:175px\9;"/>
		                          <p class="center" style="display: none;">例如：<span class="blue bold">张三</span></p>
		                        </td>
		                      </tr>
		                      <tr>
		                        <td colspan="4">
		                          <input type="checkbox" id="term_loattrfull" name="term_loattrfull" value="1"/>有全文链接&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							      <input type="checkbox" class="ml25px" id="term_loattrfree" name="term_loattrfree" value="1"/>有免费全文链接&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							      <input type="checkbox" class="ml25px" id="term_hasabstract" name="term_hasabstract" value="1"/>有摘要&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							      <input type="checkbox" class="ml25px" id="term_isReview" name="term_isReview" value="1"/>文献类型为综述&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							      <input type="checkbox" class="ml25px" id="term_isF1000M" name="term_isF1000M" value="1"/>被F1000M收录
							    </td>
		                      </tr>
		                      <tr>
		                        <td colspan="2">
		                          <label>出版日期：</label>
		                          <input size="25"
									require="true" class="Wdate txt_input short" type="text"
									name="fromDate" id="fromDate"
									value=""
									onFocus="WdatePicker({isShowClear:true,readOnly:true})"/> <!-- <span class="date"></span>  -->—
									<input size="25" class="Wdate txt_input short" require="true" type="text"
									name="toDate" id="toDate"
									value=""
									onFocus="WdatePicker({isShowClear:true,readOnly:true})"/>
		                       </td>
		                        <td colspan="2"><label>PubMed 收录日期：</label><select name="updateDate" id="updateDate" style="width:190px;width:187px\9;">
		                            <option value="0">无限制</option>
		                            <option value="30">30天内</option>
		                            <option value="60">60天内</option>
		                            <option value="90">90天内</option>
		                            <option value="180">180天内</option>
		                            <option value="1">1年内</option>
		                            <option value="2">2年内</option>
		                            <option value="5">5年内</option>
		                            <option value="10">10年内</option>
		                          </select></td>
		                      </tr>
		                      <tr>
		                        <td colspan="2"><label>排序条件：</label>
		                          <select  name="display_sort" id="display_sort" style="width:361px;width:364px\9;">
		                            <option value="">PMID</option>
		                            <option value="author">作者</option>
		                            <option value="last+author">最后作者</option>
		                            <option value="journal">期刊名称</option>
		                            <option value="pub+date">出版日期</option>
		                          </select></td>
		                        <td colspan="2"><label>每页显示的记录数：</label><select id="display_rowsPerPage" name="display_rowsPerPage" style="width:187px;">
		                            <option selected="selected" value="10">10项</option>
		                            <option value="20">20项</option>
		                            <option value="50">50项</option>
		                            <option value="100">100项</option>
		                          </select></td>
		                      </tr>
		                    </tbody>
		                    </table>
		                 </div>
		                  <p class="center">
		                    <input type="button"  class="btn"
									name="advanced_search" id="advanced_search" value="我要检索"
									style="cursor: pointer" />
		                  </p>
		             </div>   
		             </div>  
		            </div> 
                  <!-- //pubmed 检索一条件 END -->
                  
                  <div class="abox_body">
				   <div class="abox_body_innerbox clearfix">
					<div id="advanceQueryDiv_0" class="pubmed_search_content" style="padding-top:0">
						<div class="restrictDiv high_restrictDiv clearfix"
							id="advanceQueryDiv_1">
							<div class="title">
								<div class="txt_change">
									<span id="span1" style="cursor: pointer"
										onclick="javascript:togglePmdDiv(this,'advanceDiv');exsimpleDivClick(this);"
										class="add"></span><span>高级检索_研究对象、语种、文献类型限定</span>
								</div>
								<div class="btn_box">
									<button onclick="doClearDiv('advanceQueryDiv_1');"
										type="button">清空</button>
								</div>
							</div>
							<div class="content clearfix" id="advanceDiv" style="display:none;">
								<div class="search_condition">
									<div class="title">
										<h4>研究对象人类/动物限定</h4>
										<a class="btn_delete"
											href="javascript:doClearDiv('div_humansAnimals');">清除</a>
									</div>
									<div class="condition" id="div_humansAnimals">
										<div>
											<input type="checkbox" name="humans_animals" value="humans"
												class="checkBox"/> 人类
										</div>
										<div>
											<input type="checkbox" name="humans_animals" value="animals"
												class="checkBox"/> 动物
										</div>
									 </div>
								 </div>
								
								<div class="search_condition">
									<div class="title">
										<h4>研究对象性别限定</h4>
										<a class="btn_delete"
											href="javascript:doClearDiv('div_gender')">清除</a>
									</div>
									<div class="condition" id="div_gender">
										<div>
											<input type="checkbox" name="gender" value="male"
												class="checkBox"/> 男
										</div>
										<div>
											<input type="checkbox" name="gender" value="female"
												class="checkBox"/> 女
										</div>
									</div>
								</div>
								
								<div class="search_condition">
									<div class="title">
										<h4>文献类型限定</h4>
										<a class="btn_delete"
											href="javascript:doClearDiv('div_article_type');">清除</a>
									</div>
									<div class="condition fix_content" id="div_article_type">
							              {{$sDocType}}
									</div>
								</div>
								
								<div class="search_condition">
									<div class="title">
										<h4>语种限定</h4>
										<a class="btn_delete"
											href="javascript:doClearDiv('div_language');">清除</a>
									</div>
									<div class="condition fix_content" id="div_language">
										 {{$sLang}}
									</div>
								</div>
								
								<div class="search_condition">
									<div class="title">
										<h4>子文档限定</h4>
										<a class="btn_delete"
											href="javascript:doClearDiv('div_subsets');">清除</a>
									</div>
									<div class="condition fix_content" id="div_subsets">
										 {{$sSubset}}
									</div>
								</div>
								
								<div class="search_condition">
									<div class="title">
										<h4>研究对象年龄限定</h4>
										<a class="btn_delete" href="javascript:doClearDiv('div_age');">清除</a>
									</div>
									<div class="condition fix_content" id="div_age">
										 {{$sAge}}
									</div>
								</div>
								<div class="clear"></div>
								<p class="center">
									<button class="btn" type="button" id="advanced2_search" name="advanced2_search" style="cursor:pointer;">我要检索</button>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			</form>
          	</div>
            <div class="bottom_line"></div>
            </div>
            <input type="hidden" id='page' value="1"/>
			<input type="hidden" id='page1' value="1"/>
			<input type="hidden" id='page2' value="1"/>
			<div id="result" class="page_content clearfix"  style="display: none;"></div>
		    <div style="display:none" id="runMsg" onclick="closeLoading();">
			  <div class="box_message">
				<div class="box_message_title clearfix">
					<div class="left_line left"></div>
					<div class="title_box left">
						<div class="left">提示：(点击关闭窗口)</div>
						<a id="close_loading"></a>
					</div>
					<div class="right_line right"></div>
				</div>
				<div class="box_message_bd">
		          <img src="/assets/images/pubmed/load.gif"/>&nbsp;&nbsp;加载中...</div>
				</div>
			 </div>
        </div>
    </div>
    <div class="p_box_btm"></div>
</div>
<p id="jour"></p>
<script src="{{ asset('assets/js/pubmed/jquery.autocomplete-min.js') }}"></script>
<script src="{{ asset('assets/js/pubmed/jquery.bgiframe.min.js') }}"></script>
<script src="{{ asset('assets/js/pubmed/common_pubmed.js') }}"></script>
<script src="{{ asset('assets/js/pubmed/pubmed_search.js') }}"></script>
 <script type="text/javascript">
   	jQuery(document).ready(function(){
		init();
		jQuery('#searchAll').keydown(function(event){
	   		if(event.keyCode == 13){jQuery("#startsearch").trigger("onclick");   }
	   	});
	});
</script>
@stop