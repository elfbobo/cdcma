@extends('front.common.layout')
@section('title')自助会议平台-国卫健康云 @stop
@section('description')自助会议平台-国卫健康云@stop
@section('keywords')自助会议平台-国卫健康云 @stop
@section('content2')
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/meeting.js"></script>
<!-- date -->
<link type="text/css" href="{{ asset('assets/js/date/css/jquery-ui-1.8.17.custom.css') }}" rel="stylesheet" />
<link type="text/css" href="{{ asset('assets/js/date/css/jquery-ui-timepicker-addon.css') }}" rel="stylesheet" />
<script type="text/javascript" src="{{ asset('assets/js/date/js/jquery-ui-1.8.17.custom.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/date/js/jquery-ui-timepicker-addon.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/date/js/jquery-ui-timepicker-zh-CN.js') }}"></script>
<script type="text/javascript">
    $(function () {
        $(".ui_timepicker").datetimepicker({
    		showHour: false,
    		showMinute: false,
            showSecond: false,
            showTime: false,
            timeFormat: '',
            stepHour: 1,
            stepMinute: 1,
            stepSecond: 1
        })
    })
</script>
<!-- date -->
<script type="text/javascript">
	function show(id){
		//var ids = id.toString();
		var arr = new Array(61,62,71,72,81,82,91,92,101,102,
		   		111,112,121,122,131,132,141,142,151,152,
				161,162,171,172,181,182,191,192,201,202,211,212);
		var last = arr.indexOf(id);
		var lasttime = arr[last-1];
		
		var abbr = ''; 
		var point = ''; 
		$('.bigbox >div').each(function(){            
			 abbr = $(this).attr('class'); 
			 if(abbr == 'time_block sele_time'){
				  point += $(this).attr('title') + ',';
				  
			 } 
		})
		if(point!=''){
			var len = point.length;
			var points = point.substring(0,len-1);
			points = points.split(",");
			var maxpoint = Math.max.apply(this,points);
			var minpoint = Math.min.apply(this,points);
			if(id<maxpoint){
				if(id<=minpoint){
					var period = gettime(id);
					for(var i = minpoint;i<=maxpoint;i++){
						$('#timebox_'+i).attr("class","time_block");
					}
					$('#timebox_'+id).attr("class","time_block sele_time");
				}else{
					var period1 = getstarttime(minpoint);
					var period2 = getendtime(id);
					var period = period1+'-'+period2;
					for(var i = id;i<=maxpoint;i++){
						$('#timebox_'+i).attr("class","time_block");
					}
					$('#timebox_'+id).attr("class","time_block sele_time");
				}
			}else if(id==maxpoint){
				var last = arr.indexOf(id);
				var lasttime = arr[last-1];
				var period1 = getstarttime(minpoint);
				var period2 = getendtime(lasttime);
				var period = period1+'-'+period2;
				var name = document.getElementById("timebox_"+id).className;
				if(name == 'time_block'){
					$('#timebox_'+id).attr("class","time_block sele_time");
				}else if(name == 'time_block sele_time'){
					$('#timebox_'+id).attr("class","time_block");
				}
			}else{
				var period1 = getstarttime(minpoint);
				var period2 = getendtime(id);
				var period = period1+'-'+period2;
				for(var i = maxpoint;i<=id;i++){
					if($("#container_"+i).children().hasClass("time_btn time_btn_no")){
						return;
					}else{
						$('#timebox_'+i).attr("class","time_block sele_time");
					}
				}
			}
		}else{
			var period = gettime(id);
			var name = document.getElementById("timebox_"+id).className;
			if(name == 'time_block'){
				$('#timebox_'+id).attr("class","time_block sele_time");
			}else if(name == 'time_block sele_time'){
				$('#timebox_'+id).attr("class","time_block");
			}
		}

		//时间弹框s 
		if($('#time_prompt_'+id).css("display") == "none"){
			$('.time_prompt').hide();
			$('#time_prompt_'+id).html('已选中:'+period);
			$('#time_prompt_'+id).show();			
			
		} else {
			$('#time_prompt_'+id).hide();
			if($('.time_line').find('.sele_time').length == 0) {
				return;
			} else {
				var num = lasttime;
				$('#time_prompt_'+num).html('已选中:'+period);
				$('#time_prompt_'+num).show();
			};	
		}
		//时间弹框e 
	}
</script>
<script type="text/javascript">
	function gettime(ids){
		var s = ids.toString();
		var ss = s.substr(s.length-1,1);
		var newstr = s.substring(0,s.length-1);
		if(ss == 1){
			var period = newstr+':00-'+newstr+':30';
		}else{
			newstr2 = parseInt(newstr)+1;
			var period = newstr+':30-'+newstr2+':00';
		}
		return period;
	}
	function getstarttime(time1){
		var s = time1.toString();
		var ss = s.substr(s.length-1,1);
		var newstr = s.substring(0,s.length-1);
		if(ss == 1){
			var start = newstr+':00';
		}else{
			var start = newstr+':30';
		}
		return start;
	}
	function getendtime(time2){
		var s = time2.toString();
		var ss = s.substr(s.length-1,1);
		var newstr = s.substring(0,s.length-1);
		if(ss == 1){
			var end = newstr+':30';
		}else{
			newstr2 = parseInt(newstr)+1;
			var end = newstr2+':00';
		}
		return end;
	}
</script>
<script>
	//$("body").click(function(e){
		
		//e.preventDefault();	
		//if(e.srcElement.className=="time_btn"){$(this).children().show();}
		//else{$('.time_prompt').hide();}
	//});
</script>
<div class="p_sml_menu">
    	您当前位置：
    	<a href="/meeting/appoint-list">自助会议平台 </a>
        >
        发布专家
</div>
<div class="p_center_box">
	<div class="left">
		@if(Auth::User()->role_id==2)
            <ul class="wid156">
                <li><a href="/meeting/appoint-list">预约会议</a></li>
                <li><a class="sele" href="/meeting/create">发布专家</a></li>
                <li><a href="/mymeeting">我的会议</a></li>
                <li><a href="/joinmeeting">观看会议</a></li>
            </ul>
        @endif
    </div>
    <div class="right wid767">
    	<div class="line_block">
            <div class="pbtm35">
            	<div class="line_label"> 预约会议时间：</div>
                <div class="line_txt">
                	<input type="text" name="meeting_time" class="ui_timepicker" value="" id="meeting_time">
                </div>
                <div class="triangle"></div>
                <label style="color:red;">*创建会议必须提前10天以上*</label>
                 <div class="clear"></div>
            </div>
        </div>
        <div class="pbtm35">
        	<div class="time_box">
            	<div class="time_line">
            		@foreach($aTime as $k=>$v)
            		<div class="bigbox">
                        <div class="time_block"  id="timebox_{{$k}}" title="{{$k}}">
                        	<div class="time_num">
                            	{{$v}}
                            </div>
                            <div class="time_btn_block relative" id="container_{{$k}}">
                            	<input class="time_btn" type="button" onclick="show({{$k}})" />
                                <div class="time_prompt" id="time_prompt_{{$k}}" >
                        			已选中：{{$v}}--{{$v}}
                        		</div>
                            </div>
                        </div>
                    </div>
                    @if($k==132)
                        <div class="time_block" >
                        	<div class="time_num">
                            	14:00
                            </div>
                        </div>
                        <div class="clear"></div>
                    @endif
                    @endforeach
                    <div class="time_block" >
                        <div class="time_num">
                            22:00
                        </div>
                    </div>
                    <div class="clear"></div>
            	</div>
        	</div>
        </div>
           <div class="line_block">
            	<div class="pbtm20">
                	<div class="line_label">专家单位：</div>
                    <div class="line_txt">
                    	<input type="text" name="expert_company" id="expert_company"/>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="line_block">
            	<div class="pbtm20">
                	<div class="line_label">专家科室：</div>
                    <div class="line_txt">
                    	<input type="text" name="expert_subject" id="expert_subject"/>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="line_block">
            	<div class="pbtm20">
                	<div class="line_label">专家姓名：</div>
                    <div class="line_txt">
                    	<input type="text" name="expert_name" id="expert_name"/>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="line_block">
            	<div class="pbtm20">
                	<div class="line_label">会议主题：</div>
                    <div class="line_txt">
                    	<input type="text" name="meeting_title" id="meeting_title"/>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="line_block">
            	<div class="pbtm20">
                	<div class="line_label">创 建 人：</div>
                    <div class="line_txt">
                    	<input type="text" name="creator_name" id="creator_name"/>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="line_block">
            	<div class="pbtm20">
                	<div class="line_label">创建人电话：</div>
                    <div class="line_txt">
                    	<input type="text" name="creator_tel" id="creator_tel"/>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="line_block">
            	<div class="pbtm20">
                	<div class="line_label">创建人邮箱：</div>
                    <div class="line_txt">
                    	<input type="text" name="creator_email" id="creator_email"/>
                    </div>
                    <div class="submit_btn_block">
                    	<input class="submit_btn" type="button" value="提交" onclick="savemeeting()"/>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            
    </div> 
    <div class="clear"></div>  
</div>
<div id="create_box1" style="display:none;">
	<div class="cover"></div>
	<div class="pop_box">
		<div class=" pop_box_txt center">
	    	确定创建会议吗?
	    </div>
	    <div class="center">
	    	<input type="button" class="question_btn" value="取消" onclick="cancelbox()"/>
	        <input type="button" class="question_btn" value="确定" onclick="submitbox()"/>
	    </div>
	</div>
</div>
<div id="create_box2" style="display:none;">
	<div class="cover"></div>
	<div class="pop_box" style="top:50%;">
		<div class=" pop_box_txt center">
	    	<p>您的会议已创建成功，</p>
			<p>请到我的会议查看。</p>
	    </div>
	    <div class="center">
	        <input type="button" class="question_btn" value="确定" onclick="window.location.href='/mymeeting'"/>
	    </div>
	</div>
</div>
<script type="text/javascript">
_isRun = false;
window.time = '';
$(function(){
$('#meeting_time').change(function(){
		var text = document.getElementById("meeting_time");
		if(window.time != $.trim(text.value)){
			window.time = $.trim(text.value);
			if(text.value.length == 11 ||text.value.length == 10){
				text.value.length = 10;
				var date = this.value;
				$.ajax({
					url:'/meeting/ajax',
					timeout:10000,
					type:'post',
					data:{type:'ajax','date':date},
					dataType:'json',
					success:function(data){
		//				var json = eval('('+data+')');
						if(data.aTimeCode == '000'){
							alert('创建会议必须提前10天，谢谢！');
							return false;
						}else if(data.aTimeCode != '' && data.aTimeCode != '000'){
							$(data.aTimeCode).each(function(){
								$("#container_"+this).children().remove();
								var str = 
									'<input class="time_btn time_btn_no" type="button" disabled="disabled" />'
									;
								$("#container_"+this).append(str);
							});
						}else{
							for(var i in data.aTime){
								
								$("#container_"+data.aTime[i]).children().remove();
								var str = 
									'<input class="time_btn" type="button" onclick="show('+data.aTime[i]+')"/>'
									;
								$("#container_"+data.aTime[i]).append(str);
								
							}
						}
					},
					error:function(XMLHttpRequest, textStatus, errorThrown){
						if(textStatus=='timeout'){
							alert("请求超时");
							window.location.reload(true);
						}
					}
				});
			}
	}
});
})
</script>
@stop
