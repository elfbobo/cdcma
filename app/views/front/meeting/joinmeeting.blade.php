@extends('front.common.layout')
@section('title')自助会议平台-国卫健康云 @stop
@section('description')自助会议平台-国卫健康云@stop
@section('keywords')自助会议平台-国卫健康云 @stop
@section('content2')
<script src="/assets/js/ajaxfileupload.js"></script>
<script>
function joinmeeting()
{
	var meetingid = $("#meeting_id").val();
	var data = {
			'meetingid':meetingid
	};
	var url = '/mymeeting/join-do'
	$.post(url,data,function(msg){
		if(msg.success == 'success'){
			if(msg.usertype == 2){
				window.location.href = msg.url2;
			}else{
				window.location.href = msg.url1;
			}
		}else if(msg.success == 'no code'){
			alert('该会议编码不存在');
		}else if(msg.success == 'no confirm'){
			alert('该会议还未确认');
		}else if(msg.success == 'time error'){
			alert('该会议还未开始');
		}else if(msg.success == 'time end'){
			alert('该会议已经结束');
		}else{
			alert('操作失败');
		}
	},"json")	
 
}
</script>
<div class="p_sml_menu">
    	您当前位置：
    	<a href="/meeting">自助会议平台 </a>
        >
        观看会议
</div>
<div class="p_center_box">
	<div class="left">
		@if(Auth::User()->role_id==2)
            <ul class="wid156">
                <li><a href="/meeting/appoint-list">预约会议</a></li>
                <li><a href="/meeting/create">发布专家</a></li>
                <li><a href="/mymeeting">我的会议</a></li>
                <li><a class="sele" href="/joinmeeting">观看会议</a></li>
            </ul>
        @endif
    </div>
    <div class="right wid767">        	
            <div class="line_block">
            	<div class="pleft100">
                	<div class="line_label">会议编码：</div>
                    <div class="line_txt">
                    	<input type="text" placeholder="请输入会议ID加入会议" name="meeting_id" id="meeting_id"/>
                    </div>
					<input type="button" class="arrows" onclick="joinmeeting()"/>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="center">
               	<input class="submit_btn" type="button" value="提交" onclick="joinmeeting()"/>
            </div>
    </div>
    <div class="clear"></div>	
</div>
@stop
