@extends('front.common.layout')
@section('title')自助会议平台-国卫健康云 @stop
@section('description')自助会议平台-国卫健康云@stop
@section('keywords')自助会议平台-国卫健康云 @stop
@section('content2')
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="{{ asset('assets/js/DatePicker/WdatePicker.js')}}" type="text/javascript"></script>
<script>
function deletebox()
{
	document.getElementById("deletebox").style.display ="block";
	document.getElementById("cover").style.display ="block";
}
function closeshow()
{
	document.getElementById("deletebox").style.display ="none";
	document.getElementById("cover").style.display ="none";
}
function popbox(meeting_id,user_id){
	document.getElementById("meeting_id").value =meeting_id;
	document.getElementById("user_id").value =user_id;
	document.getElementById("pop_box").style.display ="block";
	document.getElementById("cover").style.display ="block";
}
function deleteappoint(){
	var meeting_id = document.getElementById("meeting_id").value;
	var user_id = document.getElementById("user_id").value;
	var data = {
			'meeting_id':meeting_id,
			'user_id':user_id,
	};
		
	var url = '/meeting/delete-appointment';
	$.post(url,data,function(msg){
		if(msg == 'success'){
			document.getElementById("promptdel").style.display ="block";
			document.getElementById("pop_box").style.display ="none";
			//window.location.reload();pop_box
		}else{
			document.getElementById("pop_box").style.display ="none";
			document.getElementById("cover").style.display ="none";
			//window.location.reload();
		}
	})	
}
function closebox()
{
	document.getElementById("promptdel").style.display ="none";
	window.location.reload();
}
</script>
  <div class="p_sml_menu">
    	您当前位置：
    	<a href="/meeting/appoint-list">自助会议平台 </a>
        >
        我预约的会议
    </div>
    
    
    <div class="p_center_box">
        <div class="left">
            <ul class="wid156">
                <li><a href="/meeting/appoint-list">预约会议</a></li>
                <li><a href="/meeting/create">发布专家</a></li>
                <li><a class="sele" href="/mymeeting">我的会议</a></li>
                <li><a href="/joinmeeting">观看会议</a></li>
            </ul>
        </div>
        <input type="hidden" id="meeting_id">
        <input type="hidden" id="user_id">
        <div class="right wid767_2">
        	<div class="menu_block">
            	<a href="/mymeeting">我创建的会议</a>
                <a class="sele" href="/meeting/has-appointed-meeting">我预约的会议</a>
                <div class="clear"></div>
            </div>
            <div class="my_meeting_box">
            @foreach($oMeeting as  $key=>$value)
            	<div class="my_meeting_block">
                    <div class="my_meeting_infor">
                        <div>主题：{{$value->meeting_title}} </div>
                        <div>专家:{{$value->expert_name}}</div>
                        @if(in_array($value->id,$aStartingMeeting) && $value->meeting_end_time > date('Y-m-d H:i:s',time()))
                       		 <div>技术支持电话 ：400-062-1818</div>
                        @endif
                        <div class="my_meeting_txt">{{$value->meeting_status}}</div>
                    </div>
                    <div class="my_meeting_time">
                       {{$value->meeting_start_time}}
                    </div>
                    <div class="my_meeting_btn">
                        <div class="pbtm20">
	                        @if($aMeetingDelete[$value->id]==1)
	                           @if(in_array($value->id,$aStartingMeeting))
	                               @if($value->meeting_end_time > date('Y-m-d H:i:s',time()))
	                                <a href="/meeting/watch-meeting/{{$value->id}}"> <input class="submit_btn" type="button" value="加入会议"/></a>
	                               @else
	                                 <input  class="submit_btn" style="background-color:#d3d3d3; color:#5f5f5f;" type="button" value="会议结束"/>
	                     		   @endif
	                           @else
	                             <input class="submit_btn" type="button" style="background-color:#d3d3d3; color:#5f5f5f;" value="取 消" onclick="deletebox()"/>
	                           @endif
	                        @else
	                            @if(in_array($value->id,$aStartingMeeting))
	                              @if($value->meeting_end_time > date('Y-m-d H:i:s',time()))
	                                <a href="/meeting/watch-meeting/{{$value->id}}"> <input class="submit_btn" type="button" value="加入会议"/></a>
	                               @else
	                                 <input  class="submit_btn" style="background-color:#d3d3d3; color:#5f5f5f;" type="button" value="会议结束"/>
	                     		   @endif
	                            @else
	                            	@if($value->order_type_id != 1)
	                            	<input class="submit_btn"  type="button" value="取 消" onclick="popbox({{$value->id}},{{$iUid}})"/>
	                            	@else
	                            	<input class="submit_btn"  type="button" value="取 消" style="background-color:#d3d3d3; color:#5f5f5f;"/>
	                            	@endif
	                            @endif
	                        @endif
                        </div>
                        <div>
                        @if($value->order_type_id == 2 && $aMeetingDetail[$value->id]==1)
                             <a href="/meeting/meeting-detail/{{$value->id}}"><input class="submit_btn" type="button" value="会议详情" /></a>
                        @else
                            <input class="submit_btn btn01" style="background-color:#d3d3d3; color:#5f5f5f;" type="button" value="会议详情" />
                       @endif    
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            	<div class="wire"></div>
         @endforeach 
          </div>
                <div class=" my_meeting_txt ptop30">
                	*会议开始7天前双方代表可取消会议预约；会议开始7天内不可取消会议，请双方代表线下沟通商议。
                </div>
                
                
            </div>
    	</div>
        <div class="clear"></div>
    </div>
</div>


<div class="cover" id="cover" style="display:none;"></div>
<div class="pop_box" id="pop_box" style="display:none;">
	<div class=" pop_box_txt center">
    	确定取消会议吗
    </div>
    <div class=" center">
        <input type="button" class="question_btn" value="确定"  onclick="deleteappoint()" />
    </div>
</div>
<div class="pop_box" id = "promptdel" style="top:50%;display:none;">
	<div class=" pop_box_txt center" onclick="closebox()">
    	会议已取消。
    </div>
    <div class=" center">
        <input type="button" class="question_btn" value="确定" onclick="closebox()"/>
    </div>
</div>
<div class="pop_box" id = "deletebox" style="top:50%;display:none;">
	<div class=" pop_box_txt center" onclick="closeshow()">
    	会议开始前一周不能取消预约。
    </div>
    <div class=" center">
        <input type="button" class="question_btn" value="确定" onclick="closeshow()"/>
    </div>
</div>

@stop
