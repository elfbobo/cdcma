@extends('front.common.layout')
@section('title')自助会议平台-国卫健康云 @stop
@section('description')自助会议平台-国卫健康云@stop
@section('keywords')自助会议平台-国卫健康云 @stop
@section('content2')
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="{{ asset('assets/js/DatePicker/WdatePicker.js')}}" type="text/javascript"></script>
<script>
function saveappointment(){
	var order_hospital = $.trim($('#order_hospital').val());
	var order_department = $.trim($('#order_department').val());
	var order_name = $.trim($('#order_name').val());
	var order_tel = $.trim($('#order_tel').val());
	var order_email = $("#order_email").val();
	var meeting_id = $("#meeting_id").val();
	if(!order_hospital){
		alert('请输入您所在的医院！');
		return false;
	}
	if(!order_department){
		alert('请输入您所在的科室！');
		return false;
	}
	if(!order_name){
		alert('请输入您的姓名！');
		return false;
	}
	if(!order_tel){
		alert('请输入您的手机号！');
		return false;
	}
	var reg_mobile = /^[0-9]{11}$/;
	istel= reg_mobile.test(order_tel);
	if (!istel ) {
	    alert("手机号码格式不正确！");
	    return false;
	}
	if(!order_email){
		alert('请输入您的邮箱！');
		return false;
	}
	var reg = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
	ismail= reg.test(order_email);
	if (!ismail ) {
	    alert("邮箱格式不正确！");
	    return false;
	}
	var data = {
			'meeting_id':meeting_id,
			'order_hospital':order_hospital,
			'order_name':order_name,
			'order_department':order_department,
			'order_tel':order_tel,
			'order_email':order_email
	};
	var url = '/meeting/appoint';
	$.post(url,data,function(msg){
		if(msg=='success'){
			alert('提交成功');
//			window.location.reload();
		}else{
			alert('提交失败');
			window.location.reload();
		}
	})	
}
</script>
<div class="p_bar_bg  bg_color2">
	<div class="wid1025">
    	<div class="p_tit pleft230">
        	<a class="white" href="/meeting/appoint-list">自助会议平台</a>
            >
            预约会议
        </div>
    </div>
</div>
<div class="p_center_box">
	<div class="left">
    	<ul>
    		@if(Auth::User()->role_id==2)
            <li><a  href="/meeting"><span class="icon02">发布专家</span></a></li>
            <li><a href="/mymeeting"><span class="icon03">我的会议</span></a></li>
            <li><a class="sele" href="/meeting/appoint-list"><span class="icon04">预约会议</span></a></li>
            <li><a href="/joinmeeting"><span class="icon05">观看会议</span></a></li>
            @endif
        </ul>
    </div>
    <div class="right wid698">
    	<div class="">
    		<div class="center_block">
                <div class="center_infor3">
                    <p><label>预约人医院：</label>
                    	<input class="infor_txt" type="text" name="expert_company" id="order_hospital"  />
                    </p>
                    <p><label>预约人科室：</label>
                    	<input class="infor_txt" type="text" name="creator_name" id="order_department"  />
                    </p>
                    <p><label>预约人姓名：</label>
                    	<input class="infor_txt" type="text" name="expert_name" id="order_name" />
                    </p>
                    <p><label>预约人手机号：</label>
                    	<input class="infor_txt" type="text" name="creator_tel" id="order_tel"  />
                    </p>
                    <p><label>预约人邮箱：</label>
                    	<input class="infor_txt" type="text" name="creator_email" id="order_email"  />
                    	<input  type="hidden" value="{{$id}}"  id="meeting_id"  />
                    </p>
                </div>
    		</div>
        </div>
        <div class="center">
        	<input class="save" type="button" value="保　存" onclick="saveappointment()"/>
        </div>
    </div>
	<div class="clear"></div>
</div>
@stop
