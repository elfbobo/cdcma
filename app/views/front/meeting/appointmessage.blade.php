@extends('front.common.layout')
@section('title')自助会议平台-国卫健康云 @stop
@section('description')自助会议平台-国卫健康云@stop
@section('keywords')自助会议平台-国卫健康云 @stop
@section('content2')
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="{{ asset('assets/js/DatePicker/WdatePicker.js')}}" type="text/javascript"></script>
<script>
function appoint(id){
	 document.getElementById("cover").style.display ="block";
	 document.getElementById("pop_box2").style.display ="block";
	 document.getElementById('ids').value=id;
}
function saveappointment(){
	var order_hospital = $.trim($('#order_hospital').val());
	var order_num = $.trim($('#order_num').val());
	var order_department = $.trim($('#order_department').val());
	var order_name = $.trim($('#order_name').val());
	var order_tel = $.trim($('#order_tel').val());
	var order_email = $("#order_email").val();
	var meeting_id = $("#ids").val();
	if(!order_name){
		alert('请输入您的姓名！');
		return false;
	}
	if(!order_tel){
		alert('请输入您的手机号！');
		return false;
	}
	var reg_mobile = /^[0-9]{11}$/;
	istel= reg_mobile.test(order_tel);
	if (!istel ) {
	    alert("手机号码格式不正确！");
	    return false;
	}
	if(!order_email){
		alert('请输入您的邮箱！');
		return false;
	}
	var reg = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
	ismail= reg.test(order_email);
	if (!ismail ) {
	    alert("邮箱格式不正确！");
	    return false;
	}
	if(!order_hospital){
		alert('请输入您所在的医院！');
		return false;
	}
	if(!order_department){
		alert('请输入您所在的科室！');
		return false;
	}
	if(!order_email){
		alert('请输入您的邮箱！');
		return false;
	}
	if(!order_num){
		alert('请输入参会人数！');
		return false;
	}
	var num =/\d+/;
	isnum = num.test(order_num);
	if(!isnum){
		alert('参会人数请输入数字！');
		return false;
	}
	var data = {
			'meeting_id':meeting_id,
			'order_hospital':order_hospital,
			'order_name':order_name,
			'order_department':order_department,
			'order_tel':order_tel,
			'order_email':order_email,
			'order_num':order_num
	};
	var url = '/meeting/appoint';
	$.post(url,data,function(msg){
		if(msg=='success'){
			 document.getElementById("makesure").style.display ="block";
			 document.getElementById("pop_box2").style.display ="none";
//			window.location.reload();
		}else{
			//alert('提交失败');
			document.getElementById("cover").style.display ="block";
			window.location.reload();
		}
	})	
}
 function hiddenreport(){
	document.getElementById("makesure").style.display ="none";
	document.getElementById("cover").style.display ="none";
	window.location.reload();
}
function selfappoint()
{
	document.getElementById("selfappoint").style.display ="block";
	document.getElementById("cover").style.display ="block";
   
}
function closeselfshow()
{
   document.getElementById("selfappoint").style.display ="none";
   document.getElementById("cover").style.display ="none";
}
function closeappointdiv()
{
	document.getElementById("cover").style.display ="none";
	document.getElementById("pop_box2").style.display ="none";
}
</script>
 <div class="p_sml_menu">
    	您当前位置：
    	<a href="/meeting/appoint-list">自助会议平台 </a>
        >
        预约会议
    </div>    
    <div class="p_center_box">
        <div class="left">
            <ul class="wid156">
                <li><a class="sele" href="/meeting/appoint-list">预约会议</a></li>
                <li><a href="/meeting/create">发布专家</a></li>
                <li><a href="/mymeeting">我的会议</a></li>
                <li><a href="/joinmeeting">观看会议</a></li>
            </ul>
        </div>
        <div class="right wid767">
    		<div class="p20">
    		 @foreach($oMeeting as  $key=>$value)
    		     @if($key % 2 == 0)
		            	<div class="meeting_box2 left">
		                	<div class="m_left">
		                        <div class="">
		                            主题：{{$value->meeting_title}}
		                        </div>
		                        <div class="">
		                            专家：{{$value->expert_name}}
		                        </div>
		                        <div class="m_time">
		                            {{$value->meeting_start_time}}
		                        </div>
		                	</div>
		                    <div class="m_right">
		                     @if(!in_array($value->id,$aSelfMeetingId))
				                    @if(in_array($value->id,$aHasAppointmeeting))
					                       <input type="button" value="已预约" class="submit_btn btn01" style="background-color:#d3d3d3; color:#5f5f5f;"/>
					                 @else
					                       @if($value->order_count < $iMaxNum)
					                    	 @if(!in_array($value->id,$aHasMakeSureMeeting))	
					                    		<input type="button" value="预约（{{$iMaxNum - $value->order_count}}）" class="submit_btn" onclick="appoint({{$value->id}})"/>
					                          @else
					                 			<input type="button" value="预约（0）" class="submit_btn btn01" style="background-color:#d3d3d3; color:#5f5f5f;"/>
					            			 @endif
					                      @else
					                        <input type="button" value="预约（0）" class="submit_btn btn01" style="background-color:#d3d3d3; color:#5f5f5f;"/>
					                      @endif
					                @endif
					          @else
					                     @if($value->order_count < $iMaxNum)
					                    	 @if(!in_array($value->id,$aHasMakeSureMeeting))	
					                    		<input type="button" value="预约（{{$iMaxNum - $value->order_count}}）" class="submit_btn" onclick ="selfappoint()"/>
					                          @else
					                 			<input type="button" value="预约（0）" class="submit_btn btn01" style="background-color:#d3d3d3; color:#5f5f5f;"/>
					            			  @endif
					                      @else
					                        <input type="button" value="预约（0）" class="submit_btn btn01" style="background-color:#d3d3d3; color:#5f5f5f;"/>
					                      @endif
<!--					             <input type="button" value="预约2222（{{$iMaxNum - $value->order_count}}）" class="submit_btn btn01" onclick ="selfappoint()" style="background-color:#d3d3d3; color:#5f5f5f;"/>-->
					          @endif
		                    </div>
		                    <div class="clear"></div>
		                </div>
		            @else
		                <div class="meeting_box2 right">
		                	<div class="m_left">
		                        <div class="">
		                            主题：{{$value->meeting_title}}
		                        </div>
		                        <div class="">
		                            专家：{{$value->expert_name}}
		                        </div>
		                        <div class="m_time">
		                             {{$value->meeting_start_time}}
		                        </div>
		                	</div>
		                    <div class="m_right">
		                      @if(!in_array($value->id,$aSelfMeetingId))
				                     @if(in_array($value->id,$aHasAppointmeeting))
					                        <input type="button" value="已预约" class="submit_btn btn01" style="background-color:#d3d3d3; color:#5f5f5f;"/>
					                 @else
					                        @if($value->order_count < $iMaxNum)
					                           @if(!in_array($value->id,$aHasMakeSureMeeting))
					                    	      <input type="button" value="预约（{{$iMaxNum - $value->order_count}}）" class="submit_btn" onclick="appoint({{$value->id}})"/>
					                           @else
					                              <input type="button" value="预约（0）" class="submit_btn btn01" style="background-color:#d3d3d3; color:#5f5f5f;"/>
					                           @endif 
					                        @else
					                        <input type="button" value="预约（0）" class="submit_btn btn01" style="background-color:#d3d3d3; color:#5f5f5f;"/>
					                        @endif
					                 @endif
					            @else
					                   @if($value->order_count < $iMaxNum)
					                    	 @if(!in_array($value->id,$aHasMakeSureMeeting))	
					                    		<input type="button" value="预约（{{$iMaxNum - $value->order_count}}）" class="submit_btn" onclick ="selfappoint()"/>
					                          @else
					                 			<input type="button" value="预约（0）" class="submit_btn btn01" style="background-color:#d3d3d3; color:#5f5f5f;"/>
					            			  @endif
					                      @else
					                        <input type="button" value="预约（0）" class="submit_btn btn01" style="background-color:#d3d3d3; color:#5f5f5f;"/>
					                      @endif
<!--					                <input type="button" value="预约（{{$iMaxNum - $value->order_count}}）" class="submit_btn btn01" onclick ="selfappoint()" style="background-color:#d3d3d3; color:#5f5f5f;"/>-->
					            @endif
		                    </div>
		                    <div class="clear"></div>
		                </div>
		             @endif
		     @endforeach     
                <div class="clear"></div>
                <div class=" my_meeting_txt ptop30">
                	注：会议开始一周前通知会议是否预约成功，请实时关注“我的会议”栏目中您预约的会议。
                </div>                
            </div>
    	</div>
        <div class="clear"></div>
    </div>
</div>
<div class="cover" id="cover" style="display:none;"></div>
<div class="pop_box2" id ="pop_box2" style="top:10%;display:none">
	<div class="line_block">
        <div class="pbtm20" style="height:20px;">
            <input type="hidden" value="" name="meeting_id" id="ids">
            <div class="line_label">预约人姓名：</div>
            <div class="line_txt"><input type="text" id="order_name"/></div>
            <div class="clear"></div>
        </div>
    </div>
    
    <div class="line_block">
        <div class="pbtm20">
            <div class="line_label">预约人电话：</div>
            <div class="line_txt"><input type="text" id="order_tel"/></div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="line_block">
        <div class="pbtm20">
            <div class="line_label">预约人邮箱：</div>
            <div class="line_txt"><input type="text" id ="order_email"/></div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="line_block">
        <div class="pbtm20">
            <div class="line_label">预约人医院：</div>
            <div class="line_txt"><input type="text" id="order_hospital"/></div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="line_block">
        <div class="pbtm20">
            <div class="line_label">预约人科室：</div>
            <div class="line_txt"><input type="text" id="order_department"/></div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="line_block">
        <div class="pbtm20">
            <div class="line_label">参 会 人 数：</div>
            <div class="line_txt"><input type="text" id="order_num"/></div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="line_block">
        <div class=" my_meeting_txt">
        注：请正确填写以上信息，以便联系
        </div>
    </div>
    <div class=" center ptop45">
    	<input type="button" value="提交申请" class="submit_btn mright130"  onclick="saveappointment()"/>
        <input type="button" value="取消" class="submit_btn" onclick = "closeappointdiv()"/>
    </div>
</div>
<div class="pop_box border2" id ="makesure" style="top:20%; display:none">
	<div class=" pop_box_txt center">
    	<p>您的会议预约已提交，请等待确认。</p>
		<p>注：请时时关注短信提示</p>
    </div>
    <div class=" center">
        <input type="button" class="question_btn" value="确定" onclick="hiddenreport()"/>
    </div>
</div>
<div class="pop_box border2" id ="selfappoint" style="top:20%; display:none">
	<div class=" pop_box_txt center">
    	<p>会议创建人不可预约。</p>
		<p></p>
    </div>
    <div class=" center">
        <input type="button" class="question_btn" value="确定" onclick="closeselfshow()"/>
    </div>
</div>
@stop
