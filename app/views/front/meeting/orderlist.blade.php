@extends('front.common.layout')
@section('title')自助会议平台-国卫健康云 @stop
@section('description')自助会议平台-国卫健康云@stop
@section('keywords')自助会议平台-国卫健康云 @stop
@section('content2')
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="{{ asset('assets/js/blockui/jquery.blockUI.js') }}"></script>
<script type="text/javascript">
function order_submit(meetingid,orderid){
	var data = {
			'meetingid':meetingid,
			'orderid':orderid
	};

	var url = '/mymeeting/order-do';
	$.post(url,data,function(msg){
		if(msg.success == 'success'){
			if(msg.code != ''&& msg.code != 0){
				$('#box_content').html(msg.code);
				$('#order_box').show();
				$("#box_submit").click(function(){
			    	$('#order_box').hide();
			    	window.location.reload();
	    		});
			}
		}else{
			alert('操作失败');
//			window.location.reload();
		}
	},"json")	
}
</script>
<div class="p_sml_menu">
    	您当前位置：
    	<a href="/meeting/appoint-list">自助会议平台 </a>
        >
       我的会议
</div>
<div class="p_center_box">
	<div class="left">
		@if(Auth::User()->role_id==2)
            <ul class="wid156">
                <li><a href="/meeting/appoint-list">预约会议</a></li>
                <li><a href="/meeting/create">发布专家</a></li>
                <li><a class="sele" href="/mymeeting">我的会议</a></li>
                <li><a href="/joinmeeting">观看会议</a></li>
            </ul>
        @endif
    </div>
    <div class="right wid767_2">
        <div class="menu_block">
            <a class="sele" href="/mymeeting">我创建的会议</a>
            <a href="/meeting/has-appointed-meeting">我预约的会议</a>
            <div class="clear"></div>
        </div>
        @foreach($oOrder as $k=>$v)
        <div class="my_meeting_box">
            	<div class=" meeting_look_box">
                	<div class="m_look_left">
                    	<div class="m_look_line">
                        	<div class="m_look_tit">
                            	联 系 人：
                            </div>
                            <div class="m_look_txt">
                            	{{$v->order_name}}
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="m_look_line">
                        	<div class="m_look_tit">
                            	联系电话：
                            </div>
                            <div class="m_look_txt">
                            	{{$v->order_tel}}
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="m_look_line">
                        	<div class="m_look_tit">
                            	邮    箱：
                            </div>
                            <div class="m_look_txt">
                            	{{$v->order_email}}
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="m_look_line">
                        	<div class="m_look_tit">
                            	预约医院：
                            </div>
                            <div class="m_look_txt">
                            	{{$v->order_hospital}}
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    @if($v->order_type_id == 0)
                    <div class="m_look_right">
                    	<input class="submit_btn" type="button" value="接受预约" onclick="order_submit({{$v->meeting_id}},{{$v->id}})" style="cursor:pointer;"/>
                    </div>
                    @elseif($v->order_type_id == 1)
                    <div class="m_look_right">
                    	<input class="submit_btn btn01" type="button" value="接受预约" disabled="disabled" style="background-color:#d3d3d3; color:#5f5f5f;"/>
                    </div>
                    @else
                    <div class="m_look_right">
                    	已确认
                    </div>
                    @endif
                    <div class="clear"></div>
                </div>
                <div class="wire"></div>	      
        </div>
        @endforeach    
    </div>
	<div class="clear"></div>
</div>

<div id="order_box" style="display:none;">
	<div class="cover"></div>
	<div class="pop_box" id="box">
		<div class=" pop_box_txt center">
	    	<p>会议确认成功，此次会议ID：</p>
			<p id="box_content">8888</p>
	    </div>
	    <div class="center">
	        <input type="button" class="question_btn" value="确定" id="box_submit" />
	    </div>
	</div>
</div>
@stop
