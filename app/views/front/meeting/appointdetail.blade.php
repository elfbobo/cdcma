@extends('front.common.layout')
@section('title')自助会议平台-国卫健康云 @stop
@section('description')自助会议平台-国卫健康云@stop
@section('keywords')自助会议平台-国卫健康云 @stop
@section('content2')
<div class="p_sml_menu">
    	您当前位置：
    	<a href="/meeting/appoint-list">自助会议平台 </a>
        >
        会议详情
    </div>
    
    
    <div class="p_center_box">
        <div class="left">
            <ul class="wid156">
                <li><a href="/meeting/appoint-list">预约会议</a></li>
                <li><a href="/meeting/create">发布专家</a></li>
                <li><a class="sele" href="/mymeeting">我的会议</a></li>
                <li><a href="/joinmeeting">观看会议</a></li>
            </ul>
        </div>
        <div class="right wid767_2">
        	<div class="meeting_cont">
            	<div class="pbtm20">
                    <div class="m_label">
                        会议时间：
                    </div>
                    <div class="m_txt">
                        {{$oMeetingDetail ->meeting_start_time}}
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="pbtm20">
                    <div class="m_label">
                        专家单位：
                    </div>
                    <div class="m_txt">
                        {{$oMeetingDetail ->expert_company}}
                    </div>
                    <div class="clear"></div>
                </div>
                  <div class="pbtm20">
                    <div class="m_label">
                        专家科室：
                    </div>
                    <div class="m_txt">
                        {{$oMeetingDetail ->expert_subject}}
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="pbtm20">
                    <div class="m_label">
                        专家姓名：
                    </div>
                    <div class="m_txt">
                       {{$oMeetingDetail ->expert_name}}
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="pbtm20">
                    <div class="m_label">
                        会议主题：
                    </div>
                    <div class="m_txt">
                       {{$oMeetingDetail ->meeting_title}}
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="pbtm20">
                    <div class="m_label">
                        创 建 人：
                    </div>
                    <div class="m_txt">
                       {{$oMeetingDetail ->creator_name}}
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="pbtm20">
                    <div class="m_label">
                        电    话：
                    </div>
                    <div class="m_txt">
                        {{$oMeetingDetail ->creator_tel}}
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="pbtm20">
                    <div class="m_label">
                        邮    箱：
                    </div>
                    <div class="m_txt">
                       {{$oMeetingDetail ->creator_email}}
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="pbtm20">
                    <div class="m_label">
                        会 议 ID：
                    </div>
                    <div class="m_txt">
                        {{$oMeetingDetail -> id}}
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="pbtm20">
<!--                	请使用此会议ID及密码加入会议-->
                	请使用此会议ID加入会议
                </div>                
            </div>            
    	</div>
        <div class="clear"></div>
    </div>
</div>

@stop
