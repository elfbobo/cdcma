@extends('front.common.layout')
@section('title')自助会议平台-国卫健康云 @stop
@section('description')自助会议平台-国卫健康云@stop
@section('keywords')自助会议平台-国卫健康云 @stop
@section('content2')
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="{{ asset('assets/js/blockui/jquery.blockUI.js') }}"></script>
<script type="text/javascript">
function showPopWindow(id,url){
	$('#'+id).show();
    $("#box_submit").click(function(){
        var data = {};
    	$.post(url,data,function(msg){
    		if(msg=='success'){
    		    $('#'+id).children('#box').remove();
    		    var str = '<div class="pop_box">'
    		        	+'<div class=" pop_box_txt center">'
    		        	+'会议已取消。</div>'
    		        	+'<div class="center">'
    		        	+'<input type="button" class="question_btn" value="确定" id="box_submit1"/></div></div>';
    		    $('#'+id).append(str);
    		    $("#box_submit1").click(function(){
    		    	$('#'+id).hide();
    		    	window.location.reload();
        		});
    		}else{
    			alert('取消失败');
//    			window.location.reload();
    		}
    	})	
    })
}
</script>
<script type="text/javascript">
function showUrl(meetingid){
	var data = {
		'meetingid':meetingid
	};
	var url = '/mymeeting/enter-do'
	$.post(url,data,function(msg){
		if(msg.success == 'success'){
			if(msg.url != ''){
				window.location.href = msg.url;
			}
		}else{
			alert('操作失败');
		}
	},"json")	
}
</script>
<div class="p_sml_menu">
    	您当前位置：
    	<a href="/meeting/appoint-list">自助会议平台 </a>
        >
       我的会议
</div>

<div class="p_center_box">
	<div class="left">
		@if(Auth::User()->role_id==2)
            <ul class="wid156">
                <li><a href="/meeting/appoint-list">预约会议</a></li>
                <li><a href="/meeting/create">发布专家</a></li>
                <li><a class="sele" href="/mymeeting">我的会议</a></li>
                <li><a href="/joinmeeting">观看会议</a></li>
            </ul>
        @endif
    </div>
    <div class="right wid767_2">
        <div class="menu_block">
            <a class="sele" href="/mymeeting">我创建的会议</a>
            <a href="/meeting/has-appointed-meeting">我预约的会议</a>
            <div class="clear"></div>
        </div>
        
    	<div class="my_meeting_box">
    		@foreach($oMeeting as $k=>$v)
    		<div>
            	<div class="my_meeting_block2">
                	<div>
                    	<div class="my_meeting_infor">
                        	<div>主题：{{$v->meeting_title}} </div>
                            <div>专家：{{$v->expert_name}}</div>
                        </div>
                        <div class="my_meeting_time">
                        	{{substr($v->meeting_start_time,0,16)}}-{{substr($v->meeting_end_time,11,5)}}
                        </div>
                        <div class="clear"></div>
                    </div>
                    <!--    				已取消的会议-->
                    @if($v->status == 1)
                    <div class="my_meeting_btn">
                        <div>
                        	<input class="submit_btn btn01" type="button" value="编 辑" disabled="disabled" style="background-color:#d3d3d3; color:#5f5f5f;"/>
                            <input class="submit_btn mleft90" type="button" value="取 消" disabled="disabled" style="background-color:#d3d3d3; color:#5f5f5f;"/>                            
                            <input class="submit_btn" style="float:right;background-color:#d3d3d3; color:#5f5f5f;" type="button" value="查看预约" disabled="disabled"/>
                            <div class="clear"></div>
                        </div>
                    </div>
                    
                    @else
                    <!--    				正常进行的会议-->
                    	@if($v->order == 1 || $v->cancel == 0)
	    				<a href="#"><input type="button" value="编辑" class="submit_btn btn01" disabled="disabled" style="background-color:#d3d3d3; color:#5f5f5f;"/>
	    				</a>
	    				@elseif($v->order == 2 || $v->cancel == 0)
	    				<a href="#"><input type="button" value="编辑" class="submit_btn btn01" disabled="disabled" style="background-color:#d3d3d3; color:#5f5f5f;"/>
	    				</a>
	    				@else
	    				<a href="/meeting/edit/{{$v->id}}"><input type="button" value="编辑" class="submit_btn btn01"/>
	    				</a>
	    				@endif
	    				
	    				@if($v->cancel == 1)
	    				<a href="javascript:void(0);" onclick="showPopWindow('del','/meeting/delete/{{$v->id}}')">
	    					<input class="submit_btn mleft90" type="button" value="取消"/>
	    				</a>
	    				@elseif($v->cancel == 2)
	    				<a href="javascript:void(0);" onclick="showUrl({{$v->id}})">
	    					<input class="submit_btn mleft90" type="button" value="加入会议"/>
	    				</a>
	    				@elseif($v->cancel == 3)
	    				<a href="javascript:void(0);">
	    					<input class="submit_btn mleft90" type="button" value="会议结束" disabled="disabled" style="background-color:#d3d3d3; color:#5f5f5f;"/>
	    				</a>
	    				@elseif($v->cancel == 0)
	    				<a href="javascript:void(0);">
	    					<input class="submit_btn mleft90" type="button" value="取消" disabled="disabled" style="background-color:#d3d3d3; color:#5f5f5f;"/>
	    				</a>
	    				@endif
	    				
	    				@if($v->order == 1 || $v->order == 2)
	    				<a href="/mymeeting/order-list/{{$v->id}}"><input type="button" class="submit_btn" value="查看预约" style="float:right;"/>
	    				</a>
	    				@else
	    				<a href="#"><input type="button" class="submit_btn" value="查看预约" disabled="disabled" style="float:right;background-color:#d3d3d3; color:#5f5f5f;"/>
	    				</a>
	    				@endif
                    @endif
            	</div>
            	@if($v->status == 1)
            	<div class="my_meeting_txt txt_right">
                                                         该会议已被取消
                </div>
                @else
	                @if($v->order == 1)
	                <div class="my_meeting_txt txt_right">
                                                                   此会议已被预约，请点击查看预约及时确认会议
                	</div>
                	@elseif($v->order == 2)
                		@if($v->cancel == 2)
                		<div class="my_meeting_txt txt_right">
	                                                                   技术支持电话：400-062-1818                                          
	                	</div>
	                	@else
	                	<div class="my_meeting_txt txt_right">
	                                                                   此会议已被确认，请点击查看
	                	</div>
	                	@endif
                	@else
	                @endif
                @endif
            	<div class="wire"></div>
            </div>
        	@endforeach
        	
        </div>
        <div class="p_number_box">
         {{$oMeeting->links()}}
        </div> 
    </div>
	<div class="clear"></div>
</div>

<div id="del" style="display:none;">
	<div class="cover"></div>
	<div class="pop_box" id="box">
		<div class=" pop_box_txt center" id="box_content">
	    	确定取消会议吗
	    </div>
	    <div class="center">
	        <input type="button" class="question_btn" value="确定" id="box_submit"/>
	    </div>
	</div>
</div>
<!--<div id="del" style="display:none;">-->
<!--	<div class="cover"></div>-->
<!--	<div class="pop_box" style="top:50%;">-->
<!--		<div class=" pop_box_txt center">-->
<!--	    	会议已取消。-->
<!--	    </div>-->
<!--	    <div class="center">-->
<!--	        <input type="button" class="question_btn" value="确定"/>-->
<!--	    </div>-->
<!--	</div>-->
<!--</div>-->
@stop
