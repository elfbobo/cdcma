@extends('front.common.layout')
@section('title')病例征集-国卫健康云 @stop
@section('description')病例征集-国卫健康云@stop
@section('keywords')病例征集-国卫健康云 @stop
@section('content1')
<div class="p_bar_bg bg_color2">
	<div class="wid1025">
    	<div class="p_tit pleft230">
        	<a class="white" href="/">首页</a>
            >
            <a class="white" href="/case/rule">征集规则</a>
        </div>
    </div>
</div>
<div class="p_center_box">
	<div class="left">
    	<ul>
            <li><a href="/case/rule" class="sele"><span class="icon06">征集规则</span></a></li>
            <li><a href="/case/upload"><span class="icon08">病例上传</span></a></li>
            <li><a href="/case/e-list"><span class="icon04">病例点评会</span></a></li>
            <li><a href="/case/list/0"><span class="icon03">经典病例</span></a></li>
            <li><a href="/case/experts"><span class="icon02">点评专家团</span></a></li>
        </ul>
    </div>
    <div class="right wid698">
    	<div class="center mar_b20">
        	<img src="/assets/images/front/web/logo.jpg" />
        </div>
        <div class="center_block">
            <div class="center_tit">征集规则</div>
			<div class="p_part">
            	<div class="intro">
                    <p>随着城市化进程加速、生活方式的改变，高血压患病率增长迅猛，目前我国成人高血压发病率高达29.6%，长期高血压可引起严重的心、脑、肾并发症，并最终导致靶器官的功能衰竭，严重地危害人们的健康和生命。我国高血压的防控形势面临极大的挑战，医生对高血压的管理工作任重而道远。</p>
                    <p>为了促进医生对高血压管理的学习并且分享其高血压治疗体会，展现医生治疗高血压水平，由中华医学会、慢性疾病与转化医学（英文）发起，基于国卫健康云平台的高血压案例征集活动，对筛选出来的优秀高血压案例和大家分享，探寻理论联系实践的高血压管理之道。</p>
                    <p>衷心期待您的参与并分享高血压管理的理念和手段！</p>
                </div>
                <div class="mb16">
                	<span class="collect_tit">征集时间：</span>2016年4月-2016年6月
                </div>
                <div class="mb16">
                	<div>
                    	<span class="collect_tit">征集方向：</span>针对使用大剂量CCB降压的患者的相关病例
                    </div>
                    <!-- 
                    <div class="pl28">
                        <p class="p_line clearfix">
                        	<span class="left">1)</span>
                            <span class="right">第一期（9月-11月）：针对一二级高血压新患者相关病例</span>
                        </p>
                        <p class="p_line clearfix">
                            <span class="left">2)</span>
                            <span class="right">第二期（12月-次年2月）：针对未达标患者的相关病例</span>
                        </p>
                        <p class="p_line clearfix">
                            <span class="left">3)</span>
                            <span class="right">第三期（3月-5月）：针对难治性高血压患者的相关病例</span>
                        </p>
                    </div>
                    -->
                </div>
                <div class="mb16">
                	<span class="collect_tit">征集科室：</span>临床相关科室
                </div>
                <div class="mb16">
                	<div>
                		<span class="collect_tit">评审机制：</span>
                    </div>
                    <div class="pl28">
                        <p class="p_line clearfix">
                            <span class="left">1)</span>
                            <span class="right">医生在线评审：在国卫健康云网站空中课堂页面中，定期将审核通过的病例进行展示，由网站会员对病例进行投票，评选最打动自己的病例。</span>
                        </p>
                        <p class="p_line clearfix">
                            <span class="left">2)</span>
                            <span class="right">专家评审：建立专家评审组，针对病例进行专业评审，选出的优秀病例可进入国卫健康云中病例点评会，进行深入探讨。</span>
                        </p>
                    </div>
                </div>
                <div class="mb16">
                	<div>
                		<span class="collect_tit">评审标准：</span>
                    </div>
                    <div class="pl28">
                        <p class="p_line clearfix">
                            <span class="left">1)</span>
                            <span class="right">病例内容完整，包含病例各部分信息（25%）</span>
                        </p>
                        <p class="p_line clearfix">
                            <span class="left">2)</span>
                            <span class="right">病例报告真实，能提供实验室检查化验单和影像学检查复印件（15%）</span>
                        </p>
                        <p class="p_line clearfix">
                            <span class="left">3)</span>
                            <span class="right">病例个人体会逻辑清楚，观点鲜明，理由充分（30%）</span>
                        </p>
                        <p class="p_line clearfix">
                            <span class="left">4)</span>
                            <span class="right">提出病例疑点或问题有讨论价值，能启发思考（30%）</span>
                        </p>
                    </div>
                </div>
                <div class="mb16">
                	<div>
                    	<span class="collect_tit">优秀病例分享：</span>
                    </div>
                    <div class="pl28">
						<p>线上评选出的优秀病例，将在国卫健康云病例点评会中，与顶级专家一对一分享和点评病例。</p>
                    </div>
                </div>
                <div class="mb16">
                	<div>
                    	<span class="collect_tit">奖励机制：</span>
                    </div>
                    <div class="pl28">
						<p>积极投票奖：每期征集结束后，对于投票数量最多的5名医生，给予医脉通文献卡一张，专用于文献查询</p>
                    </div>
                </div>
                <div>
                    <div>
                    	<span class="collect_tit">提交方式：</span>
                    </div>
                    <div class="pl28">
                    	<p class="p_line clearfix">
                        	<span class="left">1)</span>
                            <span class="right">方式一：登录国卫健康云网站cdcma.bizconf.cn，点击病例征集页面，进入上传病例页面，按照提交方式一，下载病例模板，填写完成后，点击上传，即可完成提交。</span>
                        </p>
                        <p class="p_line clearfix">
                        	<span class="left">2)</span>
                        	<span class="right">方式二：登陆国卫健康云网站cdcma.bizconf.cn，点击病例征集页面，进入上传病例页面，按照提交方式二，在线填写病例信息，填写完成后点击提交按钮，即可完成提交。</span>
                        </p>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div>
	<div class="clear"></div>
</div>
@stop
