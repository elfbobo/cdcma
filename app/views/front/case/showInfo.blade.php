@extends('front.common.layout')
@section('title')病例征集-国卫健康云 @stop
@section('description')病例征集-国卫健康云@stop
@section('keywords')病例征集-国卫健康云 @stop
@section('content1')
<div class="p_bar_bg bg_color2">
	<div class="wid1025">
    	<div class="p_tit pleft230">
        	<a class="white" href="/">首页</a>
            >
            <a class="white" href="/case/list/0">经典病例</a>
        </div>
    </div>
</div>
<div class="p_center_box">
	<div class="left">
    	<ul>
            <li><a href="/case/rule"><span class="icon06">征集规则</span></a></li>
            <li><a href="/case/upload"><span class="icon08">病例上传</span></a></li>
            <li><a href="/case/e-list"><span class="icon04">病例点评会</span></a></li>
            <li><a href="/case/list/0" class="sele"><span class="icon03">经典病例</span></a></li>
        </ul>
    </div>
    <div class="right wid698">
			@foreach($oCaseInfo as $k=>$v)
		        <div class="">
		            <div class="center_block">
		            	<div class="part_tit">
		                	<h4 class="title center">{{$v->case_name}}</h4>
		                    <p>上传时间：{{$v->created_at}}</p>
		                </div>
		                <div class="part_detail">
		                    <div class="intro_01">
		                        <h4 class="title">【病例亮点】</h4>
		                        <div class="pl22">
		                            <p>{{$v->case_lightspot}}</p>
		                        </div>
		                    </div>
		                    <div class="intro_02">
		                        <h4 class="title">【病例基本信息】</h4>
		                        <div class="pl22">
		                            <p style="margin-bottom:10px;">*病例名称：{{$v->case_name}}</p>
		                        </div>
		                    </div>
		                    <div class="intro_02">
		                        <h4 class="title">【患者基本资料】</h4>
		                        <div class="pl22">
		                            <p style="margin-bottom:10px;">*年　　龄：{{$v->patient_age}}</p>
		                            <p style="margin-bottom:10px;">*性　　别：{{$v->patient_sex}}</p>
		                        </div>
		                    </div>
		                    <div class="intro_02">
		                        <h4 class="title">【病情简介】</h4>
		                        <div class="pl22">
		                            <p>*主　诉：{{$v->self_reported}}</p>
		                            <p>*现病史：{{$v->hpi}}</p>
		                            <p>*既往史：{{$v->pmh}}</p>
		                        </div>
		                    </div>
		                    <div class="intro_03">
		                        <h4 class="title">【相关检查】</h4>
		                        <div class="pl22">
		                            <p><span>*辅助检查：</span></p>
		                            {{$v->sub_exam_text}}
		                            <p><span>*实验室检查：</span></p>
		                            {{$v->lab_exam_text}}
		                        </div>
		                    </div>
		                    <div class="intro_04">
		                        <h4 class="title">【诊断】</h4>
		                        <div class="pl22">
		                            {{$v->diagnose}}
		                        </div>
		                    </div>
		                    <div class="intro_05">
		                        <h4 class="title">【治疗】</h4>
		                        <div class="pl22">
		                            {{$v->cure}}
		                        </div>
		                    </div>
		                    <div class="intro_04">
		                        <h4 class="title">【随访】</h4>
		                        <div class="pl22">
		                            {{$v->follow_view}}
		                        </div>
		                    </div>
		                    <div class="intro_04">
		                        <h4 class="title">【化验单等辅助检查报告复印件（实验室检查、心电图等）】</h4>
		                        <div class="pl22">
		                            <div class="case_img">
		                            @if(!empty($v->sub_exam_fileUrl1))
		                            <img alt="" src="{{$v->sub_exam_fileUrl1}}" style="width:200px;"/>
		                            @endif
		                            @if(!empty($v->sub_exam_fileUrl2))
									<img alt="" src="{{$v->sub_exam_fileUrl2}}" style="width:200px;"/>
									@endif
		                            @if(!empty($v->sub_exam_fileUrl3))
									<img alt="" src="{{$v->sub_exam_fileUrl3}}" style="width:200px;"/>
									@endif
		                        	</div>
		                        </div>
		                    </div>
		                    <div class="intro_06">
		                        <h4 class="title">【病例的血压特点或疑难问题】</h4>
		                        <div class="pl22">
		                            {{$v->dis_qustion}}
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		    @endforeach
<div class="cover" style="display:none;"></div>
    </div>
	<div class="clear"></div>
</div>

@stop