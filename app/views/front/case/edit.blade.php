@extends('front.common.layout')
@section('title')病例征集-国卫健康云 @stop
@section('description')病例征集-国卫健康云@stop
@section('keywords')病例征集-国卫健康云 @stop
@section('content1')
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/case.js"></script>
<script>
	$(function(){
		$("#img_box_one1").hover(function(){
			$("#btn_delete1").toggle();
			});
		$("#img_box_one2").hover(function(){
			$("#btn_delete2").toggle();
			});
		$("#img_box_one3").hover(function(){
			$("#btn_delete3").toggle();
			});
	});
	
</script>
<style type="text/css">
.scan{right:38px;width:60px;filter:alpha(opacity:0);}
</style>
<div class="p_bar_bg bg_color2">
	<div class="wid1025">
    	<div class="p_tit pleft230">
        	<a class="white" href="/">首页</a>
            >
            <a class="white" href="/case/upload">病例上传</a>
        </div>
    </div>
</div>
<div class="p_center_box">
	<div class="left">
    	<ul>
            <li><a href="/case/rule"><span class="icon06">征集规则</span></a></li>
            <li><a href="/case/upload" class="sele"><span class="icon08">病例上传</span></a></li>
            <li><a href="/case/e-list"><span class="icon04">病例点评会</span></a></li>
            <li><a href="/case/list/0"><span class="icon03">经典病例</span></a></li>
            <li><a href="/case/experts"><span class="icon02">点评专家团</span></a></li>
        </ul>
    </div>
    <div class="right wid698">
        <div class="center_block">
            <div class="center_tit">病例征集说明</div>
            <div class="case_intro">
            	<p>随着城市化进程加速、生活方式的改变，高血压患病率增长迅猛，目前我国成人高血压发病率高达29.6%，长期高血压可引起严重的心、脑、肾并发症，并最终导致靶器官的功能衰竭，严重地危害人们的健康和生命，控制高血压是心脑血管病预防的切入点和关键措施。在临床工作中，医务人员发现有些高血压患者平时不关注血压，有些患者服药不规则，或自行停药，导致高血压的控制率偏低。因此，我国高血压的防控形势面临极大的挑战，医生对高血压的管理工作任重而道远。</p>
            </div>
        </div>

        <div class="center_block">
            <form action="/case/edit" method="post" enctype="multipart/form-data" onsubmit="return checkForm();">
            @foreach($oCaseInfo as $k=>$v)
            <input type="hidden" name="id" value="{{$v->id}}"/>
            <div>
                <div class="base_info">
                    <div class="left">
                        <h4 class="info_tit">病例基本信息</h4>
                        <p>
                            <label>病例名称：</label>
                            <input type="text" class="infor_txt" name="case_name" value="{{$v->case_name}}" id="case_name"/>
                        </p>
                        <p>
                            <label>医生姓名：</label>
                            <input type="text" class="infor_txt" name="doctor_name" value="{{$v->doctor_name}}" id="doctor_name"/>
                        </p>
                        <p>
                            <label>电子邮箱：</label>
                            <input type="text" class="infor_txt" name="email" value="{{$v->email}}" id="email"/>
                        </p>
                        <p>
                            <label>科　　室：</label>
                            <input type="text" class="infor_txt" name="department" value="{{$v->department}}" id="department"/>
                        </p>
                        <p>
                            <label>医　　院：</label>
                            <input type="text" class="infor_txt" name="hospital" value="{{$v->hospital}}" id="hospital"/>
                        </p>
                        <p>
                            <label>联系电话：</label>
                            <input type="text" class="infor_txt" name="contact_num" value="{{$v->contact_num}}" id="contact_num"/>
                        </p>
                    </div>
                    <div class="right">
                        <h4 class="info_tit">患者基本资料</h4>
                        <p>
                            <label>年　　龄：</label>
                            <input type="text" class="infor_txt" name="patient_age" value="{{$v->patient_age}}" id="patient_age"/>
                        </p>
                        <p>
                            <label>性　　别：</label>
                            <input type="text" class="infor_txt" name="patient_sex" value="{{$v->patient_age}}" id="patient_sex"/>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="case_detail">
                    <p>主诉：</p>
                    <textarea class="text_box" name="self_reported">{{$v->self_reported}}</textarea>
                    <p>现病史：</p>
                    <textarea class="text_box" name="hpi" id="hpi">{{$v->hpi}}</textarea>
                    <p>既往史：</p>
                    <textarea class="text_box" name="pmh">{{$v->pmh}}</textarea>
                    <p>实验室检查：</p>
                    <textarea class="text_box" name="lab_exam_text">{{$v->lab_exam_text}}</textarea>
                    <p>辅助检查（24小时动态血压监测等）：</p>
                    <textarea class="text_box" name="sub_exam_text">{{$v->sub_exam_text}}</textarea>
                    <p>诊断：</p>
                    <textarea class="text_box" name="diagnose">{{$v->diagnose}}</textarea>
                    <p>治疗：</p>
                    <textarea class="text_box" name="cure">{{$v->cure}}</textarea>
                    <p>随访：</p>
                    <textarea class="text_box" name="follow_view">{{$v->follow_view}}</textarea>
                </div>
                <div class="annex_box">
                    <h4 class="info_tit">请提供化验单等辅助检查报告复印件（实验室检查、心电图等）</h4>
                    <div class="bold">（请粘贴此处或者以附件形式随病例一同提交；请务必模糊患者个人身份信息）</div>
                    <div class="img_box">
                        <input type="hidden" value="{{$picNum}}" class="sub_exam_num" />
                        <div class="img_box_one" id="img_box_one1">
							<img alt="" id="sub_exam_thumb1" src="{{$v->sub_exam_fileUrl1}}"/>
							<input type="button" class="btn_delete" id="btn_delete1" onclick="delThumb(1);"/>
							<input type="hidden" id="sub_exam_fileUrl1" name="sub_exam_fileUrl1" value="{{$v->sub_exam_fileUrl1}}"/>
						</div>
						<div class="img_box_one" id="img_box_one2">
							<img alt="" id="sub_exam_thumb2" src="{{$v->sub_exam_fileUrl2}}"/>
							<input type="button" class="btn_delete" id="btn_delete2" onclick="delThumb(2);"/>
							<input type="hidden" id="sub_exam_fileUrl2" name="sub_exam_fileUrl2" value="{{$v->sub_exam_fileUrl2}}"/>
						</div>
						<div class="img_box_one" id="img_box_one3">
							<img alt="" id="sub_exam_thumb3" src="{{$v->sub_exam_fileUrl3}}"/>
							<input type="button" class="btn_delete" id="btn_delete3" onclick="delThumb(3);"/>
							<input type="hidden" id="sub_exam_fileUrl3" name="sub_exam_fileUrl3" value="{{$v->sub_exam_fileUrl3}}"/>
						</div>
                    </div>
                    <p class="p_right">
                    	<span>（请提供JPG或PNG格式）</span>
                        <input type="file" class="scan sub_exam" onchange="saveThumb('sub_exam');" name="sub_exam" id="sub_exam" style="display:{{$picNum==4?'none':''}};">
                        <input type="button" value="上传图片" class="p_btn"/>
                    </p>
                </div>
                <div class="ques_box">
                    <h4 class="info_tit">请说明该病例的血压特点或疑难问题：</h4>
                    <textarea class="text_box" name="dis_qustion">{{$v->dis_qustion}}</textarea>
                    <div class="center">
                        <input type="submit" value="提交" class="p_btn" name="dosubmit"/>
                    </div>
                </div>
            </div>
            @endforeach
            </form>
        </div>
    </div>
	<div class="clear"></div>
</div>
<script>
	//刷新的时候校验图片
	function func(){
		var sub_exam_fileUrl1 = $("#sub_exam_fileUrl1").val();
		if(sub_exam_fileUrl1 != ''){
			$("#sub_exam_thumb1").attr('src',sub_exam_fileUrl1);
		}
		var sub_exam_fileUrl2 = $("#sub_exam_fileUrl2").val();
		if(sub_exam_fileUrl2 != ''){
			$("#sub_exam_thumb2").attr('src',sub_exam_fileUrl2);
		}
		var sub_exam_fileUrl3 = $("#sub_exam_fileUrl3").val();
		if(sub_exam_fileUrl3 != ''){
			$("#sub_exam_thumb3").attr('src',sub_exam_fileUrl3);
		}
	
		if(sub_exam_fileUrl1 != '' && sub_exam_fileUrl2 != '' && sub_exam_fileUrl3 != ''){
			$(".sub_exam").hide();
		}
	}
	window.onload=func;
	
	function checkForm(){
		var case_name = $("#case_name").val();
		var doctor_name = $("#doctor_name").val();
		var email = $("#email").val();
		var department = $("#department").val();
		var hospital = $("#hospital").val();
		var contact_num = $("#contact_num").val();
		var patient_age = $("#patient_age").val();
		var patient_sex = $("#patient_sex").val();
		if(case_name!='' && doctor_name!='' && email!='' && department!='' && hospital!='' && contact_num!='' && patient_age!='' && patient_sex!=''){
			return true;
		}else{
			alert('请完善病例基本信息和患者基本资料');
			return false;
		}
	}
</script>
@stop