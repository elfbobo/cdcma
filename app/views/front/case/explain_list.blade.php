@extends('front.common.layout')
@section('title')病例征集-国卫健康云 @stop
@section('description')病例征集-国卫健康云@stop
@section('keywords')病例征集-国卫健康云 @stop
@section('content1')
<div class="p_bar_bg bg_color2">
	<div class="wid1025">
    	<div class="p_tit pleft230">
        	<a class="white" href="/">首页</a>
            >
            <a class="white" href="/case/e-list">病例点评会</a>
        </div>
    </div>
</div>

<div class="p_center_box">
	<div class="left">
    	<ul>
            <li><a href="/case/rule"><span class="icon06">征集规则</span></a></li>
            <li><a href="/case/upload"><span class="icon08">病例上传</span></a></li>
            <li><a href="/case/e-list" class="sele"><span class="icon04">病例点评会</span></a></li>
            <li><a href="/case/list/0"><span class="icon03">经典病例</span></a></li>
            <li><a href="/case/experts"><span class="icon02">点评专家团</span></a></li>
        </ul>
    </div>
    <div class="right wid698">
    	<div class="center mar_b20">
        	<img src="/assets/images/front/web/logo.jpg" />
        </div>
        <div class="center_block">
            <div class="center_tit">本期推荐</div>
            @if($oRecVideo)  
            <div class="cur_rec clearfix">
                <div class="left">
                	@if(($oRecVideo->meeting_starttime<date('Y-m-d H:i:s'))&&($oRecVideo->meeting_endtime>date('Y-m-d H:i:s')))
                    <a href="javascript:void 0" onclick="window.location.href='/consultation/enter/{{$oRecVideo->id}}/1'"><img src="{{$oRecVideo->meeting_thumb}}" /></a>
                    @else
                    <a href="javascript:void 0" onclick="block_text('直播尚未开始或已结束')"><img src="{{$oRecVideo->meeting_thumb}}" /></a>
                    @endif
                    
                </div>
                <div class="right">
                    <p><label>主题：</label>{{$oRecVideo->meeting_title}}</p>
                    <p><label>时间：</label>{{$oRecVideo->meeting_starttime}}</p>
                    <p><label>专家：</label>{{$oDocs[$oRecVideo->expert_id]}}</p>
                </div> 
            </div>
            @endif
        </div>
        <div class="center_block pd20">
            <div class="center_tit">往期回顾</div>
            <div class="review_box">
            	@foreach($oVideos as $k=>$v)
            	@if($k%2==0)
            	<div class="review_line clearfix @if($k==0 && count($oVideos)>2 ) bor_btm @endif">
            	@endif
                    <div class="block_box left">
                        <div class="sml_tit"><a class="tit" href="/case/e-show/{{$v->id}}">{{$v->meeting_title}}</a></div>
                        <div class="block_img">
                            <a href="/case/e-show/{{$v->id}}"><img src="{{$v->meeting_thumb}}" /></a>
                        </div>
                        <div class="block_cont">
                            <p>专家：{{$oDocs[$v->expert_id]}}</p>       
                            <p>病例提交者： {{$v->case_submiter}}</p>
                        </div>            
                    </div>
                @if($k%2==1)
            	</div>
            	@endif
                @endforeach
			</div>
        </div>
        <div class="p_number_box">
            <div>{{$oVideos->links()}}</div>
    	</div>
    </div>
	<div class="clear"></div>
</div>
<div class="pop_logoin_box center" style="top:0;display:none;" id="pop_apply_success_box">
                    <div class="pop_cont2 pop_ptop60">
                        <p id="pop_apply_success_box_html">您已报名成功，感谢您的参与。</p>
                    </div>
                    <div class="center">
                        <input class="save" type="button" value="确定" onclick="$.unblockUI()"/>
                    </div>
                </div>
<script type="text/javascript">
function block_text(text){
                		$('#pop_apply_success_box_html').html(text);
	                    $.blockUI({ message: $('#pop_apply_success_box')});
                    }
</script>                   
@stop