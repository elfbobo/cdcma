@extends('front.common.layout')
@section('title')病例征集-国卫健康云 @stop
@section('description')病例征集-国卫健康云@stop
@section('keywords')病例征集-国卫健康云 @stop
@section('content1')
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/case.js"></script>
<style type="text/css">
.file{ position:absolute; top:96px; right:300px; height:24px; filter:alpha(opacity:0);opacity: 0;width:310px }
.sub_exam{ position:absolute; top:96px; right:300px; height:24px; filter:alpha(opacity:0);width:310px }
</style>
<div class="p_bar_bg bg_color2">
	<div class="wid1025">
    	<div class="p_tit pleft230">
        	<a class="white" href="/">首页</a>
            >
            <a class="white" href="/case/upload">病例上传</a>
        </div>
    </div>
</div>
<div class="p_center_box">
	<div class="left">
    	<ul>
            <li><a href="/case/rule"><span class="icon06">征集规则</span></a></li>
            <li><a href="/case/upload" class="sele"><span class="icon08">病例上传</span></a></li>
            <li><a href="/case/e-list"><span class="icon04">病例点评会</span></a></li>
            <li><a href="/case/list/0"><span class="icon03">经典病例</span></a></li>
            <li><a href="/case/experts"><span class="icon02">点评专家团</span></a></li>
        </ul>
    </div>
    <div class="right wid698">
        <div class="center_block">
            <div class="center_tit">病例征集说明</div>
            <div class="case_intro">
            	<p>随着城市化进程加速、生活方式的改变，高血压患病率增长迅猛，目前我国成人高血压发病率高达29.6%，长期高血压可引起严重的心、脑、肾并发症，并最终导致靶器官的功能衰竭，严重地危害人们的健康和生命，控制高血压是心脑血管病预防的切入点和关键措施。在临床工作中，医务人员发现有些高血压患者平时不关注血压，有些患者服药不规则，或自行停药，导致高血压的控制率偏低。因此，我国高血压的防控形势面临极大的挑战，医生对高血压的管理工作任重而道远。</p>
            </div>
        </div>
        <div class="center_block">
            <div class="center_tit">方式一：在线上传</div>
            @foreach($oCaseInfo as $k=>$v)
            <div class="online_upload">
            	<p class="font12 mar_b28"><a href="/case/downCase/{{$v->id}}">点击下载&nbsp;&nbsp;({{$v->case_name}})</a></p>
                <p>
                	<input type="hidden" value="{{$v->id}}" id="caseId" />
                	<input type="text" class="infor_txt" id='textfield'/>
                    <input type="button" value="浏览..." class="p_btn" onclck="$('#fileField').click;"/>
                    <input type="file" name="fileField" class="file" id="fileField" onchange="document.getElementById('textfield').value=this.value"  />
                    <input type="button" value="开始上传" class="p_btn p_btn_bg" onclick="saveCaseEdit()"/>
                </p>
                <p class="font12">（注：如果您已填写好病例模板可直接上传。）</p>
            </div>
            @endforeach
        </div>
    </div>
	<div class="clear"></div>
</div>
@stop