@extends('front.common.layout')
@section('title')病例征集-国卫健康云 @stop
@section('description')病例征集-国卫健康云@stop
@section('keywords')病例征集-国卫健康云 @stop
@section('content1')
<div class="p_bar_bg bg_color2">
	<div class="wid1025">
    	<div class="p_tit pleft230">
        	<a class="white" href="/">首页</a>
            >
            <a class="white" href="/case/list/0">经典病例</a>
        </div>
    </div>
</div>
<div class="p_center_box">
	<div class="left">
    	<ul>
            <li><a href="/case/rule"><span class="icon06">征集规则</span></a></li>
            <li><a href="/case/upload"><span class="icon08">病例上传</span></a></li>
            <li><a href="/case/e-list"><span class="icon04">病例点评会</span></a></li>
            <li><a href="/case/list/0" class="sele"><span class="icon03">经典病例</span></a></li>
            <li><a href="/case/experts"><span class="icon02">点评专家团</span></a></li>
        </ul>
    </div>
    <div class="right wid698">
    	<div class="center mar_b20">
        	<img src="/assets/images/front/web/logo.jpg" />
        </div>
        <div class="center_block pd20 mbtm15">
			@foreach($oCase as $k=>$v)
            <div class="case_line @if($k%5==4 || ($k+1) == count($oCase)) bor_btm0 @endif">
            	<p><span>病例名称：</span>{{$v->case_name}}</p>
                <p><span>上传时间：</span>{{$v->created_at}}</p>
                <p><span>病例亮点：</span>{{str_cut_cms($v->case_lightspot,55)}}<a href="/case/showInfo/{{$v->id}}">【详情】</a></p>
            </div>
            @endforeach
        </div>
        <div class="p_number_box">
            <div>{{$oCase->links()}}</div>
    	</div>
    </div>
	<div class="clear"></div>
</div>

@stop