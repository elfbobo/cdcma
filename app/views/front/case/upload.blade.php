@extends('front.common.layout')
@section('title')病例征集-国卫健康云 @stop
@section('description')病例征集-国卫健康云@stop
@section('keywords')病例征集-国卫健康云 @stop
@section('content1')
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/case.js"></script>
<script>
	$(function(){
		$(".center_tit").click(function(){
			$("#blank_fill").toggle();
			});
		$("#img_box_one1").hover(function(){
			$("#btn_delete1").toggle();
			});
		$("#img_box_one2").hover(function(){
			$("#btn_delete2").toggle();
			});
		$("#img_box_one3").hover(function(){
			$("#btn_delete3").toggle();
			});
	});
	
</script>
<style type="text/css">
.file{ position:absolute; top:96px; right:300px; height:24px; filter:alpha(opacity:0);opacity: 0;width:310px }
.scan{right:38px;width:60px;filter:alpha(opacity:0);}
.p_btn2{ 
	background-color: grey;
    border: 1px solid grey;
    color: #fff;
    cursor: pointer;
    font: 14px/22px "微软雅黑";
    height: 26px;
    outline: medium none;
    width: 76px;
}
</style>
<div class="p_bar_bg bg_color2">
	<div class="wid1025">
    	<div class="p_tit pleft230">
        	<a class="white" href="/">首页</a>
            >
            <a class="white" href="/case/upload">病例上传</a>
        </div>
    </div>
</div>
<div class="p_center_box">
	<div class="left">
    	<ul>
            <li><a href="/case/rule"><span class="icon06">征集规则</span></a></li>
            <li><a href="/case/upload" class="sele"><span class="icon08">病例上传</span></a></li>
            <li><a href="/case/e-list"><span class="icon04">病例点评会</span></a></li>
            <li><a href="/case/list/0"><span class="icon03">经典病例</span></a></li>
            <li><a href="/case/experts"><span class="icon02">点评专家团</span></a></li>
        </ul>
    </div>
    <div class="right wid698">
    	<div class="center mar_b20">
        	<img src="/assets/images/front/web/logo.jpg" />
        </div>
        <div class="center_block">
            <div class="center_tit">征集内容要求</div>
            <div class="case_intro">
            	<p>随着城市化进程加速、生活方式的改变，高血压患病率增长迅猛，目前我国成人高血压发病率高达29.6%，长期高血压可引起严重的心、脑、肾并发症，并最终导致靶器官的功能衰竭，严重地危害人们的健康和生命，控制高血压是心脑血管病预防的切入点和关键措施。在临床工作中，医务人员发现有些高血压患者平时不关注血压，有些患者服药不规则，或自行停药，导致高血压的控制率偏低。因此，我国高血压的防控形势面临极大的挑战，医生对高血压的管理工作任重而道远。</p>
            </div>
        </div>
        <div class="center_block">
            <div class="center_tit">病例征集说明</div>
            <div class="case_intro">
            	<p><span>征集时间：</span>2016年4月-6月</p>
                <p><span>征集方向：</span>针对使用大剂量CCB降压的患者的相关病例</p>
            </div>
        </div>
        <div class="center_block">
            <div class="center_tit">方式一：在线上传</div>
            <div class="online_upload">
            	<p class="font12 mar_b28"><a href="/case/downModel">点击下载病例模板</a></p>
                <p>
                	<input type="text" class="infor_txt" id='textfield'/>
                    <input type="button" value="浏览..." class="p_btn" onclck="$('#fileField').click;"/>
                    <input type="file" name="fileField" class="file" id="fileField" onchange="document.getElementById('textfield').value=this.value"  />
                    <input type="button" value="开始上传" class="p_btn p_btn_bg" onclick="saveCase()"/>
                </p>
                <p class="font12">（注：如果您已填写好病例模板可直接上传。）</p>
            </div>
        </div>
        <div class="center_block">
            <div class="center_tit">
                方式二：在线填写
            <input type="button" class="drop_btn" id="drop_btn"/>
            </div>
            <form action="/case/upload" method="post" enctype="multipart/form-data" onsubmit="return checkForm();">
            <div class="blank_fill" id="blank_fill">
                <div class="base_info">
                    <div class="left">
                        <h4 class="info_tit">病例基本信息</h4>
                        <p>
                            <label>病例名称：</label>
                            <input type="text" class="infor_txt" name="case_name" id="case_name"/>
                        </p>
                        <p>
                            <label>医生姓名：</label>
                            <input type="text" class="infor_txt" name="doctor_name" id="doctor_name"/>
                        </p>
                        <p>
                            <label>电子邮箱：</label>
                            <input type="text" class="infor_txt" name="email" id="email"/>
                        </p>
                        <p>
                            <label>科　　室：</label>
                            <input type="text" class="infor_txt" name="department" id="department"/>
                        </p>
                        <p>
                            <label>医　　院：</label>
                            <input type="text" class="infor_txt" name="hospital" id="hospital"/>
                        </p>
                        <p>
                            <label>联系电话：</label>
                            <input type="text" class="infor_txt" name="contact_num" id="contact_num"/>
                        </p>
                    </div>
                    <div class="right">
                        <h4 class="info_tit">患者基本资料</h4>
                        <p>
                            <label>年　　龄：</label>
                            <input type="text" class="infor_txt" name="patient_age" id="patient_age"/>
                        </p>
                        <p>
                            <label>性　　别：</label>
                            <input type="text" class="infor_txt" name="patient_sex" id="patient_sex"/>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="case_detail">
                    <p>主诉：</p>
                    <textarea class="text_box" name="self_reported"></textarea>
                    <p>现病史：</p>
                    <textarea class="text_box" name="hpi" id="hpi"></textarea>
                    <p>既往史：</p>
                    <textarea class="text_box" name="pmh"></textarea>
                    <p>实验室检查：</p>
                    <textarea class="text_box" name="lab_exam_text"></textarea>
                    <p>辅助检查（24小时动态血压监测等）：</p>
                    <textarea class="text_box" name="sub_exam_text"></textarea>
                    <p>诊断：</p>
                    <textarea class="text_box" name="diagnose"></textarea>
                    <p>治疗：</p>
                    <textarea class="text_box" name="cure"></textarea>
                    <p>随访：</p>
                    <textarea class="text_box" name="follow_view"></textarea>
                </div>
                <div class="annex_box">
                    <h4 class="info_tit">请提供化验单等辅助检查报告复印件（实验室检查、心电图等）</h4>
                    <div class="bold">（请粘贴此处或者以附件形式随病例一同提交；请务必模糊患者个人身份信息）</div>
                    <div class="img_box">
                   	 	<input type="hidden" value="1" class="sub_exam_num" />
                    	<div class="img_box_one" id="img_box_one1" style="display:none;">
							<img alt="" id="sub_exam_thumb1" src=""/>
							<input type="button" class="btn_delete" id="btn_delete1" onclick="delThumb(1);"/>
							<input type="hidden" id="sub_exam_fileUrl1" name="sub_exam_fileUrl1" />
						</div>
						<div class="img_box_one" id="img_box_one2" style="display:none;">
							<img alt="" id="sub_exam_thumb2" src=""/>
							<input type="button" class="btn_delete" id="btn_delete2" onclick="delThumb(2);"/>
							<input type="hidden" id="sub_exam_fileUrl2" name="sub_exam_fileUrl2" />
						</div>
						<div class="img_box_one" id="img_box_one3" style="display:none;">
							<img alt="" id="sub_exam_thumb3" src=""/>
							<input type="button" class="btn_delete" id="btn_delete3" onclick="delThumb(3);"/>
							<input type="hidden" id="sub_exam_fileUrl3" name="sub_exam_fileUrl3" />
						</div>
                    </div>
                    <p class="p_right">
                    	<span>（请提供JPG或PNG格式）</span>
                        <input type="file" class="scan sub_exam" onchange="saveThumb('sub_exam');" name="sub_exam" id="sub_exam">
                        <input type="button" value="上传图片" class="p_btn"/>
                    </p>
                </div>
                <div class="ques_box">
                    <h4 class="info_tit">请说明该病例的血压特点或疑难问题：</h4>
                    <textarea class="text_box" name="dis_qustion"></textarea>
                    <div class="center">
                        <input type="submit" value="提交" class="p_btn" name="dosubmit"/>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
	<div class="clear"></div>
</div>
<!-- 遮罩 -->
<div class="cover"></div>
<div class="pop_state_box">
	<div class="close_btn" style="top:5px;">
    	<input type="button" onclick="window.location.href='/case/rule'" class="btn">
    </div>
	<h4>免责声明</h4>
    <div class="state_info" style="font-size: 13px;line-height:21px;">
        <div>欢迎访问“国卫健康云病例征集页面”，您在使用本网站服务时，即表示您接受下列条款和条件的约束。</div>
        <div class="about">关于本站</div>
        <div>
            <span class="left">(1)</span>
            <span class="right">本网站中的病例的版权归《中华医学杂志》社所有，若作者有版权声明的或文章从其它网站转载而附带有原所有站的版权声明者，其版权归属以附带声明为准。</span>
        </div>
        <div class="clear"></div>
        <div>
        	<span class="left">(2)</span>
            <span class="right">禁止在本网站病例征集中泄露任何能辨认患者个人身份的信息。</span>
        </div>
        <div class="clear"></div>
        <div>
        	<span class="left">(3)</span>
            <span class="right">病例上传者同意将相关个人信息进行展示，所有权归《中华医学杂志》社所有。</span>
        </div>
        <div class="clear"></div>
        <div>
        	<span class="left">(4)</span>
        	<span class="right">任何单位或个人认为本站内容可能涉嫌侵犯其合法权益，应该及时向本站书面反馈，并提供身份证明、权属证明及详细情况证明，本站在收到上述文件后将会尽快移除相关内容。由于与本网站链接的其它网站所造成之个人资料泄露及由此而导致的任何法律争议和后果，本网站均得免责。</span>
        </div>
        <div class="clear"></div>
        <div>
            <span class="left">(5)</span>
            <span class="right">由于用户将个人密码告知他人或与他人共享注册帐户，由此导致的任何个人资料泄露，本网站不负任何责任。</span>
        </div>
        <div class="clear"></div>
        <div>
            <span class="left">(6)</span>
            <span class="right">当政府司法机关依照法定程序要求本网站披露个人资料时，我们将根据执法单位之要求或为公共安全。</span>
        </div>
        <div class="clear"></div>
    </div>
    <div class="if_agree">
    	<p>
        	<input type="checkbox" onclick="check(1);" id="check1"/>
            <label for="check1"><span>已阅读以上责任条款</span></label>
        </p>
        <p>
        	<input type="checkbox" disabled="disabled" onclick="check(2);" id="check2"/>
            <label for="check2"><span>您同意将您的病例在此平台展示及投票？</span></label>
        </p>
        <p>
        	<input type="checkbox" disabled="disabled" onclick="check(3);" id="check3"/>
            <label for="check3"><span>请确定您提交的病例隐藏患者信息</span></label>
        </p>
    </div>
    <div class="center">
    	<input type="button" class="p_btn2" value="提交" onclick="check(4);" id="check4"/>
    </div>
</div>
<script>
	//刷新的时候校验图片
	function func(){
		var sub_exam_fileUrl1 = $("#sub_exam_fileUrl1").val();
		if(sub_exam_fileUrl1 != ''){
			$("#img_box_one1").show();
			$("#sub_exam_thumb1").attr('src',sub_exam_fileUrl1);
		}
		var sub_exam_fileUrl2 = $("#sub_exam_fileUrl2").val();
		if(sub_exam_fileUrl2 != ''){
			$("#img_box_one2").show();
			$("#sub_exam_thumb2").attr('src',sub_exam_fileUrl2);
		}
		var sub_exam_fileUrl3 = $("#sub_exam_fileUrl3").val();
		if(sub_exam_fileUrl3 != ''){
			$("#img_box_one3").show();
			$("#sub_exam_thumb3").attr('src',sub_exam_fileUrl3);
		}

		if(sub_exam_fileUrl1 != '' && sub_exam_fileUrl2 != '' && sub_exam_fileUrl3 != ''){
			$(".sub_exam").hide();
		}
		//免责声明
		var checkbox1 = document.getElementById('check1');
		var checkbox2 = document.getElementById('check2');
		var checkbox3 = document.getElementById('check3');
		if(checkbox1.checked && checkbox2.checked && checkbox3.checked){
			$('.p_btn2').addClass('p_btn').removeClass('p_btn2');
		}else{
			$('.p_btn').addClass('p_btn2').removeClass('p_btn');
		}
	}
	window.onload=func;
	
	function checkForm(){
		var case_name = $("#case_name").val();
		var doctor_name = $("#doctor_name").val();
		var email = $("#email").val();
		var department = $("#department").val();
		var hospital = $("#hospital").val();
		var contact_num = $("#contact_num").val();
		var patient_age = $("#patient_age").val();
		var patient_sex = $("#patient_sex").val();
		if(case_name!='' && doctor_name!='' && email!='' && department!='' && hospital!='' && contact_num!='' && patient_age!='' && patient_sex!=''){
			return true;
		}else{
			alert('请完善病例基本信息和患者基本资料');
			return false;
		}
	}
	function check(type){
		if(type == 1){
			var checkbox = document.getElementById('check'+type);
			if(checkbox.checked){
				document.getElementById("check2").disabled=false;
				document.getElementById("check3").disabled=false;
			}else{
				document.getElementById("check2").disabled=true;
				document.getElementById("check2").checked=false;
				document.getElementById("check3").disabled=true;
				document.getElementById("check3").checked=false;
				$('.p_btn').addClass('p_btn2').removeClass('p_btn');
			}
		}else if(type == 2 || type == 3){
			var checkbox1 = document.getElementById('check1');
			var checkbox2 = document.getElementById('check2');
			var checkbox3 = document.getElementById('check3');
			if(checkbox1.checked && checkbox2.checked && checkbox3.checked){
				$('.p_btn2').addClass('p_btn').removeClass('p_btn2');
			}else{
				$('.p_btn').addClass('p_btn2').removeClass('p_btn');
			}
		}else if(type == 4){
			var checkbox1 = document.getElementById('check1');
			var checkbox2 = document.getElementById('check2');
			var checkbox3 = document.getElementById('check3');
			if(checkbox1.checked && checkbox2.checked && checkbox3.checked){
				$('.cover').hide();
				$('.pop_state_box').hide();
			}
		}
		
	}
</script>
@stop