@extends('front.common.layout')
@section('title')病例征集-国卫健康云 @stop
@section('description')病例征集-国卫健康云@stop
@section('keywords')病例征集-国卫健康云 @stop
@section('content1')
<script src="/assets/js/video.js"></script>
<div class="p_bar_bg bg_color2">
	<div class="wid1025">
    	<div class="p_tit pleft230">
        	<a class="white" href="/">首页</a>
            >
            <a class="white" href="/case/e-list">病例点评会</a>
        </div>
    </div>
</div>
<div class="p_center_box">
	<div class="left">
    	<ul>
            <li><a href="/case/rule"><span class="icon06">征集规则</span></a></li>
            <li><a href="/case/upload"><span class="icon08">病例上传</span></a></li>
            <li><a href="/case/e-list" class="sele"><span class="icon04">病例点评会</span></a></li>
            <li><a href="/case/list/0"><span class="icon03">经典病例</span></a></li>
            <li><a href="/case/experts"><span class="icon02">点评专家团</span></a></li>
        </ul>
    </div>
    <div class="right wid698">
        <div class="center_block">
            <div class="center_tit">病例点评会</div>
			<div class="p_part">
            	<h5>{{$oReview->meeting_title}}</h5>
                <p class="p_time">{{$oReview->meeting_time}}</p>
                <p class="p_info">{{$oReview->meeting_description}}</p>
                <div class="detailed_video" id="a1">
                <br><br><br><br><br>
    			<span style="text-align:center;color:#717071;text-align:center;">浏览器版本过低，请您升级浏览器后观看</span>
            <br><br><br><br><br>	
                </div>
            </div>
            
            
        </div>
    </div>
	<div class="clear"></div>
</div>
<script type="text/javascript" src="/assets/js/player/ckplayer/ckplayer.js" charset="utf-8"></script>
<script type="text/javascript">
	var __flg; 
	var flashvars={
		f:'{{Config::get("config.cdn_url").str_replace(".mp4",".flv",$oReview->meeting_url)}}',
		c:0,
		//lv:1,
		//b:1,
		h:1,
		loaded:'loadedHandler',
		};
	var video=['{{Config::get("config.cdn_url").str_replace(".mp4",".flv",$oReview->meeting_url)}}','{{Config::get("config.cdn_url").str_replace(".mp4",".flv",$oReview->meeting_url)}}','{{Config::get("config.cdn_url").str_replace(".mp4",".flv",$oReview->meeting_url)}}'];
	CKobject.embed('/assets/js/player/ckplayer/ckplayer.swf','a1','ckplayer_a1','548','329',false,flashvars,video);


</script>   
@stop