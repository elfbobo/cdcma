@extends('front.common.layout')
@section('title')登录-国卫健康云 @stop
@section('description')登录-国卫健康云@stop
@section('keywords')登录-国卫健康云 @stop
@section('content2')
	<div class="page-header">
	  <h1> <small>慢病管理系统-登录</small></h1>
	</div>
	<form class="form-horizontal" style="margin-top:200px;width:400px;margin-left:500px;" method="post" action="/login">
	  <div class="form-group" style="">
	    <label for="user_nick" class="col-sm-2 control-label" style="width:100px;">用户名：</label>
	    <div class="col-sm-10" style="width:300px;">
	      <input type="text" class="form-control" id="user_nick" name="user_nick" placeholder="请输入用户名">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="password" class="col-sm-2 control-label" style="width:100px;">密码：</label>
	    <div class="col-sm-10" style="width:300px;">
	      <input type="password" class="form-control" id="password" name="password" placeholder="请输入密码">
	    </div>
	  </div>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default" style="margin-left:27px;">登录</button>
	    </div>
	  </div>
	</form>
@stop