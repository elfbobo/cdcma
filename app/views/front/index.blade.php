@extends('front.common.layout')
@section('title')国卫健康云 @stop
@section('description')国卫健康云@stop
@section('keywords')国卫健康云 @stop
@section('content1')
<script type ="text/javascript">
function addHots(id,urls){
	var url = "/information/add/"+id;
	$.post(url,{},function(msg){
		if(msg=='success'){
			window.location.href=urls;
		}
	})
}
</script>
<div class="upload_pop_box" style="position: absolute;top:330px;right:10px;display:none;" id="fly_box">
	<div class="pop_img"><img src="/assets/images/front/web/top_img.png" /></div>
    <div class="pop_body">
    	<p>为了促进医生对高血压管理的学习并且分享其高血压治疗体会，由中华医学会、慢性疾病与转化医学（英文）发起，基于国卫健康云平台的高血压案例征集活动，将对筛选出来的优秀高血压案例和大家分享，探寻理论联系实践的高血压管理之道。</p>
        <p>衷心期待您的参与并分享高血压管理的理念和方法！</p>
        @if(!Auth::check())
		<div class="two_btn">
            <input type="button" class="btn" value="参与投票" onclick="alert('请您先登录')"/>
            <input type="button" class="btn" value="上传病例" onclick="alert('请您先登录')"/>
        </div>
		@else
		<div class="two_btn">
            <input type="button" class="btn" value="参与投票" onclick="window.location.href='/docface'"/>
            <input type="button" class="btn" value="上传病例" onclick="window.location.href='/case/upload'"/>
        </div>
		@endif
    </div>
    <div class="pop_img">
    	<img src="/assets/images/front/web/btm_img.png" />
    </div>
</div>
<script>
window.onscroll=function ()
{
	var oDiv=document.getElementById('fly_box');
	var scrollTop=document.documentElement.scrollTop||document.body.scrollTop;
	var t=scrollTop+(document.documentElement.clientHeight-oDiv.offsetHeight)/2;
	startMove(parseInt(t));
}
var timer=null;
function startMove(iTarget)
{
	if(iTarget<330){
		iTarget = 330;
	}
	var oDiv=document.getElementById('fly_box');
	clearInterval(timer);
	timer=setInterval(function (){
		var iSpeed=(iTarget-oDiv.offsetTop)/8;
		iSpeed=iSpeed>0?Math.ceil(iSpeed):Math.floor(iSpeed);
		if(oDiv.offsetTop==iTarget){
			clearInterval(timer);
		}else{
			oDiv.style.top=oDiv.offsetTop+iSpeed+'px';
		}
	}, 30);
}
</script> 
<div class="p_intro_box">
        <div class="left">
            <div class="intro_tit">空中课堂精彩预告</div>
            @if($oFaceVideoAdvance)
            <div class="intro_doctor">
            	<div class="left">
                	<a href="/docface"><img src="{{$oFaceVideoAdvance->doc_info->doc_thumb}}" /></a>
                </div>
                <div class="right">
                	<p><label>时间：</label>{{substr($oFaceVideoAdvance->start_time,0,16)}}—{{substr($oFaceVideoAdvance->end_time,11,5)}}</p>
                    <p class="theme"><label>主题：</label><a href="">{{$oFaceVideoAdvance->video_title}}</a></p>
                    <p><label>专家：</label>{{$oFaceVideoAdvance->doc_info->doc_name}}</p>
                    <p><label>医院：</label>{{$oFaceVideoAdvance->doc_info->doc_hospital}}</p>
                </div>
                <div class="clear"></div>
            </div>
            <div class="intro_cont">
            	<div class="intro_sml_tit">课程简介 :</div>
                <div class="intro_sml_cont">
                	{{str_cut_cms($oFaceVideoAdvance->video_introduce,200)}}
                </div>
            </div>
            @endif
            <div class="intro_more">
            	<a class="more" href="/docface">more</a>
            </div>
        </div>
        <div class="right">
        	<div class="intro_tit search_title">文献检索</div>
            <div class="search_condition">
                <div class="tab">
                    <span>中文文献</span>
                </div>
                <div class="condition">
                    <select id="term_one" name="term_one">
                        <option value="全部">全部</option>
                        <option value="主题">主题</option>
                        <option value="题名或关键词">题名或关键词</option>
                        <option value="题名">题名</option>
                        <option value="创作者">创作者</option>
                        <option value="作者单位">作者单位</option>
                        <option value="关键词">关键词</option>
                        <option value="摘要">摘要</option>
                        <option value="日期">日期</option>
                        <option value="期刊—刊名">期刊—刊名</option>
                        <option value="期刊—期">期刊—期</option>
                    </select>
                    <input type="text" id="keyword_one" name="keyword_one" value="">
                </div>
                <div class="tab tab2">
                    <span>PubMed</span>
                </div>
                <div class="condition2">
                <a href="/pmd">
                    <p>高级检索_检索字段、日期等限定</p>
                    <p>高级检索_研究对象、语种、文献类型限定</p>
                </a>
                </div>
                <div class="btn_box">
                    <button class="btn" id="cn_search" onclick="search();">我要检索</button>
                </div>
            </div>	
            <script type="text/javascript">
            function search(){
                var term_one = $('#term_one').val();
                var keyword_one = $('#keyword_one').val();
                window.location.href="/cnsearch/result/"+term_one+"/"+keyword_one;
            }
            </script> 
           <!-- 
            <div class="intro_tit">资讯中心</div>
            @foreach($oInformation as $value)
               @if($aNewOrNot[$value->id] == 1)
		            <div class="message_line msg_new">
		            	<div class="m_border">
		                	<div class="left">
		                    	 <a href="javascript:void(0)" onclick="addHots({{$value->id}},'{{$value->info_url}}')">{{$value->info_title}}</a>
		                    </div>
		                    <div class="right">{{date("Y-m-d",strtotime(strval($value->created_at)))}}</div>
		                	<div class="clear"></div>
		                </div>                
		            </div>
		        @else 
		           <div class="message_line">
		            	<div class="m_border">
		                	<div class="left">
		                    	 <a href="javascript:void(0)" onclick="addHots({{$value->id}},'{{$value->info_url}}')">{{$value->info_title}}</a>
		                    </div>
		                    <div class="right">{{date("Y-m-d",strtotime(strval($value->created_at)))}}</div>
		                	<div class="clear"></div>
		                </div>                
		            </div>
		        
		        @endif
            @endforeach
            @foreach($aMaterialNews as $v)
            	@if($v['newsflag'] == 1)
            		<div class="message_line msg_new">
		            	<div class="m_border">
		                	<div class="left">
		                    	 <a href="/information/show/{{$v['id']}}">{{$v['title']}}</a>
		                    </div>
		                    <div class="right">{{$v['created_at']}}</div>
		                	<div class="clear"></div>
		                </div>                
		            </div>
		         @else
		         	<div class="message_line">
		            	<div class="m_border">
		                	<div class="left">
		                    	 <a href="/information/show/{{$v['id']}}">{{$v['title']}}</a>
		                    </div>
		                    <div class="right">{{$v['created_at']}}</div>
		                	<div class="clear"></div>
		                </div>                
		            </div>
		         @endif
            @endforeach
            <div class="intro_more" style=" padding-top:4px;">
            	<a class="more" href="/information">more</a>
            </div>
             -->
        </div>
        <div class="clear"></div>
</div>
    
@stop
@section('content2')
<!--
@if($Consultation1||$Consultation2||$Consultation3)
<div class="p_bar_bg">
	<div class="wid1025">
    	<div class="p_tit">专家大会诊精彩预告</div>
    </div>
</div>
<div class="wid1025">
	<div class="block_bg">
	@if($Consultation1)
    	<div class="block_box left">
        	<div class="sml_tit">跨领域学术交流</div>
            <div class="block_cont">
            	<p>时间：{{substr($Consultation1->meeting_starttime,0,16)}} - {{substr($Consultation1->meeting_endtime,11,5)}}</p>
                <p class="theme2">主题：<a href="/consultation">{{$Consultation1->meeting_title}}</a></p>
                <p>本期话题：</p>
                @foreach($oTopic as $k=>$v)
                <p><a href="#">{{$k+1}}. {{$v->topic_title}}</a></p>
                @endforeach
            </div>
            <div class="intro_more">
            	<a class="more more4_img" href="/consultation">more</a>
            </div>
        </div>
    @endif
    @if($Consultation2)
        <div class="block_box left m33">
        	<div class="sml_tit">科室与专家大会诊</div>
            <div class="block_img">
            	<a href="/consultation"><img src="{{$Consultation2->doc_thumb}}" /></a>
            </div>
            <div class="block_cont">
            	<p>时间：{{substr($Consultation2->meeting_starttime,0,16)}} - {{substr($Consultation2->meeting_endtime,11,5)}}</p>
                <p class="theme2">主题：<a href="/consultation">{{$Consultation2->meeting_title}}</a></p>            
            </div>            
        </div>
    @endif
    @if($Consultation3)
        <div class="block_box right">
        	<div class="sml_tit">疑难病例讨论</div>
            <div class="block_img">
            	<a href="/consultation"><img class="img0" src="{{$Consultation3->doc_thumb}}" /></a>
            </div>
        	<div class="block_cont">
            	<p>时间：{{substr($Consultation3->meeting_starttime,0,16)}} - {{substr($Consultation3->meeting_endtime,11,5)}}</p>
                <p class="theme2">主题：<a href="/consultation">{{$Consultation3->meeting_title}}</a></p>
            </div>            
        </div>
     @endif
        <div class="clear"></div>
    </div>
</div>
@endif
-->
<div class="p_bar_bg bg_color1"><div class="wid1025"><div class="p_tit">精品课程</div></div></div>
<div class="wid1025">
    <div class="block_bg" style="text-align: center;color: red;font-size: 20px;vertical-align: middle;display: table-cell;">
        @if(!count($recordfqlistRs))
        精彩内容即将上线，敬请期待！
        @else
        <div class="sml_menu_box">
            <ul></ul>
            <a id="change_more1" class="a_more" href="/docface/recview">more</a>
            <div class="clear"></div>
        </div>
        <div class="block_line" id="change_content1">
            @foreach($recordfqlistRs as $k=>$v)
            <div class="block_box1 left @if($k%3==1) dashed @endif">
                <div class="center">
                    <a href="/docface/recview-show/{{$v->id}}" class="sml_tit red mx_h">{{str_cut_cms($v->video_title,32)}}</a>
                </div>
                <div class="block_img">
                    <a href="/docface/recview-show/{{$v->id}}"><img src="{{$v->video_thumb}}" /></a>
                </div>
                <div class="block_cont height72">
                    <p>专家：{{$v->doc_name}}</p>
                    <p class="theme2">医院： {{str_cut_cms($v->doc_hospital,28)}}</p>
                </div>
            </div>
            @endforeach
            <div class="clear"></div>
        </div>
        @endif
    </div>
</div>
@if(count($oFaceVideoReview))
<div class="p_bar_bg bg_color1">
	<div class="wid1025">
    	<div class="p_tit">精彩回顾</div>
    </div>
</div>
<script>
function change_content(id){
	if(id==1){
		var other_id = 2;
	}else{
		var other_id = 1;
	}
	$('#change_title'+other_id).removeClass('sele');
	$('#change_content'+other_id).hide();
	$('#change_more'+other_id).hide();
	$('#change_title'+id).addClass('sele');
	$('#change_content'+id).show();
	$('#change_more'+id).show();
}
</script>
<div class="wid1025">
	<div class="block_bg">
    	<div class="sml_menu_box">
        	<ul>
            	<!-- 
            	<li><a class="sele" id="change_title1" onclick="change_content(1)" style="cursor:pointer;">空中课堂</a></li>
                <li class="border1"></li>
                <li><a id="change_title2" onclick="change_content(2)" style="cursor:pointer;">专家大会诊</a></li>
                 -->
            </ul>
           	<a id="change_more1" class="a_more" href="/docface/review">more</a>
           	<a id="change_more2" class="a_more" style="display:none;" href="/consultation/list">more</a>
            <div class="clear"></div>
        </div>
        <div class="block_line" id="change_content1">
        	@foreach($oFaceVideoReview as $k=>$v)
        	<div class="block_box1 left @if($k%3==1) dashed @endif">
            	<div class="center">
            		<a href="/docface/review-show/{{$v->id}}" class="sml_tit red mx_h">{{str_cut_cms($v->video_title,32)}}</a>
                </div>
                <div class="block_img">
            		<a href="/docface/review-show/{{$v->id}}"><img src="{{$v->video_thumb}}" /></a>
            	</div>
                <div class="block_cont height72">
                	<p>专家：{{$v->doc_name}}</p>
                    <p class="theme2">医院： {{str_cut_cms($v->doc_hospital,28)}}</p>
                </div>
            </div>
            @endforeach
            
            <div class="clear"></div>
        </div>
        <div class="block_line" id="change_content2" style="display:none;">
        	@foreach($Consultation4 as $k=>$v)
        	<div class="block_box1 left">
            	<div class="center">
            		<a href="/consultation/show/{{$v->id}}" class="sml_tit red mx_h">{{str_cut_cms($v->meeting_title,32)}}</a>
                </div>
                <div class="block_img">
            		<a href="/consultation/show/{{$v->id}}"><img src="{{$v->meeting_thumb}}" /></a>
            	</div>
                <div class="block_cont height72">
                	<p>专家：{{$v->doc_name}}</p>
                    <p class="theme2">医院： {{str_cut_cms($v->doc_hospital,28)}}</p>
                </div>
            </div>
            @endforeach
            <div class="clear"></div>
        </div>
    </div>
</div>
@endif
<div class="p_bar_bg bg_color2">
	<div class="wid1025">
    	<div class="p_tit">专家风采</div>
    </div>
</div>
<div class="wid1025">
	<div class="block_bg block_bg2">
    	<div class="sml_menu_box">
           	<a class="b_more" href="/docface/doc-list">more</a>
        </div>
        @foreach($oDocs as $k=>$v)
    	<div class="expert_box left @if($k%5==4) no_border @endif">
        	<div class="expert_img">
            	<a href="/docface/doc-show/{{$v->id}}"><img src="{{$v->doc_thumb}}" /></a>
            </div>
            <div class="expert_name">
            	<a href="/docface/doc-show/{{$v->id}}">{{$v->doc_name}}</a>
            </div>
        </div>
        @endforeach
        <div class="clear"></div>
    </div>
</div>
@include('front.common.contact')
@if(!Auth::check())
<script type="text/javascript">
window.onload = function(){
	var aa = document.getElementsByTagName("a");
	for(var i=0;i<aa.length;i++){
		aa[i].setAttribute("href","#");
		aa[i].onclick=function(){
			please_login();return;
		}
	}
	document.getElementById("docface_banner").setAttribute("href","/docface");
	document.getElementById("index_banner").setAttribute("href","/");
	document.getElementById("docface_banner").setAttribute("onclick","");
	document.getElementById("index_banner").setAttribute("onclick","");
	document.getElementById("login_banner").onclick=function(){show_login_box()}
	document.getElementById("register_banner").setAttribute("href","/register");
	document.getElementById("register_banner").setAttribute("onclick","");
//	document.getElementById("codeimg_banner").setAttribute("onclick","");
}
function please_login(){
	alert('请您先登录');return;
}
</script>
@endif
<!-- 判断用户是否已经更新了省份城市医院信息 -->
@if(Auth::User()&&Auth::User()->role_id==3&&is_numeric(Auth::User()->user_city)&&is_numeric(Auth::User()->user_company))
	@if(!empty($aQuestion))
		@include('front.common.survey');
	@endif
@endif

@stop
