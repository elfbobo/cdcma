@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/user.js"></script>
<script>
function change_uinfo(){
	var user_name = $.trim($('#user_name').val());
	var password_old = $('#password_old').val();
	var password_new = $('#password_new').val();
	var password_new_re = $('#password_new_re').val();
	if(!user_name){
		alert('请输入新的用户名');
		return ;
	}
	if(!password_old){
		//不修改密码
		alert('请输入旧密码');return;
//		url = '/user/chenge-user-info/'+user_name;
	}else{
		//修改密码
		if(!password_new){
			alert('请输入新密码');return;
		}
		if(password_new!=password_new_re){
			alert('两次修改的密码不一致，请重新输入');
			return;
		}
		url = '/user/chenge-user-info/'+user_name+'/'+password_old+'/'+password_new;
	}
	$.post(url,{},function(msg){
		if(msg=='nick_repeat'){
			alert('该用户名已经存在，换换其他用户名试试');
		}else if(msg=='old_err'){
			alert('原密码输入错误');
		}else if(msg=='success'){
			alert('修改成功');
			window.location.reload();
		}
	})
}
</script>
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/user">个人中心</a>
    >
    <a href="javascript::void(0)">个人信息</a>
</div>

<div class="p_center_box fund_page">
    @include('front.common.ucleft',['nowpos'=>'index'])
    <div class="right fund_intro Personal_page">
        <div class="">
            <div class="center_block">
                <div class="center_title clearfix">
                    <span>个人资料</span>
                    <a href="#" class="right set">修改资料</a>
                </div>
                <div class="">
                    <div class="upload_box">
                        <div >
                            <img  id="user_thumb" src="{{$oUser->user_thumb}}"/>
                            <span class="chg_txt">更改头像</span>
                            <input class="upload_btn" id="upload_file" name="upload_file"  type="file" onchange="saveThumb()" />    
                        </div>
                        <div class="name">姓名：{{$oUser->user_name}}</div>
                    </div>
                    <div class="center_infor">
                        <div>
                            <div class="left_txt">性别：</div>
                            <div class="right_txt" style="">@if($oUser->user_sex==1) 男 @else 女 @endif</div>
                            <div class="clear"></div>
                        </div>
                        <div>
                            <div class="left_txt">手机：</div>
                            <div class="right_txt" style="">{{$oUser->user_tel}}</div>
                            <div class="clear"></div>
                        </div>
                        <div>
                            <div class="left_txt">邮箱：</div>
                            <div class="right_txt" style="">{{$oUser->user_email}}</div>
                            <div class="clear"></div>
                        </div>
                        <div>
                            <div class="left_txt">地址：</div>
                            <div class="right_txt" style="">{{$oUser->user_address}}</div>
                            <div class="clear"></div>
                        </div>
                        <div>
                            <div class="left_txt">职位：</div>
                            <div class="right_txt" style="">{{$oUser->user_position}}</div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="center_infor2">
                        <div style="display:none;">
                            <div class="left_txt">省份：</div>
                            <div class="right_txt" style="">北京</div>
                            <div class="clear"></div>
                        </div>
                        <div  style="display:none;">
                            <div class="left_txt">城市：</div>
                            <div class="right_txt" style="">北京</div>
                            <div class="clear"></div>
                        </div>
                        @if($oUser->role_id==3)
                    	 <div>
                            <div class="left_txt">医院：</div>
                            <div class="right_txt" style="">{{$oUser->user_company}}</div>
                            <div class="clear"></div>
                        </div>
                    	@endif
                        @if($oUser->role_id==3)
	                        <div>
	                            <div class="left_txt">科室：</div>
	                            <div class="right_txt" style="">{{$oUser->user_department}}</div>
	                            <div class="clear"></div>
	                        </div>
                        @else
	                        <div>
	                            <div class="left_txt">大区：</div>
	                            <div class="right_txt" style="">{{$oUser->user_regin}}</div>
	                            <div class="clear"></div>
	                        </div>
	                        <div>
	                            <div class="left_txt">地区：</div>
	                            <div class="right_txt" style="">{{$oUser->user_area}}</div>
	                            <div class="clear"></div>
	                        </div>
                        @endif
                        <div>
                            <div class="left_txt">用户编码：</div>
                            <div class="right_txt" style="">{{strtoupper($oUser->invite_code)}}</div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="center_block">
                <div class="center_title">
                    <span>密码修改</span>
                </div>
                <div class="center_infor3">
                    <p><label>用户名：</label>{{$oUser->user_nick}}<input class="infor_txt" type="hidden" name="user_name" id="user_name" value="{{$oUser->user_nick}}" /></p>
                    <p><label>旧密码：</label><input class="infor_txt wid198" type="password" name="password_old" id="password_old" /></p>
                    <p><label>新密码：</label><input class="infor_txt wid198" type="password" name="password_new" id="password_new" /></p>
                    <p><label>确认新密码：</label><input class="infor_txt wid198" type="password" name="password_new_re" id="password_new_re"/></p>
                </div>
                <div class="btn_box">
                    <input class="btn" type="button" value="保　存" onclick="change_uinfo()"/>
                </div>
            </div>
        </div>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop