@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<script src="/assets/js/video.js"></script>
	<div class="p_sml_menu">
    	您当前位置：
    	<a href="/">首页</a>
        >
        <a href="/user">个人中心</a>
        >
        <a href="/user/my-train">我的培训</a>
        >{{$oTrain->train_title}}
    </div>
    <div class="p_video_box">
    	<div class="detailed_box">
        	<div class="bold">{{$oTrain->train_title}}</div>
            <div class="detailed_time">时间：{{date('Y-m-d',strtotime($oTrain->start_time))}}</div>
            <div class="detailed_video" id="a1">
            <br><br><br><br><br>
    			<span style="text-align:center;color:#717071;text-align:center;">浏览器版本过低，请您升级浏览器后观看</span>
            <br><br><br><br><br>
            </div>
            <div class="detailed_good">
            	<a @if($oTrain->train_download_url) onclick="download_video('{{$oTrain->train_download_url}}',3,{{$oTrain->id}})" @endif class="download" style="cursor:pointer;"></a>
                <a class="explain" onmouseover="$('#pop_explain').show()" style="cursor:pointer;"></a>
            	<a href="javascript:void(0)" class="good" onclick="docface_video_zan({{$oTrain->id}},{{Session::get('userid')}},3)"></a>
                <a href="javascript:void(0)" class="good_txt" id="docface_video_zan">{{$oTrain->train_support}}</a>
                <div class="pop_logoin_box center" id="pop_explain" style="display:none;">
                    <div class="pop_cont2" style="text-align:left;">
                        	下载密码：{{$oTrain->train_download_psw}}
                    </div>
                    <div class="center">
                        <input class="save" type="button" value="确定" onclick="$('#pop_explain').hide();"/>
                    </div>
                </div>
            </div>
        </div>        
    </div>    
<?php 
   $sVideoUrl = '';
   if (!empty($oTrain->train_url)){
   	if (user_agent_is_mobile()){
        $sVideoUrl = Config::get("config.cdn_url").$oTrain->train_url;
   	}else{
   		$sVideoUrl = Config::get("config.cdn_url").str_replace(".mp4",".flv",$oTrain->train_url);
   	}
   }
?>
<script type="text/javascript" src="/assets/js/player/ckplayer/ckplayer.js" charset="utf-8"></script>
<script type="text/javascript">
	var __flg; 
	var flashvars={
		f:'{{$sVideoUrl}}',
		c:0,
		p:2,
	    b:0,
	    loaded:'loadedHandler',
		};
	var video=['{{$sVideoUrl}}','{{$sVideoUrl}}','{{$sVideoUrl}}'];
	CKobject.embed('/assets/js/player/ckplayer/ckplayer.swf','a1','ckplayer_a1','548','329',false,flashvars,video);
	function loadedHandler(){
	    if(CKobject.getObjectById('ckplayer_a1').getType()){
	    	//添加监听handler
	        CKobject.getObjectById('ckplayer_a1').addListener('play',playHandler);
	        CKobject.getObjectById('ckplayer_a1').addListener('pause',pauseHandler);
	    }
	    else{
	        //添加监听handler
	        CKobject.getObjectById('ckplayer_a1').addListener('play','playHandler');
	        CKobject.getObjectById('ckplayer_a1').addListener('pause','pauseHandler');
	    }
	}
	
	function playHandler(){
		//用户积分
		clearInterval(__flg);
		__flg = self.setInterval("addUserMinute({{$oTrain->id}},{{Session::get('userid')}})",18000);
	}

	function addUserMinute(videoid,userid){
		var time_second = Math.round(CKobject.getObjectById('ckplayer_a1').getStatus().time);
		var time_minute = Math.round(time_second/60);
		//console.log(time_second);
		var url = "/user/video-view/"+videoid+"/"+userid+"/"+time_minute;
		$.post(url,{},function(msg){
			if(msg=='success'){
				console.log('success');
			}
		})
	}

	function ckplayer_status(str){
		//播放结束
		if(str == 'ended'){
			clearInterval(__flg);
			addUserMinute({{$oTrain->id}},{{Session::get('userid')}});
		}
	}

    
</script>
<div class="txt_bg02_wrapper" >
</div>
<div class="p_bar_bg bg_color2 wid1025" style="display:none;">
    <div class="p_tit">医友评论</div>
</div>
<div class="txt_bg02_wrapper" style="display:none;">
        <div class="txt_bg02_top"></div>
        <div class="txt_bg02_cont">
            @foreach($oComments as $oComment)
        	<div class="comment_line">
            	<div class="left wid80">
                	<a href="#"><img class="pic" src="{{$oComment->user_thumb}}" /></a>
                </div>
                <div class="right wid405">
                	<div class="name_line"><a href="#" class="name">{{$oComment->user_nick}}</a></div>
                    <div class="">
                    	<div class="left wid280">{{$oComment->comment}}</div>
                        <div class="right wid125">
                        	<a class="hand" href="javascript:void(0)" onclick="comment_zan({{$oComment->id}},3)">（<span id="comment_zan_{{$oComment->id}}">{{$oComment->zan_count}}
                        	</span>）</a>
                            <a class="reply" href="javascript:void(0)"  onclick="video_reply({{$oComment->user_id}},'{{$oComment->user_nick}}')">回复</a>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            @endforeach
            
            <div class="people_num">已有<span>{{count($oComments)}}</span>位医友发表了看法</div>
             <div class="comment_cont">
            	<div id='reply' value="" style='color:#666;position: relative;left:4px;top:1px;width:668px;height:22px;display:none;'>
				</div>
				<div name="content" id="content" contentEditable="true" style="outline:none;background-color: #FFFFFF; color: #888888;font: 14px/24px '微软雅黑';border: 1px solid #e0e0e0;height: 96px;overflow-y: auto;padding: 5px;resize: none;width: 482px;">
				</div>
            </div>
            <div class="comment_btn_box" style="position: relative;">
            	<a class="face" href="javascript:void(0);" id="showpic">插入表情</a>
            	<div  class="" style="background:#fff;position:absolute;left:8px;top:50px;display:none;height:89px;border:1px solid #D1D1D1;margin-left:14px;padding:5px 10px 5px 10px;" id="pic_list">{{$sImgLi}}
	                 <a href='javascript:void(0);' onclick='hidePic(0)' class='icon close1'></a>
	            </div>
                <input id="replay_box" class="comment_btn" type="button" value="发表评论" onclick="reply({{$oTrain->id}},3);"/>
                <div class="clear"></div>
            </div>
            </div>
            <div class="txt_bg02_btm"></div>
    </div>
<script>
	document.onclick=function(event){
		var e = event || window.event;
		var elem = e.srcElement||e.target;
		while(elem){
			if(elem.id == 'showpic' || elem.id == 'pic_list'){
				$("#pic_list").show();
				return;
			}else{
				$("#pic_list").hide();
				//return;
			}
			elem = elem.parentNode;
		}
	}
</script>
@stop
