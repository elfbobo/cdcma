@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<script src="/assets/js/user.js"></script>
<div class="p_bar_bg  bg_color2">
	<div class="wid1025">
    	<div class="p_tit pleft230">
        	<a class="white" href="/user">个人中心</a>
            >
            <a class="white" href="/user/my-order">我的预约</a>
            >
            本月可预约专家
        </div>
    </div>
</div>
<div class="p_center_box">
	<div class="left">
    	<ul>
            <li><a href="/user"><span class="icon02">个人信息</span></a></li>
            <li><a href="/user/my-score"><span class="icon03">我的积分</span></a></li>
            <li><a class="sele" href="/user/my-order"><span class="icon04">我的预约</span></a></li>
            <li><a href="/user/score-list/{{Auth::User()->role_id}}"><span class="icon05">积分排行</span></a></li>
        	@if(Auth::User()->role_id==2)
            <li><a href="/user/my-train"><span class="icon06">我的培训</span></a></li>
            @endif
            <!--  
            <li><a href="/user/caselist/1"><span class="icon06">我的病例</span></a></li>
            @if(Auth::User()->role_id==2)
            <li><a href="/user/mydoc"><span class="icon09">医生管理</span></a></li>
            @endif
            -->
        </ul>
    </div>
    <div class="right wid698">
        <div class="center_block">
            <div class="center_tit">本月可预约专家</div>
            @foreach($oDoc as $k => $v)
            <div class="appointment_box">
            	<div class="appointment_img">
                	<a href="#"><img src="{{$v->doc_thumb}}" /></a>
                </div>
                <div class="appointment_name">
                	<p class="an_txt">{{$v->doc_name}}</p>
                    <p>{{$v->doc_hospital}}</p>
                </div>
                <div class="appointment_time">
                	<p>预约时间：{{substr($v->start_time,0,16)}}—{{substr($v->end_time,11,5)}}</p>
                    <p>科室：{{$v->doc_department}}</p>
                </div>
                <div class="appointment_btn">
                	<input class="p_btn" type="button" value="预　约" @if($iIsInFront) onclick="order_do({{$v->id}})" @else onclick="$.blockUI({ message: $('#cannot_order_box')})" @endif/>
                </div>
                <div class="clear"></div>
        	</div>
            @endforeach
        </div>
    </div>
	<div class="clear"></div>
</div>

<div id="order_success_box" class="pop_logoin_box" style="display:block;display:none;">
	<div class="pop_cont2 pop_ptop60 center">
    	预约已提交，预约审核通过后会与您电话联系，请耐心等待
    </div>
    <div class="center">
    	<input type="button" class="p_btn" value="确定" onclick="$.unblockUI();"/>
    </div>
</div>

<div id="already_order_box" class="pop_logoin_box" style="display:block;display:none;">
	<div class="pop_cont2 pop_ptop60 center">
    	您已经预约了该专家，无需重复预约
    </div>
    <div class="center">
    	<input type="button" class="p_btn" value="确定" onclick="$.unblockUI();"/>
    </div>
</div>

<div id="cannot_order_box" class="pop_logoin_box" style="display:block;display:none;">
	<div class="pop_cont2 pop_ptop60 center">
    	积分排行前50名才能预约专家
    </div>
    <div class="center">
    	<input type="button" class="p_btn" value="确定" onclick="$.unblockUI();"/>
    </div>
</div>

@stop
