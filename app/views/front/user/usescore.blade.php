@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<script src="/assets/js/user.js"></script>
<style type="text/css">
<!--
.useScoreBtns { display:inline-block;padding:0px 15px;line-height:35px;font-size:14px;color:#FFF;background:#7BB547;margin:8px 0; }
.introBoxs { margin:30px 0 60px 0; }
.introBoxs h1 { font-size:20px;color:#0090D3;border-bottom:1px dashed #999;line-height:30px;text-indent:4px;padding-bottom:10px;margin-bottom:10px; }
.introBoxs p { font-size:14px;color:#888;line-height:180%;padding-left:4px; }
.introBoxs ul { margin:0px auto;width:630px;padding:80px 0 30px 0;display:table; }
.introBoxs ul li { width:210px;float:left;text-align:center;font-size:16px;line-height:220%; }
.introBoxs ul li b { font-size:22px;color:#ff9900; }
.introBoxs ul li a { padding:0px 30px;line-height:40px;font-size:16px;color:#FFF;display:table;background:#C31A1F;margin-top:8px; }
-->
</style>
<div class="p_sml_menu new_menu">您当前位置：<a href="/">首页</a> > <a href="/user">个人中心</a> > <a href="/user/my-health-score">我的健康云积分</a> > 兑换</div>
<div class="p_center_box fund_page">
    @include('front.common.ucleft',['nowpos'=>'myhealthscore'])
    <div class="right fund_intro Personal_page">
        <div class="content" style="min-height:500px;">
            <div class="my_points"><dl style="border:none;">
                <dt><img src="/assets/images/front/web/rank.png" alt=""></dt>
                <dd style="line-height:30px;padding:15px 0;font-size:14px;">
                    可兑换积分：<span>{{$integral}}</span>
                </dd>
            </dl></div>
            <div class="introBoxs">
                <h1>积分规则</h1>
                <p>{{$configRs->exchange_rule_integral}}</p>
            </div>
            <div class="introBoxs">
                <h1>积分兑换</h1>
                <ul>
                    <li>
                        <img src="/assets/images/front/web/cme.png"><br />
                        <b>{{$configRs->cmc_use_integral}}</b> 分<br />
                        <a href="javascript:;" class="createOrders" id="buycme" style="background:#0090D3;">立即兑换</a>
                    </li>
                    <li>
                        <img src="/assets/images/front/web/jpkc.png"><br />
                        <b>{{$configRs->view_video_deduct_integral}}</b> 分<br />
                        <a href="/docface/recview">立即兑换</a>
                    </li>
                    <li>
                        <img src="/assets/images/front/web/kypx.png"><br />
                        <b>{{@$configRs->kypx_use_integral?$configRs->kypx_use_integral:0}}</b> 分<br />
                        <a href="javascript:;" class="createOrders" id="buyKypx" style="background:#74B537;">立即兑换</a>
                    </li>
                </ul>
            </div>
    </div>
	<div class="clear"></div>
</div>
<div class="clear" style="height:30px;"></div>
<script type="text/javascript">
$(function(){
    $(".createOrders").on("click", function(){
        var obj_type = $(this).attr("id");
        $.ajax({
            url: "/user/integral-do?obj_type="+obj_type+"&obj_id=0",
            dataType: 'json',
            success: function(data){
                alert(data.msg);
                setTimeout(function(){
                    window.location.reload();
                }, 1000);
                return false;
            }
        });
        /*$.get("/user/integral-do?obj_type=buycme&obj_id=0",{},function(msg){
            if(msg=='success'){
                console.log('success');
            }
        })*/
    });
});
</script>
@include('front.common.contact')
@stop