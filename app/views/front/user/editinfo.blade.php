@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/user.js"></script>
<script>
function change_uinfo(){

	//用户基本信息修改
	var user_tel = $.trim($('#user_tel').val());
	var user_email = $.trim($('#user_email').val());
	var user_address = $.trim($('#user_address').val());
	var user_position = $("#user_pos_select  option:selected").val();
	var user_company_name = $.trim($('#user_company_name').val());
	var user_department = $("#user_dep_select  option:selected").val();
	if(!user_tel){
		alert('请输入您的手机号码！');
		return false;
	}
	if(!user_email){
		alert('请输入您的邮箱！');
		return false;
	}
	var reg_mobile = /^[0-9]{11}$/;
	istel= reg_mobile.test(user_tel);
	if (!istel ) {
	    alert("手机号码格式不正确！");
	    return false;
	  }
	  var reg = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
	  ismail= reg.test(user_email);
	  if (!ismail ) {
	    alert("邮箱格式不正确！");
	    return false;
	  }

	if(!user_position||user_position==0){
		alert('请选择您的职称！');
		return false;
	}
	if(!user_department||user_department==0){
		alert('请选择您的科室！');
		return false;
	}
	//省份、城市、区县、医院
	var user_province = $("#hospital1  option:selected").val();
	var user_city = $("#hospital2  option:selected").val();
	var user_county = $("#hospital3 option:selected").val();
	var user_company = $("#hospital4  option:selected").val();
	if(!user_province||user_province==0){
		alert('请选择您的省份！');
		return false;
	}
	if(!user_city||user_city==0){
		alert('请选择您的城市！');
		return false;
	}
	if(!user_county||user_county==0){
		alert('请选择您所在的区/县！');
		return false;
	}
	if(!user_company||user_company==0){
		if(!user_company_name){
			alert('请选择您的医院！');
			return false;
		}
	}

//	//用户密码修改
//	var password_old = $.trim($('#password_old').val());
//	var password_new = $.trim($('#password_new').val());
//	var password_new_re = $.trim($('#password_new_re').val());
//	if(password_old||password_new||password_new_re){
//		//修改密码
//		if(!password_old){
//			alert('请输入旧密码');return;
//		}else{
//			if(!password_new){
//				alert('请输入新密码');return;
//			}
//			if(password_new!=password_new_re){
//				alert('两次修改的密码不一致，请重新输入');
//				return;
//			}
//		}
//		var data = {'user_address':user_address,
//				'user_position':user_position,
//				'user_province':user_province,
//				'user_city':user_city,
//				'user_county':user_county,
//				'user_company':user_company,
//				'user_company_name':user_company_name,
//				'user_department':user_department,
//				'password_old':password_old,
//				'password_new':password_new
//				};
//	}else{
		//不修改密码
		var data = {'user_tel':user_tel,
				'user_email':user_email,
				'user_address':user_address,
				'user_position':user_position,
				'user_province':user_province,
				'user_city':user_city,
				'user_county':user_county,
				'user_company':user_company,
				'user_company_name':user_company_name,
				'user_department':user_department
				};

//	}

	var url = '/user/change-user-info-all';
	$.post(url,data,function(msg){
		if(msg=='nick_repeat'){
			alert('该用户名已经存在，换换其他用户名试试');
		}else if(msg=='tel_repeat'){
			alert('该手机号码用户已经存在了');
		}else if(msg=='email_repeat'){
			alert('该邮箱用户已经存在了');
		}else if(msg=='old_err'){
			alert('原密码输入错误');
		}else if(msg=='success'){
			alert('用户信息修改成功');
			window.location.href='/user';
		}else{
			alert('网络错误，请刷新当前页面重试');
		}
	})	
}
</script>
<div class="p_sml_menu new_menu">
      您当前位置：
    <a href="/">首页</a>
    >
    <a href="/user">个人中心</a>
    >
   修改资料
</div>
<div class="p_center_box fund_page">
    <div class=" wid698 wid933"  style="min-height:400px;">
        <div class="">
            <div class="center_block">
            	<div class="center_tit">个人资料</div>
                <div class="">
                	<div class="upload_box">
                    	<div >
                    		<img id="user_thumb" src="{{$oUser->user_thumb}}" />
                        	<span class="chg_txt">更改头像</span>
                        	<input class="upload_btn" id="upload_file" name="upload_file"  type="file" onchange="saveThumb()" />                        
                        </div>
                        <div class="name">姓名：{{$oUser->user_name}}</div>
                    </div>
                     <div class="center_infor wid260">
                        <p><label>性别：</label>
                        	@if($oUser->user_sex==1) 男 @else 女 @endif
                        </p>
                       <p>
                        	<label>手机：</label>
                        	<input type="text" id="user_tel" name="user_tel" value="{{$oUser->user_tel}}"/>
                        </p>
                        <p>
                        	<label>邮箱：</label>
                        	<input type="text" id="user_email" name="user_email" value="{{$oUser->user_email}}"/>
                        </p>
                        <p><label>地址：</label>
                        	<input type="text" id="user_address" name="user_address" value="{{$oUser->user_address}}"/>
                        </p>
                        <?php 
                        	$aPos = User::getPosition();
                        ?>
                        <p>
	                        <label><span class="red">*</span>职称：</label>
	                        <select id="user_pos_select" style="width:197px;margin-top:5px;">
					            <option value="0">=请选择=</option>
					            @foreach($aPos as $k=>$v)
					            <option value="{{$v}}" @if($oUser->user_position==$v) selected @endif>{{$v}}</option>
					            @endforeach
					        </select>
	                    </p>
                        <p><label>用户编码：</label>{{strtoupper($oUser->invite_code)}}</p>
                    </div>
                    <div class="center_infor wid260">
	                        <p>
                        	<label><span class="red">*</span>省份：</label>
	                        	<select id="hospital1" class="sele_txt" onchange="change_hospital(2)">
						            <option value="0">=请选择=</option>
						            @foreach($aHosp1 as $k=>$v)
						            <option value="{{$k}}" @if($oUser->user_province==$k) selected @endif>{{$v}}</option>
						            @endforeach
						        </select>
	                        </p>
	                        <p>
		                        	<label><span class="red">*</span>城市：</label>
		                        	<select id="hospital2" class="sele_txt" onchange="change_hospital(3)">
						            <option value="0">=请选择=</option>
						            @foreach($aHosp2 as $k=>$v)
						            <option value="{{$k}}" @if($oUser->user_city==$k) selected @endif>{{$v}}</option>
						            @endforeach
						            </select>
		                    </p>
		                    <p>
		                        	<label><span class="red">*</span>区/县：</label>
		                        	<select id="hospital3" class="sele_txt" onchange="change_hospital(4)">
						            <option value="0">=请选择=</option>
						            @foreach($aHosp3 as $k=>$v)
						            <option value="{{$k}}" @if($oUser->user_county==$k) selected @endif>{{$v}}</option>
						            @endforeach
						            </select>
		                    </p>
	                        <p style="width:400px;">
	                        	<label><span class="red">*</span>医院：</label>
	                        	<select id="hospital4" class="sele_txt" onchange="change_hospital(5)">
						            <option value="0">=请选择=</option>
						            @foreach($aHosp4 as $k=>$v)
						            <option value="{{$k}}" @if($oUser->user_company==$k) @if(!$oUser->user_company_name) selected @endif @endif>{{$v}}</option>
						            @endforeach
						        </select>
						        <a style="cursor:pointer;color:#254797;" onclick="$('#user_company_div').show();">手动填写?</a>
	                        </p>
	                        <p @if(!$oUser->user_company_name) style="display:none;" @endif id="user_company_div">
	                        	<label>&nbsp;</label>
	                        	<input type="text" id="user_company_name" name="user_company_name" value="{{$oUser->user_company_name}}"/>  
	                        </p>
	                                                <?php 
                        	$aDep = User::getDepartment();
                        ?>
                        <input type="hidden" id="user_province" name="user_province" value="0"/>
                        <input type="hidden" id="user_city" name="user_city" value="0"/> 
                        <input type="hidden" id="user_county" name="user_county" value="0"/>
                        <input type="hidden" id="user_company" name="user_company" value="0"/>   
	                        <p>
	                        	<label><span class="red">*</span>科室：</label>
	                        	<select id="user_dep_select" class="sele_txt">
					            <option value="0">=请选择=</option>
					            @foreach($aDep as $k=>$v)
					            <option value="{{$v}}" @if($oUser->user_department==$v) selected @endif>{{$v}}</option>
					            @endforeach
					            </select>
					            <input type="hidden" name="user_department" id="user_department" value=""/> 
	                        </p>
	                        
	                        
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="center_block" style="display:none;">
            	<div class="center_tit">密码修改</div>
                <div class="center_infor3">
                    <p><label>用户名：</label>{{$oUser->user_nick}}<input class="infor_txt" type="hidden" name="user_name" id="user_name" value="{{$oUser->user_nick}}" /></p>
                    <p><label>旧密码：</label><input class="infor_txt" type="password" name="password_old" id="password_old"  /></p>
                    <p><label>新密码：</label><input class="infor_txt" type="password" name="password_new" id="password_new" /></p>
                    <p><label>确认新密码：</label><input class="infor_txt wid198" type="password" name="password_new_re" id="password_new_re" /></p>
                </div>
            </div>
        </div>
        <div class="center">
        	<input class="save" type="button" value="确     定" onclick="change_uinfo()"/>
        </div>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop