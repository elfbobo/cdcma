@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<script src="/assets/js/user.js"></script>
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/user">个人中心</a>
    >
    积分排行
</div>
<div class="p_center_box fund_page">
    @include('front.common.ucleft',['nowpos'=>'myscore'])
	<!-- <div class="left">
            <ul>
                <li><a href="/user"><span class="personal_icon1">个人信息</span></a></li>
                <li><a href="/user/my-score" class="sele"><span class="personal_icon2_2">我的积分</span></a></li>
                @if(Auth::User()->role_id==3)
                <li><a href="/aspirin/my-auth"><span class="personal_icon3">我的认证</span></a></li>
                @endif
                <li><a href="/aspirinonline/my-enter-list"><span class="personal_icon4">我的会议</span></a></li>
                <li><a href="/aspirinoffline/index"><span class="personal_icon5">线下会议</span></a></li>
                <li><a href=""><span class="personal_icon6">视频下载</span></a></li>
                <li><a href="/aspirinresearch/my-list"><span class="personal_icon7">科研培训</span></a></li>
                <li><a href="/aspirin/contact"><span class="personal_icon8">联系客服</span></a></li>
                @if(Auth::User()->role_id==2)
                <li><a href="/user/my-train"><span class="personal_icon9">我的培训</span></a></li>
                @endif
                @if(Auth::User()->role_id==2)
                <li><a href="/user/my-order"><span class="icon04">我的预约</span></a></li>
                @endif
                <li><a href="/user/caselist/1"><span class="icon06">我的病例</span></a></li>
                @if(Auth::User()->role_id==2)
                <li><a href="/user/mydoc"><span class="icon09">医生管理</span></a></li>
                @endif
            </ul>
        </div> -->
	<div class="right fund_intro Personal_page">
        <div class="kinds_tab clearfix">
            <div onclick="window.location.href='/user/my-score';" style="cursor:pointer;">我的积分</div>
            <div class="sele" onclick="window.location.href='/user/score-list/{{Auth::User()->role_id}}';" style="cursor:pointer;">积分排行</div>
            <div class="bottom_line"></div>
        </div>
        <div class="sml_menu_box">
            <ul class="rank">
                <li><a @if($iRoleId==3) class="sele" @endif href="/user/score-list/3">医生</a></li>
                <li class="border1"></li>
                <li><a @if($iRoleId==2) class="sele" @endif href="/user/score-list/2">医学顾问</a></li>
            </ul>
            <div class="clear"></div>
        </div>
        <div class="content">
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <td class="special2" style="width: 100px">排名</td>
                        <td style="width: 150px">用户姓名</td>
                        <td style="width: 150px" >直播积分</td>
                        <td style="width: 150px" >录播积分</td>
                        <td style="width: 150px" >总积分</td>
                    </tr>
                </thead>
                <tbody>
                	@foreach($aUser as $k=>$v)
                    <tr>
                        <td @if($k==0) class="special2 T_1st" @elseif($k==1) class="special2 T_2nd" @elseif($k==2) class="special2 T_3rd" @else class="special2" @endif>{{$k+1}}</td>
                        <td>{{$v['user_name']}}</td>
                        <td>{{$v['user_live_score']}}</td>
                        <td>{{$v['user_review_score']}}</td>
                        <td>{{$v['user_score']}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop