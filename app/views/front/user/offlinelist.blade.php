@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/user">个人中心</a>
    >
    线下会议
</div>
<div class="p_center_box fund_page">
    @include('front.common.ucleft',['nowpos'=>'aspirinoffline'])
	<!-- <div class="left">
            <ul>
                <li><a href="/user"><span class="personal_icon1">个人信息</span></a></li>
                <li><a href="/user/my-score"><span class="personal_icon2">我的积分</span></a></li>
                @if(Auth::User()->role_id==3)
                <li><a href="/aspirin/my-auth"><span class="personal_icon3">我的认证</span></a></li>
                @endif
                <li><a href="/aspirinonline/my-enter-list"><span class="personal_icon4">我的会议</span></a></li>
                <li><a href="/aspirinoffline/index" class="sele"><span class="personal_icon5_5">线下会议</span></a></li>
                <li><a href=""><span class="personal_icon6">视频下载</span></a></li>
                <li><a href="/aspirinresearch/my-list"><span class="personal_icon7">科研培训</span></a></li>
                <li><a href="/aspirin/contact"><span class="personal_icon8">联系客服</span></a></li>
                @if(Auth::User()->role_id==2)
                <li><a href="/user/my-train"><span class="personal_icon9">我的培训</span></a></li>
                @endif
                @if(Auth::User()->role_id==2)
                <li><a href="/user/my-order"><span class="icon04">我的预约</span></a></li>
                @endif
                <li><a href="/user/caselist/1"><span class="icon06">我的病例</span></a></li>
                @if(Auth::User()->role_id==2)
                <li><a href="/user/mydoc"><span class="icon09">医生管理</span></a></li>
                @endif
            </ul>
        </div> -->
    <div class="right fund_intro">
        <div class="offline_meeting">
            @foreach($oOffline as $value) 
           	 <a class="meeting_block" href="/aspirinoffline/detail/{{$value->id}}">
		            <div class="item-list">
		                <div class="item-flex">
		                    <div class="item-theme">会议主题：{{$value->offline_title}}</div>
		                    <div class="item-time">会议时间：{{date('Y-m-d',strtotime($value->offline_start_time))}}</div>
		                    <div class="item-price">{{$value->offline_least_gold}}</div>
		                    @if($value->hasapply == 1)
		                    	<div class="item-state" >已报名</div>
		                    @else
		                    	 <div class="item-state" style="visibility: hidden">已报名</div>
		                    @endif
		                </div>
		            </div>
		           </a>
		      @endforeach    
        </div>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop