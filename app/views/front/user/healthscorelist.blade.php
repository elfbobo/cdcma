@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<script src="/assets/js/user.js"></script>
<style type="text/css">
<!--
.useScoreBtns { display:inline-block;padding:0px 15px;line-height:35px;font-size:14px;color:#FFF;background:#7BB547;margin:8px 0; }
-->
</style>
<div class="p_sml_menu new_menu">您当前位置：<a href="/">首页</a> > <a href="/user">个人中心</a> > <a href="/user/my-health-score">我的健康云积分</a> > 积分排行</div>
<div class="p_center_box fund_page">
    @include('front.common.ucleft',['nowpos'=>'myhealthscore'])
    <div class="right fund_intro Personal_page">
        <div class="kinds_tab clearfix">
            <div onclick="window.location.href='/user/my-health-score';" style="cursor:pointer;">我的积分</div>
            <div class="sele" onclick="window.location.href='/user/health-score-list/{{Auth::User()->role_id}}';" style="cursor:pointer;">积分排行</div>
            <div class="bottom_line"></div>
        </div>
        <div class="sml_menu_box">
            <ul class="rank">
                <li><a @if($iRoleId==3) class="sele" @endif href="/user/health-score-list/3">医生</a></li>
                <li class="border1"></li>
                <li><a @if($iRoleId==2) class="sele" @endif href="/user/health-score-list/2">医学顾问</a></li>
            </ul>
            <div class="clear"></div>
        </div>
        <div class="content">
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <td class="special2" style="width: 100px">排名</td>
                        <td style="width: 150px">用户姓名</td>
                        <td style="width: 150px" >参会积分</td>
                        <td style="width: 150px" >录播积分</td>
                        <td style="width: 150px" >总积分</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($aUser as $k=>$v)
                    <tr>
                        <td @if($k==0) class="special2 T_1st" @elseif($k==1) class="special2 T_2nd" @elseif($k==2) class="special2 T_3rd" @else class="special2" @endif>{{$k+1}}</td>
                        <td>{{$v['user_name']}}</td>
                        <td>{{$v['health_live_integral']}}</td>
                        <td>{{$v['health_video_integral']}}</td>
                        <td>{{$v['health_integrals']}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="clear"></div>
</div>
@include('front.common.contact')
@stop