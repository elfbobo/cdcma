@extends('front.common.layout')
@section('title')忘记密码-国卫健康云 @stop
@section('description')忘记密码-国卫健康云@stop
@section('keywords')忘记密码-国卫健康云 @stop
@section('content2')
<script>
function forget(){
	var email = $('#email').val();
	if(!email){
		alert('请输入您的邮箱');
		return;
	}
	var url = '/forget';
	$.post(url,{'email':email},function(msg){
		if(msg=="not_exist"){
			alert('该邮箱不存在！');
			return;
		}else if(msg=='success'){
			$.blockUI({ message: $('#email_success_box')});
			return;
		}else{
			alert('操作失败，请刷新当前页面重试');
		}
	});
	
}
</script>
<div class="p_bar_bg bg_color2">
	<div class="wid1025">
    	<div class="p_tit">
        	<a class="white" href="#">忘记密码</a>
            >
            输入邮箱
        </div>
    </div>
</div>
<div class="p_center_box">
    <div class=" wid698 wid933"  style="min-height:400px;">
        <div class="">
            <div class="center_block">
                <div class="center_tit white_bg">请输入邮箱</div>
                <div class="center_infor3 center p0">
                    <p><input id="email" class="infor_txt" type="text" style="width:325px;" /></p>
                </div>
                <div class="center ptop45">
            		<input class="p_btn mright130" type="button" value="确　定" onclick="forget();"/>
                    <input class="p_btn p_btn_bg" type="button" value="取　消" onclick="window.location.href='/'"/>
       	 		</div>
            </div>
        </div>
        
    </div>
</div>

<div id="email_success_box" class="pop_logoin_box" style="display:block;display:none;">
	<div class="pop_cont2 pop_ptop60 center">
    	找回密码邮件已经发送到您的邮箱,点击邮件里的链接修改您的密码。
    </div>
    <div class="center">
    	<input type="button" class="p_btn" value="确定" onclick="$.unblockUI();"/>
    </div>
</div>
@stop
