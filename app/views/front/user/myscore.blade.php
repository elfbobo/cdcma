@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<script src="/assets/js/user.js"></script>
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/user">个人中心</a>
    >
    我的积分
</div>
<div class="p_center_box fund_page">
    @include('front.common.ucleft',['nowpos'=>'myscore'])
    <div class="right fund_intro Personal_page">
        <div class="kinds_tab clearfix">
            <div class="sele" onclick="window.location.href='/user/my-score';" style="cursor:pointer;">我的积分</div>
            <div onclick="window.location.href='/user/score-list/{{Auth::User()->role_id}}';" style="cursor:pointer;">积分排行</div>
            <div class="bottom_line"></div>
        </div>
        <div class="content">
            <div class="my_points">
                <dl>
                    <dt><img src="/assets/images/front/web/rank.png" alt=""></dt>
                    <dd>
                        我的积分：<span>{{$oUser->user_score}}</span>
                    </dd>
                </dl>
                <div style="text-align:left;margin-left:65px;">
                    <p>累计观看直播时间：{{intval($iLiveMin/60)}}小时{{$iLiveMin%60}}分钟</p>
                    <p>累计观看录播时间：{{intval($iReviewMin/60)}}小时{{$iReviewMin%60}}分钟</p>
                </div>
            </div>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <td class="special" style="width:450px">标题</td>
                        <td style="width:90px">观看形式</td>
                        <td style="width:180px">观看时长（分钟）</td>
                    </tr>
                </thead>
                <tbody>
                @foreach($oVideoLog as $k=>$v)
                    @if($v->video_title&&$v->watch_minutes)
                    <tr>
                        <td class="special">{{$v->video_title}}</td>
                        <td>@if($v->video_type==1)录播@else直播@endif</td>
                        <td>{{$v->watch_minutes}}</td>
                    </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
            <div class="clear"></div>   
            <div class="p_number_box">{{@$oVideoLog->links()}}</div>
        </div>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop