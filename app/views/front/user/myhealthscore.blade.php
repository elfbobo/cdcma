@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<script src="/assets/js/user.js"></script>
<style type="text/css">
<!--
.useScoreBtns { display:inline-block;padding:0px 15px;line-height:35px;font-size:14px;color:#FFF;background:#7BB547;margin:8px 0; }
-->
</style>
<div class="p_sml_menu new_menu">您当前位置：<a href="/">首页</a> > <a href="/user">个人中心</a> > 我的健康云积分</div>
<div class="p_center_box fund_page">
    @include('front.common.ucleft',['nowpos'=>'myhealthscore'])
    <div class="right fund_intro Personal_page">
        <div class="kinds_tab clearfix">
            <div class="sele" onclick="window.location.href='/user/my-health-score';" style="cursor:pointer;">我的积分</div>
            <div onclick="window.location.href='/user/health-score-list/{{Auth::User()->role_id}}';" style="cursor:pointer;">积分排行</div>
            <div class="bottom_line"></div>
        </div>
        <div class="content">
            <div class="my_points"><dl style="border:none;">
                <dt><img src="/assets/images/front/web/rank.png" alt=""></dt>
                <dd style="line-height:30px;padding:15px 0;font-size:14px;">
                    我的积分：<span>{{$totalIntegral}}</span><br />
                    可兑换积分：<b>{{$integral}}</b><br />
                    <a href="/user/use-score" class="useScoreBtns">积分兑换</a>
                </dd>
            </dl></div>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <td class="special" style="width:450px;padding-left:10px;">标题</td>
                        <td style="width:90px">形式</td>
                        <td style="width:180px">积分</td>
                    </tr>
                </thead>
                <tbody>
                @foreach($oList as $k=>$v)
                <tr>
                    <td class="special" style="padding-left:10px;">{{$v->obj_title}}</td>
                    <td><?php
                    switch ($v->obj_type) {
                        case 'class':
                            echo '参会获取';
                            break;
                        case 'buycme':
                            echo '兑换CME课程';
                            break;
                        default:
                            echo '观看视频';
                            break;
                    }
                    ?></td>
                    <td>{{$v->number}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
            <div class="clear"></div>   
            <div class="p_number_box">{{@$oList->links()}}</div>
        </div>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop