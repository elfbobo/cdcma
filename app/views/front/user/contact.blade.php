@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<div class="p_sml_menu new_menu">
      您当前位置：
    <a href="/">首页</a>
    >
    <a href="/user">个人中心</a>
    >
   联系客服
</div>
<div class="p_center_box fund_page">
    @include('front.common.ucleft',['nowpos'=>'contact'])
	<!-- <div class="left">
            <ul>
                <li><a href="/user"><span class="personal_icon1">个人信息</span></a></li>
                <li><a href="/user/my-score"><span class="personal_icon2">我的积分</span></a></li>
                @if(Auth::User()->role_id==3)
                <li><a href="/aspirin/my-auth"><span class="personal_icon3">我的认证</span></a></li>
                @endif
                <li><a href="/aspirinonline/my-enter-list"><span class="personal_icon4">我的会议</span></a></li>
                <li><a href="/aspirinoffline/index"><span class="personal_icon5">线下会议</span></a></li>
                <li><a href=""><span class="personal_icon6">视频下载</span></a></li>
                <li><a href="/aspirinresearch/my-list"><span class="personal_icon7">科研培训</span></a></li>
                <li><a href="/aspirin/contact" class="sele"><span class="personal_icon8_8">联系客服</span></a></li>
                @if(Auth::User()->role_id==2)
                <li><a href="/user/my-train"><span class="personal_icon9">我的培训</span></a></li>
                @endif
                @if(Auth::User()->role_id==2)
                <li><a href="/user/my-order"><span class="icon04">我的预约</span></a></li>
                @endif
                <li><a href="/user/caselist/1"><span class="icon06">我的病例</span></a></li>
                @if(Auth::User()->role_id==2)
                <li><a href="/user/mydoc"><span class="icon09">医生管理</span></a></li>
                @endif
            </ul>
        </div> -->
    <div class="right fund_intro science">
    	<div class="top_banner"><img src="/assets/images/front/web/fund_intro_bg.png" alt=""></div>
        <div class="contact_box">
            <div class="intro_txt">
                <p>整合阿司匹林专项基金、心血管风险筛查项目、世界心脏日主题传播相关视频及资料，提炼并合理分类内容。在平台现有渠道进行零时差同步广泛覆盖。</p>
                <p>每一个行动都有获益，越多参与越多获益。积分实时到账。</p>
            </div>
            <div class="phone_box">
                <p>您有任何疑问，可以直接拨打电话：</p>
                <p class="phone"><?php echo HOTLINE;?></p>
            </div>
        </div>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop