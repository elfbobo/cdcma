@extends('front.common.layout')
@section('title')忘记密码-国卫健康云 @stop
@section('description')忘记密码-国卫健康云@stop
@section('keywords')忘记密码-国卫健康云 @stop
@section('content2')
<div class="p_bar_bg bg_color2">
	<div class="wid1025">
    	<div class="p_tit">
        	<a class="white" href="#">密码修改</a>
            >
            设置新密码
        </div>
    </div>
</div>
<script>
function block_text(text){
	$('#pop_apply_success_box_html').html(text);
    $.blockUI({ message: $('#pop_apply_success_box')});
}
function submit_reset(email,code){
	var new_pwd1 = $('#new_pwd1').val();
	var new_pwd2 = $('#new_pwd2').val();
	if(!new_pwd1){
		block_text('请输入新密码');
		return;
	}
	if(new_pwd1!=new_pwd2){
		block_text('两次输入的密码不一致');
		return;
	}
	var url = '/user/resetpwd';
	var data = {'email':email,'new_pwd':new_pwd1,'code':code};
	$.post(url,data,function(msg){
		if(msg=='success'){
			block_text('密码修改成功，现在就去登录吧');
			return;
		}else{
			block_text('请求失败');
		}
	});
}
</script>
<div class="p_center_box">
    <div class=" wid698 wid933"  style="min-height:400px;">
        <div class="">
            <div class="center_block">
                <div class="center_tit white_bg">密码修改</div>
                <div class="center_infor3 center p0">
                    <p><label>新密码：</label><input class="infor_txt" type="password" id="new_pwd1"/></p>
                    <p><label>确认密码：</label><input class="infor_txt" type="password" id="new_pwd2" style="width:212px;" /></p>
                </div>
                <div class="center ptop45">
            		<input class="p_btn" type="button" value="确　定" onclick="submit_reset('{{Input::get('user')}}','{{Input::get('code')}}')"/>
       	 		</div>
            </div>
        </div>
        
    </div>
</div>
			<div class="pop_logoin_box center" style="top:0;display:none;" id="pop_apply_success_box">
                    <div class="pop_cont2 pop_ptop60">
                        <p id="pop_apply_success_box_html"></p>
                    </div>
                    <div class="center">
                        <input class="save" type="button" value="确定" onclick="$.unblockUI()"/>
                    </div>
                </div>
<div id="email_success_box" class="pop_logoin_box" style="display:block;display:none;">
	<div class="pop_cont2 pop_ptop60 center">
    	密码修改成功，现在就去登录吧
    </div>
    <div class="center">
    	<input type="button" class="p_btn" value="确定" onclick="$.unblockUI();"/>
    </div>
</div>
@stop
