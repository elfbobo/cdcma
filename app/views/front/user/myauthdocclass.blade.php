@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<style type="text/css">
<!--
.expert_list_box .list { border-bottom:1px dotted #CCC;padding-bottom:20px;position:relative; }
.expert_list_box .list p { position:absolute;top:20px;right:20px;color:orange;font-size:16px;font-weight:bold; }
.expert_list_box .list .list-pic { width:170px !important;height:100px !important;padding:0 10px 0 10px; }
.expert_list_box .list .list-pic img { width:170px !important;height:100px !important; }
-->
</style>
<script src="/assets/js/user.js"></script>
<div class="p_sml_menu new_menu">您当前位置：<a href="/">首页</a> > <a href="/user">个人中心</a> > <a href="/user/myauthdoclist">我的讲者</a> > 课程列表</div>
<div class="p_center_box fund_page">
    @include('front.common.ucleft',['nowpos'=>'mydoclist'])
    <div class="right fund_intro Personal_page">
        <div class="kinds_tab clearfix">
            <div class="sele">{{$docInfo->user_name}}发布的课程</div>
            <div>&nbsp;</div>
            <div class="bottom_line"></div>
        </div>
        <div class="content" style="padding:30px 20px;">
            <div class="expert_list_box" style="padding:0px;">
                @foreach($oEducation as $k=>$v)
                <?php
                $photo = $v['ppt_thumb'] ? $domains.$v['ppt_thumb'] : "/assets/images/dafault/default.jpg";
                ?>
                <div class="list" style="cursor:pointer;" onclick="window.open('/user/myauthdocclassdetail?id={{$v->id}}', '', '');">
                    <div class="list-pic"><img src="{{$photo}}"></div>
                    <div class="list-item">
                        <div class="list-item-title" style="font-size:16px;padding:5px 0 40px 0;">{{$v->ppt_title}}</div>
                        <div class="list-item-intro" style="line-height:180%;">{{@$v->created_at}}</div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="clear"></div>   
            <div class="p_number_box" style="padding-top:10px;">{{$oEducation->appends($aLink)->links()}}</div>
        </div>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop