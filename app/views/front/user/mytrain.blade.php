@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<script src="/assets/js/user.js"></script>
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/user">个人中心</a>
    >
    我的培训
</div>
<div class="p_center_box fund_page">
    @include('front.common.ucleft',['nowpos'=>'mytrain'])
	<!-- <div class="left">
            <ul>
                <li><a href="/user"><span class="personal_icon1">个人信息</span></a></li>
                <li><a href="/user/my-score"><span class="personal_icon2">我的积分</span></a></li>
                @if(Auth::User()->role_id==3)
                <li><a href="/aspirin/my-auth"><span class="personal_icon3">我的认证</span></a></li>
                @endif
                <li><a href="/aspirinonline/my-enter-list"><span class="personal_icon4">我的会议</span></a></li>
                <li><a href="/aspirinoffline"><span class="personal_icon5">线下会议</span></a></li>
                <li><a href=""><span class="personal_icon6">视频下载</span></a></li>
                <li><a href="/aspirinresearch/my-list"><span class="personal_icon7">科研培训</span></a></li>
                <li><a href="/aspirin/contact"><span class="personal_icon8">联系客服</span></a></li>
                @if(Auth::User()->role_id==2)
                <li><a href="/user/my-train" class="sele"><span class="personal_icon9_9">我的培训</span></a></li>
                @endif
                @if(Auth::User()->role_id==2)
                <li><a href="/user/my-order"><span class="icon04">我的预约</span></a></li>
                @endif
                <li><a href="/user/caselist/1"><span class="icon06">我的病例</span></a></li>
                @if(Auth::User()->role_id==2)
                <li><a href="/user/mydoc"><span class="icon09">医生管理</span></a></li>
                @endif
            </ul>
        </div> -->
    <div class="right fund_intro online_meeting">
        <!-- <div class="top_banner"><img src="/assets/images/front/web/fund_intro_bg.png" alt=""></div> -->
        <div class="online_meeting_box training_page">
        	@if($oTrain)
			@foreach($oTrain as $k=>$v)
            <div class="item-list">
                <div class="item-flex">
                    <div class="item-time">培训主题：{{$v->train_title}}</div>
                    <div class="clearfix">
                    @if(($v->start_time<date('Y-m-d H:i:s',(time()+300)))&&($v->end_time>date('Y-m-d H:i:s')))
                    <div class="item-count" onclick="window.location.href='/user/enter-train/{{$v->id}}'" >参加培训</div>
	                @elseif($v->end_time<date('Y-m-d H:i:s'))
	                
	                @if($v->train_type==2)
	                <div class="item-count" onclick="window.location.href='/user/train-review/{{$v->id}}'" >观看录播</div>
	                @else
	                <div class="item-count ban" onclick="alert('培训已过期')">参加培训</div>
	                @endif
	                
	                @else
	                <div class="item-count" onclick="alert('未到会议时间')">参加培训</div>
	                @endif
                    </div>
                </div>
                <div class="item-time" style="padding-top: 15px">培训时间：{{substr($v->start_time,0,16)}}—{{substr($v->end_time,11,5)}}</div>
            </div>
            @endforeach
        	@endif
        </div>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop