@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<div class="p_sml_menu new_menu">
    您当前位置：
    <a href="/">首页</a>
    >
    <a href="/user">个人中心</a>
    >
    线下会议
</div>
<div class="p_center_box fund_page">
	<div class="left">
    	<ul>
            <li><a href="/user"><span class="personal_icon1">个人信息</span></a></li>
            <li><a href="/user/my-score"><span class="personal_icon2">我的积分</span></a></li>
            @if(Auth::User()->role_id==3)
            <li><a href="/aspirin/my-auth"><span class="personal_icon3">我的认证</span></a></li>
            @endif
            <li><a href="/aspirinonline/my-enter-list"><span class="personal_icon4">我的会议</span></a></li>
            <li><a href="/aspirinoffline/index" class="sele"><span class="personal_icon5_5">线下会议</span></a></li>
<!--             <li><a href=""><span class="personal_icon6">视频下载</span></a></li> -->
            <li><a href="/aspirinresearch/my-list"><span class="personal_icon7">科研培训</span></a></li>
            <li><a href="/aspirin/contact"><span class="personal_icon8">联系客服</span></a></li>
            @if(Auth::User()->role_id==2)
            <li><a href="/user/my-train"><span class="personal_icon9">我的培训</span></a></li>
            @endif
            <!--  
            @if(Auth::User()->role_id==2)
            <li><a href="/user/my-order"><span class="icon04">我的预约</span></a></li>
            @endif
            <li><a href="/user/caselist/1"><span class="icon06">我的病例</span></a></li>
            @if(Auth::User()->role_id==2)
            <li><a href="/user/mydoc"><span class="icon09">医生管理</span></a></li>
            @endif
            -->
        </ul>
    </div>
    <div class="right personal">
        <div class="">
            <div class="item-list">
                <div class="item-flex">
                    <div class="item-title">会议时间：</div>
                    <div class="item-cont">{{date("Y.m.d H:i",strtotime($oOffline->offline_start_time))}}-{{date("H:i",strtotime($oOffline->offline_end_time))}}</div>
                </div>
            </div>
            <div class="item-list">
                <div class="item-flex">
                    <div class="item-title">会议主题：</div>
                    <div class="item-cont">{{$oOffline->offline_title}}</div>
                </div>
            </div>
            <div class="item-list">
                <div class="item-flex">
                    <div class="item-title">会议简介：</div>
                    <div class="item-cont item-cont2">{{$oOffline->offline_content}}</div>
                </div>
            </div>
            <div class="item-list">
                <div class="item-flex">
                    <div class="item-title">需要消耗：</div>
                    <div class="item-cont"><div class="item-price">{{$oOffline->offline_least_gold}}</div></div>
                </div>
            </div>
            <div class="btn_box">
           		 @if($result =='limitcount')  
                 	<button class="btn" onclick="alert('已达报名人数上限');">申请参加</button>
                 @elseif($result =='hasjoin')  
                 	<button class="btn" onclick="alert('您已报名该会议');">申请参加</button>
                 @elseif($result =='nostart')  
                 	<button class="btn" onclick="alert('该会议报名未开始');">申请参加</button>
                 @elseif($result =='hasend')  
                 	<button class="btn" onclick="alert('该会议报名已结束');">申请参加</button>
                 @elseif($result =='noscore')  
                 	<button class="btn" onclick="alert('您的积分不足');">申请参加</button>
                 @else
           		  	<button class="btn" onclick="$('#cover').show();$('#pop_box').show();" id="result">申请参加</button>
                 @endif
            </div>
        </div>
    </div>
	<div class="clear"></div>
</div>
<div class="cover" id="cover" style="display:none ;"></div>
<div class="modal_window style_one" id="pop_box" style="display:none ;">
    <div class="cont_txt" style="text-align: left">
        <p>即将消耗{{$oOffline->offline_least_gold}}积分，报名后不能取消，是否参加会议？</p>
    </div>
    <div class="btn_box">
        <button class="btn" onclick="$('#pop_box').hide();$('#cover').hide();">取消</button>
        <button class="btn" onclick="joinmeeting({{$oOffline->id}})">确定参加</button>
    </div>
</div>
<script type="text/javascript">
	function joinmeeting(id){
		var url = '/aspirinoffline/apply-check';
		var data = {'id':id};
		$.post(url,data,function(msg){
			if(msg == 'success'){
				$('#result').attr('disabled',true); 
				alert('报名成功');
				window.location.href = '/aspirinoffline/index';
			}else{
				alert('操作失败');
			}
		})
	}
</script>
@include('front.common.contact')
@stop