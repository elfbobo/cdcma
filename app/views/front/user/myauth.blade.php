@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<div class="p_sml_menu new_menu">
      您当前位置：
    <a href="/">首页</a>
    >
    <a href="/user">个人中心</a>
    >
   我的认证
</div>
<div class="p_center_box fund_page">
    @include('front.common.ucleft',['nowpos'=>'myauth'])
	<!-- <div class="left">
            <ul>
                <li><a href="/user"><span class="personal_icon1">个人信息</span></a></li>
                <li><a href="/user/my-score"><span class="personal_icon2">我的积分</span></a></li>
                @if(Auth::User()->role_id==3)
                <li><a href="/aspirin/my-auth" class="sele"><span class="personal_icon3_3">我的认证</span></a></li>
                @endif
                <li><a href="/aspirinonline/my-enter-list"><span class="personal_icon4">我的会议</span></a></li>
                <li><a href="/aspirinoffline/index"><span class="personal_icon5">线下会议</span></a></li>
                <li><a href=""><span class="personal_icon6">视频下载</span></a></li>
                <li><a href="/aspirinresearch/my-list"><span class="personal_icon7">科研培训</span></a></li>
                <li><a href="/aspirin/contact"><span class="personal_icon8">联系客服</span></a></li>
                @if(Auth::User()->role_id==2)
                <li><a href="/user/my-train"><span class="personal_icon9">我的培训</span></a></li>
                @endif
                @if(Auth::User()->role_id==2)
                <li><a href="/user/my-order"><span class="icon04">我的预约</span></a></li>
                @endif
                <li><a href="/user/caselist/1"><span class="icon06">我的病例</span></a></li>
                @if(Auth::User()->role_id==2)
                <li><a href="/user/mydoc"><span class="icon09">医生管理</span></a></li>
                @endif
            </ul>
        </div> -->
    <div class="right fund_intro Personal_page">
        <div class="certification">
            <div class="input_box">
                <label for="">姓名：</label>
                <input type="text" class="input_text" value="{{$oUser->user_name}}" disabled>
            </div>
            <div class="input_box">
                <label for="">医院：</label>
                <input type="text" class="input_text" value="{{sub_str($sHospitalName,12)}}" disabled>
            </div>
            <div class="input_box">
                <label for="">科室：</label>
                <input type="text" class="input_text" value="{{$oUser->user_department}}" disabled>
            </div>
            <?php 
            $aPos = User::getPosition();
            ?>  
            <div class="input_box">
                <label for="">职称：</label>
                <select name="" id="user_position" class="input_select">
                	@foreach($aPos as $k=>$v)
						<option value="{{$v}}" @if($oUser->user_position==$v) selected @endif>{{$v}}</option>
					@endforeach
                </select>
            </div>
            <div class="input_box">
                <label for="">医师证号：</label>
                <input type="text" class="input_text" placeholder="请输入您的医师执业证号" id="card_number" value="{{$oUser->card_number}}">
            </div>
            <div class="input_box last">
                <label for="">上传证件照片：</label>
                <div class="file_box">
                    <label for="file-pic"><img id="thumb" @if($oUser->card_thumb) src="{{$oUser->card_thumb}}" @else src="/assets/images/front/web/add.png" @endif style="width:96px;height:96px;"></label>
                    <input id="file-pic" type="file" class="input_file" name="file-pic" onchange="saveCardThumb();">
                </div>
				<input type="hidden" id="card_thumb" value="{{$oUser->card_thumb}}"/>
            </div>
            <div class="input_box" style="width:410px;">
				<span style="color:red;">注：请上传您的医师执业证书照片</span>
			</div>
            <div class="btn_box" @if($oUser->card_auth_flag == 2) style="display:none;" @endif>
                <input type="button" class="btn" value="立即认证" onclick="submitAuth();"/>
            </div>
        </div>
    </div>
	<div class="clear"></div>
</div>
<div class="cover" style="display:none;" id="cover"></div>
<div class="modal_window style_one" style="display:none;" id="submit_certification_box">
    <p class="font_bold">您已提交成功</p>
    <p>我们将在3个工作日内进行审核</p>
    <p>请您耐心等待审核</p>
    <div class="btn_box">
        <button class="btn" onclick="$('#cover').hide();$('#submit_certification_box').hide();">我知道了</button>
    </div>
</div>
<script>
	function saveCardThumb()
	{
		$.ajaxFileUpload({
			url: '/aspirin/upload-user-card',
			secureuri: false,
			fileElementId: "file-pic",
			dataType: "json",
			success: function(data, status) {
				$('#thumb').attr('src', data.card_thumb);
				$('#card_thumb').val(data.card_thumb);
			}
		})
	}
	function submitAuth()
	{
        alert('请前往国卫健康云APP进行认证。');
        return false;
		var user_position = $('#user_position').val();
		var card_number = $('#card_number').val();
		var card_thumb = $('#card_thumb').val();
		if(!user_position){
			alert('请选择职称');
			return false;
		}
		if(!card_number){
			alert('请填写医师证号');
			return false;
		}
		if(!card_thumb){
			alert('请上传证件照片');
			return false;
		}
		var url = '/aspirin/submit-auth';
		var data = {'user_position':user_position,'card_number':card_number,'card_thumb':card_thumb};
		$.post(url,data,function(msg){
			if(msg.success){
				$('#cover').show();
            	$('#submit_certification_box').show();
			}else{
				alert('请重新提交');
				return;
			}
		  },'json')
	}
</script>
@include('front.common.contact')
@stop