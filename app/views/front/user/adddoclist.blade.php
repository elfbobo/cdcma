@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<style type="text/css">
<!--
.expert_list_box .list { border-bottom:1px dotted #CCC;padding-bottom:20px;position:relative; }
.expert_list_box .list p { position:absolute;top:20px;right:20px;color:orange;font-size:16px;font-weight:bold; }
.expert_list_box .list .list-pic { width:110px !important;height:100px !important;padding-left:10px; }
.expert_list_box .list .list-pic img { width:100px !important;height:100px !important; }
.search_box{
    position: absolute;
    z-index: 0;
    right: 15px;
    top: 15px;
}
.search_box .txt_input{
    vertical-align: middle;
    border: #a3a5ab solid 1px;
    padding: 5px;
    width: 275px;
}
.search_box .btn{
    background: #d00089;
    width: 100px;
    height: 28px;
    border: 1px solid #e268b8;
    color: #fff;
    font-size: 14px;
    padding-top: 3px\9;
    vertical-align: middle;
}
.list .relation{
    display: block;
    margin-bottom: 10px;
}
.list .btn{
    display: block;
    text-align: center;
    line-height: 30px;
    width: 95px;
    height: 30px;
    background: #0090d3;
    color: #fff;
}
-->
</style>
<script src="/assets/js/user.js"></script>
<div class="p_sml_menu new_menu">您当前位置：<a href="/">首页</a> > <a href="/user">个人中心</a> > <a href="/user/mydoclist">我的医生</a> > 添加医生</div>
<div class="p_center_box fund_page">
    @include('front.common.ucleft',['nowpos'=>'mydoclist'])
    <div class="right fund_intro Personal_page">
        <div class="kinds_tab clearfix">
            <div class="sele">添加医生</div>
            <div class="bottom_line"></div>
        </div>
        <div class="search_box">
            <form action="/user/add-doclist" method="get">
                <input type="text" name="keyword" class="txt_input" autocomplete="off" placeholder="姓名/手机号/医院" value="{{$keyword}}">
                <input type="submit" class="btn" id="advanced_search" value="我要检索" style="cursor: pointer">
            </form>
        </div>
        <div class="content" style="padding:30px 20px;">
            <div class="expert_list_box" style="padding:0px;">
                @if(!count($docList))
                <div class="empty_box">
                    啊哦~ 未找到任何结果！
                </div>
                @endif
                @foreach($docList as $k=>$v)
                <?php
                if($v['user_company']){
                    $sUserCompany = $v['hospital_name'];
                }else{
                    $sUserCompany = $v['user_company_name']?$v['user_company_name']:"--";
                }
                $photo = $v['user_thumb']?"{$domains}".$v['user_thumb']:"{$domains}/assets/images/dafault/default.jpg";
                ?>
                <div class="list">
                    <p style="text-align: right">
                        @if($v['link_rep_id'] == $userid)
                            <span class="relation">已关联我</span>
                        @else
                            @if(!empty($v['deputy_name']))
                            <span class="relation">已关联代表{{$v['deputy_name']}}</span>
                            @else
                            <span class="relation" style="color: #ababab">未关联代表</span>
                            @endif
                            <button class="btn" data-click="join-us" data-id="{{$v->id}}" style="float: right">关联我</button>
                        @endif
                    </p>
                    <div class="list-pic"><img src="{{$photo}}"></div>
                    <div class="list-item">
                        <div class="list-item-title" style="font-size:16px;padding:5px 0 10px 0;">{{$v->user_name}}</div>
                        <div class="list-item-intro" style="line-height:180%;">职称：{{@$v->user_department?$v->user_department:"--"}}<br />医院：{{$sUserCompany}}</div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="clear"></div>   
            <div class="p_number_box" style="padding-top:10px;">{{@$docList->links()}}</div>
        </div>
    </div>
	<div class="clear"></div>
</div>
<script type="text/javascript">
    $(function(){
        $('[data-click="join-us"]').click(function(){
            var id = $(this).data('id');
            $.post('/user/do-add-doclist', {id:id}, function(data){
                if (data.status == 0) {
                    alert(data.msg);
                } else {
                    location.reload();
                }
            });
        });
    });
</script>
@include('front.common.contact')
@stop