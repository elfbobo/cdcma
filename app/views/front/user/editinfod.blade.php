@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<script src="/assets/js/ajaxfileupload.js"></script>
<script src="/assets/js/user.js"></script>
<script>
function change_uinfo(){

	//用户基本信息修改
	var user_tel = $.trim($('#user_tel').val());
	var user_email = $.trim($('#user_email').val());
	var user_address = $.trim($('#user_address').val());
	var user_position = $.trim($('#user_position').val());
	//省份、城市、区县、医院
	var user_province = $("#hospital21  option:selected").val();
	var user_city = $("#hospital22  option:selected").val();
	var user_regin = $("#regin  option:selected").val();
	var user_area = $("#area  option:selected").val();
	if(!user_tel){
		alert('请输入您的手机号码！');
		return false;
	}
	if(!user_email){
		alert('请输入您的邮箱！');
		return false;
	}
	var reg_mobile = /^[0-9]{11}$/;
	istel= reg_mobile.test(user_tel);
	if (!istel ) {
	    alert("手机号码格式不正确！");
	    return false;
	  }
	  var reg = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
	  ismail= reg.test(user_email);
	  if (!ismail ) {
	    alert("邮箱格式不正确！");
	    return false;
	  }
	
	if(!user_province||user_province==0){
		alert('请选择您的省份！');
		return false;
	}
	if(!user_city||user_city==0){
		alert('请选择您的城市！');
		return false;
	}

	if(!user_regin||user_regin==0){
		alert('请选择您的大区！');
		return false;
	}
	if(!user_area||user_area==0){
		alert('请选择您的地区！');
		return false;
	}

	//用户密码修改
//	var password_old = $.trim($('#password_old').val());
//	var password_new = $.trim($('#password_new').val());
//	var password_new_re = $.trim($('#password_new_re').val());
//	if(password_old||password_new||password_new_re){
//		//修改密码
//		if(!password_old){
//			alert('请输入旧密码');return;
//		}else{
//			if(!password_new){
//				alert('请输入新密码');return;
//			}
//			if(password_new!=password_new_re){
//				alert('两次修改的密码不一致，请重新输入');
//				return;
//			}
//		}
//		var data = {'user_address':user_address,
//				'user_position':user_position,
//				'user_province':user_province,
//				'user_city':user_city,
//				'user_regin':user_regin,
//				'user_area':user_area,
//				'password_old':password_old,
//				'password_new':password_new
//				};
//	}else{
		//不修改密码
		var data = {'user_address':user_address,
				'user_tel':user_tel,
				'user_email':user_email,
				'user_position':user_position,
				'user_province':user_province,
				'user_city':user_city,
				'user_regin':user_regin,
				'user_area':user_area
				};

//	}

	var url = '/user/change-user-info-all';
	$.post(url,data,function(msg){
		if(msg=='nick_repeat'){
			alert('该用户名已经存在，换换其他用户名试试');
		}else if(msg=='tel_repeat'){
			alert('该手机号码用户已经存在了');
		}else if(msg=='email_repeat'){
			alert('该邮箱用户已经存在了');
		}else if(msg=='old_err'){
			alert('原密码输入错误');
		}else if(msg=='success'){
			alert('用户信息修改成功');
			window.location.href='/user';
		}else{
			alert('网络错误，请刷新当前页面重试');
		}
	})	
}
</script>
<div class="p_sml_menu new_menu">
      您当前位置：
    <a href="/">首页</a>
    >
    <a href="/user">个人中心</a>
    >
   修改资料
</div>
<div class="p_center_box fund_page">
    <div class=" wid698 wid933"  style="min-height:400px;">
        <div class="">
            <div class="center_block">
            	<div class="center_tit">个人资料</div>
                <div class="">
                	<div class="upload_box">
                    	<div >
                    		<img id="user_thumb" src="{{$oUser->user_thumb}}" />
                        	<span class="chg_txt">更改头像</span>
                        	<input class="upload_btn" id="upload_file" name="upload_file"  type="file" onchange="saveThumb()" />                        
                        </div>
                        <div class="name">姓名：{{$oUser->user_name}}</div>
                    </div>
                    <div class="center_infor wid260">
                        <p><label>性别：</label>
                        	@if($oUser->user_sex==1) 男 @else 女 @endif
                        </p>
                       <p>
                        	<label>手机：</label>
                        	<input type="text" id="user_tel" name="user_tel" value="{{$oUser->user_tel}}"/>
                        </p>
                        <p>
                        	<label>邮箱：</label>
                        	<input type="text" id="user_email" name="user_email" value="{{$oUser->user_email}}"/>
                        </p>
                        <p><label>地址：</label>
                        	<input type="text" id="user_address" name="user_address" value="{{$oUser->user_address}}"/>
                        </p>
                         <p>
                        	<label>职称：</label>
                        	<input type="text" id="user_position" name="user_position"  value="{{$oUser->user_position}}"/>  
                        </p>
                        
                    </div>
                    <div class="center_infor wid260">
	                        <p>
                        	<label><span class="red">*</span>省份：</label>
	                        	<select id="hospital21" class="sele_txt" onchange="change_hospital2(2)">
						            <option value="0">=请选择=</option>
						            @foreach($aHosp1 as $k=>$v)
						            <option value="{{$k}}" @if($oUser->user_province==$k) selected @endif>{{$v}}</option>
						            @endforeach
						        </select>
	                        </p>
	                        <p>
		                        	<label><span class="red">*</span>城市：</label>
		                        	<select id="hospital22" class="sele_txt" onchange="change_hospital2(3)">
						            <option value="0">=请选择=</option>
						            @foreach($aHosp2 as $k=>$v)
						            <option value="{{$k}}" @if($oUser->user_city==$k) selected @endif>{{$v}}</option>
						            @endforeach
						            </select>
		                    </p>
                        <input type="hidden" id="user_province" name="user_province" value="0"/>
                        <input type="hidden" id="user_city" name="user_city" value="0"/> 
	                      <?php 
							$sRegin = User::getReginCache();
							$aRegin = json_decode($sRegin);
							?>
							<script>
							function change_regin(){
				            	var regin = $("#regin  option:selected").val();
				            	var url = '/user/area-info/'+regin;
				            	$.post(url,{},function(json){
				            		data = json;
				                	if(data=='noinfo'){
				        				var html = '<option value="0">=请选择=</option>';
				        				$("#area").html(html);
				        				
				        			}else{
				        				var bShow = false;
				        				var html = '<option value="0">=请选择=</option>';
				        				for(var elem in data){
				        					bShow = true;
				        					html += '<option value="' + elem + '">' + data[elem] + '</option>';
				        				}
				        				if(bShow){
				        					$("#area").html(html);
				        				}
				        			}
				            	},'json');
				            }
							</script>
	                     <p>
	                        	<label><span class="red">*</span>大区：</label>
	                        	<select id="regin" class="sele_txt" onchange="change_regin()">
					            <option value="0">=请选择=</option>
					            @foreach($aRegin as $k=>$v)
					            <option value="{{$k}}" @if($oUser->user_regin==$k) selected @endif>{{$v}}</option>
					            @endforeach
					            </select>
					            <input type="hidden" name="user_regin" id="user_regin" value=""/>  
	                        </p>
	                        <p>
	                        	<label><span class="red">*</span>地区：</label>
	                        	<select id="area" class="sele_txt" >
					            <option value="0">=请选择=</option>
					             @foreach($oArea as $k=>$v)
					            <option value="{{$k}}" @if($oUser->user_area==$k) selected @endif>{{$v}}</option>
					            @endforeach
					            </select>
	                        	<input type="hidden" name="user_area" id="user_area" value=""/>  
	                        </p> 
	                        <p><label>用户编码：</label>{{strtoupper($oUser->invite_code)}}</p>  
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="center_block" style="display:none;">
            	<div class="center_tit">密码修改</div>
                <div class="center_infor3">
                    <p><label>用户名：</label>{{$oUser->user_nick}}<input class="infor_txt" type="hidden" name="user_name" id="user_name" value="{{$oUser->user_nick}}" /></p>
                    <p><label>旧密码：</label><input class="infor_txt" type="password" name="password_old" id="password_old"  /></p>
                    <p><label>新密码：</label><input class="infor_txt" type="password" name="password_new" id="password_new" /></p>
                    <p><label>确认新密码：</label><input class="infor_txt wid198" type="password" name="password_new_re" id="password_new_re" /></p>
                </div>
            </div>
        </div>
        <div class="center">
        	<input class="save" type="button" value="确     定" onclick="change_uinfo()"/>
        </div>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop