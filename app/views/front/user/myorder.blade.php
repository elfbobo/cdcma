@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<script src="/assets/js/user.js"></script>
<div class="p_bar_bg  bg_color2">
	<div class="wid1025">
    	<div class="p_tit pleft230">
        	<a class="white" href="/user">个人中心</a>
            >
            我的预约
        </div>
    </div>
</div>
<div class="p_center_box">
	<div class="left">
    	<ul>
            <li><a href="/user"><span class="icon02">个人信息</span></a></li>
            <li><a href="/user/my-score"><span class="icon03">我的积分</span></a></li>
            <li><a class="sele" href="/user/my-order"><span class="icon04">我的预约</span></a></li>
            <li><a href="/user/score-list/{{Auth::User()->role_id}}"><span class="icon05">积分排行</span></a></li>
        	@if(Auth::User()->role_id==2)
            <li><a href="/user/my-train"><span class="icon06">我的培训</span></a></li>
            @endif
            <!--  
            <li><a href="/user/caselist/1"><span class="icon06">我的病例</span></a></li>
            @if(Auth::User()->role_id==2)
            <li><a href="/user/mydoc"><span class="icon09">医生管理</span></a></li>
            @endif
            -->
        </ul>
    </div>
    <div class="right wid698" style="min-height:400px;">
    	<div class="rank_table_box">
        	<table class="app_table" border="0" cellpadding="0" cellspacing="0">
            	<tr>
                	<th width="80">专　家</th>
                    <th width="240">科　室</th>
                    <th width="100">预约时间</th>
                </tr>
                @foreach($oUserOrder as $k=>$v)
                <tr>
                    <td>{{$v->doc_name}}</td>
                    <td>{{$v->doc_department}}</td>
                    <td>{{$v->order_time}}</td>
                </tr>
                @endforeach
            </table>
        </div>
        <div class="txt_right">
        	<input class="save" type="button" value="马上预约" onclick="window.location.href='/user/order-now'" />
        </div>
    </div>
	<div class="clear"></div>
</div>


@stop
