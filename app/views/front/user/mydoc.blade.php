@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<style>
.background_gray{ background-color: #ababab;}
</style>
<script src="/assets/js/user.js"></script>
<div class="p_bar_bg  bg_color2">
	<div class="wid1025"><div class="p_tit pleft230"><a class="white" href="/user">个人中心</a> > 我的积分</div></div>
</div>
<div class="p_center_box">
	<div class="left">
    	<ul>
            <li><a href="/user"><span class="icon02">个人信息</span></a></li>
            <li><a href="/user/my-score"><span class="icon03">我的积分</span></a></li>
            @if(Auth::User()->role_id==2)
            <li><a href="/user/my-order"><span class="icon04">我的预约</span></a></li>
            @endif
            <li><a href="/user/score-list/{{Auth::User()->role_id}}"><span class="icon05">积分排行</span></a></li>
        	@if(Auth::User()->role_id==2)
            <li><a href="/user/my-train"><span class="icon06">我的培训</span></a></li>
            @endif
            <!--  
            <li><a href="/user/caselist/1"><span class="icon06">我的病例</span></a></li>
            @if(Auth::User()->role_id==2)
            <li><a class="sele" href="/user/mydoc"><span class="icon09">医生管理</span></a></li>
            @endif
            -->
        </ul>
    </div>
    <div class="right wid698">
		@foreach($oDoc as $k=>$v)
			 @if(!empty($aUserSurvey[$v->id])&&$aUserSurvey[$v->id])
			 	<!-- 已经选择用户类型了 -->
			 	<div class="management_box  @if($k%2==0) left @else right @endif" style="height:99px;">
		        	<div class="management_info">
		        		@if($aUserSurvey[$v->id]==1)
			    		<div class="management_info_block" style="margin-top:25px;">
		                	拜<br />新<br />同
		                </div>
			    		@elseif($aUserSurvey[$v->id]==2)
			    		<div class="management_info_block" style="margin-top:25px;">
		                	拜<br />阿
		                </div>
			    		@elseif($aUserSurvey[$v->id]==3)
			    		<div class="management_info_block">
		                	拜<br />新<br />同
		                </div>
		                <div class="management_info_block">
		                	拜<br />阿
		                </div>
			    		@endif
		            </div>
		        	<div class="management_left" style="width:160px;padding-top:15px;">
		            	<div class="management_pad">
		                	姓名 : {{$v->user_name}}
		                </div>
		                @if($aUserSurvey[$v->id]==2||Config::get('config.phase_id')==1)
		                <div class="management_pad" style="width:276px;">
		                	医院 : {{str_cut_cms($v->user_company,32)}}
		                </div>
		                @else
		                <div class="management_pad">
		                	医院 : {{str_cut_cms($v->user_company,22)}}
		                </div>
		                @endif
		            </div>
		            <div class="management_right">
		            @if(!empty($aUserAdalateType[$v->id]))
		            	@if($aUserAdalateType[$v->id]==1)
		            	<div class="management_pad2">
		                	<input class="management_btn2" style="cursor:default;" type="button" value="拜新同偏好者" />
		                </div>
		                @else
		                <div class="management_pad2">
		                	<input class="management_btn2" style="cursor:default;" type="button" value="拜新同中立者" />
		                </div>
		                @endif
		            @else
		            	@if(($aUserSurvey[$v->id]==1||$aUserSurvey[$v->id]==3)&&Config::get('config.phase_id')!=1)
		            	<div class="management_pad2">
		                	<input class="management_btn" onclick="pushSurvey({{$v->id}})" type="button" value="推送调研" />
		                </div>
		                @endif
		            @endif
		            </div>
		            <div class="clear"></div>
		        </div>
			 @else
			    <!-- 还未选择用户类型 -->
				<div class="management_box @if($k%2==0) left @else right @endif" style="height:99px;">
		        	<div class="management_left">
		            	<div class="management_pad">
		                	姓名 : {{$v->user_name}}
		                </div>
		                <div class="management_pad">
		                	医院 : {{str_cut_cms($v->user_company,20)}}
		                </div>
		            </div>
		            <div class="management_right">
		            	<div class="management_pad">
		                	<input class="management_btn " id="user_type_{{$v->id}}_2" onclick="check_user_type(2,{{$v->id}},this)" type="button" value="拜阿用户" />
		                </div>
		                <div class="management_pad">
		                	<input class="management_btn " id="user_type_{{$v->id}}_1" onclick="check_user_type(1,{{$v->id}},this)" type="button" value="拜新同用户" />
		                </div>
		            </div>
		            <div class="clear"></div>
		        </div>
		    @endif
		@endforeach
        <div class="clear"></div>
        <div class="center">
        	<input class="management_btn" type="button" value="提交" onclick="checkDocType()" />
        </div>
        
    </div>
	<div class="clear"></div>
</div>

@stop
