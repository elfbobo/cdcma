@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<div class="p_sml_menu new_menu">
      您当前位置：
    <a href="/">首页</a>
    >
    <a href="/user">个人中心</a>
    >
   科研培训
</div>
<div class="p_center_box fund_page">
    @include('front.common.ucleft',['nowpos'=>'aspirinresearchlist'])
	<!-- <div class="left">
            <ul>
                <li><a href="/user"><span class="personal_icon1">个人信息</span></a></li>
                <li><a href="/user/my-score"><span class="personal_icon2">我的积分</span></a></li>
                @if(Auth::User()->role_id==3)
                <li><a href="/aspirin/my-auth"><span class="personal_icon3">我的认证</span></a></li>
                @endif
                <li><a href="/aspirinonline/my-enter-list"><span class="personal_icon4">我的会议</span></a></li>
                <li><a href="/aspirinoffline/index"><span class="personal_icon5">线下会议</span></a></li>
                <li><a href=""><span class="personal_icon6">视频下载</span></a></li>
                <li><a href="/aspirinresearch/my-list" class="sele"><span class="personal_icon7_7">科研培训</span></a></li>
                <li><a href="/aspirin/contact"><span class="personal_icon8">联系客服</span></a></li>
                @if(Auth::User()->role_id==2)
                <li><a href="/user/my-train"><span class="personal_icon9">我的培训</span></a></li>
                @endif
                @if(Auth::User()->role_id==2)
                <li><a href="/user/my-order"><span class="icon04">我的预约</span></a></li>
                @endif
                <li><a href="/user/caselist/1"><span class="icon06">我的病例</span></a></li>
                @if(Auth::User()->role_id==2)
                <li><a href="/user/mydoc"><span class="icon09">医生管理</span></a></li>
                @endif
            </ul>
        </div> -->
    <div class="right fund_intro science">
<!--     	<div class="top_banner"><img src="/assets/images/front/web/fund_intro_bg.png" alt=""></div> -->
        <div class="list-box">
        	@foreach($oApply as $k=>$v)
        	<a href="/aspirinresearch/my-show/{{$v->id}}">
                <div class="list-item">
                    <span class="list-item-time">{{substr($v->created_at,0,10)}}</span>
                    <span class="list-item-name">{{$v->apply_name}}</span>
                    @if($v->apply_type == 2)
                    <span class="list-item-state"><span style="color:red;border:1px solid red;">未通过审核</span></span>
                    @elseif($v->apply_type == 1)
                    <span class="list-item-state"><span class="success">审核通过</span></span>
                    @else
                    <span class="list-item-state"><span>审核中</span></span>
                    @endif
                </div>
            </a>
            @endforeach
        </div>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop