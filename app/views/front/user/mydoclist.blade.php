@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<style type="text/css">
<!--
.expert_list_box .list { border-bottom:1px dotted #CCC;padding-bottom:20px;position:relative; }
.expert_list_box .list p { position:absolute;top:20px;right:20px;color:orange;font-size:16px;font-weight:bold; }
.expert_list_box .list .list-pic { width:110px !important;height:100px !important;padding-left:10px; }
.expert_list_box .list .list-pic img { width:100px !important;height:100px !important; }
.list .btn{
    display: block;
    text-align: center;
    line-height: 30px;
    width: 95px;
    height: 30px;
    background: #0090d3;
    color: #fff;
}
-->
</style>
<script src="/assets/js/user.js"></script>
<div class="p_sml_menu new_menu">您当前位置：<a href="/">首页</a> > <a href="/user">个人中心</a> > 我的医生</div>
<div class="p_center_box fund_page">
    @include('front.common.ucleft',['nowpos'=>'mydoclist'])
    <div class="right fund_intro Personal_page">
        <div class="add_doctor">
            <a href="/user/add-doclist">添加医生</a>
        </div>
        <div class="kinds_tab clearfix">
            <div class="sele" onclick="window.location.href='/user/mydoclist';" style="cursor:pointer;">我的医生</div>
            <div onclick="window.location.href='/user/myauthdoclist';" style="cursor:pointer;">我的讲者</div>
            <div class="bottom_line"></div>
        </div>
        <div class="content" style="padding:30px 20px;">
            <div class="expert_list_box" style="padding:0px;">
                @if(!count($docList))
                <div class="empty_box">
                    啊哦~ 未找到任何结果！
                </div>
                @endif
                @foreach($docList as $k=>$v)
                <?php
                if((int)$v['user_company']){
                    $sUserCompany = Hospital::where('id',(int)$v['user_company'])->pluck('name');
                }else{
                    $sUserCompany = $v['user_company']?$v['user_company']:"--";
                }
                $photo = $v['user_thumb']?"{$domains}".$v['user_thumb']:"{$domains}/assets/images/dafault/default.jpg";
                ?>
                <div class="list" style="cursor:pointer;">
                    <p>
                        <button class="btn" data-click="remove-us" data-id="{{$v->id}}" style="float: right">移除</button>
                    </p>
                    <div class="list-pic"><img src="{{$photo}}"></div>
                    <div class="list-item">
                        <div class="list-item-title" style="font-size:16px;padding:5px 0 10px 0;">{{$v->user_name}}</div>
                        <div class="list-item-intro" style="line-height:180%;">职称：{{@$v->user_department?$v->user_department:"--"}}<br />医院：{{$sUserCompany}}</div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="clear"></div>   
            <div class="p_number_box" style="padding-top:10px;">{{@$docList->links()}}</div>
        </div>
    </div>
	<div class="clear"></div>
</div>
<script type="text/javascript">
    $(function(){
        $('[data-click="remove-us"]').click(function(){
            var id = $(this).data('id');
            $.post('/user/do-remove-doctor', {id:id}, function(data){
                if (data.status == 0) {
                    alert(data.msg);
                } else {
                    location.reload();
                }
            });
        });
    });
</script>
@include('front.common.contact')
@stop