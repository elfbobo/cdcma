@extends('front.common.layout')
@section('title')个人中心-国卫健康云 @stop
@section('description')个人中心-国卫健康云@stop
@section('keywords')个人中心-国卫健康云 @stop
@section('content2')
<link rel="stylesheet" type="text/css" href="/assets/css/common.css"/>
<style type="text/css">
<!--
.expert_list_box .list { border-bottom:1px dotted #CCC;padding-bottom:20px;position:relative; }
.expert_list_box .list p { position:absolute;top:20px;right:20px;color:orange;font-size:16px;font-weight:bold; }
.expert_list_box .list .list-pic { width:170px !important;height:100px !important;padding:0 10px 0 10px; }
.expert_list_box .list .list-pic img { width:170px !important;height:100px !important; }
.videoBoxs { width:600px;float:left; }
.videoBoxs .bold { font-size:20px;color:#0090d3; }
.videoBoxs .detailed_time { font-size:12px;padding:10px 0px;color:#666; }
.videoBoxs .contentBoxs { font-size:14px;color:#777;padding-top:15px; }
.docInfoBoxs { width:220px;float:right;background:#FFF;padding:20px 10px; }
.docInfoBoxs .photoBoxs { width:100%;text-align:center;padding:10px 0 20px 0; }
.docInfoBoxs .photoBoxs img { width:130px; }
.docInfoBoxs .docTxtBoxs { width:200px;padding:0px 10px;font-size:14px;line-height:180%;color:#777; }
.docInfoBoxs .docTxtBoxs b { width:48px;float:left; }
.docInfoBoxs .docTxtBoxs p { width:152px;float:left; }
-->
</style>
<script src="/assets/js/user.js"></script>
<div class="p_sml_menu new_menu" style="margin-bottom:15px;">您当前位置：<a href="/">首页</a> > <a href="/user">个人中心</a> > <a href="/user/myauthdoclist">我的讲者</a> > <a href="/user/myauthdocclass?doc_id={{$oEducation->user_id}}">课程列表</a> > 课程详情</div>
<div class="p_center_box fund_page">
    @include('front.common.ucleft',['nowpos'=>'mydoclist'])
    <div class="right fund_intro Personal_page" style="width:855px;background:none;">
        <!--中间部分-->
        <ul class="videoBoxs">
            <div style="background:#FFF;width:100%;float:left;padding:20px 15px;width:570px;">
                <div class="bold">{{$oEducation->ppt_title}}</div>
                <div class="detailed_time">时间：{{substr($oEducation->created_at,0,10)}}</div>
                <video width="570" height="330" controls>
                    <source src="{{$oEducation->video_url}}" type="video/mp4">
                    <object data="{{$oEducation->video_url}}" width="570" height="330">
                        <embed src="{{$oEducation->video_url}}" width="570" height="330">
                    </object> 
                </video>
            </div>
            <div style="background:#FFF;width:100%;float:left;padding:15px 15px 20px 15px;width:570px;margin-top:20px;">
                <div class="bold">简介</div>
                <div class="contentBoxs">{{@$oEducation->education_content?$oEducation->education_content:$oEducation->ppt_title}}</div>
            </div>
        </ul>
        <!--右边部分-->
        <ul class="docInfoBoxs">
            <div class="photoBoxs"><img src="{{@$doc_info->user_thumb}}"></div>
            <div class="docTxtBoxs"><b>姓名：</b><p>{{@$doc_info->user_name}}</p></div>
            <div class="docTxtBoxs"><b>职称：</b><p>{{@$doc_info->user_position?$doc_info->user_position:"--"}}</p></div>
            <div class="docTxtBoxs"><b>医院：</b><p>{{@$doc_info->user_company_name?$doc_info->user_company_name:"--"}}</p></div>
            <div class="docTxtBoxs"><b>科室：</b><p>{{@$doc_info->user_department?$doc_info->user_department:"--"}}</p></div>
            @if($doc_info->addressstr) <div class="docTxtBoxs"><b>省市：</b><p>{{$doc_info->addressstr}}</p></div> @endif
            </div>
        </ul>
    </div>
	<div class="clear"></div>
</div>
@include('front.common.contact')
@stop