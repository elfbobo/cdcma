@extends('front.common.layout')
@section('title')病例征集-国卫健康云 @stop
@section('description')病例征集-国卫健康云@stop
@section('keywords')病例征集-国卫健康云 @stop
@section('content1')
<div class="p_bar_bg bg_color2">
	<div class="wid1025">
    	<div class="p_tit pleft230">
        	<a class="white" href="">个人中心</a>
            >
            <a class="white" href="">我的病例</a>
        </div>
    </div>
</div>
<div class="p_center_box">
	<div class="left">
    	<ul>
        <li><a href="/user"><span class="icon02">个人信息</span></a></li>
        <li><a href="/user/my-score"><span class="icon03">我的积分</span></a></li>
        @if(Auth::User()->role_id==2)
        <li><a href="/user/my-order"><span class="icon04">我的预约</span></a></li>
        @endif
        <li><a href="/user/score-list/{{Auth::User()->role_id}}"><span class="icon05">积分排行</span></a></li>
        @if(Auth::User()->role_id==2)
        <li><a href="/user/my-train"><span class="icon06">我的培训</span></a></li>
        @endif
        <!-- 
        <li><a href="/user/caselist/1" class="sele"><span class="icon06">我的病例</span></a></li>
        @if(Auth::User()->role_id==2)
        <li><a href="/user/mydoc"><span class="icon09">医生管理</span></a></li>
        @endif
        -->
      </ul>
    </div>
    <div class="right wid698">
    	<div class="center mar_b20">
        	<img src="/assets/images/front/web/logo.jpg" />
        </div>
        <div class="center_block">
        	<div class="rank_table_box mycase_box">
				<table class="record_table" border="0" cellpadding="0" cellspacing="0">
					<tbody>
                          <tr>
                              <th width="55">序号</th>
                              <th width="290">标题</th>
                              <th width="85">编辑</th>
                              <th width="85">状态</th>
                          </tr>
                          @foreach($oCase as $k=>$v)
                          <tr>
                              <td class="bold">{{$k+1}}</td>
                              <td class="pl10">{{$v->case_name}}</td>
                              @if($v->status ==0 )
                              <td><a style="color:#0090d3;" href="/case/edit/{{$v->id}}">修改</a>/
                              @if($v->uploadType ==1)
                              <a style="color:#0090d3;"  href="/case/edit/{{$v->id}}">查看</a></td>
                              @else
                              <a style="color:#0090d3;"  href="/case/show/{{$v->id}}">查看</a></td>
                              @endif
                              @else
                               <td>修改/查看</td>
                              @endif
                              @if($v->status == '-1')
                              <td title="{{$v->nopass_reason}}">{{$statusArr[$v->status]}}</td>
                              @else
                              <td>{{$statusArr[$v->status]}}</td>
                              @endif
                          </tr>
                          @endforeach
					</tbody>
				</table>
            </div>
        </div>
    </div>
	<div class="clear"></div>
</div>

@stop