@extends('front.common.layout')
@section('title')空中课堂-国卫健康云 @stop
@section('description')空中课堂-国卫健康云@stop
@section('keywords')空中课堂-国卫健康云 @stop
@section('content1')
<script src="/assets/js/video.js"></script>
<?php 
if($oScoreLog){
	$iBlue1 = $oScoreLog->content_degree;
	$iGray1 = 5-$iBlue1;
	$iBlue2 = $oScoreLog->class_level;
	$iGray2 = 5-$iBlue2;
	$iBlue3 = $oScoreLog->overall_score;
	$iGray3 = 5-$iBlue3;
}
?>
	<div class="p_sml_menu">
    	您当前位置：
    	<a href="/">首页</a>
        >
        <a href="/docface">空中课堂</a>
        >
        <a href="/docface/review">精彩回顾</a>
        >详细
    </div>
    <div class="clearfix">
    <div class="p_left_box left">
    	<div class="detailed_box">
        	<div class="bold">{{$oReview->video_title}}</div>
            <div class="detailed_time">时间：{{substr($oReview->start_time,0,10)}}</div>
			@if($oReview->channel_type == 2)
			<input type="hidden" id="recordtime">
        	@endif
            <div class="detailed_video" id="a1">
			@if($oReview->channel_type == 2)
				<img src="{{$oReview->video_thumb}}" width="100%">
        	@endif
            <!-- <br><br><br><br><br>
    			<span style="text-align:center;color:#717071;text-align:center;">浏览器版本过低，请您升级浏览器后观看</span>
            <br><br><br><br><br> -->
            </div>
            
            <div class="detailed_good">
	            <a @if($oReview->video_download_url) onclick="download_video('{{$oReview->video_download_url}}',1,{{$oReview->id}})" @endif class="download" style="cursor:pointer;"></a>
                <a class="explain" onmouseover="$('#pop_explain').show()" style="cursor:pointer;"></a>
            	<a href="javascript:void(0)" class="good" onclick="docface_video_zan({{$oReview->id}},{{$iUserId}},1)"></a>
                <a href="javascript:void(0)" class="good_txt" id="docface_video_zan">{{$oReview->video_support}}</a>
                <div class="pop_logoin_box center" id="pop_explain" style="display:none;">
                    <div class="pop_cont2" style="text-align:left;">
                        	下载密码：{{$oReview->video_download_psw}}
                    </div>
                    <div class="center">
                        <input class="save" type="button" value="确定" onclick="$('#pop_explain').hide();"/>
                    </div>
                </div>         
            </div>
        </div>
    </div>
    <div class="p_right_box">
    	<div class="txt_bg01">
    		<div class="txt_bg01_top"></div>
    		<div class="txt_bg01_cont">
	        	<div class="tit">本期专家</div>
	        	<div class="left">
	            	<a href="/docface/doc-show/{{$oReview->doc_info->id}}"><img src="{{$oReview->doc_info->doc_thumb}}" /></a>
	            </div>
	            <div class="right">
	            	<p><label>专家: </label>{{$oReview->doc_info->doc_name}}</p>
	                <p><label>职称: </label>{{$oReview->doc_info->doc_position}}</p>
	                <p><label>科室: </label>{{$oReview->doc_info->doc_department}}</p>
	                <p><label>医院: </label>{{$oReview->doc_info->doc_hospital}}</p>
	            </div>
	            <div class="clear"></div>
	            <div class="ptop10 max-height">
	            {{str_cut_cms($oReview->doc_info->doc_introduction,100)}}
	            </div>
	            <div class="intro_more">
	            	<a class="more" href="/docface/doc-show/{{$oReview->doc_info->id}}">more</a>
	            </div>
            </div>
            <div class="txt_bg01_btm"></div>
        </div>
<!--         <div class="txt_bg01"> -->
<!--         	<div class="txt_bg01_top"></div> -->
<!--             <div class="txt_bg01_cont"> -->
<!-- 	        	<div class="tit">本期简介</div> -->
<!-- 	            <div class="ptop10"> -->
<!-- 	            	{{$oReview->video_introduce}} -->
<!-- 	            </div> -->
<!-- 	        </div> -->
<!--             <div class="txt_bg01_btm"></div> -->
<!--         </div> -->
    </div>
    </div>
    <input type="hidden" id="score1" value="0"/>
	<input type="hidden" id="score2" value="0"/>
	<input type="hidden" id="score3" value="0"/>
	<input type="hidden" id="videoid" value="{{$oReview->id}}"/>
	<input type="hidden" id="userid" value="{{$iUserId}}"/>
    <div class="evaluation">
    	<!-- 判断是否已经提交过星级评分，每人限评一次，评过之后不再可点（3个分开判断） -->
		@if($oScoreLog && $oScoreLog->content_degree != 0)
			<div class="list01">
	            <span>内容丰富程度：</span>
	            @for($i=1;$i<=$iBlue1;$i++)
	            <span><img src="/assets/images/front/web/icon_star1.png" class="star"></span>  
	            @endfor
	            @for($i=1;$i<=$iGray1;$i++)
	            <span><img src="/assets/images/front/web/icon_star2.png" class="star"></span>  
	            @endfor
	        </div>
        @else
	        <div class="list">
	            <span>内容丰富程度：</span>
	            @for($i=1;$i<=5;$i++)
	            <span><img src="/assets/images/front/web/icon_star2.png" class="star"></span>  
	            @endfor
	        </div>
        @endif
        @if($oScoreLog && $oScoreLog->class_level != 0)
			<div class="list02">
	            <span>专家讲课水平：</span>
	            @for($i=1;$i<=$iBlue2;$i++)
	            <span><img src="/assets/images/front/web/icon_star1.png" class="star"></span>  
	            @endfor
	            @for($i=1;$i<=$iGray2;$i++)
	            <span><img src="/assets/images/front/web/icon_star2.png" class="star"></span>  
	            @endfor
	        </div>
        @else
	        <div class="list2">
	            <span>专家讲课水平：</span>
	            @for($i=1;$i<=5;$i++)
	            <span><img src="/assets/images/front/web/icon_star2.png" class="star"></span>  
	            @endfor
	        </div>
        @endif
        @if($oScoreLog && $oScoreLog->overall_score != 0)
			<div class="list03">
	            <span>整体综合评分：</span>
	            @for($i=1;$i<=$iBlue3;$i++)
	            <span><img src="/assets/images/front/web/icon_star1.png" class="star"></span>  
	            @endfor
	            @for($i=1;$i<=$iGray3;$i++)
	            <span><img src="/assets/images/front/web/icon_star2.png" class="star"></span>  
	            @endfor
	        </div>
        @else
	        <div class="list3">
	            <span>整体综合评分：</span>
	            @for($i=1;$i<=5;$i++)
	            <span><img src="/assets/images/front/web/icon_star2.png" class="star"></span>  
	            @endfor   
	        </div>
        @endif
    </div>
    <div class="comment">
        <div class="p_bar_bg w_auto bg_color2">
            <div class="p_tit">医友评论</div>
        </div> 
        <div class="">
        	<div class="txt_bg02_top"></div>
        	<div class="txt_bg02_cont">
        	@foreach($oComments as $oComment)
        	<div class="comment_line">
            	<div class="left wid80">
                	<a href="#"><img class="pic" src="{{$oComment->user_thumb}}" /></a>
                </div>
                <div class="right wid405">
                	<div class="name_line"><a href="#" class="name">{{$oComment->user_nick}}</a></div>
                    <div class="">
                    	<div class="left wid280">{{$oComment->comment}}</div>
                        <div class="right wid125">
                        	<a class="hand" href="javascript:void(0)" onclick="comment_zan({{$oComment->id}},1)">（<span id="comment_zan_{{$oComment->id}}">{{$oComment->zan_count}}
                        	</span>）</a>
                            <a class="reply" href="javascript:void(0)"  onclick="video_reply({{$oComment->user_id}},'{{$oComment->user_nick}}')">回复</a>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            @endforeach
            
            <div class="people_num">已有<span>{{count($oComments)}}</span>位医友发表了看法</div>
            <div class="comment_cont">
            	<div id='reply' value="" style='color:#666;position: relative;left:4px;top:1px;height:22px;display:none;'>
				</div>
				<div name="content" id="content" contentEditable="true" style="margin:0 0 auto 0;outline:none;background-color: #FFFFFF; color: #888888;font: 14px/24px '微软雅黑';border: 1px solid #e0e0e0;height: 96px;overflow-y: auto;padding: 5px;resize: none;width: 482px;">
				</div>
            </div>
            <div class="comment_btn_box" style="position: relative;">
            	<a class="face" href="javascript:void(0);" id="showpic">插入表情</a>
            	<div  class="" style="background:#fff;position:absolute;left:8px;top:50px;display:none;height:89px;border:1px solid #D1D1D1;margin-left:14px;padding:5px 10px 5px 10px;" id="pic_list">{{$sImgLi}}
	                 <a href='javascript:void(0);' onclick='hidePic(0)' class='icon close1'></a>
	            </div>
                <input id="replay_box" class="comment_btn" type="button" value="发表评论" onclick="reply({{$oReview->id}},1);"/>
                <div class="clear"></div>
            </div>
            </div>
            <div class="txt_bg02_btm"></div>
        </div>
    </div>    
    <div class="clear"></div>
<?php 
	$sVideoUrl = '';
   	if (!empty($oReview->video_url)){
   		if (user_agent_is_mobile()){
        	$sVideoUrl = Config::get("config.cdn_url").$oReview->video_url;
   		}else{
   			$sVideoUrl = Config::get("config.cdn_url").str_replace(".mp4",".flv",$oReview->video_url);
   		}
   	}
   	$sVideoId = '';
   	if (!empty($oReview->video_id)){
   		$sVideoId = Config::get("config.cdn_url").$oReview->video_id;
   	}
?>
<script type="text/javascript">
	$('.evaluation .list01 span').click(function(){
		$('.evaluation .list01 span').unbind("click");
	});
	$('.evaluation .list02 span').click(function(){
    	$('.evaluation .list02 span').unbind("click");
    });
	$('.evaluation .list03 span').click(function(){
    	$('.evaluation .list03 span').unbind("click");
    });
</script>
<script type="text/javascript">
    $('.evaluation .list span').click(function(){
        console.log($(this).index())
        $(this).index()
        $(this).parent().find('.star').each(function(){
            $(this).attr('src','/assets/images/front/web/icon_star2.png')
        })
        for(var i=0 ; i<$(this).index();i++){
            $(this).parent().find('.star').eq(i).attr('src','/assets/images/front/web/icon_star1.png')
        }
        $('#score1').val($(this).index());
        docface_video_score(1);
        $('.evaluation .list span').unbind("click");
    });
    $('.evaluation .list2 span').click(function(){
        console.log($(this).index())
        $(this).index()
        $(this).parent().find('.star').each(function(){
            $(this).attr('src','/assets/images/front/web/icon_star2.png')
        })
        for(var i=0 ; i<$(this).index();i++){
            $(this).parent().find('.star').eq(i).attr('src','/assets/images/front/web/icon_star1.png')
        }
        $('#score2').val($(this).index());
        docface_video_score(1);
        $('.evaluation .list2 span').unbind("click");
    });
    $('.evaluation .list3 span').click(function(){
        console.log($(this).index())
        $(this).index()
        $(this).parent().find('.star').each(function(){
            $(this).attr('src','/assets/images/front/web/icon_star2.png')
        })
        for(var i=0 ; i<$(this).index();i++){
            $(this).parent().find('.star').eq(i).attr('src','/assets/images/front/web/icon_star1.png')
        }
        $('#score3').val($(this).index());
        docface_video_score(1);
        $('.evaluation .list3 span').unbind("click");
    });
</script>
<script type="text/javascript">
function docface_video_score(device){
	var videoid = $('#videoid').val();
	var userid = $('#userid').val();
	var score1 = $('#score1').val();
	var score2 = $('#score2').val();
	var score3 = $('#score3').val();
	var url = '/docface/video_score';
	var data = {videoid:videoid,userid:userid,score1:score1,score2:score2,score3:score3,device:device};
	$.post(url,data,function(msg){
		if(msg == 'success'){
// 			alert('评分成功！');
			return;
		}else{
			alert('您已经点评过了！');
			return;
		}
	})
}
</script>
 <?php if($oReview->channel_type == 1 || $oReview->channel_type == 3){ ?>
<script type="text/javascript" src="/assets/js/player/ckplayer/ckplayer.js" charset="utf-8"></script>
<script type="text/javascript">
	var __flg; 
	var flashvars={
		f:'{{$sVideoUrl}}',
		c:0,
		p:2,
	    b:0,
	    loaded:'loadedHandler',
		};
	var video=['{{$sVideoUrl}}','{{$sVideoUrl}}','{{$sVideoUrl}}'];
	CKobject.embed('/assets/js/player/ckplayer/ckplayer.swf','a1','ckplayer_a1','548','329',false,flashvars,video);

	function loadedHandler(){
	    if(CKobject.getObjectById('ckplayer_a1').getType()){
	    	//添加监听handler
	        CKobject.getObjectById('ckplayer_a1').addListener('play',playHandler);
	        CKobject.getObjectById('ckplayer_a1').addListener('pause',pauseHandler);
	    }
	    else{
	        //添加监听handler
	        CKobject.getObjectById('ckplayer_a1').addListener('play','playHandler');
	        CKobject.getObjectById('ckplayer_a1').addListener('pause','pauseHandler');
	    }
	}
	
	function playHandler(){
		//用户积分
		clearInterval(__flg);
		__flg = self.setInterval("addUserMinute({{$oReview->id}},{{$iUserId}})",180000);
	}

	function addUserMinute(videoid,userid){
		var time_second = Math.round(CKobject.getObjectById('ckplayer_a1').getStatus().time);
		var time_minute = Math.round(time_second/60);
		console.log(time_second);
		var url = "/docface/video_view/"+videoid+"/"+userid+"/"+time_minute;
		$.post(url,{},function(msg){
			if(msg=='success'){
				console.log('success');
			}
		})
	}
	function ckplayer_status(str){
		//播放结束
		if(str == 'ended'){
			clearInterval(__flg);
			addUserMinute({{$oReview->id}},{{$iUserId}});
		}
	}
</script>
<?php }elseif($oReview->channel_type == 2){ ?>
<!-- 目睹	 -->
<script>window.FETCHER_SERVER_URL = "ws://fetcher.mudu.tv:8088";</script>
<script src="//static.mudu.tv/fetcher/bundle.6d7aca164d2389e8bea6.js"></script>
<script src="//static.mudu.tv/static/websdk/sdk.js"></script>
<script>
playHandler();
var __flgtime;
Mudu.Init( {{$sVideoId}}, function(){
    //初始化播放器
    var player = new Mudu.Player({
        // 播放器容器ID，播放器会被添加到该DOM元素中
        containerId: 'a1',
        // 播放器播放类型：支持`live`和`vod`两个值，live为直播播放，vod为点播播放
        type: 'live',
        // 播放器视频播放地址
        src: "{{$sVideoUrl}}",
        image: 'http://cdcma.bizconf.cn{{$oReview->video_thumb}}',
        // 播放器是否自动播放
        autoplay: false,
        // 播放器是否显示控制条
        controls: true,
        // 播放器是否循环播放, 默认为false
        repeat: false,
        // 播放器宽度，单位为像素，默认为480
        width: 548,
        // 播放器高度，单位为像素，默认为270
        height: 329
    });
    //开始播放 player.play();
    var state = player.getState()
    //暂停播放 player.pause();
    // 返回直播间名字，类型为string
    var roomName = Mudu.Room.GetName();
    // 返回直播状态，类型为number: `1`为正在直播，`0`为不在直播
    var roomLiveStatus = Mudu.Room.GetLiveStatus();
    // 返回直播间浏览量，类型为number整数
    var roomViewNum = Mudu.Room.GetViewNum();
    // 返回直播间视频地址，类型为string
    var roomPlayAddr = Mudu.Room.GetPlayAddr();
    Mudu.MsgBus.On('Player.Play', function (player) {
	    //console.log('Mudu Player 播放开始')
	    clearInterval(__flgtime);
	    __flgtime = self.setInterval(function(){
            		var time_old = $("#recordtime").val();
            		if(!time_old){
            			time_old = 0;
            		}
					var time_now = 1+parseInt(time_old);
					$("#recordtime").val(time_now);
            	},1000);
	})
	Mudu.MsgBus.On("Player.Stoped",function (player) {
	    //console.log('Mudu Player 停止播放')
	    clearInterval(__flgtime);
	})
	Mudu.MsgBus.On("Player.Waiting",function (player) {
	    //console.log('Mudu Player 缓冲中')
	    clearInterval(__flgtime);
	})
});
function playHandler(){
	__flg = self.setInterval("addUserMinute({{$oReview->id}},{{$iUserId}})",120000);
}

function addUserMinute(videoid,userid){
	var time_second = $("#recordtime").val();
	var time_minute = Math.round(time_second/60);
	console.log(time_minute);
	if(time_minute >=3){
		var url = "/docface/video_view/"+videoid+"/"+userid+"/"+time_minute;
		$.post(url,{},function(msg){
			if(msg=='success'){
				console.log('success');
			}
		})
	}	
}

</script>
<?php } ?>
<script>
	document.onclick=function(event){
		var e = event || window.event;
		var elem = e.srcElement||e.target;
		while(elem){
			if(elem.id == 'showpic' || elem.id == 'pic_list'){
				$("#pic_list").show();
				return;
			}else{
				$("#pic_list").hide();
				//return;
			}
			elem = elem.parentNode;
		}
	}
</script>
@stop
