@extends('front.common.layout')
@section('title')空中课堂-国卫健康云 @stop
@section('description')空中课堂-国卫健康云@stop
@section('keywords')空中课堂-国卫健康云 @stop
@section('content2')
<div class="p_sml_menu">
    	您当前位置：
    	<a href="/">首页</a>
        >
        <a href="/docface">空中课堂</a>
        > 
        专家风采
    </div>
    <div class="wid969">
    @if($oDoc)
	@foreach($oDoc as $k=>$v)
        <div class="block_box left white_box auto_h">
            <div class="left">
                <a href="/docface/doc-show/{{$v->id}}"><img class="doc_pic" src="{{$v->doc_thumb}}" /></a>
            </div>
            <div class="right txt_box">
                <p><label>专家:</label> {{str_cut_cms($v->doc_name,16)}}</p>
                <p><label>职称:</label> {{str_cut_cms($v->doc_position,16)}}</p>
                <p><label>科室:</label> {{str_cut_cms($v->doc_department,16)}}</p>
                <p><label>医院:</label> {{str_cut_cms($v->doc_hospital,16)}}</p>
            </div>
            <div class="clear"></div>
        </div>
		        @endforeach
		@endif
        <div class="clear"></div>	
        <div class="p_number_box">{{$oDoc->links()}}</div>
    </div>    
@stop
