@extends('front.common.layout')
@section('title')空中课堂-国卫健康云 @stop
@section('description')空中课堂-国卫健康云@stop
@section('keywords')空中课堂-国卫健康云 @stop
@section('content1')
<script src="/assets/js/video.js"></script>
	<div class="p_sml_menu">
    	您当前位置：
    	<a href="/">首页</a>
        >
        空中课堂
    </div>    
    
    @if(count($oFaceVideoAdvance))
    <div class="p_countdown_box">
    	<div class="countdown_left">
            @if($oFaceVideoAdvance->start_time>date('Y-m-d H:i:s'))
            @if((strtotime($oFaceVideoAdvance->start_time)-60*20)<time())
            <a href="/docface/enter-live/{{$oFaceVideoAdvance->id}}">
            @else
            <a href="#">
            @endif
            <?php 
            $aDepart = explode('|',$oFaceVideoAdvance->department_id); 
            ?>
            @if(in_array(5, $aDepart))
            	<img class="countdown_img" src="/assets/images/front/web/img001.jpg" /></a>
            @else
            	<img class="countdown_img" src="/assets/images/front/web/img04.jpg" /></a>
            @endif
            <div class="countdown">
            	<?php 
					$time = strtotime($oFaceVideoAdvance->start_time)-time();		
					$day = floor($time/(24*60*60));
					$day = str_pad($day,2,'0',STR_PAD_LEFT);
					
					$hour = floor(($time-$day*(24*60*60))/(60*60));
					$hour = str_pad($hour,2,'0',STR_PAD_LEFT);
					
					$minute = floor(($time-$day*(24*60*60)-$hour*(60*60))/(60));
					$minute = str_pad($minute,2,'0',STR_PAD_LEFT);
					
					$second = floor(($time-$day*(24*60*60)-$hour*(60*60)-$minute*60));
					$second = str_pad($second,2,'0',STR_PAD_LEFT);
				?>
				<div id="day">
                <div class="countdown_num day1 num_{{$day[0]}}"></div>
                <div class="countdown_num day2 num_{{$day[1]}}"></div>
                </div>
                <div id="hour">
                <div class="countdown_num hour1 num_{{$hour[0]}}"></div>
                <div class="countdown_num hour2 num_{{$hour[1]}}"></div>
                </div>
                <div id="minute">
                <div class="countdown_num minute1 num_{{$minute[0]}}"></div>
                <div class="countdown_num minute2 num_{{$minute[1]}}"></div>
                </div>
                <div id="second">
                <div class="countdown_num second1 num_{{$second[0]}}"></div>
                <div class="countdown_num second2 num_{{$second[1]}}"></div>
                </div>
                <input type="hidden" id="time" value="{{$time}}">
                <script>
					var int=self.setInterval("clock()", 1000)
					function clock(){
						var time = $("#time").val();
						if(time<0){
							return false;
							//window.location.reload();
						}
						$("#time").val(time-1);
						var nDay=Math.floor(time/(60*60) / 24); //Math.floor(nMS/(60*60) / 24) % 31
						var nH=Math.floor(time/(60*60)) % 24;//折合小时 var nH=Math.floor(nMS/(60*60) / 24)*24+Math.floor(nMS/(60*60)) % 24;
						var nM=Math.floor(time/(60)) % 60;//分
						var nS=Math.floor(time) % 60;
						$('#second').find('div:eq(0)').attr('class','countdown_num second1 num_'+Math.floor(nS/10)).end().find('div:eq(1)').attr('class','countdown_num second2 num_'+Math.floor(nS%10));
						$('#minute').find('div:eq(0)').attr('class','countdown_num minute1 num_'+Math.floor(nM/10)).end().find('div:eq(1)').attr('class','countdown_num minute2 num_'+Math.floor(nM%10));
						$('#hour').find('div:eq(0)').attr('class','countdown_num hour1 num_'+Math.floor(nH/10)).end().find('div:eq(1)').attr('class','countdown_num hour2 num_'+Math.floor(nH%10));
						$('#day').find('div:eq(0)').attr('class','countdown_num day1 num_'+Math.floor(nDay/10)).end().find('div:eq(1)').attr('class','countdown_num day2 num_'+Math.floor(nDay%10));
					}
				</script>
            </div>
            <div class="prompt">直播会议开始前20分钟可点击倒计时页面进行视频、声音的测试</div>
            @elseif(($oFaceVideoAdvance->start_time<date('Y-m-d H:i:s'))&&($oFaceVideoAdvance->end_time>date('Y-m-d H:i:s')))
            <a href="/docface/enter-live/{{$oFaceVideoAdvance->id}}"><img class="countdown_img" src="/assets/images/front/web/img07.jpg" /></a>
            @else
            <a href="#"><img src="/assets/images/front/web/img04.jpg"></a>
                <div class="countdown expect">
            	</div>  
            @endif
        </div>
        <div class="countdown_right">
        	<div class="h130">
            	<div class="left">
                	<a href=""><img src="{{$oFaceVideoAdvance->doc_info->doc_thumb}}" /></a>
                </div>
                <div class="right wid240">
                	<p><label>时间：</label>{{substr($oFaceVideoAdvance->start_time,0,16)}}—{{substr($oFaceVideoAdvance->end_time,11,5)}}</p>
                    <p class="h48"><label>主题：</label><a class="h48" href="">{{$oFaceVideoAdvance->video_title}}</a></p>
                    <p><label>专家：</label>{{$oFaceVideoAdvance->doc_info->doc_name}}</p>
                    <p><label>医院：</label>{{$oFaceVideoAdvance->doc_info->doc_hospital}}</p>
                </div>
                <div class="clear"></div>
            </div>
            <div class="intro_sml_tit">专家简介 :</div>
            <div class="intro_sml_cont h120">
                	{{str_cut_cms($oFaceVideoAdvance->doc_info->doc_introduction,220)}}
            </div>
        	<div class="intro_more">
            	<a class="more" href="/docface/doc-show/{{$oFaceVideoAdvance->doc_info->id}}">more</a>
            </div>            
        </div>
        <div class="clear"></div>
    </div>
    @endif
@stop
@section('content2')
@if($oFaceVideoAdvance1)
<div class="p_bar_bg">
	<div class="wid1025">
    	<div class="p_tit">精彩预告</div>
    </div>
</div>
<div class="wid1025">
	<div class="block_bg block_bg2">
    	<div class="foreshow_box">
        	<div class="foreshow_left">
            	<div class="left wid125">
                	<a href=""><img src="{{$oFaceVideoAdvance1->doc_info->doc_thumb}}" /></a>
                </div>
                <div class="right wid210">
                	<p><label>时间：</label>{{substr($oFaceVideoAdvance1->start_time,0,16)}}—{{substr($oFaceVideoAdvance1->end_time,11,5)}}</p>
                    <p><label>主题：</label><a href="">{{$oFaceVideoAdvance1->video_title}}</a></p>
                    <p><label>专家：</label>{{$oFaceVideoAdvance1->doc_info->doc_name}}</p>
                    <p><label>医院：</label>{{$oFaceVideoAdvance1->doc_info->doc_hospital}}</p>
                </div>
                <div class="clear"></div>
            </div>
            <div class="foreshow_right">
            	<p><label>课程简介 :</label></p>
                <p>
                	{{str_cut_cms($oFaceVideoAdvance1->video_introduce,220)}}
                </p>
            </div>
            <div class="clear"></div>        	
        </div>
    </div>
</div>
@endif
@if(count($oFaceVideoReview))
<div class="p_bar_bg bg_color1">
	<div class="wid1025">
    	<div class="p_tit">精彩回顾</div>
    </div>
</div>
<div class="wid1025">
	<div class="block_bg">
    	<div class="sml_menu_box">
           	<a class="a_more" href="/docface/review">more</a>
            <div class="clear"></div>
        </div>
        <div class="block_line">
        	@foreach($oFaceVideoReview as $k=>$v)
        	<div class="block_box1 left @if($k%3==1) dashed @endif">
            	<div class="center">
            		<a href="/docface/review-show/{{$v->id}}" class="sml_tit red mx_h">{{str_cut_cms($v->video_title,32)}}</a>
                </div>
                <div class="block_img">
            		<a href="/docface/review-show/{{$v->id}}"><img src="{{$v->video_thumb}}" /></a>
            	</div>
                <div class="block_cont height72">
                	<p>专家：{{$v->doc_name}}</p>
                    <p class="theme2">医院： {{str_cut_cms($v->doc_hospital,28)}}</p>
                </div>
            </div>
            @endforeach
            <div class="clear"></div>
        </div>
    </div>
</div>
@endif

@if(count($oDocs))
<div class="p_bar_bg bg_color2">
	<div class="wid1025">
    	<div class="p_tit">专家风采</div>
    </div>
</div>
<div class="wid1025">
	<div class="block_bg block_bg2">
    	<div class="sml_menu_box">
           	<a class="b_more" href="/docface/doc-list">more</a>
        </div>
        @foreach($oDocs as $k=>$v)
    	<div class="expert_box left @if($k%5==4) no_border @endif">
        	<div class="expert_img">
            	<a href="/docface/doc-show/{{$v->id}}"><img src="{{$v->doc_thumb}}" /></a>
            </div>
            <div class="expert_name">
            	<a href="/docface/doc-show/{{$v->id}}">{{$v->doc_name}}</a>
            </div>
        </div>
        @endforeach
        <div class="clear"></div>
    </div>
</div>
@endif
@if(!Auth::check())
<script type="text/javascript">
window.onload = function(){
	var aa = document.getElementsByTagName("a");
	for(var i=0;i<aa.length;i++){
		aa[i].setAttribute("href","#");
		aa[i].onclick=function(){
			please_login();return;
		}
	}
	document.getElementById("docface_banner").setAttribute("href","/docface");
	document.getElementById("index_banner").setAttribute("href","/");
	document.getElementById("docface_banner").setAttribute("onclick","");
	document.getElementById("index_banner").setAttribute("onclick","");
	document.getElementById("login_banner").onclick=function(){show_login_box()}
	document.getElementById("register_banner").setAttribute("href","/register");
	document.getElementById("register_banner").setAttribute("onclick","");
//	document.getElementById("codeimg_banner").setAttribute("onclick","");
}
function please_login(){
	alert('请您先登录');return;
}
</script>
@endif

@stop