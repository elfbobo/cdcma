@extends('front.common.layout')
@section('title')空中课堂-国卫健康云 @stop
@section('description')空中课堂-国卫健康云@stop
@section('keywords')空中课堂-国卫健康云 @stop
@section('content1')
<div class="p_sml_menu">
    	您当前位置：
    	<a href="/">首页</a>
        >
        <a href="/docface">空中课堂</a>
        > 
        <a href="/docface/doc-list">专家风采</a>
        >
        详细
    </div>
    <div class="intro_box">
    	<div class="intro_top">
        	<div class="wid330" style="width: 530px;margin-left: 200px;">
                <div class="intro_left">
                    <img class="doc_pic" src="{{$oDoc->doc_thumb}}" />
                </div>
                <div class="intro_right" style="width: 400px;">
                    <p class="font16">{{$oDoc->doc_name}}</p>
                    <p><label>职称:</label> {{$oDoc->doc_position}}</p>
                    <p><label>科室:</label> {{$oDoc->doc_department}}</p>
                    <p><label>医院:</label> {{$oDoc->doc_hospital}}</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="intro_cont">
        	<p class="bold">专家简介：</p>
            <p>{{$oDoc->doc_introduction}}</p>
        </div>
    </div>
</div>
<div class="p_bar_bg bg_color2">
	<div class="wid1025">
    	<div class="p_tit">相关课程</div>
    </div>
</div>
<div class="wid969">
@foreach($oDocFaceVideo as $k=>$v)
	<div class="block_box left white_box">
        <div class="sml_tit"><a class="tit" href="">{{$v->video_title}}</a></div>
        <div class="block_img">
        @if($v->video_type == 1)
            <a href="/docface/review-show/{{$v->id}}"><img src="{{$v->video_thumb}}" /></a>
        @else
        	<a href="/docface/enter-live/{{$v->id}}"><img src="{{$v->video_thumb}}" /></a>
        @endif
        </div>
        <div class="block_cont">
            <p>专家：{{$oDoc->doc_name}}</p>
            <p class="theme2">医院：{{str_cut_cms($oDoc->doc_hospital,28)}}</p>            
        </div>            
    </div>
@endforeach
	<div class="clear"></div>	
    <div class="p_number_box">{{$oDocFaceVideo->links()}}</div>
@stop
