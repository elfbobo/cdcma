@extends('front.common.layout')
@section('title')空中课堂-国卫健康云 @stop
@section('description')空中课堂-国卫健康云@stop
@section('keywords')空中课堂-国卫健康云 @stop
@section('content2')
<?php if($oFaceVideo->channel_type == 2){ ?>
    <script src="/assets/js/video.js"></script>
    <script type="text/javascript" src="/assets/js/jquery-ui-1.10.4.custom.min.js"></script>
    <script>window.FETCHER_SERVER_URL = "ws://fetcher.mudu.tv:8088";</script>
    <script src="//static.mudu.tv/fetcher/bundle.6d7aca164d2389e8bea6.js"></script>
    <script src="//static.mudu.tv/static/websdk/sdk.js"></script>
    <div class="p_sml_menu">
        您当前位置：
        <a href="/docface"> 空中课堂</a>
        >
      直播中 
    </div>
    <link rel="stylesheet" href="/assets/css/lkstyle.css">
    <div>
        <div class="lkmaindiv">
            <div class="lkchannel">
                <div class="lkchannel-icon"><img src="{{$videopic}}" width="100%" height="100%"></div>
                <div class="lkchannel-title">{{$videoname}}</div>
            </div>
            <div class="lkquesans">
                <div class="titleDiv"><h3>问答</h3></div>
                <div class="lkquesansBox">
                    <ul>
                    </ul>
                    <div style="height: 90px;"></div>
                </div>
                <div class="lk-shuru-input">
                    <div class="inputDiv"><textarea class="txt" id="sendmsg"></textarea></div>
                    <div class="btnDiv"><input class="btn" type="button" id="sendBtn" value="发送"></div>
                </div>
            </div>
            <div class="lkvideoDiv" id="a2"><img src="http://cdcma.bizconf.cn{{$oFaceVideo->video_thumb}}" width="100%" height="100%"></div>
            <div class="lkpptDiv"><div id="mudulive"></div></div>
            <a href="javascript:;" class="changebtn"><img src="/assets/images/front/changebtn.png" width="70" height="24"></a>        
        </div>
        <div style="height: 50px;"></div>
    <script type="text/javascript">
    $(function(){
         $(".changebtn").click(function(){
            if($(".lkvideoDiv").width()<400) {
                $(".lkvideoDiv").animate({width:"692px",height:"390px",top:"115px",left:"0px"}, 500);
                $(".lkpptDiv").animate({width:"320px", height:"180px",top:"0px",left:"706px"},500);
                player.setPlayerSize(320,180);
            }else{
                player.setPlayerSize(692,390);
                $(".lkpptDiv").animate({width:"692px",height:"390px",top:"115px",left:"0px"}, 500);
                $(".lkvideoDiv").animate({width:"320px", height:"180px",top:"0px",left:"706px"},500);
            }
        });
    })
    </script>
        <div id="pinjiaFloat" class="floatmain pie" style="display: none;">
            <table class="floatboxs">
                <tr>
                    <td>&nbsp;</td>
                    <td width="500" valign="middle" height="100%">
                        <div class="tcDivBox">
                            <a href="javascript:;" class="closeBtn closeA"></a>
                            <div class="tcTitleBox">提示</div>      
                            <div class="kdymain">
                                <div class="kdybox">
                                    <div class="kdy_bz">
                                        <div class="kdy_bz_t">
                                            <div class="kdy_bz_d">
                                                <div class="ktitle">备课充分内容熟练</div>
                                                <div class="ktuodong">
                                                    <div class="kbg"></div>
                                                    <div class="kzhizhen"><span></span></div>
                                                </div>
                                                <div class="knumber"><span id="score1">0</span>分</div>
                                            </div>
                                            <div class="kdy_bz_d">
                                                <div class="ktitle">逻辑清晰案例精彩</div>
                                                <div class="ktuodong">
                                                    <div class="kbg"></div>
                                                    <div class="kzhizhen"><span></span></div>
                                                </div>
                                                <div class="knumber"><span id="score2">0</span>分</div>
                                            </div>
                                            <div class="kdy_bz_d">
                                                <div class="ktitle">授课内容专业正确</div>
                                                <div class="ktuodong">
                                                    <div class="kbg"></div>
                                                    <div class="kzhizhen"><span></span></div>
                                                </div>
                                                <div class="knumber"><span id="score3">0</span>分</div>
                                            </div>
                                            <div class="kdy_bz_d">
                                                <div class="ktitle">讲课生动风趣</div>
                                                <div class="ktuodong">
                                                    <div class="kbg"></div>
                                                    <div class="kzhizhen"><span></span></div>
                                                </div>
                                                <div class="knumber"><span id="score4">0</span>分</div>
                                            </div>
                                            <div class="kdy_bz_d">
                                                <div class="ktitle">课程效果好</div>
                                                <div class="ktuodong">
                                                    <div class="kbg"></div>
                                                    <div class="kzhizhen"><span></span></div>
                                                </div>
                                                <div class="knumber"><span id="score5">0</span>分</div>
                                            </div>
                                        </div>            
                                    </div>
                                    <div class="allFenshu">综合评分：<span id="score6">0</span>分</div>
                                </div>    
                            </div>
                            <input type="hidden" id="videoid" value="{{$oFaceVideo->id}}"/>
                            <input type="hidden" id="userid" value="{{$iUserId}}"/>
                            <div class="promptBtn"><a href="javascript:;" class="dpbtn btn">确认</a></div>
                        </div>   
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
        <input type="hidden" id="topicpage" value="1">
       <!--  <input type="hidden" id="looksecond" value="0"> -->
    <script type="text/javascript">
    $(".dpbtn").click(function(){
        var videoid = $('#videoid').val();
        var userid = $('#userid').val();
        var score1 = $('#score1').html();
        var score2 = $('#score2').html();
        var score3 = $('#score3').html();
        var score4 = $('#score4').html();
        var score5 = $('#score5').html();
        var score6 = $('#score6').html();
        var url = '/docface/live_score';
        var data = {videoid:videoid,userid:userid,score1:score1,score2:score2,score3:score3,score4:score4,score5:score5,score6:score6,device:1};
        $.post(url,data,function(msg){
            if(msg == 'success'){
                // alert('评分成功！');
                $("#pinjiaFloat").remove();
                return;
            }else{
                alert('您已经点评过了！');
                return;
            }
        })
    })
    var player;
    Mudu.Init({{$oFaceVideo->video_id}}, function(){
        //初始化播放器
        player = new Mudu.Player({
            // 播放器容器ID，播放器会被添加到该DOM元素中
            containerId: 'mudulive',
            // 播放器播放类型：支持`live`和`vod`两个值，live为直播播放，vod为点播播放
            type: 'live',
            // 播放器视频播放地址
            src: Mudu.Room.GetPlayAddr(),
            image: 'http://cdcma.bizconf.cn{{$oFaceVideo->video_thumb}}',
            // 播放器是否自动播放
            autoplay: false,
            // 播放器是否显示控制条
            controls: true,
            // 播放器是否循环播放, 默认为false
            repeat: false,
            // 播放器宽度，单位为像素，默认为480
            width: 320,
            // 播放器高度，单位为像素，默认为270
            height: 180
        });
        Mudu.Room.User.Assign('{{$sUserName}}', '{{$sUserthumb}}','{{$iUserId}}');  
        //获取banner
        var ads = Mudu.Room.GetBanners();
        if(ads){
            $("#videobanner").find('img').attr('src',ads[0].img);
        }
        //开始播放 player.play();
        var state = player.getState()
        //暂停播放 player.pause();
        // 返回直播间名字，类型为string
        var roomName = Mudu.Room.GetName();
        // 返回直播状态，类型为number: `1`为正在直播，`0`为不在直播
        var roomLiveStatus = Mudu.Room.GetLiveStatus();
        // 返回直播间浏览量，类型为number整数
        var roomViewNum = Mudu.Room.GetViewNum();
        // 返回直播间视频地址，类型为string
        var roomPlayAddr = Mudu.Room.GetPlayAddr();
        Mudu.MsgBus.On(
            // 事件名，值为Room.StreamEvent
            'Room.StreamEvent',
            // 事件处理函数，参数类型为object
            function (data) {
                data = JSON.parse(data);
                if(data.event == 1){
                    player.play();
                }else{
                    player.stop();
                    $("#pinjiaFloat").show();
                }
            }
        );
        //获取第一页话题
        gettopic();
        //接受新话题
        Mudu.MsgBus.On('Topic.New',function (topic) {
            topic = JSON.parse(topic);
            if(topic.checked == 1){//判断是否已审核
                $(".insertTopic").find(".infor").each(function(){
                    if($(this).html() == topic.message){
                        $(this).parent().remove();
                    }
                });
                html = '<li id="'+topic.id+'"><div class="name">'+topic.username+'</div><div class="time">'+topic.updated_at+'</div><div class="infor">'+topic.message+'</div></li>';
                $(".lkquesansBox ul").prepend(html);
            }
        })
        Mudu.MsgBus.On('Topic.Reply.New',function (reply) {
            reply = JSON.parse(reply);
            // console.log(reply);
            html = '<li><div class="name">'+reply.username+'</div><div class="time">'+reply.updated_at+'</div><div class="infor">'+reply.message+'</div></li>';
            $("#"+reply.belong_to).after(html);
        })
        //ppt
        var isPptOpen = Mudu.Room.PPT.IsOpen();
        if(isPptOpen){
            var pptUrl = Mudu.Room.PPT.GetUrl();
            $("#a2").find("img").attr("src", pptUrl);
            Mudu.MsgBus.On( "PPT.Changed", function (data) {
                data = JSON.parse(data);
                // console.log('新的ppt图片地址为：', data.url);
                $("#a2").find("img").attr("src", data.url);
            })
        }
    });
    //获取话题互动列表
    function gettopic(){
        var page = $("#topicpage").val();
        if(page != 0){
            Mudu.Room.Topic.Get(page,function (response) {
             // response格式为: {status: 'y', flag: 100, topics: [topicItem1, topicItem2, ...]}
                response = JSON.parse(response);
                if(response.topics && response.topics.length>0){
                    var html = '';
                    for (var i = 0; i < response.topics.length; i++) {
                        html += '<li id="'+response.topics[i].id+'"><div class="name">'+response.topics[i].username+'</div><div class="time">'+response.topics[i].updated_at+'</div><div class="infor">'+response.topics[i].message+'</div></li>';
                        if(response.topics[i].replies){
                            for (var k = 0; k < response.topics[i].replies.length; k++) {
                                html += '<li><div class="name">'+response.topics[i].replies[k].username+'</div><div class="time">'+response.topics[i].replies[k].updated_at+'</div><div class="infor">'+response.topics[i].replies[k].message+'</div></li>';
                            }
                        }
                    }
                    $(".lkquesansBox ul").append(html);
                    page=parseInt(page)+1;
                    $("#topicpage").val(page)
                    gettopic();
                }else{
                    $("#topicpage").val(0);
                }
            })
        }
        return false;   
    }
    //发送话题
    $("#sendBtn").click(function(){
        var sendmsg = $("#sendmsg").val();
        if(sendmsg){
            Mudu.Room.Topic.SendTopic({
                msg: sendmsg
            },
            function (response) {
                response = JSON.parse(response);
                if(response.status == 'y'){
                    html = '<li class="insertTopic"><div class="name">{{$sUserName}}</div><div class="time"><?php echo date('Y-m-d H:i:s'); ?></div><div class="infor">'+sendmsg+'</div></li>';
                    $(".lkquesansBox ul").prepend(html);
                    $("#sendmsg").val('');
                }           
            })
        }
    })
    $(function(){
        $(".floatmain .closeBtn").click(function(){
            $(this).parents(".floatmain").hide();
        })
        $(".kzhizhen").draggable({drag:function(){
                var kdiv = $(this).parents(".kdy_bz_d");
                ksz(kdiv);
            },containment: "parent",axis:"x"});  
        //$("#drag").draggable({containment: "div#main",axis:"x"}); //只允许横向拖动
        //$("#drag").draggable({containment: "div#main",axis:"y"}); //只允许竖向拖动
        $(".kdy_bz_d").resizable({
        });
        $(".ktuodong").click(function(e){
           var divx = $(".ktuodong").offset().left-$(document).scrollLeft();
            //("#drag").offset().left;
            //这里可得到鼠标X坐标
            var pointX = e.pageX;
            var x = pointX - divx;
            console.log(x);
            if(x<=244){$(this).find(".kzhizhen").css("left",x);}
            else{$(this).find(".kzhizhen").css("left",244);}
            var kdiv = $(this).parents(".kdy_bz_d");
            ksz(kdiv);
        });
    })
    function ksz(kdiv){
        var kleft = parseInt(kdiv.find(".kzhizhen").css("left"));
        var n = Math.round(100*kleft/244);
        kdiv.find(".ktuodong .kbg").css("width",kleft);
        kdiv.find(".knumber span").html(Math.round(n/5));
        var allfenshu = 0;
        $(".kdymain .kdybox .knumber").each(function(){
            allfenshu = allfenshu + Number($(this).find("span").text());
        })
        $(".kdymain .kdybox .allFenshu span").text(allfenshu);
    }
    </script>
    <script type="text/javascript">
    $(function(){
        // setInterval(function(){
        //  var looksecond = $("#looksecond").val();
        //  if(looksecond){
        //      looksecond = parseInt(looksecond)+60;
        //  }else{
        //      looksecond = 60;
        //  }
        //  $("#looksecond").val(looksecond);
        // },60000);
        setInterval("addUserMinuteLive({{$oFaceVideo->id}},{{$iUserId}})",180000);
        // setInterval("addUserMinuteLive({{$oFaceVideo->id}},{{$iUserId}})",10000);
    }) 
    function addUserMinuteLive(videoid,userid){
        // var timestamp = $("#looksecond").val();
        // var time_second = timestamp/60;
        // console.log(time_second);
        var timestamp = Date.parse(new Date());
        var time_second = timestamp/1000;
        var url = "/docface/video_view_live/"+videoid+"/"+userid+"/"+time_second;
        $.post(url,{},function(msg){
            if(msg=='success'){
                console.log('success');
            }
        })
    }
    </script>
    </div>

@stop
<?php }else{ ?>
    <script src="/assets/js/video.js"></script>
    <script type="text/javascript" src="/assets/js/jquery-ui-1.10.4.custom.min.js"></script>
    <link rel="stylesheet" href="/assets/css/lkstyle.css">
    <div class="p_sml_menu">
        您当前位置：
        <a href="/docface"> 空中课堂</a>
        >
      直播中 
    </div> 
    <div class="p_center_box">
        <div class="i_cont" style="height:600px;width:940px;">
            <iframe width="100%" height="100%" src="{{$oFaceVideo->video_url}}?nickName={{$sUserName}}"></iframe>
        </div>
        <div class="clear"></div>
    </div>
    <div id="pinjiaFloat" class="floatmain pie" style="display: none;">
        <table class="floatboxs">
            <tr>
                <td>&nbsp;</td>
                <td width="500" valign="middle" height="100%">
                    <div class="tcDivBox">
                        <a href="javascript:;" class="closeBtn closeA"></a>
                        <div class="tcTitleBox">提示</div>      
                        <div class="kdymain">
                            <div class="kdybox">
                                <div class="kdy_bz">
                                    <div class="kdy_bz_t">
                                        <div class="kdy_bz_d">
                                            <div class="ktitle">备课充分内容熟练</div>
                                            <div class="ktuodong">
                                                <div class="kbg"></div>
                                                <div class="kzhizhen"><span></span></div>
                                            </div>
                                            <div class="knumber"><span id="score1">0</span>分</div>
                                        </div>
                                        <div class="kdy_bz_d">
                                            <div class="ktitle">逻辑清晰案例精彩</div>
                                            <div class="ktuodong">
                                                <div class="kbg"></div>
                                                <div class="kzhizhen"><span></span></div>
                                            </div>
                                            <div class="knumber"><span id="score2">0</span>分</div>
                                        </div>
                                        <div class="kdy_bz_d">
                                            <div class="ktitle">授课内容专业正确</div>
                                            <div class="ktuodong">
                                                <div class="kbg"></div>
                                                <div class="kzhizhen"><span></span></div>
                                            </div>
                                            <div class="knumber"><span id="score3">0</span>分</div>
                                        </div>
                                        <div class="kdy_bz_d">
                                            <div class="ktitle">讲课生动风趣</div>
                                            <div class="ktuodong">
                                                <div class="kbg"></div>
                                                <div class="kzhizhen"><span></span></div>
                                            </div>
                                            <div class="knumber"><span id="score4">0</span>分</div>
                                        </div>
                                        <div class="kdy_bz_d">
                                            <div class="ktitle">课程效果好</div>
                                            <div class="ktuodong">
                                                <div class="kbg"></div>
                                                <div class="kzhizhen"><span></span></div>
                                            </div>
                                            <div class="knumber"><span id="score5">0</span>分</div>
                                        </div>
                                    </div>            
                                </div>
                                <div class="allFenshu">综合评分：<span id="score6">0</span>分</div>
                            </div>    
                        </div>
                        <input type="hidden" id="videoid" value="{{$oFaceVideo->id}}"/>
                        <input type="hidden" id="userid" value="{{$iUserId}}"/>
                        <div class="promptBtn"><a href="javascript:;" class="dpbtn btn">确认</a></div>
                    </div>   
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        var s1 = setInterval("addUserMinuteLive({{$oFaceVideo->id}},{{$iUserId}})",180000);
        var s2 =setInterval(test,1000);
        function test(){
            var timestamp = Date.parse(new Date());
            if(timestamp >= <?php echo strtotime($oFaceVideo->end_time)*1000; ?>){
                $("#pinjiaFloat").show();
                clearInterval(s2);
            }
        }

        $(".dpbtn").click(function(){
            var videoid = $('#videoid').val();
            var userid = $('#userid').val();
            var score1 = $('#score1').html();
            var score2 = $('#score2').html();
            var score3 = $('#score3').html();
            var score4 = $('#score4').html();
            var score5 = $('#score5').html();
            var score6 = $('#score6').html();
            var url = '/docface/live_score';
            var data = {videoid:videoid,userid:userid,score1:score1,score2:score2,score3:score3,score4:score4,score5:score5,score6:score6,device:1};
            $.post(url,data,function(msg){
                if(msg == 'success'){
                    // alert('评分成功！');
                    $("#pinjiaFloat").remove();
                    return;
                }else{
                    alert('您已经点评过了！');
                    return;
                }
            })
        })
        $(function(){
            $(".floatmain .closeBtn").click(function(){
                $(this).parents(".floatmain").hide();
            })
            $(".kzhizhen").draggable({drag:function(){
                    var kdiv = $(this).parents(".kdy_bz_d");
                    ksz(kdiv);
                },containment: "parent",axis:"x"});  
            //$("#drag").draggable({containment: "div#main",axis:"x"}); //只允许横向拖动
            //$("#drag").draggable({containment: "div#main",axis:"y"}); //只允许竖向拖动
            $(".kdy_bz_d").resizable({
            });
            $(".ktuodong").click(function(e){
               var divx = $(".ktuodong").offset().left-$(document).scrollLeft();
                //("#drag").offset().left;
                //这里可得到鼠标X坐标
                var pointX = e.pageX;
                var x = pointX - divx;
                console.log(x);
                if(x<=244){$(this).find(".kzhizhen").css("left",x);}
                else{$(this).find(".kzhizhen").css("left",244);}
                var kdiv = $(this).parents(".kdy_bz_d");
                ksz(kdiv);
            });
        })
        function ksz(kdiv){
            var kleft = parseInt(kdiv.find(".kzhizhen").css("left"));
            var n = Math.round(100*kleft/244);
            kdiv.find(".ktuodong .kbg").css("width",kleft);
            kdiv.find(".knumber span").html(Math.round(n/5));
            var allfenshu = 0;
            $(".kdymain .kdybox .knumber").each(function(){
                allfenshu = allfenshu + Number($(this).find("span").text());
            })
            $(".kdymain .kdybox .allFenshu span").text(allfenshu);
        }
    </script>
@stop
<?php } ?>