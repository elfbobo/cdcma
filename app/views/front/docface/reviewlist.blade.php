@extends('front.common.layout')
@section('title')空中课堂-国卫健康云 @stop
@section('description')空中课堂-国卫健康云@stop
@section('keywords')空中课堂-国卫健康云 @stop
@section('content1')
<div class="p_sml_menu">
    	您当前位置：
    	<a href="/">首页</a>
        >
        <a href="/docface">空中课堂</a>
        > 
        精彩回顾
    </div>
    <div class="p_section_menu">
    	<a href="/docface/review" @if($iDepartmentId==0) class="cur" @endif style="margin: 0 0px;">全部</a>
    		<?php 
			$aDepartment = Config::get('config.department');
			?>
			@foreach($aDepartment as $k=>$v)
			<a href="/docface/review/{{$k}}" @if($iDepartmentId==$k) class="cur" @endif style="margin: 0 0px;">{{$v}}</a>
			@endforeach
    </div>
@stop
@section('content2')
<div class="wid969">
	@if($oVideoReview)
	@foreach($oVideoReview as $k=>$v)
	<div class="block_box left white_box">
        <div class="sml_tit"><a class="tit" href="/docface/review-show/{{$v->id}}">{{$v->video_title}}</a></div>
        <div class="block_img">
            <a href="/docface/review-show/{{$v->id}}"><img src="{{$v->video_thumb}}" /></a>
        </div>
        <div class="block_cont">
            <p>专家：{{$v->doc_name}}</p>
            <p class="theme2">医院：{{str_cut_cms($v->doc_hospital,28)}}</a></p>            
        </div>            
    </div>
    @endforeach
	@endif
	<div class="clear"></div>	
    
    <div class="p_number_box">
    	{{$oVideoReview->links()}}
    </div>
    
    
</div>
@stop
