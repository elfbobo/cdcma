<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>国卫健康云的用户，您好</div>
<br>
		<div>
			请点击以下链接重新设置您的密码：<br><br>
			<a href="{{$url}}">{{$url}}</a><br>
			（如果无法点击，请将它复制到浏览器的地址栏中。）<br><br>
			国卫健康云<br>
			<a href="http://cdcma.bizconf.cn/">http://cdcma.bizconf.cn/</a>
		</div>
	</body>
</html>
