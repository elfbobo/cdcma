<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>尊敬的{{$user_name}}，您好</div><br>
		<div>
			您下载的会议资料文件<br>
			{{$body}}<br><br>
			（如果无法点击，请将它复制到浏览器的地址栏中。）<br><br><br><br><br>
			国卫健康云<br>
			<a href="http://cdcma.bizconf.cn/">http://cdcma.bizconf.cn/</a><br><br><br>
		</div>
	</body>
</html>
