<?php

/*
|--------------------------------------------------------------------------
| Application Routes
| 应用程序路由
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| 在这里你可以为应用程序注册所有的路由。
| It's a breeze. Simply tell Laravel the URIs it should respond to
| 这非常的容易。简单的告诉 Laravel 这些 URI 应该响应，
| and give it the Closure to execute when that URI is requested.
| 并且给它一个闭包，当那个URI发起请求后执行它。
|
*/
Route::any('/weixin', 'WeixinController@getIndex');
Route::Controller('wechat', 'WechatController');
Route::post('/test', array('uses' => 'HomeController@Test'));
Route::get('/videoauditauto/', array('uses' => 'HomeController@Videoauditauto'));
Route::get('/app/index', function () {
    return View::make('front.app');
});
Route::get('admlogin', array('uses' => 'AdminController@Login'));
Route::post('admlogin', array('uses' => 'AdminController@LoginDo'));
Route::get('admlogout', array('uses' => 'AdminController@LoginOut'));
Route::get('admsys', array('uses' => 'AdminController@SettingSys'));
Route::post('admsys', array('uses' => 'AdminController@SettingSys'));

Route::get('/admcmeorder', array('uses' => 'AdmCmeController@OrderList'));        //好医生兑换记录列表
Route::get('/admcmeexport', array('uses' => 'AdmCmeController@exportList'));    //好医生导出详情
Route::get('/admcmecheck', array('uses' => 'AdmCmeController@checkInfo'));
Route::post('/admcmecheck', array('uses' => 'AdmCmeController@checkInfo'));
Route::get('/admkypxorder', array('uses' => 'AdmKypxController@OrderList'));    //科研培训兑换记录列表

Route::group(array('before' => 'admallauth'), function () {
    //后台首页
    Route::get('kyadmin', array('uses' => 'AdminController@Index'));
});
//病例征集
Route::group(array('before' => 'admcase'), function () {
    Route::get('/admcase/list', array('uses' => 'AdmCaseController@getCaseList'));
    Route::get('/admcase/check/{id}', array('uses' => 'AdmCaseController@getCaseCheck'));
    Route::post('/admcase/check/{id}', array('uses' => 'AdmCaseController@postCaseCheck'));
    Route::get('/admcase/download/{id}', array('uses' => 'AdmCaseController@downloadCase'));
    Route::get('/admcase/downZip', array('uses' => 'AdmCaseController@downZip'));
    Route::get('/admcase/downAllCase', array('uses' => 'AdmCaseController@downAllCase'));
    Route::get('/admcase/change/{id}', array('uses' => 'AdmCaseController@change'));
});
//后台资讯中心 
Route::group(array('before' => 'adminformation'), function () {
    Route::get('/adminformation/information-list', array('uses' => 'AdmInformationController@InformationList'));
    Route::get('/adminformation/add-information', array('uses' => 'AdmInformationController@AddInformation'));
    Route::get('/adminformation/do-information-add', array('uses' => 'AdmInformationController@DoInformationAdd'));
    Route::get('/adminformation/do-dele/{iId}', array('uses' => 'AdmInformationController@DoDele'));
    Route::get('/adminformation/do-edit/{iId}', array('uses' => 'AdmInformationController@DoEdit'));
    Route::get('/adminformation/do-information-edit', array('uses' => 'AdmInformationController@DoInformationEdit'));
    Route::post('/adminformation/upload-thumb', array('uses' => 'AdmInformationController@UploadThumb'));

    //资讯中心对接微信号发布推送消息  20160513 dll add
    //1:慢性疾病与转化医学 cdtm2015、2:心内空间 cmacardio、3:呼吸科空间 cmaresp
    Route::get('/admmaterial/list/{iType}', array('uses' => 'AdmMaterialController@MaterialList'));
    Route::get('/admmaterial/add/{iType}', array('uses' => 'AdmMaterialController@MaterialAdd'));
    Route::post('/admmaterial/add-do', array('uses' => 'AdmMaterialController@MaterialAddDo'));
    Route::get('/admmaterial/edit/{iId}', array('uses' => 'AdmMaterialController@MaterialEdit'));
    Route::post('/admmaterial/edit-do/{iId}', array('uses' => 'AdmMaterialController@MaterialEditDo'));
    Route::get('/admmaterial/delete/{iId}', array('uses' => 'AdmMaterialController@MaterialDelete'));
    Route::post('/admmaterial/image-upload', array('uses' => 'AdmMaterialController@ImageUpload'));
    Route::post('/admmaterial/push', array('uses' => 'AdmMaterialController@MaterialPush'));
    Route::get('/admmaterial/preview/{iId}/{iType}', array('uses' => 'AdmMaterialController@MaterialPreview'));
    Route::post('/admmaterial/preview-do', array('uses' => 'AdmMaterialController@MaterialPreviewDo'));
});

//后台小调研
Route::group(array('before' => 'admresearch'), function () {
    Route::get('/admresearch/research-list', array('uses' => 'AdmResearchController@ResearchList'));
    Route::get('/admresearch/add-period', array('uses' => 'AdmResearchController@AddPeriod'));
    Route::get('/admresearch/do-period-add', array('uses' => 'AdmResearchController@DoPeriodAdd'));
    Route::post('/admresearch/upload-thumb', array('uses' => 'AdmResearchController@UploadThumb'));
    Route::get('/admresearch/edit-research/{id}', array('uses' => 'AdmResearchController@EditResearch'));
    Route::get('/admresearch/do-period-edit', array('uses' => 'AdmResearchController@DoPeriodEdit'));
    Route::get('/admresearch/delete-research/{id}', array('uses' => 'AdmResearchController@DeleteResearch'));
    Route::get('/admresearch/add-question', array('uses' => 'AdmResearchController@AddQuestion'));
    Route::post('/admresearch/upload-thumb', array('uses' => 'AdmResearchController@UploadThumb'));//文件上传
    Route::get('/admresearch/question-list/{id}', array('uses' => 'AdmResearchController@QuestionList'));
    Route::get('/admresearch/add-question/{iId}', array('uses' => 'AdmResearchController@AddQuestion'));
    Route::get('/admresearch/do-adds', array('uses' => 'AdmResearchController@DoAdds'));
    Route::get('/admresearch/do-add', array('uses' => 'AdmResearchController@DoAdd'));
    Route::get('/admresearch/edit-survey/{id}', array('uses' => 'AdmResearchController@EditSurvey'));
    Route::get('/admresearch/delete-survey/{id}/{iperiod}', array('uses' => 'AdmResearchController@DeleteSurvey'));
    Route::get('/admresearch/survey-result/{id}', array('uses' => 'AdmResearchController@SurveyResult'));
    Route::get('/admresearch/result-list/{id}', array('uses' => 'AdmResearchController@ResultList'));
    Route::get('/admresearch/research_result/{id}', array('uses' => 'AdmResearchController@ResearchResult'));
    Route::get('/admresearch/user-id', array('uses' => 'AdmResearchController@UserId'));
    Route::get('/admresearch/users/{id}/{sd}', array('uses' => 'AdmResearchController@Users'));
});
//名医面对面
Route::group(array('before' => 'admdocface'), function () {
    //名医面对面
    Route::get('/admdocface/doc', array('uses' => 'AdmDocFaceController@Doc'));    //专家
    Route::get('/admdocface/add-doc', array('uses' => 'AdmDocFaceController@AddDoc'));    //新增专家
    Route::post('/admdocface/add-doc-do', array('uses' => 'AdmDocFaceController@AddDocDo'));    //新增专家do
    Route::get('/admdocface/edit-doc/{id}', array('uses' => 'AdmDocFaceController@EditDoc'));    //编辑专家
    Route::post('/admdocface/edit-doc-do/{id}', array('uses' => 'AdmDocFaceController@EditDocDo'));    //编辑专家do
    Route::get('/admdocface/del-doc/{id}', array('uses' => 'AdmDocFaceController@DelDoc'));        //删除专家
    Route::post('/admdocface/upload-doc-thumb', array('uses' => 'AdmDocFaceController@UploadDocThumb'));
    Route::get('/admdocface/live-score/{id}', array('uses' => 'AdmDocFaceController@LiveScore'));//试题列表
    Route::get('/admdocface/survey/{id}', array('uses' => 'AdmDocFaceController@SurveyList'));//试题列表
    Route::get('/admdocface/add-survey/{id}', array('uses' => 'AdmDocFaceController@SurveyAdd'));//试题新增
    Route::post('/admdocface/add-survey/{id}', array('uses' => 'AdmDocFaceController@SurveyAddDo'));//试题新增
    Route::get('/admdocface/edit-survey/{id}', array('uses' => 'AdmDocFaceController@SurveyEdit'));//试题编辑
    Route::post('/admdocface/edit-survey/{id}', array('uses' => 'AdmDocFaceController@SurveyEditDo'));//试题编辑
    Route::get('/admdocface/del-survey/{id}', array('uses' => 'AdmDocFaceController@SurveyDel'));//试题编辑
    Route::get('/admdocface/sign-in-log/{id}', array('uses' => 'AdmDocFaceController@SignInLog'));        //导出签到记录
    Route::get('/admdocface/sign-in-log-offline/{id}',
        array('uses' => 'AdmDocFaceController@SignInLogOffline'));//导出线下会议签到记录(已废弃)
    Route::get('/admdocface/export-offline-sign-in-log',
        array('uses' => 'AdmDocFaceController@ExportOfflineSignInLog'));//导出线下会议签到记录--新版
    Route::get('/admdocface/survey-log/{id}', array('uses' => 'AdmDocFaceController@SurveyLog'));
    Route::get('/admdocface/export-log/{id}', array('uses' => 'AdmDocFaceController@ExportLog'));
    Route::get('/admdocface/week-log/{id}', array('uses' => 'AdmDocFaceController@WeekLog'));
    Route::get('/admdocface/export-week-log/{id}/{c?}', array('uses' => 'AdmDocFaceController@ExportWeekLog'));
    Route::get('/admdocface/one-video-log/{id}', array('uses' => 'AdmDocFaceController@OneVideoLog'));
    Route::get('/admdocface/review-log', array('uses' => 'AdmDocFaceController@ReivewLog')); //导出录播观看情况
    Route::post('/admdocface/export-review-log', array('uses' => 'AdmDocFaceController@ExportReivewLog')); //导出录播观看情况导出
    //月报导出
    Route::get('/admdocface/month', array('uses' => 'AdmDocFaceController@Month'));
    Route::get('/admdocface/mounth-log/{s}/{c?}', array('uses' => 'AdmDocFaceController@MounthLog'));
    Route::get('/admdocface/mounth-log-regin/{s}/{r}', array('uses' => 'AdmDocFaceController@MounthLogRegin'));
    Route::get('/admdocface/cluster/{s}', array('uses' => 'AdmDocFaceController@Cluster'));
    Route::get('/admdocface/regin-docs/{s}', array('uses' => 'AdmDocFaceController@ReginDocs'));
    Route::get('/admdocface/area-docs/{s}', array('uses' => 'AdmDocFaceController@AreaDocs'));
    Route::get('/admdocface/rep-docs/{s}', array('uses' => 'AdmDocFaceController@RepDocs'));
    Route::get('/admdocface/bigdoc-docs/{s}', array('uses' => 'AdmDocFaceController@BigdocDocs'));

    //更新签到积分
    Route::get('/admdocface/update-sign-in/{id}', array('uses' => 'AdmDocFaceController@UpdateSignIn'));

    //周报告
    Route::get('/admdocface/week-cluster/{id}', array('uses' => 'AdmDocFaceController@WeekCluster'));
    Route::get('/admdocface/week-regin-docs/{id}', array('uses' => 'AdmDocFaceController@WeekReginDocs'));
    Route::get('/admdocface/week-area-docs/{id}', array('uses' => 'AdmDocFaceController@WeekAreaDocs'));
    Route::get('/admdocface/week-rep-docs/{id}', array('uses' => 'AdmDocFaceController@WeekRepDocs'));
    Route::get('/admdocface/week-bigdoc-docs/{id}', array('uses' => 'AdmDocFaceController@WeekBigdocDocs'));

    Route::get('/admdocface/video', array('uses' => 'AdmDocFaceController@Video'));    //视频
    Route::get('/admdocface/add-video', array('uses' => 'AdmDocFaceController@AddVideo'));    //新增视频
    Route::post('/admdocface/add-video-do', array('uses' => 'AdmDocFaceController@AddVideoDo'));    //新增视频do
    Route::get('/admdocface/edit-video/{id}', array('uses' => 'AdmDocFaceController@EditVideo'));    //编辑视频
    Route::post('/admdocface/edit-video-do/{id}', array('uses' => 'AdmDocFaceController@EditVideoDo'));    //编辑视频do
    Route::get('/admdocface/del-video/{id}', array('uses' => 'AdmDocFaceController@DelVideo'));        //删除视频
    Route::post('/admdocface/upload-video-thumb', array('uses' => 'AdmDocFaceController@UploadVideoThumb'));
    Route::post('/admdocface/get-video-url', array('uses' => 'AdmDocFaceController@GetVideoUrl'));
    Route::get('/admdocface/review-export/{id}', array('uses' => 'AdmDocFaceController@ReviewExport'));
    //清除用户观看某直播的积分
    Route::get('/admdocface/clear-user-score/{id}', array('uses' => 'AdmDocFaceController@ClearUserScore'));
    //积分整理
    Route::get('/admdocface/calcu-user-score', array('uses' => 'AdmDocFaceController@CalcuUserScore'));
    //20170102 移动端注册用户直播录播次数统计
    Route::get('/admadmdocface/h5user-log', array('uses' => 'AdmDocFaceController@H5userLog'));

});

//精品课堂
Route::group(array('before' => 'admdocfacerec'), function () {
    //名医面对面
    Route::get('/admdocfacerec/doc', array('uses' => 'AdmDocFaceRecController@Doc'));    //专家
    Route::get('/admdocfacerec/add-doc', array('uses' => 'AdmDocFaceRecController@AddDoc'));    //新增专家
    Route::post('/admdocfacerec/add-doc-do', array('uses' => 'AdmDocFaceRecController@AddDocDo'));    //新增专家do
    Route::get('/admdocfacerec/edit-doc/{id}', array('uses' => 'AdmDocFaceRecController@EditDoc'));    //编辑专家
    Route::post('/admdocfacerec/edit-doc-do/{id}', array('uses' => 'AdmDocFaceRecController@EditDocDo'));    //编辑专家do
    Route::get('/admdocfacerec/del-doc/{id}', array('uses' => 'AdmDocFaceRecController@DelDoc'));        //删除专家
    Route::post('/admdocfacerec/upload-doc-thumb', array('uses' => 'AdmDocFaceRecController@UploadDocThumb'));
    Route::get('/admdocfacerec/live-score/{id}', array('uses' => 'AdmDocFaceRecController@LiveScore'));//试题列表
    Route::get('/admdocfacerec/survey/{id}', array('uses' => 'AdmDocFaceRecController@SurveyList'));//试题列表
    Route::get('/admdocfacerec/add-survey/{id}', array('uses' => 'AdmDocFaceRecController@SurveyAdd'));//试题新增
    Route::post('/admdocfacerec/add-survey/{id}', array('uses' => 'AdmDocFaceRecController@SurveyAddDo'));//试题新增
    Route::get('/admdocfacerec/edit-survey/{id}', array('uses' => 'AdmDocFaceRecController@SurveyEdit'));//试题编辑
    Route::post('/admdocfacerec/edit-survey/{id}', array('uses' => 'AdmDocFaceRecController@SurveyEditDo'));//试题编辑
    Route::get('/admdocfacerec/del-survey/{id}', array('uses' => 'AdmDocFaceRecController@SurveyDel'));//试题编辑
    Route::get('/admdocfacerec/sign-in-log/{id}', array('uses' => 'AdmDocFaceRecController@SignInLog'));        //导出签到记录
    Route::get('/admdocfacerec/sign-in-log-offline/{id}',
        array('uses' => 'AdmDocFaceRecController@SignInLogOffline'));//导出线下会议签到记录(已废弃)
    Route::get('/admdocfacerec/export-offline-sign-in-log',
        array('uses' => 'AdmDocFaceRecController@ExportOfflineSignInLog'));//导出线下会议签到记录--新版
    Route::get('/admdocfacerec/survey-log/{id}', array('uses' => 'AdmDocFaceRecController@SurveyLog'));
    Route::get('/admdocfacerec/export-log/{id}', array('uses' => 'AdmDocFaceRecController@ExportLog'));
    Route::get('/admdocfacerec/week-log/{id}', array('uses' => 'AdmDocFaceRecController@WeekLog'));
    Route::get('/admdocfacerec/export-week-log/{id}/{c?}', array('uses' => 'AdmDocFaceRecController@ExportWeekLog'));
    Route::get('/admdocfacerec/one-video-log/{id}', array('uses' => 'AdmDocFaceRecController@OneVideoLog'));
    Route::get('/admdocfacerec/review-log', array('uses' => 'AdmDocFaceRecController@ReivewLog')); //导出录播观看情况
    Route::post('/admdocfacerec/export-review-log',
        array('uses' => 'AdmDocFaceRecController@ExportReivewLog')); //导出录播观看情况导出
    //月报导出
    Route::get('/admdocfacerec/month', array('uses' => 'AdmDocFaceRecController@Month'));
    Route::get('/admdocfacerec/mounth-log/{s}/{c?}', array('uses' => 'AdmDocFaceRecController@MounthLog'));
    Route::get('/admdocfacerec/mounth-log-regin/{s}/{r}', array('uses' => 'AdmDocFaceRecController@MounthLogRegin'));
    Route::get('/admdocfacerec/cluster/{s}', array('uses' => 'AdmDocFaceRecController@Cluster'));
    Route::get('/admdocfacerec/regin-docs/{s}', array('uses' => 'AdmDocFaceRecController@ReginDocs'));
    Route::get('/admdocfacerec/area-docs/{s}', array('uses' => 'AdmDocFaceRecController@AreaDocs'));
    Route::get('/admdocfacerec/rep-docs/{s}', array('uses' => 'AdmDocFaceRecController@RepDocs'));
    Route::get('/admdocfacerec/bigdoc-docs/{s}', array('uses' => 'AdmDocFaceRecController@BigdocDocs'));
    //更新签到积分
    Route::get('/admdocfacerec/update-sign-in/{id}', array('uses' => 'AdmDocFaceRecController@UpdateSignIn'));
    //周报告
    Route::get('/admdocfacerec/week-cluster/{id}', array('uses' => 'AdmDocFaceRecController@WeekCluster'));
    Route::get('/admdocfacerec/week-regin-docs/{id}', array('uses' => 'AdmDocFaceRecController@WeekReginDocs'));
    Route::get('/admdocfacerec/week-area-docs/{id}', array('uses' => 'AdmDocFaceRecController@WeekAreaDocs'));
    Route::get('/admdocfacerec/week-rep-docs/{id}', array('uses' => 'AdmDocFaceRecController@WeekRepDocs'));
    Route::get('/admdocfacerec/week-bigdoc-docs/{id}', array('uses' => 'AdmDocFaceRecController@WeekBigdocDocs'));
    //
    Route::get('/admdocfacerec/video', array('uses' => 'AdmDocFaceRecController@Video'));    //视频
    Route::get('/admdocfacerec/add-video', array('uses' => 'AdmDocFaceRecController@AddVideo'));    //新增视频
    Route::post('/admdocfacerec/add-video-do', array('uses' => 'AdmDocFaceRecController@AddVideoDo'));    //新增视频do
    Route::get('/admdocfacerec/edit-video/{id}', array('uses' => 'AdmDocFaceRecController@EditVideo'));    //编辑视频
    Route::post('/admdocfacerec/edit-video-do/{id}',
        array('uses' => 'AdmDocFaceRecController@EditVideoDo'));    //编辑视频do
    Route::get('/admdocfacerec/del-video/{id}', array('uses' => 'AdmDocFaceRecController@DelVideo'));        //删除视频
    Route::get('/admdocfacerec/add-audio', array('uses' => 'AdmDocFaceRecController@AddAudio'));    //新增音频
    Route::post('/admdocfacerec/add-audio-do', array('uses' => 'AdmDocFaceRecController@AddAudioDo'));    //新增音频do
    Route::get('/admdocfacerec/edit-audio/{id}', array('uses' => 'AdmDocFaceRecController@EditAudio'));    //编辑音频
    Route::post('/admdocfacerec/edit-audio-do/{id}',
        array('uses' => 'AdmDocFaceRecController@EditAudioDo'));    //编辑音频do
    Route::get('/admdocfacerec/del-audio/{id}', array('uses' => 'AdmDocFaceRecController@DelAudio'));        //删除音频

    Route::post('/admdocfacerec/upload-video-file', array('uses' => 'AdmDocFaceRecController@UploadVideoFile'));

    Route::post('/admdocfacerec/upload-video-thumb', array('uses' => 'AdmDocFaceRecController@UploadVideoThumb'));
    Route::post('/admdocfacerec/upload-video-banner', array('uses' => 'AdmDocFaceRecController@UploadVideoBanner'));
    Route::post('/admdocfacerec/get-video-url', array('uses' => 'AdmDocFaceRecController@GetVideoUrl'));
    Route::get('/admdocfacerec/review-export/{id}', array('uses' => 'AdmDocFaceRecController@ReviewExport'));
    //清除用户观看某直播的积分
    Route::get('/admdocfacerec/clear-user-score/{id}', array('uses' => 'AdmDocFaceRecController@ClearUserScore'));
    //积分整理
    Route::get('/admdocfacerec/calcu-user-score', array('uses' => 'AdmDocFaceRecController@CalcuUserScore'));
    //移动端注册用户直播录播次数统计
    Route::get('/admdocfacerec/h5user-log', array('uses' => 'AdmDocFaceRecController@H5userLog'));
});

//科研培训
Route::group(array('before' => 'admtrainvideo'), function () {
    Route::get('/admtrainvideo/video', array('uses' => 'AdmTrainVideoController@Video'));    //视频
    Route::get('/admtrainvideo/add-video', array('uses' => 'AdmTrainVideoController@AddVideo'));    //新增视频
    Route::post('/admtrainvideo/add-video-do', array('uses' => 'AdmTrainVideoController@AddVideoDo'));    //新增视频do
    Route::get('/admtrainvideo/edit-video/{id}', array('uses' => 'AdmTrainVideoController@EditVideo'));    //编辑视频
    Route::post('/admtrainvideo/edit-video-do/{id}',
        array('uses' => 'AdmTrainVideoController@EditVideoDo'));    //编辑视频do
    Route::get('/admtrainvideo/del-video/{id}', array('uses' => 'AdmTrainVideoController@DelVideo'));        //删除视频
    Route::post('/admtrainvideo/upload-video-file', array('uses' => 'AdmTrainVideoController@UploadVideoFile'));
});

//app更新
Route::group(array('before' => 'admapp'), function () {
    //名医面对面
    Route::get('/admapp/version', array('uses' => 'AdmAppController@Version'));    //更新列表
    Route::get('/admapp/add-version', array('uses' => 'AdmAppController@AddVersion'));    //新增专家
    Route::post('/admapp/add-version-do', array('uses' => 'AdmAppController@AddVersionDo'));    //新增专家do
    Route::get('/admapp/edit-version/{id}', array('uses' => 'AdmAppController@EditVersion'));    //编辑专家
    Route::post('/admapp/edit-version-do/{id}', array('uses' => 'AdmAppController@EditVersionDo'));    //编辑专家do
    Route::get('/admapp/del-version/{id}', array('uses' => 'AdmAppController@DelVersion'));        //删除专家
});

Route::group(array('before' => 'admauth'), function () {
    //用户管理
    Route::get('/admuser', array('uses' => 'AdmUserController@ListUser'));    //用户列表
    Route::get('/admuser2', array('uses' => 'AdmUserController@ListUser2'));    //用户列表
    Route::get('/admuser/add-user', array('uses' => 'AdmUserController@AddUser'));    //用户新增
    Route::post('/admuser/add-do', array('uses' => 'AdmUserController@AddUserDo'));
    Route::get('/admuser/edit-user/{id}', array('uses' => 'AdmUserController@EditUser'));    //用户编辑
    Route::post('/admuser/edit-do/{id}', array('uses' => 'AdmUserController@EditUserDo'));
    Route::get('/admuser/del-user/{id}', array('uses' => 'AdmUserController@DelUser'));        //删除
    Route::get('/admuser/import-user', array('uses' => 'AdmUserController@ImportUser'));        //导入用户页面
    Route::post('/admuser/import-do', array('uses' => 'AdmUserController@ImportDo'));
    Route::post('/admuser/change-psw/{id}/{psw}', array('uses' => 'AdmUserController@ChangePsw'));        //重置密码
    Route::get('/admuser/update-area', array('uses' => 'AdmUserController@UpdateArea'));            //更新地区缓存
    Route::get('/admuser/update-regin', array('uses' => 'AdmUserController@UpdateRegin'));            //更新大区缓存
    Route::get('/admuser/update-hospital', array('uses' => 'AdmUserController@UpdateHospital'));    //更新医院缓存
    Route::get('/admuser/export-user', array('uses' => 'AdmUserController@ExportUser'));
    Route::get('/admuser/export-user-tel/{id}', array('uses' => 'AdmUserController@ExportUserTel'));
    Route::get('/admuser/export-user-info/{id}', array('uses' => 'AdmUserController@ExportUserInfo'));
    Route::get('/admuser/one-user-info/{id}', array('uses' => 'AdmUserController@OneUserInfo'));
    Route::get('/admuser/update-user-regin-area', array('uses' => 'AdmUserController@UpdateUserReginArea'));
    Route::post('/admuser/update-user-regin-area-do', array('uses' => 'AdmUserController@UpdateUserReginAreaDo'));
    Route::get('/admuser/update-shangwu-user', array('uses' => 'AdmUserController@UpdateShangwuUser'));
    Route::get('/admuser/other', array('uses' => 'AdmUserController@Other'));
    //西区ME用户积分信息导出
    Route::get('/admuser/xqmeexpory', array('uses' => 'AdmUserController@Xqmeexpory'));
    //用户分组
    Route::get('/admuser/list-group', array('uses' => 'AdmUserController@getListGroup'));
    Route::get('/admuser/add-group', array('uses' => 'AdmUserController@getAddGroup'));
    Route::post('/admuser/add-group-do', array('uses' => 'AdmUserController@postAddGroupdo'));
    Route::get('/admuser/edit-group/{id}', array('uses' => 'AdmUserController@getEditGroup'));
    Route::post('/admuser/edit-group-do/{id}', array('uses' => 'AdmUserController@postEditGroupDo'));
    Route::get('/admuser/del-group/{id}', array('uses' => 'AdmUserController@getDelGroup'));

    //我的培训
    Route::get('/admuser/train-list', array('uses' => 'AdmUserController@TrainList'));
    Route::get('/admuser/train-del/{id}', array('uses' => 'AdmUserController@TrainDel'));
    Route::get('/admuser/train-add', array('uses' => 'AdmUserController@TrainAdd'));
    Route::post('/admuser/train-add-do', array('uses' => 'AdmUserController@TrainAddDo'));
    Route::get('/admuser/train-edit/{id}', array('uses' => 'AdmUserController@TrainEdit'));
    Route::post('/admuser/train-edit-do/{id}', array('uses' => 'AdmUserController@TrainEditDo'));
    Route::get('/admuser/train-export/{id}', array('uses' => 'AdmUserController@TrainExport'));
    //调研题目管理

    Route::get('/admsurvey/question-list', array('uses' => 'AdmSurveyController@QuestionList'));
    Route::get('/admsurvey/delete-survey/{iId}', array('uses' => 'AdmSurveyController@DeleteSurvey'));
    Route::get('/admsurvey/edit-survey/{iId}', array('uses' => 'AdmSurveyController@EditSurvey'));
    Route::get('/admsurvey/add-question', array('uses' => 'AdmSurveyController@AddQuestion'));
    Route::get('/admsurvey/do-add', array('uses' => 'AdmSurveyController@DoAdd'));
    Route::get('/admsurvey/add-choice/{iId}/{phase_id}', array('uses' => 'AdmSurveyController@AddChoice'));
    Route::get('/admsurvey/do-add-choice', array('uses' => 'AdmSurveyController@DoAddChoice'));
    Route::get('/admsurvey/has-choice-or-not/{iId}/{phase_id}', array('uses' => 'AdmSurveyController@HasChoiceOrNot'));
    Route::get('/admsurvey/search-survey', array('uses' => 'AdmSurveyController@SearchSurvey'));
    Route::get('/admsurvey/do-adds', array('uses' => 'AdmSurveyController@DoAdds'));
    Route::get('/admsurvey/survey-result', array('uses' => 'AdmSurveyController@SurveyResult'));

    //旧版我的预约
    Route::get('/admuser/order', array('uses' => 'AdmUserController@Order'));
    Route::get('/admuser/export-order', array('uses' => 'AdmUserController@ExportOrder'));

    //新版我的预约
    Route::get('/admuser/neworder', array('uses' => 'AdmUserController@NewOrder'));
    Route::get('/admuser/neworder/adddoc', array('uses' => 'AdmUserController@NewOrderAddDoc'));
    Route::post('/admuser/neworder/adddocdo', array('uses' => 'AdmUserController@NewOrderAddDocDo'));
    Route::get('/admuser/neworder/editdoc/{id}', array('uses' => 'AdmUserController@NewOrderEditDoc'));
    Route::post('/admuser/neworder/editdocdo/{id}', array('uses' => 'AdmUserController@NewOrderEditDocDo'));
    Route::get('/admuser/del-order-doc/{id}', array('uses' => 'AdmUserController@DelOrderDoc'));

    //病例后台权限
    Route::post('/admuser/user-admin-case/{uid}/{type}', array('uses' => 'AdmUserController@UserAdminCase'));

    //专家大会诊
    Route::controller('admexpert', 'AdmExpertController');
    Route::get('/admexpert/userinfo', array('uses' => 'AdmExpertController@getUserinfo'));

    //显示redislog
    Route::get('/log-redis/{id}', array('uses' => 'AdminController@LogRedis'));
    //清空redislog
    Route::get('/clear-log-redis/{id}', array('uses' => 'AdminController@ClearLogRedis'));

    //20150724直播录播点击量统计
    Route::get('/export-video-count', array('uses' => 'AdmDocFaceController@ExportVideoCount'));

    Route::get('/flush-all', array('uses' => 'AdminController@FlushAll'));

    //*************20151221调研栏目后台新增功能路由 start ****************
    Route::get('/admuser/user_rep_relate', array('uses' => 'AdmUserController@UserRepRelate'));
    Route::get('/admuser/user_medlive_relate', array('uses' => 'AdmUserController@UserMedliveRelate'));
    Route::get('/admuser/user_medlive_reg', array('uses' => 'AdmUserController@UserMedliveReg'));
    Route::get('/admuser/offline-list', array('uses' => 'AdmUserController@getOfflineList'));
    Route::get('/admuser/offline-add', array('uses' => 'AdmUserController@getOfflineAdd'));
    Route::Controller('admelearning', 'AdmElearningController');//e-learning
    //*************20151221调研栏目后台新增功能路由 end ****************

    //20160307新增路由--更新医生关联代表
    Route::get('/admuser/update-doc-link-rep', array('uses' => 'AdmUserController@getUpdateDocLinkRep'));
    Route::post('/admuser/update-doc-link-rep-do', array('uses' => 'AdmUserController@postUpdateDocLinkRepDo'));

    //***************阿司匹林专项基金 start****************
    Route::Controller('admaspirineducation', 'AdmAspirinEducationController');
    Route::Controller('admaspirinonline', 'AdmAspirinOnlineController');
    Route::controller('admaspirininfo', 'AdmAspirinInfoController');            //专项基金轮播图
    Route::controller('admaspirinresearch', 'AdmAspirinResearchController');    //专项基金科研申请
    Route::controller('admaspirinscreening', 'AdmAspirinScreeningController');  //专项基金筛查
    Route::controller('admaspirinoffline', 'AdmAspirinOfflineController');      //个人中心——线下会议
    Route::Controller('admaspirinuser', 'AdmAspirinUserController');   //医师认证审核
    Route::Controller('admaspirin', 'AdmAspirinController');         //专项基金周报导出
    //***************阿司匹林专项基金 end****************
});
//自助会议平台后台
Route::get('/admmeeting/meeting-detail/{id}', array('uses' => 'AdmMeetingController@MeetingDetail'));
Route::get('/admmeeting', array('uses' => 'AdmMeetingController@MeetingList'));
Route::get('/admmeeting/open-meeting/{id}', array('uses' => 'AdmMeetingController@OpenMeeting'));
Route::get('/admmeeting/meeting-message/{id}', array('uses' => 'AdmMeetingController@MeetingMessage'));
// 会议管理后台
Route::get('/admmeeting-manager', array('uses' => 'AdmMeetingManagerController@MeetingList'));
Route::get('/admmeeting-manager/create', array('uses' => 'AdmMeetingManagerController@MeetingCreate'));
Route::get('/admmeeting-manager/edit/{id}', array('uses' => 'AdmMeetingManagerController@MeetingEdit'));
Route::post('/admmeeting-manager/create-do', array('uses' => 'AdmMeetingManagerController@doMeetingCreate'));
Route::post('/admmeeting-manager/edit-do/{id}', array('uses' => 'AdmMeetingManagerController@doMeetingEdit'));
Route::get('/admmeeting-manager/delete/{id}', array('uses' => 'AdmMeetingManagerController@DeleteMetting'));
Route::get('/admmeeting-manager/sign-list/{id}', array('uses' => 'AdmMeetingManagerController@SignMettingList'));
Route::get('/admmeeting-manager/material', array('uses' => 'AdmMeetingManagerController@MaterialList'));
Route::get('/admmeeting-manager/upload', array('uses' => 'AdmMeetingManagerController@MaterialUpload'));
Route::get('/admmeeting-manager/edit-material/{id}', array('uses' => 'AdmMeetingManagerController@MaterialEdit'));
Route::get('/admmeeting-manager/delete-material/{id}', array('uses' => 'AdmMeetingManagerController@MaterialDelete'));
Route::post('/admmeeting-manager/edit-material-do/{id}', array('uses' => 'AdmMeetingManagerController@doMaterialEdit'));
Route::post('/admmeeting-manager/upload-material', array('uses' => 'AdmMeetingManagerController@MaterialUploadFile'));
Route::post('/admmeeting-manager/upload-do', array('uses' => 'AdmMeetingManagerController@doMaterialUpload'));

//前台路由
Route::get('login', array('uses' => 'HomeController@Login'));
Route::get('/meeting-auto-sms', array('uses' => 'HomeController@MeetingAutoSms'));
Route::get('/cancelmeeting', array('uses' => 'HomeController@CancelMeeting'));
Route::get('/meetingbeforetenday/{day}', array('uses' => 'HomeController@MeetingbeforeTenday'));//10/8短信提醒，删除七天未确定的会议
Route::get('/notsuccessappoint', array('uses' => 'HomeController@NotsuccessAppoint'));//七天预约失败
Route::get('/survey/question/{id}', array('uses' => 'SurveyController@Question'));
Route::get('register', array('uses' => 'HomeController@Register'));
Route::post('register-code/{code}', array('uses' => 'HomeController@RegisterCode'));
Route::post('register-do/{id?}', array('uses' => 'HomeController@RegisterDo'));
Route::post('/user/upload-user-first-thumb', array('uses' => 'UserController@UploadUserFirstThumb'));
Route::post('/user/upload-user-first-thumb_certificate',
    array('uses' => 'UserController@UploadUserFirstThumbcertificate'));
Route::post('/user/upload-user-card-thumb', array('uses' => 'UserController@UploadUserCardThumb'));
Route::any('/user/register-check/{nick}/{tel}/{email?}', array('uses' => 'HomeController@RegisterCheck'));
Route::post('/user/area-info/{rid}', array('uses' => 'UserController@AreaInfo'));
Route::post('login', array('uses' => 'HomeController@LoginDo'));
Route::get('logout', array('uses' => 'HomeController@LoginOut'));
Route::get('/', array('uses' => 'HomeController@Index'));
Route::post('/user/hospital-child/{pid}', array('uses' => 'UserController@HospitalChild'));
Route::get('/forget', array('uses' => 'HomeController@Forget'));    //忘记密码
Route::post('/forget', array('uses' => 'HomeController@postForget'));    //忘记密码
Route::post('/agree-disclaimer', array('uses' => 'HomeController@postAgreeDisclaimer'));    //同意免责条款

Route::get('/user/resetpwd', array('uses' => 'HomeController@ResetPwd'));    //找回密码
Route::post('/user/resetpwd', array('uses' => 'HomeController@ResetPwdDo'));    //找回密码--提交新密码

Route::any('/code', array('uses' => 'HomeController@getCode'));
Route::any('/auth-code', array('uses' => 'HomeController@AuthCode'));
//名医面对面
Route::get('/docface', array('uses' => 'DocFaceController@Index'));    //专家
Route::get('/docface/review/{id?}', array('uses' => 'DocFaceController@Review'));    //回顾列表
Route::get('/docface/recview/{id?}', array('uses' => 'DocFaceController@Recview'));    //精品课程
Route::get('/docface/doc-list', array('uses' => 'DocFaceController@DocList'));        //专家风采列表
Route::get('/docface/doc-show/{id}', array('uses' => 'DocFaceController@DocShow'));        //专家风采详细
//加麦粒未成功做的定制任务
Route::get('/fail-gold-repeat', array('uses' => 'HomeController@getFailGoldRepeat'));
Route::post('/docface/live_score', array('uses' => 'DocFaceController@LiveScore'));
//登录验证
Route::group(array('before' => 'auth'), function () {
    Route::get('/docface/videoauditauto', array('uses' => 'DocFaceController@Videoauditauto'));
    //名医面对面
    Route::get('/docface/enter-live/{id}', array('uses' => 'DocFaceController@EnterLive'));
    Route::get('/docface/review-show/{id}', array('uses' => 'DocFaceController@ReviewShow'));
    Route::get('/docface/recview-show/{id}', array('uses' => 'DocFaceController@RecviewShow'));
    Route::post('/docface/video_zan/{vid}/{uid}/{cid}', array('uses' => 'DocFaceController@VideoZan'));
    Route::post('/docface/video_score', array('uses' => 'DocFaceController@VideoScore'));
    Route::post('/docface/video_view/{vid}/{uid}/{min}/{device?}',
        array('uses' => 'DocFaceController@VideoView'));    //录播
    Route::post('/docface/video_view_live/{vid}/{uid}/{s}/{device?}',
        array('uses' => 'DocFaceController@VideoViewLive'));    //直播
    Route::post('/docface/comment-info/{id}/{cid}/{device?}', array('uses' => 'DocFaceController@CommentInfo'));
    Route::post('/docface/comment_zan/{comid}/{cid?}', array('uses' => 'DocFaceController@CommentZan'));
    //资讯中心
    Route::Controller('information', 'InformationController');
    //个人中心
    Route::get('/user/mydoclist', array('uses' => 'UserController@MyDocList'));                        //我的医生
    Route::get('/user/mydocviewlog', array('uses' => 'UserController@MyDocViewLog'));                    //医生的视频观看记录
    Route::get('/user/myauthdoclist', array('uses' => 'UserController@MyAuthDocList'));                //我的讲者
    Route::get('/user/myauthdocclass', array('uses' => 'UserController@MyAuthDocClass'));                //医生的视频观看记录
    Route::get('/user/myauthdocclassdetail', array('uses' => 'UserController@MyAuthDocClassDetail'));    //医生的视频观看记录
    Route::get('/user/add-doclist', array('uses' => 'UserController@addDoctorList'));                    // 添加我的医生列表
    Route::post('/user/do-add-doclist', array('uses' => 'UserController@addDoctorList'));
    Route::post('/user/do-remove-doctor', array('uses' => 'UserController@doRemoveDoctor'));

    Route::get('/user', array('uses' => 'UserController@Index'));
    Route::post('/user/video-view/{iVideoId}/{iUserId}/{iMin}', array('uses' => 'UserController@VideoView'));
    Route::get('/user/user-info-change', array('uses' => 'UserController@UserInfoChange'));    //修改用户信息页面
    Route::post('/user/chenge-user-info/{unick}/{oldpsw?}/{newpsw?}', array('uses' => 'UserController@ChengeUserInfo'));
    Route::post('/user/upload-user-thumb', array('uses' => 'UserController@UploadUserThumb'));

    Route::get('/user/my-score', array('uses' => 'UserController@MyScore'));                            //我的积分
    Route::get('/user/my-health-score', array('uses' => 'UserController@MyHealthScore'));                //我的积分
    Route::get('/user/health-score-list/{r}', array('uses' => 'UserController@HealthScoreList'));        //我的积分
    Route::get('/user/use-score', array('uses' => 'UserController@UseScore'));                            //积分兑换页面
    Route::get('/user/integral-do', array('uses' => 'UserController@Integraldo'));                            //积分兑换页面


    Route::get('/user/my-order', array('uses' => 'UserController@MyOrder'));                    //我的预约
    Route::get('/user/order-now', array('uses' => 'UserController@OrderNow'));                    //马上预约
    Route::post('/user/order-now-do/{t}/{n}/{d}', array('uses' => 'UserController@OrderNowDo'));        //马上预约do
    Route::get('/user/score-list/{r}', array('uses' => 'UserController@ScoreList'));        //积分排行
    Route::get('/user/score-list2/{r}', array('uses' => 'UserController@ScoreList2'));        //积分排行
    Route::post('/user/change-rep-info/{rid}/{aid}', array('uses' => 'UserController@ChangeRepInfo'));
    Route::post('/user/change-doc-city-info/{pid}/{cid}/{couid}/{hid}/{dname}/{cname?}',
        array('uses' => 'UserController@ChangeDocCityInfo'));
    Route::post('/user/change-doc-pos-info/{pname}', array('uses' => 'UserController@ChangeDocPosInfo'));
    Route::post('/user/change-rep-city-info/{pid}/{cid}', array('uses' => 'UserController@ChangeRepCityInfo'));
    Route::get('/user/caselist/{id}', array('uses' => 'CaseController@getCaseList'));

    //新版我的预约--预约功能
    Route::post('/user/order-do/{docid}', array('uses' => 'UserController@OrderDo'));
    Route::post('/user/change-user-info-all', array('uses' => 'UserController@ChangeUserInfoAll'));//修改个人信息
    Route::get('/user/my-train', array('uses' => 'UserController@MyTrain'));        //我的培训
    Route::get('/user/enter-train/{id}', array('uses' => 'UserController@EnterTrain'));    //进入我的培训
    Route::get('/user/train-review/{id}', array('uses' => 'UserController@TrainReview'));    //进入我的培训录播
    //专家大会诊
    Route::get('/consultation', array('uses' => 'ConsultationController@Index'));    //首页
    Route::get('/consultation/list', array('uses' => 'ConsultationController@getList'));
    Route::get('/consultation/show/{id}', array('uses' => 'ConsultationController@getShow'));
    Route::post('/consultation/apply', array('uses' => 'ConsultationController@Apply'));
    Route::get('/consultation/enter/{vid}/{type}', array('uses' => 'ConsultationController@Enter'));        //观看直播


    //病例征集
    Route::get('/case/rule', array('uses' => 'CaseController@Rule'));//征集规则
    Route::get('/case/upload', array('uses' => 'CaseController@getCaseUpload'));//病例上传
    Route::post('/case/upload', array('uses' => 'CaseController@postCaseUpload'));//病例上传
    Route::get('/case/edit/{id}', array('uses' => 'CaseController@getCaseEdit'));//病例修改
    Route::post('/case/edit', array('uses' => 'CaseController@postCaseEdit'));//病例修改
    Route::get('/case/list/{id}', array('uses' => 'CaseController@getCaseList'));//经典病例
    Route::get('/case/show/{id}', array('uses' => 'CaseController@getCaseShow'));//病例详细
    Route::get('/case/showInfo/{id}', array('uses' => 'CaseController@getCaseShowInfo'));//病例详细
    Route::get('/case/e-list', array('uses' => 'CaseController@getCaseExplainList'));//病例解读
    Route::get('/case/e-show/{id}', array('uses' => 'CaseController@getCaseExplainShow'));//病例解读
    Route::post('/case/vote', array('uses' => 'CaseController@postCaseVote'));//病例解读

    Route::post('/case/uploadFile', array('uses' => 'CaseController@uploadFile'));
    Route::post('/case/uploadFileEdit/{id}', array('uses' => 'CaseController@uploadFileEdit'));
    Route::post('/case/uploadPic/{name}', array('uses' => 'CaseController@uploadPic'));
    Route::post('/case/uploadAtta', array('uses' => 'CaseController@uploadAtta'));
    Route::get('/case/downModel', array('uses' => 'CaseController@downModel'));
    Route::get('/case/downCase/{id}', array('uses' => 'CaseController@downCase'));
    Route::get('/case/experts', function () {
        return View::make('front.case.experts');
    });

    //下载视频统计通用
    Route::post('/download-video/{catid}/{videoid}', array('uses' => 'HomeController@DownloadVideo'));
    //******************20151221前台新增路由 start*********************
    Route::get('/user/mydoc', array('uses' => 'UserController@MyDoc'));
    Route::post('/user/doc-type-do', array('uses' => 'UserController@DocTypeDo'));
    Route::post('/user/doc-push-survey-do/{docid}', array('uses' => 'UserController@DocPushSurveyDo'));
    Route::post('/user/survey-ajax-submit', array('uses' => 'UserController@SurveyAjaxSubmit'));
    Route::post('/user/agree-medlive-reg/{flag}', array('uses' => 'UserController@AgreeMedliveReg'))->where('flag',
        '[1-3]');
    Route::get('/user/agree-reg-medlive-submit', array('uses' => 'UserController@AgreeRegMedliveSubmit'));
    Route::post('/user/survey-close', array('uses' => 'UserController@SurveyClose'));//用户关闭了调研弹框

    Route::Controller('pmd', 'PubmedController');
    Route::Controller('cnsearch', 'CnsearchController');
});
//***************pc端专项基金 start**********************
Route::Controller('aspirin', 'AspirinController');                    //专项基金首页
Route::Controller('aspirinoffline', 'AspirinOfflineController');      //线下会议（个人中心）
Route::group(array('before' => 'aspirinpcauth'), function () {
    Route::Controller('aspirinonline', 'AspirinOnlineController');        //在线会议
    Route::Controller('aspirinresearch', 'AspirinResearchController');    //科研培训
});
//***************pc端专项基金   end**********************

Route::group(array('before' => 'meetingauth'), function () {
    //******************20151221前台新增路由 start*********************
    Route::get('/meeting/watch-meeting/{id}', array('uses' => 'MeetingController@WatchMeeting'));//自助会议平台
    Route::get('/meeting/test', array('uses' => 'MeetingController@Test'));
    Route::get('/meeting/create', array('uses' => 'MeetingController@Index'));//自助会议平台——创建会议
    Route::post('/meeting/create-do', array('uses' => 'MeetingController@CreateDo'));
    Route::get('/mymeeting', array('uses' => 'MeetingController@MyMeeting'));//自助会议平台——我的会议
    Route::get('/meeting/edit/{id}', array('uses' => 'MeetingController@Edit'));//自助会议平台——编辑会议
    Route::post('/meeting/edit-do/{id}', array('uses' => 'MeetingController@EditDo'));
    Route::post('/meeting/delete/{id}', array('uses' => 'MeetingController@Delete'));//自助会议平台——取消会议
    Route::post('/meeting/ajax', array('uses' => 'MeetingController@Ajax'));
    Route::get('/mymeeting/order-list/{id}', array('uses' => 'MeetingController@OrderList'));//自助会议平台——查看预约
    Route::post('/mymeeting/order-do', array('uses' => 'MeetingController@OrderDo'));//自助会议平台——接受预约
    Route::get('/joinmeeting', array('uses' => 'MeetingController@JoinMeeting'));//自助会议平台——加入会议
    Route::post('/mymeeting/enter-do', array('uses' => 'MeetingController@EnterDo'));//自助会议平台——主持人进入会议
    Route::post('/mymeeting/join-do', array('uses' => 'MeetingController@JoinDo'));//自助会议平台——参会方进入会议
    //自助会议平台-----会议预约
    Route::post('/meeting/appoint', array('uses' => 'MeetingController@Appoint'));//预约会议
    Route::post('/meeting/makesure', array('uses' => 'MeetingController@Makesure'));//确定用户是否已经预约该会议
    Route::post('/meeting/delete-appointment', array('uses' => 'MeetingController@DeleteAppointment'));//取消预约
    Route::get('/meeting/appoint-list', array('uses' => 'MeetingController@AppointList'));//会议列表
    Route::get('/meeting/meeting-detail/{id}', array('uses' => 'MeetingController@MeetingDetail'));//会议详情
    //Route::get('/meeting/delete-appointment/{id}/{name}',array('uses'=>'MeetingController@DeleteAppointment'));//取消会议
    Route::get('/meeting/has-appointed-meeting', array('uses' => 'MeetingController@HasAppointedMeeting'));//已经预约的会议列别
    Route::get('/meeting/appoint-message/{id}', array('uses' => 'MeetingController@AppointMessage'));//预约表单页
    //------------------------//
});

//手机端调研
Route::get('/docface/survey-pre/{id}', array('uses' => 'DocFaceController@SurveyPre'));
Route::get('login-mobile', array('uses' => 'HomeController@LoginMobile'));
Route::post('login-mobile', array('uses' => 'HomeController@LoginMobileDo'));
//手机注册
Route::get('/docface/survey-register/{code?}', array('uses' => 'HomeController@RegisterMobile'));
Route::post('/docface/survey-register/{code}', array('uses' => 'HomeController@RegisterMobileDo'));
//线下会议注册
Route::get('/docface/survey-registernewuser/{code?}/{meetingcode?}',
    array('uses' => 'HomeController@RegisterMobilenewuser'));
Route::post('/docface/survey-registernewuser/{code?}/{meetingcode?}',
    array('uses' => 'HomeController@RegisterMobileDonewuser'));
Route::get('/docface/survey/{id}', array('uses' => 'DocFaceController@Survey'));
Route::post('/docface/survey/{id}', array('uses' => 'DocFaceController@SurveyDo'));
// 
Route::get('/docface/offline-sign/{MeetingCode?}',
    array('uses' => 'DocFaceController@OfflineSign'));       //线下会议签到不限次数  20170502 dll
Route::post('/docface/offline-sign-do', array('uses' => 'DocFaceController@OfflineSignDo')); //线下会议签到不限次数  20170502 dll


Route::get('apihotline', array('uses' => 'ApiUserController@getHotline'));

//获取版本
Route::get('apiversion', array('uses' => 'ApiVersionController@getVersion'));

Route::Controller('apivideo', 'ApiVideoController');//名医面对面接口
Route::Controller('apitrainvideo', 'ApiTrainVideoController');//科研培训接口
//liuying   API    S===============================
Route::Controller('apiuser', 'ApiUserController');
//liuying   API    S===============================
Route::Controller('apicon', 'ApiConsultationController');//名医面对面接口
//guoqing   /API
Route::Controller('apiinformation', 'ApiInformationController');//调研接口
Route::Controller('apihelp', 'ApiHelpController');//全文求助接口
Route::Controller('apipmd', 'ApiPubmedController');//pubmed检索
Route::Controller('apicnsearch', 'ApiCnsearchController');//pubmed接口
Route::Controller('survey', 'SurveyController');//20160201小调研（平稳达标&早起达标）
Route::Controller('apimaterial', 'ApiMaterialController');//资讯中心微信订阅号  dll


//***********阿司匹林专项基金 start*****************
Route::Controller('apiaspirineducation', 'ApiAspirinEducationController');
Route::Controller('apiaspirininfo', 'ApiAspirinInfoController');
Route::Controller('apiaspirinresearch', 'ApiAspirinResearchController');
Route::Controller('apiaspirinscreening', 'ApiAspirinScreeningController');
Route::Controller('apiaspirinoffline', 'ApiAspirinOfflineController');
Route::Controller('apiaspirinonline', 'ApiAspirinOnlineController');
//分享
Route::Controller('aspirinshare', 'AspirinShareController');
//***********阿司匹林专项基金   end *****************

//******************我的医生 start********************
Route::Controller('apimy', 'ApiMyController');
//******************我的医生 end********************

//******************会议管理 start********************
Route::Controller('apimeeting', 'ApiMeetingController');
//******************会议管理 end********************

//***************移动端 start**********************
Route::Controller('mobile', 'MobileController');
Route::Controller('mobile-docface', 'MobileDocFaceController');
Route::Controller('mobile-user', 'MobileUserController');
Route::Controller('mobile-mytrain', 'MobileMyTrainController');
Route::Controller('mobile-aspirin', 'MobileAspirinController');
Route::Controller('mobile-aspirin-online', 'MobileAspirinOnlineController');
Route::Controller('aspirinfund', 'MobileAspirinFundController');
//***************移动端 end**********************

//更新图标显示
Route::Controller('apiupdateflag', 'ApiUpdateFlagController');
//测试接口
Route::Controller('apidoc', 'ApiDocController');
Route::get('/test-add-score', array('uses' => 'HomeController@TestAddScore'));
Route::get('/test-score-list/{r}', array('uses' => 'HomeController@TestScoreList'));

Route::get('/{other?}', array('uses' => 'HomeController@Index'));
Route::Controller('test', 'TestController');

//Event::listen("illuminate.query", function($query, $bindings, $time, $name){
//  //$queries = DB::logQuery($query, $bindings, $time) ;
//  echo "<pre>";
//  var_dump($query);
////echo "</pre>";
//var_dump($bindings);
////echo "</br>";
// 
//});
