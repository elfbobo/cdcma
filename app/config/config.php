<?php
return array(
		'department'=>array(
						1=>'内分泌科',
							
						2=>'心血管内科',
							
						3=>'神经内科',
							
						4=>'肾病内科',
				
						5=>'社区'
					),
		'rooturl' => 'http://cdcma.bizconf.cn',

		'cdn_url' => '',
		
		//前台是否开启调研，0：否；1：是；
		'is_survey_open'		=> 0,
		
		//当前开启的是哪一期的调研
		'phase_id'				=> 1,	
		
		//注册医脉通的app_name
		'medlive_reg_app_name'	=> 'cdcma.bizconf.cn',
		
		//选项
		'options'				=>array('A','B','C','D','E','F','G','H','I','J','K','L'),
		
		//资讯管理权限     
		'information'           =>array(1612),
		
		//小调研管理权限
		'research'           =>array(18),
		
		//名医面对面、专家 权限
		'docface'           =>array(37310),
		
		//医脉通麦粒typeid
		'survey_gold_type'		=> 1255,
		
		//参与调研奖励麦粒数
		'survey_gold'			=> 0,
		
		//调研限制麦粒份数
		'survey_gold_set'		=> 0,		
		
		//大区属于ME类型的大区id数组
		'regin_me'				=> array(9,16,17,28,29,30,31,32,33),	
		
		//是否开通全文求助功能(0：关闭；1：开通)
		'is_pub_email_open'		=> 0,
		
		//接收文献求助的邮箱
		'pubmed_email'			=> '760155427@qq.com',
		
		//*******************自助会议平台配置项 start*****************
		//配置每场会最多几个人预约
		'self_help_meeting_order_set'	=> 5,
		
		//配置可以几方参与该会议，为“1”表示只有一个人可以和创建者一起参与会议
		'self_help_meeting_attent_set'	=> 10,
		
		//会畅接口————预约会议
		'hc_api_url'           => 'http://api.confcloud.cn/openapi/confReservation',
		'hc_api_url_v2'        => 'http://api.bizvideo.cn/openapi/confReservation',
		//会畅接口————企业分配的密钥md5key
		'hc_api_key'       =>  '021502088497e9f81af54e7506096abb',
					
		//配置时间段对应的标记数值（一个标记值代表一个色块）			
		'time_code_flag' =>  array('61','62','71','72','81','82','91','92','101','102',
									'111','112','121','122','131','132','141','142','151','152',
									'161','162','171','172','181','182','191','192','201','202','211','212'),
		//*******************自助会议平台配置项 end*******************
		
		'medlive_url'	=> 'http://www.medlive.cn',

		// ios是否审核
		'ios_check' => 1
	);