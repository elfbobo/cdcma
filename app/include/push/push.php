<?php
set_time_limit(0);
require '../include/common.inc.php';
require 'class/push.class.php';
require 'class/Data.class.php';
require 'class/RunPush.class.php';
require 'lib/function.lib.php';
require 'config/config.php';
$aIntParam = array(
 					'_push_url'=>PUSH_URL,
 					'_domain'=>PUSH_DOMAIN, 
 					'_title'=>'',
 					'_devicetype'=>DEVICETYPE_OTHER,
 					'_key'=>AUTH_KEY_ZZK,
					'_sendtype'=>SENDTYPE_PUSH,
//'_iosurl'=>'www.baidu.com',
//'_androidurl'=>'www.baidu.com'
);

$data = Data::GetUsers();
$push = new Push('post' ,$aIntParam);

$userid = array('zzk'=>array(),'scyy'=>array());
$apnstokens = array('zzk'=>array(),'scyy'=>array());

$aPushData = array();
foreach ($data as $key=>$val){
	if( IsInTime($val['inputtime'] , MIN_TIME , MAX_TIME  )){

		if($val['p_type'] == P_TYPE_ZZK){
	   
			// echo "zzk推送通知 </br>";

			($val['apns_token'] && $val['m_type'] =='ios' ) ?  $apnstokens['zzk'][] = $val['apns_token'] : '';

			($val['apns_token'] && $val['m_type'] =='android' ) ?  $userid['zzk'][] = $val['apns_token'] : '';
			 
		}
		if($val['p_type'] == P_TYPE_SCYY){
	   
			//  echo "scyy推送通知 </br>";
			($val['apns_token'] && $val['m_type'] =='ios' ) ?  $apnstokens['scyy'][] = $val['apns_token'] : '';

			($val['apns_token'] && $val['m_type'] =='android' ) ?  $userid['scyy'][] = $val['apns_token'] : '';

		}
	}else{

		unset($data[$key]);
	}
}
/**
 *
 *掌心控（安卓版）	cn.medlive.palmheart
 *神彩EE（安卓版） 	cn.medlive.palmneuro

 *神采奕奕 	com.kingyee.inhouse.shencaiyiyi
 *掌心控 	com.kingyee.inhouse.zhangxinkong

 */
$title = '';
$msg = '试用期即将结束，快攒积分升级吧!';
$msg_email = '您的文献求助结果已发送至邮箱，请查收';
if( $apnstokens['zzk'] ){
	$setting = array('appid'=>PUSH_APPID_IOS_ZZK,'user_id'=>'','title'=>$title,'content'=>$msg,'apnstokens'=> $apnstokens['zzk']);
	runPost($push , $setting );
}
if( $apnstokens['scyy'] ){

	$setting = array('key'=>AUTH_KEY_SCYY,'appid'=>PUSH_APPID_IOS_SCYY,'user_id'=>'','title'=>$title,'content'=>$msg,'apnstokens'=> $apnstokens['zzk']);
	runPost($push , $setting );

}
if( $userid['zzk'] ){
	$setting = array('appid'=>PUSH_APPID_AND_ZZK ,'apnstokens'=>'','user_id'=>$userid['zzk'],'title'=>$title,'content'=>$msg);
	runPost($push , $setting );
}

if($userid['scyy']){
	$setting = array('key'=>AUTH_KEY_SCYY,'appid'=>PUSH_APPID_AND_SCYY ,'apnstokens'=>'','user_id'=>$userid['scyy'],'title'=>$title,'content'=>$msg);
	runPost($push , $setting );
}

$Helpdata = Data::getUsersHelpPNums();

foreach ($Helpdata as $key=>$val){
	if($val['p_type'] == P_TYPE_ZZK){
		 
		if($val['apns_token'] && $val['m_type'] =='ios' ){
				
			$setting = array('key'=>AUTH_KEY_SCYY,'appid'=>PUSH_APPID_IOS_ZZK,'user_id'=>'','apnstokens'=>$val['apns_token'],'content'=>$msg_email);
				
			runPost($push , $setting );
				
			Data::updateUserHelpNum ($val['userid']);
		}
			
		if( ($val['apns_token'] && $val['m_type'] =='android')||( $val['m_type'] =='' && $val['apns_token'] && preg_match('/^[1-9]\d*$/',$val['apns_token']))){
				
			$setting = array('key'=>AUTH_KEY_SCYY,'appid'=>PUSH_APPID_AND_ZZK,'user_id'=>$val['apns_token'],'apnstokens'=>'','content'=>$msg_email);
				
			runPost($push , $setting );
				
			Data::updateUserHelpNum ($val['userid']);
		}
	}
	if($val['p_type'] == P_TYPE_SCYY){
		 
		if( $val['apns_token'] && $val['m_type'] =='ios' ){

			$setting = array('key'=>AUTH_KEY_SCYY,'appid'=>PUSH_APPID_IOS_SCYY,'user_id'=>'','apnstokens'=>$val['apns_token'],'content'=>$msg_email);

			runPost($push , $setting );
			Data::updateUserHelpNum ($val['userid']);

		}

		if( ($val['apns_token'] && $val['m_type'] =='android') || ( $val['m_type'] =='' && $val['apns_token'] && preg_match('/^[1-9]\d*$/',$val['apns_token'])) ){

			$setting = array('key'=>AUTH_KEY_SCYY,'appid'=>PUSH_APPID_AND_SCYY,'user_id'=>$val['apns_token'],'apnstokens'=>'','content'=>$msg_email);

			runPost($push , $setting );
			Data::updateUserHelpNum ($val['userid']);
		}

	}
}
