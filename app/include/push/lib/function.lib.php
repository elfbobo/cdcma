<?php

function  IsInTime($inputtime , $min , $max  ){
	$time = time();
	if ( ( ($time - $inputtime ) > $min ) && ( ( $time - $inputtime ) < $max ) )  {
		
		return true;
	}else{
		
		return  false;
	}
}

function post( $url , $data ){
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $surl);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	ob_start();
	curl_exec($ch);
	$out = ob_get_clean();
	curl_close($ch);
	
	
	return $out;
}


function runPost($obj ,$setting ,$aParam = array(),$isInit = FALSE){
	
	include_once  ROOT.'/app/include/push/class/RunPush.class.php';
	try {
		return RunPush::RunPost( $obj , $setting ,$aParam ,$isInit);
		
		
	}catch (Exception $e){
		
		exit( $e->getMessage());
	}
}

function pushMsg($data = array(),$case = '', $push ,$msg = '',$title = ''){
	
	if( $data && $case && is_object( $push ) && $msg){
		
		
		switch ($case){
			
			case 'zzk_ios':
				
				$setting = array('appid'=>PUSH_APPID_IOS_ZZK,'user_id'=>'','title'=>$title,'content'=>$msg,'apnstokens'=> $data);
				
				break;
			case 'zzk_and':
				
				$setting = array('appid'=>PUSH_APPID_AND_ZZK ,'apnstokens'=>'','user_id'=>$data,'title'=>$title,'content'=>$msg);
				break;
			case 'scyy_ios':
				
				$setting = array('appid'=>PUSH_APPID_IOS_SCYY ,'apnstokens'=>$data,'user_id'=>'','title'=>$title,'content'=>$msg);
				break;
			case 'scyy_and':
				
				$setting = array('appid'=>PUSH_APPID_AND_SCYY ,'apnstokens'=>'','user_id'=>$data,'title'=>$title,'content'=>$msg);
				break;			
		}	
		
	    runPost($push , $setting );
			
	}else{
		
		return false;
	}
	
	
	
	
}