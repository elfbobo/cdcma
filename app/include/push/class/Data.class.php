<?php

class Data{

	private  static  function  _SelectUser(){


		global $db;
			
		$sql = "SELECT
			 				userid,
			 				m_type,
			 				p_type,
			 				inputtime,
			 				apns_token
			 		    FROM
			 		         `snf_member` 	
			 		    WHERE
			 		       apns_token <> ''	       		
			 	";
			
		if( $result = $db->select( $sql ) ){

			return $result;

		}else{

			throw new Exception('db select is error  OR result isempty . Please check the Data ......');
		}
	}

	public static function GetUsers(){

			
		try {

			$userdata = self::_SelectUser();
				
		}catch ( Exception $ex){

				
			exit($ex->getmessage());
		}

		return $userdata;
	}

	private static function _UpdateUser( $userid ){

		global  $db;

		$sql = "UPDATE
					  `snf_member`
                SET
                      is_help_num = 0
                WHERE
                      userid = $userid
				";
		if( $result = $db->query( $sql ) ){

			return $result;

		}else{

			throw new Exception('db is error  ......');
		}
	}


	public static function updateUserHelpNum( $userid ){

		try {
				
			$re = self::_UpdateUser( $userid );

		}catch ( Exception $ex){
				
			exit($ex->getmessage());
		}

		return $re;

	}



	private static function _getUsersHelpPNums(){


		global $db;
			
		$sql = "SELECT
			 				userid,
			 				m_type,
			 				p_type,
			 				apns_token,
			 				is_help_num
			 		    FROM
			 		         `snf_member`
			 		    WHERE
			 		         is_help_num > 0		       		
			 	";	
			
		if( $result = $db->select( $sql ) ){

			return $result;

		}else{

			throw new Exception('db select is error OR result isempty . Please check the Data ......');
		}

	}

	public static function getUsersHelpPNums(){


		try {
				
			$data = self::_getUsersHelpPNums();

		}catch ( Exception $ex){
				
			exit($ex->getmessage());
		}

		return $data;


	}

}