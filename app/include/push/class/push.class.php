<?php
/**
 * 功能： 构造初始化推送数据， 完成推送
 * 暂时只有苹果推送功能
 * @version 1.0
 * @author kk
 * @param ARRAY $aParam 初始化数组 key-value 
 * 
 * @return false or ture
 */
include 'curl.class.php';
class  Push {
	public $debug =  FALSE;//调试状态
    //public $debug =  TRUE;//调试状态
	//push api
	private $PushApi = '';
	//推送接口
	//推送返回数据
	//推送发送数据
	//推送方法
	//加密盐值
	private $_salt = '';
	//设备类型
	private $_devicetype = '';
	//通讯key
	private $_key = '';
	
	//推送客户端域名	
	private   $_domain = '';
	
	//推送类型
	private $_sendtype = '';
	
	//依次连接客户端域名、通讯KEY和加密附属形成的字符串的MD5值
	private $_site = '';
	
	//应用软件id
	private $_appid = '';
	
	
	//苹果设备唯一id
	public $_apnstokens = '';
	
	
	public $_user_id = '';
	
	//tvalue  ios 为apnstoken android为百度  userid
	public $_tvalue = '';
	
	//推送 标题
	public $_title = '';	
	
	//推送内容
	public $_content = '';
	
	//notification_style 通知样式，int型
	
	public $_notification_style = 1;
	
	//open_type 点击通知后的行为
	
	public $_open_type = 2;
	
	//custom_action  用户自定义行为
	
	public $_custom_content = '';
	public $_custom_action = '';
	public $_message_type = 1;
	public $_message_typeIn = 0;
	
	//url
	 
	public $_url = '';
	
    //推送消息
   /**
	 *1.title：通知标题，可以为空；如果为空则设为appid对应的应用名;
	 *2.content：必填，推送通知中的内容
	 *3.notification_style：通知样式，int型。4代表响铃; 2代表振动; 1代表可清除; 默认1， 如需多种样式，将值相加即可。例如：响铃与振动：6;（2+4=6）
	 *4.open_type：点击通知后的行为，int型。 1代表打开url，2代表自定义行为，其他值打开应用，默认打开应用
	 *5.url：点击通知后需要打开的Url地址。只有open_type为1时才有效，才需要设置
	 *6.custom_action:用户自定义行为。只有open_type为2时才有效，才需要设置，例如打开新浪微博客户端'intent:#Intent;action=android.intent.action.MAIN;category=android.intent.category.LAUNCHER;launchFlags=0x10000000;component=com.sina.weibo/.MainTabActivity;end'（此处需要前台开发者支持） 		
	 *7.badge:右角标数字（暂时不支持android）
	 * Enter description here ...
	 * @var array
	 */
    public  $_message = array();
	//右上角标志数字
	public  $_flag;
	
	 public $_badge = 0;
	 //Phone OS type
	public $_os = '';
	
	//推送push_url
	public $_push_url = '';
	//curl对象
	public $_curl;
	 //设备类型
	 
	//返回结果
	public $_return = '';
	
	//ios升级url
	public $_iosurl = '';
	
	//and升级url
	public $_androidurl = '';	
	
	//list	推送设备的类型和`token
    public $_list = '';	
	
	// post data
	/**
	 *1.salt： 必填，加密附属,随机数，用于验证
	 *2.domain:必填，客户端域名,（由服务端提供）
	 *3.site:必填，MD5值md5($domain.$key.$salt),依次连接客户端域名、通讯KEY和加密附属形成的字符串的MD5值
	 *4.appid:必填，应用软件id
	 *5.devicetype:必填，推送设备类型,（1为该应用下的全部用户，2为该应用下全部的ios用户，3为该应用下全部的android用户，9为该应用下的指定用户：需要在list中指定）
	 *6.message:必填，推送的通知格式（一维数组的json_encode值）
	 *7.list:推送的指定用户列表，只有devicetype为9时才有效，才需要必填,二维数组的json_encode值。其中ttype（1为ios，2为android ）、tvalue（ios时为apns_token，android时为百度的userid）
	 * @var Array
	 */
    public $_data = array();
	
	CONST OS_IOS = 'ios';
	 
	CONST OS_AND = 'android';
	 
	CONST IPHONE = 'iPhone';
	 
	CONST IPAD = 'iPad';
	
	//推送设备类型
	CONST DEVICETYPE_IOS = 2; //ios设备
	CONST DEVICETYPE_AND = 3;  //android 设备
	CONST DEVICETYPE_OTHER = 9; //其他设备
	CONST DEVICETYPE_ALL = 1;   //全部
	
	//推送类型
	CONST SENDTYPE_PUSH = 1;//普通推送
	CONST SENDTYPE_UPDATE = 2;//升级推送
	
	//系统类型
	CONST TTYPE_IOS = 1; //IOS
	CONST TTYPE_AND = 2;
	//METHOD
	
	CONST POST = 'post';
	CONST GET  = 'get';
	CONST SUCCESS_CODE  = '1';            //推送成功
    CONST FAIL_CODE_ONE = '-1';           //没有传入任何变量
    CONST FAIL_CODE_TWO = '-2';           //缺少必填项
    CONST FAIL_CODE_THREE = '-3';         //请检查是否有必填参数为空
	CONST FAIL_CODE_FOUR = '-4';          //域名或通讯密钥非法
	CONST FAIL_CODE_FIVE = '-5';          //无效的参数值
	CONST FAIL_CODE_SIX = '-6';           //服务器中没有找到关于此应用的证书信息
	CONST FAIL_CODE_NINE = '-9';          //其他

	public function __construct( $method = self::POST , $aParam ){
		try{
			//if( (!is_array($aParam)) || empty($aParam)){
				
			//	throw new Exception('param type is not right </br>');
				
		   //	}
	
			 if( $aParam ) {
			   	
			   		    $this->Init( $method ,$aParam);
			   	
			  }
			 include_once  ROOT.'/app/include/push/class/curl.class.php'; 
			 if ( $this->_curl = new cURL() ){
			   		return $this;
			   }else{
			   		throw new Exception('Init curl is failed');
			   }
			 // 初始化参数
			// $this->_init( $aParam );
			 
			// $this->_makeSalt();
			 
			// $this->_consite();
			 
		}catch(Exception $ex){
			
			echo $ex->getMessage ();
		}
		//$this->_buildQuery();
	}
	
	public function test(){
		dd(5);
	}
	
	//获取推送内容
	public function Getcontent(){
		
		return  $this->_content;
		
	}
   //修改内容
   public function Setcontent( $newContent = ''){
		
		  $this->_content = $newContent;
		 //$this->_contentInData();
		  return $this;
		
	}
	public  function Init( $method = self::POST , $aParam ){
		try{
			if( (!is_array($aParam)) || empty($aParam)){
				
				throw new Exception('param type is not right </br>');
				
			}
			
			 // 初始化参数
			 $this->_init( $aParam );
			 
			 $this->_makeSalt();
			 
			 $this->_consite();
			 
			 if( $method == self::GET ){
			 	$this->_buildQuery();
			 }
			 
		// if($method == self::POST ){
			 	
		//	 	$this->buildParam();
		//	 }
			 
		}catch (Exception $ex){
			
			echo $ex->getMessage ();
		}
	}
	//初始化数据
	/**
	 * 构造函数初始化数据 专用函数
	 * 
	 * @param ARRAY $aParam 
	 * @throws Exception
	 */
	private  function  _init( $aParam ){
		
		foreach ($aParam as $key=>$val ){
			
			if( isset($this->$key) ){

				$this->$key = $val;
				
			}else{
				
				throw new Exception("invaid param( ${key} ) init is forbidden </br>" ,2);
			}
			
		}
		return  $this;
		
	}
	//构造site值
	/**
	 * 构造加密的值
	 * 依次链接domian 和 salt 和 key
	 * @throws Exception
	 */
	private function _consite(){
		
		if( $this->_domain != '' && $this->_salt != '' && $this->_key){
			
			$this->_site = md5($this->_domain.$this->_key.$this->_salt);
			
		}else{
			 $this->_site = '';
			 throw new Exception('build site is faild! </br>');
	
		}
		return  $this;
	}
	
   /**
    * 生成随机码
    * 
    */
	private function _makeSalt(){
		
		$this->_salt = rand( 0 , 9999 );
		
		return $this;
		
	}
	/**
	 * 获取随机码
	 * 
	 */
	public function GetSalt(){
		return $this->_salt;
	}
	/**
	 * 修改appid
	 * 
	 * @param String $appid
	 */
	public function Setappid( $appid = '' ){
		
		if( $appid ){
			
			$this->_appid = $appid;
		//	$this->_appidInData();
		}
		
		return  $this;
	}
	/**
	 * 修改苹果apnstokens
	 * @param String OR array $apnstokens 如果是string  则是一个apnstoken  数组则是多个
	 */
	public function  Setapnstokens( $apnstokens = ''){
		
		$this->_apnstokens = $apnstokens;
		//$this->_listInData();
		return $this;
		
	}
	/**
	 * 修改百度 userid
	 * @param String OR array $user_id 如果是string  则是一个userid  数组则是多个
	 */
	public function  Setuser_id( $user_id = '' ){
		
		$this->_user_id = $user_id;
		//$this->_listInData();
		return $this;
		
	}
	public function  Setmessage_typeIn( $messagetypein = 0 ){
		
		$this->_message_typeIn = $messagetypein;
		return $this;
		
	}
	public function Settitle( $title = '' ){
		
		if( $title ){
			$this->_title = $title;
		}
		return  $this;
	}
	public function Setos( $os = '' ){
		if( $os == self::OS_AND){
			$this->_os = self::OS_AND;
		}elseif($os == self::OS_IOS){
			$this->_os = self::OS_IOS;
		}
		return  $this;
	}
	
	public function Setcustom_action( $custom_action = '' ){
		
		if( $custom_action ){
			$this->_custom_action = $custom_action;
		}
		return  $this;
	}
	public function Setcustom_content( $custom_content = '' ){
		
		if( $custom_content ){
			$this->_custom_content = $custom_content;
		}
		return  $this;
	}
	public function Setnotification_style( $notification_style = '' ){
		
		if( $notification_style ){
			$this->_notification_style = intval($notification_style);
		}
		return  $this;
	}
	public function Setopen_type( $open_type = '' ){
		$this->_open_type = intval($open_type);
		return  $this;
	}
	public function Setdevicetype( $devicetype = '' ){
		$devicetypes = array(DEVICETYPE_ALL,DEVICETYPE_AND,DEVICETYPE_IOS,DEVICETYPE_OTHER);
		if( $devicetype && in_array($devicetype, $devicetypes )){
			$this->_devicetype = intval($devicetype);
		}
		return  $this;
	}
    public function Setkey( $key = '' ){
		
		if( $key ){
			//如果修改key 就要重新生成site
			$this->_key = $key;
			
			$this->_consite();
			
		}else{
			throw new Exception('key is  empty');
		}
		return  $this;
	}
	public function Setmessage_type( $message_type = 1 ){
		
		if( $message_type == 0 || $message_type == 1){
			//如果修改key 就要重新生成site
			$this->_message_type = $message_type;
		}else{
			throw new Exception('message_type is  not 0 or 1');
		}
		return  $this;
	}
	public function RequestByCurlPost( ){
		
		$curl = & $this->_curl;
		if( $this->_url && !empty($this->_data) ){
			if ( $this->debug ){
				echo 'URL:'.$this->_push_url.'<br/>';
				printr($this->_data);
			}else{
				//$this->_return = $curl->post($this->_push_url ,$this->_data);	
			}
		}else{
			throw new Exception('url or data  is not riggt');
		}
		
		return $this;		
	}
	
	
	/**
	 * 请求接口
	 * 暂时弃用
	 * get方式
	 * @throws Exception
	 */
	public  function RequestByCurlGet( ){	
		$curl = & $this->_curl;
		$data = array();
		if( $this->_push_url ){
			
			$this->_return = $curl->get($this->_push_url );
			
		}else{
			throw new Exception('url is not riggt');
		}
		return $this;
	}
	//build query
	/**
	 * 构建url的query串
	 * 暂时弃用
	 * @throws Exception
	 */
	
	private function _buildQuery(){
		$query = '?';
		$data = array('_apnstokens','_salt','_site','_appid','_devicetype','_content','_domain');
        try {
        	if($this->_push_url == ''){
        		throw new Exception('url is  empty </br>');
        	}else{
        	   $this->_url = $this->_push_url.$this->_link($data,$query);	
        	}
        	
        }catch (Exception $ex){
        	echo $ex->getMessage ();
        	
        }
		return $this;
	}
	/**
	 *连接需要传递的必须参数
	 * 暂时弃用
	 * @param  需要的类的公共变量  array $data
	 * @param  query的初始化值 $query
	 * @throws Exception  异常
	 */
	private function _link( $data = array() ,$query = ''){
		
		if( $data ){
			$query .= http_build_query( $this->_http_build_query_array($data));
			
			//$query  = str_replace('_', '', $query );
	     	/*foreach ( $data as $val){
	     		if( $this->$val ){
	     			
	     			//$query .= str_replace('_', '', $val ).'='.$this->$val;
	     		}else{
	     			throw new Exception($val.' is not right or empty </br>');
	     		}
	     	}
	     	*/
	     	return $query;
		}else{
			
			throw new Exception('build query\'s  array  is not right or empty </br>');
		}
		
	}
	
	
	/**
	 * 暂时弃用
	 * 构建数组 去除_,并对应其相应的值
	 * @param array 初始化数组 $data
	 */
	private function _http_build_query_array( $data ){
		$return_data = array();
		foreach ( $data as $val ){
			
			$key = str_replace('_','', $val);
			
			$return_data[$key]=$this->$val;
		}
		return $return_data;
	}
	/**
	 * 输出请求结果
	 * 

	 */
	public  function GetResault(){
      
		$result = $this->_dealReturn();
		$ret = 'start';
		if( $result['resno'] == 1){
			
			$ret .= '标题：'.$this->_title;	
			$ret .= ',信息：'.$this->_content.'推送成功';
		    $ret .= '</br>';	
			
		}else{
			$ret .= '返回码:'.$result['resno'];
			$ret .= '</br>';
			$ret .= '返回信息：'.$result['prompt'];
			$ret .= '</br>';			
		}
		return $ret;
	}
	
	private function _message_typeInInMessage(){
		
		$this->_message['message_type'] = $this->_message_typeIn;
		
	}
	
	private function _open_typeInMessage(){
		
		$this->_message['open_type'] = $this->_open_type;
		
	}	
	private function _titleInMessage(){
		
		$this->_message['title'] = $this->_title;
		
	}
	
	private function _contentInMessage(){
		if( $this->_content ){
			
			$this->_message['content'] = $this->_content;
		}else{
			
			throw new Exception('content is empty!');
		}
		
		
	}	
	
	private function _notification_styleInMessage(){
		
		$this->_message['notification_style'] = $this->_notification_style;
		
	}
	
	
    private function _urlInMessage(){
		
    	if( $this->_message['open_type'] == 1){
    		if( $this->_url )
    		
    			$this->_message['url'] = $this->_url;
    			
    		else
    			throw new Exception('url is empty!');

    	}else{
    		
    		$this->_message['url'] = '';
    	}
		
		
	}	

	private function _custom_actionInMessage(){
		
    	if( $this->_message['open_type'] == 2){
    	
    		    if( $this->_custom_action ){
    		
	    			$this->_message['custom_action'] = $this->_custom_action;
	    			
	    		}else{
	    			
	    			//throw new Exception(' custom_action is empty!');
	    			$this->_message['custom_action'] = '';
	    		}
    	}else{
    		
    		$this->_message['custom_action'] = '';
    	}
		
		
	}	
	private function _custom_contentInMessage(){
        if( $this->_custom_content ){
	    			$this->_message['custom_content'] = $this->_custom_content;
	    	}else{
	    			throw new Exception('custom_content is empty!');
	    }

	}	
	private function _badgeInMessage(){
		
    			$this->_message['badge'] = $this->_badge;
    			
	}
	
	public function buildMessage(){
		   
			$this->_titleInMessage();
			
	        $this->_contentInMessage();
	        
	        $this->_notification_styleInMessage();
	        
	        $this->_open_typeInMessage();
	        
	        $this->_message_typeInInMessage();
	        
	        $this->_urlInMessage();
	        
	        //$this->_custom_actionInMessage();//不传custom_action
	        $this->_custom_contentInMessage();
	        
	        $this->_badgeInMessage();
	}
	/**
	 *添加附属加密随即数到post 数组中
	 * @throws Exception
	 */
	
	private function _saltInData(){
		
			if( $this->_salt ){
		    	
		    	$this->_data['salt'] = $this->_salt;
		    }else{
		    	throw new Exception('salt is empty!');
		    }		
		
	}
	
	/**
	 *添加 域 到post 数组中
	 * @throws Exception
	 */	
	private function _domainInData(){
		
			    
			if( $this->_domain ){
		    	
		    	$this->_data['domain'] = $this->_domain;
		    }else{
		    	throw new Exception('domain is empty!');
		    }		
		
	}
	
	/**
	 *添加appid到post 数组中
	 * @throws Exception
	 */
	
	private function  _appidInData(){
		
			if( $this->_appid ){
		    	
		    	$this->_data['appid'] = $this->_appid;
		    	
		    }else{
		    	
		    	throw new Exception('appid is empty!');
		    }
	}
	
    /**
	 *添加附属加密site到post 数组中
	 * @throws Exception
	 */
	private function _siteInData(){
		
			if( $this->_site ){
		    	
		    	$this->_data['site'] = $this->_site;
		    	
		    }else{
		    	
		    	throw new Exception('site is empty!');
		    	
		    }		
		
	}
	
	
	/**
	 *添加设备类型Devicetype到post 数组中
	 * @throws Exception
	 */	
	private function _devicetypeInData(){
		
			if( $this->_devicetype ){
		    	
		    	$this->_data['devicetype'] = $this->_devicetype;
		    }else{
		    	throw new Exception('devicetype is empty!');
		    }		
		
		
	}
	
	/**
	 *添加MESSAGE到post 数组中
	 * @throws Exception
	 */	
	private function _messageInData(){
		    
		    #构建message数组
		    
		    $this->buildMessage();
		
			if( !empty($this->_message) ){
		    	
		    	$this->_data['message'] = json_encode( $this->_message );
		    	
		    }else{
		    	throw new Exception('message is empty!');
		    }		
		
		
	}	
	
	/**
	 *添加推送类型Sendtype到post 数组中
	 **暂时弃用
	 * @throws Exception
	 */
	private function _sendtypeInData(){
		
			    	
			if( $this->_sendtype ){
		    	
		    	$this->_data['sendtype'] = $this->_sendtype;
		    }else{
		    	throw new Exception('sendtype is empty!');
		    }		
		
	}
	/**
	 *添加标题Title到post 数组中
	 *暂时弃用
	 * @throws Exception
	 */	
	private function _titleInData(){
		
			if( $this->_title ){
		    	
		    	$this->_data['title'] = $this->_title;
		    }else{
		    	$this->_data['title'] = '';
		    }		
		
	}
	
	
	/**
	 *添加推送内容Content到post 数组中
	 **暂时弃用
	 * @throws Exception
	 */		
	private function _contentInData(){
		
			if( $this->_content ){
		    	
		    	$this->_data['content'] = $this->_content;
			}else{
		    	throw new Exception('content is empty!');
		    }		
		
	}
	
	/**
	 *添加iosurl到post 数组中
	 **暂时弃用
	 * @throws Exception
	 */	
	private function _iosurlInData(){
		
		    
			if( $this->_iosurl ){
		    	
		    	$this->_data['iosurl'] = $this->_iosurl;
		    }	
		
		
	}
	
	/**
	 *添加andurl到post 数组中
	 **暂时弃用
	 * @throws Exception
	 */
	private function  _androidurlInData(){
		
			if( $this->_androidurl ){
		    	
		    	$this->_data['androidurl'] = $this->_androidurl;
		    }
	}
   /**
	 *添加苹果右上角Badget到post 数组中
	 **暂时弃用
	 * @throws Exception
	 */
	private function _badgeInData(){
		
		   if( $this->_badge ){
		    	
		    	$this->_data['badge'] = $this->_badge;
		    }else{
		    	
		    	$this->_data['badge'] = 0;
		    	
		    }		
		
		
		
	}
	/***
	 * 设置and的message_type
	 */
	private function _message_typeInData(){
	
		$this->_data['message_type'] = $this->_message_type;
	}
   /**
	 *添加推送名单list到post 数组中
	 * @throws Exception
	 */
	private function _listInData(){
		
		    if( $this->_devicetype == self::DEVICETYPE_OTHER ){
		    	$aList = array();
		    	if( $this->_user_id ){
		    		
			    	if(is_array( $this->_user_id ) ){
			    		
			    		foreach ( $this->_user_id as $useid ){
			    			
			    			$aList[] = array('ttype'=>self::TTYPE_AND,'tvalue'=>$useid);
			    			
			    		}
			    		
			    	}else{
			    		
			    		$aList[] = array('ttype'=>self::TTYPE_AND,'tvalue'=>$this->_user_id);
			    	}
		    	
		    	}
		     if( $this->_apnstokens ){
			       	 if(is_array( $this->_apnstokens )){
				    		
				    		foreach ( $this->_apnstokens as $apnstoken ){
				    			
				    			$aList[] = array('ttype'=>self::TTYPE_IOS,'tvalue'=>$apnstoken);
				    			
				    		}
				    		
				      }else{
				    		
				    		$aList[] = array('ttype'=>self::TTYPE_IOS,'tvalue'=>$this->_apnstokens);
				      }
		     	 }
		     	 
		     	if( empty($aList)){
		     		
		     		throw new Exception( 'devicetype is 9 ,AList  is empty');
		     	}else{
		     		
		     		 $this->_data['list'] = json_encode( $aList );
		     	}
		        
		    	
		    }else{
		    	
		    	$this->_data['list'] = '';
		    }		
		
	}
	
	
	public function buildParam(  ){
		    
            $this->_saltInData();

		    $this->_domainInData();
		    
            $this->_appidInData();
            
		    $this->_siteInData();
		    
		    $this->_devicetypeInData();
		    $this->_message_typeInData();
		    
           // $this->_sendtypeInData();

		   // $this->_titleInData();	

		    
           // $this->_contentInData();

		   // $this->_iosurlInData();	
		    	    	
		   //  $this->_androidurlInData();	
		    	    	
		   // $this->_badgeInData();	

		    $this->_messageInData();
		    
            $this->_listInData();		 

	}
	
	
	//处理返回结果
	/**
	 * 处理返回的结果 ,json_decode
	 * 返回结果格式		JSON:
	 * 			    {
	 * 			        resno : 正数（表示成功）或负数（表示出错）
	 * 			        prompt : 提示信息,
	 * 			    } 
     * 
	 */

	private function _dealReturn(){
		return json_decode($this->_return, true);
	}
	//建立查询
	private function  _buildRequest(){
		
		
	}
	
	public function post(){
				 if ( $this->debug ){
				 	$this->buildParam();
					echo 'URL:'.$this->_push_url.'<br/>';
					printr($this->_data);
				 }else{
				    $this->buildParam();
				    
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $this->_push_url );
					
					curl_setopt($ch, CURLOPT_POST, 1);
					
					curl_setopt($ch, CURLOPT_POSTFIELDS ,$this->_data);
					
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //CUROPT_RETURNTRANSFER - 返回内容，而不是直接输出
					//ob_start();
					$rejson = curl_exec($ch);
					 //curl_exec($ch);
					//$rejson = ob_end_flush();
					//$this->_return = 
					curl_close($ch);
					$ret = 'start';
					$res = json_decode($rejson,true);
//					if( $res['resno'] == 1){
//						
//						$ret .= '标题：'.$this->_title;	
//						$ret .= ',信息：'.$this->_content.'推送成功';
//					    $ret .= '</br>';	
//						
//					}else{
//						$ret .= '返回码:'.$res['resno'];
//						$ret .= '</br>';
//						$ret .= '返回信息：'.$res['prompt'];
//						$ret .= '</br>';			
//					}
//				return $ret;
				return $res;
			}
		}
			
}