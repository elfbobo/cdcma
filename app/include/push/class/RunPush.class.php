<?php
class RunPush{
	//throw new Exception($message, $code)
	/**
	 * 通过传递对象 和设置参数  对对象成员进行修改，并且运行对象中post方法 进行请求接口
	 * @param object $obj
	 * @param 需要post的参数数组 array $aparam   如同  $aIntParam = array(
	 *									 					'_url'=>PUSH_URL,
	 *									 					'_domain'=>PUSH_DOMAIN, 
	 *									 					'_title'=>'',
	 *									 					'_devicetype'=>DEVICETYPE_OTHER,
	 *									 					'_key'=>AUTH_KEY_ZZK,
	 *														'_sendtype'=>SENDTYPE_PUSH,
	 *														'_iosurl'=>'www.baidu.com',
	 *									                    '_androidurl'=>'www.baidu.com'
	 *									 	);
	 * 
	 * 
	 * @param boolean $isInit 是否重新初始化参数
	 */
	public static function Run( $obj , $aparam = array() ,$isInit = false ){
			if( @is_object( $obj ) ){
			      $r = new ReflectionClass($obj);
			      if( $isInit == true){
						  if( @is_array( $aparam ) && !empty( $aparam ) ){	
								   	 	if(method_exists( $obj , 'Init' )) { 
								   	 		$obj->Init( 'post' ,$aparam ); 
								   	 	}else{
								   	 		throw new Exception('init function not exist!') ;
								   	 	}
						  }else{
								throw new Exception('aparam is not right') ;
						  }
			      }  
				   try{
				   	    if(method_exists( $obj , 'post' )) { 
				   	 		return $obj->post(); 
				   	 	}else{
				   	 		throw new Exception('post function not exist!') ;
				   	 	}
				   	 	
		   			   	/*if(method_exists( $obj , 'GetResault' )) { 
				   	 		return $obj->GetResault(); 
				   	 	}else{
				   	 		throw new Exception('GetResault function not exist!') ;
				   	 	}*/
				   	 }catch ( Exception $e){
				   	 		exit($e->getMessage ());
				       } 
		}else{
				throw new Exception( 'object not invalid' );
		}
		
	}
	
	/**
	 * 设置对象参数
	 * @param object $obj
	 * @param 设置参数数组  $settings  $key 对应修改成员名称及其方法  $val对应相应的修改值    如同：array('appid'=>PUSH_APPID_IOS_ZZK,'user_id'=>'')
	 * @throws Exception
	 */	
	
	public static function set($obj ,$settings = array()){
		if( @is_object( $obj ) ){
			   $r = new ReflectionClass($obj);
			   if( @is_array( $settings ) && !empty( $settings ) ){
			   	
			   	   foreach ( $settings  as $key=>$val){
			   	   	
			   	   			
						   try{
								//当该类不存在该方法时，会抛出ReflectionException异常，所以必须捕获异常。
								if($method = $r->getMethod('Set'.$key)){
															
									if($method->isPublic() && !$method->isAbstract()){
										
									    $function = 'Set'.$key;
									    
										$obj->$function($val);
										
									}else{
										throw new Exception('Set'.$key.' method is unvaliable!');
									}
									
								}else{
									
									   throw new Exception('Set'.$key.' method is not a function!');
								}
								
							}catch(ReflectionException $e){
								
								  
								 exit($e->getMessage ());	
							}
			   	   	
			   	   }
			   }else{
			   	           throw new Exception('setting is not right') ;
			   }
		
		}else{
				
				throw new Exception( 'object not invalid' );
				
		}
		
		
   }	
	/**
	 * 执行方法
	 * @param  $obj  执行对象
	 * @param  $setting  设置对象参数
 	 * @param  $aparam   初始化对象其他基本信息参数
	 * @param  $isInit   是否初始化基本信息参数
	 */
	public static function RunPost($obj ,$setting = array() ,$aparam = array(),$isInit = false){
		
		if( $setting ){
			try{
				self::set($obj ,$setting);
			}catch(Exception $e){
								
				exit($e->getMessage ());	
				
				}
			
		}
		return  self::Run($obj ,$aparam);
		
	}
}