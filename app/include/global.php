<?php
function Images()
{
    //图片表情
    $tips = array(
        '微笑',
        '撇嘴',
        '色',
        '得意',
        '流泪',
        '睡觉',
        '大哭',
        '尴尬',
        '发火',
        '调皮',
        '大笑',
        '郁闷',
        '抓狂',
        '吐',
        '偷笑',
        '可爱',
        '白眼',
        '傲慢',
        '犯困',
        '惊恐',
        '流汗',
        '奋斗',
        '疑惑',
        '晕',
        '衰',
        '敲打',
        '再见',
        '擦汗',
        '抠鼻',
        '鼓掌',
        '糗',
        '坏笑',
        '左哼哼',
        '右哼哼',
        '哈欠',
        '鄙视',
        '委屈',
        '快哭了',
        '阴险',
        '可怜',
        '潜水',
        '石化',
        '安慰',
        '鲜花',
        '手势-棒',
        '手势-逊',
        '握手',
        '胜利'
    );
    $imageli = '';
    for ($i = 0; $i <= 47; $i++) {

        if ($i <= 9) {
            $imageli .= '<img src = "' . PHP_IMG . '/front/emotion/' . $i . '.gif"   title="' . $tips [$i] . '"
			               style = \'cursor:pointer;display:inline;\' onclick="insertIcon(\'' . PHP_IMG . '/front/emotion/' . $i . '.gif\')" /> ';
        } else {
            $imageli .= '<img src = "' . PHP_IMG . '/front/emotion/' . $i . '.gif"   title="' . $tips [$i] . '"
			               style = \'cursor:pointer;display:inline;\' onclick="insertIcon(\'' . PHP_IMG . '/front/emotion/' . $i . '.gif\')" /> ';

        }
        if (($i == 15 || $i == 31 || $i == 47) && $i > 0) {
            $imageli .= "<br/>";
        }
    }
    return $imageli;
}

/**
 * 获取当前页面完整URL地址
 */
function get_url()
{
    $sys_protocal = isset ($_SERVER ['SERVER_PORT']) && $_SERVER ['SERVER_PORT'] == '443' ? 'https://' : 'http://';
    $php_self = $_SERVER ['PHP_SELF'] ? safe_replace($_SERVER ['PHP_SELF']) : safe_replace($_SERVER ['SCRIPT_NAME']);
    $path_info = isset ($_SERVER ['PATH_INFO']) ? safe_replace($_SERVER ['PATH_INFO']) : '';
    $relate_url = isset ($_SERVER ['REQUEST_URI']) ? safe_replace($_SERVER ['REQUEST_URI']) : $php_self . (isset ($_SERVER ['QUERY_STRING']) ? '?' . safe_replace($_SERVER ['QUERY_STRING']) : $path_info);
    //return $sys_protocal . (isset ( $_SERVER ['HTTP_HOST'] ) ? $_SERVER ['HTTP_HOST'] : '') . $relate_url;
    return $relate_url;
}

function safe_replace($string)
{
    $string = str_replace('%20', '', $string);
    $string = str_replace('%27', '', $string);
    $string = str_replace('*', '', $string);
    $string = str_replace('"', '&quot;', $string);
    $string = str_replace("'", '', $string);
    $string = str_replace("\"", '', $string);
    $string = str_replace('//', '', $string);
    $string = str_replace(';', '', $string);
    $string = str_replace('<', '&lt;', $string);
    $string = str_replace('>', '&gt;', $string);
    $string = str_replace('(', '', $string);
    $string = str_replace(')', '', $string);
    $string = str_replace("{", '', $string);
    $string = str_replace('}', '', $string);
    return $string;
}

function get_filetype($filename)
{
    $extend = explode(".", $filename);
    return strtolower($extend[count($extend) - 1]);
}

function mkdirs($path, $mode = 0777)
{
    if (!file_exists($path)) {
        mkdirs(dirname($path), $mode);
        mkdir($path, $mode);
    }
}

/*****************身份证号验证**********/
function validation_filter_id_card($id_card)
{
    if (strlen($id_card) == 18) {
        return idcard_checksum18($id_card);
    } elseif ((strlen($id_card) == 15)) {
        $id_card = idcard_15to18($id_card);
        return idcard_checksum18($id_card);
    } else {
        return false;
    }
}

// 计算身份证校验码，根据国家标准GB 11643-1999
function idcard_verify_number($idcard_base)
{
    if (strlen($idcard_base) != 17) {
        return false;
    }
    //加权因子
    $factor = array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);
    //校验码对应值
    $verify_number_list = array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');
    $checksum = 0;
    for ($i = 0; $i < strlen($idcard_base); $i++) {
        $checksum += substr($idcard_base, $i, 1) * $factor[$i];
    }
    $mod = $checksum % 11;
    $verify_number = $verify_number_list[$mod];
    return $verify_number;
}

// 将15位身份证升级到18位
function idcard_15to18($idcard)
{
    if (strlen($idcard) != 15) {
        return false;
    } else {
        // 如果身份证顺序码是996 997 998 999，这些是为百岁以上老人的特殊编码
        if (array_search(substr($idcard, 12, 3), array('996', '997', '998', '999')) !== false) {
            $idcard = substr($idcard, 0, 6) . '18' . substr($idcard, 6, 9);
        } else {
            $idcard = substr($idcard, 0, 6) . '19' . substr($idcard, 6, 9);
        }
    }
    $idcard = $idcard . idcard_verify_number($idcard);
    return $idcard;
}

// 18位身份证校验码有效性检查
function idcard_checksum18($idcard)
{
    if (strlen($idcard) != 18) {
        return false;
    }
    $idcard_base = substr($idcard, 0, 17);
    if (idcard_verify_number($idcard_base) != strtoupper(substr($idcard, 17, 1))) {
        return false;
    } else {
        return true;
    }
}

/*****************身份证号验证**********/
/*****************银行卡号验证**********/
function luhm($s)
{
    $n = 0;
    for ($i = strlen($s); $i >= 1; $i--) {
        $index = $i - 1;
        //偶数位
        if ($i % 2 == 0) {
            $n += $s{$index};
        } else {//奇数位
            $t = $s{$index} * 2;
            if ($t > 9) {
                $t = (int)($t / 10) + $t % 10;
            }
            $n += $t;
        }
    }
    return ($n % 10) == 0;
}

/*****************银行卡号验证**********/
//获取routeName
function getRouteName()
{
    $url = $_SERVER['REQUEST_URI'];
    $url = str_replace(strstr($url, '?'), '', $url);
    $aItems = explode('/', $url);
    $class = $aItems[1];
    return $class;
}

function str_cut_cms($string, $length, $dot = ' ...')
{
    $string = strip_tags($string);
    return mb_strimwidth($string, 0, $length, "...", "UTF-8");
}

function resizeImage($im, $dest, $maxwidth, $maxheight)
{
    $img = getimagesize($im);
    switch ($img[2]) {
        case 1:
            $im = @imagecreatefromgif($im);
            break;
        case 2:
            $im = @imagecreatefromjpeg($im);
            break;
        case 3:
            $im = @imagecreatefrompng($im);
            break;
    }

    $pic_width = imagesx($im);
    $pic_height = imagesy($im);
    $resizewidth_tag = false;
    $resizeheight_tag = false;
    if (($maxwidth && $pic_width > $maxwidth) || ($maxheight && $pic_height > $maxheight)) {
        if ($maxwidth && $pic_width > $maxwidth) {
            $widthratio = $maxwidth / $pic_width;
            $resizewidth_tag = true;
        }

        if ($maxheight && $pic_height > $maxheight) {
            $heightratio = $maxheight / $pic_height;
            $resizeheight_tag = true;
        }

        if ($resizewidth_tag && $resizeheight_tag) {
            if ($widthratio < $heightratio) {
                $ratio = $widthratio;
            } else {
                $ratio = $heightratio;
            }
        }


        if ($resizewidth_tag && !$resizeheight_tag) {
            $ratio = $widthratio;
        }
        if ($resizeheight_tag && !$resizewidth_tag) {
            $ratio = $heightratio;
        }
        $newwidth = $pic_width * $ratio;
        $newheight = $pic_height * $ratio;

        if (function_exists("imagecopyresampled")) {
            $newim = imagecreatetruecolor($newwidth, $newheight);
            imagecopyresampled($newim, $im, 0, 0, 0, 0, $newwidth, $newheight, $pic_width, $pic_height);
        } else {
            $newim = imagecreate($newwidth, $newheight);
            imagecopyresized($newim, $im, 0, 0, 0, 0, $newwidth, $newheight, $pic_width, $pic_height);
        }

        imagejpeg($newim, $dest);
        imagedestroy($newim);
    } else {
        imagejpeg($im, $dest);
    }
}


function mb_unserialize($serial_str)
{
    // print_r($serial_str);
    // $out = preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $serial_str );
    $out = preg_replace_callback('!s:(\d+):"(.*?)";!s', function ($r) {
        $str = "s:" . strlen($r[2]) . ":" . '"' . $r[2] . '";';
        return $str;
    }, $serial_str);
    return unserialize($out);
}

function writeFile($filename, $content)
{
    $handle = fopen($filename, 'a');
    fwrite($handle, $content);
    fclose($handle);
}

function recordLog($filename, $content, $dir)
{
    if (!file_exists($dir)) {
        mkdir($dir, 0777, true);
    }
    $fileFullName = $dir . '/' . $filename . '.txt';
    writeFile($fileFullName, $content);
}

function user_agent_is_mobile()
{
    $agent = isset($_SERVER['HTTP_USER_AGENT']) ? strtolower($_SERVER['HTTP_USER_AGENT']) : '';
    if (preg_match('/iphone|android|ipad|windows phone|micromessenger/', $agent)) {
        return true;
    }
    return false;
}

/**
 * 判断访问来源是否为微信浏览器
 */
function user_agent_is_weixin()
{
    $agent = isset($_SERVER['HTTP_USER_AGENT']) ? strtolower($_SERVER['HTTP_USER_AGENT']) : '';
    if (preg_match('/micromessenger/', $agent)) {
        return true;
    }
    return false;
}

/**
 * 与接口通信工具
 *
 * @param string $url
 * @param array $data
 * @param string $method
 * @param int $timeout
 * @return array
 */
function requestApiByCurl($sUrl, $aData, $sMethod = 'Post', $iTimeout = 10)
{
    $sMethod = strtolower($sMethod);
    $ch = curl_init();
    if ($sMethod == 'get') {
        $sUrl .= '?' . http_build_query($aData);
    }
    curl_setopt($ch, CURLOPT_URL, $sUrl);
    if ($sMethod == 'post') {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $aData);
    }
    $iTimeout = intval($iTimeout);
    if ($iTimeout) {
        curl_setopt($ch, CURLOPT_TIMEOUT, $iTimeout);
    }
    ob_start();
    curl_exec($ch);
    $sOut = ob_get_clean();
    curl_close($ch);
    return json_decode($sOut, true);
}

//不同环境下获取真实的IP
function GetIP()
{
    static $realip = null;
    if ($realip !== null) {
        return $realip;
    }
    //判断服务器是否允许$_SERVER
    if (isset($_SERVER)) {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $realip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $realip = $_SERVER['HTTP_CLIENT_IP'];
        } else {
            $realip = $_SERVER['REMOTE_ADDR'];
        }
    } else {
        //不允许就使用getenv获取
        if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown")) {
            $realip = getenv("HTTP_CLIENT_IP");
        } else {
            if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown")) {
                $realip = getenv("HTTP_X_FORWARDED_FOR");
            } else {
                if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown")) {
                    $realip = getenv("REMOTE_ADDR");
                } else {
                    if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'],
                            "unknown")) {
                        $realip = $_SERVER['REMOTE_ADDR'];
                    } else {
                        $realip = "unknown";
                    }
                }
            }
        }
    }

    return $realip;
}

function sub_str($str, $length = 0, $append = true)
{
    $str = trim($str);
    $strlength = strlen($str);
    if ($length == 0 || $length >= $strlength) {
        return $str;  //截取长度等于0或大于等于本字符串的长度，返回字符串本身
    } elseif ($length < 0) {
        $length = $strlength + $length;//那么截取长度就等于字符串长度减去截取长度
        if ($length < 0) {
            $length = $strlength;//如果截取长度的绝对值大于字符串本身长度，则截取长度取字符串本身的长度
        }
    }
    if (function_exists('mb_substr')) {
        $newstr = mb_substr($str, 0, $length, 'utf-8');
    } elseif (function_exists('iconv_substr')) {
        $newstr = iconv_substr($str, 0, $length, 'utf-8');
    } else {
        //$newstr = trim_right(substr($str, 0, $length));
        $newstr = substr($str, 0, $length);
    }
    if ($append && $str != $newstr) {
        $newstr .= '...';
    }
    return $newstr;

}

/**
 * 秒数转时间
 * @param $time
 * @return bool|string
 */
function sec_to_time($time)
{
    if (is_numeric($time)) {
        $value = array(
            "years" => 0,
            "days" => 0,
            "hours" => 0,
            "minutes" => 0,
            "seconds" => 0,
        );
        if ($time >= 31556926) {
            $value["years"] = floor($time / 31556926);
            $time = ($time % 31556926);
        }
        if ($time >= 86400) {
            $value["days"] = floor($time / 86400);
            $time = ($time % 86400);
        }
        if ($time >= 3600) {
            $value["hours"] = floor($time / 3600);
            $time = ($time % 3600);
        }
        if ($time >= 60) {
            $value["minutes"] = floor($time / 60);
            $time = ($time % 60);
        }
        $value["seconds"] = floor($time);
        //return (array) $value;
        $result = [];
        if ($value["years"]) {
            $result[] = $value["years"] . "年";
        }
        if ($value["days"]) {
            $result[] = $value["days"] . "天";
        }
        if ($value["hours"]) {
            $result[] = $value["hours"] . "小时";
        }
        if ($value["minutes"]) {
            $result[] = $value["minutes"] . "分";
        }
        if ($value["seconds"]) {
            $result[] = $value["seconds"] . "秒";
        }

        Return join('', $result);

    } else {
        return (bool)false;
    }
}


//将用户名进行处理，中间用星号表示
function substr_cut($user_name)
{
    //获取字符串长度
    $strlen = mb_strlen($user_name, 'utf-8');
    //如果字符创长度小于2，不做任何处理
    if ($strlen < 2) {
        return $user_name;
    } else {
        //mb_substr — 获取字符串的部分
        $firstStr = mb_substr($user_name, 0, 1, 'utf-8');
        $lastStr = mb_substr($user_name, -1, 1, 'utf-8');
        //str_repeat — 重复一个字符串
        return $strlen == 2 ? $firstStr . str_repeat('*',
                mb_strlen($user_name, 'utf-8') - 1) : $firstStr . str_repeat("*", $strlen - 2) . $lastStr;
    }
}

/**
 * xss 过滤
 * @param $string
 * @param bool $low
 * @return string|array
 */
function clean_xss(&$string, $low = false)
{
    if (!is_array($string)) {
        $string = trim($string);
        $string = strip_tags($string);
        $string = htmlspecialchars($string);
        if ($low) {
            return $string;
        }
        $string = str_replace(array('"', "\\", "'", "/", "..", "../", "./", "//"), '', $string);
        $no = '/%0[0-8bcef]/';
        $string = preg_replace($no, '', $string);
        $no = '/%1[0-9a-f]/';
        $string = preg_replace($no, '', $string);
        $no = '/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+/S';
        $string = preg_replace($no, '', $string);

        return $string;
    }
    $keys = array_keys($string);
    foreach ($keys as $key) {
        $string[$key] = clean_xss($string[$key], $low);
    }

    return $string;
}

/**
 * 验证身份证号
 * @param $id
 * @return bool
 */
function check_idcard($id)
{
    $id = strtoupper($id);
    $regx = "/(^\d{15}$)|(^\d{17}([0-9]|X)$)/";
    $arr_split = array();
    if (!preg_match($regx, $id)) {
        return false;
    }
    if (15 == strlen($id)) //检查15位
    {
        $regx = "/^(\d{6})+(\d{2})+(\d{2})+(\d{2})+(\d{3})$/";

        @preg_match($regx, $id, $arr_split);
        //检查生日日期是否正确
        $dtm_birth = "19" . $arr_split[2] . '/' . $arr_split[3] . '/' . $arr_split[4];
        if (!strtotime($dtm_birth)) {
            return false;
        } else {
            return true;
        }
    } else      //检查18位
    {
        $regx = "/^(\d{6})+(\d{4})+(\d{2})+(\d{2})+(\d{3})([0-9]|X)$/";
        @preg_match($regx, $id, $arr_split);
        $dtm_birth = $arr_split[2] . '/' . $arr_split[3] . '/' . $arr_split[4];
        if (!strtotime($dtm_birth)) //检查生日日期是否正确
        {
            return false;
        } else {
            //检验18位身份证的校验码是否正确。
            //校验位按照ISO 7064:1983.MOD 11-2的规定生成，X可以认为是数字10。
            $arr_int = array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);
            $arr_ch = array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');
            $sign = 0;
            for ($i = 0; $i < 17; $i++) {
                $b = (int)$id{$i};
                $w = $arr_int[$i];
                $sign += $b * $w;
            }
            $n = $sign % 11;
            $val_num = $arr_ch[$n];
            if ($val_num != substr($id, 17, 1)) {
                return false;
            } else {
                return true;
            }
        }
    }
}

/**
 * 使图片背景透明
 * @param $path
 * @return resource
 */
function image2transparent($path)
{
    //载入图像，返回图像资源
    $img = imagecreatefromjpeg($path);

    // 去除护眼色
    // 查看图片0坐标的颜色
    $rgb = imagecolorat($img, 0, 0);
    $r = ($rgb >> 16) & 0xFF;
    $g = ($rgb >> 8) & 0xFF;
    $b = $rgb & 0xFF;
    // 0坐标颜色非白色时处理
    if ($r != 255 && $g != 255 && $b != 255) {
        //要处理的色阶起始值
        $begin_r = 247;
        $begin_g = 252;
        $begin_b = 246;
        list($src_w, $src_h) = getimagesize($path);// 获取原图像信息

        $i = 0;
        $src_white = imagecolorallocate($img, 255, 255, 255);
        for ($x = 0; $x < $src_w; $x++) {
            for ($y = 0; $y < $src_h; $y++) {
                $rgb = imagecolorat($img, $x, $y);
                $r = ($rgb >> 16) & 0xFF;
                $g = ($rgb >> 8) & 0xFF;
                $b = $rgb & 0xFF;
                if ($r == 255 && $g == 255 && $b == 255) {
                    $i++;
                    continue;
                }
                if (($r > 200 && $g > 200 && $b > 200) && ($r <= $begin_r && $g <= $begin_g && $b <= $begin_b)) {
                    imagefill($img, $x, $y, $src_white);//替换成白色
                }
            }
        }
    }

    //$img = $src_im;
    // 使图片透明
    imagecolortransparent($img, imagecolorallocatealpha($img, 255, 255, 255, 127));

    return $img;
}