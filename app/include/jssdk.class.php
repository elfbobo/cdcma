<?php

class jssdk
{
    private $appId;
    private $api_domain;
    private $appSecret;

    public function __construct($config = [])
    {
        $this->appId = isset($config['appid']) ? $config['appid'] : 'wx959ff41a33559b46';
        $this->api_domain = isset($config['domain']) ? $config['domain'] : 'cdcma.bizconf.cn';
        $this->appSecret = isset($config['appsecret']) ? $config['appsecret'] : 'b296c2b567eac4dfc008d88f2aa46982';
    }

    public function getSignPackage()
    {
        $jsapiTicket = $this->getJsApiTicket();
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $timestamp = time();
        $nonceStr = $this->createNonceStr();

        // 这里参数的顺序要按照 key 值 ASCII 码升序排序
        $arr = array('jsapi_ticket' => $jsapiTicket, 'noncestr' => $nonceStr, 'timestamp' => $timestamp, 'url' => $url);
        ksort($arr);
        $string = '';
        foreach ($arr as $k => $v) {
            $string .= $string ? "&" . $k . "=" . $v : $k . "=" . $v;
        }

        $signature = sha1($string);

        $signPackage = array(
            "appId" => $this->appId,
            "nonceStr" => $nonceStr,
            "timestamp" => $timestamp,
            "url" => $url,
            "signature" => $signature,
            "rawString" => $string
        );
        return $signPackage;
    }

    private function createNonceStr($length = 16)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }

    private function getJsApiTicket()
    {
        // jsapi_ticket 应该全局存储与更新，以下代码以写入到文件中做示例
        $baseurl = public_path() . '/json/';
        $file = $baseurl . "jsapi_ticket.json";
        if (file_exists($file)) {
            $data = json_decode(file_get_contents($file));
        }
        if (!isset($data) || $data->expire_time < time()) {
            $accessToken = $this->getAccessToken();
            // 如果是企业号用以下 URL 获取 ticket
            // $url = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token=$accessToken";
            $url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=$accessToken";
            $res = json_decode(self::httpGet($url));
            if (!isset($res->ticket)) {
                return '';
            }
            $ticket = $res->ticket;
            if (!isset($data)) {
                $data = new stdClass();
            }
            if ($ticket) {
                $data->expire_time = time() + 7000;
                $data->jsapi_ticket = $ticket;
                $fp = fopen($baseurl . "jsapi_ticket.json", "w");
                fwrite($fp, json_encode($data));
                fclose($fp);
            }
        } else {
            $ticket = $data->jsapi_ticket;
        }
        return $ticket;
    }

    private function getAccessToken()
    {
        // access_token 应该全局存储与更新，以下代码以写入到文件中做示例
        $baseurl = public_path() . '/json/';
        $file = $baseurl . "access_token.json";
        if (file_exists($file)) {
            $data = json_decode(file_get_contents($file));
        }
        if (!isset($data) || $data->expire_time < time()) {
            // 如果是企业号用以下URL获取access_token
            // $url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=".APPID."&corpsecret=".APPSECRET;
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" . $this->appId . "&secret=" . $this->appSecret;
            $res = json_decode(self::httpGet($url));
            $access_token = $res->access_token;
            if (!isset($data)) {
                $data = new stdClass();
            }
            if ($access_token) {
                $data->expire_time = time() + 7000;
                $data->access_token = $access_token;
                $fp = fopen($baseurl . "access_token.json", "w");
                fwrite($fp, json_encode($data));
                fclose($fp);
            }
        } else {
            $access_token = $data->access_token;
        }
        return $access_token;
    }

    private function httpGet($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($curl, CURLOPT_TIMEOUT, 500);
        curl_setopt($curl, CURLOPT_TIMEOUT, 2);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_URL, $url);

        $res = curl_exec($curl);
        curl_close($curl);

        return $res;
    }
}

