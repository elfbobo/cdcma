<?php
return array(
	'other_type' =>  '非常抱歉，{$MsgType}类型的消息目前暂不支持。',
	'cancel'     =>  '您已取消关注。',
	'welcome'    =>  '您好，欢迎您关注。',
	'cdtm_welcome'      => '您好，欢迎关注中华医学会杂志社新刊《慢性疾病与转化医学（英文）》。本刊官网：www.cdatm.org。投稿网址：https://www.evise.com/evise/faces/pages/navigation/NavController.jspx?JRNL_ACR=CDTM。季刊，64页。目前免收审稿费和版面费。',
	'cmacardio_welcome' => '您好，这里是中华医学会杂志社“心内空间”。【BP中文版免费申领中，回复姓名，单位科室，电话，邮箱即可免费试用BP，活动详情请见底部菜单】【中华医学会杂志社最新推出医学App：菁医汇，欢迎登陆苹果应用商店下载】',
	'cmaresp_welcome'   => '你好，我们是中华医学会杂志社为呼吸科医生建立的服务平台。【中华医学会杂志社最新推出医学App：菁医汇（iPad版），欢迎登陆苹果应用商店下载】查看底部菜单，点击“指南共识”获取呼吸科最新指南；点击“病例讨论”，精彩病例纷沓而来；点击“我要订阅”，微信订阅中华系列杂志。'
);