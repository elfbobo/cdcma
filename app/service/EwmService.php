<?php
/**
 * 生成二维码服务
 */
class EwmService
{
	
	/**
	 * 创建二维码
	 * @param unknown_type $str
	 */
	public static function createErweima( $str, $logo = true){
		if( !$str ) {
			return false;
		}
		require ( app_path().'/include/phpqrcode/phpqrcode.php' );
		$sNewName = date('YmdHis').rand(1,9999). '.png';
		$sFilePath = '/upload/ewm/'. date('Ymd').'/';
		mkdirs(public_path().$sFilePath);
	
		$filename = public_path().$sFilePath.$sNewName;
		$errorCorrectionLevel = 'H';
		$matrixPointSize = 10;
		QRcode::png($str, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
		if( !$logo || !file_exists( public_path().$logo ) ){
			return $sFilePath.$sNewName;
		}
		//添加logo
		$QR = imagecreatefromstring(file_get_contents($filename));
		$logo = imagecreatefromstring(file_get_contents( public_path().$logo));
		if (imageistruecolor($logo)) imagetruecolortopalette($logo, false, 65535);
		$QR_width = imagesx($QR);//二维码图片宽度
		$QR_height = imagesy($QR);//二维码图片高度
		$logo_width = imagesx($logo);//logo图片宽度
		$logo_height = imagesy($logo);//logo图片高度
		$logo_qr_width = $QR_width / 5;
		$scale = $logo_width/$logo_qr_width;
		$logo_qr_height = $logo_height/$scale;
		$from_width = ($QR_width - $logo_qr_width) / 2;
		//重新组合图片并调整大小
		imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width,
			 $logo_qr_height, $logo_width, $logo_height);
		imagepng($QR, $filename);
		return $sFilePath.$sNewName;
	}
} 