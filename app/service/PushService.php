<?php
/**
 * 推送服务
 * 消息通知总结：
	·11  在线会议，听者报名或取消参会给讲者发送推送通知，点击进去 我的会议-主讲会议列表
	·12  在线会议，讲者邀请医生参加会议给听者发送推送通知，点击进入我的会议-参加会议列表
	·13  在线会议，每周一早上六点给讲者推送可以报名讲课的通知，点击进入讲者报名列表页
	·14  在线会议，每周一早上六点给听者推送可以报名参会的通知，或者讲者取消会议给听者推送通知，点击进入听者报名列表页
	.15 在线会议，讲者报名成功给同城市所有医生发送推送通知，点击进入听者报名列表页
	·31  科研申请，审核成功或者失败发送推送通知，点击进入科研申请  列表  
	·32  科研申请，上传集赞照片审核成功是发送推送通知，点击进入培训课程 列表 页面
	·33  科研申请，上传集赞照片审核失败发送推动通知，说明失败原因，后台加审核失败的原因，到上传照片页面
	·21  健康教育，课件审核成功或者失败，发送推送通知，点击进入 列表
	·41  线下会议，报名成功，点击进入线下会议列表
	·42  线下会议，报名失败，说明失败原因，点击进入线下会议列表
    .51 我的医生，当我的医生被其它代表拉走时通知

	.所有的审核失败都加一个失败原因框，除去线下会议的自动审核
	
	catid总结如下：
	1：在线会议；（11、个人中心-我的会议-主讲会议；12：个人中心-我的会议-参加会议；13：讲者报名列表页；14：听者报名列表页；15：听者报名列表页）
	2：健康教育；（21、健康教育-我的课程-列表）
	3：科研申请；（31、科研申请-我的申请-列表；32、培训课程列表、33：科研申请-我的申请-列表）
	4：线下会议；（41、个人中心-线下会议-列表；42、个人中心-线下会议-列表）
    5：我的医生;
 */
class PushService
{
	//在线会议，听者报名参会给讲者发送的推送通知(catid=11)
	const ONLINE_LISTENER_JOIN = '{{username}}报名了您的会议【{{title}}】';
	//在线会议，听者取消报名参会给讲者发送的推送通知(catid=11)
	const ONLINE_LISTENER_CANCEL = '{{username}}取消了参加您的会议【{{title}}】';
	//在线会议，讲者邀请医生参加会议给听者发送的推送通知(catid=12)
	const ONLINE_SPEAKER_INVITED = '{{username}}邀请您参加【{{title}}】会议';
	//在线会议，讲者取消会议给听者发送的推送通知(catid=14)
	const ONLINE_SPEAKER_CANCEL = '{{username}}的【{{title}}】会议已取消';
	//每周一早上六点给讲者推送可以报名讲课的通知(catid=13)
	const ONLINE_SPEAKER_NOTICE = '下周的在线会议可以开始报名讲课啦！';
	//每周一早上六点给听者推送可以报名参会的通知(catid=14)
	const ONLINE_LISTENER_NOTICE = '本周在线会议可以开始报名参会啦！';
	//讲者报名成功给同城市所有医生发送的推送通知(catid=15)
	const ONLINE_LISTENER_ALL = '{{userhospital}}{{username}}{{meetingtime}}在线授课，欢迎报名参加';
	
	//科研申请，审核成功发送推送通知(catid=31)
	const RESEARCH_CHECK_SUCCESS = '{{name}}的科研申请审核通过';
	//科研申请，审核失败发送推送通知(catid=31)
	const RESEARCH_CHECK_FAIL = '{{name}}的科研申请审核未通过，因{{reason}}';
	
	//科研申请，上传集赞照片审核成功是发送推送通知(catid=32)
	const RESEARCH_ZANPHOTO_CHECK_SUCCESS = '您的科研申请上传的集赞照片审核通过';
	//科研申请，上传集赞照片审核失败发送推动通知(catid=33)
	const RESEARCH_ZANPHOTO_CHECK_FAIL = '您的科研申请上传的集赞照片审核未通过，因{{reason}}';
	
	//健康教育审核通过(catid=21)
	const EDUCATION_CHECK_SUCCESS = '您录制的健康教育课件【{{title}}】已审核通过';
	//健康教育审核未通过(catid=21)
	const EDUCATION_CHECK_FAIL = '您录制的健康教育课件【{{title}}】审核未通过，因{{reason}}';
	
	//线下会议，报名成功(catid=41)
	const OUTLINE_APPLY_SUCCESS = '您的线下会议【{{title}}】报名成功';
	//线下会议，报名失败(catid=42)
	const OUTLINE_APPLY_FAIL = '您的线下会议【{{title}}】报名失败';
	
	/**
	 * 
	 * @param  $title 推送通知的标题，暂时为定值“国卫健康云”
	 * @param  $content 推送通知内容
	 * @param  $catid 栏目id
	 * @param  $iUserId 发送推送通知的用户id
	 * @param  $id 若通知点击进入的是详细页面，需要传入该详细页的id，否则传0
	 * @return boolean
	 */
	public static function postClient($title,$content,$catid,$iUserId,$id=0){
		include_once  ROOT.'/app/include/push/config/config.php';
		include_once  ROOT.'/app/include/push/class/push.class.php';  //先把类包含进来，实际路径根据实际情况进行修改。
		include_once  ROOT.'/app/include/push/lib/function.lib.php';
		$aIntParam = array(
				'_push_url'=>PUSH_URL,
				'_domain'=>PUSH_DOMAIN,
				'_title'=>$title,
				'_key'=>PUSH_KEY,
				'_sendtype'=>SENDTYPE_PUSH,
				'_open_type'=>2
		);
			
		$aCatType = array(11,12,13,14,15,21,31,32,33,41,42,51);
			
		if( !in_array($catid, $aCatType)){
			dd("栏目类型错误");
		}
		
		//记录推送log
		$aNotice = array(
				'user_id'		=> $iUserId,
				'notice_content'=> $content,
				'notice_type'	=> $catid,
				'detail_id'		=> $id
		);
		$oNotice = new AspirinUserNotice($aNotice);
		$oNotice->save();
		
		//获取设备token等信息
		$oPushDevice = AspirinUserDevice::where('user_id',$iUserId)->first();
		$sDeviceToken = '';
		$iDeviceType = 1;
		if($oPushDevice){
			$aCustom = array('id'=>$oNotice->id);
			$jCustom  = json_encode($aCustom);
			
			$sDeviceToken = $oPushDevice->token;
			$iDeviceType = $oPushDevice->device_type;
		
			$aIntParam['_devicetype'] = DEVICETYPE_OTHER;
			$oPush = new Push('post',$aIntParam);
			$setting = array();
			$setting['appid']= PUSH_APPID_IOS;
			if($iDeviceType == 2){
				//安卓
				$setting['appid']= PUSH_APPID_AND;
				$setting['user_id']= array($sDeviceToken);
			}else{
				//ios
				$setting['apnstokens']= array($sDeviceToken);
			}
			$setting['content']= trim(str_cut_cms($content, 60));
			$setting['message_type'] =  1;//2014-10-31添加消息类型0：消息（透传给应用的消息体）1：通知（对应设备上的消息通知）默认值为1。
			$setting['open_type']    =  2;
			$setting['custom_content'] = $jCustom;
			$return = runPost($oPush , $setting );
			$sPushLog = (isset($return['resno'])?$return['resno']:'').':'.(isset($return['prompt'])?$return['prompt']:'');
			if(!$sPushLog){
				$sPushLog = json_encode($return);
			}
		}else{
			$sPushLog = '尚未收集到该用户设备token';
		}
		$oNotice->push_log = $sPushLog;
		$oNotice->save();
		return true;
	}
	
	
	public static function postClientBatch($title,$content,$catid,$aUserId,$id=0){
		include_once  ROOT.'/app/include/push/config/config.php';
		include_once  ROOT.'/app/include/push/class/push.class.php';  //先把类包含进来，实际路径根据实际情况进行修改。
		include_once  ROOT.'/app/include/push/lib/function.lib.php';
		$aIntParam = array(
				'_push_url'=>PUSH_URL,
				'_domain'=>PUSH_DOMAIN,
				'_title'=>$title,
				'_key'=>PUSH_KEY,
				'_sendtype'=>SENDTYPE_PUSH,
				'_open_type'=>2
		);
			
		$aCatType = array(11,12,13,14,15,21,31,32,33,41,42);
			
		if( !in_array($catid, $aCatType)){
			dd("栏目类型错误");
		}
	
		//获取设备token等信息android
		$aPushServiceAndroid = AspirinUserDevice::whereIn('user_id',$aUserId)
								->where('device_type',2)
								->lists('token');
		
		//获取设备token等信息ios
		$aPushServiceIos = AspirinUserDevice::whereIn('user_id',$aUserId)
								->where('device_type',1)
								->lists('token');
		//已收集用户token的用户，可以推送
		$aUserPushId = AspirinUserDevice::whereIn('user_id',$aUserId)
								->lists('user_id');
		//未收集用户token的用户，不能推送
		$aUserNotPushId = array_diff($aUserId, $aUserPushId);
		//将未推送的用户推送信息计入数据库
		$aNoticeNotPush = array();
		$sCreatedAt = date('Y-m-d H:i:s');
		foreach($aUserNotPushId as $iUserId){
			$aNotice = array(
					'user_id'		=> $iUserId,
					'notice_content'=> $content,
					'notice_type'	=> $catid,
					'detail_id'		=> $id,
					'push_log'		=> '尚未收集到该用户设备token',
					'created_at'	=> $sCreatedAt
			);
			$aNoticeNotPush[] = $aNotice;
		}
		if(count($aNoticeNotPush)){
			//记录到数据库中
			AspirinUserNotice::insert($aNoticeNotPush);
		}
		//将已推送的用户推送信息计入数据库
		$aNoticePush = array();
		foreach($aUserPushId as $iUserId){
			$aNotice = array(
					'user_id'		=> $iUserId,
					'notice_content'=> $content,
					'notice_type'	=> $catid,
					'detail_id'		=> $id,
					'push_log'		=> 'batch_push',
					'created_at'	=> $sCreatedAt
			);
			$aNoticePush[] = $aNotice;
		}
		if(count($aNoticePush)){
			//记录到数据库中
			AspirinUserNotice::insert($aNoticePush);
		}
		//ios
		if(count($aPushServiceIos)){
			//每5000分为一组，缓解推送服务器压力
			$aChunk = array_chunk($aPushServiceIos,5000);
			foreach($aChunk as $v){
				$aCustom = array('id'=>0);
				$jCustom  = json_encode($aCustom);
				$aIntParam['_devicetype'] = DEVICETYPE_OTHER;
				$oPush = new Push('post',$aIntParam);
				$setting = array();
				$setting['appid']= PUSH_APPID_IOS;
				//ios
				$setting['apnstokens']= $v;
				
				$setting['content']= trim(str_cut_cms($content, 60));
				$setting['message_type'] =  1;//消息类型0：消息（透传给应用的消息体）1：通知（对应设备上的消息通知）默认值为1。
				$setting['open_type']    =  2;
				$setting['custom_content'] = $jCustom;
				$return = runPost($oPush , $setting );
			}
		}
		//android
		if(count($aPushServiceAndroid)){
			//每5000分为一组，缓解推送服务器压力
			$aChunk = array_chunk($aPushServiceAndroid,5000);
			foreach($aChunk as $v){
				$aCustom = array('id'=>0);
				$jCustom  = json_encode($aCustom);
				$aIntParam['_devicetype'] = DEVICETYPE_OTHER;
				$oPush = new Push('post',$aIntParam);
				$setting = array();
				//安卓
				$setting['appid']= PUSH_APPID_AND;
				$setting['user_id']= array($v);
		
				$setting['content']= trim(str_cut_cms($content, 60));
				$setting['message_type'] =  1;//消息类型0：消息（透传给应用的消息体）1：通知（对应设备上的消息通知）默认值为1。
				$setting['open_type']    =  2;
				$setting['custom_content'] = $jCustom;
				$return = runPost($oPush , $setting );
			}
		}
		return true;
	}
} 