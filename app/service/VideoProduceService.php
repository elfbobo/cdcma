<?php
/**
 * 视频合成器
 * 官方文档地址：
 * https://ffmpeg.org/ffmpeg.html
 */
class VideoProduceService
{
	//ffmpeg命令调用路径
	public static $sFFmpegCommand = '/usr/local/ffmpeg/bin/ffmpeg';
	//视频合成时生成的临时文件路径
	private static $sTmpUrl = '/upload/education/produce/';
	//临时文件路径--单次请求
	private static $sUserTmpUrl = '';
	
	private static $iImageCount = 0;

    /**
     * 临时文件路径写入器
     * @param $sUserTmpUrl
     */
	public static function setUserTmpUrl($sUserTmpUrl) {
	    self::$sUserTmpUrl = $sUserTmpUrl;
    }

    /**
     * 本次合成的图片数量写入器
     * @param $iImageCount
     */
	public static function setImageCount($iImageCount) {
	    self::$iImageCount = $iImageCount;
    }

	/**
	 * 调用视频合成程序
	 */
	public static function operate($aImageAndTime=array(),$sAudioUrl='',$iUserId = 0){
		
		set_time_limit(0);
		ini_set("memory_limit","512M");
		
		if(!$aImageAndTime || !$sAudioUrl){
			return 'error';
		}
		
		//本次合成的图片数量
		self::$iImageCount = count($aImageAndTime);
		
		//为当前用户创建文件存储路径
		self::$sUserTmpUrl = public_path().self::$sTmpUrl.$iUserId.'/'.date('YmdHis',time()).rand(1000,9999);
		mkdirs(self::$sUserTmpUrl);
		//将音频文件复制到当前文件夹
		copy(public_path().$sAudioUrl,self::$sUserTmpUrl.'/audio.mp3');

        //记录音频真实来源,用于异常音频的追踪
        file_put_contents(self::$sUserTmpUrl.'/copy_audio_source.txt', json_encode(array('source'=>public_path().$sAudioUrl, 'dest'=>self::$sUserTmpUrl.'/audio.mp3')),FILE_APPEND);

		//图片停留时间数据备份
		file_put_contents(self::$sUserTmpUrl.'/image_and_time.txt',json_encode($aImageAndTime),FILE_APPEND);
		
		//1、图片生成mp4文件
		self::imageToMp4($aImageAndTime);
		
		//2、MP4文件合并
		$sNoVoiceMp4 = self::noVoiceMp4();
		
		//3、与音频合并
		//$sOutPutUrl = self::makeVoiceMp4("/disk-500/web/server/public/upload/education/audio/20180802/3213211.mp3",public_path().$sAudioUrl);
		//echo public_path().$sAudioUrl;
		//var_dump($sOutPutUrl);
		//die;
		$sOutPutUrl = self::makeVoiceMp4($sNoVoiceMp4,public_path().$sAudioUrl);
		
		//删除临时文件
		self::delTmpFile();
		
		//返回成功合成的视频路径
		$sVideoUrl = Config::get('app.url').str_replace(public_path(), '', $sOutPutUrl);
		return $sVideoUrl;
	}

    /**
     * 重新合成指定文件
     * @param $sOutPutUrl
     * @return string
     */
	public static function reOperate($sOutPutUrl)
    {
        set_time_limit(0);
        ini_set("memory_limit","512M");

        $sUserTmpUrl = dirname($sOutPutUrl);
        // 如果合成文件不存在时重新合成
        if (!file_exists(public_path() . $sUserTmpUrl . 'complex.mp4')) {

            // 当前用户临时缓存
            self::$sUserTmpUrl = $sUserTmpUrl;

            //图片停留时间数据备份
            $image_and_time = json_decode(file_get_contents($sUserTmpUrl . '/image_and_time.txt'), true);

            // ppt总数
            self::$iImageCount = count($image_and_time);

            //1、图片生成mp4文件
            self::imageToMp4($image_and_time);

            //2、MP4文件合并
            $sNoVoiceMp4 = self::noVoiceMp4();

            //3、与音频合并
            $sOutPutUrl = self::makeVoiceMp4($sNoVoiceMp4, $sUserTmpUrl . '/audio.mp3');

            // 删除临时文件
            VideoProduceService::delTmpFile();
        }
        return $sOutPutUrl;
    }
	
	/**
	 * 图片生成无声的mp4文件
	 * @param unknown $aImageAndTime
	 */
	public static function imageToMp4($aImageAndTime){
		$i = 1;
		foreach ($aImageAndTime as $a){
			$iTime = $a['time'];
			// 当图片停留时间为0秒时不处理
            if (!$iTime) continue;
			$sUrl = public_path().$a['url'];
			/**
			 * -r 1 设置帧速率
			 * -y 若文件存在则覆盖
			 * -loop 1 循环输入流
			 * -i 输入源
			 * -c:v mpeg4 输出视频格式
			 * -t 时间
			*/
			$sCommond = self::$sFFmpegCommand." -threads 2 -r 1 -y -loop 1 -i ".$sUrl." -t ".$iTime." ".self::$sUserTmpUrl.'/'.$i.".mp4";
			// $sCommond = self::$sFFmpegCommand." -r 1 -y -loop 1 -i ".$sUrl." -c:v libx264 -t ".$iTime." -vf scale=-2:640 ".self::$sUserTmpUrl.'/'.$i.".mp4";
// 			$sCommond = self::$sFFmpegCommand." -r 1 -y -loop 1 -i ".$sUrl." -c:v libx264 -t ".$iTime." ".self::$sUserTmpUrl.'/'.$i.".mp4 2>&1";
			$output = array();
			exec($sCommond,$output);
			$i++;
		}
	}
	
	/**
	 * 所有mp4文件合并
	 */
	public static function noVoiceMp4(){
		
		//输入源文件地址
		$sTextUrl = self::$sUserTmpUrl."/pics.txt";
		$sTextContent = "";
		for($i=1;$i<=self::$iImageCount;$i++){
			if(!$sTextContent){
				$sTextContent = "file '".$i.".mp4'";
			}else{
				$sTextContent .= "\r\n"."file '".$i.".mp4'";
			}
		}
		if (!file_exists($sTextUrl)) {
            file_put_contents($sTextUrl, $sTextContent, FILE_APPEND);
        }
		
		//生成的无声mp4文件地址
		$sNoVoiceMp4 = self::$sUserTmpUrl.'/pics.mp4';
		/**
		 * -y 若文件存在则覆盖
		 * -f 输入源为文件
		 * concat 合并
		 * -i 输入源
		 * -r 复制或删除输入帧以达到恒定的输出帧速率FPS
		 * -c copy 采用源视频编码
		 */
		$sCommond = self::$sFFmpegCommand." -y -f concat -i ".$sTextUrl." -r 1 -c copy ".$sNoVoiceMp4;
		$sCommond = self::$sFFmpegCommand." -threads 2 -y -f concat -i ".$sTextUrl." -c copy ".$sNoVoiceMp4." 2>&1";

		exec($sCommond,$output);
		return $sNoVoiceMp4;
	}
	
	/**
	 * 合成最终的有声视频
	 * @param unknown $sNoVoiceMp4
	 * @param unknown $sAudioUrl
	 * @return string
	 */
	public static function makeVoiceMp4($sNoVoiceMp4,$sAudioUrl){
		$sOutPutUrl = self::$sUserTmpUrl.'/'.'complex'.'.mp4';
		/**
		 * -y 若文件存在则覆盖
		 * -i 输入源
		 * -vf Create the filtergraph specified by filtergraph and use it to filter the stream.
		 * fps=1 设置帧速率
		 * -absf aac_adtstoasc 特定配置比特流过滤器
		 * -r 1 :复制或删除输入帧以达到恒定的输出帧速率FPS
		 */
		$sCommond = self::$sFFmpegCommand." -threads 2 -y -i ".$sNoVoiceMp4." -i ".$sAudioUrl." -vf fps=1 -absf aac_adtstoasc -r 1 ".$sOutPutUrl;

		exec($sCommond,$output);
		return $sOutPutUrl;
	}
	
	public static function delTmpFile(){
		//删除掉单独图片生成的无声MP4
		for($i=1;$i<=self::$iImageCount;$i++){
			$sFile = self::$sUserTmpUrl.'/'.$i.'.mp4';
			if(file_exists($sFile)){
				@unlink($sFile);
			}
		}
		
		//删除掉无声的mp4文件
		$sFile = self::$sUserTmpUrl.'/pics.mp4';
		@unlink($sFile);
		
		//删除临时生成的连接文件
		$sFile = self::$sUserTmpUrl.'/pics.txt';
		@unlink($sFile);
	}
	
	//截取视频的一小段 
	//ffmpeg  -i ./plutopr.mp4 -vcodec copy -acodec copy -ss 00:00:10 -to 00:00:15 ./cutout1.mp4 -y
	//参考网页：http://blog.csdn.net/huangxingli/article/details/46663143
	//测试 phpworks/test/ffmpeg/file/test
	// /usr/local/ffmpeg/bin/ffmpeg -i ./test.mp4 -vcodec copy -acodec copy -ss 00:40:01 -to 00:48:15 ./test4.mp4 -y
	
	
	
} 