<?php
/**
 * 麦粒相关服务
 * 
 * 1267		bayer_manbing	国卫健康云_线下会议报名(catid=41)(减麦粒)
 * 1266		bayer_manbing	国卫健康云_筛查(catid=51)
 * 1265		bayer_manbing	国卫健康云_科研申请(catid=31)
 * 1264		bayer_manbing	国卫健康云_健康教育_录制课件(catid=21)
 * 1263		bayer_manbing	国卫健康云_在线会议_参与互动(catid=12)
 * 1262		bayer_manbing	国卫健康云_在线会议_观看直播(catid=11)
 * 
 * 麦粒功能模块：10个麦粒=1元
 * 1.	线下会议：暂定设限最低10麦粒起，申请医生自行输入麦粒数额，价高者得。(catid=41)(减麦粒)
 * 2.	在线会议：参与直播会议观看超过20分钟获得10麦粒，申请人无麦粒(catid=11)
 * 3.	在线会议：参与互动会议，参与方参与一次会议讨论获得20麦粒，申请人无麦粒(catid=12)
 * 4.	健康教育：医生录制一套患者音频奖励50麦粒。(catid=21)
 * 5.	科研申请：上传完整并通过审核的科研报告奖励50麦粒(catid=31)
 * 6.	筛查：筛查一次获得1麦粒。(catid=51)
 */
class GoldService
{
	public static $iReduceGoldTypeId = '1267';
	/**
	 * 加麦粒操作
	 * @param $iCatId
	 * @param $iSourceId
	 * @param $iGold  不传加默认麦粒数
	 */
	public static function addGold($iUserId,$iCatId,$iSourceId){
		return true;
	}
	
	/**
	 * 减麦粒操作
	 * @param $iCatId
	 * @param $iSourceId
	 * @param $iGold  不传减默认麦粒数
	 */
	public static function reduceGold($iUserId,$iCatId,$iSourceId,$iGold=0){
		return true;
	}
	
	/**
	 * 获取指定功能所加麦粒数
	 * @return multitype:number
	 */
	public static function getCatAddGold(){
		return array(
				11 => 1,
				12 => 2,
				21 => 3,
				31 => 4,
				51 => 1
		);
		
	}
	
	/**
	 * 获取指定功能typeid
	 * @return multitype:number
	 */
	public static function getCatTypeId($iCatId){
		$aTypeId = array(
				11 => '1262',
				12 => '1263',
				21 => '1264',
				31 => '1265',
				51 => '1266',
				41 => '1267'
		);
		if(isset($aTypeId[$iCatId])){
			return $aTypeId[$iCatId];
		}else{
			return 0;
		}
	}
	
	/**
	 * 获取指定功能所减麦粒数
	 * @return multitype:number
	 */
	public static function getCatReduceGold(){
		return array(
				41 => 0
		);
	}
	
} 