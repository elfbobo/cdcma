<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
| 应用程序 & 路由 过滤器
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| 下面你将会找到应用程序的 "before" 和 "after" 事件，
| which may be used to do any work before or after a request into your
| 它可用于执行任何工作，在请求到达应用程序之前或之后。
| application. Here you may also register your custom route filters.
| 这里你也可以注册自定义的路由过滤器。
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/**
 * 应用程序事件执行顺序如下：
 *     1.执行应用程序事件          App::before($request)
 *     2.执行前置操作                 Route::filter($route, $request)
 *     3.执行了之前注册进路由的回调函数，并获取响应实例
 *     4.执行后置操作                 Route::filter($route, $request, $response)
 *     5.执行应用程序事件          App::after($request, $response)
 *     6.发送 3 中获取的响应实例
 *     7.执行应用程序事件          App::finish($request, $response)
 *     8.执行应用程序事件          App::shutdown($application)
 */

// App::finish(function($request, $response)
// {
        
// });

// App::shutdown(function($application)
// {
        
// });

/*
|--------------------------------------------------------------------------
| Authentication Filters
| 验证过滤器
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| 下面的过滤器是用于验证当前会话的用户，
| session is logged into this application. The "basic" filter easily
| 是否已经登录到应用程序。"basic" 过滤器
| integrates HTTP Basic authentication for quick, simple checking.
| 轻松集成了 HTTP 基础身份验证，来进行快速简单的检查。
|
*/

Route::filter('auth', function()
{
	if (!Auth::check()) {
	//	return Redirect::guest('login');
//		return "<script>alert('请您先登录');window.history.go()</script>";die;
		return Redirect::to('/');
	}else{
		if(!Session::has('roleid')||!Session::has('userid')){
			Session::put('roleid',Auth::User()->role_id);//用户角色
			Session::put('userid',Auth::User()->id);//用户id
		}
	}	
});
Route::filter('aspirinpcauth', function()
{
	if (!Auth::check()) {
		return Redirect::to('/');
	}else{
		/*$aPilotCity = User::getPilotCity();
		$aPilotCountry = User::getPilotCountry();
		if(Auth::User()->role_id == 3){
			if(!in_array(Auth::User()->user_city,$aPilotCity) && !in_array(Auth::User()->user_county,$aPilotCountry)){
				//非试点城市无法进入专项基金
				return "<script>alert('仅限试点城市用户使用');window.location.href='/';</script>";die;
			}
		}*/
		if(!Session::has('roleid')||!Session::has('userid')){
			Session::put('roleid',Auth::User()->role_id);//用户角色
			Session::put('userid',Auth::User()->id);//用户id
		}
	}
});
//空中课堂(点击菜单后先跳登录页，登录完留在本页)
Route::filter('mobileauth', function()
{
	if (!Auth::check()) {
		return Redirect::to('/mobile/login/1');
	}else{
		if(!Session::has('roleid')||!Session::has('userid')){
			Session::put('roleid',Auth::User()->role_id);//用户角色
			Session::put('userid',Auth::User()->id);//用户id
		}
	}
});
//专项基金(点击菜单后先跳登录页，登录完留在本页)
Route::filter('mobileauth2', function()
{
	if (!Auth::check()) {
		return Redirect::to('/mobile/login/2');
	}else{
		if(!Session::has('roleid')||!Session::has('userid')){
			Session::put('roleid',Auth::User()->role_id);//用户角色
			Session::put('userid',Auth::User()->id);//用户id
		}
	}
});
//个人中心(点击菜单后先跳登录页，登录完留在本页)
Route::filter('mobileauth3', function()
{
	if (!Auth::check()) {
		return Redirect::to('/mobile/login');
	}else{
		if(!Session::has('roleid')||!Session::has('userid')){
			Session::put('roleid',Auth::User()->role_id);//用户角色
			Session::put('userid',Auth::User()->id);//用户id
		}
	}
});
//手机版专项基金用户权限验证：是否是试点城市
Route::filter('cityauth', function()
{
	if (!Auth::check()) {
		return Redirect::to('/');
	}else{
		/*$aPilotCity = User::getPilotCity();
		$aPilotCountry = User::getPilotCountry();
		if(Auth::User()->role_id == 3){
			if(!in_array(Auth::User()->user_city,$aPilotCity) && !in_array(Auth::User()->user_county,$aPilotCountry)){
				//非试点城市无法进入专项基金
				return Redirect::to('/mobile-aspirin/warning');
			}
		}*/
	}
});

//前台自助会议平台(仅代表可以进入)
Route::filter('meetingauth', function()
{
	if (!Auth::check()){
		return Redirect::guest('/');
	} else if(Auth::User()->role_id!=2){
		return Redirect::guest('/');
	}
});

//后台超管
Route::filter('admauth', function()
{
	if (!Auth::check()){
		return Redirect::guest('admlogin');
	} else if(Auth::User()->role_id!=1){
		return Redirect::guest('admlogin');
	}
});

//后台病例征集
Route::filter('admcase', function()
{
	if (!Auth::check()){
		return Redirect::guest('admlogin');
	} else {
		$oUserAdminCase = UserAdminCase::where('user_id',Auth::User()->id)->first();
		if(!count($oUserAdminCase)&&Auth::User()->role_id!=1){
			return Redirect::guest('admlogin');
		}
	}
});
//后台资讯中心管理
Route::filter('adminformation', function()
{
	if (!Auth::check()){ 
		return Redirect::guest('admlogin');
	} else {
		$id = Auth::User()->id;   
		$aInformUser = Config::get('config.information');   
	    if(!in_array($id,$aInformUser) && Auth::User()->role_id != 1){  
				return Redirect::guest('admlogin'); 
		}  
	}
});
//后台小调研权限
Route::filter('admresearch', function()
{
	if (!Auth::check()){ 
		return Redirect::guest('admlogin');
	} else {
		$id = Auth::User()->id;   
		$aResearch = Config::get('config.research');   
	    if(!in_array($id,$aResearch) && Auth::User()->role_id != 1){  
				return Redirect::guest('admlogin'); 
		}  
	}
});
//名医面对面权限
Route::filter('admdocface', function()
{
	if (!Auth::check()){
		return Redirect::guest('admlogin');
	} else {
		$id = Auth::User()->id;
		$aDocFace = Config::get('config.docface');
		if(!in_array($id,$aDocFace) && Auth::User()->role_id != 1){
			return Redirect::guest('admlogin');
		}
	}
});
//后台管理员权限过滤器，有后台部分权限的用户都可以通过该过滤器
//超管，病例征集，资讯中心，小调研 ，名医面对面 目前有这五种权限的用户
Route::filter('admallauth', function()
{
	if (!Auth::check()){
		return Redirect::guest('admlogin');
	} else {
		//若当前用户不是超管，判断是否有其他权限
		if(Auth::User()->role_id!=1){
			$oUserAdminCase = UserAdminCase::where('user_id',Auth::User()->id)->first();
			//若没有病例征集管理权限,判断是否有其他权限
			if(!count($oUserAdminCase)){
				$aInformUser = Config::get('config.information');
				//若没有资讯中心管理权限，判断是否有其他权限
				if(!in_array(Auth::User()->id,$aInformUser)){
					$aResearch = Config::get('config.research');
					//若没有小调研管理权限，判断是否有其他权限
					if(!in_array(Auth::User()->id,$aResearch)){
						$aDocFace = Config::get('config.docface');
						//若没有名医面对面管理权限
						if(!in_array(Auth::User()->id, $aDocFace)){
							//没有后台所有权限，跳到登录页面
							return Redirect::guest('admlogin');
						}
					}
				}
			}
		}
	}
});
Route::filter('auth.basic', function()
{
	return Auth::basic();
});

//liuy  API 登录过滤  S ==============================

//api接口验证
Route::filter('apilogin', function()
{	
	global $iUserId;
	if(Input::has('token')){//token 包含用户信息，校验
		//提取token
		$token = Input::get('token');
		   //验证token
		   try{
		   		 $userInfo = explode('\t', Crypt::decrypt($token));
		   }
		   catch(Exception $e) {
		   		return json_encode(array('success'=>false,'msg'=>'Token验证失败', 'err_code' => 1001));
		   }
		   $iUserId = (!empty($userInfo[0])) && is_numeric($userInfo[0]) ? $userInfo[0] : 0; 
			if( $iUserId ){
			   	    $oUser  = User::find( $iUserId );//获取用户信息
			   		if( !$oUser ){
			   			return json_encode(array('success'=>false,'msg'=>'Token验证失败', 'err_code' => 1001));
			   		}
			   }else{
			   		return json_encode(array('success'=>false,'msg'=>'Token验证失败', 'err_code' => 1001));
			   }
	}else{
		return json_encode(array('success' => false, 'msg' => 'token为空，校验失败！', 'err_code' => 1000));
	}
});

//判断不是医生用户返回错误信息
Route::filter('apionlydoc', function()
{
	global $iUserId;
	$iRoleId  = User::where('id', $iUserId )->pluck('role_id');//获取用户信息
	if($iRoleId!=3){
		return json_encode(array('success'=>false,'msg'=>'该功能只对医生用户开放权限'));
	}
});
//判断不是认证的医生用户返回错误信息
Route::filter('apidoc', function()
{
	global $iUserId;
	$oUser = User::find($iUserId);
	$iRoleId  = $oUser->role_id;//获取用户信息
	if($iRoleId!=3 && $iRoleId!=2){  //代表也可参与
		return json_encode(array('success'=>false,'msg'=>'该功能只对医生用户开放权限'));
	}
	if($iRoleId == 3 && $oUser->card_auth_flag != 2){
		return json_encode(array('success'=>false,'msg'=>'医师认证审核还未通过，不可参与'));
	}
}); 
//liuy  API 登录过滤  E ==============================

/*
|--------------------------------------------------------------------------
| Guest Filter
| 游客过滤器
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| "guest" 过滤器与上面的验证过滤器对应，
| it simply checks that the current user is not logged in. A redirect
| 它只是简单的检查当前用户是未登录的。
| response will be issued if they are, which you may freely change.
| 如果他已经登录，那么一个重定向响应将会被发出，你可以自由的修改。
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
| CSRF保护 过滤器
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| 该 CSRF 过滤器是负责保护您的应用程序，
| cross-site request forgery attacks. If this special token in a user
| 针对跨站点请求伪造攻击。如果这个用户会话中的特殊令牌
| session does not match the one given in this request, we'll bail.
| 在这个请求中无法匹配一个给定的值，我们将给予保护。
|
*/

Route::filter('csrf', function()
{
	if (Session::token() !== Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

/*
|--------------------------------------------------------------------------
| 自定义 [前置] 过滤器
|--------------------------------------------------------------------------
|
*/

// Route::filter('beforeFilter', function($route, $request)
// {
        
// });

/*
|--------------------------------------------------------------------------
| 自定义 [后置] 过滤器
|--------------------------------------------------------------------------
|
*/

// Route::filter('afterFilter', function($route, $request, $response)
// {
        
// });
