<?php

use Illuminate\Support\Facades\Schema;

interface weixin
{
    public function getIndex();                                //入口（处理主要业务）

    public function getAddTemporaryMaterial($data, $type, $newsType);    //上传临时素材

    public function getAddTemporaryMaterialNews($data, $newsType);        //上传临时图文素材

    public function getPreview($data, $newsType);                        //预览接口

    public function getPush($data, $newsType);                            //根据分组进行群发

    public static function getAddMaterialContentImg($data);    //图文素材中图片上传
}

class WeixinController extends BaseController implements weixin
{
    private $postObj;
    private $fromUserName;
    private $toUserName;
    private $content;
    private $wx;
    private $msg_array;

    public function __construct(WeixinBaseController $wx)
    {
        $this->wx = $wx;
        $this->msg_array = array();
    }

    //入口（处理主要业务）
    public function getIndex()
    {
        if (ob_get_contents()) {
            ob_end_clean();
        }
        $postStr = isset($GLOBALS["HTTP_RAW_POST_DATA"]) ? $GLOBALS["HTTP_RAW_POST_DATA"] : '';//获取post过来的数据
        if (!empty($postStr)) {
            $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
            $this->postObj = $postObj;
            $this->fromUserName = $postObj->FromUserName;//发送方
            $this->toUserName = $postObj->ToUserName;//接收方（即本公众账号）
            $this->content = trim($postObj->Content);
            $time = time();
            $textTpl = "<xml>
							<ToUserName><![CDATA[%s]]></ToUserName>
							<FromUserName><![CDATA[%s]]></FromUserName>
							<CreateTime>%s</CreateTime>
							<MsgType><![CDATA[%s]]></MsgType>
							<Content><![CDATA[%s]]></Content>
							<FuncFlag>0</FuncFlag>
							</xml>";
            $msgType = "text";
            $contentStr = $this->dealMsg();
            $resultStr = sprintf($textTpl, $this->fromUserName, $this->toUserName, $time, $msgType, $contentStr);
            $this->saveMsgOut($this->fromUserName, $this->toUserName, $time, $msgType, $contentStr);//记录回复的信息
            echo $resultStr;
            exit;
        } else {
            $this->wx->makeInfo(Input::all());
            $this->wx->valid();//非消息推送时，默认为校验
        }
        flush();
    }

    private function dealMsg()
    {
        $postObj = $this->postObj;
        $fromUsername = $postObj->FromUserName;//发送方
        $toUsername = $postObj->ToUserName;//开发者微信号
        $keyword = trim($postObj->Content);//文本消息内容
        $FMsgType = trim($postObj->MsgType);//消息类型

        $this->saveMsgIn();//记录收到的消息
        $contentStr = $this->classMsg();//回复的内容
        return $contentStr;
    }

    /**
     * 保存收到的消息(文本、图片、位置、链接等)
     */
    private function saveMsgIn()
    {
        $info = (array)$this->postObj;
        $fields = $this->get_fields('weixin_msg_in');
        $arr = array();
        $other = '';
        foreach ($info as $key => $value) {
            if (in_array($key, $fields)) {
                $arr[$key] = addslashes($value);
            }
            $other .= $key . ":" . $value . ";<br/>";
        }
        $arr['other'] = addslashes($other);
        DB::table('weixin_msg_in')->insert($arr);
    }

    /**
     * 保存回复的信息(目前只支持文本消息)
     */
    private function saveMsgOut($ToUserName, $FromUserName, $CreateTime, $MsgType, $Content)
    {
        $arr = array(
            'ToUserName' => $ToUserName,
            'FromUserName' => $FromUserName,
            'CreateTime' => $CreateTime,
            'MsgType' => $MsgType,
            'Content' => addslashes($Content)
        );
        DB::table('weixin_msg_out')->insert($arr);
    }

    /**
     * 获取数据表的表结构
     */
    private function get_fields($table)
    {
        $columns = Schema::getColumnListing($table);
        return $columns;
    }

    private function classMsg()
    {
        $postObj = $this->postObj;
        $MsgType = trim($postObj->MsgType);//消息类型
        if ($MsgType == 'text') {
            return $this->dealMsgText(); //文本
        } elseif ($MsgType == 'event') {
            return $this->dealMsgEvent(); //事件
        } else {
            return str_replace('{$MsgType}', $MsgType, Lang::get('weixin.other_type'));
        }
    }

    /**
     * 处理文本消息
     */
    private function dealMsgText()
    {
        $postObj = $this->postObj;
        $keyword = trim($postObj->Content);//文本消息内容
        $FMsgType = trim($postObj->MsgType);//消息类型
        $keyword = make_semiangle($keyword);//变成半角
        $keyword = str_replace(' ', '', $keyword);//空格删除
        $keyword = strtoupper($keyword);
        $keyword = addslashes($keyword);

        //消息内容逗号分割
        $this->msg_array = explode(',', $keyword);
        return;
        return str_replace('{$MsgType}', $FMsgType, Lang::get('weixin.other_type'));    //默认回复
    }

    /**
     * 处理event类型消息
     */
    private function dealMsgEvent()
    {
        $postObj = $this->postObj;
        $toUsername = $postObj->ToUserName;//本公众账号
        $MsgType = trim($postObj->MsgType);//消息类型
        $Event = trim($postObj->Event);
        if ($Event == 'subscribe') {  //新用户订阅
            if ($toUsername == 'gh_31eeb343e584') {
                return Lang::get('weixin.cdtm_welcome');
            } elseif ($toUsername == 'gh_8dd92cf8f227') {
                return Lang::get('weixin.cmacardio_welcome');
            } elseif ($toUsername == 'gh_12f767158242') {
                return Lang::get('weixin.cmaresp_welcome');
            } else {
                return Lang::get('weixin.welcome');
            }
        } elseif ($Event == 'unsubscribe') {  //取消关注
            return Lang::get('weixin.cancel');
        }
        return;
        return str_replace('{$MsgType}', $MsgType, Lang::get('weixin.other_type'));
    }

    /**
     * 上传临时素材($newsType 对应不同公众号)
     */
    public function getAddTemporaryMaterial($data, $type, $newsType)
    {
        return $this->wx->getBaseAddTemporaryMaterial($data, $type, $newsType);
    }

    /**
     * 上传临时图文素材
     */
    public function getAddTemporaryMaterialNews($data, $newsType)
    {
        return $this->wx->getBaseAddTemporaryMaterialNews($data, $newsType);
    }

    /**
     * 根据分组进行群发
     */
    public function getPush($data, $newsType)
    {
        return $this->wx->getBasePush($data, $newsType);
    }

    /**
     * 预览接口
     */
    public function getPreview($data, $newsType)
    {
        return $this->wx->getBasePreview($data, $newsType);
    }

    /**
     * 图文素材内容中图片上传接口
     */
    public static function getAddMaterialContentImg($data)
    {
        return WeixinBaseController::getBaseAddMaterialContentImg($data);
    }
}