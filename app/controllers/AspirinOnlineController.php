<?php

//pc端专项基金在线会议
class AspirinOnlineController extends BaseController {

	//会议申请者可提前多长时间开启会议（单位：分钟）
	const ADVANCETIME = 10;
	//不足5人参加时取消会议
	const LIMIT_CANCEL_COUNT = 5;
	const PAGESIZE = 6;
	const CATID = 1;
	private $iUserId = 0;
	private $iRoleId = 0;
	public $iCacheTime;

	public function __construct(){
		$this->iCacheTime = 60; //1小时
		$this->iUserId = Session::get('userid');
		$this->iRoleId = Session::get('roleid');
	}
	
	//在线会议-查看课件列表页
	public function getPptList($iCatId=1)
	{
		$oPpt = AspirinOnlinePpt::where('catid',$iCatId)->orderby('id','asc')->get();
		return View::make('front.aspirin.online.pptlist')->with('oPpt',$oPpt)->with('iCatId',$iCatId);
	}
	
	//在线会议-查看课件详情页
	public function getPptShow($iPptId,$iFlag=0)
	{
		$oPpt = AspirinOnlinePpt::find($iPptId);
		$oPptImage = AspirinOnlinePptImage::where('ppt_id',$iPptId)->orderby('id','asc')->get();
		return View::make('front.aspirin.online.pptshow')->with('oPpt',$oPpt)->with('oPptImage',$oPptImage)->with('iFlag',$iFlag);
	}
	
	//在线会议-精彩回顾列表页
	public function getReviewList($iCatId=0)
	{
		$oVideoList = AspirinOnlineSpeaker::where('open_flag',1)->where('cancel_flag',0)->where('video_url','!=','');
		if($iCatId == 1 || $iCatId == 2){
			$oVideoList = $oVideoList->where('online_catid',$iCatId);
		}
		$oVideoList = $oVideoList->orderBy('created_at','desc')->paginate(self::PAGESIZE);
		foreach($oVideoList as $k=>$v){
			$oPpt = AspirinOnlinePpt::find($v->ppt_id);
			$oVideoList[$k]['ppt_title'] = $oPpt?$oPpt->ppt_title:'';
			$oVideoList[$k]['ppt_thumb'] = $oPpt?$oPpt->ppt_thumb:'';
			$oUser = User::select('user_name','user_company','user_company_name')->where('id',$v->speaker_id)->first();
			if($oUser){
				$oVideoList[$k]['doc_name'] = $oUser->user_name;
				$sUserCompany = $oUser->user_company;
				if(is_numeric($sUserCompany)&&$sUserCompany!=0){
					$sUserCompany = Hospital::where('id',$sUserCompany)->pluck('name');
				}
				if(!$sUserCompany){
					$sUserCompany = $oUser->user_company_name;
				}
				$oVideoList[$k]['doc_hospital'] = $sUserCompany;
			}
		}
		$iCountAll = AspirinOnlineSpeaker::where('open_flag',1)->where('cancel_flag',0)->where('video_url','!=','')->count();
		$iCount1 = AspirinOnlineSpeaker::where('open_flag',1)->where('cancel_flag',0)->where('online_catid',1)->where('video_url','!=','')->count();
		$iCount2 = AspirinOnlineSpeaker::where('open_flag',1)->where('cancel_flag',0)->where('online_catid',2)->where('video_url','!=','')->count();
		return View::make('front.aspirin.online.reviewlist')->with('oVideoList',$oVideoList)
			->with('iCountAll',$iCountAll)
			->with('iCount1',$iCount1)
			->with('iCount2',$iCount2)
			->with('iCatId',$iCatId);
	}

	//在线会议-精彩回顾详情页
	public function getReviewShow($iVideoId)
	{
		$iPage = Input::get('page',1);
		$iPagesize = Input::get('pagesize',self::PAGESIZE);
		$oVideo = AspirinOnlineSpeaker::find($iVideoId);
		if($oVideo){
			$oOnlineTime = AspirinOnlineTime::find($oVideo->online_time_id);
			$oPpt = AspirinOnlinePpt::find($oVideo->ppt_id);
			//浏览量+1
			$oVideo->increment('video_hits');
			//是否点过赞
			$iZan = AspirinSupportLog::hasSupport(self::CATID, $this->iUserId, $iVideoId);
			//专家信息
			$oUser = User::select('user_name','user_company','user_company_name')
				->where('id',$oVideo->speaker_id)
				->first();
			$sUserCompany = $oUser->user_company;
			if(is_numeric($sUserCompany)&&$sUserCompany!=0){
				$sUserCompany = Hospital::where('id',$sUserCompany)->pluck('name');
			}
			if(!$sUserCompany){
				$sUserCompany = $oUser->user_company_name;
			}
			//星级评分
			$oScoreLog = AspirinOnlineVideoScore::where('video_id',$iVideoId)->where('user_id',$this->iUserId)->first();
			//评论列表
			$oComments = AspirinOnlineComment::getComment($iVideoId,0,0);
		}
		$sImgLi = Images();
		return View::make('front.aspirin.online.reviewshow')->with('iVideoId',$iVideoId)
			->with('oOnlineTime',$oOnlineTime)
			->with('oPpt',$oPpt)
			->with('iZan',$iZan)
			->with('oUser',$oUser)
			->with('sUserCompany',$sUserCompany)
			->with('oScoreLog',$oScoreLog)
			->with('oComments',$oComments)
			->with('iUserId',$this->iUserId)
			->with('sImgLi',$sImgLi)
			->with('oVideo',$oVideo);
	}

	//在线会议-精彩回顾-点赞
	public function postZanVideo()
	{
		$iVideoId = intval(Input::get('videoid'));
		$oVideo = AspirinOnlineSpeaker::find($iVideoId);
		if(!$oVideo){
			echo 'error';die;
		}
		//查找当前用户是否点赞
		$iHasSupport = AspirinSupportLog::hasSupport(self::CATID,$this->iUserId,$iVideoId);
		if($iHasSupport){
			echo 'again';die;
		}
		//新增一条点赞记录
		$sUserAgent = isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'';
		$aSupportLog = array(
			'cat_id'	=> self::CATID,
			'user_id'	=> $this->iUserId,
			'source_id'	=> $iVideoId,
			'user_agent'=> $sUserAgent,
			'ip'        => GetIP(),
			'created_at'=> date('Y-m-d H:i:s',time())
		);
		$oSupportLog = new AspirinSupportLog($aSupportLog);
		$oSupportLog->save();
		//更新当前视频的点赞总数
		$oVideo->increment('video_zan');
		echo 'success';die;
	}

	//在线会议-精彩回顾-星级评分
	public function postScore()
	{
		$iVideoId = intval(Input::get('videoid'));
		$iScore1 = Input::get('score1',0);
		$iScore2 = Input::get('score2',0);
		$iScore3 = Input::get('score3',0);
		AspirinOnlineVideoScore::submitScore($this->iUserId,$iVideoId,$iScore1,$iScore2,$iScore3);
	}

	//在线会议-精彩回顾-评论
	public function postComment()
	{
		$iVideoId = intval(Input::get('videoid'));
		$oVideo = AspirinOnlineSpeaker::where('id',$iVideoId)->first();
		if(!$oVideo){
			echo 'error';die;
		}
		$aComment = array(
			'video_id'  => $iVideoId,
			'user_id'   => $this->iUserId,
			'comment'   => trim(Input::get('comment')),
			'created_at'=> date('Y-m-d H:i:s',time())
		);
		$oComment = new AspirinOnlineComment($aComment);
		$oComment->save();
		echo 'success';die;
	}

	//在线会议-精彩回顾-评论点赞
	public function postZanComment()
	{
		$iCommentId = intval(Input::get('commentid'));
		$oComment = AspirinOnlineComment::find($iCommentId);
		if(!$oComment){
			echo 'error';die;
		}
		//查找当前用户是否点赞
		$oZanLog = AspirinOnlineCommentZanLog::where('comment_id',$iCommentId)->where('user_id',$this->iUserId)->first();
		if($oZanLog){
			echo 'again';die;
		}
		//新增一条点赞记录
		$aZanLog = array(
			'comment_id'=> $iCommentId,
			'user_id'	=> $this->iUserId,
			'created_at'=> date('Y-m-d H:i:s',time())
		);
		$oZanLog = new AspirinOnlineCommentZanLog($aZanLog);
		$oZanLog->save();
		//更新当前评论的点赞总数
		$oComment->increment('zan_count');
		echo 'success';die;
	}
	
	//在线会议讲者报名先选课件
	public function getPpt($iCatId=1)
	{
		$sNowTime = date('Y-m-d H:i:s',time());
		$oOnline = AspirinOnline::where('catid',$iCatId)->where('show_end_time','>',$sNowTime)->orderby('created_at','desc')->first();
		$oPreOpen = AspirinOnlineSpeaker::where('online_catid',$iCatId)->where('open_flag',0)->where('cancel_flag',0)->get();
		$oHasOpen = AspirinOnlineSpeaker::where('online_catid',$iCatId)->where('open_flag',1)->get();
		$iListenerCount = 0;
		foreach($oHasOpen as $k=>$v){
			$iListenerCount+= $v->listener_count;
		}
		$iHasOpen = count($oHasOpen)!=0?count($oHasOpen):0;     //成功举办的场数
		$iTotalCount = count($oHasOpen) + $iListenerCount;      //参与人数
		$iPreOpen = count($oPreOpen)!=0?count($oPreOpen):0;     //预告场数
		$oPpt = AspirinOnlinePpt::where('catid',$iCatId)->orderby('id','asc')->get();
		// print_r($oPpt->toArray());die;
		return View::make('front.aspirin.online.pptchoose')->with('oPpt',$oPpt)
			->with('oOnline',$oOnline)
			->with('iHasOpen',$iHasOpen)
			->with('iTotalCount',$iTotalCount)
			->with('iPreOpen',$iPreOpen)
			->with('iCatId',$iCatId);
	}
	
	//在线会议-讲者报名列表页
	public function getSpeakerList($iCatId=1,$iPptId=0)
	{
		$sNowTime = date('Y-m-d H:i:s',time());
		$oOnline = AspirinOnline::where('catid',$iCatId)->where('show_end_time','>',$sNowTime)->orderby('created_at','desc')->first();
		if(count($oOnline)){
			$oPreOpen = AspirinOnlineSpeaker::where('online_catid',$iCatId)->where('open_flag',0)->where('cancel_flag',0)->get();
			$oHasOpen = AspirinOnlineSpeaker::where('online_catid',$iCatId)->where('open_flag',1)->get();
			$iListenerCount = 0;
			foreach($oHasOpen as $k=>$v){
				$iListenerCount+= $v->listener_count;
			}
			$iHasOpen = count($oHasOpen)!=0?count($oHasOpen):0;     //成功举办的场数
			$iTotalCount = count($oHasOpen) + $iListenerCount;      //参与人数
			$iPreOpen = count($oPreOpen)!=0?count($oPreOpen):0;     //预告场数
		
			//列出上周已报名会议
			$aLastWeek = array();
			$oLastLog = AspirinOnlineSpeaker::where('online_id',$oOnline->id)
				->where('speaker_id',$this->iUserId)
				->where('cancel_flag',0)
				->orderby('created_at','desc')
				->first();
			$sNowTime = date('Y-m-d H:i',time());
			if($oLastLog){
				$oLastTime = AspirinOnlineTime::find($oLastLog->online_time_id);
				$sThisMon = date('Y-m-d',(time()-((date('w')==0?7:date('w'))-1)*24*3600)).' 00:00:00';
				$sNextMon = date('Y-m-d',(time()+(7-(date('w')==0?7:date('w')))*24*3600)).' 23:59:59';
				if($oLastTime->online_date < $sNextMon && $oLastTime->online_date > $sThisMon){
					$sStartTime = substr($oLastTime->online_date,0,10).' '.substr($oLastTime->time_period,0,5);
					$sEndTime = substr($oLastTime->online_date,0,10).' '.substr($oLastTime->time_period,6,5);
					//讲者可提前10分钟启动会议
					$sStart = date('Y-m-d H:i',(strtotime($sStartTime)-self::ADVANCETIME*60));
					if($sNowTime < $sStart){
						$aLastWeek[]= array(
							'online_id'     => $oOnline->id,
							'online_time_id'=> $oLastTime->id,
							'online_date'   => date('Y年m月d日',strtotime($oLastTime->online_date)),
							'time_period'   => $oLastTime->time_period,
							'speaker_count' => $oLastTime->speaker_count,
							'limit_count'   => Config::get('config.self_help_meeting_order_set'),
							'enter_flag'    => 2     //按钮显示为"启动会议"，不可点
						);
					}else if($sNowTime > $sStart && $sNowTime < $sEndTime){
						$aLastWeek[]= array(
							'online_id'     => $oOnline->id,
							'online_time_id'=> $oLastTime->id,
							'online_date'   => date('Y年m月d日',strtotime($oLastTime->online_date)),
							'time_period'   => $oLastTime->time_period,
							'speaker_count' => $oLastTime->speaker_count,
							'limit_count'   => Config::get('config.self_help_meeting_order_set'),
							'enter_flag'    => 3     //按钮显示为"启动会议"，可点
						);
					}
				}
			}
			//列出下一周的会议排期，时间依次递增，每周更新一次
			$sNextMon = date('Y-m-d',time()).' 00:00:00';
			$sNextMonReal = date('Y-m-d',(time()+(7-(date('w')==0?7:date('w')))*24*3600)).' 23:59:59';
			$sNextSun = date('Y-m-d',strtotime($sNextMonReal)+6*24*3600).' 23:59:59';
			$oOnlineTime = AspirinOnlineTime::where('online_id',$oOnline->id)
				->where('online_date','>',$sNextMon)
				->where('online_date','<=',$sNextSun)
				->where('speaker_count','<',Config::get('config.self_help_meeting_order_set'))
				->orderby('online_date','asc')
				->orderby('time_period','asc')
				->get();
			$aOnlineTime = array();
			//启动会议时用于会畅展示的课件链接
			$sPptHtml = Config::get('app.url').'/mobile/ppt/'.$iPptId;
			$sPptHtml = isset($sPptHtml)?$sPptHtml:'';
			foreach($oOnlineTime as $k=>$v){
				$oLog = AspirinOnlineSpeaker::where('online_id',$oOnline->id)
					->where('online_time_id',$v->id)
					->where('speaker_id',$this->iUserId)
					->where('cancel_flag',0)
					->first();
				if(!count($oLog)){
					$iFlag = 1;         //报名人数未满，可报名
				}else{
					$iFlag = 2;         //报名人数未满，已经报名  按钮显示为"启动会议"，不可点
				}
				$aOnlineTime[] = array(
					'online_id'     => $oOnline->id,
					'online_time_id'=> $v->id,
					'online_date'   => date('Y年m月d日',strtotime($v->online_date)),
					'time_period'   => $v->time_period,
					'speaker_count' => $v->speaker_count,
					'limit_count'   => Config::get('config.self_help_meeting_order_set'),
					'enter_flag'    => $iFlag
				);
			}
			//报名者满25名后该时间段会议将至于最下方
			$oOnlineOverTime = AspirinOnlineTime::where('online_id',$oOnline->id)
				->where('online_date','>',$sNextMon)
				->where('online_date','<=',$sNextSun)
				->where('speaker_count','=',Config::get('config.self_help_meeting_order_set'))
				->orderby('online_date','asc')
				->orderby('time_period','asc')
				->get();
			$aOnlineOverTime = array();
			foreach($oOnlineOverTime as $k=>$v){
				$aOnlineOverTime[] = array(
					'online_id'     => $oOnline->id,
					'online_time_id'=> $v->id,
					'online_date'   => date('Y年m月d日',strtotime($v->online_date)),
					'time_period'   => $v->time_period,
					'speaker_count' => $v->speaker_count,
					'limit_count'   => Config::get('config.self_help_meeting_order_set'),
					'enter_flag'    => 0          //报名人数已满，不可报名
				);
			}
			$aOnlineTime = array_merge($aLastWeek,$aOnlineTime,$aOnlineOverTime);
			//申报信息填写记录
			$oUserInfo = AspirinOnlineSpeaker::where('speaker_id',$this->iUserId)
				->where('cancel_flag',0)
				->orderby('created_at','desc')
				->first();
			return View::make('front.aspirin.online.speakerlist')->with('aOnlineTime',$aOnlineTime)
				->with('iHasOpen',$iHasOpen)
				->with('iTotalCount',$iTotalCount)
				->with('iPreOpen',$iPreOpen)
				->with('sPptHtml',$sPptHtml)
				->with('iPptId',$iPptId)
				->with('oUserInfo',$oUserInfo)
				->with('oOnline',$oOnline);
		}else{
			return View::make('front.aspirin.online.speakerlist');
		}
	}

	//在线会议-讲者报名-提交
	public function postSpeakerEnter()
	{
		$oUser = User::find($this->iUserId);
		//试点地区仅限召开102场
		/*$aPilotCountry = User::getPilotCountry();
		$aCityUser = User::where('user_city',$oUser->user_city)->lists('id');
		if(in_array($oUser->user_county,$aPilotCountry)){
			$aCityUser = User::where('user_county',$oUser->user_county)->lists('id');
		}
		if(!empty($aCityUser)){
			$iAllCount = AspirinOnlineSpeaker::where('cancel_flag',0)->whereIn('speaker_id',$aCityUser)->count();
			if($iAllCount && $iAllCount >= 200){
				echo 'shangxian';die;
			}
		}*/
		//只有通过医师认证并且是副高及以上权限的医生才可以作为讲者报名（后改为主治及以上）
		if($oUser->role_id == 3 && $oUser->card_auth_flag != 2){
			echo 'nocard';die;
		}
		$aPos = User::getPosition();
		if(($oUser->role_id == 3 && ($oUser->user_position == $aPos[2] || $oUser->user_position == $aPos[3] || $oUser->user_position == $aPos[4] )) || $oUser->role_id == 2){
			if($oUser->user_city != 320100){
				$oMonthLimit = AspirinOnlineSpeaker::where('speaker_id',$this->iUserId)->where('cancel_flag',0)->orderby('created_at','desc')->first();
				if($oMonthLimit){
					$iNowMonth = date('m',time());
					$iOnlineMonth = substr($oMonthLimit->created_at,5,2);
                    // 单个账号--南京每月限报2场，其他城市每月限报1场（取消）
					/*if ($iOnlineMonth == $iNowMonth){
						echo 'one';die;     //每月只可报名一场会议
					}*/
				}
			}else{
				$oMonthLimit = AspirinOnlineSpeaker::where('speaker_id',$this->iUserId)->where('cancel_flag',0)->orderby('created_at','desc')->get();
				$iNum = 0;
				$iNowMonth = date('m',time());
				foreach($oMonthLimit as $m=>$n){
					$iOnlineMonth = substr($n->created_at,5,2);
					if ($iOnlineMonth == $iNowMonth){
						$iNum++;
					}
				}
				// 单个账号--南京每月限报2场，其他城市每月限报1场（取消）
				/*if($iNum>=12){
					echo 'twice';die;     //南京用户每月限报名两场会议
				}*/
			}
			/*$oTwiceLimit = AspirinOnlineSpeaker::where('speaker_id',$this->iUserId)->where('cancel_flag',0)->orderby('created_at','desc')->get();
			if(count($oTwiceLimit)==2){
				echo 'twice';die;        //试运营期内限每位讲者只能参与两次
			}
			//判断用户是否已经申报过该季度会议（心内和神内只能报名一个）
			$iOnlineId = intval(Input::get('iOnlineId'));
			$sNowTime = date('Y-m-d H:i:s',time());
			$aOnline = AspirinOnline::where('show_start_time','<',$sNowTime)
														->where('show_end_time','>',$sNowTime)
														->orWhere('id',$iOnlineId)
														->orderby('created_at','desc')
														->lists('id');
			if(!empty($aOnline)){
				$oOnlineLog = AspirinOnlineSpeaker::whereIn('online_id',$aOnline)->where('speaker_id',$this->iUserId)->where('cancel_flag',0)->first();
				if(count($oOnlineLog)){
					echo 'again';die;
				}else{
					echo 'success';die;
				}
			}else{
				echo 'error';die;
			}*/
			$iOnlineId = intval(Input::get('iOnlineId'));
			$oOnline = AspirinOnline::find($iOnlineId);
			if($oOnline){
				echo 'success';die;
			}else{
				echo 'error';die;
			}
		}else{
			echo 'noauth';die;
		}
	}

	//在线会议-讲者报名-确认
	public function postSpeakerSubmit()
	{
		$oUser = User::find($this->iUserId);
		if($oUser->role_id == 3 && $oUser->card_auth_flag != 2){
			echo 'nocard';die;
		}
		$aPos = User::getPosition();
		if($oUser->role_id == 3 && $oUser->user_position != $aPos[2] && $oUser->user_position != $aPos[3] && $oUser->user_position != $aPos[4]){
			echo 'nocard';die;
		}
		$iPptId = intval(Input::get('iPptId'));
		$iOnlineId = intval(Input::get('iOnlineId'));
		$iOnlineTimeId = intval(Input::get('timeid'));
		$sName = trim(Input::get('declare_name'));
		$sIdNum = trim(Input::get('declare_id_num'));
		$sBank = trim(Input::get('declare_bank'));
		$sBankNum = trim(Input::get('declare_bank_num'));
		$oOnlineTime = AspirinOnlineTime::find($iOnlineTimeId);
		$oOnline = AspirinOnline::find($iOnlineId);
		//调用会畅接口相关准备s
		$sOnlineDate = $oOnlineTime->online_date;
		$sOnlineStartTime = substr($sOnlineDate,0,11).substr($oOnlineTime->time_period,0,5).':00';
		$sOnlineEndTime = substr($sOnlineDate,0,11).substr($oOnlineTime->time_period,6,5).':00';
		//根据会议的起始和结束时间返回时间段数组，和自助会议平台一样，以半小时计，例如：06:00为61,06:30为62（不足半点的按半点算，06:20也计为61）
		$aTimePeriod = AspirinOnline::getTimePeriod($sOnlineStartTime,$sOnlineEndTime);
		//判断时间段是否被占用,筛选出数组中同一时间段的个数
		$aTimeRecordLog = MeetingTime::where('meeting_date',$sOnlineDate)->lists('meeting_time_period','id');
		$aCountRecord = array_count_values($aTimeRecordLog);
		$iMaxNum = Config::get('config.self_help_meeting_order_set');
		$aTimeCode = array();
		foreach($aCountRecord as $k=>$v){
			if($v >= $iMaxNum){
				$aTimeCode[] = $k;
			}
		}
		if(!empty($aTimeCode)){
			foreach($aTimePeriod as $v){
				if(in_array($v,$aTimeCode)){
					echo 'notime';die;
				}
			}
		}
		//获取会议编码
		$aRes = MeetingTime::where('meeting_date',$sOnlineDate)->whereIn('meeting_time_period',$aTimePeriod)->lists('meeting_time_period','meeting_code');
		if(!empty($aRes)){
			$aRes = array_keys($aRes);
			$oCode = MeetingCode::whereNotIn('code',$aRes)->orderby('id','asc')->first();
		}else{
			// $oCode = MeetingCode::orderby('id','desc')->first();
			$oCode = MeetingCode::orderBy(DB::raw('RAND()'))->first();
		}
		if($oCode){
			//调用会畅接口**********************start**********************/
			$sCode = $oCode->code;
			$sApiKey = Config::get('config.hc_api_key');
			$sApiUrl = Config::get('config.hc_api_url_v2');
			$sTimeStamp = time();
			$sToken = md5($oCode->name.'|'.$sApiKey.'|'.$sTimeStamp);
			$iDuration = (strtotime($sOnlineEndTime) - strtotime($sOnlineStartTime))/60;
			$data = array(
				'loginName'  => $oCode->name,
				'timeStamp'  => $sTimeStamp,
				'token'  	 => $sToken,
				'confName'   => $oOnline->online_title,
				'hostKey'    => $oCode->key,
				'startTime'  => $sOnlineStartTime,
				'duration'   => $iDuration,
				'optionJbh'  => 0,
				'optionStartType'  => 1
			);
			$aReturnInfo = requestApiByCurl($sApiUrl,$data,'POST');
			//调用会畅接口**********************end*********************/
			//记录接口返回信息
			if($aReturnInfo['status'] == 100){
				//记录讲者申报信息
				$oSpeakerInfo = AspirinOnlineSpeaker::where('online_id',$iOnlineId)
					->where('online_time_id',$iOnlineTimeId)
					->where('speaker_id',$this->iUserId)
					->where('cancel_flag',1)
					->first();
				if(count($oSpeakerInfo)){
					$oSpeakerInfo->declare_name        = $sName;
					$oSpeakerInfo->declare_id_number   = $sIdNum;
					$oSpeakerInfo->declare_bank        = $sBank;
					$oSpeakerInfo->declare_bank_number = $sBankNum;
					$oSpeakerInfo->open_flag      = 0;
					$oSpeakerInfo->cancel_flag    = 0;
					$oSpeakerInfo->listener_count = 0;
					$oSpeakerInfo->save();
				}else{
					$aSpeakerInfo = array(
						'ppt_id'            => $iPptId,
						'online_catid'      => $oOnline->catid,
						'online_id'         => $iOnlineId,
						'online_time_id'    => $iOnlineTimeId,
						'speaker_id'	    => $this->iUserId,
						'declare_name'	    => $sName,
						'declare_id_number'	=> $sIdNum,
						'declare_bank'		=> $sBank,
						'declare_bank_number'=> $sBankNum,
						'created_at'        => date('Y-m-d H:i:s',time())
					);
					$oSpeakerInfo = new AspirinOnlineSpeaker($aSpeakerInfo);
					$oSpeakerInfo->save();
				}
				$iOnlineSpeakerId = $oSpeakerInfo->id;
				//报名总人数加1
				$oOnlineTime->increment('speaker_count');
				$aLog = array(
					'online_id'  	=>  $iOnlineSpeakerId,
					'audio'  		=>  $aReturnInfo['data']['audio'],
					'hoststarturl'  =>  $aReturnInfo['data']['hostStartUrl'],
					'starturl'  	=>  $aReturnInfo['data']['startUrl'],
					'confstatus'  	=>  $aReturnInfo['data']['confStatus'],
					'optionjbh'  	=>  $aReturnInfo['data']['optionJbh'],
					'meetingno'  	=>  $sCode, //$aReturnInfo['data']['meetingNo'],
					'api_id'  		=>  $aReturnInfo['data']['id'],
					'starttime' 	=>  $aReturnInfo['data']['startTime'],
					'duration'  	=>  $aReturnInfo['data']['duration'],
					'meetingname'   =>  $aReturnInfo['data']['meetingName'],
					'token'  		=>  $aReturnInfo['data']['token'],
					'pwd'  			=>  $aReturnInfo['data']['h323pwd'],
					'userid'  		=>  $aReturnInfo['data']['userId'],
					'joinurl'  		=>  $aReturnInfo['data']['joinUrl'],
					'hostkey'  		=>  $aReturnInfo['data']['hostKey'],
					'maxcount'  	=>  $aReturnInfo['data']['maxCount'],
					'created_at'  	=>  date('Y-m-d H:i:s',time())
				);
				$oMeetingApiLog = new MeetingApiLog($aLog);
				$oMeetingApiLog->save();
				//记录会议时间段
				foreach($aTimePeriod as $val){
					$aPeriod = array(
						'online_id'           => $iOnlineSpeakerId,
						'meeting_date'        => $sOnlineDate,
						'meeting_time_period' => $val,
						'meeting_code'        => $sCode,
						'created_at'          => date('Y-m-d H:i:s',time())
					);
					$oMeetingTime = new MeetingTime($aPeriod);
					$oMeetingTime->save();
				}
				//讲者报名成功推送给该城市所有医生
				$oUser = User::find($this->iUserId);
				$sUserCompany = $oUser->user_company;
				if(is_numeric($sUserCompany)&&$sUserCompany!=0){
					$sUserCompany = Hospital::where('id',$sUserCompany)->pluck('name');
				}
				if(!$sUserCompany){
					$sUserCompany = $oUser->user_company_name;
				}
				$oDoctorUser = User::where('role_id',3)->where('card_auth_flag',2)->where('user_city',$oUser->user_city)->where('id','!=',$this->iUserId)->get();
				foreach($oDoctorUser as $k=>$v){
					//发送消息通知
					$sMeetingtime = date('Y年m月d日',strtotime($oOnlineTime->online_date)).$oOnlineTime->time_period;
					$sPushContent = PushService::ONLINE_LISTENER_ALL;
					$sPushContent = str_replace('{{userhospital}}', $sUserCompany, $sPushContent);
					$sPushContent = str_replace('{{username}}', $oUser->user_name, $sPushContent);
					$sPushContent = str_replace('{{meetingtime}}', $sMeetingtime, $sPushContent);
					$aNotice = array(
						'user_id'		=> $v['id'],
						'notice_content'=> $sPushContent,
						'notice_type'	=> 15,
						'detail_id'		=> $oOnline->catid
					);
					$oNotice = new AspirinUserNotice($aNotice);
					$oNotice->save();
				}
				echo 'success';die;
			}else{
				echo 'meetingerror';die;
			}
		}else{
			echo 'codeerror';die;
		}
	}

	//在线会议-听者参会-列表页(catid: 1:心内；2：神内；)
	public function getListenerList($iCatId=1)
	{
		$s_online_date = Input::get('s_online_date');
		$s_keys = Input::get('s_keys');
		$aOnlineId = AspirinOnline::where('catid',$iCatId)->lists('id');
		if(!empty($aOnlineId)){
			//显示本周及下周同城市讲者报名的会议
			$sThisMon = date('Y-m-d',(time()-((date('w')==0?7:date('w'))-1)*24*3600)).' 00:00:00';
			$sNextSun = date('Y-m-d',strtotime($sThisMon)+13*24*3600).' 23:59:59';
			$oUser = User::find($this->iUserId);
			$iCity = $oUser->user_city;
			//获取所有同城市讲者数组
			$aPos = User::getPosition();
			$aPos = array($aPos[2],$aPos[3],$aPos[4]);
			$aUser = User::where('id','!=',$this->iUserId)->where(function($query) use($aPos){
				$query->where('role_id',3)->where('card_auth_flag',2)->whereIn('user_position',$aPos)->orWhere('role_id',2);
			})->lists('id','id');
			/*$aUser = User::where('role_id',3)->where('user_city',$iCity)->where('card_auth_flag',2)
				->whereIn('user_position',$aPos)
				->where('id','!=',$this->iUserId)
				->lists('id','user_name');*/
			// print_r($aUser);die;
			$aOnlineInfo = array();
			$oInfo = AspirinOnlineSpeaker::leftJoin('aspirin_online_time','aspirin_online_time.id','=','aspirin_online_speaker.online_time_id')
				->leftJoin('aspirin_online_ppt','aspirin_online_ppt.id','=','aspirin_online_speaker.ppt_id')
				->select('aspirin_online_speaker.*','aspirin_online_time.online_date','aspirin_online_time.time_period','aspirin_online_time.speaker_count','aspirin_online_ppt.ppt_title');
			if($s_online_date){
				$sThisMon = "{$s_online_date} 00:00:00";
				$sNextSun = "{$s_online_date} 23:59:59";
				$oInfo = $oInfo->where('online_date','>=',$sThisMon)
					->where('online_date','<=',$sNextSun);
			}else{
				$oInfo = $oInfo->where('online_date','>=',$sThisMon)
					->where('online_date','<=',$sNextSun);
			}
			if($s_keys){
				$oInfo = $oInfo->where('ppt_title','LIKE','%'.$s_keys.'%');
				// $oInfo = $oInfo->where('ppt_title','LIKE', "'%{$s_keys}%'");
			}
			$oInfo = $oInfo->where('cancel_flag',0)
				->whereIn('aspirin_online_speaker.online_id',$aOnlineId)
				->orderby('online_date','asc')
				->orderby('created_at','asc')
				->get();
			// $log = DB::getQueryLog();
			// print_r($log);die;
			foreach($oInfo as $key=>$val){
				$iBtnFlag = 0;
				$sNowTime = date('Y-m-d H:i',time());
				//判断是否已报名或已同意某讲者会议
				$oListenerLog = AspirinOnlineListener::where('online_time_id',$val->online_time_id)
					->where('speaker_id',$val->speaker_id)
					->where('listener_id',$this->iUserId)
					->where(function($query){
						$query->where('join_type',0)
						->orWhere('is_agree',1);
					})->first();
				if(count($oListenerLog)){
					$sStartTime = substr($val->online_date,0,10).' '.substr($val->time_period,0,5);
					if($sNowTime < $sStartTime){
						$iBtnFlag = 3;		//已报名，未开始
					}else{
						$iBtnFlag = 2;		//已报名、可参加(包含已报名、已同意,不可再次报名的情况，按钮显示为可点的“参加会议”)
					}
				}else{
					if($val->listener_count == Config::get('config.self_help_meeting_attent_set')){
						$iBtnFlag = 1;		//人数已满
					}
				}
				$aRes = array();
				$sEndTime = substr($val->online_date,0,10).' '.substr($val->time_period,6,5);
				// print_r($val->speaker_id);
				// print_r($aUser);
				// print_r($val->speaker_id);
				// print_r($aUser);
				// var_dump(in_array($val->speaker_id,$aUser));
				// var_dump($sNowTime < $sEndTime);
				if(in_array($val->speaker_id,$aUser) && $sNowTime < $sEndTime){
					$oDoctorInfo = User::find($val->speaker_id);
					if(!$oDoctorInfo->user_company){
						$sHospital = $oDoctorInfo->user_company_name;
					}else{
						$sHospital = Hospital::where('id',$oDoctorInfo->user_company)->pluck('name');
					}
					$aRes[] = array(
						'speaker_id'       => $val->speaker_id,
						'doc_name'         => $oDoctorInfo->user_name,
						'doc_department'   => $oDoctorInfo->user_department,
						'doc_hospital'     => $sHospital,
						'listener_count'   => $val->listener_count,
						'listener_limit'   => Config::get('config.self_help_meeting_attent_set'),
						'btn_flag'         => $iBtnFlag,
					);
				}
				if($aRes){
					$aOnlineInfo[] = array(
						'ppt_id'        => $val->ppt_id,
						'online_title'  => $val->ppt_title,
						'online_time_id'=> $val->online_time_id,
						'online_date'   => date('Y年m月d日',strtotime($val->online_date)),
						'time_period'   => $val->time_period,
						'speaker_info'  => $aRes
					);
				}
			}
			// print_r($aOnlineInfo);die;
			//同一主题同一时间段的讲者合并显示
			$resultArr = array();
			if($aOnlineInfo){
				$aOnlineInfoCount = count($aOnlineInfo);
				for($i=0;$i<$aOnlineInfoCount;$i++){
					$flag = 0;
					for($j=$i;$j<$aOnlineInfoCount;$j++){
						if($i!=$j&& !empty($aOnlineInfo[$i]) && !empty($aOnlineInfo[$j])&& $aOnlineInfo[$i]['ppt_id'] == $aOnlineInfo[$j]['ppt_id'] && $aOnlineInfo[$i]['online_time_id'] == $aOnlineInfo[$j]['online_time_id']){
							$aRes1 = array();
							if(count($aOnlineInfo[$i]['speaker_info'])==1){
								$aRes1[] = $aOnlineInfo[$i]['speaker_info'][0];
								$aRes1[] = $aOnlineInfo[$j]['speaker_info'][0];
							}else{
								$aRes1 = $aOnlineInfo[$i]['speaker_info'];
								$aRes1[] = $aOnlineInfo[$j]['speaker_info'][0];
							}
							$aOnlineInfo[$i]['speaker_info'] = $aRes1;
							//unset($aOnlineInfo[$j]);
							$aOnlineInfo[$j] = array();
	
							$flag = 1;
						}
					}
					if(!empty( $aOnlineInfo[$i])){
						$resultArr[] = $aOnlineInfo[$i];
					}
	
				}
			}
			return View::make('front.aspirin.online.listenerlist')
				->with('aOnlineInfo',$resultArr)
				->with('s_keys',$s_keys)
				->with('s_online_date',$s_online_date)
				->with('iCatId',$iCatId);
		}else{
			$resultArr = array();
			return View::make('front.aspirin.online.listenerlist')
				->with('aOnlineInfo',$resultArr)
				->with('s_keys',$s_keys)
				->with('s_online_date',$s_online_date)
				->with('iCatId',$iCatId);
		}
	}

	//在线会议-听者参会-提交
	public function postListenerEnter()
	{
		$oUser = User::find($this->iUserId);
		/*if($oUser->role_id == 3 && $oUser->card_auth_flag != 2){
			echo 'nocard';die;
		}*/
		$iOnlineTimeId = intval(Input::get('timeid'));
		$oOnlineTime = AspirinOnlineTime::find($iOnlineTimeId);
		$sOnlineTime = substr($oOnlineTime->online_date,0,11).substr($oOnlineTime->time_period,0,5).':00';
		$sOnlineEndTime = substr($oOnlineTime->online_date,0,11).substr($oOnlineTime->time_period,6,5);
		$sThisSun = date('Y-m-d',(time()+(7-(date('w')==0?7:date('w')))*24*3600)).' 23:59:59';
		$sNowTime = date('Y-m-d H:i',time());
		//听者只限报当周的会议，下周会议可看不可报
		if($sOnlineTime > $sThisSun){
			echo 'nextweek';die;
		}else{
			if($sNowTime > $sOnlineTime && $sNowTime <= $sOnlineEndTime){
				echo 'hasstart';die;
			}
			if($sNowTime > $sOnlineEndTime){
				echo 'hasend';die;
			}
			$oLog = AspirinOnlineListener::where('online_time_id',$iOnlineTimeId)->where('listener_id',$this->iUserId)->where(function($query){
				$query->where('join_type',0)->orWhere('is_agree',1);
			})->first();
			if(count($oLog)){
				echo 'onlyone';die;
			}
			echo 'success';die;
		}
	}

	//在线会议-听者参会-确认
	public function postListenerSubmit()
	{
		$oUser = User::find($this->iUserId);
		/*if($oUser->role_id == 3 && $oUser->card_auth_flag != 2){
			echo 'nocard';die;
		}*/
		$iOnlineTimeId = intval(Input::get('timeid'));
		$iSpeakerId = intval(Input::get('speakerid'));
		//判断是否已报名该讲者会议
		$oListenerLog = AspirinOnlineListener::where('online_time_id',$iOnlineTimeId)
			->where('speaker_id',$iSpeakerId)
			->where('listener_id',$this->iUserId)
			->where('join_type',0)
			->first();
		if(count($oListenerLog)){
			echo 'again';die;
		}else{
			//判断是否有讲者邀请，如果有，报名时直接更改为同意状态
			$oInviteLog = AspirinOnlineListener::where('online_time_id',$iOnlineTimeId)
				->where('speaker_id',$iSpeakerId)
				->where('listener_id',$this->iUserId)
				->where('join_type',1)
				->first();
			if(count($oInviteLog)){
				$oInviteLog->is_agree = 1;
				$oInviteLog->save();
			}else{
				$aListenerLog = array(
					'online_time_id' => $iOnlineTimeId,
					'speaker_id'     => $iSpeakerId,
					'listener_id'    => $this->iUserId,
					'join_type'      => 0,
					'created_at'     => date('Y-m-d H:i:s',time())
				);
				$oListener = new AspirinOnlineListener($aListenerLog);
				$oListener->save();
			}
			//听者总数加1
			$oSpeakerInfo = AspirinOnlineSpeaker::where('online_time_id',$iOnlineTimeId)->where('speaker_id',$iSpeakerId)->where('cancel_flag',0)->first();
			$oSpeakerInfo->increment('listener_count');
			echo 'success';die;
		}
	}

	//个人中心-我的会议-主讲会议
	public function getMyEnterList()
	{
		//只取主讲记录里最新的一条显示，过期的会议不再展示
		$oInfo = AspirinOnlineSpeaker::leftJoin('aspirin_online_time','aspirin_online_time.id','=','aspirin_online_speaker.online_time_id')
			->select('aspirin_online_speaker.speaker_id','aspirin_online_speaker.cancel_flag','aspirin_online_speaker.ppt_id','aspirin_online_time.*')
			->where('speaker_id',$this->iUserId)
			->where('cancel_flag',0)
			->orderby('aspirin_online_time.id','desc')
			->first();
		// $log = DB::getQueryLog();
		// print_r($log);die;
		if($oInfo){
			$oOnlineInfo = AspirinOnline::find($oInfo->online_id);
			$oPpt = AspirinOnlinePpt::find($oInfo->ppt_id);
			//会议时间
			$sNowTime = date('Y-m-d H:i',time());
			$sStartTime = substr($oInfo->online_date,0,10).' '.substr($oInfo->time_period,0,5);
			$sEndTime = substr($oInfo->online_date,0,10).' '.substr($oInfo->time_period,6,5);
			//讲者可提前10分钟启动会议
			$sStart = date('Y-m-d H:i',(strtotime($sStartTime)-self::ADVANCETIME*60));
			if($sNowTime < $sStart){
				$iFlag = 0;        //取消按钮可点;启动会议不可点;邀请医生可点
			}elseif($sNowTime > $sEndTime){
				$iFlag = 2;        //取消按钮不可点,变成“会议结束”;启动会议不可点;邀请医生不可点
			}else{
				$iFlag = 1;        //取消按钮不可点;启动会议可点;邀请医生不可点
			}
			//启动会议时用于会畅展示的课件链接
			$sPptHtml = Config::get('app.url').'/mobile/ppt/'.$oInfo->ppt_id;
			$sPptHtml = isset($sPptHtml)?$sPptHtml:'';
			//报名或邀请的医生
			$aHosp = Hospital::getChildren();
			$oInvite = AspirinOnlineListener::where('online_time_id',$oInfo->id)
				->where('speaker_id',$this->iUserId)
				// ->where('join_type',1)
				->orderBy('join_type','desc')
				->orderBy('created_at','desc')
				->get();
			$aInviteList = array();
			foreach($oInvite as $k=>$v){
				$aInvite = array();
				$oUser = User::select('user_name','user_company','user_company_name')->where('id',$v->listener_id)->first();
				if($oUser){
					$aInvite['doc_name'] = $oUser->user_name;
					$sUserCompany = $oUser->user_company;
					if(is_numeric($sUserCompany)&&$sUserCompany!=0){
						$sUserCompany = Hospital::where('id',$sUserCompany)->pluck('name');
					}
					if(!$sUserCompany){
						$sUserCompany = $oUser->user_company_name;
					}
					$aInvite['doc_hospital'] = $sUserCompany;
				}
				$aInvite['invite_id'] = $v->id;
				$aInvite['join_type'] = $v->join_type;
				$aInviteList[] = $aInvite;
			}		
			return View::make('front.aspirin.online.myenterlist')->with('oInfo',$oInfo)
				->with('sPptHtml',$sPptHtml)
				->with('iFlag',$iFlag)
				->with('oPpt',$oPpt)
				->with('aInviteList',$aInviteList)
				->with('aHosp',$aHosp)
				->with('oOnlineInfo',$oOnlineInfo);
		}else{
			return View::make('front.aspirin.online.myenterlist');
		}
	}

	//个人中心-我的会议-讲者取消会议
	public function postSpeakerCancel()
	{
		$iOnlineTimeId = intval(Input::get('timeid'));
		$oSpeakerInfo = AspirinOnlineSpeaker::where('online_time_id',$iOnlineTimeId)->where('speaker_id',$this->iUserId)->where('cancel_flag',0)->first();
		if($oSpeakerInfo){
			$oSpeakerInfo->cancel_flag=1;
			$oSpeakerInfo->save();
		}
		if(AspirinOnlineSpeaker::getCancel($oSpeakerInfo->id,$iOnlineTimeId,$this->iUserId)){
			echo 'success';die;
		}else{
			echo 'error';die;
		}
	}

	//个人中心-我的会议-讲者启动会议
	public function postSpeakerOpen()
	{
		$iOnlineTimeId = intval(Input::get('timeid'));
		$oSpeakerInfo = AspirinOnlineSpeaker::where('online_time_id',$iOnlineTimeId)
			->where('speaker_id',$this->iUserId)
			->where('cancel_flag',0)
			->first();
		//会议时间
		$oOnlineTime = AspirinOnlineTime::find($iOnlineTimeId);
		$sNowTime = date('Y-m-d H:i',time());
		$sStartTime = substr($oOnlineTime->online_date,0,10).' '.substr($oOnlineTime->time_period,0,5);
		$sEndTime = substr($oOnlineTime->online_date,0,10).' '.substr($oOnlineTime->time_period,6,5);
		//讲者可提前10分钟启动会议
		$sStart = date('Y-m-d H:i',(strtotime($sStartTime)-self::ADVANCETIME*60));
		if($oSpeakerInfo->listener_count < self::LIMIT_CANCEL_COUNT && $sNowTime > $sStart && $sNowTime < $sStartTime){
			return json_encode(array('success'=> false,'notice'=>'您的会议不足5人参加，暂不能启动会议','url'=>''));
			die;
		}
		if($oSpeakerInfo->listener_count < self::LIMIT_CANCEL_COUNT && $sNowTime >= $sStartTime){
			//会议开始，不足5人，取消
			$oSpeakerInfo->cancel_flag = 1;
			$oSpeakerInfo->save();
			if(AspirinOnlineSpeaker::getCancel($oSpeakerInfo->id,$iOnlineTimeId,$this->iUserId)){
				return json_encode(array('success'=> false,'notice'=>'您的会议因不足5人已取消会议。','url'=>''));
				die;
			}
		}
		$oMeetingLog = MeetingApiLog::where('online_id',$oSpeakerInfo->id)->orderBy('id','desc')->first();
		if($oMeetingLog){
			//开启标志置为1
			$oSpeakerInfo->open_flag=1;
			$oSpeakerInfo->save();
			$sUrl = $oMeetingLog->hoststarturl;
			return json_encode(array('success'=> true,'notice'=>'','url'=>$sUrl));
			die;
		}else{
			return json_encode(array('success'=> false,'notice'=>'启动失败','url'=>''));
			die;
		}
	}

	//个人中心-我的会议-讲者邀请医生-根据城市获取医院信息（区县不是必填项）
	public function postHospital()
	{
		$iCityId = intval(Input::get('cityid'));
		$aHospital = array();
		$aCountyId = Hospital::where('parent_id',$iCityId)->lists('id');
		foreach($aCountyId as $value){
			$aHospitalAll[] = Hospital::where('parent_id',$value)->lists('name','id');
		}
		foreach($aHospitalAll as $val){
			foreach($val as $k=>$v){
				$aHospital[$k] = $v;
			}
		}
		if($aHospital){
			return json_encode($aHospital);
		}else{
			return json_encode('noinfo');
		}
	}

	//个人中心-我的会议-讲者选择可邀请医生页
	public function postInviteDoctor()
	{
		$iOnlineTimeId = intval(Input::get('timeid'));
		$iHospitalId = intval(Input::get('iHospitalId'));
		$aDoctorInfo = array();
		$sUserCompanyName = Hospital::where('id',$iHospitalId)->pluck('name');
		//只可邀请试点城市医生s
		$aPilotCity = User::getPilotCity();
		$aPilotCountry = User::getPilotCountry();
		$iCountryId = Hospital::where('id',$iHospitalId)->pluck('parent_id');
		if(!in_array($iCountryId,$aPilotCountry)){
			$iCityId = Hospital::where('id',$iCountryId)->pluck('parent_id');
			if(!in_array($iCityId,$aPilotCity)){
				return json_encode(array('success'=> true,'aDoctorInfo'=>$aDoctorInfo));
				die;
			}
		}
		//只可邀请试点城市医生e
		$aDoctor = User::where('role_id',3)->where('id','!=',$this->iUserId)
			->where(function($query) use($iHospitalId,$sUserCompanyName){
				$query->where('user_company',$iHospitalId)->orWhere('user_company_name',$sUserCompanyName);
			})->lists('user_name','id');
		$aLog = AspirinOnlineListener::where('online_time_id',$iOnlineTimeId)
			->where('speaker_id',$this->iUserId)
			->lists('listener_id','id');
		foreach($aDoctor as $k=>$v){
			if(!in_array($k,$aLog)){
				$aDoctorInfo[] = array(
					'id'       =>   $k,
					'user_name' => $v
				);
			}
		}
		return json_encode(array('success'=> true,'aDoctorInfo'=>$aDoctorInfo));
		die;
	}

	//个人中心-我的会议-讲者邀请医生提交
	public function postInviteSubmit()
	{
		$iOnlineTimeId = intval(Input::get('timeid'));
		$sDoctorInfo = Input::get('doctorids');
		$aDoctorInfo = explode('|',$sDoctorInfo);
		if(count($aDoctorInfo) > Config::get('config.self_help_meeting_attent_set')){
			return json_encode(array('success'=> false,'notice'=>'您最多可邀请10名医生参会'));
			die;
		}
		foreach($aDoctorInfo as $k=>$v){
			$aInvite = array(
				'online_time_id'=> $iOnlineTimeId,
				'speaker_id'    => $this->iUserId,
				'listener_id'   => $v,
				'join_type'     => 1,
				'created_at'    => date('Y-m-d H:i:s',time())
			);
			$oInvite = new AspirinOnlineListener($aInvite);
			$oInvite->save();
			//发送消息通知
			$sName  = User::where('id',$this->iUserId)->pluck('user_name');
			$oOnlineTime = AspirinOnlineTime::find($iOnlineTimeId);
			$oOnline = AspirinOnline::find($oOnlineTime->online_id);
			$sOnlineTitle = $oOnline->online_title;
			$sPushContent = PushService::ONLINE_SPEAKER_INVITED;
			$sPushContent = str_replace('{{username}}', $sName, $sPushContent);
			$sPushContent = str_replace('{{title}}', $sOnlineTitle, $sPushContent);
			// PushService::postClient('国卫健康云',$sPushContent,12,$v['id']);
			$aNotice = array(
				'user_id'		=> @$v['id'],
				'notice_content'=> $sPushContent,
				'notice_type'	=> 12
			);
			$oNotice = new AspirinUserNotice($aNotice);
			$oNotice->save();
		}
		return json_encode(array('success'=> true,'notice'=>''));
		die;
	}

	//个人中心-我的会议-讲者删除邀请医生
	public function postInviteDelete()
	{
		$iInviteId = intval(Input::get('inviteid'));
		$oInvite = AspirinOnlineListener::find($iInviteId);
		if($oInvite){
			$oInvite->delete();
			echo 'success';die;
		}else{
			echo 'error';die;
		}
	}

	//个人中心-我的会议-参加会议列表
	public function getMyAttendList()
	{
		//参加的会议
		$aInfo1 =array();
		$oListenerInfo = AspirinOnlineListener::where('listener_id',$this->iUserId)
			->where('join_type',0)
			->orderby('created_at','asc')
			->get();
		foreach($oListenerInfo as $k=>$v){
			$aInfo1[] = AspirinOnlineListener::getJoinInfo($v->online_time_id, $v->speaker_id);
		}
		//已同意的会议
		$aInfo2 =array();
		$oAgreeInfo = AspirinOnlineListener::where('listener_id',$this->iUserId)
			->where('join_type',1)
			->where('is_agree',1)
			->orderby('created_at','asc')
			->get();
		foreach($oAgreeInfo as $k=>$v){
			$aInfo2[] = AspirinOnlineListener::getJoinInfo($v->online_time_id, $v->speaker_id);
		}
		//所有可参加的会议
		$aInfo1 = array_filter($aInfo1);
		$aInfo2 = array_filter($aInfo2);
		$aAttendInfo = array_merge($aInfo1,$aInfo2);
		//被邀请的会议
		$aInfo3 =array();
		$oInviteInfo = AspirinOnlineListener::where('listener_id',$this->iUserId)
			->where('join_type',1)
			->where('is_agree',0)
			->orderby('created_at','asc')
			->get();
		foreach($oInviteInfo as $k=>$v){
			$aInfo3[] = AspirinOnlineListener::getInviteInfo($v->online_time_id, $v->speaker_id,0);
		}
		//忽略的会议
		$aInfo4 =array();
		$oIgnoreInfo = AspirinOnlineListener::where('listener_id',$this->iUserId)
			->where('join_type',1)
			->where('is_agree',2)
			->orderby('created_at','asc')
			->get();
		foreach($oIgnoreInfo as $k=>$v){
			$aInfo4[] = AspirinOnlineListener::getInviteInfo($v->online_time_id, $v->speaker_id,1);
		}
		//所有被邀请的会议
		$aInfo3 = array_filter($aInfo3);
		$aInfo4 = array_filter($aInfo4);
		$aInviteInfo = array_merge($aInfo3,$aInfo4);
		return View::make('front.aspirin.online.myattendlist')
			->with('aAttendInfo',$aAttendInfo)
			->with('aInviteInfo',$aInviteInfo);
	}

	//个人中心-我的会议-听者参加会议
	public function postListenerAttend()
	{
		$iOnlineTimeId = intval(Input::get('timeid'));
		$iSpeakerId = intval(Input::get('speakerid'));
		$oOnlineTime = AspirinOnlineTime::find($iOnlineTimeId);
		//会议时间
		$sNowTime = date('Y-m-d H:i',time());
		$sStartTime = substr($oOnlineTime->online_date,0,10).' '.substr($oOnlineTime->time_period,0,5);
		$sEndTime = substr($oOnlineTime->online_date,0,10).' '.substr($oOnlineTime->time_period,6,5);
		if($sNowTime < $sStartTime){
			return json_encode(array('success'=> false,'notice'=>'该会议还未开始','url'=>''));
			die;
		}elseif($sNowTime > $sEndTime){
			return json_encode(array('success'=> false,'notice'=>'该会议已结束','url'=>''));
			die;
		}else{
			$oSpeakerInfo = AspirinOnlineSpeaker::where('online_time_id',$iOnlineTimeId)
				->where('speaker_id',$iSpeakerId)
				->orderby('created_at','desc')
				->first();
			if($oSpeakerInfo->cancel_flag == 1){
				return json_encode(array('success'=> false,'notice'=>'该会议已经取消','url'=>''));
				die;
			}
			if($oSpeakerInfo->open_flag != 1){
				return json_encode(array('success'=> false,'notice'=>'该会议还未启动','url'=>''));
				die;
			}
			$oMeetingLog = MeetingApiLog::where('online_id',$oSpeakerInfo->id)->orderBy('id','desc')->first();
			// $oMeetingLog = MeetingApiLog::where('online_id',$oSpeakerInfo->id)->first();
			if($oMeetingLog){
				//更改为已参加
				$oAttendLog = AspirinOnlineListener::where('online_time_id',$iOnlineTimeId)
					->where('speaker_id',$iSpeakerId)
					->where('listener_id',$this->iUserId)
					->first();
				$oAttendLog->has_attend = 1;
				$oAttendLog->save();
				$sUrl = $oMeetingLog->joinurl;
				return json_encode(array('success'=> true,'notice'=>'','url'=>$sUrl));
				die;
			}else{
				return json_encode(array('success'=> false,'notice'=>'进入会议失败','url'=>''));
				die;
			}
		}
	}

	//个人中心-我的会议-同意会议
	public function postAgreeAttend()
	{
		$iOnlineTimeId = intval(Input::get('timeid'));
		$iSpeakerId = intval(Input::get('speakerid'));
		$oInfo = AspirinOnlineListener::where('online_time_id',$iOnlineTimeId)
			->where('speaker_id',$iSpeakerId)
			->where('listener_id',$this->iUserId)
			->where('join_type',1)
			->first();
		if(count($oInfo)){
			$oInfo->is_agree = 1;
			$oInfo->save();
			$oCountLog = AspirinOnlineSpeaker::where('online_time_id',$iOnlineTimeId)->where('speaker_id',$iSpeakerId)->first();
			if(count($oCountLog)){
				$oCountLog->increment('listener_count');
			}
			echo 'success';die;
		}else{
			echo 'error';die;
		}
	}

	//个人中心-我的会议-忽略会议
	public function postIgnoreAttend()
	{
		$iOnlineTimeId = intval(Input::get('timeid'));
		$iSpeakerId = intval(Input::get('speakerid'));
		$oInfo = AspirinOnlineListener::where('online_time_id',$iOnlineTimeId)
			->where('speaker_id',$iSpeakerId)
			->where('listener_id',$this->iUserId)
			->where('join_type',1)
			->first();
		if(count($oInfo)){
			$oInfo->is_agree = 2;
			$oInfo->save();
			echo 'success';die;
		}else{
			echo 'error';die;
		}
	}

	/**
	 * 在线会议——讲者申报确认   get 提交
	 * http://2kdll.kydev.net/apiaspirinonline/speaker-submit?iPptId=1&iOnlineId=1&timeid=55&declare_name=代路路&declare_id_num=131182199211112222&declare_bank=北京银行&declare_bank_num=6222100056753098&token=
	 */
	public function getSpeakerSettime()
	{
		$iPptId = intval(Input::get('iPptId'));
		$sAct = Input::get('sAct');
		if(!Input::get('iPptId')){
			return Redirect::to('/aspirinonline/ppt/');
		}
		$iOnlineId = intval(Input::get('iOnlineId'));
		$iCatId = Input::get('iCatId')?intval(Input::get('iCatId')):1;
		$sNowTime = date('Y-m-d H:i:s',time());
		$oOnline = AspirinOnline::where('catid',$iCatId)->where('show_end_time','>',$sNowTime)->orderby('created_at','desc')->first();
		$oPreOpen = AspirinOnlineSpeaker::where('online_catid',$iCatId)->where('open_flag',0)->where('cancel_flag',0)->get();
		$oHasOpen = AspirinOnlineSpeaker::where('online_catid',$iCatId)->where('open_flag',1)->get();
		$iListenerCount = 0;
		foreach($oHasOpen as $k=>$v){
			$iListenerCount+= $v->listener_count;
		}
		$iHasOpen = count($oHasOpen)!=0?count($oHasOpen):0;     //成功举办的场数
		$iTotalCount = count($oHasOpen) + $iListenerCount;      //参与人数
		$iPreOpen = count($oPreOpen)!=0?count($oPreOpen):0;     //预告场数
		//获取所有有效的主题列表
		$oOnlineList = AspirinOnline::where('catid',$iCatId)->where('show_end_time','>',$sNowTime)->orderby('created_at','desc')->get();
		$defaultOnlineInfo = array();
		if($oOnlineList){
			$onlineList = $oOnlineList->toArray();
			foreach ($onlineList as $key => $value) {
				if(!$iOnlineId && $key==0){
					$defaultOnlineInfo = $value;
				}elseif($iOnlineId==$value['id']){
					$defaultOnlineInfo = $value;
				}
			}
		}
		// print_r($defaultOnlineInfo);die;
		$selectDefDate = date("Y-m-d", strtotime($oOnlineList[0]->show_start_time));
		//申报信息填写记录
		$oUserInfo = AspirinOnlineSpeaker::where('speaker_id',$this->iUserId)
			->where('cancel_flag',0)
			->orderby('created_at','desc')
			->first();
		//启动会议时用于会畅展示的课件链接
		$sPptHtml = Config::get('app.url').'/mobile/ppt/'.$iPptId;
		$sPptHtml = isset($sPptHtml)?$sPptHtml:'';
		return View::make('front.aspirin.online.speakersettime')
			->with('iCatId',$iCatId)
			->with('oOnline',$oOnline)
			->with('iTotalCount',$iTotalCount)
			->with('selectDefDate',$selectDefDate)
			->with('iPreOpen',$iPreOpen)
			->with('iHasOpen',$iHasOpen)
			->with('defaultOnlineInfo',$defaultOnlineInfo)
			->with('oOnlineList',$oOnlineList)
			->with('iOnlineId',$iOnlineId)
			->with('oUserInfo',$oUserInfo)
			->with('sPptHtml',$sPptHtml)
			->with('iPptId',$iPptId);
	}

	/**
	 * 在线会议——讲者申报确认   get 提交
	 * http://2kdll.kydev.net/apiaspirinonline/speaker-submit?iPptId=1&iOnlineId=1&timeid=55&declare_name=代路路&declare_id_num=131182199211112222&declare_bank=北京银行&declare_bank_num=6222100056753098&token=
	 */
	public function postSpeakerSettimeSave()
	{
		if(!Input::get('iPptId') || !Input::get('iOnlineId')){
			echo  $this->formatRet(false,"缺少必要参数。");exit;
		}
		$iPptId = intval(Input::get('iPptId'));
		$iOnlineId = intval(Input::get('iOnlineId'));
		$sOnlineDate = trim(Input::get('sOnlineDate'));
		$sOnlineStartTime = trim(Input::get('sOnlineStartTime'));
		$sOnlineEndTime = trim(Input::get('sOnlineEndTime'));
		if(!$sOnlineDate){
			echo $this->formatRet(false,"请输入会议日期。");
			exit;
		}
		if(!$sOnlineStartTime){
			echo $this->formatRet(false,"请输入会议开始时间");
			exit;
		}
		if(!$sOnlineEndTime){
			echo $this->formatRet(false,"请输入会议结束时间。");exit;
		}
		$aOnlineTime = array(
			'online_id'   => $iOnlineId,
			'online_date' => $sOnlineDate,
			'time_period' => "{$sOnlineStartTime}-{$sOnlineEndTime}",
			'created_at'  => date('Y-m-d H:i:s',time())
		);
		//判断改时段是否存在
		$isHave = AspirinOnlineTime::where("online_id",$iOnlineId)->
			where("online_date","{$aOnlineTime['online_date']}")->
			where("time_period","{$aOnlineTime['time_period']}")->first();
		if($isHave){
			echo $this->formatRet(true, "", "", '', array("data"=>array('timeid'=>$isHave->id)));
			// echo $this->formatRet(false,"对不起，提交失败！");
			exit;
		}
		$oOnlineTime = new AspirinOnlineTime($aOnlineTime);
		$res = $oOnlineTime->save();
		if($res){
			echo $this->formatRet(true, "", "", '', array("data"=>array('timeid'=>$oOnlineTime->id)));
			exit;
		}else{
			echo $this->formatRet(false,"对不起，提交失败！");
			exit;
		}
	}

}