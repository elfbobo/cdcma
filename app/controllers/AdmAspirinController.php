<?php

class AdmAspirinController extends BaseController {
	
	CONST  PAGESIZE = 10;

	public function getIndex(){
		Return View::make('admin.aspirin.export');
	}

	//专项基金用户导出
	public function getExportUser()
	{
		//导出csv
		set_time_limit(0);
		header( "Cache-Control: public" );
		header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
		header("Content-Disposition:attachment;filename=入组用户_".date('Ymdhis').".csv");
		header('Content-Type:APPLICATION/OCTET-STREAM');
		ob_start();

		$header = '';
		$header .= "试点城市,医生姓名,医院,科室,电话,归属代表,地区,大区"."\n";
		$header_str =  iconv("utf-8",'gbk',$header);
		$line = "";
		
		//筛选出未参加过在线会议的入组用户
		$aSpeaker = AspirinOnlineSpeaker::where('cancel_flag',0)->orderby('id','asc')->lists('speaker_id');
		$aListener = AspirinOnlineListener::where('is_agree','!=',2)->orderby('id','asc')->lists('listener_id');
		$aLog = array_merge($aSpeaker,$aListener);
		$aLog = array_unique($aLog);
		$oUser = User::where('role_id','>',2)->where('is_h5',4);
		if(!empty($aLog)){
			$oUser = $oUser->whereNotIn('id',$aLog);
		}
		$oUser = $oUser->orderby('id','asc')->get();
		$aUserRegin = User::where('role_id',2)->lists('user_regin','id');
		$aRegin = DB::table('regin')->lists('regin_name','id');
		$aUserArea = User::where('role_id',2)->lists('user_area','id');
		$aArea = DB::table('area')->lists('area_name','id');
		
		foreach($oUser as $k=>$v){
			$v = $this->formatUserInfo($v);
         	$line .= $v->user_city;
         	$line .= ','.$v->user_name;
         	$line .= ','.$v->user_company;
         	$line .= ','.$v->user_department;
         	$line .= ','.$v->user_tel;
         	$iRepId = $v->link_rep_id;
         	$iReginId = !empty($aUserRegin[$iRepId])?$aUserRegin[$iRepId]:0;
         	$sReginName = !empty($aRegin[$iReginId])?$aRegin[$iReginId]:$iReginId;
         	$iAreaId = !empty($aUserArea[$iRepId])?$aUserArea[$iRepId]:0;
         	$sAreaName = !empty($aArea[$iAreaId])?$aArea[$iAreaId]:$iAreaId;
         	if($iRepId){
         		$oRepInfo = User::find($iRepId);
         		$line .= ','.$oRepInfo->user_name;
	         	$line .= ','.$sAreaName;
	         	$line .= ','.$sReginName;
         	}else{
         		$line .= ',,,';
         	}
         	$line .= "\n";
		}
		$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}

	//在线会议详情导出
	public function getExportOnline()
	{
		//导出csv
		set_time_limit(0);
		header( "Cache-Control: public" );
		header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
		header("Content-Disposition:attachment;filename=在线会议_".date('Ymdhis').".csv");
		header('Content-Type:APPLICATION/OCTET-STREAM');
		ob_start();
		$header = '';
		$header .= "试点城市,医生姓名,医院,科室,电话,归属代表,地区,大区,身份,课程主题,参与场次,参会人数,会议时间"."\n";
		$header_str =  iconv("utf-8",'gbk',$header);
		$line = "";
		$aUserRegin = User::where('role_id',2)->lists('user_regin','id');
		$aRegin = DB::table('regin')->lists('regin_name','id');
		$aUserArea = User::where('role_id',2)->lists('user_area','id');
		$aArea = DB::table('area')->lists('area_name','id');
		$oSpeakerLog = AspirinOnlineSpeaker::leftJoin('user','user.id','=','aspirin_online_speaker.speaker_id')->select('aspirin_online_speaker.*','user.role_id','user.user_name','user.user_province','user.user_city','user.user_county','user.user_company','user.user_company_name','user.user_department','user.user_tel','user.user_regin','user.user_area','user.link_rep_id')->where('cancel_flag',0)->orderby('speaker_id','asc')->get();
		if($oSpeakerLog){
			foreach($oSpeakerLog as $v){
				$v = $this->formatUserInfo($v);
				$line .= $v->user_city;
				//代表信息
				if($v->role_id==2){
					$line .= ',,,,';
					$line .= ','.$v->user_name;
					$sAreaName = !empty($aArea[$v->user_area])?$aArea[$v->user_area]:$v->user_area;
					$line .= ','.$sAreaName;
					$sReginName = !empty($aRegin[$v->user_regin])?$aRegin[$v->user_regin]:$v->user_regin;
					$line .= ','.$sReginName;
				}else{
					$line .= ','.$v->user_name;
					$line .= ','.$v->user_company;
					$line .= ','.$v->user_department;
					$line .= ','.$v->user_tel;
					$iRepId = $v->link_rep_id;
					$iReginId = !empty($aUserRegin[$iRepId])?$aUserRegin[$iRepId]:0;
					$sReginName = !empty($aRegin[$iReginId])?$aRegin[$iReginId]:$iReginId;
					$iAreaId = !empty($aUserArea[$iRepId])?$aUserArea[$iRepId]:0;
					$sAreaName = !empty($aArea[$iAreaId])?$aArea[$iAreaId]:$iAreaId;
					if($iRepId){
						$oRepInfo = User::find($iRepId);
						$line .= ','.$oRepInfo->user_name;
						$line .= ','.$sAreaName;
						$line .= ','.$sReginName;
					}else{
						$line .= ',,,';
					}
				}
				$line .= ','.'讲者';
				$sPpt = AspirinOnlinePpt::where('id',$v->ppt_id)->pluck('ppt_title');
				$line .= ','.$sPpt;
				//获取参与场次（同一主题下讲课和报名听课的会议总场数）
				$iListener = 0;
				$iSpeaker = AspirinOnlineSpeaker::where('ppt_id',$v->ppt_id)->where('speaker_id',$v->speaker_id)->where('cancel_flag',0)->count();
				$aSpeaker = AspirinOnlineSpeaker::where('ppt_id',$v->ppt_id)->where('cancel_flag',0)->lists('speaker_id','id');
				if(!empty($aSpeaker)){
					$iListener = AspirinOnlineListener::where('listener_id',$v->speaker_id)->where('is_agree','!=',2)->whereIn('speaker_id',$aSpeaker)->count();
				}
				$iCount = $iSpeaker+$iListener;
				$line .= ','.$iCount;
				$line .= ','.$v->listener_count;
				$oOnlineTime = AspirinOnlineTime::find($v->online_time_id);
				$sTime = substr($oOnlineTime->online_date,0,11).$oOnlineTime->time_period;
				$line .= ','.$sTime;
				$line .= "\n";
			}
		}
		$oListenerLog = AspirinOnlineListener::leftJoin('user','user.id','=','aspirin_online_listener.listener_id')->select('aspirin_online_listener.*','user.role_id','user.user_name','user.user_province','user.user_city','user.user_county','user.user_company','user.user_company_name','user.user_department','user.user_tel','user.user_regin','user.user_area','user.link_rep_id')->where('is_agree','!=',2)->orderby('listener_id','asc')->get();
		if($oListenerLog){
			foreach($oListenerLog as $v){
				$v = $this->formatUserInfo($v);
				$line .= $v->user_city;
				//代表信息
				if($v->role_id==2){
					$line .= ',,,,';
					$line .= ','.$v->user_name;
					$sAreaName = !empty($aArea[$v->user_area])?$aArea[$v->user_area]:$v->user_area;
					$line .= ','.$sAreaName;
					$sReginName = !empty($aRegin[$v->user_regin])?$aRegin[$v->user_regin]:$v->user_regin;
					$line .= ','.$sReginName;
				}else{
					$line .= ','.$v->user_name;
					$line .= ','.$v->user_company;
					$line .= ','.$v->user_department;
					$line .= ','.$v->user_tel;
					$iRepId = $v->link_rep_id;
					$iReginId = !empty($aUserRegin[$iRepId])?$aUserRegin[$iRepId]:0;
					$sReginName = !empty($aRegin[$iReginId])?$aRegin[$iReginId]:$iReginId;
					$iAreaId = !empty($aUserArea[$iRepId])?$aUserArea[$iRepId]:0;
					$sAreaName = !empty($aArea[$iAreaId])?$aArea[$iAreaId]:$iAreaId;
					if($iRepId){
						$oRepInfo = User::find($iRepId);
						$line .= ','.$oRepInfo->user_name;
						$line .= ','.$sAreaName;
						$line .= ','.$sReginName;
					}else{
						$line .= ',,,';
					}
				}
	
				$line .= ','.'听众';
				$oSpeakerInfo = AspirinOnlineSpeaker::where('online_time_id',$v->online_time_id)->where('speaker_id',$v->speaker_id)->where('cancel_flag',0)->first();
				if($oSpeakerInfo){
					$sPpt = AspirinOnlinePpt::where('id',$oSpeakerInfo->ppt_id)->pluck('ppt_title');
					$line .= ','.$sPpt;
					//获取参与场次（同一主题下讲课和报名听课的会议总场数）
					$iListener = 0;
					$iSpeaker = AspirinOnlineSpeaker::where('ppt_id',$oSpeakerInfo->ppt_id)->where('speaker_id',$v->listener_id)->where('cancel_flag',0)->count();
					$aSpeaker = AspirinOnlineSpeaker::where('ppt_id',$oSpeakerInfo->ppt_id)->where('cancel_flag',0)->lists('speaker_id','id');
					if(!empty($aSpeaker)){
						$iListener = AspirinOnlineListener::where('listener_id',$v->listener_id)->where('is_agree','!=',2)->whereIn('speaker_id',$aSpeaker)->count();
					}
					$iCount = $iSpeaker+$iListener;
					$line .= ','.$iCount;
					$line .= ','.$oSpeakerInfo->listener_count;
				}else{
					$line .= ',,,';
				}
				$oOnlineTime = AspirinOnlineTime::find($v->online_time_id);
				$sTime = substr($oOnlineTime->online_date,0,11).$oOnlineTime->time_period;
				$line .= ','.$sTime;
				$line .= "\n";
			}
		}
		$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}

	//健康教育录制详情导出
	public function getExportEducation()
	{
		//导出csv
		set_time_limit(0);
		header( "Cache-Control: public" );
		header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
		header("Content-Disposition:attachment;filename=健康教育_".date('Ymdhis').".csv");
		header('Content-Type:APPLICATION/OCTET-STREAM');
		ob_start();

		$header = '';
		$header .= "试点城市,医生姓名,医院,科室,归属代表,地区,大区,录制主题,时长,视频url,创建时间"."\n";
		$header_str =  iconv("utf-8",'gbk',$header);
		$line = "";
		$aUserRegin = User::where('role_id',2)->lists('user_regin','id');
		$aRegin = DB::table('regin')->lists('regin_name','id');
		$aUserArea = User::where('role_id',2)->lists('user_area','id');
		$aArea = DB::table('area')->lists('area_name','id');
		$oLog = AspirinEducation::leftJoin('user','user.id','=','aspirin_education.user_id')->select('aspirin_education.*','user.role_id','user.user_province','user.user_city','user.user_county','user.user_company','user.user_company_name','user.user_department','user.user_regin','user.user_area','user.link_rep_id')->where('video_flag',0)->orderby('user_id','asc')->get();
		if($oLog){
			foreach($oLog as $v){
				$v = $this->formatUserInfo($v);
				$line .= $v->user_city;
				//代表信息
				if($v->role_id==2){
					$line .= ',,,';
					$line .= ','.$v->user_name;
					$sAreaName = !empty($aArea[$v->user_area])?$aArea[$v->user_area]:$v->user_area;
					$line .= ','.$sAreaName;
					$sReginName = !empty($aRegin[$v->user_regin])?$aRegin[$v->user_regin]:$v->user_regin;
					$line .= ','.$sReginName;
				}else{
					$line .= ','.$v->user_name;
					$line .= ','.$v->user_company;
					$line .= ','.$v->user_department;
					$iRepId = $v->link_rep_id;
					$iReginId = !empty($aUserRegin[$iRepId])?$aUserRegin[$iRepId]:0;
					$sReginName = !empty($aRegin[$iReginId])?$aRegin[$iReginId]:$iReginId;
					$iAreaId = !empty($aUserArea[$iRepId])?$aUserArea[$iRepId]:0;
					$sAreaName = !empty($aArea[$iAreaId])?$aArea[$iAreaId]:$iAreaId;
					if($iRepId){
						$oRepInfo = User::find($iRepId);
						$line .= ','.$oRepInfo->user_name;
						$line .= ','.$sAreaName;
						$line .= ','.$sReginName;
					}else{
						$line .= ',,,';
					}
				}
				$line .= ','.$v->ppt_title;
				$line .= ','.$v->video_time;
				$line .= ','.$v->video_url;
				$line .= ','.substr($v->created_at,0,10);
				$line .= "\n";
			}
		}
		$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}

	//专项基金科研申请和筛查记录导出
	public function getExportApplyScreening($flag = 0)
	{
		//导出csv
		set_time_limit(0);
		header( "Cache-Control: public" );
		header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
		header("Content-Disposition:attachment;filename=科研筛查_".date('Ymdhis').".csv");
		header('Content-Type:APPLICATION/OCTET-STREAM');
		ob_start();

		$header = '';
		$header .= "试点城市,医生姓名,医院,科室,归属代表,地区,大区,提交科研申请次数,风险筛查次数"."\n";
		$header_str =  iconv("utf-8",'gbk',$header);
		$line = "";
		$lastMon = date('Y-m-d', strtotime("-1 week")).' 00:00:00';
		//筛选出参与过科研申请或者筛查的用户
		if($flag == 1){
			$aApply = AspirinResearchApply::where('created_at','>=',$lastMon)->orderby('id','asc')->lists('user_id');
			$aScreening = AspirinScreeningHitLog::where('created_at','>=',$lastMon)->orderby('id','asc')->lists('user_id');
		}else{
			$aApply = AspirinResearchApply::orderby('id','asc')->lists('user_id');
			$aScreening = AspirinScreeningHitLog::orderby('id','asc')->lists('user_id');
		}
		$aLog = array_merge($aApply,$aScreening);
		$aLog = array_unique($aLog);
		if(!empty($aLog)){
			$oUser = User::where('role_id','>',1);
			$oUser = $oUser->whereIn('id',$aLog);
			$oUser = $oUser->orderby('id','asc')->get();
			$aUserRegin = User::where('role_id',2)->lists('user_regin','id');
			$aRegin = DB::table('regin')->lists('regin_name','id');
			$aUserArea = User::where('role_id',2)->lists('user_area','id');
			$aArea = DB::table('area')->lists('area_name','id');
			foreach($oUser as $k=>$v){
				$v = $this->formatUserInfo($v);
				$line .= $v->user_city;
				//代表信息
				if($v->role_id==2){
					$line .= ',,,';
					$line .= ','.$v->user_name;
					$sAreaName = !empty($aArea[$v->user_area])?$aArea[$v->user_area]:$v->user_area;
					$line .= ','.$sAreaName;
					$sReginName = !empty($aRegin[$v->user_regin])?$aRegin[$v->user_regin]:$v->user_regin;
					$line .= ','.$sReginName;
				}else{
					$line .= ','.$v->user_name;
					$line .= ','.$v->user_company;
					$line .= ','.$v->user_department;
					$iRepId = $v->link_rep_id;
					$iReginId = !empty($aUserRegin[$iRepId])?$aUserRegin[$iRepId]:0;
					$sReginName = !empty($aRegin[$iReginId])?$aRegin[$iReginId]:$iReginId;
					$iAreaId = !empty($aUserArea[$iRepId])?$aUserArea[$iRepId]:0;
					$sAreaName = !empty($aArea[$iAreaId])?$aArea[$iAreaId]:$iAreaId;
					if($iRepId){
						$oRepInfo = User::find($iRepId);
						$line .= ','.$oRepInfo->user_name;
						$line .= ','.$sAreaName;
						$line .= ','.$sReginName;
					}else{
						$line .= ',,,';
					}
				}
				if($flag == 1){
					$iApply = AspirinResearchApply::where('user_id',$v->id)->where('created_at','>=',$lastMon)->count();
					$iScreeningHit = AspirinScreeningHitLog::where('user_id',$v->id)->where('created_at','>=',$lastMon)->count();
				}else{
					$iApply = AspirinResearchApply::where('user_id',$v->id)->count();
					$iScreeningHit = AspirinScreeningHitLog::where('user_id',$v->id)->count();
				}
	         	$iApply = isset($iApply)?$iApply:0;
	         	$line .= ','.$iApply;
	         	$iScreeningHit = isset($iScreeningHit)?$iScreeningHit:0;
	         	$line .= ','.$iScreeningHit;
				$line .= "\n";
			}
		}
		$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	//筛查点击记录详情
	public function getExportScreening()
	{
		set_time_limit(0);
		header('Cache-control:public');
		header('Pragma:public');
		header('Content-type:application/vnd.ms-excel');
		header('Content-Disposition:attachment;filename=screening_log_'.date('YmdHis').'.csv');
		header('Content-Type:APPLICATION/OCTET-STREAM');
		ob_start();
		$header_str = iconv("utf-8",'gbk',"登录用户id,登录用户名,设备,创建时间\n");
		$file_str="";
		$oLog = AspirinScreeningHitLog::join('user','aspirin_screening_hit_log.user_id','=','user.id')
			->select('aspirin_screening_hit_log.*','user.user_name')
			->orderBy('aspirin_screening_hit_log.user_id','asc')
			->orderBy('aspirin_screening_hit_log.created_at','desc')
			->get();
		if($oLog){
			foreach($oLog as $v){
				$file_str .= $v->user_id.',';
				$file_str .= $v->user_name.',';
				$file_str .= $v->device.',';
				$file_str .= $v->created_at."\n";
			}
		}
		$file_str= iconv("utf-8",'GBK//TRANSLIT',$file_str);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}

}