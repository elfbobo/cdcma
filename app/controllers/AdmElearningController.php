<?php

use Illuminate\Support\Facades\Redirect;

class AdmElearningController extends BaseController {
	
	CONST  PAGESIZE = 10;
	
	/**
	 * 视频
	 */
	public function getVideoList(){
		$oList = ElearningVideo::getVideoList(self::PAGESIZE);
		return View::make('admin.elearning.videolist')->with('oList',$oList);
	}
	
	public function getVideoAdd(){
		return View ::make('admin.elearning.videoadd');
	}
	
	public function postVideoAddDo(){
		$aInput = Input::all();
		$aVideo = array(
			'video_title'	=> $aInput['video_title'],
			'video_url'		=> $aInput['video_url'],
			'video_thumb'	=> $aInput['thumb_url']
		);
		$oVideo = new ElearningVideo($aVideo);
		$oVideo->save();
		return Redirect::to('/admelearning/video-list');
	}
	
	public function getVideoEdit($iId){
		$oVideo = ElearningVideo::find($iId);
		return View::make('admin.elearning.videoedit')->with('oVideo',$oVideo);
	}
	
	public function postVideoEditDo($iId){
		$aInput = Input::all();
		$oVideo = ElearningVideo::find($iId);
		if($oVideo){
			$oVideo->video_title = $aInput['video_title'];
			$oVideo->video_url = $aInput['video_url'];
			$oVideo->video_thumb = $aInput['thumb_url'];
			$oVideo->save();
		}
		return Redirect::to('/admelearning/video-list');
	}
	
	public function getVideoDel($iId){
		$oVideo = ElearningVideo::find($iId);
		$oVideo->delete();
		return $this->showMessage('删除视频成功','/admelearning/video-list');
	}
	
	/**
	 * ppt
	 */
	public function getPptList(){
		$oList = ElearningPpt::getPptList(self::PAGESIZE);
		return View::make('admin.elearning.pptlist')->with('oList',$oList);
	}
	
	public function getPptAdd(){
		return View ::make('admin.elearning.pptadd');
	}
	
	public function postPptAddDo(){
		$aInput = Input::all();
		$sPptUrl = $aInput['ppt_url'];
		$sImageFolder = str_replace('/ppt/ppt/', '/ppt/image/', $sPptUrl);
		$sImageFolder = str_replace('.pptx', '', $sImageFolder);
		$sImageFolder = str_replace('.ppt', '', $sImageFolder);
		$aPpt = array(
			'ppt_title'		=> $aInput['ppt_title'],
			'ppt_url'		=> $aInput['ppt_url'],
			'ppt_thumb'		=> $aInput['thumb_url'],
			'images_folder'	=> $sImageFolder
		);
		$oPpt = new ElearningPpt($aPpt);
		$oPpt->save();
		return Redirect::to('/admelearning/ppt-list');
	}
	
	public function getPptEdit($iId){
		$oPpt = ElearningPpt::find($iId);
		return View::make('admin.elearning.pptedit')->with('oPpt',$oPpt);
	}
	
	public function postPptEditDo($iId){
		$aInput = Input::all();
		$oPpt = ElearningPpt::find($iId);
		$sPptUrl = $aInput['ppt_url'];
		$sImageFolder = str_replace('/ppt/ppt/', '/ppt/image/', $sPptUrl);
		$sImageFolder = str_replace('.pptx', '', $sImageFolder);
		$sImageFolder = str_replace('.ppt', '', $sImageFolder);
		if($oPpt){
			$oPpt->ppt_title = $aInput['ppt_title'];
			$oPpt->ppt_url = $aInput['ppt_url'];
			$oPpt->ppt_thumb = $aInput['thumb_url'];
			$oPpt->images_folder = $sImageFolder;
			$oPpt->save();
		}
		return Redirect::to('/admelearning/ppt-list');
	}
	
	public function getPptDel($iId){
		$oPpt = ElearningPpt::find($iId);
		$oPpt->delete();
		return $this->showMessage('删除ppt成功','/admelearning/ppt-list');
	}
	
	/**
	 * pdf
	 */
	public function getPdfList(){
		$oList = ElearningPdf::getPdfList(self::PAGESIZE);
		return View::make('admin.elearning.pdflist')->with('oList',$oList);
	}
	
	public function getPdfAdd(){
		return View ::make('admin.elearning.pdfadd');
	}
	
	public function postPdfAddDo(){
		$aInput = Input::all();
		$sPdfUrl = $aInput['pdf_url'];
		$aPdf = array(
			'pdf_title'		=> $aInput['pdf_title'],
			'pdf_url'		=> $aInput['pdf_url'],
			'pdf_thumb'		=> $aInput['thumb_url'],
			'swf_url'		=> $aInput['swf_url']
		);
		$oPdf = new ElearningPdf($aPdf);
		$oPdf->save();
		return Redirect::to('/admelearning/pdf-list');
	}
	
	public function getPdfEdit($iId){
		$oPdf = ElearningPdf::find($iId);
		return View::make('admin.elearning.pdfedit')->with('oPdf',$oPdf);
	}
	
	public function postPdfEditDo($iId){
		$aInput = Input::all();
		$oPdf = ElearningPdf::find($iId);
		$sPdfUrl = $aInput['pdf_url'];
		if($oPdf){
			$oPdf->pdf_title = $aInput['pdf_title'];
			$oPdf->pdf_url = $aInput['pdf_url'];
			$oPdf->pdf_thumb = $aInput['thumb_url'];
			$oPdf->swf_url = $aInput['swf_url'];
			$oPdf->save();
		}
		return Redirect::to('/admelearning/pdf-list');
	}
	
	public function getPdfDel($iId){
		$oPdf = ElearningPdf::find($iId);
		$oPdf->delete();
		return $this->showMessage('删除pdf成功','/admelearning/pdf-list');
	}
	
	//上传图片
	public function postUploadThumb(){
		if($_FILES['upload_file']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['upload_file']['name'];
			$attach_fileext = get_filetype($attach_filename);
			$rand_name = date('YmdHis', time()).rand(1000,9999);
			$sFileName = $rand_name.'.'.$attach_fileext;
			$sPath = "/upload/elearning/thumb";
			$sRealPath = public_path().$sPath;
			mkdirs($sRealPath);
			// $attach_filesize = filesize($_FILES['upload_file']['tmp_name']);
			move_uploaded_file($_FILES['upload_file']['tmp_name'], $sRealPath.DS.$sFileName);
			$sFileNameS = $rand_name . '_s.' . $attach_fileext;
			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 1000, 1000 );
			$sFileUrl = $sPath.'/'.$sFileNameS;
			$json = array('thumb'=>$sFileUrl);
			echo json_encode($json);
			die;
		}
	}
	
	//上传ppt
	public function postUploadPpt(){
		if($_FILES['upload_file_ppt']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['upload_file_ppt']['name'];
			$attach_fileext = get_filetype($attach_filename);
			$rand_name = date('YmdHis', time()).rand(1000,9999);
			$sFileName = $rand_name.'.'.$attach_fileext;
			$sPath = "/upload/elearning/ppt/ppt";
			$sTempPath = "/upload/elearning/ppt/temp";
			$sRealPath = public_path().$sPath;
			$sTempRealPath = public_path().$sTempPath;
			mkdirs($sRealPath);
			mkdirs($sTempRealPath);
			move_uploaded_file($_FILES['upload_file_ppt']['tmp_name'], $sRealPath.DS.$sFileName);
			copy($sRealPath.DS.$sFileName, $sTempRealPath.DS.$sFileName);
			$sFileUrl = $sPath.'/'.$sFileName;
			$json = array('url'=>$sFileUrl);
			echo json_encode($json);
			die;
		}
	}
	
	//上传pdf
	public function postUploadPdf(){
		if($_FILES['upload_file_pdf']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['upload_file_pdf']['name'];
			$attach_fileext = get_filetype($attach_filename);
			$rand_name = date('YmdHis', time()).rand(1000,9999);
			$sFileName = $rand_name.'.'.$attach_fileext;
			$sPath = "/upload/elearning/pdf/pdf";
			$sRealPath = public_path().$sPath;
			mkdirs($sRealPath);
			move_uploaded_file($_FILES['upload_file_pdf']['tmp_name'], $sRealPath.DS.$sFileName);
			$sFileUrl = $sPath.'/'.$sFileName;
			$json = array('url'=>$sFileUrl);
			echo json_encode($json);
			die;
		}
	}
	
	//上传pdf
	public function postUploadSwf(){
		if($_FILES['upload_file_swf']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['upload_file_swf']['name'];
			$attach_fileext = get_filetype($attach_filename);
			$rand_name = date('YmdHis', time()).rand(1000,9999);
			$sFileName = $rand_name.'.'.$attach_fileext;
			$sPath = "/upload/elearning/pdf/swf";
			$sRealPath = public_path().$sPath;
			mkdirs($sRealPath);
			move_uploaded_file($_FILES['upload_file_swf']['tmp_name'], $sRealPath.DS.$sFileName);
			$sFileUrl = $sPath.'/'.$sFileName;
			$json = array('url'=>$sFileUrl);
			echo json_encode($json);
			die;
		}
	}
	
}