<?php

/**
 * API用户信息操作类
 * @author liuy
 *
 */

use Illuminate\Support\Facades\Input;

class ApiUserController extends BaseController
{
    const PAGESIZE = 10;
    public $iUserId = 0;
    public $_time = 0;
    public $_formatTime = '';

    public function __construct()
    {
        //parent::__construct();
        $this->beforeFilter('apilogin', array(
            'except' => array(
                'postRegister',
                'postCheckCode',
                'postLogin',
                'getRegin',
                'getArea',
                'getDepartment',
                'getHospital',
                'getHotline',
                'getPosition',
                'getCheckInviteCode'
            )
        ));
        global $iUserId;
        $this->iUserId = &$iUserId;
        $this->_time = time();
        $this->_formatTime = date("Y-m-d H:i:s", $this->_time);
    }

    /**
     * 用户登录
     * http://cdma.local/apiuser/login?user_nick=gaohongye3&password=123456&is_android=
     * eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
     * eyJpdiI6InQzTHZBQTBkVjBHWDdqcXZxcyt5YUM5TXU1eGsraENBRkNXbkJSWmRJQU09IiwidmFsdWUiOiJITHQ1V2ozTmJnaU5mS1RTczk4emRBazIyYXJGaUtJdmNoVklZOEFBTGlJPSIsIm1hYyI6ImI2YWY1MjY4NzNkYWQ1MzExODMxNTJiOTU3Yjc1ZTNkM2U2ZTc2MzJlMWY5YTk5YzM1NTU5NDE3Zjk5MmUzNjQifQ==
     */
    public function postLogin()
    {
        $sUname = trim(input::get('user_nick'));
        $sPassword = trim(input::get('password'));
        if (!$sUname || !$sPassword) {
            echo $this->formatRet(false, '请输入账号和密码');
            exit;
        }
        $oUser = User::where('user_nick', $sUname)->orWhere('user_tel', $sUname)->orWhere('user_email',
            $sUname)->first();
        if (!$oUser) {
            echo $this->formatRet(false, '账号或密码错误');
            exit;
        }
        //密码校验
        if (!Auth::attempt(array('user_nick' => $sUname, 'password' => $sPassword))
            && !Auth::attempt(array('user_tel' => $sUname, 'password' => $sPassword))
            && !Auth::attempt(array('user_email' => $sUname, 'password' => $sPassword))) {
            echo $this->formatRet(false, '密码错误');
            exit;
        }
        $iUid = $oUser->id;
        //判断用户是否同意了免责条款  1：还没有同意；2：已经同意可以浏览页面
        $oUserDisclaimerLog = UserDisclaimerLog::where('user_id', $iUid)->first();
        $iUserType = $oUser->role_id;
        $sHasGreenDisclaimer = 2;
        // if($iUserType == 3){//等于3医生需要同意免责声明
        if (count($oUserDisclaimerLog) < 1) {
            $sHasGreenDisclaimer = 1;
        } else {
            $sHasGreenDisclaimer = 2;
        }
        // }
        //给当前用户分配注册编码
        if (!$oUser->invite_code) {
            //生成邀请码
            $iInvitedCode = User::MakeInviteCode();
            $oUser->invite_code = $iInvitedCode;
            $oUser->save();
        }
        //判断当前用户是否是安卓非试点，如果是，不允许登录
        $is_android = intval(input::get('is_android', 0));
        if ($is_android == 1) {
            /*$aPilotCity = User::getPilotCity();
			$aPilotCountry = User::getPilotCountry();
			if($oUser->role_id == 3){
				if(!in_array($oUser->user_city,$aPilotCity) && !in_array($oUser->user_county,$aPilotCountry)){
					//非试点城市无法进入专项基金
					echo $this->formatRet(false, '仅限试点城市用户使用');
					exit;
				}
			}*/
        }
        //返回tokent及用户信息
        $sToken = Crypt::encrypt($oUser->id . '\t' . $oUser->user_nick . '\t' . time());
        //用户是否是副高及以上职称标志
        $iPositionFlag = 0;
        if ($oUser->role_id == 2) {
            $iPositionFlag = 1;
        } elseif ($oUser->role_id == 3) {
            /**
             * '1'    => '住院医师',
             * '2' => '主治医师',
             * '3' => '副主任医师',
             * '4' => '主任医师',
             * '5' => '其它' */
            $aPos = User::getPosition();
            if ($oUser->user_position == $aPos[2] || $oUser->user_position == $aPos[3] || $oUser->user_position == $aPos[4]) {
                $iPositionFlag = 2;
            }
        }
        $info = array(
            'token' => $sToken,
            'greed_with_disclaimer' => $sHasGreenDisclaimer,
            'role_id' => $oUser->role_id,
            'user_nick' => $oUser->user_nick,
            'user_name' => $oUser->user_name,
            'user_sex' => $oUser->user_sex,
            'user_tel' => $oUser->user_tel,
            'user_email' => $oUser->user_email,
            'user_address' => $oUser->user_address,
            'user_city' => $oUser->user_city,
            'user_department' => $oUser->user_department,
            'user_position' => $oUser->user_position,
            'user_company' => $oUser->user_company,
            'user_id' => $oUser->id,
            'user_codes' => $oUser->invite_code ? $oUser->invite_code : str_pad($oUser->id, 5, "0", STR_PAD_LEFT),
            'user_thumb' => $oUser->user_thumb ? Config::get('app.url') . $oUser->user_thumb . "?t=" . time() : Config::get('app.url') . '/assets/images/dafault/default.jpg?t=' . time(),
            'invite_code' => $oUser->invite_code,
            'card_auth_flag' => $oUser->card_auth_flag,
            'position_flag' => $iPositionFlag
        );
        echo $this->formatRet(true, '', $info);
        exit;
    }

    /*
	 * http://cdma.local/apiuser/agree-disclaimer
	 */
    public function postAgreeDisclaimer()
    {
        $iUserId = Input::get('user_id');
        $oUserDisclaimerLog = UserDisclaimerLog::where('user_id', $iUserId)->first();
        if (count($oUserDisclaimerLog) > 0) {
            echo $this->formatRet(true, '您已同意免责条款');
            exit;
        } else {
            $oUserDisclaimerLogAdd = new UserDisclaimerLog();
            $oUserDisclaimerLogAdd->user_id = $iUserId;
            $oUserDisclaimerLogAdd->save();
            echo $this->formatRet(true, '您已同意免责条款');
            exit;
        }
    }

    /**
     * 校验cwid是否可用
     */
    public function postCheckCode()
    {
        $sCode = trim(Input::get('invite_code'));
        //判断是否是拜耳代表注册
        if (strtoupper($sCode) == 'BAYER1' || strtoupper($sCode) == 'BAYER2') {
            echo $this->formatRet(true);
            exit;
        }
        //查找对应cwid的代表
        $oDaiBiao = User::getExitUserByCode($sCode);
        if (!$oDaiBiao) {
            echo $this->formatRet(false, '邀请码有误，请您核对后输入');
            exit;
        }
        echo $this->formatRet(true);
        exit;
    }

    /**
     * 注册
     * http://cdma.local/apiuser/register?invite_code=APBHBW&user_nick=ggg&password=123&password_confirm=123&user_name=gaohongye&user_sex=1&user_tel=13285466587&user_email=452353@qq.com&user_department=%E8%82%BF%E7%98%A4%E5%86%85%E7%A7%91&user_province=1&user_city=2&user_county=2&user_company=4&user_company_name=1234_
     * http://cdma.local/apiuser/register?invite_code=APBHBW&user_nick=ggg&password=123&password_confirm=123&user_name=gaohongye&user_sex=1&user_tel=13285466587&user_email=452353@qq.com&user_department=%E8%82%BF%E7%98%A4%E5%86%85%E7%A7%91&user_company=ghyqwe
     */
    public function postRegister()
    {
        //所需字段
        $aInput = Input::only('user_nick', 'user_name', 'user_sex', 'user_tel', 'user_email', 'user_address',
            'user_province', 'user_city', 'user_county', 'user_department', 'user_position', 'user_company',
            'user_company_name', 'user_regin', 'user_area');
        $sPassword = Input::get('password');
        $sPasswordConfirm = Input::get('password_confirm');
        $iGreedWithDisclaimer = Input::get('greed_with_disclaimer');
        if ($iGreedWithDisclaimer != 2) {
            echo $this->formatRet(false, '请同意免责申明，完成注册');
            exit;
        }
        $sInviteCode = Input::get('invite_code');//邀请码
        //校验邀请码
        if (strtoupper($sInviteCode) != 'BAYER1' && strtoupper($sInviteCode) != 'BAYER2') {
            $oDaiBiao = User::getExitUserByCode($sInviteCode);
            if (!$oDaiBiao) {
                echo $this->formatRet(false, '邀请码有误，请您核对您的邀请码');
                exit;
            }
        }
        //必填校验
        $aVlidate = array_merge($aInput,
            array('password' => $sPassword, 'password_confirmation' => $sPasswordConfirm));//用于验证两次密码一致
        if (strtoupper($sInviteCode) != 'BAYER1' && strtoupper($sInviteCode) != 'BAYER2') {
            $validator = Validator::make($aVlidate, User::getRegUserRule(), User::getRegUserError());
        } else {
            $validator = Validator::make($aVlidate, User::getRegUserRuleRep(), User::getRegUserErrorRep());
        }
        if ($validator->fails()) {
            $msg = '验证未通过！';
            $messages = $validator->messages()->toArray();
            foreach ($messages as $k => $v) {
                foreach ($v as $n => $m) {
                    $msg .= $m;
                }
            }
            echo $this->formatRet(false, $msg);
            exit;
        }
        $mobile = '/^1[34578][0-9]{9}$/';
        if (!preg_match($mobile, Input::get('user_tel'))) {
            $msg = "手机校验未通过";
            echo $this->formatRet(false, $msg);
            exit;
        }
        $email = '/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/';
        if (!preg_match($email, Input::get('user_email'))) {
            $msg = "邮箱校验未通过";
            echo $this->formatRet(false, $msg);
            exit;
        }
        if (strtoupper($sInviteCode) != 'BAYER1' && strtoupper($sInviteCode) != 'BAYER2') {
            $iUserCompany = Input::has('user_company') ? Input::get('user_company') : 0;
            $iUserCompanyStr = Input::has('user_company_name') ? Input::get('user_company_name') : '';
            if ($iUserCompany == 0 && $iUserCompanyStr == '') {
                $msg = "请选择医院信息";
                echo $this->formatRet(false, $msg);
                exit;
            }
        }
        $oUser = new User();
        foreach ($aInput as $k => $v) {
            if ($v) {
                $oUser->$k = $v;
            }
        }
        $oUser->password = Hash::make($sPassword);//密码
        if (strtoupper($sInviteCode) != 'BAYER1' && strtoupper($sInviteCode) != 'BAYER2') {
            $oUser->role_id = 3;//医生
            $oUser->link_user_id = $oDaiBiao->id;//关联代表
        } else {
            $oUser->role_id = 2;//代表
            $oUser->user_cwid = $aInput['user_nick'];//代表
            $oUser->link_user_id = 0;//关联代表
            if (strtoupper($sInviteCode) == 'BAYER1') {
                $oUser->rep_type = 1;//关联代表类型
            } else {
                if (strtoupper($sInviteCode) == 'BAYER2') {
                    $oUser->rep_type = 2;//关联代表类型
                }
            }
        }
        //头像处理
        // if ( Input::hasFile('file') ){
        // 	$file = Input::file('file');
        // 	if( $file ){
        // 		$oUser->user_thumb = $this->dealAvatar($file);
        // 	}
        // }
        //医师证照
        $oUser->card_number = Input::has('card_number') ? Input::get('card_number') : '';
        // if ( Input::hasFile('cardfile') ){
        // 	$cardfile = Input::file('cardfile');
        // 	if( $cardfile ){
        // 		$oUser->card_thumb = $this->dealAvatar($cardfile);
        // 	}
        // }
        // if(!empty($oUser->card_number) && !empty($oUser->card_thumb) ){
        // 	$oUser->card_auth_flag = 1;
        // }
        //生成邀请码
        $oUser->invite_code = User::MakeInviteCode();
        //当前注册者关联的所有的上级ids（gk）
        if (isset($oUser['link_user_id']) && $oUser['link_user_id'] != 0) {
            $oSuperior = User::where('id', $oUser['link_user_id'])->first();
            if ($oSuperior->superior_ids) {
                $aSuperiorIds = unserialize($oSuperior->superior_ids);
                array_push($aSuperiorIds, $oUser['link_user_id']);
                $oUser->superior_ids = serialize($aSuperiorIds);
            } else {
                $oUser->superior_ids = serialize(array($oUser['link_user_id']));
            }
            //获取当前用户关联代表的id
            $iRepId = User::getRepId($oUser->superior_ids);
            $oUser->link_rep_id = $iRepId;
        }
        $oUser->created_at = date('Y-m-d H:i:s', time());
        $oUser->save();
        $iUid = $oUser->id;
        //保存免责声明
        $oUserGreedWithDisclaimer = new UserDisclaimerLog;
        $oUserGreedWithDisclaimer->user_id = $iUid;
        $oUserGreedWithDisclaimer->save();
        if ($oUser->role_id == 3) {
            //为用户注册医脉通
            User::RegMedlive($oUser);
        }
        //返回tokent及用户信息
        $sToken = Crypt::encrypt($oUser->id . '\t' . $oUser->user_nick . '\t' . time());
        echo $this->formatRet(true, '',
            array('token' => $sToken, 'user_id' => $oUser->id, 'invite_code' => $oUser->invite_code));
        exit;
    }

    /**
     * 获取个人信息
     */
    public function postUserInfo()
    {
        $oUser = User::find($this->iUserId);
        if (!$oUser) {
            echo $this->formatRet(false, '用户不存在');
            exit;
        }
        //给当前用户分配注册编码
        if (!$oUser->invite_code) {
            //生成邀请码
            $iInvitedCode = User::MakeInviteCode();
            $oUser->invite_code = $iInvitedCode;
            $oUser->save();
        }
        //返回tokent及用户信息
        $sToken = Crypt::encrypt($oUser->id . '\t' . $oUser->user_nick . '\t' . time());
        $sUserRegin = $oUser->user_regin;
        $sUserArea = $oUser->user_area;
        if (is_numeric($sUserRegin)) {
            $sRegin = User::getReginCache();
            $oRegin = json_decode($sRegin);
            $sUserRegin = $oRegin->$sUserRegin;
        }
        if (is_numeric($sUserArea)) {
            $sArea = User::getAreaCache();
            $oArea = json_decode($sArea);
            $sR = $oUser->user_regin;
            $sUserArea = $oArea->$sR->$sUserArea;
        }
        $sUserCity = $oUser->user_city;
        if (is_numeric($sUserCity)) {
            $iUserCity = $sUserCity;
            $sUserCity = Hospital::where('id', $sUserCity)->pluck('name');
            if (!$sUserCity) {
                $sUserCity = $iUserCity;
            }
        }
        $sUserCompany = $oUser->user_company;
        if (is_numeric($sUserCompany)) {
            $iUserCompany = $sUserCompany;
            if ($iUserCompany == 0) {
                $sUserCompany = $oUser->user_company_name;
            } else {
                $sUserCompany = Hospital::where('id', $sUserCompany)->pluck('name');
                if (!$sUserCompany) {
                    $sUserCompany = $iUserCompany;
                }
            }
        }
        if (!$sUserCompany) {
            $sUserCompany = $oUser->user_company_name;
        }
        $info = array(
            'token' => $sToken,
            'role_id' => $oUser->role_id,
            'user_nick' => $oUser->user_nick,
            'user_name' => $oUser->user_name,
            'user_sex' => $oUser->user_sex,
            'user_tel' => $oUser->user_tel,
            'user_email' => $oUser->user_email,
            'user_address' => $oUser->user_address,
            'user_city' => $sUserCity,
            'user_department' => $oUser->user_department,
            'user_position' => $oUser->user_position,
            'user_regin' => $sUserRegin,
            'user_area' => $sUserArea,
            'user_company' => $sUserCompany,
            'user_id' => $oUser->id,
            'user_thumb' => $oUser->user_thumb ? Config::get('app.url') . $oUser->user_thumb : '',
            'invite_code' => $oUser->invite_code
        );
        echo $this->formatRet(true, '', $info);
        exit;
    }

    //修改个人信息页面
    public function getChangeUserInfo()
    {
        $oUser = User::find($this->iUserId);
        if (!$oUser) {
            echo $this->formatRet(false, '用户不存在');
            exit;
        }
        //返回tokent及用户信息
        $sToken = Crypt::encrypt($oUser->id . '\t' . $oUser->user_nick . '\t' . time());
        $aRegin = array();
        $aArea = array();
        $aProvince = array();
        $aCity = array();
        $aCounty = array();
        $aHospital = array();
        if ($oUser->role_id == 3) {
            //医生
            $aProvince = Hospital::getChildrenAPI();
            $aCity = Hospital::getChildrenAPI($oUser->user_province);
            $aCounty = Hospital::getChildrenAPI($oUser->user_city);
            $aHospital = Hospital::getChildrenAPI($oUser->user_county);
        } else {
            if ($oUser->role_id == 2) {
                //代表
                $sRegin = User::getReginCache();
                $aRegin = json_decode($sRegin);
                $aResRegin = array();
                foreach ($aRegin as $k => $v) {
                    $aResRegin[] = array('id' => $k, 'name' => $v);
                }
                $aRegin = $aResRegin;
                $sR = $oUser->user_regin;
                $sArea = User::getAreaCache();
                $oAreaAll = json_decode($sArea);
                $oArea = $oAreaAll->$sR;
                $aArea = array();
                foreach ($oArea as $k => $v) {
                    $aArea[] = array('id' => $k, 'name' => $v);
                }
                $aProvince = Hospital::getChildrenAPI();
                $aCity = Hospital::getChildrenAPI($oUser->user_province);
            }
        }
        $iUserCompany = $oUser->user_company;
        if (trim($oUser->user_company_name) != '') {
            $iUserCompany = 0;
        }
        $aDepartmentArr = User::getDepartment();
        $aDepartment = array();
        foreach ($aDepartmentArr as $k => $v) {
            $aDepartment[] = array('id' => $k, 'name' => $v);
        }
        $iDepartment = 0;
        if ($oUser->user_department) {
            foreach ($aDepartmentArr as $k => $v) {
                if ($v == $oUser->user_department) {
                    $iDepartment = $k;
                }
            }
        }
        $aPositionArr = User::getPosition();
        $aPosition = array();
        foreach ($aPositionArr as $k => $v) {
            $aPosition[] = array('id' => $k, 'name' => $v);
        }
        $iPosition = 0;
        if ($oUser->user_position) {
            foreach ($aPositionArr as $k => $v) {
                if ($v == $oUser->user_position) {
                    $iPosition = $k;
                }
            }
        }
        $info = array(
            'token' => $sToken,
            'role_id' => $oUser->role_id,
            'user_nick' => $oUser->user_nick,
            'user_name' => $oUser->user_name,
            'user_sex' => $oUser->user_sex,
            'user_tel' => $oUser->user_tel,
            'user_email' => $oUser->user_email,
            'user_address' => $oUser->user_address,
            'user_department' => $iDepartment,
            'user_position' => $oUser->role_id == 2 ? $oUser->user_position : $iPosition,
            'user_regin' => $oUser->user_regin,
            'user_area' => $oUser->user_area,
            'user_province' => $oUser->user_province,
            'user_city' => $oUser->user_city,
            'user_county' => $oUser->user_county,
            'user_company' => $iUserCompany,
            'user_company_name' => $oUser->user_company_name,
            'user_id' => $oUser->id,
            'user_thumb' => $oUser->user_thumb ? Config::get('app.url') . $oUser->user_thumb : '',
            'invite_code' => $oUser->invite_code,
            'arr_regin' => $aRegin,
            'arr_area' => $aArea,
            'arr_province' => $aProvince,
            'arr_city' => $aCity,
            'arr_county' => $aCounty,
            'arr_hospital' => $aHospital,
            'arr_department' => $aDepartment,
            'arr_position' => $aPosition
        );
        echo $this->formatRet(true, '', $info);
        exit;
    }

    //新接口v2版
    public function postChangeUserInfoDo()
    {
        $oUser = User::find($this->iUserId);
        if (!$oUser) {
            echo $this->formatRet(false, '用户不存在');
            exit;
        }
        $input = Input::all();
        //保存到数据库的数据数组
        $aData = array();
        //验证用户输入
        if (isset($input['user_address'])) {
            $aData['user_address'] = $input['user_address'];
        }
        if (isset($input['user_province']) && $input['user_province'] != 0 && is_numeric($input['user_province'])) {
            $aData['user_province'] = intval($input['user_province']);
        } else {
            echo $this->formatRet(false, '请选择省份信息');
            exit;
        }
        if (isset($input['user_city']) && $input['user_city'] != 0 && is_numeric($input['user_city'])) {
            $aData['user_city'] = intval($input['user_city']);
        } else {
            echo $this->formatRet(false, '请选择城市信息');
            exit;
        }
        if ($oUser->role_id == 3) {
            //医生
            if (isset($input['user_county']) && $input['user_county'] != 0 && is_numeric($input['user_county'])) {
                $aData['user_county'] = intval($input['user_county']);
            } else {
                echo $this->formatRet(false, '请选择区/县信息');
                exit;
            }
            if (isset($input['user_company']) && $input['user_company'] != 0 && is_numeric($input['user_company'])) {
                $aData['user_company'] = intval($input['user_company']);
                if (isset($input['user_company_name'])) {
                    $aData['user_company_name'] = $input['user_company_name'];
                } else {
                    $aData['user_company_name'] = '';
                }
            } else {
                if (isset($input['user_company_name']) && $input['user_company_name'] != '') {
                    $aData['user_company_name'] = $input['user_company_name'];
                } else {
                    echo $this->formatRet(false, '请选择医院信息');
                    exit;
                }
            }
            if (isset($input['user_department']) && $input['user_department'] != 0 && is_numeric($input['user_department'])) {
                $aDepartment = User::getDepartment();
                $aData['user_department'] = $aDepartment[intval($input['user_department'])];
            } else {
                echo $this->formatRet(false, '请选择科室信息');
                exit;
            }
            if (isset($input['user_position']) && $input['user_position'] != 0 && is_numeric($input['user_position'])) {
                $aPosition = User::getPosition();
                $aData['user_position'] = $aPosition[intval($input['user_position'])];
            } else {
                echo $this->formatRet(false, '请选择职称信息');
                exit;
            }
        } else {
            if ($oUser->role_id == 2) {
                //代表
                if (isset($input['user_regin']) && $input['user_regin'] != 0 && is_numeric($input['user_regin'])) {
                    $aData['user_regin'] = intval($input['user_regin']);
                } else {
                    echo $this->formatRet(false, '请选择大区信息');
                    exit;
                }
                if (isset($input['user_area']) && $input['user_area'] != 0 && is_numeric($input['user_area'])) {
                    $aData['user_area'] = intval($input['user_area']);
                } else {
                    echo $this->formatRet(false, '请选择地区信息');
                    exit;
                }
                if (isset($input['user_position'])) {
                    $aData['user_position'] = $input['user_position'];
                }
            }
        }
        foreach ($aData as $k => $v) {
            $oUser->$k = $v;
        }
        $oUser->save();
        echo $this->formatRet(true, '');
        exit;
    }

    /**
     * @url apiuser/change-user-info-dov2
     */
    public function postChangeUserInfoDov2()
    {
        $oUser = User::find($this->iUserId);
        if (!$oUser) {
            echo $this->formatRet(false, '用户不存在');
            exit;
        }
        $input = Input::all();
        //保存到数据库的数据数组
        $aData = array();
        $aData['user_sex'] = $input['user_sex'];
        //验证用户输入
        if (empty($input['user_tel'])) {
            echo $this->formatRet(false, '请输入您的手机号码');
            exit;
        }
        if (empty($input['user_email'])) {
            echo $this->formatRet(false, '请输入您的邮箱');
            exit;
        }
        $mobile = '/^\d{11}$/';
        if (!preg_match($mobile, Input::get('user_tel'))) {
            $msg = "手机校验未通过";
            echo $this->formatRet(false, $msg);
            exit;
        }
        //手机号码和邮箱是否重复
        $sMsg = User::IsUserTelEmailRepeat($input, $this->iUserId);
        if ($sMsg == 'tel_repeat') {
            echo $this->formatRet(false, '该手机号码用户已经存在了');
            exit;
        } elseif ($sMsg == 'email_repeat') {
            echo $this->formatRet(false, '该邮箱用户已经存在了');
            exit;
        } else {
            $aData['user_tel'] = $input['user_tel'];
            $aData['user_email'] = $input['user_email'];
        }
        if (isset($input['user_address'])) {
            $aData['user_address'] = $input['user_address'];
        }
        if (isset($input['user_province']) && $input['user_province'] != 0 && is_numeric($input['user_province'])) {
            $aData['user_province'] = intval($input['user_province']);
        } else {
            echo $this->formatRet(false, '请选择省份信息');
            exit;
        }
        if (isset($input['user_city']) && $input['user_city'] != 0 && is_numeric($input['user_city'])) {
            $aData['user_city'] = intval($input['user_city']);
        } else {
            echo $this->formatRet(false, '请选择城市信息');
            exit;
        }
        if ($oUser->role_id == 3) {
            //医生
            if (isset($input['user_county']) && $input['user_county'] != 0 && is_numeric($input['user_county'])) {
                $aData['user_county'] = intval($input['user_county']);
            } else {
                echo $this->formatRet(false, '请选择区/县信息');
                exit;
            }
            if (isset($input['user_company']) && $input['user_company'] != 0 && is_numeric($input['user_company'])) {
                $aData['user_company'] = intval($input['user_company']);
                if (isset($input['user_company_name'])) {
                    $aData['user_company_name'] = $input['user_company_name'];
                } else {
                    $aData['user_company_name'] = '';
                }
            } else {
                if (isset($input['user_company_name']) && $input['user_company_name'] != '') {
                    $aData['user_company_name'] = $input['user_company_name'];
                } else {
                    echo $this->formatRet(false, '请选择医院信息');
                    exit;
                }
            }
            if (isset($input['user_department']) && $input['user_department'] != 0 && is_numeric($input['user_department'])) {
                $aDepartment = User::getDepartment();
                $aData['user_department'] = $aDepartment[intval($input['user_department'])];
            } else {
                echo $this->formatRet(false, '请选择科室信息');
                exit;
            }
            if (isset($input['user_position']) && $input['user_position'] != 0 && is_numeric($input['user_position'])) {
                $aPosition = User::getPosition();
                $aData['user_position'] = $aPosition[intval($input['user_position'])];
            } else {
                echo $this->formatRet(false, '请选择职称信息');
                exit;
            }
        } else {
            if ($oUser->role_id == 2) {
                //代表
                if (isset($input['user_regin']) && $input['user_regin'] != 0 && is_numeric($input['user_regin'])) {
                    $aData['user_regin'] = intval($input['user_regin']);
                } else {
                    echo $this->formatRet(false, '请选择大区信息');
                    exit;
                }
                if (isset($input['user_area']) && $input['user_area'] != 0 && is_numeric($input['user_area'])) {
                    $aData['user_area'] = intval($input['user_area']);
                } else {
                    echo $this->formatRet(false, '请选择地区信息');
                    exit;
                }
                if (isset($input['user_position'])) {
                    $aData['user_position'] = $input['user_position'];
                }
            }
        }
        foreach ($aData as $k => $v) {
            $oUser->$k = $v;
        }
        $oUser->save();
        echo $this->formatRet(true, '');
        exit;
    }

    /**
     * 修改头像
     */
    public function postAvatar()
    {
        $oUser = User::find($this->iUserId);
        if (!$oUser) {
            echo $this->formatRet(false, '用户不存在');
            exit;
        }
        if (!Input::hasFile('file')) {
            echo $this->formatRet(false, '请上传头像');
            exit;
        }
        $file = Input::file('file');
        $sUserThumb = $this->dealAvatar($file);
        $oUser->user_thumb = $sUserThumb;
        $oUser->save();
        echo $this->formatRet(true, '', array('user_thumb' => Config::get('app.url') . $oUser->user_thumb));
        exit;
    }

    /**
     * 修改密码
     */
    public function postChangePassword()
    {
        $oUser = User::find($this->iUserId);
        if (!$oUser) {
            echo $this->formatRet(false, '用户不存在');
            exit;
        }
        $sPassword = trim(Input::get('password'));
        $sNewPassword = trim(Input::get('new_password'));
        $sNewPasswordConfirm = trim(Input::get('new_password_confirm'));

        $validator = Validator::make(
            array(
                'password' => $sPassword,
                'new_password' => $sNewPassword,
                'new_password_confirmation' => $sNewPasswordConfirm
            ),//用于验证两次密码一致
            array('password' => 'required', 'new_password' => 'required|confirmed'),//confirmed用于验证两次密码一致
            array(
                'password.required' => '原密码不可为空！',
                'new_password.required' => '新密码不可为空！',
                'new_password.confirmed' => '两次密码输入不一致！'
            )
        );
        if ($validator->fails()) {
            $msg = '';
            $messages = $validator->messages()->toArray();
            foreach ($messages as $k => $v) {
                foreach ($v as $n => $m) {
                    $msg .= $m;
                }
            }
            echo $this->formatRet(false, $msg);
            exit;
        }
        //校验原密码
        if (!Hash::check(trim($sPassword), $oUser->password)) {
            echo $this->formatRet(false, '原密码错误');
            exit;
        }
        //更新密码
        $oUser->password = Hash::make($sNewPassword);
        $oUser->save();
        $sToken = Crypt::encrypt($oUser->id . '\t' . $oUser->user_nick . '\t' . time());
        echo $this->formatRet(true, '', array('token' => $sToken));
        exit;
    }

    /**
     *
     * 获取积分排行
     */
    public function postScoreList()
    {
        $oUser = User::find($this->iUserId);
        if (!$oUser) {
            echo $this->formatRet(false, '用户不存在');
            exit;
        }
        $iRoleId = Input::get('role_id');
        if (!in_array($iRoleId, array(2, 3))) {
            //取当前用户的role_id
            $iRoleId = $oUser->role_id;
        }
        //获取当前用户积分排行
        $iUserRank = User::getUserRankById($oUser->role_id, $this->iUserId);
        //获取前一百名用户的积分排行
        $aUser = User::getUserScoreList($iRoleId, 100, $this->iUserId);
        //数据处理
        $aArr = array();
        foreach ($aUser as $score) {
            $aArr[] = array(
                'role_id' => $score['role_id'],
                'user_nick' => $score['user_name'],
                'user_id' => $score['id'],
                'live_min' => $score['user_live_score'],
                'review_min' => $score['user_review_score'],
                'user_score' => $score['user_score']
            );
        }
        echo $this->formatRet(true, '', array('user_rank' => $iUserRank), $aArr);
        exit;
    }

    /**
     * 我的积分
     */
    public function postMyscore()
    {
        $oUser = User::find($this->iUserId);
        if (!$oUser) {
            echo $this->formatRet(false, '用户不存在');
            exit;
        }
        $iUserScore = $oUser->user_score;
        $iLiveMin = VideoLog::getUserMinByType($this->iUserId, 2);//直播
        $iReviewMin = VideoLog::getUserMinByType($this->iUserId, 1);//录播
        //获取列表
        $oVideoLogs = VideoLog::getUserVideoLog($this->iUserId);
        $aArr = array();
        foreach ($oVideoLogs as $log) {
            $aArr[] = array(
                'video_title' => $log->video_title,
                'video_type' => $log->video_type,
                'watch_minutes' => $log->watch_minutes,
                'video_id' => $log->video_id
            );
        }
        echo $this->formatRet(true, '',
            array('user_score' => $iUserScore, 'live_min' => $iLiveMin, 'review_min' => $iReviewMin), $aArr);
        exit;
    }

    /**
     * 预约列表
     */
    public function postReservList()
    {
        $oUser = User::find($this->iUserId);
        if (!$oUser) {
            echo $this->formatRet(false, '用户不存在');
            exit;
        }
        $oReservList = UserOrder::where('user_id', $this->iUserId)->get();
        $aArr = array();
        foreach ($oReservList as $reserv) {
            $aArr[] = array(
                'doc_name' => $reserv->doc_name,
                'doc_department' => $reserv->doc_department,
                'order_time' => $reserv->order_time,
                'order_id' => $reserv->id
            );
        }
        echo $this->formatRet(true, '', null, $aArr);
        exit;
    }

    /**
     * 新增预约
     */
    public function postReservAdd()
    {
        $oUser = User::find($this->iUserId);
        if (!$oUser) {
            echo $this->formatRet(false, '用户不存在');
            exit;
        }
        if ($oUser->role_id == 3) {
            echo $this->formatRet(false, '该账号无权限');
            exit;
        }//医生不可预约
        $aInput = Input::only('doc_name', 'doc_department', 'order_time');
        $validator = Validator::make($aInput, UserOrder::getValidateRule(), UserOrder::getValidateError());
        if ($validator->fails()) {
            $msg = '验证未通过！';
            $messages = $validator->messages()->toArray();
            foreach ($messages as $k => $v) {
                foreach ($v as $n => $m) {
                    $msg .= $m;
                }
            }
            echo $this->formatRet(false, $msg);
            exit;
        }
        //入库
        $aUserorder = array(
            'user_id' => $this->iUserId,
            'doc_name' => $aInput['doc_name'],
            'doc_department' => $aInput['doc_department'],
            'order_time' => $aInput['order_time']
        );
        $oReserv = new UserOrder($aUserorder);
        $oReserv->save();
        echo $this->formatRet(true, '', array('order_id' => $oReserv->id));
        exit;
    }

    //====================通用   S=============================

    /**
     * 通用，处理头像
     * @param unknown_type $file
     */
    private function dealAvatar($file)
    {
        $sExt = $file->getClientOriginalExtension();//扩展名
        if (!in_array($sExt, array('jpg', 'png', 'jpeg'))) {
            echo $this->formatRet(false, '图片格式错误，目前支持jpg、png、jpeg');
            exit;
        }
        //处理图片
        $sNewName = date('YmdHis') . rand(1, 9999) . '.' . $sExt;//新文件名
        $sFilePath = '/upload/' . date('Y') . '/' . date('md') . '/';
        mkdirs(public_path() . $sFilePath,0777);
        $file->move(public_path() . $sFilePath, $sNewName);
        return $sFilePath . $sNewName;
    }

    //====================通用  E =============================
    /*
	 * http://cdma.local/apiuser/rep-new-info?token=eyJpdiI6InZkQ3Q5MVA3TTl6aUtWdkRORFJOVDVuSFIxc2JkVG5USGVWUEo3eWVmeWc9IiwidmFsdWUiOiJteG1WZDN3T1JYYkpPbW1SbXhzOHZWdDljVUZGR2NwTVlMTE82M2JETmVIMVhzMzVmQjhMeWpzMDJsUklOdWhOXC9IS25JdlVFUEhWUzNmUnpVQUU0SEE9PSIsIm1hYyI6IjljY2NkNDc4MWEwOWZjYzE4NGNhNjZhMzg4NDRjOTM0M2VkOTcxNmNmOGMzNGZkYWEzMThiNzFkMmU3MDcxYjIifQ==
	 */
    public function getRepNewInfo()
    {
        // $this->iUserId = 1794;
        $iUserId = $this->iUserId;
        $oUser = User::find($iUserId);
        if ($oUser->role_id == 2 && !is_numeric($oUser->user_regin)) {
            //用户信息需要更新地区数组
            $sArea = User::getAreaCache();
            $sRegin = User::getReginCache();
            $oArea = json_decode($sArea);
            $oRegin = json_decode($sRegin);
            $aRegin = array();
            foreach ($oRegin as $k => $v) {
                $aRegin[] = array(
                    'id' => $k,
                    'name' => $v
                );
            }
            // $aData = array('regin'=>$aRegin,'area'=>$aArea);
            echo $this->formatRet(true, '', $aRegin);
            exit;
        } else {
            echo $this->formatRet(false, '');
            exit;
        }
    }

    /*
	 * http://cdma.local/apiuser/rep-regin-area?areaid=1&reginid=2&token=eyJpdiI6InZkQ3Q5MVA3TTl6aUtWdkRORFJOVDVuSFIxc2JkVG5USGVWUEo3eWVmeWc9IiwidmFsdWUiOiJteG1WZDN3T1JYYkpPbW1SbXhzOHZWdDljVUZGR2NwTVlMTE82M2JETmVIMVhzMzVmQjhMeWpzMDJsUklOdWhOXC9IS25JdlVFUEhWUzNmUnpVQUU0SEE9PSIsIm1hYyI6IjljY2NkNDc4MWEwOWZjYzE4NGNhNjZhMzg4NDRjOTM0M2VkOTcxNmNmOGMzNGZkYWEzMThiNzFkMmU3MDcxYjIifQ==
	 */
    public function postRepReginArea()
    {
        // $this->iUserId = 1794;
        $iUserId = $this->iUserId;
        if (!Input::has('areaid') || !Input::has('reginid')) {
            echo $this->formatRet(false, '请选择大区及地区');
            exit;
        }
        $iAreaId = Input::get('areaid');
        $iReginId = Input::get('reginid');
        $oUser = User::find($iUserId);
        $oUser->user_regin = $iReginId;
        $oUser->user_area = $iAreaId;
        $oUser->save();
        echo $this->formatRet(true, '更新标准大区及地区成功');
    }

    /*
	 * http://cdma.local/apiuser/regin
	 */
    public function getRegin()
    {
        $sRegin = User::getReginCache();
        $oRegin = json_decode($sRegin);
        $aRegin = array();
        foreach ($oRegin as $k => $v) {
            $aRegin[] = array(
                'id' => $k,
                'name' => $v
            );
        }
        echo $this->formatRet(true, '', $aRegin);
    }

    /*
	 * http://cdma.local/apiuser/area?reginid=1
	 */
    public function getArea()
    {
        $iReginId = Input::get('reginid');
        $sArea = User::getAreaCache();
        $oArea = json_decode($sArea);
        if (isset($oArea->$iReginId)) {
            $oArea = $oArea->$iReginId;
            $aArea = array();
            foreach ($oArea as $k => $v) {
                $aArea[] = array(
                    'id' => $k,
                    'name' => $v
                );
            }
            echo $this->formatRet(true, '', $aArea);
        } else {
            echo $this->formatRet(true, '', array());
        }
    }

    public function getDepartment()
    {
        $aDep = User::getDepartment();
        $arr = array();
        foreach ($aDep as $k => $v) {
            $arr[] = array(
                'id' => $k,
                'name' => $v
            );
        }
        echo $this->formatRet(true, '', $arr);
    }

    public function getPosition()
    {
        $aPos = User::getPosition();
        $arr = array();
        foreach ($aPos as $k => $v) {
            $arr[] = array(
                'id' => $k,
                'name' => $v
            );
        }
        echo $this->formatRet(true, '', $arr);
    }

    /*
	 * http://cdma.local/apiuser/hospital?pid=1
	 */
    //医院下拉获取下一级
    public function getHospital()
    {
        $iPId = Input::has('pid') ? Input::get('pid') : 0;
        $aHosp = Hospital::getChildren($iPId);
        $arr = array();
        foreach ($aHosp as $k => $v) {
            if ($k >= 10000000) {
                if (!$arr) {
                    $arr[] = array(
                        'id' => 0,
                        'name' => '=请选择='
                    );
                }
            }
            $arr[] = array(
                'id' => $k,
                'name' => $v
            );
        }
        echo $this->formatRet(true, '', $arr);
        exit;
    }

    /*
	 * 接口判断用户是否更新城市及科室信息
	 * http://cdma.local/apiuser/new-city-info?token=eyJpdiI6InZkQ3Q5MVA3TTl6aUtWdkRORFJOVDVuSFIxc2JkVG5USGVWUEo3eWVmeWc9IiwidmFsdWUiOiJteG1WZDN3T1JYYkpPbW1SbXhzOHZWdDljVUZGR2NwTVlMTE82M2JETmVIMVhzMzVmQjhMeWpzMDJsUklOdWhOXC9IS25JdlVFUEhWUzNmUnpVQUU0SEE9PSIsIm1hYyI6IjljY2NkNDc4MWEwOWZjYzE4NGNhNjZhMzg4NDRjOTM0M2VkOTcxNmNmOGMzNGZkYWEzMThiNzFkMmU3MDcxYjIifQ==
	 */
    public function getNewCityInfo()
    {
        $iUserId = $this->iUserId;
        $oUser = User::find($iUserId);
        if ($oUser->role_id == 2 && (!is_numeric($oUser->user_city))) {
            $aHosp = Hospital::getChildren();
            $arr = array();
            foreach ($aHosp as $k => $v) {
                $arr[] = array(
                    'id' => $k,
                    'name' => $v
                );
            }
            echo $this->formatRet(true, '代表', array('province' => $arr));
            exit;
        }
        if ($oUser->role_id == 3 && (!is_numeric($oUser->user_city) || !is_numeric($oUser->user_company))) {
            $aHosp = Hospital::getChildren();
            $arr1 = array();
            foreach ($aHosp as $k => $v) {
                $arr1[] = array(
                    'id' => $k,
                    'name' => $v
                );
            }

            $aDep = User::getDepartment();
            $arr2 = array();
            foreach ($aDep as $k => $v) {
                $arr2[] = array(
                    'id' => $k,
                    'name' => $v
                );
            }
            echo $this->formatRet(true, '医生', array('province' => $arr1, 'department' => $arr2));
            exit;
        }
        echo $this->formatRet(false, '');
        exit;
    }

    /*
	 * 接口更新省份城市医院科室等信息
	 * http://cdma.local/apiuser/new-city-info-do?user_province=1&user_city=2&user_county=3&user_company=4&user_company_name=ghy&user_department=心血管内科&token=eyJpdiI6InZkQ3Q5MVA3TTl6aUtWdkRORFJOVDVuSFIxc2JkVG5USGVWUEo3eWVmeWc9IiwidmFsdWUiOiJteG1WZDN3T1JYYkpPbW1SbXhzOHZWdDljVUZGR2NwTVlMTE82M2JETmVIMVhzMzVmQjhMeWpzMDJsUklOdWhOXC9IS25JdlVFUEhWUzNmUnpVQUU0SEE9PSIsIm1hYyI6IjljY2NkNDc4MWEwOWZjYzE4NGNhNjZhMzg4NDRjOTM0M2VkOTcxNmNmOGMzNGZkYWEzMThiNzFkMmU3MDcxYjIifQ==
	 */
    public function postNewCityInfoDo()
    {
        $input = Input::all();
        $iUserId = $this->iUserId;
        $oUser = User::find($iUserId);
        if (!is_numeric($input['user_province']) || $input['user_province'] == 0) {
            echo $this->formatRet(false, '请选择省份');
            exit;
        }
        if (!is_numeric($input['user_city']) || $input['user_city'] == 0) {
            echo $this->formatRet(false, '请选择城市');
            exit;
        }
        if ($oUser->role_id == 3) {
            //医生
            if (!is_numeric($input['user_county']) || $input['user_county'] == 0) {
                echo $this->formatRet(false, '请选择区/县');
                exit;
            }
            if (!is_numeric($input['user_company']) || $input['user_company'] == 0) {
                if (!$input['user_company_name']) {
                    echo $this->formatRet(false, '请选择医院');
                    exit;
                }
            }
            if (!$input['user_department']) {
                echo $this->formatRet(false, '请选择科室');
                exit;
            }
        }
        foreach ($input as $k => $v) {
            if ($k != 'token') {
                $oUser->$k = $v;
            }
        }
        $oUser->save();
        echo $this->formatRet(true, '成功');
        exit;
    }

    /*
	 * 接口判断医生用户是否更新职称信息
	 * /apiuser/doc-new-pos-info?token=
	 */
    public function getDocNewPosInfo()
    {
        $iUserId = $this->iUserId;
        $oUser = User::find($iUserId);
        $aPos = User::getPosition();
        if ($oUser->role_id == 3 && (!in_array($oUser->user_position, $aPos))) {
            $arr = array();
            foreach ($aPos as $k => $v) {
                $arr[] = array(
                    'id' => $k,
                    'name' => $v
                );
            }
            echo $this->formatRet(true, '医生', array('position' => $arr));
            exit;
        }
        echo $this->formatRet(false, '');
        exit;
    }

    /*
	 * 接口更新医生职称信息
	 * /apiuser/doc-new-pos-info-do?user_position=主任医师&token=
	 */
    public function postDocNewPosInfoDo()
    {
        $input = Input::all();
        $iUserId = $this->iUserId;
        $oUser = User::find($iUserId);
        $aPos = User::getPosition();
        if ($oUser->role_id == 3) {
            if (!$input['user_position'] || !in_array($input['user_position'], $aPos)) {
                echo $this->formatRet(false, '请选择职称');
                exit;
            }
        }
        $oUser->user_position = $input['user_position'];
        $oUser->save();
        echo $this->formatRet(true, '成功');
        exit;
    }

    /*
	 * 个人中心-我的培训列表页面
	 * http://cdma.local/apiuser/user-train?token=eyJpdiI6InQzTHZBQTBkVjBHWDdqcXZxcyt5YUM5TXU1eGsraENBRkNXbkJSWmRJQU09IiwidmFsdWUiOiJITHQ1V2ozTmJnaU5mS1RTczk4emRBazIyYXJGaUtJdmNoVklZOEFBTGlJPSIsIm1hYyI6ImI2YWY1MjY4NzNkYWQ1MzExODMxNTJiOTU3Yjc1ZTNkM2U2ZTc2MzJlMWY5YTk5YzM1NTU5NDE3Zjk5MmUzNjQifQ==
	 */
    public function getUserTrain()
    {
        $iUserId = $this->iUserId;
        $oUser = User::find($iUserId);
        if ($oUser->role_id != 2) {
            //如果该用户不是代表
            echo $this->formatRet(false, '权限受阻，我的培训列表只有代表才能访问');
            exit;
        }
        // $oTrain = UserTrain::where('end_time','>=',date('Y-m-d H:i:s'))->orderBy('start_time','ASC')->get();
        $oTrain = UserTrain::getAllTrain();;
        $aTrain = array();
        foreach ($oTrain as $k => $v) {
            $aTrain[] = array(
                'id' => $v->id,
                'train_title' => $v->train_title,
                'start_time' => strtotime($v->start_time),
                'end_time' => strtotime($v->end_time),
                'time' => substr($v->start_time, 0, 16) . '-' . substr($v->end_time, 11, 5),
                'has_review' => ($v->train_type == 2) ? 1 : 0
            );
        }
        echo $this->formatRet(true, '', $aTrain);
        exit;
    }

    /*
	 * 个人中心-我的培训进入培训
	 * http://cdma.local/apiuser/enter-train?id=7&token=eyJpdiI6InQzTHZBQTBkVjBHWDdqcXZxcyt5YUM5TXU1eGsraENBRkNXbkJSWmRJQU09IiwidmFsdWUiOiJITHQ1V2ozTmJnaU5mS1RTczk4emRBazIyYXJGaUtJdmNoVklZOEFBTGlJPSIsIm1hYyI6ImI2YWY1MjY4NzNkYWQ1MzExODMxNTJiOTU3Yjc1ZTNkM2U2ZTc2MzJlMWY5YTk5YzM1NTU5NDE3Zjk5MmUzNjQifQ==
	 */
    public function getEnterTrain()
    {
        if (!Input::has('token') || !Input::has('id')) {
            echo $this->formatRet(false, "传递参数不正确");
            exit;
        }
        $iUserId = $this->iUserId;
        $oUser = User::find($iUserId);
        $iTrainId = Input::get('id');
        $oTrain = UserTrain::find($iTrainId);
        if ($oTrain) {
            //用户信息存数据库操作
            $aTrainLog = array(
                'train_id' => $iTrainId,
                'user_id' => $iUserId,
                'device' => 1
            );
            $oUserTrainLog = new UserTrainLog($aTrainLog);
            $oUserTrainLog->save();
            $url = $oTrain->train_url . '?nickName=' . urlencode($oUser->user_nick);
            $arr = array('url' => $url);
            echo $this->formatRet(true, '', $arr);
            exit;
        } else {
            echo $this->formatRet(false, '不存在该培训或者该培训已被删除');
            exit;
        }
    }

    /**
     * 个人中心-我的培训-查看录播视频
     * http://cdma.local/apiuser/train-review?trainid=7&token=eyJpdiI6InQzTHZBQTBkVjBHWDdqcXZxcyt5YUM5TXU1eGsraENBRkNXbkJSWmRJQU09IiwidmFsdWUiOiJITHQ1V2ozTmJnaU5mS1RTczk4emRBazIyYXJGaUtJdmNoVklZOEFBTGlJPSIsIm1hYyI6ImI2YWY1MjY4NzNkYWQ1MzExODMxNTJiOTU3Yjc1ZTNkM2U2ZTc2MzJlMWY5YTk5YzM1NTU5NDE3Zjk5MmUzNjQifQ==
     */
    public function getTrainReview()
    {
        if (!Input::has('token') || !Input::has('trainid')) {
            echo $this->formatRet(false, "传递参数不正确");
            exit;
        }
        $iUserId = $this->iUserId;
        $iTrainId = Input::get('trainid', 0);
        $oTrain = UserTrain::find($iTrainId);
        $iCatId = 3;
        $isZan = FaceVideoZanLog::isUserZan($iTrainId, $iUserId, $iCatId);
        if ($oTrain) {
            //点击量+1
            $oTrain->increment('train_review_hits');
            $arr = array(
                'train_title' => $oTrain->train_title,
                'train_url' => Config::get('app.url') . $oTrain->train_url,
                'zancount' => $oTrain->train_support,
                'iszan' => $isZan
            );
            echo $this->formatRet(true, '', $arr);
            exit;
        } else {
            echo $this->formatRet(false, '不存在该培训或者该培训已被删除');
            exit;
        }
    }

    /*
	 * 个人中心-我的培训-记录观看视频时长
	 *  http://cdma.local/apiuser/train-log?ivideoid=7&imin=12&token=eyJpdiI6InQzTHZBQTBkVjBHWDdqcXZxcyt5YUM5TXU1eGsraENBRkNXbkJSWmRJQU09IiwidmFsdWUiOiJITHQ1V2ozTmJnaU5mS1RTczk4emRBazIyYXJGaUtJdmNoVklZOEFBTGlJPSIsIm1hYyI6ImI2YWY1MjY4NzNkYWQ1MzExODMxNTJiOTU3Yjc1ZTNkM2U2ZTc2MzJlMWY5YTk5YzM1NTU5NDE3Zjk5MmUzNjQifQ==
	 * */
    public function getTrainLog()
    {
        $iUserId = $this->iUserId;
        $iVideoId = Input::get('ivideoid');
        $iMin = Input::get('imin');
        $oUserTrainReviewLogfind = UserTrainReviewLog::where('train_id', $iVideoId)->where('user_id',
            $iUserId)->first();
        if (count($oUserTrainReviewLogfind)) {
            if ($oUserTrainReviewLogfind->watch_minutes < $iMin) {
                $oUserTrainReviewLogfind->watch_minutes = $iMin;
                $oUserTrainReviewLogfind->save();
                echo $this->formatRet(true, '成功');
                exit;
            } else {
                echo $this->formatRet(true, '成功');
                exit;
            }
        } else {
            $oUserTrainReviewLog = new UserTrainReviewLog;
            $oUserTrainReviewLog->user_id = $iUserId;
            $oUserTrainReviewLog->train_id = $iVideoId;
            $oUserTrainReviewLog->watch_minutes = $iMin;
            $oUserTrainReviewLog->device = 1;
            $oUserTrainReviewLog->save();
            echo $this->formatRet(true, '成功');
            exit;
        }
    }

    //获取资讯电话接口
    public function getHotline()
    {
        $sHotline = HOTLINE;
        echo $this->formatRet(true, '', array('hotline' => $sHotline));
        exit;
    }

    /**
     * 新版我的预约--本月可预约专家
     * http://cdma.local/apiuser/reserv-expert-list?token=eyJpdiI6InQzTHZBQTBkVjBHWDdqcXZxcyt5YUM5TXU1eGsraENBRkNXbkJSWmRJQU09IiwidmFsdWUiOiJITHQ1V2ozTmJnaU5mS1RTczk4emRBazIyYXJGaUtJdmNoVklZOEFBTGlJPSIsIm1hYyI6ImI2YWY1MjY4NzNkYWQ1MzExODMxNTJiOTU3Yjc1ZTNkM2U2ZTc2MzJlMWY5YTk5YzM1NTU5NDE3Zjk5MmUzNjQifQ==
     */
    public function getReservExpertList()
    {
        if (!Input::has('token')) {
            echo $this->formatRet(false, "传递参数不正确");
            exit;
        }
        $iUserId = $this->iUserId;
        $oUser = User::find($iUserId);
        if ($oUser->role_id != 2) {
            echo $this->formatRet(false, "您没有权限进入该页面");
            exit;
        }
        Auth::loginUsingId($iUserId);
        //获取本月可预约专家
        $oDoc = UserOrderDoc::where('start_time', '>', date('Y-m-d H:i:s'))->where(function ($query) {
            $query->where('user_regin', '')->orWhere('user_regin', 'LIKE', "%|Auth::User()->user_regin|%");
        })->get();
        //积分排行前50代表
        //$oUser = User::getUserScoreList(2,50);
        $iIsInFront = 1;
        /*foreach($oUser as $k=>$v){
			if($iUserId==$v->id){
				$iIsInFront = 1;
				break;
			}
		}*/
        $aDoc = array();
        foreach ($oDoc as $k => $v) {
            $aDoc[] = array(
                'doc_id' => $v->id,
                'doc_name' => $v->doc_name,
                'doc_department' => $v->doc_department,
                'doc_hospital' => $v->doc_hospital,
                'doc_thumb' => $v->doc_thumb ? Config::get('app.url') . $v->doc_thumb : '',
                'time' => substr($v->start_time, 0, 16) . '-' . substr($v->end_time, 11, 5)
            );
        }
        $arr = array('experts' => $aDoc, 'isinfront' => $iIsInFront);
        echo $this->formatRet(true, '', $arr);
        exit;
    }

    /**
     * 新版我的预约--预约专家提交
     */
    public function getReservExpertOrder()
    {
        if (!Input::has('token') || !Input::has('docid')) {
            echo $this->formatRet(false, "传递参数不正确");
            exit;
        }
        $iUserId = $this->iUserId;
        $iDocId = Input::get('docid');
        $oDoc = UserOrderDoc::find($iDocId);
        if (!$oDoc) {
            echo $this->formatRet(false, "不存在该专家");
            exit;
        }
        $oUser = User::find($iUserId);
        if ($oUser->role_id != 2) {
            echo $this->formatRet(false, "您没有权限执行该操作");
            exit;
        }
        //积分排行前50代表
        //$oUser = User::getUserScoreList(2,50);
        $iIsInFront = 1;
        /*foreach($oUser as $k=>$v){
			if($iUserId==$v->id){
				$iIsInFront = 1;
				break;
			}
		}*/
        if ($iIsInFront == 0) {
            echo $this->formatRet(false, "积分排行前50名才能预约专家 ");
            exit;
        }
        //查看是否有该用户的该条预约信息
        $oUserOrder = UserOrder::where('user_id', $iUserId)->where('user_order_doc_id', $iDocId)->get();
        if (count($oUserOrder)) {
            echo $this->formatRet(false, "您已经预约了该专家，无需重复预约 ");
            exit;
        }
        $aUserOrder = array(
            'user_id' => $iUserId,
            'user_order_doc_id' => $oDoc->id,
            'doc_name' => $oDoc->doc_name,
            'doc_department' => $oDoc->doc_department,
            'order_time' => substr($oDoc->start_time, 0, 10)
        );
        $oUserOrder = new UserOrder($aUserOrder);
        $oUserOrder->save();
        echo $this->formatRet(true, '预约已提交，预约审核通过后会与您电话联系，请耐心等待');
        exit;
    }

    /**
     * 代表管理医生页面
     * http://cdma.local/apiuser/mydoc
     */
    public function getMydoc()
    {
        $iUserId = $this->iUserId;
        $iRoleId = User::where('id', $iUserId)->pluck('role_id');
        if ($iRoleId != 2) {
            //若不是代表
            echo $this->formatRet(false, "您没有权限");
            exit;
        }
        //获取当前代表下面的医生用户
        $oDoc = User::select('id', 'user_name', 'user_company', 'user_company_name')->where('link_rep_id',
            $iUserId)->get();
        $aDocIds = array();
        $aHospital = Hospital::lists('name', 'id');
        foreach ($oDoc as $k => $v) {
            $aDocIds[] = $v->id;
            //获得医院名称
            $sUserCompany = $v->user_company;
            if (is_numeric($sUserCompany) && $sUserCompany != 0) {
                $sUserCompany = !empty($aHospital[$sUserCompany]) ? $aHospital[$sUserCompany] : '';
            }
            if (!$sUserCompany) {
                $sUserCompany = $v->user_company_name;
            }
            $oDoc[$k]->user_company = $sUserCompany;
        }
        $aUserSurvey = User::select('id', 'user_type')->whereIn('id', $aDocIds)->lists('user_type', 'id');
        $aUserAdalateType = UserSurveySub::select('user_id', 'adalate_type')
            ->where('phase_id', Config::get('config.phase_id'))
            ->whereIn('user_id', $aDocIds)
            ->where('adalate_type', '>', 0)
            ->lists('adalate_type', 'user_id');
        $aData = array();
        foreach ($oDoc as $k => $v) {
            $iSurveyFlag = 0;    //是否显示推送调研
            $iUserType = 0;        //用户类型
            $iAdalateType = 0;    //拜新同偏好者或拜新同中立者（1/2）
            if (!empty($aUserSurvey[$v->id])) {
                $iUserType = $aUserSurvey[$v->id];
                if (!empty($aUserAdalateType[$v->id])) {
                    $iAdalateType = $aUserAdalateType[$v->id];
                }
            }
            if ($iAdalateType == 0 && ($iUserType == 1 || $iUserType == 3)) {
                $iSurveyFlag = 1;
            }
            $arr = array(
                'id' => $v->id,
                'user_name' => $v->user_name,
                'user_company' => $v->user_company,
                'survey_flag' => $iSurveyFlag,
                'user_type' => $iUserType,
                'adalate_type' => $iAdalateType
            );
            //第一期特殊处理，只能选择用户类型 start
            if (Config::get('config.phase_id') == 1) {
                $arr['survey_flag'] = 0;
                $arr['adalate_type'] = 0;
            }
            //第一期特殊处理，只能选择用户类型 end
            $aData[] = $arr;
        }
        echo $this->formatRet(true, '', $aData);
        exit;
    }

    /**
     * 代表管理医生页面用户选择拜新同或拜阿用户提交
     * http://cdma.local/apiuser/submit-user-type?doctype=25:3|26:2|27:1
     */
    public function getSubmitUserType()
    {
        $sDocType = Input::get('doctype', '');
        if (!$sDocType) {
            echo $this->formatRet(false, "请至少选择一个用户类型后提交");
            exit;
        }
        $aDocType = explode('|', $sDocType);
        foreach ($aDocType as $k => $v) {
            $aUserTypeInfo = explode(':', $v);
            $iDocId = $aUserTypeInfo[0];
            $iDocType = $aUserTypeInfo[1];
            $oUser = User::find($iDocId);
            if ($oUser && $oUser->user_type == 0) {
                $oUser->user_type = $iDocType;
                $oUser->save();
            }
        }
        echo $this->formatRet(true, '提交成功');
        exit;
    }

    /**
     * 推送调研接口
     * http://cdma.local/apiuser/push-survey?uid=1787
     */
    public function getPushSurvey()
    {
        $iDocId = Input::get('uid', 0);
        if (!$iDocId) {
            echo $this->formatRet(false, "参数错误");
            exit;
        }
        $iCloseSurvey = Config::get('config.is_survey_open');
        if ($iCloseSurvey != 1) {
            echo $this->formatRet(false, "调研功能已关闭，无法推送调研");
            exit;
        }
        $iUserType = User::select('user_type')->where('id', $iDocId)->pluck('user_type');
        if ($iUserType != 1 && $iUserType != 3) {
            //不是拜新同用户
            echo $this->formatRet(false, "无法给当前用户推送调研");
            exit;
        }
        $oUserSurveySub = UserSurveySub::where('user_id', $iDocId)->where('phase_id', Config::get('config.phase_id'))->first();
        if (!$oUserSurveySub) {        //若未选择过用户类型（表中无该医生记录）,给用户推送当前期调研
            $aUserSurveySub = array(
                'user_id' => $iDocId,
                'phase_id' => Config::get('config.phase_id')
            );
            $oUserSurveySub = new UserSurveySub($aUserSurveySub);
            $oUserSurveySub->save();
        } else {
            $oUserSurveySub->is_refuse_survey = 0;
            $oUserSurveySub->save();
        }
        echo $this->formatRet(true, '推送调研成功');
        exit;
    }

    /**
     * 全文求助是否开启
     * http://cdma.local/apiuser/open-close?token=
     */
    public function getOpenClose()
    {
        $iOPenOrClose = Config::get("config.is_pub_email_open");
        $data = array('flag' => $iOPenOrClose);
        echo $this->formatRet(true, '', $data);
    }

    /**
     * http://2kghy.kydev.net/apiuser/user-device-save?device_token=5d92cb47d459941686321bf7f8ecfc348bdb8ec6c2f42a0b928e804be313e1e8&device_type=1&token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
     * 收集用户设备token
     */
    public function getUserDeviceSave()
    {
        $sToken = Input::get('device_token', '');
        $iDeviceType = Input::get('device_type', 1);
        $oUserDevice = AspirinUserDevice::where('user_id', $this->iUserId)->first();
        if (!empty($oUserDevice)) {
            //存在该用户记录则修改
            $oUserDevice->token = $sToken;
            $oUserDevice->device_type = $iDeviceType;
            $oUserDevice->save();
        } else {
            //不存在，新增
            $aUserDevice = array(
                'user_id' => $this->iUserId,
                'token' => $sToken,
                'device_type' => $iDeviceType
            );
            $oUserDevice = new AspirinUserDevice($aUserDevice);
            $oUserDevice->save();
        }
        echo $this->formatRet(true, '成功');
        exit;
    }

    /**
     * http://2kghy.kydev.net/apiuser/notice-unread-count?token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
     * 获取未读消息通知条数
     */
    public function getNoticeUnreadCount()
    {
        $iUserId = $this->iUserId;
        $iCount = AspirinUserNotice::where('user_id', $iUserId)->where('has_read', 0)->count();
        echo $this->formatRet(true, '成功', '', array('count' => $iCount));
        exit;
    }

    /**
     * http://2kghy.kydev.net/apiuser/notice-list?page=1&pagesize=3&token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
     * 消息通知列表页
     */
    public function getNoticeList()
    {
        $iPage = Input::get('page', 1);
        $iPageSize = Input::get('pagesize', 10);
        $oNotice = AspirinUserNotice::where('user_id', $this->iUserId)
            ->orderBy('id', 'DESC')
            ->skip(($iPage - 1) * $iPageSize)
            ->take($iPageSize)
            ->get();
        $aNotice = array();
        foreach ($oNotice as $v) {
            $arr = array(
                'id' => $v->id,
                'content' => $v->notice_content,
                'notice_type' => $v->notice_type,
                'has_read' => $v->has_read,
                'created_at' => substr($v->created_at, 0, 16)
            );
            $aNotice[] = $arr;
        }
        echo $this->formatRet(true, '成功', '', $aNotice);
        exit;
    }

    /**
     * http://2kghy.kydev.net/apiuser/notice-detail?id=1&token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
     * 消息通知详细
     */
    public function getNoticeDetail()
    {
        $iNoticeId = Input::get('id', 0);
        if ($iNoticeId == 0) {
            //批量推送的消息，获取用户最新的批量发送的消息
            $oNotice = AspirinUserNotice::where('user_id', $this->iUserId)
                ->where('push_log', 'batch_push')
                ->orderBy('has_read', 'ASC')
                ->orderBy('id', 'DESC')
                ->first();
        } else {
            $oNotice = AspirinUserNotice::find($iNoticeId);
        }
        if (!$oNotice) {
            echo $this->formatRet(false, '该消息通知不存在');
            exit;
        }
        //设置为已读状态
        if ($oNotice->has_read == 0) {
            $oNotice->has_read = 1;
            $oNotice->save();
        }
        $aNotice = array(
            'id' => $iNoticeId,
            'notice_type' => $oNotice->notice_type,
            'detail_id' => $oNotice->detail_id
        );
        echo $this->formatRet(true, '成功', '', $aNotice);
        exit;
    }

    /**
     * http://2kghy.kydev.net/apiuser/share-info?token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
     * 个人中心分享APP
     */
    public function getShareInfo()
    {
        //获取当前用户的用户编码
        $sCode = User::where('id', $this->iUserId)->pluck('invite_code');
        $aShareInfo = array(
            'share_title' => '国卫健康云',
            'share_thumb' => Config::get('app.url') . '/assets/images/mobile/logo.png',
            'share_url' => Config::get('app.url') . '/aspirinshare/download/' . $sCode
        );
        echo $this->formatRet(true, '成功', '', $aShareInfo);
        exit;
    }

    /**
     * 我的麦粒
     * http://2kghy.kydev.net/apiuser/my-maili?token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
     */
    public function getMyMaili()
    {
        global $iUserId;
        $iUid = $iUserId;
        $aData = array();
        $iMedliveId = User::getDocUserMedliveId($iUid);
        $aData['used_num'] = AspirinUserGoldLog::getUserUsedMailiCount($iUid);
        $aData['own_num'] = User::getUserMLCount($iMedliveId);
        $sHashId = User::getHashidOrCheckid($iMedliveId, User::HASH_EXT_KEY_HASHID);
        $sCheckId = User::getHashidOrCheckid($iMedliveId, User::HASH_EXT_KEY_CHECKID);
        $aData['url'] = 'http://www.' . TOP_DOMAIN . '/force_login.php?hashid=' . $sHashId . '&checkid=' . $sCheckId . '&url=' . urlencode('http://gift.medlive.cn/gift/');
        echo $this->formatRet(true, '成功', '', $aData);
        exit;
    }

    /**
     * 接口获取是否更新医师认证信息（弹出认证页面）
     * /apiuser/doc-new-card-info?token=
     */
    public function getDocNewCardInfo()
    {
        $oUser = User::find($this->iUserId);
        if (!$oUser) {
            echo $this->formatRet(false, '该用户不存在');
            exit;
        }
        $arr = array();
        $aHospital = array();
        $aPositionArr = User::getPosition();
        // var_dump($oUser->card_auth_flag);
        // echo $this->formatRet(true,'成功',$aPositionArr,'success');
        // exit;
        $card_auth_flags = array(0, 3);
        if ($oUser->role_id == 3 && (in_array($oUser->card_auth_flag,
                    $card_auth_flags) || !in_array($oUser->user_position, $aPositionArr))) {
            $sHospital = '';
            $aHospital = Hospital::getChildren($oUser->user_county);
            if ($oUser->user_company != 0 && is_numeric($oUser->user_company)) {
                foreach ($aHospital as $k => $v) {
                    if ($k == $oUser->user_company) {
                        $sHospital = $v;
                    }
                }
            } else {
                $sHospital = $oUser->user_company_name;
            }
            $aPosition = array();
            foreach ($aPositionArr as $k => $v) {
                if ($v != "住院医师") {
                    $aPosition[] = array('id' => $k, 'name' => $v);
                }
            }
            $iPosition = 0;
            if ($oUser->user_position) {
                foreach ($aPositionArr as $k => $v) {
                    if ($v == $oUser->user_position) {
                        $iPosition = $k;
                    }
                }
            }
            $arr = array(
                'flag' => 1,
                'user_name' => $oUser->user_name,
                'user_company' => $sHospital ? $sHospital : '',
                'user_department' => $oUser->user_department,
                'user_position' => $iPosition,
                // 'card_number'       => $oUser->card_number?$oUser->card_number:'',
                // 'card_thumb'        => $oUser->card_thumb?Config::get('app.url').$oUser->card_thumb:'',
                'arr_position' => $aPosition
            );
            echo $this->formatRet(true, '成功', $arr, 'success');
            exit;
        } else {
            $arr = array('flag' => 0);
            echo $this->formatRet(true, '成功', $arr, 'success');
            exit;
        }
    }

    /**
     * 接口获取 个人中心--医师认证
     * /apiuser/doc-card-info?token=
     */
    public function getDocCardInfo()
    {
        $oUser = User::find($this->iUserId);
        $arr = array();
        $aHospital = array();
        if ($oUser && $oUser->role_id == 3) {
            $sHospital = '';
            $aHospital = Hospital::getChildren($oUser->user_county);
            if ($oUser->user_company != 0 && is_numeric($oUser->user_company)) {
                foreach ($aHospital as $k => $v) {
                    if ($k == $oUser->user_company) {
                        $sHospital = $v;
                    }
                }
            } else {
                $sHospital = $oUser->user_company_name;
            }
            $aPositionArr = User::getPosition();
            $aPosition = array();
            foreach ($aPositionArr as $k => $v) {
                $aPosition[] = array('id' => $k, 'name' => $v);
            }
            $iPosition = 0;
            if ($oUser->user_position) {
                foreach ($aPositionArr as $k => $v) {
                    if ($v == $oUser->user_position) {
                        $iPosition = $k;
                    }
                }
            }
            $arr = array(
                'user_name' => $oUser->user_name,
                'user_company' => $sHospital ? $sHospital : '',
                'user_department' => $oUser->user_department,
                'user_position' => $iPosition,
                'card_number' => $oUser->card_number ? $oUser->card_number : '',
                'card_thumb' => $oUser->card_thumb ? Config::get('app.url') . $oUser->card_thumb : '',
                'arr_position' => $aPosition
            );
            echo $this->formatRet(true, '成功', $arr, 'success');
            exit;
        }
        echo $this->formatRet(false, '');
        exit;
    }

    /**
     * 接口医师认证信息提交
     * /apiuser/doc-chk-code-do?user_position=1&card_number=&token=
     */
    public function postDocChkCodeDo()
    {
        $oUser = User::find($this->iUserId);
        if (!$oUser) {
            echo $this->formatRet(false, '用户不存在', null, null, ['info'=>'用户不存在']);
            exit;
        }
        $input = Input::all();
        $invite_code_true = 0;
        // $input['invite_code'] = 'OQK8B';
        if (isset($input['invite_code'])) {
            $link_rep_id = User::where('role_id', 2)->where("invite_code", $input['invite_code'])->pluck("id");
            if ($oUser['link_rep_id'] == $link_rep_id) {
                $invite_code_true = 1;
            }
        }
        if (!$invite_code_true) {
            echo $this->formatRet(false, '您输入的邀请码与原邀请码不一致，请更新信息再认证，谢谢配合！', null, null, ['info'=>'您输入的邀请码与原邀请码不一致，请更新信息再认证，谢谢配合！']);
            exit;
        }
        echo $this->formatRet(true, '成功', null, null, ['info'=>'成功','status'=>200]);
        exit;
    }

    /**
     * 接口医师认证信息提交
     * /apiuser/doc-card-info-do?user_position=1&card_number=&token=
     */
    public function postDocCardInfoDo()
    {
        $oUser = User::find($this->iUserId);
        if (!$oUser) {
            echo $this->formatRet(false, '用户不存在');
            exit;
        }
        $input = Input::all();
        // print_r($oUser->role_id);
        // print_r($input);die;
        //保存到数据库的数据数组
        $aData = array();
        if ($oUser->role_id == 3) {
            // echo $invite_code_true;die;
            if (isset($input['user_position']) && $input['user_position'] != 0 && is_numeric($input['user_position'])) {
                $aPosition = User::getPosition();
                $aData['user_position'] = $aPosition[intval($input['user_position'])];
            } else {
                echo $this->formatRet(false, '请选择职称信息');
                exit;
            }
            if (isset($input['card_number'])) {
                $aData['card_number'] = $input['card_number'];
            } else {
                echo $this->formatRet(false, '请填写医师证号');
                exit;
            }
            if (isset($input['user_mobile'])) {
                $aData['user_mobile'] = $input['user_mobile'];
            } else {
                $aData['user_mobile'] = $oUser->user_tel;
                //echo $this->formatRet(false, '请填写医师联系手机号');exit;
            }
            //modified henry 2018-09-10 13:20
            /*
            if (isset($oUser['user_mobile'])) {
            	$aData['user_mobile'] = $oUser['user_mobile'];
            } else {
            	echo $this->formatRet(false, '请填写医师联系手机号');
            	exit;
            }*/

            // 验证医院信息是否完整
            if (empty($oUser->user_company_name) && ($oUser->user_company <= 0)) {
                echo $this->formatRet(false, '您的医院信息不完整，请到个人中心重新设置');
                exit;
            }

            // 验证身份证号
            if (!isset($input['bank_idcard_code']) || 18 != strlen(trim($input['bank_idcard_code']))) {
                echo $this->formatRet(false, '身份证号格式有误');
                exit;
            }

            if (!Input::hasFile('cardfile')) {
                echo $this->formatRet(false, '请上传医师证照');
                exit;
            }
            $file = Input::file('cardfile');
            $aData['card_thumb'] = $this->dealAvatar($file);
            if (!Input::hasFile('sign_icon')) {
                echo $this->formatRet(false, '您必须签名同意协议');
                exit;
            }
            $signfile = Input::file('sign_icon');
            $aData['sign_icon'] = $this->dealAvatar($signfile);

            // 医师认证信息 bugfix
            list($width, $height, $type, $attr) = getimagesize(public_path() . $aData['sign_icon']);
            // 如果宽大于高时表示为小图片，则需要图片合成
            if ($width > $height) {
                $image_source = imagecreatefromjpeg(public_path(). '/assets/images/aspirin/ysrz.jpg');
                // 使图片背景透明
                $image_dest = image2transparent(public_path(). $aData['sign_icon']);

                // 缩放比例
                $per = round(95/$height,3);
                $n_w = $width*$per;
                $n_h = $height*$per;
                $new_dest = imagecreatetruecolor($n_w, $n_h);
                // copy部分图像并调整
                imagecopyresized($new_dest, $image_dest,0, 0,0, 0,$n_w, $n_h, $width, $height);
                // 合并图片
                imagecopymerge($image_source, $new_dest, 480, 3330, 0, 0, imagesx($new_dest), imagesy($new_dest), 100);
                // 覆盖原文件
                imagejpeg($image_source,public_path() . $aData['sign_icon']);
            }

            foreach ($aData as $k => $v) {
                $oUser->$k = $v;
            }
            $oUser->card_auth_flag = 1;
            $res = $oUser->save();
            if ($res) {
                //保存医生付款信息
                $UserBankAccount = new UserBankAccount();
                $UserBankAccount->user_id = $this->iUserId;
                $UserBankAccount->bank_realname = $input['bank_realname'];
                $UserBankAccount->bank_idcard_code = $input['bank_idcard_code'];
                $UserBankAccount->bank_name = $input['bank_name'];
                $UserBankAccount->bank_account = $input['bank_account'];
                $UserBankAccount->save();
                echo $this->formatRet(true, '成功');
                exit;
            } else {
                echo $this->formatRet(false, '');
                exit;
            }
        }
        echo $this->formatRet(false, '');
        exit;
    }

    /**
     * 点击进入专项基金时判断是否认证
     * /apiuser/aspirin-auth?token=
     */
    public function getAspirinAuth()
    {
        $oUser = User::find($this->iUserId);
        if (!$oUser) {
            echo $this->formatRet(false, '用户不存在');
            exit;
        }
        if ($oUser->role_id != 3) {
            $iFlag = '';
        } else {
            $iFlag = $oUser->card_auth_flag;
        }
        $arr = array('flag' => $iFlag);
        echo $this->formatRet(true, '成功', $arr, 'success');
        exit;
    }

    /**
     * 判断用户是否是副高及以上职称用户
     * /apiuser/position-auth?token=
     */
    public function getPositionAuth()
    {
        $oUser = User::find($this->iUserId);
        if (!$oUser) {
            echo $this->formatRet(false, '用户不存在');
            exit;
        }
        $iFlag = 0;
        if ($oUser->role_id == 2) {
            $iFlag = 1;
        } elseif ($oUser->role_id == 3) {
            $aPos = User::getPosition();
            if ($oUser->user_position == $aPos[2] || $oUser->user_position == $aPos[3] || $oUser->user_position == $aPos[4]) {
                $iFlag = 2;
            }
        }
        $arr = array('flag' => $iFlag);
        echo $this->formatRet(true, '成功', $arr, 'success');
        exit;
    }

    /**
     * 判断用户是否是试点城市用户，非试点无权进入
     * /apiuser/aspirin-pilotcity?token=
     */
    public function getAspirinPilotcity()
    {
        $oUser = User::find($this->iUserId);
        if (!$oUser) {
            echo $this->formatRet(false, '用户不存在');
            exit;
        }
        // $aPilotCity = User::getPilotCity();
        // $aPilotCountry = User::getPilotCountry();
        $iFlag = 0;
        /*if($oUser->role_id == 3){
			if(!in_array($oUser->user_city,$aPilotCity) && !in_array($oUser->user_county,$aPilotCountry)){
				$iFlag = 1;
			}
		}*/
        $arr = array('flag' => $iFlag);
        echo $this->formatRet(true, '成功', $arr, 'success');
        exit;
    }

    /**
     * 我的国卫健康云积分
     * health_integral
     */
    public function postMyintegral()
    {
        $iUid = $this->iUserId;
        $oUser = User::find($iUid);
        if (!$oUser) {
            echo $this->formatRet(false, '用户不存在');
            exit;
        }
        $integral = $oUser->health_integral;
        //获取积分日志列表
        $IntegralLog = new IntegralLog();
        $totalIntegral = IntegralLog::where('user_id', $iUid)->where('types', "=", 'in')->sum('number');
        $aArr = array();
        $page = Input::get('page');
        $skip = ($page - 1) * 15;
        $oResult = IntegralLog::select('user_health_integral_log.*')
            ->where('types', '=', "in")->where('user_id', '=', $iUid)
            ->orderBy('created_at', 'DESC')->skip($skip)->take(15)->get();
        foreach ($oResult as $log) {
            switch ($log->obj_type) {
                case 'video':
                    $obj_type = "视频";
                    break;
                case 'view':
                    $obj_type = "视频";
                    break;
                case 'class':
                    $obj_type = "参会";
                    break;
                case 'buycme':
                    $obj_type = "CME";
                    break;
            }
            if ($log->number == 0) {
                continue;
            }
            $aArr[] = array(
                'title' => $log->obj_title,
                'type' => $obj_type,
                'number' => $log->number,
                'obj_id' => $log->obj_id
            );
        }
        echo $this->formatRet(true, '获取成功！', array('health_integral' => $integral, 'total_integral' => $totalIntegral),
            $aArr);
        exit;
    }

    /**
     *
     * 获取积分排行 postScoreList
     */
    public function postIntegralrank()
    {
        $iUid = $this->iUserId;
        $oUser = User::find($iUid);
        if (!$oUser) {
            echo $this->formatRet(false, '用户不存在');
            exit;
        }
        $iRoleId = Input::get('role_id');
        $sClient = Input::get('_client');
        $iIosCheck = Config::get('config.ios_check');
        if (!in_array($iRoleId, array(2, 3))) {
            //取当前用户的role_id
            $iRoleId = $oUser->role_id;
        }
        //获取当前用户积分排行
        $iUserRank = User::getUserRankById($oUser->role_id, $this->iUserId, 'integral');
        //获取前一百名用户的积分排行
        $aUser = User::getUserScoreList($iRoleId, 100, $this->iUserId, 'integral');
        //数据处理
        $aArr = array();
        foreach ($aUser as $score) {
            // ios 审核期间用户姓名星号标识
            if ($sClient == 'ios' && $iIosCheck) {
                $score['user_name'] = substr_cut($score['user_name']);
            }
            $aArr[] = array(
                'role_id' => $score['role_id'],
                'user_nick' => $score['user_name'],
                'user_id' => $score['id'],
                'live_min' => $score['health_live_integral'],
                'review_min' => $score['health_video_integral'],
                'user_score' => $score['health_integrals']
            );
        }
        echo $this->formatRet(true, '', array('user_rank' => $iUserRank), $aArr);
        exit;
    }

    /**
     * 获取系统基础设置信息
     **/
    public function postSysinfo()
    {
        $Configs = new Configs();
        $configRs = $Configs::find(1);
        //我的当前积分
        $health_integral = User::where('id', $this->iUserId)->pluck('health_integral');
        $data_info = array(
            'health_integral' => $health_integral,
            'video_integral' => $configRs->cmc_use_integral,
            'live_integral' => $configRs->live_integral,
            'view_video_deduct_integral' => $configRs->view_video_deduct_integral,
            'cmc_use_integral' => $configRs->cmc_use_integral,
            'integral_tel' => $configRs->integral_tel,
            'exchange_rule_integral' => $configRs->exchange_rule_integral,
            'teach_agreement' => $configRs->teach_agreement,
            'view_teacher_video_agreement' => $configRs->view_teacher_video_agreement,
            'doc_agreement' => $configRs->doc_agreement,
            'kypx_use_integral' => $configRs->kypx_use_integral
        );
        $notice_id = intval(Input::get('notice_id', 0));
        if ($notice_id) {
            $noticeInfo = AspirinUserNotice::find($notice_id)->toArray();
            switch ($noticeInfo['notice_type']) {
                case 150:
                    if ($noticeInfo['agreement_txt']) {
                        $data_info['view_teacher_video_agreement'] = $noticeInfo['agreement_txt'];
                    }
                    if (!empty($noticeInfo['agreement_img'])) {
                        $data_info['view_teacher_video_agreement_img'] = url($noticeInfo['agreement_img']);
                    }
                    break;
                case 160:
                    if ($noticeInfo['agreement_txt']) {
                        $data_info['teach_agreement'] = $noticeInfo['agreement_txt'];
                    }
                    break;
            }
        }
        // 处理部分 android 端 notice_id 没传情况下的协议不正确的 bug
        else {
            // 获取当前用户的基础与银行信息
            $oUser = User::where('id', $this->iUserId)->first();
            $oUserBankAccount = DB::table('view_user_bank_account')->where('user_id', $this->iUserId)->first();
            // 重新生成协议金额
            if ($oUser->user_fee == 0) {
                $oUser->user_fee = 800;
            }
            // 获取医院信息
            if($oUser->user_company != 0 && is_numeric($oUser->user_company)){
                $oUser->user_address = Hospital::where('id', $oUser->user_company)->pluck('name');
            }else{
                $oUser->user_address = $oUser->user_company_name;
            }
            $find = array("{#user_name#}","{#idcode#}","{#address#}","{#telephone#}","{#fee#}","{#bank_code#}","{#bank_address#}","{#date#}");
            $replace = array($oUser->user_name, $oUserBankAccount->bank_idcard_code, $oUser->user_address, $oUser->user_tel, $oUser->user_fee, $oUserBankAccount->bank_account, $oUserBankAccount->bank_name, date('Y/m/d'));
            $data_info['view_teacher_video_agreement'] = str_replace($find, $replace, $configRs->view_teacher_video_agreement);
        }

        echo $this->formatRet(true, '', $data_info);
        exit;
    }

    /**
     * 用户健康云积分增减操作
     **/
    public function getIntegraldo()
    {
        //获取系统基础设置信息
        /*
		$Configs = new Configs();
		$configSys = $Configs::find(1);
		//积分日志初始化
		$IntegralLog = new IntegralLog();
		//获取用户积分相关信息
		$iUid = $this->iUserId;
		$oUser = User::select("health_integral","health_live_integral","health_video_integral")->find( $iUid );
		if( !$oUser ){
			echo $this->formatRet(false, '用户不存在');
			exit;
		}
		*/
        //obj_id=705&obj_type=video&types=in
        $oInput = Input::all();
        if (!Input::has('token') || !Input::has('obj_id') || !Input::has('obj_type')) {
            echo $this->formatRet(false, "传递参数不正确");
            exit;
        }
        $IntegralLog = new IntegralLog();
        if (empty($oInput['obj_id'])) {
            $oInput['obj_id'] = (int)IntegralLog::where('obj_type', 'buycme')->where('types', 'out')->max('obj_id') + 1;
        }
        $res = $IntegralLog->updateIntegral($this->iUserId, $oInput['obj_id'], $oInput['obj_type']);
        if ($res['isok']) {
            echo $this->formatRet(true, "恭喜您，操作成功！", null, null, ['obj_id' => $oInput['obj_id']]);
            exit;
        } else {
            echo $this->formatRet(false, $res['msg'] ? $res['msg'] : "对不起，操作失败。");
            exit;
        }
        /*
		$uArr = array(
			'user_id'	=>	$iUid,
			'obj_id'	=>	$oInput['obj_id'],
			'obj_type'	=>	$oInput['obj_type'],
			'balance'	=>	$oUser->health_integral,
			'ctime'		=>	time()
		);
		$integralArr = array();
		$types = '';
		// $log = DB::getQueryLog();
		// print_r($log);die;
		switch ($oInput['obj_type']) {
			case 'video': //观看精品课程扣除
				$uArr['number'] = $configSys->view_video_deduct_integral;
				$uArr['types'] = $types = "out";
				$uArr['obj_title'] = FaceVideo::where('id',$oInput['obj_id'])->pluck('video_title');
				$integralArr = array(
					'health_integral' => $oUser->health_integral-$configSys->view_video_deduct_integral,
				);
				break;
			case 'class': //参会赠送
				$uArr['number'] = $configSys->live_integral;
				$uArr['types'] = $types = "in";
				$uArr['obj_title'] = "参会赠送"; //FaceVideo::where('id',$oInput['obj_id'])->pluck('video_title');
				$integralArr = array(
					'health_integral' 		=> $oUser->health_integral+$configSys->live_integral,
					'health_live_integral'	=> $oUser->health_live_integral+$configSys->live_integral
				);
				break;
			case 'view': //观看健康教育视频赠送
				$uArr['number'] = $configSys->video_integral;
				$uArr['types'] = $types = "in";
				$uArr['obj_title'] = AspirinEducation::where('id',$oInput['obj_id'])->pluck('ppt_title');
				$integralArr = array(
					'health_integral' 		=> $oUser->health_integral+$configSys->video_integral,
					'health_video_integral'	=> $oUser->health_video_integral+$configSys->video_integral
				);
				break;
			case 'buycme': //兑换好医生CME课程消耗
				$uArr['number'] = $configSys->cmc_use_integral;
				$uArr['obj_title'] = $obj_title = "购买好医生CME课程。";
				$uArr['types'] = $types = "out";
				$integralArr = array(
					'health_integral' => $oUser->health_integral-$configSys->cmc_use_integral,
				);
				break;
		}
		if($integralArr['health_integral']<0){
			echo  $this->formatRet(false,"对不起，您的积分余额不足。");
			exit;
		}
		$oUser->id = $iUid;
		foreach($integralArr as $k=>$v){
			$oUser->$k = $v;
		}
		if($oUser->save()){
			$IntegralLog->AddItem($uArr)->save();
			echo  $this->formatRet(true,"恭喜您，操作成功！");
			exit;
		}else{
			echo  $this->formatRet(false,"对不起，操作失败。");
			exit;
		}
		*/
    }

    /**
     * 获取积分详情
     * @return mixed
     * [get] /apiuser/integral-list?obj_type=&page=1&pagesize=10&token=
     */
    public function getIntegralList()
    {
        $obj_type = strtolower(Input::get('obj_type', ''));
        $types = strtolower(Input::get('types', 'out'));
        $iPage = Input::get('page', 1);
        $iPagesize = Input::get('pagesize', self::PAGESIZE);

        // 获取积分列表
        $list = IntegralLog::where(function ($query) use ($obj_type, $types) {
            if (!empty($obj_type)) {
                $query->where('obj_type', $obj_type);
            }
            if (!empty($types)) {
                $query->where('types', $types);
            }
        })->where('user_id', $this->iUserId)
            ->orderBy("created_at", "desc")
            ->skip(($iPage - 1) * $iPagesize)
            ->take($iPagesize)
            ->get();

        $alist = [];
        // 获取审核状态
        foreach ($list as $oIntegeral) {
            $item = $oIntegeral->toArray();
            $item['created_at'] = $oIntegeral->created_at ? $oIntegeral->created_at->format('Y/m/d') : null;
            $item['updated_at'] = $oIntegeral->updated_at ? $oIntegeral->updated_at->format('Y/m/d') : null;
            if ($obj_type == 'buycme') {
                $info = CmeInfo::where('obj_id', $oIntegeral->obj_id)->where('user_id', $this->iUserId)->first();
                $item['status'] = $info->status ?: 0;
                $item['check_time'] = $info->status ? $info->updated_at->format('Y/m/d') : null;
            } else {
                $item['status'] = 1;
                $item['check_time'] = $item['updated_at'];
            }
            $item['number'] = (string)$item['number'];
            $alist[] = $item;
        }

        return Response::make(['status' => true, 'msg' => 'ok', 'data' => $alist]);
    }

    //我的医生 role_id=2:代表身份   role_id=3:医生  card_auth_flag
    public function getMydoclist()
    {
        $iUid = $this->iUserId;
        $oUser = User::find($iUid);
        if ($oUser->role_id != 2) {
            echo $this->formatRet(false, "对不起，您无权查看。");
            exit;
        }
        $page = Input::get('page');
        $pagesize = Input::get('pagesize');
        $pagesize = $pagesize ? $pagesize : 10;
        $skip = ($page - 1) * $pagesize;
        $docList = User::select('id', 'link_rep_id', 'user_name', 'user_department', 'user_position', 'user_company',
            'health_integral', 'user_thumb')
            ->where('link_rep_id', $iUid)//link_user_id
            ->where('card_auth_flag', "!=", 2)
            ->orderBy('health_integral', 'DESC')
            ->skip($skip)->take($pagesize)->get()->toArray();
        $aArr = array();
        if ($docList) {
            foreach ($docList as $row) {
                $domains = "http://cdcma.bizconf.cn"; //"http://{$_SERVER['HTTP_HOST']}"
                if ((int)$row['user_company']) {
                    $sUserCompany = Hospital::where('id', (int)$row['user_company'])->pluck('name');
                } else {
                    $sUserCompany = $row['user_company_name'] ? $row['user_company_name'] : "--";
                }
                $aArr[] = array(
                    'id' => $row['id'],
                    'link_user_id' => $row['link_rep_id'],
                    'user_name' => $row['user_name'],
                    'user_department' => $row['user_department'],
                    'user_position' => $row['user_position'] ? $row['user_position'] : "--",
                    'user_company' => $sUserCompany,
                    'health_integral' => $row['health_integral'],
                    'user_thumb' => $row['user_thumb'] ? "{$domains}" . $row['user_thumb'] : "{$domains}/assets/images/dafault/default.jpg"
                );
            }
        }
        echo $this->formatRet(true, '', '', '', array('data' => array('list' => $aArr)));
        exit;
    }

    //医生观看视频记录日志
    public function getDocviewvideolog()
    {
        $VideoViewLog = new VideoViewLog();
        $iUid = $this->iUserId;
        $oUser = User::find($iUid);
        if ($oUser->role_id != 2) {
            echo $this->formatRet(false, "对不起，您无权查看。");
            exit;
        }
        $doc_id = Input::get('doc_id');
        $page = Input::get('page');
        $pagesize = Input::get('pagesize');
        $pagesize = $pagesize ? $pagesize : 10;
        $skip = ($page - 1) * $pagesize;
        $logList = VideoViewLog::where('user_id', $doc_id)
            ->groupBy('video_id')
            ->orderBy('created_at', 'DESC')
            ->skip($skip)->take($pagesize)->get();
        $aArr = array();
        if ($logList) {
            foreach ($logList as $row) {
                $domains = "http://cdcma.bizconf.cn";
                $nowdomains = "http://{$_SERVER['HTTP_HOST']}";
                $photo = $row->video_thumb ?
                    str_replace($nowdomains, $domains, $row->video_thumb) :
                    "{$nowdomains}/assets/images/dafault/default.jpg";
                $aArr[] = array(
                    'id' => $row->id,
                    'user_id' => $row->user_id,
                    'video_title' => $row->video_title,
                    'video_thumb' => $photo,
                    'created_at' => $row->created_at
                );
            }
        }
        echo $this->formatRet(true, '', '', '', array('data' => array('list' => $aArr)));
        exit;
    }

    //我的讲者列表 card_auth_flag
    public function getMyauthdoclist()
    {
        $iUid = $this->iUserId;
        $oUser = User::find($iUid);
        if ($oUser->role_id != 2) {
            echo $this->formatRet(false, "对不起，您无权查看。");
            exit;
        }
        $page = Input::get('page');
        $pagesize = Input::get('pagesize');
        $pagesize = $pagesize ? $pagesize : 10;
        $skip = ($page - 1) * $pagesize;
        //link_user_id
        $docList = User::select('id', 'link_rep_id', 'user_name', 'user_department', 'user_position', 'user_company',
            'health_integral', 'user_thumb')
            ->where('link_rep_id', $iUid)
            ->where('card_auth_flag', 2)
            ->orderBy('health_integral', 'DESC')
            ->skip($skip)->take($pagesize)->get()->toArray();
        $aArr = array();
        if ($docList) {
            foreach ($docList as $row) {
                $domains = "http://cdcma.bizconf.cn"; //"http://{$_SERVER['HTTP_HOST']}"
                if ((int)$row['user_company']) {
                    $sUserCompany = Hospital::where('id', (int)$row['user_company'])->pluck('name');
                } else {
                    $sUserCompany = $row['user_company_name'] ? $row['user_company_name'] : "--";
                }
                $aArr[] = array(
                    'id' => $row['id'],
                    'link_user_id' => $row['link_rep_id'],
                    'user_name' => $row['user_name'],
                    'user_department' => $row['user_department'],
                    'user_position' => $row['user_position'],
                    'user_company' => $sUserCompany,
                    'health_integral' => $row['health_integral'],
                    'user_thumb' => $row['user_thumb'] ? "{$domains}" . $row['user_thumb'] : "{$domains}/assets/images/dafault/default.jpg"
                );
            }
        }
        echo $this->formatRet(true, '', '', '', array('data' => array('list' => $aArr)));
        exit;
    }

    //讲师发布课程记录
    public function getAuthdocclass()
    {
        global $iUserId;
        $iUid = $iUserId;
        $oUser = User::find($iUid);
        if ($oUser->role_id != 2) {
            echo $this->formatRet(false, "对不起，您无权查看。");
            exit;
        }
        $doc_id = Input::get('doc_id');
        $page = Input::get('page');
        $pagesize = Input::get('pagesize');
        $pagesize = $pagesize ? $pagesize : 10;
        $skip = ($page - 1) * $pagesize;
        $oEducation = AspirinEducation::where('video_flag', 0)
            ->where('user_id', $doc_id)
            ->where('education_type', 1)
            ->orderBy('created_at', 'desc')
            ->skip($skip)->take($pagesize)->get();
        $aArr = array();
        foreach ($oEducation as $v) {
            $domains = "http://cdcma.bizconf.cn"; //"http://{$_SERVER['HTTP_HOST']}";
            $photo = $v['ppt_thumb'] ? $domains . $v['ppt_thumb'] : "/assets/images/dafault/default.jpg";
            $aArr[] = array(
                'id' => $v->id,
                'user_id' => $iUid,
                'video_title' => $v->ppt_title,
                'video_thumb' => $photo,
                'created_at' => $v->created_at,
                'gourl' => "http://{$_SERVER['HTTP_HOST']}/aspirinshare/authdocclassshare?doc_id={$doc_id}&video_id={$v->id}",
                'share' => array(
                    'share_title' => $v->ppt_title,
                    'share_url' => "http://{$_SERVER['HTTP_HOST']}/aspirinshare/authdocclassshare?doc_id={$doc_id}&video_id={$v->id}",
                    'share_thumb' => Config::get('app.url') . $v->ppt_thumb,
                    'share_des' => $v->education_content ? $v->education_content : $v->ppt_title
                )
            );
        }
        echo $this->formatRet(true, '', '', '', array('data' => array('list' => $aArr)));
        exit;
    }

    //协议签名操作接口
    public function postSignagree()
    {
        $oUser = User::find($this->iUserId);
        if (!$oUser) {
            echo $this->formatRet(false, '用户不存在');
            exit;
        }
        $input = Input::all();
        $notice_type = $input['notice_type'];
        $detail_id = $input['detail_id'];
        //上传签名图
        $signfile = Input::file('sign_icon');
        $signicon = $this->dealAvatar($signfile);
        switch ($notice_type) {
            case 150: //健康教育视频
                $oEducation = AspirinEducation::find($detail_id);
                if (@$oEducation->is_signed) {
                    echo $this->formatRet(false, "您已签名，请不要重复操作");
                    exit;
                }
                $oEducation->is_signed = 1;
                $oEducation->sign_time = time();
                //签名文件更新
                $userNotice =  AspirinUserNotice::where('notice_type', 150)->where('detail_id',$detail_id)->first();
                if (empty($userNotice)) {
                    echo $this->formatRet(false, "未匹配到通知信息");
                    exit;
                }
                $agreement_file = $userNotice->agreement_img;
                // 如果通知表里协议图片为空则表示是旧协议格式
                if (empty($agreement_file)) {
                    $oEducation->sign_icon = $signicon;
                } else {
                    $oEducation->sign_icon = $this->signFile($signicon, $agreement_file);
                    if (false === $oEducation->sign_icon) {
                        echo $this->formatRet(false, "对不起，文件生成失败。");
                        exit;
                    }
                }
                $rs = $oEducation->save();
                break;
            case 160: //讲师讲课协议
                $online_id = AspirinOnlineTime::where('id', $detail_id)->pluck("online_id");
                $oInfos = AspirinOnlineSpeaker::where("online_id", $online_id)->where("online_time_id", $detail_id)->first();
                if (@$oInfos->is_signed) {
                    echo $this->formatRet(false, "您已签名，请不要重复操作");
                    exit;
                }
                $oInfos->is_signed = 1;
                $oInfos->sign_time = time();
                $oInfos->sign_icon = $signicon;
                $rs = $oInfos->save();
                break;
        }
        if ($rs) {
            echo $this->formatRet(true, "恭喜您，操作成功！");
            exit;
        } else {
            echo $this->formatRet(false, "对不起，操作失败。");
            exit;
        }
    }

    /**
     * 获取我的医生列表
     * http://cdcma.local.org/apiuser/doctors?page=1&pagesize=10&name=&mobile=&hospital=&token=
     */
    public function getDoctors()
    {
        $name = Input::get('name');
        $mobile = Input::get('mobile');
        $hospital = Input::get('hospital');
        $iPage = Input::get('page', 1);
        $iPagesize = Input::get('pagesize', self::PAGESIZE);

        $list = User::where('user.role_id', 3)
            ->select([
                'user.*',
                'deputy.user_nick as deputy_nick',
                'deputy.user_name as deputy_name',
                'deputy.user_thumb as deputy_thumb',
                'deputy.user_department as deputy_department',
                'deputy.user_position as deputy_position',
                'hospital.id as hospital_id',
                'hospital.name as hospital_name'
            ])
            ->leftJoin('user as deputy', 'deputy.id', '=', 'user.link_rep_id')
            ->leftJoin('hospital', 'user.user_company', '=', 'hospital.id')
            ->where(function ($query) use ($name, $mobile, $hospital) {
                $sqlarr = [];
                if (!empty($name)) {
                    $sqlarr[] = "user.user_name like '%{$name}%'";
                }
                if (!empty($mobile)) {
                    $sqlarr[] = "user.user_tel like '%{$mobile}%'";
                    $sqlarr[] = "user.user_mobile like '%{$mobile}%'";
                }
                if (!empty($hospital)) {
                    $sqlarr[] = "hospital.name like '%{$hospital}%'";
                    $sqlarr[] = "user.user_company_name like '%{$hospital}%'";
                }
                if (!empty($sqlarr)) {
                    $query->whereRaw(join(' OR ', $sqlarr));
                }
            })
            ->orderBy("user.updated_at", "desc")
            ->skip(($iPage - 1) * $iPagesize)
            ->take($iPagesize)
            ->get();

        $alist = [];
        foreach ($list as $oUser) {
            //用户是否是副高及以上职称标志
            $iPositionFlag = 0;
            if ($oUser->role_id == 2) {
                $iPositionFlag = 1;
            } elseif ($oUser->role_id == 3) {
                /**
                 * '1'    => '住院医师',
                 * '2' => '主治医师',
                 * '3' => '副主任医师',
                 * '4' => '主任医师',
                 * '5' => '其它' */
                $aPos = User::getPosition();
                if ($oUser->user_position == $aPos[2] || $oUser->user_position == $aPos[3] || $oUser->user_position == $aPos[4]) {
                    $iPositionFlag = 2;
                }
            }
            $alist[] = array(
                'user_id' => $oUser->id,
                'role_id' => $oUser->role_id,
                'user_nick' => $oUser->user_nick,
                'user_name' => $oUser->user_name,
                'user_sex' => $oUser->user_sex,
                'user_tel' => $oUser->user_tel,
                'user_email' => $oUser->user_email,
                'user_address' => $oUser->user_address,
                'user_city' => $oUser->user_city,
                'user_department' => $oUser->user_department,
                'user_position' => $oUser->user_position,
                'user_company' => $oUser->user_company,
                'user_thumb' => $oUser->user_thumb ? Config::get('app.url') . $oUser->user_thumb . "?t=" . time() : Config::get('app.url') . '/assets/images/dafault/default.jpg?t=' . time(),
                'card_auth_flag' => $oUser->card_auth_flag,
                'position_flag' => $iPositionFlag,
                'follow_me' => $this->iUserId == $oUser->user_id ? 1 : 0,
                'deputy_nick' => $oUser->deputy_nick,
                'deputy_name' => $oUser->deputy_name,
                'deputy_thumb' => $oUser->deputy_thumb ? Config::get('app.url') . $oUser->deputy_thumb . "?t=" . time() : Config::get('app.url') . '/assets/images/dafault/default.jpg?t=' . time(),
                'deputy_department' => $oUser->deputy_name,
                'deputy_position' => $oUser->deputy_name,
                'hospital_id' => (string)$oUser->hospital_id,
                'hospital_name' => $oUser->hospital_name ?: $oUser->user_company_name
            );
        }

        echo $this->formatRet(true, "成功", "success", $alist);
        exit;
    }

    /**
     * 生成协议文件（带签名）
     * @param $sign_file
     * @param $origin_file
     * @return bool
     */
    private function signFile($sign_file,$origin_file){
        $image_1 = imagecreatefromjpeg(public_path().$origin_file);
        $image_2 = imagecreatefromjpeg(public_path().$sign_file);
        imagecopymerge($image_1, $image_2, 1650, 5570, 0, 0, imagesx($image_2), imagesy($image_2), 100);

        $flag = imagejpeg($image_1,public_path() . $origin_file);
        return $flag ? $origin_file : false;
    }

    /**
     * 处理 ios 审核时邀请码问题
     * @return mixed
     */
    public function getCheckInviteCode()
    {
        $sClient = Input::get('_client');
        $sVersion = Input::get('_version');
        $iIosCheck = Config::get('config.ios_check');
        $iIosVersion = Config::get('config.ios_version');

        $intviteCode = '';

        // 检查是否满足审核条件
        if ($iIosVersion == $sVersion && $iIosCheck && $sClient == 'ios') {
            // ios审核时默认医药代表帐号的邀请码
            $intviteCode = '8XLFZJ';
        }

        return Response::make(['status' => true, 'msg' => 'ok', 'intvite_code' => $intviteCode]);
    }
}
