<?php
/**
 * +----------------------------------------------------------------------
 * | 国卫健康云（cdcma）
 * +----------------------------------------------------------------------
 *  .--,       .--,             | FILE: ApiMyController.php
 * ( (  \.---./  ) )            | AUTHOR: byron sampson
 *  '.__/o   o\__.'             | EMAIL: xiaobo.sun@qq.com
 *     {=  ^  =}                | QQ: 150093589
 *      >  -  <                 | WECHAT: wx5ini99
 *     /       \                | DATETIME: 2018/8/20
 *    //       \\               |
 *   //|   .   |\\              |
 *   "'\       /'"_.-~^`'-.     |
 *      \  _  /--'         `    |
 *    ___)( )(___               |-----------------------------------------
 *   (((__) (__)))              | 高山仰止,景行行止.虽不能至,心向往之。
 * +----------------------------------------------------------------------
 * | Copyright (c) 2017 http://www.zzstudio.net All rights reserved.
 * +----------------------------------------------------------------------
 */

use Illuminate\Support\Facades\Input;

class ApiDocController extends BaseController
{
    /**
     * 获取接口md 内容
     * @return mixed
     */
    public function getApi()
    {
        $content = file_get_contents(ROOT . DS . 'API.md');
        $list = $content;
        return Response::make(['status' => true, 'msg' => 'ok', 'data' => $list]);
    }

    /**
     * 空方法
     * @param array $parameters
     */
    public function missingMethod($parameters = array())
    {
        //
    }
}