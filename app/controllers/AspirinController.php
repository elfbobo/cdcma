<?php

//pc端专项基金

class AspirinController extends BaseController {

	const PAGESIZE = 8;
	private $iUserId = 0;
	private $iRoleId = 0;

	public function __construct()
	{
		$this->beforeFilter('aspirinpcauth',array('except' =>array('getContact')));
		$this->iUserId = Session::get('userid');
		$this->iRoleId = Session::get('roleid');
	}

	//首页--简介
	public function getIndex()
	{
		return View::make('front.aspirin.index');
	}

	//阿司匹林120周年--列表（pc版不分group）
	public function getInfoList()
	{
		$oInfo = AspirinInfo::orderby('created_at','desc')->paginate(self::PAGESIZE);
		return View::make('front.aspirin.info.list')->with('oInfo',$oInfo);
	}

	//阿司匹林120周年--详情
	public function getInfoShow($iId)
	{
		$oInfo = AspirinInfo::find($iId);
		if(count($oInfo)){
			$oInfo->increment('info_hits');
		}
		return View::make('front.aspirin.info.show')->with('oInfo',$oInfo);
	}

	//上传医师证照
	public function postUploadUserCard()
	{
		if($_FILES['file-pic']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['file-pic']['name'];
			$attach_fileext = get_filetype($attach_filename);
			$rand_name = date('YmdHis', time()).rand(1000,9999);
			$sFileName = $rand_name.'.'.$attach_fileext;
			$sPath = "/upload/user/$attach_fileext/".date('Ymd',time());
			$sRealPath = public_path().$sPath;
			mkdirs($sRealPath);
			// $attach_filesize = filesize($_FILES['file-pic']['tmp_name']);
			move_uploaded_file($_FILES['file-pic']['tmp_name'], $sRealPath.DS.$sFileName);
			$sFileNameS = $rand_name . '_s.' . $attach_fileext;
			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 1000, 1000 );
			$sFileUrl = $sPath.'/'.$sFileNameS;
			$json = array('card_thumb'=>$sFileUrl);
			echo json_encode($json);
			die;
		}
	}

	//医生认证提交
	public function postSubmitAuth()
	{
		$oUser = User::find($this->iUserId);
		if($oUser){
			$oUser->user_position = trim(Input::get('user_position'));
			$oUser->card_number = trim(Input::get('card_number'));
			$oUser->card_thumb = trim(Input::get('card_thumb'));
			$oUser->card_auth_flag = 1;
			$oUser->save();
			return json_encode(array('success'=>true));
		}else{
			return json_encode(array('success'=>false));
		}
	}

    //个人中心--联系客服
	public function getContact(){
		return View::make('front.user.contact');
	}

	//个人中心--我的认证
	public function getMyAuth(){
		$oUser = User::find($this->iUserId);
		if($oUser->user_company != 0 && is_numeric($oUser->user_company)){
			$sHospitalName = Hospital::where('id',$oUser->user_company)->pluck('name');
		}else{
			$sHospitalName = $oUser->user_company_name;
		}
		return View::make('front.user.myauth')->with('sHospitalName',$sHospitalName)->with('oUser',$oUser);
	}
	
}