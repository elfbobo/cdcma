<?php

class MobileDocFaceController extends BaseController {
	
	CONST  PAGESIZE = 10;
	CONST  CATID = 1;
    private $iUserId = 0;
    private $iRoleId = 0;
	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	public function __construct(){
		$this->token = '';
		if(Input::has('token')){
			$this->token = $token = Input::get('token');
			$token = Crypt::decrypt($token);
			$aUserinfo = explode('\t',$token);
			$this->iUserId = $aUserinfo[0];
			$this->iRoleId = 2;
			Session::put('roleid',2);//用户角色
			Session::put('userid',$aUserinfo[0]);//用户id
		}else{
			$this->beforeFilter('mobileauth');
			$this->iUserId = Session::get('userid');
			$this->iRoleId = Session::get('roleid');
		}		
	}

	public function getIndex(){
		//取名医面对面精彩预告最新的一条
		$oFaceVideoAdvance = FaceVideo::getLatestFaceVideoAdvance();
		if($oFaceVideoAdvance){
			$oFaceVideoAdvance1 = FaceVideo::getLatestFaceVideoAdvance($oFaceVideoAdvance->id);
			if(!$oFaceVideoAdvance1){
				$oFaceVideoAdvance1 = $oFaceVideoAdvance;
			}
		}else{
			$oFaceVideoReview = FaceVideo::getLatestFaceVideoReview(1); 
			$oFaceVideoAdvance1='';
		}
		//精彩回顾
     	$oFaceVideoReview = FaceVideo::getLatestFaceVideoReview(self::PAGESIZE);
		return View::make('mobile.docface.index')
			->with('oFaceVideoAdvance',$oFaceVideoAdvance)
			->with('oFaceVideoReview',$oFaceVideoReview)
			->with('oFaceVideoAdvance1',$oFaceVideoAdvance1);
	}

	public function getLiveScore(){
		$iVideoId = Input::get('videoid',0);
		$oVideo = FaceVideo::find($iVideoId);
		if(!$oVideo){
			echo 'error';die;
		}
		$iScore1 = Input::get('score1',0);
		$iScore2 = Input::get('score2',0);
		$iScore3 = Input::get('score3',0);
		$iScore4 = Input::get('score4',0);
		$iScore5 = Input::get('score5',0);
		$iScore6 = Input::get('score6',0);
		$iDevice = Input::get('device',0);
		FaceLiveScore::addScoreLog($iVideoId,Input::get('userid'),$iScore1,$iScore2,$iScore3,$iScore4,$iScore5,$iScore6,$iDevice);
		echo 'success';die;
	}

	/**
	 * 获取精彩回顾列表分页
	 */
	public function postVideoPage(){
		$iDepartmentId = Input::get('department_id',0);
		$iPage = Input::get('page',1)+1;
		if(!$this->iUserId || $this->iRoleId!=2){
			$oVideoReview = FaceVideo::select('face_video.*','face_doc.doc_name','face_doc.doc_thumb','face_doc.doc_position','face_doc.doc_department','face_doc.doc_hospital','face_doc.doc_introduction')
				->leftJoin('face_doc','face_video.doc_id','=','face_doc.id')
				->where('video_type',1)->where('video_permission','0');
		}else{
			$oVideoReview = FaceVideo::select('face_video.*','face_doc.doc_name','face_doc.doc_thumb','face_doc.doc_position','face_doc.doc_department','face_doc.doc_hospital','face_doc.doc_introduction')
				->leftJoin('face_doc','face_video.doc_id','=','face_doc.id')
				->where('video_type',1);
		}
		if($iDepartmentId){
			$oVideoReview = $oVideoReview->where('department_id','LIKE',"%".$iDepartmentId."|%");
		}
		$oVideoReview = $oVideoReview->OrderByCreatedAt()->skip(self::PAGESIZE*($iPage-1))->take(self::PAGESIZE)->get();
		$sHtml = '';
		foreach($oVideoReview as $v){
			$sHtml .= '<a class="article_link" href="/mobile-docface/review-show/'.$v->id.'">';
			$sHtml .= '<div class="article">';
			$sHtml .= '<div class="dot"></div>';
			$sHtml .= '<div class="article_cont">';
			$sHtml .= '<div class="art_tit">';
			$sHtml .= '<span>主题：</span>'.str_cut_cms($v->video_title,100);
			$sHtml .= '</div>';
			$sHtml .= '<div class="art_txt">';
			$sHtml .= '<div class="art_author">';
			$sHtml .= $v->doc_name.' '.$v->doc_hospital;
			$sHtml .= '</div>';
			$sHtml .= '<div class="art_time">';
			$sHtml .= substr($v->start_time,0,16).'-'.substr($v->end_time,11,5);
			$sHtml .= '</div>';
			$sHtml .= '</div>';
			$sHtml .= '</div>';
			$sHtml .= '</div>';
			$sHtml .= '</a>';
		}
		$iNextPageFlag = 0;
		if(count($oVideoReview)==self::PAGESIZE){
			$iNextPageFlag = 1;
		}
		return json_encode(array('next_page_flag'=>$iNextPageFlag,'html'=>$sHtml));
	}
	
	/**
	 * 进入直播页面
	 * @param unknown $iId
	 */
	public function getEnterLive($iId){
		//观看直播时间存入数据库
		$oFaceVideo = FaceVideo::find($iId);
		if($oFaceVideo->video_permission==1 && $this->iRoleId!=2){
			return Redirect::to('/');
		}
		//点击量+1
        if ($oFaceVideo->video_hits) {
            $oFaceVideo->increment('video_hits');
        }
		$iEndTime = strtotime($oFaceVideo->end_time);
		$iStartTime = strtotime($oFaceVideo->start_time);
		$iNowTime = time();
		if(!($iEndTime>$iNowTime&&$iNowTime>($iStartTime-20*60))){		//如果不是直播正在进行中
			return Redirect::to('/');
		}
		//数据库该直播该用户已经观看的时间存数据库、
		$oVideoLog = VideoLog::HasLog(self::CATID, $iId, 2, $this->iUserId, 0);
		$sUserName = User::where('id',$this->iUserId)->pluck('user_name');
		$sUserthumb = User::where('id',$this->iUserId)->pluck('user_thumb');
		return View::make('mobile.docface.liveshow')
			->with('sUserName',$sUserName)
			->with('sUserthumb',$sUserthumb)
			->with('iUserId',$this->iUserId)
			->with('token',$this->token)
			->with('oFaceVideo',$oFaceVideo);
	}
	
	/**
	 * 精彩回顾详细
	 * @param unknown $iId
	 */
	public function getReviewShow($iId){
		$oReview = FaceVideo::getFaceVideoReviewById($iId);
		//点击量+1
		$oReview->increment('video_hits');
		//获取星级评分
		$oScoreLog = FaceVideoScore::where('face_video_id',$iId)->where('user_id',$this->iUserId)->first();
		//获取所有评论
		$iCatId = 1;
		$oComments = Comment::getComment($iCatId,$iId);
		$sImgLi = Images();
		return View::make('mobile.docface.reviewshow')
			->with('iUserId',$this->iUserId)
			->with('oReview',$oReview)
			->with('oScoreLog',$oScoreLog)
			->with('oComments',$oComments)
			->with('sImgLi',$sImgLi);
	}
	
	/**
	 * 精彩回顾评论
	 * @param unknown $iId
	 */
	public function getReviewComment($iId){
		//获取所有评论
		$iCatId = 1;
		$oComments = Comment::getComment($iCatId,$iId);
		return View::make('mobile.docface.reviewcomment')
			->with('iUserId',$this->iUserId)
			->with('iReviewId',$iId)
			->with('oComments',$oComments);
	}
	
	/**
	 * 专家详细
	 * @param unknown $iId
	 */
	public function getDocShow($iId){
		$type = 0;
		if(Input::has('token')){
			$type = 1;
		}
		$oDoc = FaceDoc::find($iId);
		return View::make('mobile.docface.docshow')->with('oDoc',$oDoc)->with('type',$type);
	}
	
}