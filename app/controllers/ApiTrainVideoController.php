<?php

/**
 * +----------------------------------------------------------------------
 * | cdcma [ 国卫健康云 ]
 * +----------------------------------------------------------------------
 *  .--,       .--,             | FILE: ApiTrainVideoController.php
 * ( (  \.---./  ) )            | AUTHOR: byron sampson
 *  '.__/o   o\__.'             | EMAIL: xiaobo.sun@qq.com
 *     {=  ^  =}                | QQ: 150093589
 *      >  -  <                 | WECHAT: wx5ini99
 *     /       \                | DATETIME: 2018/9/17
 *    //       \\               |
 *   //|   .   |\\              |
 *   "'\       /'"_.-~^`'-.     |
 *      \  _  /--'         `    |
 *    ___)( )(___               |-----------------------------------------
 *   (((__) (__)))              | 高山仰止,景行行止.虽不能至,心向往之。
 * +----------------------------------------------------------------------
 * | Copyright (c) 2018 http://www.zzstudio.net All rights reserved.
 * +----------------------------------------------------------------------
 */
class ApiTrainVideoController extends BaseController
{
    /**
     * 视频列表
     * @return mixed
     * @url http://cdcma.xxx.net/apitrainvideo/list?token=
     */
    public function getList()
    {
        if (!Input::has('token')) {
            echo $this->formatRet(false, "传递参数不正确");
            exit;
        }
        $token = Input::get('token');
        $token = Crypt::decrypt($token);
        $aUserinfo = explode('\t', $token);
        $iUid = $aUserinfo[0];
        if ($iUid) {
            $oUser = User::find($iUid);
        }
        $page = Input::get('page', 1);
        $pagesize = Input::get('pagesize', 10);
        $pagesize = $pagesize ? $pagesize : 10;
        $skip = ($page - 1) * $pagesize;

        $oRecordshows = TrainVideo::select('train_video.*', 'face_doc.doc_name', 'face_doc.doc_thumb',
            'face_doc.doc_position', 'face_doc.doc_department', 'face_doc.doc_hospital', 'face_doc.doc_introduction')
            ->leftJoin('face_doc', 'train_video.doc_id', '=', 'face_doc.id');

        //科室检索
        $iDepartmentId = Input::get('departmentid', 0);
        if ($iDepartmentId) {
            $oRecordshows = $oRecordshows->whereRaw("find_in_set('$iDepartmentId', department_id)");
        }
        $oRecordshows = $oRecordshows->orderBy('created_at', 'DESC')->skip($skip)->take($pagesize)->get();

        $aRecordshows = array();
        foreach ($oRecordshows as $obj) {
            // $oDoc = FaceDoc::find($obj->doc_id);
            $aRecordshows[] = array(
                'id' => $obj->id,
                'video_title' => $obj->video_title,
                'video_thumb' => url($obj->video_thumb),
                'doc_name' => $obj->doc_name,
                'doc_hospital' => $obj->doc_hospital,
                'created_at' => $obj->created_at
            );
        }

        return Response::make(['success' => true, 'msg' => '成功', "data" => ['list' => $aRecordshows]]);
    }

    /**
     * 科研培训视频详情
     * http://cdcma.xxx.net/apitrainvideo/show?videoid=1&token=
     */
    public function getShow()
    {
        if (!Input::has('token') || !Input::has('videoid')) {
            echo $this->formatRet(false, "传递参数不正确");
            exit;
        }
        $token = Input::get('token');
        $token = Crypt::decrypt($token);
        $aUserinfo = explode('\t', $token);
        $iUid = $aUserinfo[0];
        $iVideoid = Input::get('videoid');
        $oVideo = TrainVideo::getFaceVideoReviewById($iVideoid);
        $oZanlog = TrainVideoZanLog::selzanlog($iVideoid)->get();
        $iZancount = count($oZanlog);
        $iszan = false;
        if ($iZancount > 0) {
            foreach ($oZanlog as $v) {
                if ($v->user_id == $aUserinfo[0]) {
                    $iszan = true;
                }
            }
        }
        if (count($oVideo)) {
            $oVideo->increment('video_hits');
            $oDoc = $oVideo->doc_info;

            $aVideo = array(
                'video_title' => $oVideo->video_title,
                'video_thumb' => url($oVideo->video_thumb),
                'video_url' => url($oVideo->video_url),
                'video_introduce' => $oVideo->video_introduce,
                'video_hits' => $oVideo->video_hits,
                'zancount' => $iZancount,
                'iszan' => $iszan,
                'doc_name' => $oDoc->doc_name,
                'doc_thumb' => url($oDoc->doc_thumb),
                'doc_position' => $oDoc->doc_position,
                'doc_department' => $oDoc->doc_department,
                'doc_hospital' => $oDoc->doc_hospital,
                'doc_introduction' => $oDoc->doc_introduction
            );
            //星级评分
            $aVideo['score'] = array();
            $oScoreLog = TrainVideoScore::where('train_video_id', $iVideoid)->where('user_id', $aUserinfo[0])->first();
            if (count($oScoreLog)) {
                $aVideo['score'] = array(
                    'score1' => $oScoreLog->content_degree,
                    'score2' => $oScoreLog->class_level,
                    'score3' => $oScoreLog->overall_score
                );
            }

            //评论列表
            $oComments = Comment::getComment(4, $iVideoid);
            $aComments = array();
            foreach ($oComments as $obj) {
                $aComments[] = array(
                    'comment_id' => $obj->id,
                    'user_nick' => $obj->user_nick,
                    'user_thumb' => url($obj->user_thumb),
                    'comment' => str_replace('/assets/images/front/emotion/',Config::get('config.rooturl').'/assets/images/front/emotion/',$obj->comment),
                    'zan_count' => $obj->zan_count,
                    'device_type' => $obj->device_type,
                    'created_at' => substr($obj->created_at, 0, 10)
                );
            }
            $aVideo['comments'] = $aComments;
            //加积分记录
            /*$creditIN = CreditController::getInstance($iVideoid, 0, $aUserinfo[0], 'ios');
            $aVideo['share_flag'] = 1;
            $aVideo['share_title'] = $oVideo->video_title;
            $aVideo['share_thumb'] = url('/assets/images/mobile/logo.png');
            $aVideo['share_url'] = url('/mobile-docface/review-show/' . $iVideoid);
            $creditIN->optCredit();*/
            // FaceVideo::addUserScoreAndViewLog($iVideoid,0,$aUserinfo[0]);//添加记录
            //精品课程弹窗是否显示
            $Configs = new Configs();
            $configRs = $Configs::find(1)->toArray();
            $aVideo['buyTips'] = 0;
            $aVideo['buyTipsTxt'] = "观看此视频需消耗{$configRs['kypx_use_integral']}积分，是否观看？";
            if ($configRs['kypx_use_integral']) {
                //获取积分日志列表
                $purchased = IntegralLog::where('user_id', $aUserinfo[0])->
                where('obj_type', '=', 'buykypx')->
                where('obj_id', '=', $iVideoid)->first();
                if (!$purchased) {
                    $aVideo['buyTips'] = 1;
                }
            }
            //插入健康教育视频观看日志
            /*$VideoViewLog = new VideoViewLog();
            $vVlog = array(
                'user_id' => $iUid,
                'video_id' => $iVideoid,
                'video_title' => $aVideo['video_title'],
                'video_thumb' => $aVideo['video_thumb']
            );
            $VideoViewLog->addItem($vVlog)->save();*/

            return Response::make(['success' => true, 'msg' => '成功', "datalist" => $aVideo]);
        } else {
            return Response::make(['success' => false, 'msg' => '视频不存在']);
        }
    }

    /**
     * 科研培训视频点赞
     * @return mixed
     * @url http://cdcma.xxx.net/apitrainvideo/surpport?videoid=1&token=
     */
    public function postSurpport()
    {
        $iVideoid = Input::get('videoid');
        $token = Input::get('token');
        $token = Crypt::decrypt($token);
        $aUserinfo = explode('\t', $token);
        $aUserinfo = explode('\t', $token);
        $iUid = $aUserinfo[0];
        if (TrainVideoZanLog::isUserZan($iVideoid, $iUid)) {
            //已赞
            return Response::make(['success' => false, 'msg' => '您已支持过该视频，无需重复支持！']);
        }
        $oZanlog = TrainVideoZanLog::UserZan($iVideoid, $iUid);

        return Response::make(['success' => true, 'msg' => '支持成功']);
    }

    /**
     * 录播视频星级评分   post提交
     * /apitrainvideo/score?videoid=1&score1=1&score2=1&score3=1&device=2&token=
     */
    public function postScore()
    {
        $iVideoId = Input::get('videoid', 0);
        $oVideo = TrainVideo::find($iVideoId);
        if (!$oVideo) {
            return Response::make(['success' => false, 'msg' => '不存在该视频']);
        }
        $iScore1 = Input::get('score1', 0);
        $iScore2 = Input::get('score2', 0);
        $iScore3 = Input::get('score3', 0);
        $token = Input::get('token');
        $token = Crypt::decrypt($token);
        $aUserinfo = explode('\t', $token);
        $iUserId = $aUserinfo[0];
        $iDevice = Input::get('device', 2);
        TrainVideoScore::addScoreLog($iVideoId, $iUserId, $iScore1, $iScore2, $iScore3, $iDevice);

        return Response::make(['success' => true, 'msg' => '成功']);
    }

    /**
     * 视频评论
     * @return mixed
     * @url /apitrainvideo/comment?videoid=1&comment=&token=
     */
    public function postComment()
    {
        $iVideoid = Input::get('videoid');
        $token = Input::get('token');
        $token = Crypt::decrypt($token);
        $aUserinfo = explode('\t', $token);
        $aComment = array(
            'user_id' => $aUserinfo[0],
            'cat_id' => 4,
            'source_id' => $iVideoid,
            'comment' => Input::get('comment'),
            'device_type' => 2,
            'created_at' => date('Y-m-d H:i:s', time())
        );
        $oComment = new Comment($aComment);
        $oComment->save();

        return Response::make(['success' => true, 'msg' => '评论成功']);
    }

    /**
     * 获取评论列表
     * @return mixed
     * @url http://cdcma.xxx.net/apitrainvideo/comment-list?videoid=1&token=
     */
    public function getCommentList()
    {
        $iVideoid = Input::get('videoid');
        $oComments = Comment::getComment(4, $iVideoid);
        $aComments = array();
        foreach ($oComments as $obj) {
            $aComments[] = array(
                'id' => $obj->id,
                'user_nick' => $obj->user_nick,
                'user_thumb' => $obj->user_thumb,
                'comment' => str_replace('/assets/images/front/emotion/',
                    Config::get('config.rooturl') . '/assets/images/front/emotion/', $obj->comment),
                'zan_count' => $obj->zan_count,
                'device_type' => $obj->device_type,
                'created_at' => $obj->created_at
            );
        }

        return Response::make(['success' => true, 'msg' => '获取记录成功', 'datalist'=>$aComments]);
    }

    /**
     * 评论点赞
     * @url /apitrainvideo/comment-surpport?commentid=250&token=
     */
    public function postCommentSurpport()
    {
        $iCatId = 4;
        $iCommentId = Input::get('commentid');
        $token = Input::get('token');
        $token = Crypt::decrypt($token);
        $aUserinfo = explode('\t', $token);
        $iUid = $aUserinfo[0];
        $oComment = Comment::where('cat_id', $iCatId)->find($iCommentId);
        if (!$oComment) {
            return Response::make(['success' => false, 'msg' => '该评论不存在！']);
        }
        $oCommentZanLog = CommentZanLog::where('cat_id', $iCatId)->where('comment_id',$iCommentId)->where('user_id',$iUid)->first();
        if ($oCommentZanLog) {
            return Response::make(['success' => false, 'msg' => '您已经赞过了！']);
        }
        $oCommentZanLog = new CommentZanLog(array(
            'comment_id' => $iCommentId,
            'user_id' => $iUid,
            'cat_id' => $iCatId,
            'created_at' => date('Y-m-d H:i:s', time())
        ));
        $oCommentZanLog->save();
        $oComment->increment('zan_count');

        return Response::make(['success' => true, 'msg' => '点赞成功']);
    }

    //&token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
    public function postTime()
    {//时间记录
        $token = Input::get('token');
        $token = Crypt::decrypt($token);
        $aUserinfo = explode('\t', $token);
        $aData = Input::all();
        $time = $aData['time'];
        $time = $time ? $time : 0;
        //加积分记录
        /*$creditIN = CreditController::getInstance($aData['videoid'], $time, $aUserinfo[0], 'ios');
        $creditIN->optCredit();*/
        // FaceVideo::addUserScoreAndViewLog($aData['videoid'],$time,$aUserinfo[0]);
        $oFaceVideo = TrainVideo::find($aData['videoid']);
        $iMin = $time;
        if ($oFaceVideo && $iMin >= 20) {
            $iVideoId = $aData['videoid'];
            $iUserId = $aUserinfo[0];
            //详细记录用户观看该视频的情况
            TrainVideoReviewLog::addUserReviewLog($iVideoId, $iUserId, $iMin);
        }

        return Response::make(['success' => true, 'msg' => '成功']);
    }


    /**
     * 返回所有科室
     * http://cdma.xxx.net/apivideo/department?token=
     */
    public function getDepartment()
    {
        if (!Input::has('token')) {
            return Response::make(['success' => false, 'msg' => '传递参数不正确']);
        }
        $token = Input::get('token');
        $token = Crypt::decrypt($token);
        $aDepartment = Config::get('config.department');
        $arr = array();
        foreach ($aDepartment as $k => $v) {
            $arr[] = array(
                'id' => $k,
                'name' => $v
            );
        }

        return Response::make(['success' => true, 'msg' => 'success', 'datalist' => $arr]);
    }
}