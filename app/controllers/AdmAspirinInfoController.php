<?php

class AdmAspirinInfoController extends BaseController {
	
	CONST  PAGESIZE = 10;

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	//轮播图系列主题列表
	public function getGroupList()
	{
	     $oGroup = AspirinInfoGroup::orderBy("created_at","desc")->paginate(self::PAGESIZE); 
		 Return View::make('admin.aspirin.info.grouplist')->with('oGroup',$oGroup);
	}

	//轮播图系列主题添加
	public function getGroupAdd()
	{
		Return View::make('admin.aspirin.info.groupadd');
	}

	public function postGroupAddDo()
	{
		$aInput = Input::all();
		$rule = array(
			'info_group_title'	=> 'required',
			'info_group_thumb'	=> 'required',
		);
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
			return $this->showMessage('请填写必填字段');
		}
		$aGroup = array(
			'info_group_title'    => trim($aInput['info_group_title']),
			'info_group_thumb'    => trim($aInput['info_group_thumb']),
			'created_at'          => date('Y-m-d H:i:s',time())
		);
		$oGroup = new AspirinInfoGroup($aGroup);
		$oGroup->save();
		return $this->showMessage('添加成功','/admaspirininfo/group-list');
	}

	//轮播图系列主题修改
	public function getGroupEdit($iId)
	{
		$oGroup = AspirinInfoGroup::find($iId);
		if($oGroup){
			Return View::make('admin.aspirin.info.groupedit')->with('oGroup',$oGroup)->with('iId',$iId);
		}else{
			return $this->showMessage('该主题不存在','/admaspirininfo/group-list');
		}
	}

	public function postGroupEditDo()
	{
		$iId = Input::get('iId');
		$aInput = Input::all();
		$rule = array(
			'info_group_title'	=> 'required',
			'info_group_thumb'	=> 'required',
		);
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
			return $this->showMessage('请填写必填字段');
		}
		$oGroup = AspirinInfoGroup::find($iId);
		if($oGroup){
			$oGroup->info_group_title = trim($aInput['info_group_title']);
			$oGroup->info_group_thumb = trim($aInput['info_group_thumb']);
			$oGroup->save();
			return $this->showMessage('修改成功','/admaspirininfo/group-list');
		}else{
			return $this->showMessage('修改失败','/admaspirininfo/group-list');
		}
	}

	//轮播图系列主题删除
	public function getGroupDelete($iId)
	{
		$oGroup = AspirinInfoGroup::find($iId);
		if($oGroup){
			$oGroup->delete();
			//删除该主题下所有资讯
			$oGroup->group()->delete();
			return $this->showMessage('删除成功','/admaspirininfo/group-list');
		}else{
			return $this->showMessage('该主题不存在','/admaspirininfo/group-list');
		}
	}

	//轮播图系列主题资讯列表
	public function getList($iGroupId)
	{
		$sTitle = Input::get('title');
		if(!empty($sTitle)){
			$oInfo = AspirinInfo::where('group_id',$iGroupId)->where('info_title','like','%'.$sTitle.'%')->paginate(self::PAGESIZE);
		}else{
			$oInfo = AspirinInfo::where('group_id',$iGroupId)->orderBy("created_at","desc")->paginate(self::PAGESIZE);
		}
			
		Return View::make('admin.aspirin.info.list')->with('oInfo',$oInfo)->with('iGroupId',$iGroupId);
	}

	//轮播图系列主题资讯添加
	public function getAdd($iGroupId)
	{
		Return View::make('admin.aspirin.info.add')->with('iGroupId',$iGroupId);
	}

	public function postAddDo()
	{
		$iGroupId = Input::get('iGroupId');
		$aInput = Input::all();
		$rule = array(
			'info_title'	    => 'required',
			'info_thumb'	    => 'required',
			'info_content'	    => 'required'
		);
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
			return $this->showMessage('请填写必填字段');
		}
		$aInfo = array(
			'group_id'         => intval($iGroupId),
			'info_title'       => trim($aInput['info_title']),
			'info_subtitle'    => trim($aInput['info_subtitle']),
			'info_thumb'       => trim($aInput['info_thumb']),
			'info_video_thumb' => trim($aInput['info_video_thumb']),
			'info_video'       => trim($aInput['info_video']),
			'info_content'     => $aInput['info_content'],
			'created_at'       => date('Y-m-d H:i:s',time())
		);
		$oInfo = new AspirinInfo($aInfo);
		$oInfo->save();
		return $this->showMessage('添加成功','/admaspirininfo/list/'.$iGroupId);
	}

	//轮播图系列主题资讯修改
	public function getEdit($iGroupId,$iId)
	{
		$oInfo = AspirinInfo::find($iId);
		if($oInfo){
			Return View::make('admin.aspirin.info.edit')->with('oInfo',$oInfo)->with('iId',$iId)->with('iGroupId',$iGroupId);
		}else{
			return $this->showMessage('该内容不存在','/admaspirininfo/list/'.$iGroupId);
		}
	}

	public function postEditDo(){
		$iId = Input::get('iId');
		$iGroupId = Input::get('iGroupId');
		$aInput = Input::all();
		$rule = array(
			'info_title'	    => 'required',
			'info_thumb'	    => 'required',
			'info_content'	    => 'required'
		);
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
			return $this->showMessage('请填写必填字段');
		}
		$oInfo = AspirinInfo::find($iId);
		if($oInfo){
			$oInfo->group_id      = intval($iGroupId);
			$oInfo->info_title    = trim($aInput['info_title']);
			$oInfo->info_subtitle = trim($aInput['info_subtitle']);
			$oInfo->info_thumb    = trim($aInput['info_thumb']);
			$oInfo->info_video_thumb    = trim($aInput['info_video_thumb']);
			$oInfo->info_video    = trim($aInput['info_video']);
			$oInfo->info_content  = $aInput['info_content'];
			$oInfo->save();
			return $this->showMessage('修改成功','/admaspirininfo/list/'.$iGroupId);
		}else{
			return $this->showMessage('修改失败','/admaspirininfo/list/'.$iGroupId);
		}
	}

	//轮播图系列主题资讯删除
	public function getDelete($iGroupId,$iId){
		$oInfo = AspirinInfo::find($iId);
		if($oInfo){
			$oInfo->delete();
			return $this->showMessage('删除成功','/admaspirininfo/list/'.$iGroupId);
		}else{
			return $this->showMessage('删除失败','/admaspirininfo/list/'.$iGroupId);
		}
	}

	//上传轮播图图片
	public function postGroupThumb(){
		if($_FILES['upload_file']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['upload_file']['name'];
			$attach_fileext = get_filetype($attach_filename);
			$rand_name = date('His', time()).rand(1000,9999);
			$sFileName = $rand_name.'.'.$attach_fileext;
			$sPath = "/upload/infogroup/$attach_fileext/".date('Ymd',time());
			$sRealPath = public_path().$sPath;
			mkdirs($sRealPath);
			// $attach_filesize = filesize($_FILES['upload_file']['tmp_name']);
			move_uploaded_file($_FILES['upload_file']['tmp_name'], $sRealPath.DS.$sFileName);
			$sFileNameS = $rand_name . '_s.' . $attach_fileext;
			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 1000, 1000 );
			$sFileUrl = $sPath.'/'.$sFileNameS;
			$json = array('info_group_thumb'=>$sFileUrl);
			echo json_encode($json);
			die;
		}
	}

	//上传资讯缩略图
	public function postInfoThumb(){
		if($_FILES['upload_file']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['upload_file']['name'];
			$attach_fileext = get_filetype($attach_filename);
			$rand_name = date('His', time()).rand(1000,9999);
			$sFileName = $rand_name.'.'.$attach_fileext;
			$sPath = "/upload/infogroup/$attach_fileext/".date('Ymd',time());
			$sRealPath = public_path().$sPath;
			mkdirs($sRealPath);
			// $attach_filesize = filesize($_FILES['upload_file']['tmp_name']);
			move_uploaded_file($_FILES['upload_file']['tmp_name'], $sRealPath.DS.$sFileName);
			$sFileNameS = $rand_name . '_s.' . $attach_fileext;
			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 1000, 1000 );
			$sFileUrl = $sPath.'/'.$sFileNameS;
			$json = array('info_thumb'=>$sFileUrl);
			echo json_encode($json);
			die;
		}
	}

	//上传视频缩略图
	public function postVideoThumb(){
		if($_FILES['upload_file0']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['upload_file0']['name'];
			$attach_fileext = get_filetype($attach_filename);
			$rand_name = date('His', time()).rand(1000,9999);
			$sFileName = $rand_name.'.'.$attach_fileext;
			$sPath = "/upload/infogroup/$attach_fileext/".date('Ymd',time());
			$sRealPath = public_path().$sPath;
			mkdirs($sRealPath);
			// $attach_filesize = filesize($_FILES['upload_file']['tmp_name']);
			move_uploaded_file($_FILES['upload_file0']['tmp_name'], $sRealPath.DS.$sFileName);
			$sFileNameS = $rand_name . '_s.' . $attach_fileext;
			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 300, 180 );
			$sFileUrl = $sPath.'/'.$sFileNameS;
			$json = array('info_video_thumb'=>$sFileUrl);
			echo json_encode($json);
			die;
		}
	}
	
}