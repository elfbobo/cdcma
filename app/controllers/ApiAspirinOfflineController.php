<?php

/**
 * API操作类
 * @author dll
 * ゃつにはつにはちつなょしっせすゃにはつゃつゅなゎ
 * κχυμγνξοθπρωψιαδλεηφβτζσ 	ΚΧΥΜΓΝΞΟΘΠΡΩΨΙΑΔΛΕΗΦΒΤΖΣ
 * лъшнвопржстуьызиагмдёщбчец 	ЛЪШНВОПРЖСТУЬЫЗИАГМДЁЩБЧЕЦ
 * ㄇㄖㄏㄎㄍㄑㄕㄘㄛㄨㄜㄠㄩㄙㄟㄣㄆㄐㄋㄔㄧㄒㄊㄌㄗㄈ
 *
 */

use Illuminate\Support\Facades\Input;

class ApiAspirinOfflineController extends BaseController {
	
	CONST  PAGESIZE = 10;
	public $iUserId = 0 ;

	public function __construct()
	{
		$this->beforeFilter('apilogin', array('except' =>array('getApplyCheck')));
		$this->beforeFilter('apionlydoc', array('only' =>array('getApply')));
		global $iUserId;
		$this->iUserId = &$iUserId;
	}

	/**
	 * 线下会议列表页
	 * http://cdma.local/apiaspirinoffline/list?token=
	 */
	public function getList(){
		$iUid = $this->iUserId;
		$aOfflineInfo = array();
		$oOffline = AspirinOffline::orderby('created_at','desc')->get();
		foreach($oOffline as $k=>$v){
			$iApplyType = 0;
			$oApplyLog = AspirinOfflineApply::where('offline_id',$v->id)->where('user_id',$iUid)->first();
			if(count($oApplyLog)){
				$iApplyType = 1;    //已报名
			}
			$aOfflineInfo[]= array(
				'id'                 => $v->id,
				'offline_title'      => $v->offline_title,
				'offline_place'      => $v->offline_place,
				'offline_start_time' => substr($v->offline_start_time,0,10),
				'offline_least_gold' => $v->offline_least_gold,
				'apply_type'         => $iApplyType
			);
		}
		echo  $this->formatRet(true,"成功","success",$aOfflineInfo);exit;
	}

	/**
	 * 线下会议详情页
	 * http://cdma.local/apiaspirinoffline/show?iOfflineId=1&token=
	 */
	public function getShow(){
		if(!Input::has('iOfflineId')){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$iUid = $this->iUserId;
		$iOfflineId = intval(Input::get('iOfflineId'));
		$oOffline = AspirinOffline::find($iOfflineId);
		$aOfflineInfo = array(
			'offline_title'      => $oOffline->offline_title,
			'offline_place'      => $oOffline->offline_place,
			'offline_time'       => substr($oOffline->offline_start_time,0,16).'-'.substr($oOffline->offline_end_time,11,5),
			'offline_content'    => str_replace('/upload/ueditor/image/', Config::get('app.url').'/upload/ueditor/image/', $oOffline->offline_content),
			'offline_least_gold' => $oOffline->offline_least_gold
		);
		echo  $this->formatRet(true,"成功","success",$aOfflineInfo);exit;
	}
	
	/**
	 * 线下会议申请参会
	 * http://cdma.local/apiaspirinoffline/apply?iOfflineId=1&token=
	 */
	public function getApply(){
		if(!Input::has('iOfflineId')){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$iOfflineId = intval(Input::get('iOfflineId'));
		$oOffline = AspirinOffline::find($iOfflineId);
		if(!count($oOffline)){
			echo $this->formatRet(false, '该会议不存在');exit;
		}
		$oApplyCount = AspirinOfflineApply::where('offline_id',$iOfflineId)->get();
		if(count($oApplyCount) >= $oOffline->offline_count && $oOffline->offline_count != 0){
			echo $this->formatRet(false, '已达报名人数上限');exit;
		}
		$iUid = $this->iUserId;
		$oUser = User::find($iUid);
		$oApplyLog = AspirinOfflineApply::where('offline_id',$iOfflineId)->where('user_id',$iUid)->first();
		if(count($oApplyLog)){
			echo $this->formatRet(false, '您已报名参加该会议');exit;
		}
		if($oOffline->apply_start_time > date('Y-m-d H:i:s',time())){
			echo $this->formatRet(false, '该会议报名未开始');exit;
		}
		if($oOffline->apply_end_time <= date('Y-m-d H:i:s',time())){
			echo $this->formatRet(false, '该会议报名已结束');exit;
		}
		//获取当前麦粒数(改为了积分)
		$iGoldCount = $oOffline->offline_least_gold;
		$iGolds = $oUser->user_score;
		if($iGolds < $iGoldCount){
			echo $this->formatRet(false,'积分不足');exit;
		}
		$oUser->user_score = $oUser->user_score - $iGoldCount;
		$oUser->save();
		//保存报名信息
		$aApply = array(
			'user_id'		=> $iUid,
			'offline_id'	=> $iOfflineId,
			'created_at'	=> date('Y-m-d H:i:s',time())
		);
		$oApply = new AspirinOfflineApply($aApply);
		$oApply->save();
		//减积分记录（type类型为3）
		$aGoldLog = array(
			'user_id'	=> $iUid,
			'gold_type'	=> 3,
			'gold_count'=> $iGoldCount,
			'source_id'	=> $iOfflineId
		);
		$oGoldLog = new AspirinUserGoldLog($aGoldLog);
		$oGoldLog->save();
		//发送报名成功通知
		$sPushContent = PushService::OUTLINE_APPLY_SUCCESS;
		$sPushContent = str_replace('{{title}}', $oOffline->offline_title, $sPushContent);
		PushService::postClient('国卫健康云',$sPushContent,41,$iUid);
		echo $this->formatRet(true,"成功","success");exit;
	}
	
}