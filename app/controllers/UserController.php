<?php

use Illuminate\Support\Facades\Redirect;

class UserController extends BaseController
{

    public $iCacheTime;

    const HASH_EXT_KEY_HASHID = 'dasfgfsdbz';
    const HASH_EXT_KEY_CHECKID = 'hiewrsbzxc';
    const CATID = 2;

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */
    public function __construct()
    {
        $this->iCacheTime = 10; //10分钟
    }

    public function Index()
    {
        //取得当前用户个人信息
        $oUser = User::find(Session::get('userid'));

        // xss 过滤
        if ($oUser) {
            // xss过滤
            $data = clean_xss($oUser->toArray(), true);
            foreach ($data as $key => $val) {
                $oUser->$key = $val;
            }
        }

        if (!$oUser->user_company) {
            $oUser->user_company = $oUser->user_company_name;
        }
        if ($oUser->role_id == 2) {            //代表
            $aHosp1 = Hospital::getChildren();
            $aHosp2 = Hospital::getChildren($oUser->user_province);
            $sR = $oUser->user_regin;
            $sArea = User::getAreaCache();
            $oAreaAll = json_decode($sArea);
            $oArea = $oAreaAll->$sR;
            return View::make('front.user.indexd')
                ->with('oUser', $oUser)
                ->with('aHosp1', $aHosp1)
                ->with('aHosp2', $aHosp2)
                ->with('oArea', $oArea);
        } elseif ($oUser->role_id == 3) {        //医生
            $aHosp1 = Hospital::getChildren();
            $aHosp2 = Hospital::getChildren($oUser->user_province);
            $aHosp3 = Hospital::getChildren($oUser->user_city);
            $aHosp4 = Hospital::getChildren($oUser->user_county);
            return View::make('front.user.index')
                ->with('oUser', $oUser)
                ->with('aHosp1', $aHosp1)
                ->with('aHosp2', $aHosp2)
                ->with('aHosp3', $aHosp3)
                ->with('aHosp4', $aHosp4);
        } else {
            $sR = $oUser->user_regin;
            $sA = $oUser->user_area;
            if (is_numeric($sR)) {
                $sRegin = User::getReginCache();
                $oRegin = json_decode($sRegin);
                $oUser->user_regin = $oRegin->$sR;
            }
            if (is_numeric($sA)) {
                $sArea = User::getAreaCache();
                $oArea = json_decode($sArea);
                $oUser->user_area = $oArea->$sR->$sA;
            }
            $iHospitalId = $oUser->user_company;
            if (is_numeric($iHospitalId)) {
                $sHospital = Hospital::where('id', $iHospitalId)->pluck('name');
                if ($sHospital) {
                    $oUser->user_company = $sHospital;
                }
            }
            return View::make('front.user.indexa')
                ->with('oUser', $oUser);
        }
        return View::make('front.user.index')
            ->with('oUser', $oUser);
    }

    //修改用户信息页面
    public function UserInfoChange()
    {
        //取得当前用户个人信息
        $oUser = User::find(Session::get('userid'));

        if (!$oUser->user_company) {
            $oUser->user_company = $oUser->user_company_name;
        }
        if ($oUser->role_id == 2) {            //代表
            $aHosp1 = Hospital::getChildren();
            $aHosp2 = Hospital::getChildren($oUser->user_province);
            $sR = $oUser->user_regin;
            $sArea = User::getAreaCache();
            $oAreaAll = json_decode($sArea);
            $oArea = $oAreaAll->$sR;
            return View::make('front.user.editinfod')
                ->with('oUser', $oUser)
                ->with('aHosp1', $aHosp1)
                ->with('aHosp2', $aHosp2)
                ->with('oArea', $oArea);
        } elseif ($oUser->role_id == 3) {        //医生
            $aHosp1 = Hospital::getChildren();
            $aHosp2 = Hospital::getChildren($oUser->user_province);
            $aHosp3 = Hospital::getChildren($oUser->user_city);
            $aHosp4 = Hospital::getChildren($oUser->user_county);
            return View::make('front.user.editinfo')
                ->with('oUser', $oUser)
                ->with('aHosp1', $aHosp1)
                ->with('aHosp2', $aHosp2)
                ->with('aHosp3', $aHosp3)
                ->with('aHosp4', $aHosp4);
        } else {
            $sR = $oUser->user_regin;
            $sA = $oUser->user_area;
            if (is_numeric($sR)) {
                $sRegin = User::getReginCache();
                $oRegin = json_decode($sRegin);
                $oUser->user_regin = $oRegin->$sR;
            }
            if (is_numeric($sA)) {
                $sArea = User::getAreaCache();
                $oArea = json_decode($sArea);
                $oUser->user_area = $oArea->$sR->$sA;
            }
            $iHospitalId = $oUser->user_company;
            if (is_numeric($iHospitalId)) {
                $sHospital = Hospital::where('id', $iHospitalId)->pluck('name');
                if ($sHospital) {
                    $oUser->user_company = $sHospital;
                }
            }
            return View::make('front.user.editinfoa')
                ->with('oUser', $oUser);
        }

        return View::make('front.user.editinfo')
            ->with('oUser', $oUser);
    }

    //修改密码
    public function ChengeUserInfo($sUserNick, $sOldPsw = '', $sNewPsw = '')
    {
        $iUid = Session::get('userid');
        $aUser = User::where('id', $iUid)->get()->toArray();
        $aUser = $aUser[0];
        $aUser['user_nick'] = $sUserNick;
        $flg = User::IsUserMsgRepeat($aUser, $iUid);
        $flg = User::IsUserNickRepeat($aUser, $iUid);
        if ($flg) {
            echo 'nick_repeat';
            die;
        }
        $oUser = User::find($iUid);
        $oUser->user_nick = $sUserNick;
        //用户名正确
        if ($sOldPsw) {
            //查看旧密码是否填写正确
            if (Hash::check($sOldPsw, $oUser->password)) {
                $oUser->password = Hash::make($sNewPsw);
                $oUser->save();
                echo 'success';
                die;
                //修改成功
            } else {
                //原密码输入错误
                echo 'old_err';
                die;
            }
        }
    }

    public function ChangeUserInfoAll()
    {
        $aInput = Input::all();
        $oUser = User::find(Session::get('userid'));
        if (isset($aInput['password_old'])) {
            //有修改密码
            $sOldPsw = $aInput['password_old'];
            $sNewPsw = $aInput['password_new'];
            //查看旧密码是否填写正确
            if (Hash::check($sOldPsw, $oUser->password)) {
                unset($aInput['password_old']);
                unset($aInput['password_new']);
                foreach ($aInput as $k => $v) {
                    $oUser->$k = $v;
                }
                $oUser->password = Hash::make($sNewPsw);
                $oUser->save();
                return 'success';
                //修改成功
            } else {
                //原密码输入错误
                echo 'old_err';
                die;
            }
        } else {
            //无修改密码
            //查看手机号码和邮箱是否重复
            $sMsg = User::IsUserTelEmailRepeat($aInput, Session::get('userid'));
            if ($sMsg) {
                return $sMsg;
            }

            foreach ($aInput as $k => $v) {
                $oUser->$k = $v;
            }
            $oUser->save();
            return 'success';
        }
    }

    //上传用户图片
    public function UploadUserThumb()
    {
        if ($_FILES['upload_file']['error'] > 0) {
            $error = $_FILES['thumb']['error'];
        } else {
            $attach_filename = $_FILES['upload_file']['name'];
            $attach_fileext = get_filetype($attach_filename);
            $rand_name = date('YmdHis', time()) . rand(1000, 9999);

            $sFileName = $rand_name . '.' . $attach_fileext;

            $sPath = "/upload/user/$attach_fileext/" . date('Ymd', time());

            $sRealPath = public_path() . $sPath;
            mkdirs($sRealPath);
//		    $attach_filesize = filesize($_FILES['upload_file']['tmp_name']);
            move_uploaded_file($_FILES['upload_file']['tmp_name'], $sRealPath . DS . $sFileName);

            $sFileNameS = $rand_name . '_s.' . $attach_fileext;
            resizeImage($sRealPath . DS . $sFileName, $sRealPath . DS . $sFileNameS, 1000, 1000);

            $sFileUrl = $sPath . '/' . $sFileNameS;

            //修改当前用户图片路径
            $iUid = Session::get('userid');
            $oUser = User::find($iUid);
            $oUser->user_thumb = $sFileUrl;
            $oUser->save();

            $json = array('user_thumb' => $sFileUrl);
            echo json_encode($json);
            die;
        }
    }

    //上传用户图片
    public function UploadUserFirstThumb()
    {
        if ($_FILES['upload_file']['error'] > 0) {
            $error = $_FILES['thumb']['error'];
        } else {
            $attach_filename = $_FILES['upload_file']['name'];
            $attach_fileext = get_filetype($attach_filename);
            $rand_name = date('YmdHis', time()) . rand(1000, 9999);

            $sFileName = $rand_name . '.' . $attach_fileext;

            $sPath = "/upload/user/$attach_fileext/" . date('Ymd', time());

            $sRealPath = public_path() . $sPath;
            mkdirs($sRealPath);
//		    $attach_filesize = filesize($_FILES['upload_file']['tmp_name']);
            move_uploaded_file($_FILES['upload_file']['tmp_name'], $sRealPath . DS . $sFileName);

            $sFileNameS = $rand_name . '_s.' . $attach_fileext;
            resizeImage($sRealPath . DS . $sFileName, $sRealPath . DS . $sFileNameS, 1000, 1000);

            $sFileUrl = $sPath . '/' . $sFileNameS;

            $json = array('user_thumb' => $sFileUrl);
            echo json_encode($json);
            die;
        }
    }

    //上传用户图片
    public function UploadUserFirstThumbcertificate()
    {
        if ($_FILES['upload_file_certificate']['error'] > 0) {
            $error = $_FILES['thumb']['error'];
        } else {
            $attach_filename = $_FILES['upload_file_certificate']['name'];
            $attach_fileext = get_filetype($attach_filename);
            $rand_name = date('YmdHis', time()) . rand(1000, 9999);

            $sFileName = $rand_name . '.' . $attach_fileext;

            $sPath = "/upload/user/$attach_fileext/" . date('Ymd', time());

            $sRealPath = public_path() . $sPath;
            mkdirs($sRealPath);
            //		    $attach_filesize = filesize($_FILES['upload_file']['tmp_name']);
            move_uploaded_file($_FILES['upload_file_certificate']['tmp_name'], $sRealPath . DS . $sFileName);

            $sFileNameS = $rand_name . '_s.' . $attach_fileext;
            resizeImage($sRealPath . DS . $sFileName, $sRealPath . DS . $sFileNameS, 1000, 1000);

            $sFileUrl = $sPath . '/' . $sFileNameS;

            $json = array('user_thumb' => $sFileUrl);
            echo json_encode($json);
            die;
        }
    }

    //上传用户医师证照
    public function UploadUserCardThumb()
    {
        if ($_FILES['upload_card_file']['error'] > 0) {
            $error = $_FILES['thumb']['error'];
        } else {
            $attach_filename = $_FILES['upload_card_file']['name'];
            $attach_fileext = get_filetype($attach_filename);
            $rand_name = date('YmdHis', time()) . rand(1000, 9999);

            $sFileName = $rand_name . '.' . $attach_fileext;

            $sPath = "/upload/usercard/$attach_fileext/" . date('Ymd', time());

            $sRealPath = public_path() . $sPath;
            mkdirs($sRealPath);
            //		    $attach_filesize = filesize($_FILES['upload_file']['tmp_name']);
            move_uploaded_file($_FILES['upload_card_file']['tmp_name'], $sRealPath . DS . $sFileName);

            $sFileNameS = $rand_name . '_s.' . $attach_fileext;
            resizeImage($sRealPath . DS . $sFileName, $sRealPath . DS . $sFileNameS, 1000, 1000);

            $sFileUrl = $sPath . '/' . $sFileNameS;

            $json = array('card_thumb' => $sFileUrl);
            echo json_encode($json);
            die;
        }
    }

    //我的积分
    public function MyScore()
    {
        $iUid = Session::get('userid');
        $oUser = User::find($iUid);
        //累计观看直播时长
        $iLiveMin = VideoLog::getUserMinByType($iUid, 2);
        //录播
        $iReviewMin = VideoLog::getUserMinByType($iUid, 1);
        //获得所有该用户观看的录播和直播
        $oVideoLog = VideoLog::getUserVideoLog($iUid, 20);
        return View::make('front.user.myscore')
            ->with('oUser', $oUser)
            ->with('iLiveMin', $iLiveMin)
            ->with('iReviewMin', $iReviewMin)
            ->with('oVideoLog', $oVideoLog);
    }

    //我的预约
    public function MyOrder()
    {
        if (Session::get('roleid') != 2) {
            //不是代表
            return Redirect::to('/');
        }
        $iUid = Session::get('userid');
        $oUserOrder = UserOrder::where('user_id', $iUid)->get();
        return View::make('front.user.myorder')
            ->with('oUserOrder', $oUserOrder);
    }

    //马上预约
    public function OrderNow()
    {
        //获取本月可预约专家
        $oDoc = UserOrderDoc::where('start_time', '>', date('Y-m-d H:i:s'))
            ->where(function ($query) {
                $query->where('user_regin', '')
                    ->orWhere('user_regin', 'LIKE', "%|" . Auth::User()->user_regin . "|%");
            })
            ->get();
        $iUserId = Session::get('userid');
        //积分排行前50代表
// 		$oUser = User::getUserScoreList(2,50);
// 		$iIsInFront = 0;
// 		foreach($oUser as $k=>$v){
// 			if($iUserId==$v->id){
// 				$iIsInFront = 1;
// 				break;
// 			}
// 		}
        $iIsInFront = 1;
        return View::make('front.user.ordernow')
            ->with('oDoc', $oDoc)
            ->with('iIsInFront', $iIsInFront);
    }

    //马上预约do
    public function OrderNowDo($sTime, $sName, $sDepar)
    {
        $aUserOrder = array(
            'user_id' => Session::get('userid'),
            'doc_name' => $sName,
            'doc_department' => $sDepar,
            'order_time' => $sTime
        );
        $oUserOrder = new UserOrder($aUserOrder);
        $oUserOrder->save();
        return 'success';
    }

    //积分排行
    public function ScoreList($iRoleId)
    {

        //获取前一百名用户的积分排行
        $aUser = User::getUserScoreList($iRoleId, 100, Session::get('userid'));
        return View::make('front.user.scorelist')
            ->with('aUser', $aUser)
            ->with('iRoleId', $iRoleId);
    }

    //积分排行
    public function ScoreList2($iRoleId)
    {
        //获取前一百名用户的积分排行
        $oUser = User::where('role_id', $iRoleId)->orderBy('user_score', 'DESC')->take(100)->get();
        return View::make('front.user.scorelist')
            ->with('oUser', $oUser)
            ->with('iRoleId', $iRoleId);
    }

    /**
     * 修改代表大区及地区
     */
    public function ChangeRepInfo($iReginId, $iAreaId)
    {
//		$sArea = User::getAreaCache();
//		$sRegin = User::getReginCache();
//		$aArea = json_decode($sArea);
//		$aRegin = json_decode($sRegin);
//		$sReginName = $aRegin->$iReginId;
//		$sAreaName = $aArea->$iAreaId;
        $oUser = User::find(Session::get('userid'));
        $oUser->user_regin = $iReginId;
        $oUser->user_area = $iAreaId;
        $oUser->save();

        return 'success';
    }

    public function AreaInfo($iReginId)
    {
        $sArea = User::getAreaCache();
        $oArea = json_decode($sArea);
        if (isset($oArea->$iReginId)) {
            $oReginArea = $oArea->$iReginId;
            $aArea = array();
            foreach ($oReginArea as $k => $v) {
                $aArea[$k] = $v;
            }
            if (!$aArea) {
                return json_encode('noinfo');
            }
            return json_encode($aArea);
        } else {
            return json_encode('noinfo');
        }
    }

    public function HospitalChild($pid = 0)
    {
        $arr = Hospital::getChildren($pid);
        if ($arr) {
            return json_encode($arr);
        } else {
            return json_encode('noinfo');
        }
    }

    //更新医生职称   dll add
    public function ChangeDocPosInfo($pname = '')
    {
        $oUser = User::find(Session::get('userid'));
        $oUser->user_position = $pname;
        $oUser->save();
        return 'success';
    }

    public function ChangeDocCityInfo($pid, $cid, $conid = 0, $hid = 0, $dname = '', $pname = '', $cname = '')
    {
        $oUser = User::find(Session::get('userid'));
        $oUser->user_province = $pid;
        $oUser->user_city = $cid;
        $oUser->user_county = $conid;
        $oUser->user_company = $hid;
        $oUser->user_department = $dname;
        $oUser->user_position = $pname;
        $oUser->user_company_name = $cname;
        $oUser->save();

        return 'success';
    }

    public function ChangeRepCityInfo($pid, $cid)
    {
        $oUser = User::find(Session::get('userid'));
        $oUser->user_province = $pid;
        $oUser->user_city = $cid;
        $oUser->save();

        return 'success';
    }

    //我的培训
    public function MyTrain()
    {
        if (Session::get('roleid') != 2) {
            return Redirect::to('/');
        }
//		$oTrain = UserTrain::where('end_time','>=',date('Y-m-d H:i:s'))->orderBy('start_time','ASC')->get();
        $oTrain = UserTrain::getAllTrain();
        return View::make('front.user.mytrain')
            ->with('oTrain', $oTrain);
    }

    //进入培训直播
    public function EnterTrain($iId)
    {
        if (Session::get('roleid') != 2) {
            return Redirect::to('/');
        }
        $oTrain = UserTrain::find($iId);
        if ($oTrain) {
            //用户信息存数据库操作
            $iUid = Session::get('userid');
            $aTrainLog = array(
                'train_id' => $iId,
                'user_id' => $iUid
            );
            $oUserTrainLog = new UserTrainLog($aTrainLog);
            $oUserTrainLog->save();
            header("Location:" . $oTrain->train_url . "?nickName=" . Auth::User()->user_nick);
            exit;
//			return Redirect::to($oTrain->train_url.'?nickName='.Auth::User()->user_nick);
        } else {
            return Redirect::to('/');
        }
    }

    //进入培训录播
    public function TrainReview($iId)
    {
        if (Session::get('roleid') != 2) {
            return Redirect::to('/');
        }
        $oTrain = UserTrain::find($iId);
        if ($oTrain) {
            //点击量+1
            $oTrain->increment('train_review_hits');
            //获取所有评论
            $iCatId = 3;
            $oComments = Comment::getComment($iCatId, $iId);
            $sImgLi = Images();

            return View::make('front.user.mytrainreview')
                ->with('oTrain', $oTrain)
                ->with('oComments', $oComments)
                ->with('sImgLi', $sImgLi);
        } else {
            return Redirect::to('/');
        }
    }

    //观看录播时长统计操作
    public function VideoView($iVideoId, $iUserId, $iMin)
    {
        //操作
        $oUserTrainReviewLogExists = UserTrainReviewLog::where('train_id', $iVideoId)->where('user_id',
            $iUserId)->first();
        if (count($oUserTrainReviewLogExists)) {
            if ($oUserTrainReviewLogExists->watch_minutes < $iMin) {
                $oUserTrainReviewLogExists->watch_minutes = $iMin;
                $oUserTrainReviewLogExists->save();
                echo 'success';
                die;
            } else {
                echo 'success';
                die;
            }
        } else {
            $oUserTrainReviewLog = new UserTrainReviewLog;
            $oUserTrainReviewLog->train_id = $iVideoId;
            $oUserTrainReviewLog->user_id = $iUserId;
            $oUserTrainReviewLog->watch_minutes = $iMin;
            $oUserTrainReviewLog->save();
            echo 'success';
            die;
        }
    }


    //新版我的预约--提交
    public function OrderDo($iDocId)
    {
        $oDoc = UserOrderDoc::find($iDocId);
        if (!$oDoc) {
            return 'error';
        }
        $iUserId = Session::get('userid');
        //查看是否有该用户的该条预约信息
        $oUserOrder = UserOrder::where('user_id', $iUserId)
            ->where('user_order_doc_id', $iDocId)
            ->get();
        if (count($oUserOrder)) {
            return 'repeat';
        }
        $aUserOrder = array(
            'user_id' => $iUserId,
            'user_order_doc_id' => $oDoc->id,
            'doc_name' => $oDoc->doc_name,
            'doc_department' => $oDoc->doc_department,
            'order_time' => substr($oDoc->start_time, 0, 10)
        );
        $oUserOrder = new UserOrder($aUserOrder);
        $oUserOrder->save();
        return 'success';
    }

    //医生管理栏目
    public function MyDoc()
    {
        $iUserId = Session::get('userid');
        $iRoleId = Session::get('roleid');
        if ($iRoleId != 2) {
            //若不是代表
            return Redirect::to('/');
        }
        //获取当前代表下面的医生用户
        $oDoc = User::select('id', 'user_name', 'user_company', 'user_company_name')
            ->where('link_rep_id', $iUserId)
            ->get();
        $aDocIds = array();
        $aHospital = Hospital::lists('name', 'id');
        foreach ($oDoc as $k => $v) {
            $aDocIds[] = $v->id;
            //获得医院名称
            $sUserCompany = $v->user_company;
            if (is_numeric($sUserCompany) && $sUserCompany != 0) {
                $sUserCompany = !empty($aHospital[$sUserCompany]) ? $aHospital[$sUserCompany] : '';
            }
            if (!$sUserCompany) {
                $sUserCompany = $v->user_company_name;
            }
            $oDoc[$k]->user_company = $sUserCompany;
        }

        $aUserSurvey = User::select('id', 'user_type')
            ->whereIn('id', $aDocIds)
            ->lists('user_type', 'id');
        $aUserAdalateType = UserSurveySub::select('user_id', 'adalate_type')
            ->where('phase_id', Config::get('config.phase_id'))
            ->whereIn('user_id', $aDocIds)
            ->where('adalate_type', '>', 0)
            ->lists('adalate_type', 'user_id');
        //第一期特殊处理，第一期特殊处理，只能选择用户类型 start
        if (Config::get('config.phase_id') == 1) {
            $aUserAdalateType = array();
        }
        //第一期特殊处理，第一期特殊处理，只能选择用户类型 end
        return View::make('front.user.mydoc')
            ->with('oDoc', $oDoc)
            ->with('aUserSurvey', $aUserSurvey)
            ->with('aUserAdalateType', $aUserAdalateType);
    }

    //医生管理栏目选择用户类型ajax提交
    public function DocTypeDo()
    {
        $json = Input::get('data', '');
        $aData = json_decode($json);
        $iRoleId = Session::get('roleid');
        if ($iRoleId != 2) {
            //若不是代表
            return 'error';
        }
        foreach ($aData as $k => $v) {
            if ($v) {
                $oUser = User::find($v[0]);
                if ($oUser && $oUser->user_type == 0) {
                    $oUser->user_type = $v[1];
                    $oUser->save();
                }
            }
        }
        return 'success';
    }

    //推送调研ajax提交
    public function DocPushSurveyDo($iDocId = 0)
    {
        $iCloseSurvey = Config::get('config.is_survey_open');
        if ($iCloseSurvey != 1) {
            //未开启调研
            return 'survey_closed';
        }
        $oUserSurveySub = UserSurveySub::where('user_id', $iDocId)
            ->where('phase_id', Config::get('config.phase_id'))
            ->first();
        $iUserType = User::select('user_type')->where('id', $iDocId)->pluck('user_type');
        if ($iUserType != 1 && $iUserType != 3) {
            //不是拜新同用户
            return 'error';
        }
        if (!$oUserSurveySub) {        //若未选择过用户类型（表中无该医生记录）,给用户推送当前期调研
            $aUserSurveySub = array(
                'user_id' => $iDocId,
                'phase_id' => Config::get('config.phase_id')
            );
            $oUserSurveySub = new UserSurveySub($aUserSurveySub);
            $oUserSurveySub->save();
        } else {
            $oUserSurveySub->is_refuse_survey = 0;
            $oUserSurveySub->save();
        }
        return 'success';
    }

    //调研提交
    public function SurveyAjaxSubmit()
    {
        $aInput = Input::all();
        $iCloseSurvey = Config::get('config.is_survey_open');
        if ($iCloseSurvey == 1) {
            //若开启调研，则判断当前用户是否可以参加调研
            $oUserSurveySub = UserSurveySub::where('user_id', Session::get('userid'))->where('phase_id',
                Config::get('config.phase_id'))->first();
            if ($oUserSurveySub
                && $oUserSurveySub->is_refuse_survey == 0
                && $oUserSurveySub->has_survey == 0) {
                //用户可以参加调研,取出当前期的调研题
                $aQuestion = UserSurveyQuestion::getAllQuestionAndItem(Config::get('config.phase_id'));
                if (empty($aInput['survey']) || count($aInput['survey']) != count($aQuestion)) {
                    return json_encode(array('success' => false, 'msg' => '请检查答题结果是否有遗漏'));
                }
                $aSurvey = $aInput['survey'];
                $aResult = array();        //存储用户选择结果
                $aOptions = Config::get('config.options');
                foreach ($aSurvey as $k => $v) {
                    //记录答题结果
                    $iPhaseId = Config::get('config.phase_id');
                    $iUserId = Session::get('userid');
                    $sR = '';
                    if (is_array($v)) {
                        //多选
                        foreach ($v as $val) {
                            if (in_array($val, $aOptions)) {
                                $sR .= $val;
                            }
                        }
                    } else {
                        //单选
                        if (in_array($v, $aOptions)) {
                            $sR = $v;
                        }
                    }
                    if (!$sR) {
                        return json_encode(array('success' => false, 'msg' => '提交失败'));
                    }
                    $aResult[] = array(
                        'phase_id' => $iPhaseId,
                        'question_id' => $k,
                        'user_id' => $iUserId,
                        'option' => $sR
                    );
                }
                UserSurveyAnswer::insert($aResult);
                //更新用户已答题标记位
                //判断用户是拜新同偏好者还是拜新同中立者
                $iAdalateType = User::getUserAdalateType();
                $oUserSurveySub->has_survey = 1;
                $oUserSurveySub->adalate_type = $iAdalateType;
                $oUserSurveySub->save();
                return json_encode(array('success' => true, 'msg' => '提交成功'));
            }
        }
        return json_encode(array('success' => false, 'msg' => '您没有权限继续答题，如有问题可拨打电话联系我们'));
    }

    //提交调研后选择是否同意注册医脉通选择
    public function AgreeMedliveReg($iFlag)
    {
        return;
    }

    //同意注册医脉通后加麦粒操作
    public function AgreeRegMedliveSubmit()
    {
        return '';
    }

    //用户关闭调研弹框
    public function SurveyClose()
    {
        $iUserId = Session::get('userid');
        $iCloseSurvey = Config::get('config.is_survey_open');
        if ($iCloseSurvey == 1) {
            //若开启调研
            $oUserSurveySub = UserSurveySub::where('user_id', Session::get('userid'))->where('phase_id',
                Config::get('config.phase_id'))->first();
            if ($oUserSurveySub && $oUserSurveySub->has_survey == 0) {
                $oUserSurveySub->is_refuse_survey = 1;
                $oUserSurveySub->save();
            }
        }
        return 'success';
    }

    //加麦粒
    public function addUserCredit($iUserId, $iTypeId, $iMessageId, $iGold, $iScore)
    {
        return '';
    }

    //获取hashid和checkid 通过不同的key值
    public static function getHashidOrCheckid($user, $downloadKey = '')
    {
        return '0';
    }

    //我的医生
    public function MyDocList()
    {
        $iUserId = Session::get('userid');
        $iRoleId = Session::get('roleid');
        if ($iRoleId != 2) {
            //若不是代表
            return Redirect::to('/user/');
        }
        $docList = User::select('id', 'link_user_id', 'user_name', 'user_department', 'user_position', 'user_company',
            'health_integral', 'user_thumb')
            ->where('link_rep_id', $iUserId)
            ->where('card_auth_flag', "!=", 2)
            ->orderBy('health_integral', 'DESC')
            ->paginate(10);

        return View::make('front.user.mydoclist')
            ->with('docList', $docList)
            ->with('domains', "http://cdcma.bizconf.cn");
        // ->with('domains',"http://{$_SERVER['HTTP_HOST']}");
    }

    //医生观看视频记录日志
    public function MyDocViewLog()
    {
        $VideoViewLog = new VideoViewLog();
        $iUserId = Session::get('userid');
        $iRoleId = Session::get('roleid');
        if ($iRoleId != 2) {
            //若不是代表
            return Redirect::to('/user/');
        }
        $doc_id = Input::get('doc_id');
        $aLink = array();
        $aLink['doc_id'] = $doc_id;
        $docInfo = user::find($doc_id);
        $logList = VideoViewLog::where('user_id', $doc_id)
            ->groupBy('video_id')
            ->orderBy('created_at', 'DESC')
            ->paginate(10);
        return View::make('front.user.mydocviewlog')
            ->with('aLink', $aLink)
            ->with('logList', $logList)
            ->with('docInfo', $docInfo)
            ->with('domains', "http://cdcma.bizconf.cn")
            ->with('nowdomains', "http://{$_SERVER['HTTP_HOST']}");
    }

    //我的讲者
    public function MyAuthDocList()
    {
        $iUserId = Session::get('userid');
        $iRoleId = Session::get('roleid');
        if ($iRoleId != 2) {
            //若不是代表
            return Redirect::to('/user/');
        }
        $docList = User::select('id', 'link_user_id', 'user_name', 'user_department', 'user_position', 'user_company',
            'health_integral', 'user_thumb')
            ->where('link_rep_id', $iUserId)
            ->where('card_auth_flag', 2)
            ->orderBy('health_integral', 'DESC')
            ->paginate(10);
        return View::make('front.user.myauthdoclist')
            ->with('docList', $docList)
            ->with('domains', "http://cdcma.bizconf.cn")
            ->with('nowdomains', "http://{$_SERVER['HTTP_HOST']}");
    }

    /**
     * 移除医生
     * @return mixed
     */
    public function doRemoveDoctor()
    {
        $iUserId = Session::get('userid');
        $user_doctor_id = Input::get('id');
        // 表单验证
        $validator = Validator::make(
            Input::all(),
            array(
                'id' => 'required|numeric'
            ),
            ['id.required' => '医生不能为空']
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $message) {
                return Response::make(['status' => 0, 'msg' => $message]);
            }
        }

        // 获取已存在的医生信息
        $doctor = User::where('id', $user_doctor_id)
            ->where('link_rep_id', $iUserId)
            ->where('role_id', 3)
            ->first();
        if ($doctor) {
            $doctor->link_rep_id = 0;
            $doctor->save();
        }

        return Response::make(['status' => 1, 'msg' => '移除成功']);
    }

    /**
     * 添加我的医生列表
     * user/add-doclist
     */
    public function addDoctorList()
    {
        $iUserId = Session::get('userid');
        $iRoleId = Session::get('roleid');
        if ($iRoleId != 2) {
            //若不是代表
            return Redirect::to('/user/');
        }

        // 提交请求
        if (Request::ajax() && strtolower(Request::server('REQUEST_METHOD')) == 'post') {
            if (!Input::has('id')) {
                return Response::make(['status' => 0, 'msg' => '参数错误']);
            }
            $user_id = Input::get('id', 0);
            // 具体业务
            $doctor = User::where('id', $user_id)->where('role_id', 3)->first();
            // 查看医生是否存在
            if (!$doctor) {
                return Response::make(['status' => 0, 'msg' => '该医生不存在']);
            }

            // 存在的记录非本人的情况下，进行修改所属代表
            if ($doctor->link_rep_id != $iUserId) {
                $push_user_id = $doctor->link_rep_id;
                $doctor->link_rep_id = $iUserId;
                $doctor->save();
                // 推送消息
                if ($push_user_id) {
                    $deputy_name = User::where('id', $iUserId)->pluck('user_name');
                    if ($deputy_name) {
                        $content = "您名下的医生 {$doctor->user_name} 已被代表 {$deputy_name} 挖走喽~";
                        PushService::postClient('国卫健康云', $content, 51, $push_user_id, 5);
                    }
                }
            }
            return Response::make(['status' => 1, 'msg' => '关联成功']);
        }

        // 获取参数
        $keyword = Input::get('keyword');

        $docList = User::where('user.role_id', 3)
            ->select([
                'user.*',
                'deputy.user_nick as deputy_nick',
                'deputy.user_name as deputy_name',
                'deputy.user_thumb as deputy_thumb',
                'deputy.user_department as deputy_department',
                'deputy.user_position as deputy_position',
                'hospital.id as hospital_id',
                'hospital.name as hospital_name'
            ])
            ->leftJoin('user as deputy', 'deputy.id', '=', 'user.link_rep_id')
            ->leftJoin('hospital', 'user.user_company', '=', 'hospital.id')
            ->where(function ($query) use ($keyword) {
                $sqlarr = [];
                if (!empty($keyword)) {
                    $sqlarr[] = "user.user_name like '%{$keyword}%'";
                    $sqlarr[] = "user.user_tel like '%{$keyword}%'";
                    $sqlarr[] = "hospital.name like '%{$keyword}%'";
                    $sqlarr[] = "user.user_company_name like '%{$keyword}%'";
                }
                if (!empty($sqlarr)) {
                    $query->whereRaw(join(' OR ', $sqlarr));
                }
            })
            ->orderBy("user.updated_at", "desc")
            ->paginate(10);

        return View::make('front.user.adddoclist')
            ->with('docList', $docList)
            ->with('domains', "http://cdcma.bizconf.cn")
            ->with('keyword', $keyword)
            ->with('userid', $iUserId);
    }

    //讲师发布课程记录
    public function MyAuthDocClass()
    {
        $iUserId = Session::get('userid');
        $iRoleId = Session::get('roleid');
        if ($iRoleId != 2) {
            //若不是代表
            return Redirect::to('/user/');
        }
        $doc_id = Input::get('doc_id');
        $docInfo = user::find($doc_id);
        $aLink = array();
        $aLink['doc_id'] = $doc_id;
        $oEducation = AspirinEducation::where('video_flag', 0)
            ->where('user_id', $doc_id)
            ->where('education_type', 1)
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        // $log = DB::getQueryLog();
        // print_r($log);die;
        return View::make('front.user.myauthdocclass')
            ->with('aLink', $aLink)
            ->with('docInfo', $docInfo)
            ->with('oEducation', $oEducation)
            ->with('domains', "http://cdcma.bizconf.cn")
            ->with('nowdomains', "http://{$_SERVER['HTTP_HOST']}");
    }

    //讲师课程详情 ------------------------------------------------------------------------- NOT FINISHED
    public function MyAuthDocClassDetail()
    {
        $iUid = $iUserId = Session::get('userid');
        /*$iRoleId = Session::get('roleid');
        if($iRoleId!=2){
            //若不是代表
            return Redirect::to('/user/myauthdocclass?doc_id=');
        }*/
        $iEducationId = $video_id = Input::get('id');
        $oEducation = AspirinEducation::find($video_id);
        if (!$oEducation) {
            return Redirect::to('/user/myauthdocclass?doc_id=' . $oEducation->user_id);
            // echo $this->formatRet(false,"该健康教育视频不存在","");
            // exit;
        }
        $doc_info = user::find($oEducation->user_id);
        $sUserCompany = $doc_info->user_company;
        if (is_numeric($sUserCompany) && $sUserCompany != 0) {
            $sUserCompany = Hospital::where('id', $sUserCompany)->pluck('name');
            if ($sUserCompany) {
                $doc_info->user_company_name = $sUserCompany;
            }
        }
        // print_r($doc_info);die;
        return View::make('front.user.myauthdocclassdetail')
            ->with('oEducation', $oEducation)
            ->with('doc_info', $doc_info)
            ->with('iUid', $iUid);
    }

    //我的健康云积分
    public function MyHealthScore()
    {
        $iUid = $iUserId = Session::get('userid');
        $oUser = User::find($iUid);
        if (!$oUser) {
            return Redirect::to('/user/my-health-score');
            // echo $this->formatRet(false, '用户不存在');
            // exit;
        }
        $integral = $oUser->health_integral;
        //获取积分日志列表
        $IntegralLog = new IntegralLog();
        $totalIntegral = IntegralLog::where('user_id', $iUid)->where('types', "=", 'in')->sum('number');
        $aArr = array();
        $oList = IntegralLog::select('user_health_integral_log.*')
            ->where('types', '=', "in")->where('user_id', '=', $iUid)
            ->where('number', '>', 0)
            ->orderBy('created_at', 'DESC')->paginate(15);
        // print_r($oList->toArray());die;
        return View::make('front.user.myhealthscore')
            ->with('oUser', $oUser)
            ->with('integral', $integral)
            ->with('totalIntegral', $totalIntegral)
            ->with('oList', $oList);
    }

    //我的健康云积分排行
    public function HealthScoreList($iRoleId)
    {
        $iUid = $iUserId = Session::get('userid');
        $oUser = User::find($iUid);
        if (!$oUser) {
            return Redirect::to('/user/my-health-score');
            // echo $this->formatRet(false, '用户不存在');
            // exit;
        }
        if (!in_array($iRoleId, array(2, 3))) {
            return Redirect::to('/user/my-health-score');
            //取当前用户的role_id
            // $iRoleId = $oUser->role_id;
        }
        //获取当前用户积分排行
        $iUserRank = User::getUserRankById($iRoleId, $iUid, 'integral');
        //获取前一百名用户的积分排行
        $aUser = User::getUserScoreList($iRoleId, 100, $iUid, 'integral');
        // print_r($aUser);die;
        return View::make('front.user.healthscorelist')
            ->with('oUser', $oUser)
            ->with('iRoleId', $iRoleId)
            ->with('iUserRank', $iUserRank)
            ->with('aUser', $aUser);
    }

    // 积分兑换页面
    // [id] =&gt; 1
    // [video_integral] =&gt; 10
    // [live_integral] =&gt; 10
    // [view_video_deduct_integral] =&gt; 10
    // [cmc_use_integral] =&gt; 60
    // [integral_tel] =&gt; 010-85879900-8067
    // [exchange_rule_integral] =&gt; 每参加一次自助会议积10分。
    // [teach_agreement] =&gt; 讲师报名成功后，参与者满6人（含讲者），提醒讲者签署协议。
    // [view_teacher_video_agreement] =&gt; 讲师录制的视频发布并审核通过后，观看人数达到10人，提醒讲者签署协议。
    // [doc_agreement] =&gt; 医生提交认证签，签署的医生认证协议。
    // [video_audit_times] =&gt; 20
    public function UseScore()
    {
        $iUid = $iUserId = Session::get('userid');
        $oUser = User::find($iUid);
        if (!$oUser) {
            return Redirect::to('/user/my-health-score');
            // echo $this->formatRet(false, '用户不存在');
            // exit;
        }
        $integral = $oUser->health_integral;
        //获取积分日志列表
        $IntegralLog = new IntegralLog();
        $totalIntegral = IntegralLog::where('user_id', $iUid)->where('types', "=", 'in')->sum('number');
        //获取基础配置
        $Configs = new Configs();
        $configRs = $Configs::find(1);
        // print_r($configRs->toArray());die;
        return View::make('front.user.usescore')
            ->with('integral', $integral)
            ->with('totalIntegral', $totalIntegral)
            ->with('configRs', $configRs);
    }

    /**
     * 用户健康云积分增减操作
     **/
    public function Integraldo()
    {
        $iUid = $iUserId = Session::get('userid');
        $oInput = Input::all();
        if (!Input::has('obj_id') || !Input::has('obj_type')) {
            echo $this->formatRet(false, "传递参数不正确");
            exit;
        }
        $IntegralLog = new IntegralLog();
        $res = $IntegralLog->updateIntegral($iUid, $oInput['obj_id'], $oInput['obj_type']);
        if ($res['isok']) {
            echo $this->formatRet(true, "恭喜您，操作成功！");
            exit;
        } else {
            echo $this->formatRet(false, $res['msg'] ? $res['msg'] : "对不起，操作失败。");
            exit;
        }
    }

}
