<?php
/**
 *                             _ooOoo_
 *                            o8888888o
 *                            88" . "88
 *                            (| -_- |)
 *                            O\  =  /O
 *                         ____/`---'\____
 *                       .'  \\|     |//  `.
 *                      /  \\|||  :  |||//  \
 *                     /  _||||| -:- |||||-  \
 *                     |   | \\\  -  /// |   |
 *                     | \_|  ''\---/''  |   |
 *                     \  .-\__  `-`  ___/-. /
 *                   ___`. .'  /--.--\  `. . __
 *                ."" '<  `.___\_<|>_/___.'  >'"".
 *               | | :  `- \`.;`\ _ /`;.`/ - ` : | |
 *               \  \ `-.   \_ __\ /__ _/   .-` /  /
 *          ======`-.____`-.___\_____/___.-`____.-'======
 *                             `=---='
 *          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *                     佛祖保佑        永无BUG
 *            佛曰:
 *                   写字楼里写字间，写字间里程序员；
 *                   程序人员写程序，又拿程序换酒钱。
 *                   酒醒只在网上坐，酒醉还来网下眠；
 *                   酒醉酒醒日复日，网上网下年复年。
 *                   但愿老死电脑间，不愿鞠躬老板前；
 *                   奔驰宝马贵者趣，公交自行程序员。
 *                   别人笑我忒疯癫，我笑自己命太贱；
 *                   不见满街漂亮妹，哪个归得程序员？
 */

/**
 * 会议管理操作接口类
 * Class ApiMettingController
 */
class ApiMeetingController extends BaseController
{
    CONST  PAGESIZE = 10;
    public $iUserid = 0;

    public function __construct()
    {
        $this->beforeFilter('apilogin');
        global $iUserId;
        $this->iUserId = &$iUserId;
    }

    /**
     * 获取当天会议列表
     * @return mixed
     * [get] /apimeeting/meeting-list?page=1&pagesize=10&token=
     */
    public function getMeetingList()
    {
        global $iUserId;

        $iPage = Input::get('page', 1);
        $iPagesize = Input::get('pagesize', self::PAGESIZE);

        $date = Input::get('date', date('Y-m-d'));
        $type = (int)Input::get('type', 0);
        $isme = (int)Input::get('isme', 0);

        // 查询当天会议列表
        $oList = MeetingManageSign::select([
	            'meeting_manage.type',
	            'meeting_manage.title',
	            'meeting_manage_sign.*',
	            'province.name as province_name',
	            'city.name as city_name'
        	])
            ->join(DB::raw('meeting_manage'), 'meeting_manage.id', '=', 'meeting_manage_sign.meeting_manage_id')
            ->leftJoin(DB::raw('hospital province'), 'meeting_manage_sign.province', '=', 'province.id')
            ->leftJoin(DB::raw('hospital city'), 'meeting_manage_sign.city', '=', 'city.id')
            ->where("meeting_manage_sign.sign_date", '=', $date)
            ->where(function ($query) use ($type, $isme, $iUserId) {
                if ($isme) {
                    $query->where('meeting_manage_sign.user_id', $iUserId);
                }else{
                    $query->where('meeting_manage.type', $type);
                }
            })
            ->orderBy('meeting_manage_sign.sign_date', 'asc')
            ->skip(($iPage - 1) * $iPagesize)
            ->take($iPagesize)
            ->get();

        $aList = [];
        foreach ($oList as $item) {
            $aList[] = [
                "id" => $item->id,
                //"thumb" => null,
                "type" => $item->type,
                "title" => $item->title,
                "user_id" => $item->user_id,
                "province_name" => $item->province_name,
                "city_name" => $item->city_name,
                "meeting_manage_id" => $item->meeting_manage_id,
                //"datetime" => date('Y-m-d H:i', strtotime($item->datetime)),
                "sign_date" => $item->sign_date,
                "start_time" => date('H:i', strtotime(date('Y-m-d ') . $item->start_time)),
                "end_time" => date('H:i', strtotime(date('Y-m-d ') . $item->end_time)),
                "province" => $item->province,
                "city" => $item->city,
                "hospital" => $item->hospital,
                "chairman" => $item->chairman,
                "isupload" => $item->isupload,
                "created_at" => $item->created_at->format('Y-m-d H:i'),
                "updated_at" => $item->updated_at->format('Y-m-d H:i'),
                "status" => $item->status,
            ];
        }

        return Response::make(['status' => true, 'msg' => 'ok', 'data' => $aList]);
    }

    /**
     * 获取每月的会议统计
     * @return mixed
     * @url /apimeeting/month-meeting?token=
     */
    public function getMonthMeeting()
    {
        global $iUserId;

        $year = Input::get('year', date('Y'));
        $month = Input::get('month', date('m'));
        $type = (int)Input::get('type', 0);
        $isme = Input::get('isme', 0);

        $firstday = "{$year}-{$month}-01";
        $lastday = date('Y-m-d', strtotime("$firstday +1 month -1 day"));

        $list = MeetingManageSign::select([DB::raw('count(meeting_manage_sign.id) as num, ' . $type . ' as type, DATE_FORMAT(meeting_manage_sign.sign_date, "%d") as day')])
            ->leftJoin('meeting_manage', 'meeting_manage.id', '=', 'meeting_manage_sign.meeting_manage_id')
            ->where('meeting_manage_sign.sign_date', '>=', $firstday)
            ->where('meeting_manage_sign.sign_date', '<=', $lastday)
            ->where(function ($query) use ($isme, $iUserId, $type) {
                if ($isme) {
                    $query->where('meeting_manage_sign.user_id', $iUserId);
                } else {
                    $query->where('meeting_manage.type', $type);
                }
            })
            ->groupBy('day')
            ->get()->toArray();

        return Response::make(['status' => true, 'msg' => 'ok', 'data' => $list]);
    }

    /**
     * 获取会议详情
     * @return mixed
     * @url /apimeeting/meeting-info?id=7&token=
     */
    public function getMeetingInfo()
    {
        $iUserid = $this->iUserId;

        $id = Input::get('id', 0);
        $more = Input::get('more', 0);

        $info = MeetingManageSign::select([
                'meeting_manage_sign.*',
                'meeting_manage.type',
                'meeting_manage.title',
                'province.name as province_name',
                'city.name as city_name'
            ])
            ->leftJoin('meeting_manage', 'meeting_manage.id', '=', 'meeting_manage_sign.meeting_manage_id')
            ->leftJoin(DB::raw('hospital province'), 'meeting_manage_sign.province', '=', 'province.id')
            ->leftJoin(DB::raw('hospital city'), 'meeting_manage_sign.city', '=', 'city.id')
            ->where('meeting_manage_sign.id', $id)->first();

        if ($info) {
            $info = $info->toArray();
            unset($info['datetime'],$info['date_time']);

            // 预定义冗余字段
            // $info['video'] = $info['photo'] = $info['signin'] = $info['meals'] = [];

            $info['start_time'] = date('H:i', strtotime(date('Y-m-d ') . $info['start_time']));
            $info['end_time'] = date('H:i', strtotime(date('Y-m-d ') . $info['end_time']));

            // 会议是否可编辑，会议开始时间24小时内不能修改会议信息
            $info['isedit'] = strtotime($info['sign_date'] . ' ' . $info['start_time']) > strtotime('+1 day') ? 1 : 0;
            // 是否为我创建的
            $info['isown'] = ($iUserid == $info['user_id']);
            // 获取 subject
            $info['subject'] = MeetingManageSubject::where('meeting_sign_id', $id)->get()->toArray();

            foreach ($info['subject'] as &$subject) {
                $subject['start_time'] = date('H:i', strtotime(date('Y-m-d ') . $subject['start_time']));
                $subject['end_time'] = date('H:i', strtotime(date('Y-m-d ') . $subject['end_time']));
                if ($more && $info['isown']) {
                    // 预定义冗余字段
                    // $subject['agreement'] = $subject['bank'] = $subject['idcard'] = [];
                    // 获取附件信息
                    $attachlist = MeetingManageAttach::select(['id', 'type', 'thumb', 'filepath', 'ext', 'created_at'])
                        ->where('meeting_subject_id', $subject['id'])
                        ->get()->toArray();
                    foreach ($attachlist as $attach) {
                        if (!isset($subject[$attach['type']]) || is_array($subject[$attach['type']])) {
                            $subject[$attach['type']][] = $attach;
                        }
                    }
                }
            }

            // 是否需要更多数据
            if ($more && $info['isown']) {
                $attachlist = MeetingManageAttach::select(['id','type','thumb','filepath','ext','created_at'])
                    ->where('meeting_sign_id', $id)
                    ->where('meeting_subject_id', 0)
                    ->get()->toArray();
                foreach ($attachlist as $attach) {
                    if (!isset($info[$attach['type']]) || is_array($info[$attach['type']])) {
                        $info[$attach['type']][] = $attach;
                    }
                }
            }
        }

        return Response::make(['status' => true, 'msg' => 'ok', 'data' => $info]);
    }

    /**
     * 创建会议
     * @param
     * {
     * "type": 0,
     * "start_time": "2018-09-06 08:00:00",
     * "province": 110000,
     * "city": 110101,
     * "hospital": "测试医院",
     * "chairman": "张三",
     * "subject": [
     * {
     * "subject": "第一主题",
     * "speaker": "第一讲者"
     * },
     * {
     * "subject": "第二主题",
     * "speaker": "第二讲者"
     * }
     * ]
     * }
     * @return mixed
     * @url /apimeeting/sign?token=
     */
    public function postSignMeeting()
    {
        global $iUserId;

        $data = Input::all();

        // @file_put_contents(app_path().'/storage/sign-request.log', PHP_EOL . '--------------' . PHP_EOL . json_encode($data) . PHP_EOL. PHP_EOL, FILE_APPEND);

        // 兼容 ios 框架提交 json 异常问题
        if (isset($data['jsonStr'])) {
            $data = json_decode($data['jsonStr'], true);
        }

        // 表单验证
        $validator = Validator::make(
            $data,
            [
                'meeting_manage_id' => 'required',
                'sign_date' => 'required',
                'start_time' => 'required',
                'end_time' => 'required',
                'province' => 'required',
                'city' => 'required',
                'hospital' => 'required',
                //'chairman' => 'required',
            ],
            [
                'meeting_manage_id.required' => '会议不能为空',
                'sign_date.required' => '会议日期不能为空',
                'start_time.required' => '会议时间不能为空',
                'end_time.required' => '会议时间不能为空',
                'province.required' => '会议地点所在省不能为空',
                'city.required' => '会议地点所在市不能为空',
                'hospital.required' => '医院不能为空',
                //'chairman.required' => '会议主席不能为空',
            ]
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $message) {
                return Response::make(['status' => false, 'msg' => $message]);
            }
        }

        // 校验会议类型 id 是否存在
        if (!MeetingManage::where('id', $data['meeting_manage_id'])->count()){
            return Response::make(['status' => false, 'msg' => '会议类型id有误']);
        }

        $info = MeetingManageSign::where('user_id', $iUserId)
            ->where('meeting_manage_id', $data['meeting_manage_id'])
            ->where('sign_date', '=', $data['sign_date'])
            ->first();
        if ($info) {
            // 去除每天只能报名一次的限制
            //return Response::make(['status' => false, 'msg' => '抱歉，今天你已经报过名了']);
        }

        // 保存数据
        $info = new MeetingManageSign($data);
        $info->user_id = $iUserId;
        $info->save();

        if ($info->id) {
            // 保存主题信息
            if (isset($data['subject'])) {
                foreach ($data['subject'] as $item) {
                    $subject = new MeetingManageSubject();
                    $subject->subject = $item['subject'];
                    $subject->start_time = $item['start_time'];
                    $subject->end_time = $item['end_time'];
                    $subject->speaker = $item['speaker'];
                    $subject->meeting_sign_id = $info->id;
                    $subject->save();
                }
            }
            return Response::make(['status' => true, 'msg' => '报名成功']);
        }

        return Response::make(['status' => false, 'msg' => '报名失败']);
    }

    /**
     * 编辑报名的会议信息
     * @return mixed
     * @url [post] /apimeeting/edit-sign?token=
     */
    public function postEditSign()
    {
        global $iUserId;

        $data = Input::all();

        // 兼容 ios 框架提交 json 异常问题
        if (isset($data['jsonStr'])) {
            $data = json_decode($data['jsonStr'], true);
        }

        // 表单验证
        $validator = Validator::make(
            $data,
            [
                'id' => 'required',
                'sign_date' => 'required',
                'start_time' => 'required',
                'end_time' => 'required',
                'province' => 'required',
                'city' => 'required',
                'hospital' => 'required',
                //'chairman' => 'required',
            ],
            [
                'id.required' => '报名信息 id 不能为空',
                'sign_date.required' => '会议日期不能为空',
                'start_time.required' => '会议时间不能为空',
                'end_time.required' => '会议时间不能为空',
                'province.required' => '会议地点所在省不能为空',
                'city.required' => '会议地点所在市不能为空',
                'hospital.required' => '医院不能为空',
                //'chairman.required' => '会议主席不能为空',
            ]
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $message) {
                return Response::make(['status' => false, 'msg' => $message]);
            }
        }

        $id = $data['id'];
        $subjects = isset($data['subject']) ? $data['subject'] : [];
        unset($data['id'], $data['subject']);

        $info = MeetingManageSign::where('user_id', $iUserId)
            ->where('id', $id)
            ->first();
        if (empty($info)) {
            return Response::make(['status' => false, 'msg' => '未找到待修改的报名信息']);
        }

        $allow = ['id', 'sign_date', 'start_time', 'end_time', 'province', 'city', 'hospital', 'chairman'];
        foreach ($data as $name => $value) {
            if (in_array($name, $allow)) {
                $info->$name = $value;
            }
        }
        $info->save();

        if (!empty($subjects)) {
            MeetingManageSubject::where('meeting_sign_id', $info->id)->delete();
            foreach ($subjects as $item) {
                $subject = new MeetingManageSubject();
                $subject->subject = $item['subject'];
                $subject->speaker = $item['speaker'];
                $subject->start_time = $item['start_time'];
                $subject->end_time = $item['end_time'];
                $subject->meeting_sign_id = $info->id;
                $subject->save();
            }
        }

        return Response::make(['status' => true, 'msg' => '修改成功']);
    }

    /**
     * 获取安排会议时的某天会议列表
     * @return mixed
     * @url http://cdcma.weui.net/apimeeting/list?token=
     */
    public function getList()
    {
        global $iUserId;

        $date = Input::get('date', date('Y-m-d'));

        $oList = MeetingManage::select('*')->where(function ($query) use ($date) {
            $query->where('start_date', '<=', $date)->where('end_date', '>=', $date);
        })->get();

        $meetingIds = $signList = [];
        foreach ($oList as $item) {
            $meetingIds[] = $item->id;
        }
        if (!empty($meetingIds)) {
            $signList = MeetingManageSign::select(['id', 'meeting_manage_id'])
                ->whereIn('meeting_manage_id', $meetingIds)
                ->where('user_id', $iUserId)
                ->where('sign_date', $date)
                ->lists('id', 'meeting_manage_id');
        }

        // 处理是否报名字段
        $list = [];
        foreach ($oList as $item) {
            $list[] = [
                "id" => $item->id,
                "type" => $item->type,
                "title" => $item->title,
                "start_date" => $item->start_date,
                "end_date" => $item->end_date,
                "is_sign" => isset($signList[$item->id]) ? 1 : 0,
                "status" => $item->status,
            ];
        }

        return Response::make(['status' => true, 'msg' => 'ok', 'data' => $list]);
    }

    /**
     * 安排会议中已报名的会议列表
     * @return mixed
     * @url /apimeeting/sign-meeting-list?date=2018-09-06&page=1&pagesize=10token=
     */
    public function getSignMeetingList()
    {
        global $iUserId;

        $iPage = Input::get('page', 1);
        $iPagesize = Input::get('pagesize', self::PAGESIZE);
        $date = Input::get('date', date('Y-m-d'));

        $oList = MeetingManageSign::select([
                'meeting_manage_sign.*',
                'meeting_manage.type',
                'meeting_manage.title',
                'province.name as province_name',
                'city.name as city_name'
            ])
            ->leftJoin('meeting_manage', 'meeting_manage.id', '=', 'meeting_manage_sign.meeting_manage_id')
            ->leftJoin(DB::raw('hospital province'), 'meeting_manage_sign.province', '=', 'province.id')
            ->leftJoin(DB::raw('hospital city'), 'meeting_manage_sign.city', '=', 'city.id')
            ->where('meeting_manage_sign.sign_date', '>=', $date)
            ->where('meeting_manage_sign.sign_date', '<=', $date)
            ->where('meeting_manage_sign.user_id', '=', $iUserId)
            ->skip(($iPage - 1) * $iPagesize)
            ->take($iPagesize)
            ->get();

        $aList = [];
        foreach ($oList as $item) {
            $aList[] = [
                "id" => $item->id,
                //"thumb" => null,
                "type" => 0,
                "title" => $item->title,
                "province_name" => $item->province_name,
                "city_name" => $item->city_name,
                "meeting_manage_id" => $item->meeting_manage_id,
                "sign_date" => $item->sign_date,
                "start_time" => date('H:i', strtotime(date('Y-m-d ') . $item->start_time)),
                "end_time" => date('H:i', strtotime(date('Y-m-d ') . $item->end_time)),
                "province" => $item->province,
                "city" => $item->city,
                "hospital" => $item->hospital,
                "chairman" => $item->chairman,
                "isupload" => $item->isupload,
                "created_at" => $item->created_at->format('Y-m-d H:i'),
                "updated_at" => $item->updated_at->format('Y-m-d H:i'),
                "status" => $item->status,
            ];
        }

        return Response::make(['status' => true, 'msg' => 'ok', 'data' => $aList]);
    }

    /**
     * 我的会议接口
     * @return mixed
     * @url /apimeeting/my-meeting-list?page=1&pagesize=10token={{token}}
     */
    public function getMyMeetingList()
    {
        global $iUserId;

        $iPage = Input::get('page', 1);
        $iPagesize = Input::get('pagesize', self::PAGESIZE);

        $oList = MeetingManageSign::select([
                'meeting_manage_sign.*',
                'meeting_manage.type','meeting_manage.title','meeting_manage.start_date','meeting_manage.end_date',
                'province.name as province_name',
                'city.name as city_name'
            ])
            ->leftJoin('meeting_manage', 'meeting_manage.id', '=', 'meeting_manage_sign.meeting_manage_id')
            ->leftJoin(DB::raw('hospital province'), 'meeting_manage_sign.province', '=', 'province.id')
            ->leftJoin(DB::raw('hospital city'), 'meeting_manage_sign.city', '=', 'city.id')
            ->where('meeting_manage_sign.user_id', '=', $iUserId)
            ->skip(($iPage - 1) * $iPagesize)
            ->take($iPagesize)
            ->get();

        $aList = [];
        foreach ($oList as $item) {
            $aList[] = [
                "id" => $item->id,
                //"thumb" => null,
                "type" => $item->type,
                "title" => $item->title,
                "province_name" => $item->province_name,
                "city_name" => $item->city_name,
                "meeting_manage_id" => $item->meeting_manage_id,
                "sign_date" => $item->sign_date,
                "start_time" => date('H:i', strtotime(date('Y-m-d ') . $item->start_time)),
                "end_time" => date('H:i', strtotime(date('Y-m-d ') . $item->end_time)),
                "province" => $item->province,
                "city" => $item->city,
                "hospital" => $item->hospital,
                "chairman" => $item->chairman,
                "isupload" => $item->isupload,
                "created_at" => $item->created_at->format('Y-m-d H:i'),
                "updated_at" => $item->updated_at->format('Y-m-d H:i'),
                "status" => $item->status,
            ];
        }

        return Response::make(['status' => true, 'msg' => 'ok', 'data' => $aList]);
    }

    /**
     * 上传资料
     * @return mixed
     */
    public function postMeetingDataUpload()
    {
        set_time_limit(0);
        global $iUserId;

        // 文件上传类型
        $types = [
            'video'     => '现场讲课视频',
            'photo'     => '现场照片',
            'schedule'  => '会议日程',
            'signin'    => '签到表',
            'meals'      => '餐费票照片',
            // 讲者资料
            'agreement' => '专家服务协议',
            'bank'      => '银行卡正面照片',
            //'speaker'   => '讲者身份证正反面照片',
        ];
        $data = Input::all();

        // 表单验证
        $validator = Validator::make(
            $data,
            [
                'type' => 'required',
                'sign_id' => 'required',
            ],
            [
                'type.required' => '上传文件类型不能为空',
                'sign_id.required' => '报名信息 id 不能为空',
            ]
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $message) {
                return Response::make(['status' => false, 'msg' => $message]);
            }
        }

        // 强制根类型
        if (in_array($data['type'], array('video', 'photo', 'signin', 'meals', 'schedule'))) {
            $data['subject_id'] = 0;
        }

        if (!Input::hasFile('file')) {
            return Response::make(['status' => false, 'msg' => '待上传的文件不可为空']);
        }

        // 检查报名 id 是否为自己报名的会议
        $sign = MeetingManageSign::where('id', $data['sign_id'])->first();
        if ($sign['user_id'] != $iUserId) {
            return Response::make(['status' => false, 'msg' => '您只能给自己的会议上传资料']);
        }

        $file = Input::file('file');
        $sExt = $file->getClientOriginalExtension();//扩展名

        //处理文件
        $sFileName = date('YmdHis') . rand(1, 9999);
        $sNewName = $sFileName . '.' . $sExt;//新文件名
        $sFilePath = '/upload/meeting/' . date('Ymd') . '/';
        $output = array();

        mkdirs(public_path() . $sFilePath);
        $file->move(public_path() . $sFilePath, $sNewName);

        // 9月14日更新：会议上传支持件：专家服务协议、签到表、讲者身份证正反面复印件、银行卡正面复印件（在复印件上写明开户行名称和专家姓名）、会议日程、现场照片、现场讲课视频5-10秒（须包含讲者，听众和PPT）
        if (in_array($sExt, array('jpg', 'gif', 'png', 'jpeg'))) {
            /*if (!in_array($data['type'], ['photo', 'signin'])) {
                return Response::make(['status' => false, 'msg' => '只允许照片与签到信息为图片']);
            }*/
        } else if(in_array($sExt, array('mp4', 'avi', 'wmv', '3gp', 'mov'))) {
            if (!in_array($data['type'], ['video'])) {
                return Response::make(['status' => false, 'msg' => '只允许现场讲课录制为视频格式']);
            }
            $ffmpg = VideoProduceService::$sFFmpegCommand;
            //$ffmpg = '/usr/local/bin/ffmpeg';
            $realFilePath = ROOT_DIR . $sFilePath;
            // 非 mp4 时转码
            if (strtolower($sExt) != 'mp4') {
                $sNewName = $sFileName . '.mp4';
                $sCommond = $ffmpg . " -i '{$realFilePath}{$sFileName}.{$sExt}' -y -c:v copy '{$realFilePath}{$sNewName}'";
                exec($sCommond, $output);
            }

            $sCommond = $ffmpg . " -i '{$realFilePath}{$sNewName}' -y -r 1 -t 1 '{$realFilePath}{$sFileName}.jpg'";
            exec($sCommond, $output);
            // 缩略图
            $thumb = $sFilePath . $sFileName . '.jpg';
        } else {
            return Response::make(['status' => false, 'msg' => $sExt . '类型上传不支持']);
        }

        // 文件路径
        $filePath = $sFilePath . $sNewName;

        $type = strtolower($data['type']);
        // 入库记录
        $record = [
            'meeting_sign_id' => $data['sign_id'],
            'type' => ($type == 'speaker' ? 'idcard' : $type),
            'meeting_subject_id' => isset($data['subject_id']) ? $data['subject_id'] : 0,
            'ext' => $sExt,
            'thumb' => isset($thumb) ? url($thumb) : '',
            'filepath' => url($filePath)
        ];
        $oAttach = new MeetingManageAttach($record);

        $oAttach->user_id = $iUserId;
        $oAttach->save();

        $record['id'] = $oAttach->id;
        // 检测是否全部上传完成
        $list = MeetingManageAttach::select('type')->where('meeting_sign_id', $data['sign_id'])->groupBy('type')->lists('type');
        $intersection = array_diff(array_keys($types) + ['idcard'], array_values($list));

        $error = [];
        foreach ($types as $key => $value) {
            if (in_array($key, $intersection)) {
                $error[] = $value . '不能为空';
            }
        }
        $record['finish'] = empty($error) ? true : false;
        $record['error'] = $error;

        // 处理是否上传资料标识
        $sign = MeetingManageSign::where('id', $data['sign_id'])->where('isupload', 0)->first();
        if ($sign && empty($error)) {
            $sign->isupload = 1;
            $sign->save();
        }

        return Response::make([
            'status' => true,
            'msg' => '上传成功',
            'data' => $record
        ]);
    }

    /**
     * 删除附件
     * @return mixed
     */
    public function getDelDataFile()
    {
        global $iUserId;

        $data = Input::all();
        // 表单验证
        $validator = Validator::make(
            $data,
            [
                'id' => 'required'
            ],
            [
                'id.required' => '附件 id 不能为空'
            ]
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $message) {
                return Response::make(['status' => false, 'msg' => $message]);
            }
        }

        $info = MeetingManageAttach::where('id', $data['id'])->where('user_id', $iUserId)->first();
        if ($info) {
            $thumb = parse_url($info->thumb, PHP_URL_PATH);
            if (file_exists(ROOT_DIR . $thumb)) {
                @unlink(ROOT_DIR . $thumb);
            }
            $filepath = parse_url($info->filepath, PHP_URL_PATH);
            if (file_exists(ROOT_DIR . $filepath)) {
                @unlink(ROOT_DIR . $filepath);
            }
            $info->delete();
        }

        return Response::make(['status' => true, 'msg' => '附件删除完成']);
    }

    /**
     * 获取资源列表
     * @return mixed
     */
    public function getMaterialList()
    {
        $iPage = Input::get('page', 1);
        $iPagesize = Input::get('pagesize', self::PAGESIZE);

        $list = MeetingManageMaterial::orderBy('id', 'desc')
            ->skip(($iPage - 1) * $iPagesize)
            ->take($iPagesize)
            ->get()->toArray();

        foreach ($list as &$item) {
            $item['filepath'] = url($item['filepath']);
        }

        return Response::make(['status' => true, 'msg' => 'ok', 'data'=>$list]);
    }

    /**
     * 发送附件信息
     * @return mixed
     */
    public function postSendMaterial()
    {
        global $iUserId;
        set_time_limit(0);

        $id = Input::get('id', 0);
        $mail = Input::get('mail', '');

        if (!$id) {
            return Response::make(['status' => false, 'msg' => '资料标识不能为空']);
        }
        $pattern = "/^([0-9A-Za-z\\-_\\.]+)@([0-9a-z]+\\.[a-z]{2,3}(\\.[a-z]{2})?)$/i";
        if ( !preg_match( $pattern, $mail ) )
        {
            return Response::make(['status' => false, 'msg' => '邮箱地址不合法']);
        }

        $info = MeetingManageMaterial::where('id', $id)->first();
        if (empty($info->id)) {
            return Response::make(['status' => false, 'msg' => '资料文件不存在']);
        }

        // 获取用户姓名
        $user_name = User::where('id', $iUserId)->pluck('user_name');
        $body = '';
        $attach = null;
        switch ($info->type) {
            case 0:
                $body = "PPT文稿（{$info->title}）在附件，请查看。";
                $attach = array('path' => url($info->filepath), 'name'=>$info->title, 'mime'=>'application/vnd.ms-powerpoint');
                break;
            case 1:
                $url = url($info->filepath);
                $body = "视频（{$info->title}）下载链接为：<a href=\"{$url}\">{$url}</a>，请点击下载。";
                break;
        }

        $subject = '【会议管理】会议资料';
        // 发送邮件
        Mail::send('emails.meeting.material', array('user_name'=>$user_name, 'body'=>$body), function($message) use ($mail, $subject, $attach)
        {
            $message->to($mail, $mail)->subject($subject);
            // 如果附件不为空时
            if (!is_null($attach)) {
                if (is_array($attach)) {
                    $message->attach($attach['path'], array('as' => $attach['name'], 'mime' => $attach['mime']));
                } else {
                    $message->attach($attach);
                }
            }
        });

        return Response::make(['status' => true, 'msg' => '文件已发送']);
    }
}