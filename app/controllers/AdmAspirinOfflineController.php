<?php

class AdmAspirinOfflineController extends BaseController {
	
	CONST  PAGESIZE = 10;

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	//线下会议列表
	public function getList()
	{
		$sTitle = Input::get('title');
		if(!empty($sTitle)){
			$oOffline = AspirinOffline::where('offline_title','like','%'.$sTitle.'%')->orderBy("created_at","desc")->paginate(self::PAGESIZE);
		}else{
			$oOffline = AspirinOffline::orderBy("created_at","desc")->paginate(self::PAGESIZE);
		}
		Return View::make('admin.aspirin.offline.list')->with('oOffline',$oOffline);
	}

	//线下会议添加
	public function getAdd()
	{
		Return View::make('admin.aspirin.offline.add');
	}

	public function postAddDo()
	{
		$aInput = Input::all();
		$rule = array(
			'offline_title'	        => 'required',
			'offline_place'	        => 'required',
			'offline_start_time'	=> 'required',
			'offline_end_time'	    => 'required',
			'apply_start_time'	    => 'required',
			'apply_end_time'	    => 'required',
			'offline_content'	    => 'required',
			'offline_least_gold'	=> 'required',
			'offline_count'	        => 'required'
		);
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
			return $this->showMessage('请填写必填字段');
		}
		if($aInput['offline_start_time'] < $aInput['apply_end_time']){
			return $this->showMessage('报名截止时间不得晚于会议开始时间');
		}
		$aOffline = array(
			'offline_title'       => trim($aInput['offline_title']),
			'offline_place'       => trim($aInput['offline_place']),
			'offline_start_time'  => trim($aInput['offline_start_time']),
			'offline_end_time'    => trim($aInput['offline_end_time']),
			'apply_start_time'    => trim($aInput['apply_start_time']),
			'apply_end_time'      => trim($aInput['apply_end_time']),
			'offline_content'     => $aInput['offline_content'],
			'offline_least_gold'  => intval($aInput['offline_least_gold']),
			'offline_count'       => intval($aInput['offline_count']),
			'created_at'          => date('Y-m-d H:i:s',time())
		);
		$oOffline = new AspirinOffline($aOffline);
		$oOffline->save();
		return $this->showMessage('添加成功','/admaspirinoffline/list');
	}

	//线下会议修改
	public function getEdit($iId)
	{
		$oOffline = AspirinOffline::find($iId);
		if($oOffline){
			Return View::make('admin.aspirin.offline.edit')->with('oOffline',$oOffline)->with('iId',$iId);
		}else{
			return $this->showMessage('该内容不存在','/admaspirinoffline/list');
		}
	}

	public function postEditDo(){
		$iId = Input::get('iId');
		$aInput = Input::all();
		$rule = array(
			'offline_title'	        => 'required',
			'offline_place'	        => 'required',
			'offline_start_time'	=> 'required',
			'offline_end_time'	    => 'required',
			'apply_start_time'	    => 'required',
			'apply_end_time'	    => 'required',
			'offline_content'	    => 'required',
			'offline_least_gold'	=> 'required',
			'offline_count'	        => 'required'
		);
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
			return $this->showMessage('请填写必填字段');
		}
		if($aInput['offline_start_time'] < $aInput['apply_end_time']){
			return $this->showMessage('报名截止时间不得晚于会议开始时间');
		}
		$oOffline = AspirinOffline::find($iId);
		if($oOffline){
			$oOffline->offline_title       = trim($aInput['offline_title']);
			$oOffline->offline_place       = trim($aInput['offline_place']);
			$oOffline->offline_start_time  = trim($aInput['offline_start_time']);
			$oOffline->offline_end_time    = trim($aInput['offline_end_time']);
			$oOffline->apply_start_time    = trim($aInput['apply_start_time']);
			$oOffline->apply_end_time      = trim($aInput['apply_end_time']);
			$oOffline->offline_content     = $aInput['offline_content'];
			$oOffline->offline_least_gold  = intval($aInput['offline_least_gold']);
			$oOffline->offline_count       = intval($aInput['offline_count']);
			$oOffline->save();
			return $this->showMessage('修改成功','/admaspirinoffline/list');
		}else{
			return $this->showMessage('修改失败','/admaspirinoffline/list');
		}
	}

	//线下会议删除
	public function getDelete($iId){
		$oOffline = AspirinOffline::find($iId);
		if($oOffline){
			$oOffline->delete();
			$oOffline->apply()->delete();
			return $this->showMessage('删除成功','/admaspirinoffline/list');
		}else{
			return $this->showMessage('删除失败','/admaspirinoffline/list');
		}
	}
	//线下会议报名列表
	public function getApplyUser($iId){
		$oOffline = AspirinOffline::find($iId);
		if($oOffline){
			$oApplyInfo = AspirinOfflineApply::leftJoin('user','user.id','=','aspirin_offline_apply.user_id')
				->select('user.user_cwid','user.user_name','aspirin_offline_apply.*')
				->where('offline_id',$iId)
				->orderby('created_at','desc')
				->paginate(self::PAGESIZE);
			Return View::make('admin.aspirin.offline.userapply')->with('oApplyInfo',$oApplyInfo);
		}else{
			return $this->showMessage('该会议不存在','/admaspirinoffline/list');
		}
	}
	
}