<?php

class AdmMeetingController extends BaseController {
	
	CONST  PAGESIZE = 10;

	/*
	|--------------------------------------------------------------------------
	| 自助会议平台
	|--------------------------------------------------------------------------
	|zgq
	*/

	//会议列表页
	public function MeetingList()
	{   
		$sDate = date("Y-m-d H:i:s");
		$oMeeting = Meeting::orderBy('meeting_start_time')->where('meeting_start_time','>',$sDate);
		$input =Input::all();
		$aLink = array();
	    if(isset($input['expert_name'])&&$input['expert_name']){
	        $oMeeting = $oMeeting->where('expert_name','LIKE','%'.$input['expert_name'].'%');
		    $aLink['expert_name'] = $input['expert_name'];
		}
	    if(isset($input['user_cwid'])&&$input['user_cwid']){
	    	$oUser = User::where('user_cwid',$input['user_cwid'])->first();
	    	if(count($oUser)){
				$iId = $oUser ->id;
				$oMeeting = $oMeeting->where('representative','=',$iId);
	    	}
			$aLink['user_cwid'] = $input['user_cwid'];
		}
		if(isset($input['invite_code'])&&$input['invite_code']){
	    	$oUser = User::where('invite_code',$input['invite_code'])->first();
	    	if(count($oUser)){
				$iId = $oUser ->id;
				$oMeeting = $oMeeting->where('representative','=',$iId);
	    	}
			$aLink['invite_code'] = $input['invite_code'];
		}
		if(isset($input['creator_name'])&&$input['creator_name']){
			$oMeeting = $oMeeting->where('creator_name','LIKE','%'.$input['creator_name'].'%');
			$aLink['creator_name'] = $input['creator_name'];
		}
		if(isset($input['creator_tel'])&&$input['creator_tel']){
	  	    $oMeeting = $oMeeting->where('creator_tel','LIKE','%'.$input['creator_tel'].'%');
			$aLink['creator_tel'] = $input['creator_tel'];
		}
		if(isset($input['creator_email'])&&$input['creator_email']){
			$oMeeting = $oMeeting->where('creator_email','LIKE','%'.$input['creator_email'].'%');
			$aLink['creator_email'] = $input['creator_email'];
		}
		$oMeeting = $oMeeting->paginate(self::PAGESIZE);
		return View::make('admin.meeting.meetinglist')->with('aLink',$aLink)->with('oMeeting',$oMeeting);
	}

	//打开会议--or---关闭会议
	public function OpenMeeting($iId)
	{  
		$oMeeting = Meeting::find($iId);
		if($oMeeting ->status == 1){
			$oMeeting ->status = 0;
			$oMeeting ->save();
		}else{
			$oMeeting ->status = 1;
			$oMeeting ->save();
		}
		return Redirect::to('/admmeeting');
	}

	//会议详情
	public function MeetingDetail($iId)
	{
		$oMeeting = Meeting::find($iId);
		$oMeetingOrder = MeetingOrder::where('meeting_id',$iId)->get();
		return View::make('admin.meeting.meetingdetail')->with('oMeetingOrder',$oMeetingOrder)->with('oMeeting',$oMeeting);
	}

	//自助会议平台---会议导出
	public function MeetingMessage($iMeetingId)
	{
		// $sMeetingMessage = "会议id,会议主题,专家姓名,专家单位,专家科室,创建人姓名,创建人手机号,创建人邮箱,预约会议人数,会议参会人数,会议开始时间,会议结束时间,会议实际时长,预约人姓名,预约人手机号,预约人邮箱,预约人医院,预约人科室,"."\n";
		$sMeetingMessage = "会议id,会议主题,专家姓名,专家单位,专家科室,创建人姓名,创建人手机号,创建人邮箱,预约会议人数,会议参会人数,";
		$sMeetingMessage .= "会议开始时间,会议结束时间,会议实际时长,预约人姓名,预约人手机号,预约人邮箱,预约人医院,预约人科室,"."\n";
		$oMeeting = Meeting::find($iMeetingId);
		$sMeetingMessage .= $oMeeting ->id.',';
		$sMeetingMessage .= $oMeeting ->meeting_title.',';
		$sMeetingMessage .= $oMeeting ->expert_name.',';
		$sMeetingMessage .= $oMeeting ->expert_company.',';
		$sMeetingMessage .= $oMeeting ->exper_subject.',';
		$sMeetingMessage .= $oMeeting ->creator_name.',';
		$sMeetingMessage .= '`'.$oMeeting ->creator_tel.',';
		$sMeetingMessage .= '`'.$oMeeting ->creator_email.',';
		$sMeetingMessage .= $oMeeting ->order_count.',';
		$sMeetingMessage .= '会议部统计'.',';
		$sMeetingMessage .='`'. $oMeeting ->meeting_start_time.',';
		$sMeetingMessage .= '`'.$oMeeting ->meeting_end_time.',';
		$sMeetingMessage .= '会议部确认'.',';
		//预约成功的记录
		$iMeetingId = $oMeeting ->id;
		$oMeetingOrder = MeetingOrder::where('meeting_id',$iMeetingId)->where('order_type_id',2)->first();
		if($oMeetingOrder){
			$sMeetingMessage .= $oMeetingOrder ->order_name.',';
			$sMeetingMessage .= '`'.$oMeetingOrder ->order_tel.',';
			$sMeetingMessage .= '`'.$oMeetingOrder ->order_email.',';
			$sMeetingMessage .= $oMeetingOrder ->order_hospital.',';
			$sMeetingMessage .= $oMeetingOrder ->order_department.',';
		}
		$sMeetingMessage = strval($sMeetingMessage);
		$sMeetingMessage = iconv("UTF-8",'GB2312//IGNORE',$sMeetingMessage);
		header("Content-type:text/csv");
		header("Content-Type: application/force-download");
		header("Content-Disposition: attachment; filename=".date('Y-m-d').".csv");
		header('Expires:0');
		header('Pragma:public');
		echo $sMeetingMessage;
	}

}