<?php

/**
 * API操作类
 * @author dll
 *
 */
use Illuminate\Support\Facades\Input;

class ApiAspirinScreeningController extends BaseController {
	
	CONST  PAGESIZE = 10;
	public $iUserId = 0 ;

	public function __construct()
	{
		$this->beforeFilter('apilogin');
		$this->beforeFilter('apidoc', array('only' =>array('getSubmit1','getSubmit2','getSubmit3','getSubmit4')));
		global $iUserId;
		$this->iUserId = &$iUserId;
	}

	/***
	 * 风险筛查外链地址
	 */
	public function getUrl()
	{
		$aLog = array(
			'user_id'     => $this->iUserId,
			'device'      => Input::get('device')?trim(Input::get('device')):'app',
			'created_at'  => date('Y-m-d H:i:s',time())
		);
		$oLog = new AspirinScreeningHitLog($aLog);
		$oLog->save();
		$aScreening = array('url' => 'http://whder.mymeeting.com.cn');
		echo  $this->formatRet(true,"成功",$aScreening,"");exit;
	}

	/**
	 * 筛查第一页
	 * http://cdma.local/apiaspirinscreening/submit1?sex=1&age=1992年12月&weight=50&height=170&token=
	 */
	public function getSubmit1()
	{
		$iUid = $this->iUserId;
		$aInput = Input::all();
		$rule = array(
			'sex'        => 'required',
			'age'        => 'required',
			'weight'     => 'required',
			'height'     => 'required'
		);
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
			$msg = '请将信息填写完整，谢谢';
			echo $this->formatRet(false,$msg);exit;
		}
		$sex    = intval($aInput['sex']);
		$age    = trim($aInput['age']);
		$weight = intval($aInput['weight']);
		$height = intval($aInput['height']);
		//根据年月计算真实年龄
		$realage = AspirinScreening::getRealAge($age,'年','月');
		//筛查记录保存
		$aScreening = array(
			'user_id'     => $iUid,
			'sex'         => $sex,
			'age'         => $realage,
			'weight'      => $weight,
			'height'      => $height,
			'device_type' => 0,       //app
			'created_at'  => date('Y-m-d H:i:s',time())
		);
		$oScreening = new AspirinScreening($aScreening);
		$oScreening->save();
		$iSourceId = $oScreening->id;
		$aScreening = array('screening_id' => $iSourceId);
		echo  $this->formatRet(true,"成功","success",$aScreening);exit;
	}

	/**
	 * 筛查第二页
	 * http://cdma.local/apiaspirinscreening/submit2?id=1&question0=&question1=2&question2=2&question3=2&question31=2&token=
	 */
	public function getSubmit2()
	{
		$aInput = Input::all();
		$question1 = $aInput['question1'];
		$question2 = $aInput['question2'];
		$question3 = $aInput['question3'];
		$question0 = isset($aInput['question0'])?intval($aInput['question0']):0;
		$question31 = isset($aInput['question31'])?intval($aInput['question31']):0;
		if($question1 == AspirinScreening::SCREENING_DEFAULT){
			echo $this->formatRet(false,'请选择是否吸烟，谢谢');exit;
		}
		if($question2 == AspirinScreening::SCREENING_DEFAULT){
			echo $this->formatRet(false,'请选择是否有早发心脑血管疾病家庭史，谢谢');exit;
		}
		if($question3 == AspirinScreening::SCREENING_DEFAULT){
			echo $this->formatRet(false,'请选择是否有确诊的糖尿病，谢谢');exit;
		}
		if ($question3 == AspirinScreening::SCREENING_YES && $question31 == AspirinScreening::SCREENING_DEFAULT){
			echo $this->formatRet(false,'请选择是否在服用降糖药，谢谢');exit;
		}
		$iId = intval(Input::get('id'));
		$oScreening = AspirinScreening::find($iId);
		if(count($oScreening)){
			if ($oScreening->sex == 0 && $question0 == AspirinScreening::SCREENING_DEFAULT){
				echo $this->formatRet(false,'请选择是否绝经，谢谢');exit;
			}
			$oScreening->question0 = $question0;
			$oScreening->question1 = $question1;
			$oScreening->question2 = $question2;
			$oScreening->question3 = $question3;
			$oScreening->question31 = $question31;
			$oScreening->save();
			$aScreening = array('screening_id' => $iId);
			echo  $this->formatRet(true,"成功","success",$aScreening);exit;
		}else{
			echo $this->formatRet(false,'记录不存在，请重试');exit;
		}
	}

	/**
	 * 筛查第三页
	 * http://cdma.local/apiaspirinscreening/submit3?id=1&question4=&question41=2&question42=2&question5=2&question51=2&question52=2&question53=2&question54=2&token=
	 */
	public function getSubmit3()
	{
		$aInput = Input::all();
		$question4 = $aInput['question4'];
		$question41 = isset($aInput['question41'])?intval($aInput['question41']):0;
		$question42 = isset($aInput['question42'])?intval($aInput['question42']):0;
		$question5 = $aInput['question5'];
		$question51 = isset($aInput['question51'])?intval($aInput['question51']):0;
		$question52 = isset($aInput['question52'])?intval($aInput['question52']):0;
		$question53 = isset($aInput['question53'])?intval($aInput['question53']):0;
		$question54 = isset($aInput['question54'])?intval($aInput['question54']):0;
		if($question4 == AspirinScreening::SCREENING_DEFAULT){
			echo $this->formatRet(false,'请选择是否有已诊断的高血脂，谢谢');exit;
		}
		if($question5 == AspirinScreening::SCREENING_DEFAULT){
			echo $this->formatRet(false,'请选择是否有已诊断的高血压，谢谢');exit;
		}
		if ($question4 == AspirinScreening::SCREENING_YES && $question41 == AspirinScreening::SCREENING_DEFAULT){
			echo $this->formatRet(false,'请选择是否在用降脂药，谢谢');exit;
		}
		if ($question4 == AspirinScreening::SCREENING_YES && $question42 == AspirinScreening::SCREENING_DEFAULT){
			echo $this->formatRet(false,'请选择治疗前总胆固醇水平，谢谢');exit;
		}
		if ($question4 == AspirinScreening::SCREENING_NONE && $question41 == AspirinScreening::SCREENING_DEFAULT){
			echo $this->formatRet(false,'请选择是否在用降脂药，谢谢');exit;
		}
		if ($question5 == AspirinScreening::SCREENING_YES && $question51 == AspirinScreening::SCREENING_DEFAULT){
			echo $this->formatRet(false,'请选择是否在用降压药，谢谢');exit;
		}
		if ($question5 == AspirinScreening::SCREENING_YES && $question52 == AspirinScreening::SCREENING_DEFAULT){
			echo $this->formatRet(false,'请填写治疗前收缩压，谢谢');exit;
		}
		if ($question5 == AspirinScreening::SCREENING_YES && $question53 == AspirinScreening::SCREENING_DEFAULT){
			echo $this->formatRet(false,'请选择治疗前舒张压，谢谢');exit;
		}
		if ($question5 == AspirinScreening::SCREENING_YES && $question54 == AspirinScreening::SCREENING_DEFAULT){
			echo $this->formatRet(false,'请选择治疗后收缩压，谢谢');exit;
		}
		if ($question5 == AspirinScreening::SCREENING_NONE && $question51 == AspirinScreening::SCREENING_DEFAULT){
			echo $this->formatRet(false,'请选择是否在用降压药，谢谢');exit;
		}
		$iId = intval(Input::get('id'));
		$oScreening = AspirinScreening::find($iId);
		if(count($oScreening)){
			$oScreening->question4 = $question4;
			$oScreening->question41 = $question41;
			$oScreening->question42 = $question42;
			$oScreening->question5 = $question5;
			$oScreening->question51 = $question51;
			$oScreening->question52 = $question52;
			$oScreening->question53 = $question53;
			$oScreening->question54 = $question54;
			$oScreening->save();
			$aScreening = array('screening_id' => $iId);
			echo  $this->formatRet(true,"成功","success",$aScreening);exit;
		}else{
			echo $this->formatRet(false,'记录不存在，请重试');exit;
		}
	}

	/**
	 * 筛查第四页
	 * http://cdma.local/apiaspirinscreening/submit4?id=1&question6=&question7=2&question8=2&question9=2&question91=[1,2,3]&token=
	 */
	public function getSubmit4()
	{
		$iUid = $this->iUserId;
		$aInput = Input::all();
		$question6 = $aInput['question6'];
		$question7 = $aInput['question7'];
		$question8 = $aInput['question8'];
		$question9 = $aInput['question9'];
		$question91 = isset($aInput['question91'])?trim($aInput['question91']):'';
		if($question6 == AspirinScreening::SCREENING_DEFAULT){
			echo $this->formatRet(false,'请选择是否有已诊断的冠心病或心肌梗死病史，谢谢');exit;
		}
		if($question7 == AspirinScreening::SCREENING_DEFAULT){
			echo $this->formatRet(false,'请选择是否有已诊断的脑卒中病史，谢谢');exit;
		}
		if($question8 == AspirinScreening::SCREENING_DEFAULT){
			echo $this->formatRet(false,'请选择是否患有胃肠道出血/溃疡等疾病，谢谢');exit;
		}
		if($question9 == AspirinScreening::SCREENING_DEFAULT){
			echo $this->formatRet(false,'请选择是否正在服用抗血小板药物，谢谢');exit;
		}
		if ($question9 == AspirinScreening::SCREENING_YES && empty($question91)){
			echo $this->formatRet(false,'请选择抗血小板药物名称，谢谢');exit;
		}
		if($question91){
			$aInfo = json_decode($question91,true);
			$question91 = implode(',',$aInfo);
		}
		$iId = intval(Input::get('id'));
		$oScreening = AspirinScreening::find($iId);
		if(count($oScreening)){
			$oScreening->question6 = $question6;
			$oScreening->question7 = $question7;
			$oScreening->question8 = $question8;
			$oScreening->question9 = $question9;
			$oScreening->question91 = $question91;
			$oScreening->save();
			$weight = $oScreening->weight;
			$height = $oScreening->height;
			$sex = $oScreening->sex;
			//BMI值
			$iBmi = $weight/($height*0.01*$height*0.01);
			$iBmi = round($iBmi,2);
			/************筛查公式计算得出结果s***************/
			//年龄得分
			$iAgeScore = AspirinScreening::getAgeScore($sex,$oScreening->age);
			//TC得分
			$iTcScore = AspirinScreening::getTcScore($sex,$oScreening->question42);
			//血压得分
			$iBpScore = AspirinScreening::getBpScore($sex,$oScreening->question52,$oScreening->question53);
			//糖尿病得分
			$iDmScore = AspirinScreening::getDmScore($sex,$oScreening->question3);
			//吸烟得分
			$iSmokeScore = AspirinScreening::getSmokeScore($oScreening->question1);
			//总分数
			$iScore = $iAgeScore+$iTcScore+$iBpScore+$iDmScore+$iSmokeScore;
			//评估CHD风险百分比
			$sPercent = AspirinScreening::getPercent($sex,$iScore);
			//存在的心血管危险因素
			$sDanger = '';
			$aDanger = AspirinScreening::getDangerElement($iId);
			if($aDanger){
				$sDanger = implode(',',$aDanger);
			}
			//获取界面呈现内容
			$aContent = AspirinScreening::getInfo($sPercent,$aDanger,$iBmi);
			/************筛查公式计算得出结果e***************/
			//获取结果页二维码链接地址
			$sUrl = '/aspirinshare/screening-result?id='.$iId;
			$sUrl = Config::get('app.url').$sUrl;
			$sEwmUrl = AspirinEwm::getErweima($sUrl);
			$oScreening->bmi       = $iBmi;
			$oScreening->score     = $iScore;
			$oScreening->percent   = $sPercent;
			$oScreening->flag      = $aContent[0];
			$oScreening->danger    = $sDanger;
			$oScreening->save();
			$oScreeningLog = AspirinScreeningLog::where('user_id',$iUid)->first();
			if(count($oScreeningLog)){
				$oScreeningLog->increment('app_use_count');
			}else{
				$aScreeningLog = array(
					'user_id'        => $iUid,
					'app_use_count'  => 1,
					'created_at'     => date('Y-m-d H:i:s',time())
				);
				$oScreeningLog = new AspirinScreeningLog($aScreeningLog);
				$oScreeningLog->save();
			}
			$iGoldCount = $oScreeningLog->app_use_count;
			if($iGoldCount < AspirinScreeningLog::MAILI_LIMIT){
				//在APP中使用每进行一次筛查可获得1麦粒。帐号上限100麦粒
				// $sRes = GoldService::addGold($iUid, 51, $iId);
			}
			$aResult = array(
				'sex'         => $sex,
				'age'         => $oScreening->age,
				'height'      => $height,
				'weight'      => $weight,
				'bmi'         => $iBmi,
				'flag'        => $aContent[0],
				'flag_text'   => $aContent[1],
				'suggestion'  => $aContent[2],
				'danger_element'=> $aDanger,
				'ewm_url'     => Config::get('app.url').$sEwmUrl,
				'share_flag'  => 1,
				'share_title' => '专项基金风险筛查报告',
				'share_des'   => '专项基金风险筛查报告',
				'share_thumb' => Config::get('app.url').'/assets/images/mobile/logo.png',
				'share_url'   => $sUrl
		
			);
			echo  $this->formatRet(true,"成功","success",$aResult);exit;
		}else{
			echo $this->formatRet(false,'记录不存在，请重试');exit;
		}
	}
	
	/**
	 * 返回专项基金筛查——筛查表分享
	 * http://cdma.local/apiaspirinscreening/screening-share?token=
	 */
	public function getScreeningShare(){
		$aShare = array(
			'share_flag'  => 1,
			'share_title' => '专项基金风险筛查表',
			'share_des'   => '专项基金风险筛查表',
			'share_thumb' => Config::get('app.url').'/assets/images/mobile/logo.png',
			'share_url'   => Config::get('app.url').'/aspirinshare/screening-ewm?iUid='.$this->iUserId
		);
		echo  $this->formatRet(true,"成功","success",$aShare);exit;
	}
	
	/**
	 * 返回专项基金筛查——筛查历史列表页
	 * /apiaspirinscreening/log?token=
	 */
	public function getLog()
	{
		$iUid = $this->iUserId;
		$oLog = AspirinScreening::where('user_id',$iUid)->where('bmi','!=','')->orderby('created_at','desc')->get();
		$aLog = array();
		foreach ($oLog as $k=>$v){
			$aLog[] = array(
				'screening_id'   => $v->id,
				'screening_time' => substr($v->created_at,0,16)
			);
		}
		echo  $this->formatRet(true,"成功","success",$aLog);exit;
	}
	
	/**
	 * 返回专项基金筛查——筛查历史结果页
	 * /apiaspirinscreening/result?id=&token=
	 */
	public function getResult()
	{
		$iId = Input::get('id');
		$oScreening = AspirinScreening::find($iId);
		$aDanger = array();
		if($oScreening->danger){
			$aDanger = explode(',',$oScreening->danger);
		}
		//获取界面呈现内容
		$aContent = AspirinScreening::getInfo($oScreening->percent,$aDanger,$oScreening->bmi);
		//获取结果页二维码链接地址
		$sUrl = '/aspirinshare/screening-result?id='.$iId;
		$sUrl = Config::get('app.url').$sUrl;
		$sEwmUrl = AspirinEwm::getErweima($sUrl);
		
		$aResult = array(
			'screening_id'=> $oScreening->id,
			'sex'         => $oScreening->sex,
			'age'         => $oScreening->age,
			'height'      => $oScreening->height,
			'weight'      => $oScreening->weight,
			'bmi'         => $oScreening->bmi,
			'flag'        => $oScreening->flag,
			'flag_text'   => $aContent[1],
			'suggestion'  => $aContent[2],
			'danger_element'=> $aDanger,
			'ewm_url'     => Config::get('app.url').$sEwmUrl,
			'share_flag'  => 1,
			'share_title' => '专项基金风险筛查报告',
			'share_des'   => '专项基金风险筛查报告',
			'share_thumb' => Config::get('app.url').'/assets/images/mobile/logo.png',
			'share_url'   => $sUrl
		);
		echo  $this->formatRet(true,"成功",$aResult,"success");exit;
	}
	
	/**
	 * 返回专项基金筛查——筛查历史结果详细页
	 * /apiaspirinscreening/result-detail?id=&token= 
	 */
	public function getResultDetail()
	{
		$aResult = array();
		$oScreening = AspirinScreening::find(Input::get('id'));
		if($oScreening){
			$iQuestion42 = $oScreening->question42;
			switch($iQuestion42){
			case 1:
				$sQuestion42 = '<4.14mmol/L(<160mg/dl)';
				break;
			case 2:
				$sQuestion42 = '4.15-5.17mmol/L(160-199mg/dl)';
				break;
			case 3:
				$sQuestion42 = '5.18-6.21mmol/L(200-239mg/dl)';
				break;
			case 4:
				$sQuestion42 = '6.22-7.24mmol/L(240-279mg/dl)';
				break;
			case 5:
				$sQuestion42 = '≥7.25mmol/L(≥280mg/dl)';
				break;
			case 6:
				$sQuestion42 = '不清楚';
				break;
			default :
				$sQuestion42 = '';
				break;
			}
			$iQuestion53 = $oScreening->question53;
			switch($iQuestion53){
				case 1:
					$sQuestion53 = '小于80mmHg';
					break;
				case 2:
					$sQuestion53 = '80-84mmHg';
					break;
				case 3:
					$sQuestion53 = '85-89mmHg';
					break;
				case 4:
					$sQuestion53 = '90-99mmHg';
					break;
				case 5:
					$sQuestion53 = '大于等于100mmHg';
					break;
				case 6:
					$sQuestion53 = '不清楚';
					break;
				default :
					$sQuestion53 = '';
					break;
			}
			$iQuestion54 = $oScreening->question54;
			switch($iQuestion54){
				case 1:
					$sQuestion54 = '小于等于140mmHg';
					break;
				case 2:
					$sQuestion54 = '大于等于140mmHg';
					break;
				default :
					$sQuestion54 = '';
					break;
			}
			$iQuestion91 = $oScreening->question91;
			$sQuestion91 = '';
			if($iQuestion91){
				$aQuestion91 = explode(',',$iQuestion91);
				if(in_array('1',$aQuestion91)){
					$sQuestion91 .= '阿司匹林';
				}
				if(in_array('2',$aQuestion91)){
					$sQuestion91 .= ' 氯吡格雷';
				}
				if(in_array('3',$aQuestion91)){
					$sQuestion91 .= ' 其它';
				}
			}
			$aResult = array(
				'sex'         => $oScreening->sex == 0?'女':'男',
				'age'         => $oScreening->age,
				'height'      => $oScreening->height.'cm',
				'weight'      => $oScreening->weight.'kg',
				'question1'   => $oScreening->question1 != 0?($oScreening->question1 == 1?'否':'是'):'',
				'question0'   => $oScreening->question0 != 0?($oScreening->question0 == 1?'否':'是'):'',
				'question2'   => $oScreening->question2 != 0?($oScreening->question2 == 1?'否':'是'):'',
				'question3'   => $oScreening->question3 != 0?($oScreening->question3 != 3?($oScreening->question3 == 1?'否':'是'):'不清楚'):'',
				'question31'  => $oScreening->question31 != 0?($oScreening->question31 == 1?'否':'是'):'',
				'question4'   => $oScreening->question4 != 0?($oScreening->question4 != 3?($oScreening->question4 == 1?'否':'是'):'不清楚'):'',
				'question41'  => $oScreening->question41 != 0?($oScreening->question41 == 1?'否':'是'):'',
				'question42'  => $sQuestion42,
				'question5'   => $oScreening->question5 != 0?($oScreening->question5 != 3?($oScreening->question5 == 1?'否':'是'):'不清楚'):'',
				'question51'  => $oScreening->question51 != 0?($oScreening->question51 == 1?'否':'是'):'',
				'question52'  => $oScreening->question52 != 0?$oScreening->question52.'mmHg':'',
				'question53'  => $sQuestion53,
				'question54'  => $sQuestion54,
				'question6'   => $oScreening->question6 != 0?($oScreening->question6 == 1?'否':'是'):'',
				'question7'   => $oScreening->question7 != 0?($oScreening->question7 == 1?'否':'是'):'',
				'question8'   => $oScreening->question8 != 0?($oScreening->question8 != 3?($oScreening->question8 == 1?'否':'是'):'不清楚'):'',
				'question9'   => $oScreening->question9 != 0?($oScreening->question9 == 1?'否':'是'):'',
				'question91'  => $sQuestion91
			);
		}
		echo  $this->formatRet(true,"成功",$aResult,"success");exit;
	}
	
}