<?php
/**
 * API用户信息操作类
 * @author liuy
 *
 */
use Illuminate\Support\Facades\Input;
class ApiUpdateFlagController extends BaseController {

	public function __construct()
	{
		$this->beforeFilter('apilogin');
	}

	/**
	 *更新标志显示
	 * http://cdma.local/apiupdateflag/update-flag?token=
	 * eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
	 * eyJpdiI6InQzTHZBQTBkVjBHWDdqcXZxcyt5YUM5TXU1eGsraENBRkNXbkJSWmRJQU09IiwidmFsdWUiOiJITHQ1V2ozTmJnaU5mS1RTczk4emRBazIyYXJGaUtJdmNoVklZOEFBTGlJPSIsIm1hYyI6ImI2YWY1MjY4NzNkYWQ1MzExODMxNTJiOTU3Yjc1ZTNkM2U2ZTc2MzJlMWY5YTk5YzM1NTU5NDE3Zjk5MmUzNjQifQ==
	 */
	public function getUpdateFlag(){
		global $iUserId;
		$iUid = $iUserId; 
		$oUserLoginTime = UserInformationTime::where('user_id',$iUid)->first();
		$oLastUpdateTime = InformationUpdateTime::first();
		if(count($oUserLoginTime)){
			if(count($oLastUpdateTime)){
				$iUpdateTime = $oUserLoginTime -> user_time;
				$iLastUpdateTime = $oLastUpdateTime ->update_time;
				if($iUpdateTime < $iLastUpdateTime){
					$iUpdateLog = 1;
				}else{
					$iUpdateLog = 0;
				}
			}else{
				$iUpdateTime = $oUserLoginTime -> user_time;
				$iLastUpdateTime = 0;
				if($iUpdateTime < $iLastUpdateTime){
					$iUpdateLog = 1;
				}else{
					$iUpdateLog = 0;
				}
			}
		}else{
			$iUpdateLog = 0;
			$iLastUpdateTime = 0;
		}
		return json_encode(array('success'=>true,'update_flag'=>$iUpdateLog,'last_update_time'=>$iLastUpdateTime));
	}
	
}