<?php

class AdminController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	//后台首页
	public function Index()
	{
		return View::make('admin.index');
	}
	
	//登录页面
	public function Login(){
		if(Auth::check()){
			if(Auth::User()->role_id != 1){//admin登录
				$oUserAdminCase = UserAdminCase::where('user_id',Auth::User()->id)->first();
				if(!$oUserAdminCase){
					$aInfom = Config::get('config.information');
					$aResearch = Config::get('config.research');
					$aDocFace = Config::get('config.docface');
					if(!in_array(Auth::User()->id, $aInfom)){
						if(!in_array(Auth::User()->id, $aResearch)){
							if(!in_array(Auth::User()->id, $aDocFace)){
								return  View::make('admin.login');
							}
						}
					}
					
				}
			}
			return Redirect::to('/kyadmin');
		}else{
			return  View::make('admin.login');
		}
	}

	//登陆页面
	public function LoginDo(){
		$rules = array(
			'user_nick'	=> 'required',
			'password'	=> 'required'
		);
		$input = array(
			'user_nick'	=> Input::get('user_nick'),
			'password'	=> Input::get('password'),
		);
		$validator = Validator::make(Input::all(), $rules);
		if($validator->passes()) {
			if (Auth::attempt($input)) {
				$oUser = User::find(Auth::User()->id);
				if($oUser->role_id!=1){
					$oUserAdminCase = UserAdminCase::where('user_id',Auth::User()->id)->first();
					if(!$oUserAdminCase){
						$aInfom = Config::get('config.information');
						if(!in_array(Auth::User()->id, $aInfom)){
							$aResearch = Config::get('config.research');
							if(!in_array(Auth::User()->id, $aResearch)){
								$aDocFace = Config::get('config.docface');
								if(!in_array(Auth::User()->id, $aDocFace)){
							  		return Redirect::to('/admlogin');
								}
							}
						}
					}
				}
				return Redirect::to('/kyadmin');
			} else {
				return Redirect::to('/admlogin');
			}
		}
		return Redirect::to('/admlogin');
	}
	
	//退出
	public function LoginOut(){
		Auth::logout();
		return Redirect::to('/admlogin');
	}
	
	public function LogRedis($iVideoId){
		$sCacheName = 'sign_in_cache_'.$iVideoId;
		if(Cache::has($sCacheName)){
			$sSignInCache = Cache::get($sCacheName);
			$aSignInCahce = mb_unserialize($sSignInCache);
			dd($aSignInCahce);
		}else{
			return 'no_log';
		}
	}
	
	public function ClearLogRedis($iVideoId){
		$sCacheName = 'sign_in_cache_'.$iVideoId;
		Cache::forget($sCacheName);
		return 'success';
	}
	
	public function FlushAll(){
		Cache::flush();
		return 'success';
	}

	//系统基础设置
	public function SettingSys(){

	    if (!Session::get('userid')) {
            return Redirect::to('/admlogin');
        }

		$Configs = new Configs();
		$configRs = $Configs::find(1);
		// print_r($configRs);die;
		$aInput = Input::all();
		if(@$aInput['action']=="save"){
			unset($aInput['action']);
			if($configRs->addDoc($aInput)->save()){
				return $this->showMessage('恭喜您，设置成功。','/admsys');
			}else{
				return $this->showMessage('对不起，保存失败。');
			}
		}
		Return View::make('admin.setting')->with('oSysinfo',$configRs);
	}
	
}