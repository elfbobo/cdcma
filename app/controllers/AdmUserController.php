<?php

use Illuminate\Support\Facades\Redirect;

class AdmUserController extends BaseController {
	
	CONST  PAGESIZE = 10;

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	//后台首页
	public function ListUser()
	{
		//取出所有用户
		$oUsers = User::where('role_id','!=',1);  
		$input = Input::all();
        $aLink = array();
		if(isset($input['user_type'])&&$input['user_type']){
			$oUsers = $oUsers->where('user_type','=',$input['user_type']);
			$aLink['user_type'] = $input['user_type'];
		}
		if(isset($input['role_id'])&&$input['role_id']){
			$oUsers = $oUsers->where('role_id','=',$input['role_id']);
			$aLink['role_id'] = $input['role_id'];
		}
		if(isset($input['user_name'])&&$input['user_name']){
			$oUsers = $oUsers->where('user_name','LIKE',"%".$input['user_name']."%");
			$aLink['user_name'] = $input['user_name'];
		}
		if(isset($input['user_cwid'])&&$input['user_cwid']){
			$oUsers = $oUsers->where('user_cwid','LIKE',"%".$input['user_cwid']."%");
			$aLink['user_cwid'] = $input['user_cwid'];
		}
		if(isset($input['invite_code'])&&$input['invite_code']){
			$oUsers = $oUsers->where('invite_code','LIKE',"%".$input['invite_code']."%");
			$aLink['invite_code'] = $input['invite_code'];
		}
		if(isset($input['user_nick'])&&$input['user_nick']){
			$oUsers = $oUsers->where('user_nick','LIKE',"%".$input['user_nick']."%");
			$aLink['user_nick'] = $input['user_nick'];
		}
		if(isset($input['user_tel'])&&$input['user_tel']){
			$oUsers = $oUsers->where('user_tel','LIKE',"%".$input['user_tel']."%");
			$aLink['user_tel'] = $input['user_tel'];
		}
		if(isset($input['user_email'])&&$input['user_email']){
			$oUsers = $oUsers->where('user_email','LIKE',"%".$input['user_email']."%");
			$aLink['user_email'] = $input['user_email'];
		}
		if(isset($input['link_rep_id'])&&$input['link_rep_id']){
			$oUsers = $oUsers->where('link_rep_id',$input['link_rep_id']);
			$aLink['link_rep_id'] = $input['link_rep_id'];
		}
		if(isset($input['link_user_id'])&&$input['link_user_id']){
			$oUsers = $oUsers->where('link_user_id',$input['link_user_id']);
			$aLink['link_user_id'] = $input['link_user_id'];
		}
		if(isset($input['case_checkbox'])){
			$aUserAdminCase = UserAdminCase::lists('user_id','id');
			$oUsers = $oUsers->whereIn('id',$aUserAdminCase);
		}
		$num = User::where('role_id','!=',1)->count();
		$pageNum = 5000;
		if($num > $pageNum){
			$pages = ceil($num/$pageNum);
			for($i=1; $i<=$pages; $i++){
				$start = ($i-1)*$pageNum+1;
				$end = $i*$pageNum;
				if($i == $pages) $end = $num-($i-1)*$pageNum;
				$aTotalNum[] = "{$start}-{$end}";
			}
		}else{
			$aTotalNum[] = "1-{$num}";
		}
		$oUsers = $oUsers->paginate(self::PAGESIZE);
		// $log = DB::getQueryLog();
		// print_r($log);die;
		foreach ($oUsers as $k=>$v){
			$iUserType = $v -> user_type;
			switch($iUserType){
				case 1:
					$v ->user_type = "拜新同用户";
					break;
				case 2:
					$v ->user_type = "拜阿用户";
					break; 
				case 3:
					$v ->user_type = "拜新同和拜阿用户";
					break;
				case 0:
					$v ->user_type = '尚未划分';
			} 
			$iUserId = $v->id;
			//直播时长
			$iUserTotalLiveMin = VideoLog::where('user_id',$iUserId)->where('video_type','2')->sum('watch_minutes');
			//录播时长
			$iUserTotalReviewMin = VideoLog::where('user_id',$iUserId)->where('video_type','1')->sum('watch_minutes');
			$iUserTotalLiveScore = $iUserTotalLiveMin;
			$iUserTotalReviewScore = $iUserTotalReviewMin;
			$oUsers[$k]->user_score = $iUserTotalLiveScore+$iUserTotalReviewScore;
			$oUsers[$k]->user_live_score = $iUserTotalLiveScore;
			$oUsers[$k]->user_review_score = $iUserTotalReviewScore;
		}
		$aRole = DB::table('role')->lists('name','id');
		//获取所有有病例管理权限的用户
		$aUserAdminCase1 = UserAdminCase::where('type',1)->lists('user_id','id');
		$aUserAdminCase2 = UserAdminCase::where('type',2)->lists('user_id','id');
		//获取所有代表
		$aRepName = User::where('role_id',2)->lists('user_name','id');
		$aRepCode = User::where('role_id',2)->lists('invite_code','id');
		return View::make('admin.user.list')
			->with('oUsers',$oUsers)
			->with('aRole',$aRole)
			->with('aLink',$aLink)
			->with('aUserAdminCase1',$aUserAdminCase1)
			->with('aUserAdminCase2',$aUserAdminCase2)
			->with('aRepName',$aRepName)
			->with('aRepCode',$aRepCode)
			->with('aTotalNum',$aTotalNum);
	}
	
	public function OneUserInfo($iId){
		$oUser = User::where('id',$iId)->get();
		if($oUser)
			dd($oUser->toArray());
		else 
			echo 'id is wrong!';
	}
	
	public function ListUser2()
	{
		//取出所有用户
		$oUsers = User::where('role_id','!=',1);
		$input = Input::all();
		$aLink = array();
		if(isset($input['user_type'])&&$input['user_type']){
		    $oUsers = $oUsers->where('user_type',$input['user_type']);
			$aLink['user_type'] = $input['user_type'];
		}
		if(isset($input['role_id'])&&$input['role_id']){
			$oUsers = $oUsers->where('role_id',$input['role_id']);
			$aLink['role_id'] = $input['role_id'];
		}
		if(isset($input['user_name'])&&$input['user_name']){
			$oUsers = $oUsers->where('user_name','LIKE',"%".$input['user_name']."%");
			$aLink['user_name'] = $input['user_name'];
		}
		if(isset($input['user_cwid'])&&$input['user_cwid']){
			$oUsers = $oUsers->where('user_cwid','LIKE',"%".$input['user_cwid']."%");
			$aLink['user_cwid'] = $input['user_cwid'];
		}
		if(isset($input['user_nick'])&&$input['user_nick']){
			$oUsers = $oUsers->where('user_nick','LIKE',"%".$input['user_nick']."%");
			$aLink['user_nick'] = $input['user_nick'];
		}
		if(isset($input['user_tel'])&&$input['user_tel']){
			$oUsers = $oUsers->where('user_tel','LIKE',"%".$input['user_tel']."%");
			$aLink['user_tel'] = $input['user_tel'];
		}
		if(isset($input['user_email'])&&$input['user_email']){
			$oUsers = $oUsers->where('user_email','LIKE',"%".$input['user_email']."%");
			$aLink['user_email'] = $input['user_email'];
		}
		$oUsers = $oUsers->paginate(self::PAGESIZE);
		/*foreach ($oUsers as $k=>$v){
			$iUserId = $v->id;
			//直播时长
			$iUserTotalLiveMin = VideoLog::where('user_id',$iUserId)->where('video_type','2')->sum('watch_minutes');
			//录播时长
			$iUserTotalReviewMin = VideoLog::where('user_id',$iUserId)->where('video_type','1')->sum('watch_minutes');
			$iUserTotalLiveScore = $iUserTotalLiveMin;
			$iUserTotalReviewScore = intval($iUserTotalReviewMin/2);
			$oUsers[$k]->user_score = $iUserTotalLiveScore+$iUserTotalReviewScore;
			$oUsers[$k]->user_live_score = $iUserTotalLiveScore;
			$oUsers[$k]->user_review_score = $iUserTotalReviewScore;
		}*/
		$aRole = DB::table('role')->lists('name','id');
		return View::make('admin.user.list')
			->with('oUsers',$oUsers)
			->with('aRole',$aRole)
			->with('aLink',$aLink);
	}
	
	//新增用户
	public function AddUser(){
		return View::make('admin.user.add');
	}
	
	//新增用户提交
	public function AddUserDo(){
		$aInput = Input::all();
		$rule = User::getUserRule();
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
		    return $this->showMessage('请填写必填字段');
		}
		//用户信息是否重复
		$msg = User::IsUserMsgRepeat($aInput);
		if($msg){
			return $this->showMessage($msg);
		}
		$oUser = new User();
		//验证成功
		if($oUser->addUser($aInput)->save()){
			return $this->showMessage('添加用户成功,密码默认为用户手机号码','/admuser');
		}else{
			return $this->showMessage('添加用户失败');
		}
	}
	
	//编辑用户
	public function EditUser($id){
		$oUser = User::find($id);
		return View::make('admin.user.edit')->with('oUser',$oUser);
	}
	
	//编辑提交
	public function EditUserDo($id){
		$aInput = Input::all();
		$rule = User::getUserRule();
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
		    return $this->showMessage('请填写必填字段');
		}
		//用户信息是否重复
		$msg = User::IsUserMsgRepeat($aInput,$id);
		if($msg){
			return $this->showMessage($msg);
		}
		$oUser = User::find($id);
		if($oUser->addUser($aInput)->save()){
			return $this->showMessage('编辑用户成功');
		}else{
			return $this->showMessage('编辑用户失败');
		}
	}
	
	//删除
	public function DelUser($id){
		$oUser = User::find($id);
		$oUser->delete();
		return $this->showMessage('删除用户成功','/admuser');
	}
	
	//导入用户
	public function ImportUser(){
		return View::make('admin.user.import');
	}
	
	/*
	 * 导入用户
	 */
	public function ImportDo(){
		set_time_limit(0);
		$oUser = new User();
		$result = $oUser->adminImport($_FILES);
		return $this->showmessage($result);
	}
	
	//代表预约专家列表
	public function Order(){
		$oUserOrder = UserOrder::orderBy('created_at','DESC')->paginate(self::PAGESIZE);
		//获取用户排行榜
		$oUser = User::where('role_id',2)->orderBy('user_score','DESC')->get();
		$aUserOrder = array();
		foreach($oUser as $k=>$v){
			$aUserOrder[$v->id] = $k+1;
		}
		foreach($oUserOrder as $k=>$v){
			$oUser = User::find($v->user_id);
			$oUserOrder[$k]['user_id'] = $oUser->id;
			$oUserOrder[$k]['user_name'] = $oUser->user_name;
			$oUserOrder[$k]['user_cwid'] = $oUser->user_cwid;
		}
		return View::make('admin.user.neworder.order')->with('oUserOrder',$oUserOrder)->with('aUserOrder',$aUserOrder);
	}
	
	//代表预约专家导出
	public function ExportOrder(){
		$oUserOrder = UserOrder::orderBy('created_at','DESC')->get();
		//获取用户排行榜
		$oUser = User::where('role_id',2)->orderBy('user_score','DESC')->get();
		$aUserOrder = array();
		foreach($oUser as $k=>$v){
			$aUserOrder[$v->id] = $k+1;
		}
		foreach($oUserOrder as $k=>$v){
			$oUser = User::find($v->user_id);
			$oUserOrder[$k]['user_id'] = $oUser->id;
			$oUserOrder[$k]['user_name'] = $oUser->user_name;
			$oUserOrder[$k]['user_cwid'] = $oUser->user_cwid;
		}
		//导出csv
		header( "Cache-Control: public" );
    	header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
    	header("Content-Disposition:attachment;filename=order_".date('Ymdhis').".csv");
    	header('Content-Type:APPLICATION/OCTET-STREAM');
    	ob_start();
		$header_str =  iconv("utf-8",'gbk',"id,排行榜排名,代表姓名,代表CWID,专家,科室,预约时间,类型,创建时间\n");
		$file_str="";
		if($oUserOrder){
			foreach($oUserOrder as $k =>$r){
				$type = '';
				if($r->user_order_doc_id==0){
					$type = "旧版本数据";
				}
				$order = isset($aUserOrder[$r->user_id])?$aUserOrder[$r->user_id]:'';
				$file_str.= $r->id.','.$order.','.$r->user_name.','.$r->user_cwid.','.$r->doc_name.','.$r->doc_department.','.$r->order_time.','.$type.','.$r->created_at."\n";
			}
		}else{}
		$file_str = iconv("utf-8",'GBK//TRANSLIT',$file_str);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	public function ChangePsw($iId,$sPsw){
		$oUser = User::find($iId);
		$oUser->password = Hash::make($sPsw);
		$oUser->save();
		return 'success';
	}
	
	public function UpdateArea(){
		if(Cache::has('area')){
			Cache::forget('area');
		}
		User::getAreaCache();
		return 'success';
	}
	
	public function UpdateRegin(){
		if(Cache::has('regin')){
			Cache::forget('regin');
		}
		User::getReginCache();
		return 'success';
	}
	
	public function UpdateHospital(){
		if(Cache::has('cache_hospital')){
			Cache::forget('cache_hospital');
		}
		User::getHospitalCache();
		return 'success';
	}
	
	public function ExportUser(){
		set_time_limit(0);
		$oUser = User::where('role_id',2)->get();
		//导出csv
		header( "Cache-Control: public" );
    	header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
    	header("Content-Disposition:attachment;filename=".date('Ymdhis').".csv");
    	header('Content-Type:APPLICATION/OCTET-STREAM');
    	ob_start();
    	$header = '';
		$header .= "代表大区,代表地区,编号,代表姓名,医生姓名,医院,,,,,,,,,,,,,,,,,总积分,直播积分,录播积分"."\n";
		$header_str = iconv("utf-8",'gbk',$header);
		$line = "";
		$iKey = 0;
		foreach ($oUser as $k=>$v){
			$iUserId = $v->id;
			//直播时长
			$iUserTotalLiveMin = VideoLog::where('user_id',$iUserId)->where('video_type','2')->sum('watch_minutes');
			//录播时长
			$iUserTotalReviewMin = VideoLog::where('user_id',$iUserId)->where('video_type','1')->sum('watch_minutes');
			$iUserTotalLiveScore = $iUserTotalLiveMin;
			$iUserTotalReviewScore = $iUserTotalReviewMin;
			$oUser[$k]->user_score = $iUserTotalLiveScore+$iUserTotalReviewScore;
			$oUser[$k]->user_live_score = $iUserTotalLiveScore;
			$oUser[$k]->user_review_score = $iUserTotalReviewScore;
		}
		foreach($oUser as $k=> $user){
				$sR = $user->user_regin;
				$sA = $user->user_area;
				if(is_numeric($sR)){
			   		$sRegin = User::getReginCache();
					$oRegin = json_decode($sRegin);
					$user->user_regin = $oRegin->$sR;
			   	}
				if(is_numeric($sA)){
			   		$sArea = User::getAreaCache();
					$oArea = json_decode($sArea);
					$user->user_area = $oArea->$sR->$sA;
			   	}
				$line .=  str_ireplace(',','，',$user->user_regin);
	         	$line .= ','. str_ireplace(',','，',$user->user_area);
				$line .= ','.($iKey+1);
				$line .= ','. str_ireplace(',','，',$user->user_name);
				$line .= ',';
				$line .= ',';
				$line .= ',';
				$line .= ',';
				$line .= ',';
				$line .= ',';
				$line .= ',';
				$line .= ',';
				$line .= ',';
				$line .= ',';
				$line .= ',';
				$line .= ',';
				$line .= ',';
				$line .= ',';
				$line .= ',';
				$line .= ',';
				$line .= ',';
				$line .= ',';
				$line .= ','.$user->user_score;
	         	$line .= ','.$user->user_live_score;
	         	$line .= ','.$user->user_review_score;
	         	$line .= "\n";
	         	$line .= $this->getUserExportInfo($user->id,$iKey+1,1);
	         	$iKey++;
		}
		$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	public function getUserExportInfo($iUid,$str,$count){
		$oUser = User::where('link_user_id',$iUid)->get();
		if(!count($oUser)){
			return '';
		}
		$line = '';
		foreach ($oUser as $k=>$v){
			$iUserId = $v->id;
			//直播时长
			$iUserTotalLiveMin = VideoLog::where('user_id',$iUserId)->where('video_type','2')->sum('watch_minutes');
			//录播时长
			$oReviewMinLog = VideoLog::where('user_id',$iUserId)->where('video_type','1')->get();
			$iUserTotalLiveScore = $iUserTotalLiveMin;
			$iUserTotalReviewScore = 0;
			foreach($oReviewMinLog as $key=>$val){
				$iUserTotalReviewScore += intval($val->watch_minutes/2);
			}
			$oUser[$k]->user_score = $iUserTotalLiveScore+$iUserTotalReviewScore;
			$oUser[$k]->user_live_score = $iUserTotalLiveScore;
			$oUser[$k]->user_review_score = $iUserTotalReviewScore;
		}
		foreach($oUser as $key_user=> $user){
			$c = 0;
			$line .= ',';$c++;
			$line .= ',';$c++;
			$line .= $str.'.'.($key_user+1);
			if($count==1){
				$line .= ',';$c++;
			}else{
				$line .= ',';$c++;
				for($i=0;$i<$count-1;$i++){
		         	$line .= ',';$c++;
		         	$line .= ',';$c++;
		        }
			}
			$sUserCompany = $user->user_company;
			if(is_numeric($sUserCompany)&&$sUserCompany!=0){
				$sUserCompany = Hospital::where('id',$sUserCompany)->pluck('name');
			}
			if(!$sUserCompany){
				$sUserCompany = $user->user_company_name;
			}
         	$line .= ','. str_ireplace(',','，',$user->user_name);$c++;
         	$line .= ','. str_ireplace(',','，',$sUserCompany);$c++;
         	$d = 21-$c;
			for($i=0;$i<$d;$i++){
	         	$line .= ',';$c++;
	        }
         	$line .= ','.$user->user_score;
         	$line .= ','.$user->user_live_score;
         	$line .= ','.$user->user_review_score;
         	$line .= "\n";
         	$line .= $this->getUserExportInfo($user->id,$str.'.'.($key_user+1),$count+1);
		}
		return $line;
	}
	
	public function ExportUserTel($iFlag){
		set_time_limit(0);
		$iFlagArr = explode('-',$iFlag);
		$oUser = User::where('role_id','>',1)->offset($iFlagArr[0]-1)->limit(5000)->get();
		/*if($iFlag==1){
			$oUser = User::where('role_id','>',1)->where('id','<',20000)->get();
		}elseif($iFlag==2){
			$oUser = User::where('role_id','>',1)->where('id','>=',20000)->get();
		}*/
		$aUserRegin = User::where('role_id',2)->lists('user_regin','id');
		$aReginCluster = DB::table('regin')->lists('cluster_id','id');
		$aRegin = DB::table('regin')->lists('regin_name','id');
		$aCluster = DB::table('cluster')->lists('cluster_name','id');
		$aArea = DB::table('area')->lists('area_name','id');
		$aUserArea = User::where('role_id',2)->lists('user_area','id');
		//导出csv
		header( "Cache-Control: public" );
    	header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
    	header("Content-Disposition:attachment;filename=".date('Ymdhis').".csv");
    	header('Content-Type:APPLICATION/OCTET-STREAM');
    	ob_start();
    	$header = '';
		$header .= "id,姓名,用户名,角色,性别(1:男;2:女),电话号码,邮箱,省份,城市,区/县,医院,科室,代表cluster,代表大区,代表地区,医脉通id,医师证号,注册时间"."\n";
		$header_str = iconv("utf-8",'gbk',$header);
		$line="";
		foreach($oUser as $k=>$v){
			if($v->role_id==2){
				$sRoleName = '代表';
			}else{
				$sRoleName = '医生';
			}
			$v = $this->formatUserInfo($v);
			$line .= $v->id;
			$line .= ','.$v->user_name;
			$line .= ','.$v->user_nick;
			$line .= ','.$sRoleName;
			$line .= ','.$v->user_sex;
         	$line .= ','.$v->user_tel;
         	$line .= ','.$v->user_email;
         	$line .= ','.$v->user_province;
         	$line .= ','.$v->user_city;
         	$line .= ','.$v->user_county;
         	$line .= ','.$v->user_company;
         	$line .= ','.$v->user_department;
         	if($v->role_id==2){
         		$sClusterName = (!empty($aReginCluster[$v->user_regin])&&!empty($aCluster[$aReginCluster[$v->user_regin]]))?$aCluster[$aReginCluster[$v->user_regin]]:'';
         		$line .= ','.$sClusterName;
         		$sReginName = !empty($aRegin[$v->user_regin])?$aRegin[$v->user_regin]:$v->user_regin;
         		$line .= ','.$sReginName;
         		$sAreaName = !empty($aArea[$v->user_area])?$aArea[$v->user_area]:$v->user_area;
         		$line .= ','.$sAreaName;
         	}else{
         		$iRepId = $v->link_rep_id;
         		$iReginId = !empty($aUserRegin[$iRepId])?$aUserRegin[$iRepId]:0;
         		$sClusterName = (!empty($aReginCluster[$iReginId])&&!empty($aCluster[$aReginCluster[$iReginId]]))?$aCluster[$aReginCluster[$iReginId]]:'';
         		$line .= ','.$sClusterName;
         		$sReginName = !empty($aRegin[$iReginId])?$aRegin[$iReginId]:$iReginId;
         		$line .= ','.$sReginName;
         		$iAreaId = !empty($aUserArea[$iRepId])?$aUserArea[$iRepId]:0;
         		$sAreaName = !empty($aArea[$iAreaId])?$aArea[$iAreaId]:$iAreaId;
         		$line .= ','.$sAreaName;
         	}
         	$line .= ','.$v->medlive_id;
         	$line .= ','.$v->card_number;
         	$line .= ','.$v->created_at;
         	$line .= "\n";
		}
		$file_str = iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	//我的培训列表页
	public function TrainList(){
		$oTrain = UserTrain::orderBy('start_time','DESC')->paginate(self::PAGESIZE);
		return View::make('admin.user.train.list')->with('oTrain',$oTrain);
	}
	
	//删除我的培训
	public function TrainDel($iId){
		$oTrain = UserTrain::find($iId);
		$oTrain->delete();
		return $this->showMessage('删除培训成功','/admuser/train-list');
	}
	//我的培训视频观看情况导出
	public function TrainExport($iId)
	{ 
		$sTrainTitle = UserTrain::find($iId)->pluck('train_title');
		$oUserTrainReviewLog = UserTrainReviewLog::where('train_id',$iId)
			->join('user','user_train_review_log.user_id','=','user.id')
			->select('user_train_review_log.*','user.user_name','user.user_tel','user.user_email',
			'user.user_address','user.user_province','user.user_city',
			'user.user_county','user.user_department','user.user_position',
			'user.user_company','user.user_regin','user.user_area')
			->get();
		//导出csv
		header( "Cache-Control: public" );
		header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
		header("Content-Disposition:attachment;filename=".date('Y-m-d').".csv");
		header('Content-Type:APPLICATION/OCTET-STREAM');
		ob_start();
		$header = '';
		$header .= "培训标题,用户名,用户手机号,用户邮箱,用户地址,用户省份,用户城市,科室,职称,公司,代表大区,代表地区,观看设备,观看时长,观看日期"."\n";
		$header_str =  iconv("utf-8",'gbk',$header);
		$sUserTrainMessage="";
		$aReginData = DB::table('regin')->lists('regin_name','id');
		$aAreaData = DB::table('area')->lists('area_name','id');
		foreach($oUserTrainReviewLog as $value){
		   	$sR = $value->user_regin;
		   	$sA = $value->user_area;
		   	if(is_numeric($sR)){
		   		$value->user_regin = isset($aReginData[$sR])?$aReginData[$sR]:$sR;
		   	}
		   	if(is_numeric($sA)){
		   		$value->user_area = isset($aAreaData[$sA])?$aAreaData[$sA]:$sA;
		   	}
		   	if(is_numeric($value->user_province)){
		   		$sProvince = Hospital::where('id',$value->user_province)->pluck('name');
		   		if($sProvince){
		   			$value->user_province = $sProvince;
		   		}
		   	}
		   	if(is_numeric($value->user_city)){
		   		$sCity = Hospital::where('id',$value->user_city)->pluck('name');
		   		if($sCity){
		   			$value->user_city = $sCity;
		   		}
		   	}
			$sUserTrainMessage.= $sTrainTitle.",";
			$sUserTrainMessage.= $value ->user_name.",";
			$sUserTrainMessage.= $value ->user_tel.",";
			$sUserTrainMessage.= $value ->user_email.",";
			$sUserTrainMessage.= $value ->user_address.",";
			$sUserTrainMessage.= $value ->user_province.",";
			$sUserTrainMessage.= $value ->user_city.",";
			$sUserTrainMessage.= $value ->user_department.",";
			$sUserTrainMessage.= $value ->user_position.",";
			$sUserTrainMessage.= $value ->user_company.",";
			$sUserTrainMessage.= $value ->user_regin.",";
			$sUserTrainMessage.= $value ->user_area.",";
			if($value->device == 0){
				$sUserTrainMessage.= 'WEB'.",";
			}else if($value->device == 1){
				$sUserTrainMessage.= 'App'.",";
			}else{
				$sUserTrainMessage.= '微信'.",";
			}
			$sUserTrainMessage.= $value->watch_minutes.",";
			$sUserTrainMessage.= $value->created_at.",";
			$sUserTrainMessage.= "\n";
		}
		$file_str=  iconv("utf-8",'GBK//TRANSLIT',$sUserTrainMessage);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}

	//新增培训
	public function TrainAdd(){
		return View::make('admin.user.train.add');
	}
	
	//新增培训提交
	public function TrainAddDo(){
		$aInput = Input::all();
		$rule = UserTrain::getUserTrainRule();
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
		    return $this->showMessage('请填写必填字段');
		}
		$oTrain = new UserTrain();
		//验证成功
		if($oTrain->addTrain($aInput)->save()){
			return $this->showMessage('添加培训成功','/admuser/train-list');
		}else{
			return $this->showMessage('添加培训失败');
		}
	}
	
	//编辑培训
	public function TrainEdit($id){
		$oTrain = UserTrain::find($id);
		return View::make('admin.user.train.edit')->with('oTrain',$oTrain);
	}
	
	//编辑提交
	public function TrainEditDo($id){
		$aInput = Input::all();
		$rule = UserTrain::getUserTrainRule();
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
		    return $this->showMessage('请填写必填字段');
		}
		$oTrain = UserTrain::find($id);
		if($oTrain->addTrain($aInput)->save()){
			return $this->showMessage('编辑培训成功');
		}else{
			return $this->showMessage('编辑培训失败');
		}
	}
	
	//新版我的预约
	public function NewOrder(){
		//显示所有专家信息
		$oUserOrderDoc = UserOrderDoc::get();
		return View::make('admin.user.neworder.doclist')->with('oUserOrderDoc',$oUserOrderDoc);
	}
	
	//新版我的预约--新增专家信息
	public function NewOrderAddDoc(){
		//获得所有大区
		$sRegin = User::getReginCache();
		$oRegin = json_decode($sRegin);
		return View::make('admin.user.neworder.docadd')->with('oRegin',$oRegin);
	}
	
	//新版我的预约--新增专家提交
	public function NewOrderAddDocDo(){
		$aInput = Input::all();
		$sRegin = '';
		if(isset($aInput['user_regin'])){
			foreach($aInput['user_regin'] as $k=>$v){
				$sRegin .= '|'.$v.'|';
			}
		}
		$aInput['user_regin'] = $sRegin;
		unset($aInput['upload_file']);
		$rule = UserOrderDoc::getUserOrderDocTrainRule();
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
		    return $this->showMessage('请填写必填字段');
		}
		$oUserOrderDoc = new UserOrderDoc();
		//验证成功
		if($oUserOrderDoc->addUserOrderDoc($aInput)->save()){
			return $this->showMessage('添加我的预约专家成功','/admuser/neworder');
		}else{
			return $this->showMessage('添加我的预约专家失败');
		}
	}
	
	//新版我的预约--编辑
	public function NewOrderEditDoc($id){
		$oUserOrderDoc = UserOrderDoc::find($id);
		//获得所有大区
		$sRegin = User::getReginCache();
		$oRegin = json_decode($sRegin);
		return View::make('admin.user.neworder.docedit')->with('oUserOrderDoc',$oUserOrderDoc)->with('oRegin',$oRegin);
	}
	
	//新版我的预约--编辑提交
	public function NewOrderEditDocDo($id){
		$aInput = Input::all();
		$sRegin = '';
		if(isset($aInput['user_regin'])){
			foreach($aInput['user_regin'] as $k=>$v){
				$sRegin .= '|'.$v.'|';
			}
		}
		$aInput['user_regin'] = $sRegin;
		unset($aInput['upload_file']);
		$rule = UserOrderDoc::getUserOrderDocTrainRule();
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
		    return $this->showMessage('请填写必填字段');
		}
		$oUserOrderDoc = UserOrderDoc::find($id);
		if($oUserOrderDoc->addUserOrderDoc($aInput)->save()){
			return $this->showMessage('编辑成功');
		}else{
			return $this->showMessage('编辑失败');
		}
	}
	
	//新版我的预约--删除专家
	public function DelOrderDoc($iId){
		$oDoc = UserOrderDoc::find($iId);
		$oDoc->delete();
		return $this->showMessage('删除可预约专家成功','/admuser/neworder');
	}
	
	public function Other(){
		return View::make('admin.user.other');
	}
	
	//20150824代表大区地区信息更新
	public function UpdateUserReginArea(){
		return View::make('admin.user.updateuser');
	}
	
	public function UpdateUserReginAreaDo(){
		set_time_limit(0);
		$oUser = new User();
		$result = $oUser->UpdateUserReginAreaDo($_FILES);
		return $this->showmessage($result);
	}
	
	//病例管理权限分配
	public function UserAdminCase($iUId,$iType=1){
		$oUserAdminCase = UserAdminCase::where('user_id',$iUId)->where('type',$iType)->first();
		if($oUserAdminCase){
			//删除该用户
			$oUserAdminCase->delete();
		}else{
			$aUserAdminCase = array('user_id'=>$iUId,'type'=>$iType);
			$oUserAdminCase = new UserAdminCase($aUserAdminCase);
			$oUserAdminCase->save();
		}
		return 'success';
	}
	
	//西区ME用户积分等信息导出
	public function Xqmeexpory(){
		set_time_limit(0);
		$oUser = User::select('id','user_regin','user_area','user_name','invite_code','user_cwid','user_province','user_city','user_tel','user_score','user_live_score','user_review_score','user_position','created_at')
		->where('role_id',2)->where('user_regin','>=',28)->where('user_regin','<=',33);
		$oUser = $oUser->get();
		//导出csv
		header( "Cache-Control: public" );
		header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
		header("Content-Disposition:attachment;filename=".date('Ymdhis').".csv");
		header('Content-Type:APPLICATION/OCTET-STREAM');
		ob_start();
		$header = '';
		$header .= "身份,用户姓名,用户编码,总积分,直播总积分,录播总积分,用户层级,上级用户姓名,上级用户编码,最终代表,代表CWID,地区,大区,cluster,省份,城市,区域,医院,科室,职称,注册时间"."\n";
		$header_str =  iconv("utf-8",'gbk',$header);
		$line="";
		$iKey = 0;
		$aReginData = DB::table('regin')->lists('regin_name','id');
		$aAreaData = DB::table('area')->lists('area_name','id');
		foreach($oUser as $k=> $user){
			if($user->user_regin>=28&&$user->user_regin<=33){
				$line_pre = '';
				$sR = $user->user_regin;
				$sA = $user->user_area;
				$cluster = User::getClusterByReginId2($sR);
				if(is_numeric($sR)){
					$user->user_regin = isset($aReginData[$sR])?$aReginData[$sR]:$sR;
				}
				if(is_numeric($sA)){
					$user->user_area = isset($aAreaData[$sA])?$aAreaData[$sA]:$sA;
				}
				$user = $this->formatUserInfo($user);
				$line .= '代表';
				$line .= ','. str_ireplace(',','，',$user->user_name);
				$line .= ','. str_ireplace(',','，',"\t".$user->invite_code);
				$line .= ','.$user->user_score;
				$line .= ','.$user->user_live_score;
				$line .= ','.$user->user_review_score;
				$line .= ',0';
				$line .= ',';
				$line .= ',';
				$line .= ',';
				$line .= ','. str_ireplace(',','，',$user->user_cwid);
				$line .= ','. str_ireplace(',','，',$user->user_area);
				$line .= ','.str_ireplace(',','，',$user->user_regin);
				$line .= ','.$cluster;
				$line .= ','. str_ireplace(',','，',$user->user_province);
				$line .= ','. str_ireplace(',','，',$user->user_city);
				$line .= ',';
				$line .= ',';
				$line .= ',';
				$line_pre .= ','. str_ireplace(',','，',$user->user_name);
				$line_pre .= ','. str_ireplace(',','，',$user->user_cwid);
				$line_pre .= ','. str_ireplace(',','，',$user->user_area);
				$line_pre .= ','.str_ireplace(',','，',$user->user_regin);
				$line_pre .= ','.$cluster;
				$line .= ','. str_ireplace(',','，',$user->user_position);
				$line .= ','.$user->created_at;
				$line .= "\n";
				$line .= $this->getXqmeexpory($user->id,$iKey+1,1,$line_pre,0);
				$iKey++;
			}
		}
		$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	public function getXqmeexpory($iUid,$str,$count,$line_pre='',$iUserLevel){
		$iUserLevel = $iUserLevel+1;
		$oUser = User::select(
			'id','link_user_id','user_name','invite_code','user_province','user_city',
			'user_county','user_company','user_company_name','user_department','user_tel',
			'user_score','user_live_score','user_review_score','user_position','created_at')
			->where('link_user_id',$iUid)
			->get();
		if(!count($oUser)){ return ''; }
		$line = '';
		foreach($oUser as $key_user=> $user){
			//上级用户
			$oLinkUser = User::select('user_name','invite_code')->where('id',$user->link_user_id)->first();
			if($oLinkUser){
				$oLinkUserName = $oLinkUser->user_name;
				$oLinkUserCode = "\t".$oLinkUser->invite_code;
			}else{
				$oLinkUserName = '';
				$oLinkUserCode = '';
			}
			$user = $this->formatUserInfo($user);
			$line .= '医生';
			$line .= ','. str_ireplace(',','，',$user->user_name);
			$line .= ','. str_ireplace(',','，',$user->invite_code);
			$line .= ','.$user->user_score;
			$line .= ','.$user->user_live_score;
			$line .= ','.$user->user_review_score;
			$line .= ','.$iUserLevel;
			$line .= ','. str_ireplace(',','，',$oLinkUserName);
			$line .= ','. str_ireplace(',','，',$oLinkUserCode);
			$line .= $line_pre;
			$line .= ','. str_ireplace(',','，',$user->user_province);
			$line .= ','. str_ireplace(',','，',$user->user_city);
			$line .= ','. str_ireplace(',','，',$user->user_county);
			$line .= ','. str_ireplace(',','，',$user->user_company);
			$line .= ','. str_ireplace(',','，',$user->user_department);
			$line .= ','. str_ireplace(',','，',$user->user_position);
			$line .= ','.$user->created_at;
			$line .= "\n";
			$line .= $this->getXqmeexpory($user->id,$str.'.'.($key_user+1),$count+1,$line_pre,$iUserLevel);
		}
		return $line;
	}
	
	//医生用户关联代表批量处理
	public function UserRepRelate(){
		set_time_limit(0);
		for($i=1;$i<30000;$i++){
			$oUser = User::find($i);
			if($oUser&&$oUser->role_id==3&&!$oUser->link_rep_id){
				$iRepId = User::getRepId($oUser->superior_ids);
				$oUser->link_rep_id = $iRepId;
				$oUser->save();
			}
		}
		return 'success';
	}
	
	//已导入医脉通用户后，慢病用户关联医脉通id批量处理
	public function UserMedliveRelate(){
		set_time_limit(0);
		$aMedliveUser = DB::table('__user_info_match_1209')->select('id','medlive_id','mobile')->where('medlive_id','!=',0)->get();
		$aMedliveUserErrorLog = DB::table('__user_info_match_1209')->select('id','is_error')->where('medlive_id','!=',0)->lists('is_error','id');
		foreach($aMedliveUser as $k=>$v){
			$iUserId = $v->id;
			$iMedliveId = $v->medlive_id;
			$oUser = User::find($iUserId);
			if($oUser&&!$oUser->medlive_id){
				$oUser->medlive_id = $iMedliveId;
				$oUser->save();
			}else{
				continue;
			}
			$iSuccessFlag = 1;
			$sSuccessMsg = '';
			$sPassword = '';		//初始密码
			if(!empty($aMedliveUserErrorLog[$iUserId])&&$aMedliveUserErrorLog[$iUserId]=='Y'){
				$sSuccessMsg = "医脉通导入数据，医脉通已有该用户成功关联医脉通id";
				$iSuccessFlag = 2;
			}else{
				$sSuccessMsg = "医脉通导入数据，注册医脉通成功";
				$iSuccessFlag = 1;
				//该批用户需要获取用户的默认初始密码
				if($v->mobile){
					$sPassword = substr($v->mobile, -6,6);
				}else{
					$sPassword = '123456';
				}
			}
			//存注册log
			$aUserMedliveRegLog = array(
				'user_id'		=> $oUser->id,
				'medlive_id'	=> $iMedliveId,
				'success_flag'	=> $iSuccessFlag,
				'success_msg'	=> $sSuccessMsg,
				'default_password'=> $sPassword
			);
			$oUserMedliveRegLog = new UserMedliveRegLog($aUserMedliveRegLog);
			$oUserMedliveRegLog->save();
		}
		return 'success';
	}
	
	//为未关联医脉通id的用户批量注册
	public function UserMedliveReg(){
		set_time_limit(0);
		$oUser = User::where('role_id',3)->where('medlive_id',0)->get();
		if(!count($oUser)){
			return 'has already register all users';
		}
		foreach($oUser as $k=>$v){
			//为用户注册医脉通
			User::RegMedlive($v);
		}
		return 'success';
	}
	
	/**
	 * 更新医生关联代表
	 * 
	 */
	public function getUpdateDocLinkRep(){
		return View::make('admin.user.link');
	}
	
	public function postUpdateDocLinkRepDo(){
		$iRepId = Input::get('rep_id');
		$sDocIds = Input::get('doc_ids');
		//查找该id代表是否存在
		$oRep = User::find($iRepId);
		if(!$oRep||$oRep->role_id!=2){
			//不是代表
			return json_encode(array('success'=>false,'msg'=>'不存在该id的代表'));
		}
		$aDocId = explode(',', $sDocIds);
		foreach($aDocId as $kdoc=>$vdoc){
			$oDoc = User::find($vdoc);
			if($oDoc&&$oDoc->role_id==3){
				//医生
				$oDoc->link_user_id = $iRepId;
				$oDoc->link_rep_id = $iRepId;
				$oDoc->superior_ids = serialize(array($iRepId));
				$oDoc->save();
				//更新该医生下所有医生到该代表名下
				$this->UpdateLinkDoc($vdoc,$iRepId);
			}else{
				//代表
				$aDocIds = User::where('link_user_id',$vdoc)->lists('id');
				foreach($aDocIds as $vdoc2){
					$oDoc = User::find($vdoc2);
					if($oDoc&&$oDoc->role_id==3){
						//医生
						$oDoc->link_user_id = $iRepId;
						$oDoc->link_rep_id = $iRepId;
						$oDoc->superior_ids = serialize(array($iRepId));
						$oDoc->save();
						//更新该医生下所有医生到该代表名下
						$this->UpdateLinkDoc($vdoc2,$iRepId);
					}
				}
			}
		}
		return json_encode(array('success'=>true,'msg'=>'更新成功'));
	}
	
	public function UpdateLinkDoc($iDocId,$iRepId){
		$oDocs = User::where('link_user_id',$iDocId)->get();
		if($oDocs){
			foreach($oDocs as $k=>$v){
				$oDoc = User::find($v->id);
				$oDoc->link_rep_id = $iRepId;
				$oSuperior = User::where('id',$oDoc->link_user_id)->first();
				if ($oSuperior->superior_ids){
					$aSuperiorIds = unserialize($oSuperior->superior_ids);
					array_push($aSuperiorIds, $oDoc->link_user_id);
					$oDoc->superior_ids = serialize($aSuperiorIds);
				}else{
					$oDoc->superior_ids = serialize(array($oDoc->link_user_id));
				}
				$oDoc->save();
				//更新该医生下所有医生到该代表名下
				$this->UpdateLinkDoc($v->id,$iRepId);
			}
		}
	}
	
	/**
	 * 分组列表页
	 */
	public function getListGroup()
	{
		//取出所有分组用户
		$oUsers = User::where('role_id','=',2)->where('rep_type',3)->get();
		return View::make('admin.user.group.list')->with('oUsers',$oUsers);
	}
	
	/**
	 * 新增分组
	 */
	public function getAddGroup(){
		return View::make('admin.user.group.add');
	}
	
	/**
	 * 新增分组提交
	 */
	public function postAddGroupdo(){
		$sName = Input::get('user_name','');
		if(!$sName){
			return Redirect::to('/admuser/add-group');
		}
		//生成一个新的邀请码
		$sCode = User::MakeInviteCode();
		$aUser = array(
			'user_name'		=> $sName,
			'invite_code'	=> $sCode
		);
		$oUser = new User();
		//验证成功
		$oUser->addGroup($aUser)->save();
		$oUser->user_nick = $oUser->id;
		$oUser->save();
		return Redirect::to('/admuser/list-group');
	}
	
	/**
	 * 编辑分组
	 */
	public function getEditGroup($iId){
		$oUser = User::find($iId);
		if(!$oUser){
			return Redirect::to('/admuser/list-group');
		}
		return View::make('admin.user.group.edit')->with('oUser',$oUser);
	}
	
	/**
	 * 编辑分组提交
	 */
	public function postEditGroupDo($iId){
		$sName = Input::get('user_name','');
		if(!$sName){
			return Redirect::to('/admuser/edit-group/'.$iId);
		}
		$oUser = User::find($iId);
		$oUser->user_name = $sName;
		$oUser->save();
		return $this->showMessage('编辑分组成功','/admuser/list-group');
	}
	
	/**
	 * 删除分组
	 */
	public function getDelGroup($iId){
		//判断该分组是否已经有医生注册
		$iCount = User::where('link_rep_id',$iId)->count();
		if($iCount){
			return $this->showMessage('无法删除该分组，因为已经有医生在该分组注册','/admuser/list-group');
		}
		$oUser = User::find($iId);
		$oUser->delete();
		return $this->showMessage('删除分组成功','/admuser/list-group');
	}
	
	//*****************线下会议签到 start****************
	 /**
	  * 列表
	  */
	 public function getOfflineList(){
	 	$oOffline = OfflineMeetingSign::orderBy('id','DESC')->paginate(15);;
	 	return View::make('admin.user.offline.list')->with('oOffline',$oOffline);
	 }
	 
	 public function getOfflineAdd(){
	 	$iOfflineLastId = OfflineMeetingSign::orderBy('id','DESC')->pluck('meeting_id');
	 	if(!$iOfflineLastId){
	 		$iOfflineLastId = 100000;
	 	}
	 	$aOffline = array('meeting_id'=>($iOfflineLastId+1));
	 	$oOffline = new OfflineMeetingSign($aOffline);
	 	$oOffline->save();
	 	return $this->showMessage('添加成功','/admuser/offline-list');
	 }
	 
	//*****************线下会议签到   end ****************
	/**
	 * 商务标签用户数据更新
	 */ 
	public function UpdateShangwuUser(){
		$oUser = User::where('link_rep_id',40628)->where('is_h5','!=',3)->where('created_at','>','2017-08-14 00:00:00')->get();
		if(count($oUser)){
			foreach($oUser as $k=>$v){
				$v->is_h5 = 3;
				$v->save();
			}
			$iCount = count($oUser);
			return '成功将代表（id为40628）名下的'.$iCount.'名医生更新为商务用户';
		}else{
			return '无需更新';
		}
	}

	//后台首页
	public function ExportUserInfo($iFlag){
		set_time_limit(0);
		$iFlagArr = explode('-',$iFlag);
		//导出csv
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.date('Ymdhis').'.xls"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		// header( "Cache-Control: public" );
		// header( "Pragma: public" );
		// header("Content-type:application/vnd.ms-excel");
		// header("Content-Disposition:attachment;filename=".date('Ymdhis').".csv");
		// header('Content-Type:APPLICATION/OCTET-STREAM');
    	ob_start();
		//取出所有用户
		$oUsers = User::where('role_id','!=',1);  
		$input = Input::all();
        $aLink = array();
		if(isset($input['user_type'])&&$input['user_type']){
			$oUsers = $oUsers->where('user_type','=',$input['user_type']);
			$aLink['user_type'] = $input['user_type'];
		}
		if(isset($input['role_id'])&&$input['role_id']){
			$oUsers = $oUsers->where('role_id','=',$input['role_id']);
			$aLink['role_id'] = $input['role_id'];
		}
		if(isset($input['user_name'])&&$input['user_name']){
			$oUsers = $oUsers->where('user_name','LIKE',"%".$input['user_name']."%");
			$aLink['user_name'] = $input['user_name'];
		}
		if(isset($input['user_cwid'])&&$input['user_cwid']){
			$oUsers = $oUsers->where('user_cwid','LIKE',"%".$input['user_cwid']."%");
			$aLink['user_cwid'] = $input['user_cwid'];
		}
		if(isset($input['invite_code'])&&$input['invite_code']){
			$oUsers = $oUsers->where('invite_code','LIKE',"%".$input['invite_code']."%");
			$aLink['invite_code'] = $input['invite_code'];
		}
		if(isset($input['user_nick'])&&$input['user_nick']){
			$oUsers = $oUsers->where('user_nick','LIKE',"%".$input['user_nick']."%");
			$aLink['user_nick'] = $input['user_nick'];
		}
		if(isset($input['user_tel'])&&$input['user_tel']){
			$oUsers = $oUsers->where('user_tel','LIKE',"%".$input['user_tel']."%");
			$aLink['user_tel'] = $input['user_tel'];
		}
		if(isset($input['user_email'])&&$input['user_email']){
			$oUsers = $oUsers->where('user_email','LIKE',"%".$input['user_email']."%");
			$aLink['user_email'] = $input['user_email'];
		}
		if(isset($input['link_rep_id'])&&$input['link_rep_id']){
			$oUsers = $oUsers->where('link_rep_id',$input['link_rep_id']);
			$aLink['link_rep_id'] = $input['link_rep_id'];
		}
		if(isset($input['link_user_id'])&&$input['link_user_id']){
			$oUsers = $oUsers->where('link_user_id',$input['link_user_id']);
			$aLink['link_user_id'] = $input['link_user_id'];
		}
		if(isset($input['case_checkbox'])){
			$aUserAdminCase = UserAdminCase::lists('user_id','id');
			$oUsers = $oUsers->whereIn('id',$aUserAdminCase);
		}
		$num = User::where('role_id','!=',1)->count();
		$pageNum = 5000;
		if($num > $pageNum){
			$pages = ceil($num/$pageNum);
			for($i=1; $i<=$pages; $i++){
				$start = ($i-1)*$pageNum+1;
				$end = $i*$pageNum;
				if($i == $pages) $end = $num-($i-1)*$pageNum;
				$aTotalNum[] = "{$start}-{$end}";
			}
		}else{
			$aTotalNum[] = "1-{$num}";
		}

		$oUsers = $oUsers->offset($iFlagArr[0]-1)->limit(5000)->get();
		// $oUsers = $oUsers->paginate(self::PAGESIZE);
		// $log = DB::getQueryLog();
		// print_r($log);die;
		foreach ($oUsers as $k=>$v){
			$iUserType = $v -> user_type;
			switch($iUserType){
				case 1:
					$v ->user_type = "拜新同用户";
					break;
				case 2:
					$v ->user_type = "拜阿用户";
					break; 
				case 3:
					$v ->user_type = "拜新同和拜阿用户";
					break;
				case 0:
					$v ->user_type = '尚未划分';
			} 
			$iUserId = $v->id;
			//直播时长
			$iUserTotalLiveMin = VideoLog::where('user_id',$iUserId)->where('video_type','2')->sum('watch_minutes');
			//录播时长
			$iUserTotalReviewMin = VideoLog::where('user_id',$iUserId)->where('video_type','1')->sum('watch_minutes');
			$iUserTotalLiveScore = $iUserTotalLiveMin;
			$iUserTotalReviewScore = $iUserTotalReviewMin;
			$oUsers[$k]->user_score = $iUserTotalLiveScore+$iUserTotalReviewScore;
			$oUsers[$k]->user_live_score = $iUserTotalLiveScore;
			$oUsers[$k]->user_review_score = $iUserTotalReviewScore;
		}
		$aRole = DB::table('role')->lists('name','id');
		//获取所有有病例管理权限的用户
		$aUserAdminCase1 = UserAdminCase::where('type',1)->lists('user_id','id');
		$aUserAdminCase2 = UserAdminCase::where('type',2)->lists('user_id','id');
		//获取所有代表
		$aRepName = User::where('role_id',2)->lists('user_name','id');
		$aRepCode = User::where('role_id',2)->lists('invite_code','id');
		return View::make('admin.user.exportlist')
			->with('oUsers',$oUsers)
			->with('aRole',$aRole)
			->with('aLink',$aLink)
			->with('aUserAdminCase1',$aUserAdminCase1)
			->with('aUserAdminCase2',$aUserAdminCase2)
			->with('aRepName',$aRepName)
			->with('aRepCode',$aRepCode)
			->with('aTotalNum',$aTotalNum);
	}

}