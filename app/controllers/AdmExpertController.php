<?php

/**
 * 专家大会诊
 * @author gaoke
 */
class AdmExpertController extends BaseController {

	CONST  PAGESIZE = 10;
	public $_time   = 0;
	public $_formatTime = '';

	public function __construct(){
		//parent::__construct();
		//$this->beforeFilter('apilogin', array('except'=>array( 'postRegister','postCheckCode','postLogin')));
		global $iUserId;
		$this->iUserId = &$iUserId;
		$this->_time = time();
		$this->_formatTime  = date("Y-m-d H:i:s",$this->_time);
	}

	//同步已有数据，找到医生的所有上级节点更新到user表中字段superior_ids
	public function getDoctor(){
		$oUers = User::where('link_user_id','>',0)->where('role_id','=','3')->get();
		$i=0;
		foreach ($oUers as $user){
			if ($user->link_user_id){
				$aSuperior = array($user->link_user_id);
				$oSuperior = User::where('id',$user->link_user_id)->first();
				if ($oSuperior->link_user_id){
					array_push($aSuperior, $oSuperior->link_user_id);
					$oSuperior3 = User::where('id',$oSuperior->link_user_id)->first();
					if ($oSuperior3->link_user_id){
						array_push($aSuperior, $oSuperior3->link_user_id);
					}
				}
			}
			$oUpdate = User::find($user->id); 
			$oUpdate->superior_ids = serialize($aSuperior);
			$oUpdate->save();
			$i++;
			echo $i."=====ok<br/>";
		}
		//print_r(DB::getQueryLog());
	}
	
	public function getList($iType){
		//取出所有视频
		$oVideos = Consultation::findAllVideo($iType)->paginate(self::PAGESIZE);
		//取得所有专家
		$oDocs = FaceDoc::orderByCreatedAt()->lists('doc_name','id');
		return  View::make('admin.consultation.list')
			->with('oVideos',$oVideos)
			->with('oDocs',$oDocs)
			->with('type',$iType);
	}
	
	//新增
	public function getAdd($iType){
		//取得所有专家
		$oDocs = FaceDoc::orderByCreatedAt()->lists('doc_name','id');
		return  View::make('admin.consultation.add')
			->with('oDocs',$oDocs)
			->with('type',$iType);
	}
	
	//新增提交
	public function postAddDo($type){
		$aInput = Input::all();
		if(!isset($aInput['recommend'])){//推荐位设置，dbn add
			$aInput['recommend'] = 0;
		}
		unset($aInput['upload_file']);
		$rule = Consultation::getVideoRule();
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
		    return $this->showMessage('请按照要求填写必填字段');
		}
		$aInput['meeting_type'] = $type;
		$oConsultation = new Consultation();
		//验证成功
		if($oConsultation->addVideo($aInput)->save()){
			return $this->showMessage('新增专家大会诊成功','/admexpert/add/'.$type);
		}else{
			return $this->showMessage('添加失败');
		}
	}
	
	//编辑
	public function getEdit($id){
		$oVideo = Consultation::find($id);
		//取得所有专家
		$oDocs = FaceDoc::orderByCreatedAt()->lists('doc_name','id');
		return  View::make('admin.consultation.edit')
			->with('oVideo',$oVideo)
			->with('oDocs',$oDocs);
	}
	
	//编辑提交
	public function postEditDo($id=0){
		$aInput = Input::all();
		if(!isset($aInput['recommend'])){//推荐位设置，dbn add
			$aInput['recommend'] = 0;
		}
		unset($aInput['upload_file']);
		$rule = Consultation::getVideoRule();
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
		    return $this->showMessage('请按照要求填写必填字段');
		}
		$oConsultation = Consultation::find($id);
		if($oConsultation->addVideo($aInput,false)->save()){
			return $this->showMessage('编辑成功');
		}else{
			return $this->showMessage('编辑失败');
		}
	}
	
	//删除
	public function getDel($id){
		$oConsultation = Consultation::find($id);
		$type = $oConsultation->meeting_type;
		$oConsultation->delete();
		return $this->showMessage('删除成功','/admexpert/list/'.$type);
	}
	
	//话题列表
	public function getTopic($id){
		$oTopic = ConsultationTopic::where('meeting_id',$id)->orderBy('id','ASC')->get();
		return View::make('admin.consultation.topic')
			->with('oTopic',$oTopic)
			->with('id',$id);
	}
	
	//添加话题
	public function postAddTopic($id){
		$input = Input::all();
		if($input['optionnum']==1){
			return $this->showMessage('请至少填写一个话题');
		}
		//删除所有话题
		$oTopic = ConsultationTopic::where('meeting_id',$id);
		$oTopic->delete();
		foreach($input as $k=>$v){
			if($k!='optionnum'){
				if($v){
					$aTopic = array(
						'meeting_id'	=>$id,
						'topic_title'	=> $v		
					);
					$oTopic = new ConsultationTopic($aTopic);
					$oTopic->save();
				}
			}
		}
		return $this->showMessage('添加话题成功');
	}
	
	public function getApply($iId){
		//获取所有话题
		$oTopic = ConsultationTopic::where('meeting_id',$iId)->orderBy('id','ASC')->get();
		$aTopic = ConsultationTopic::where('meeting_id',$iId)->orderBy('id','ASC')->lists('topic_title','id');
		$oConUserReg = ConsultationUserReg::where('meeting_id',$iId)->paginate(self::PAGESIZE);
		foreach($oConUserReg as $k=>$v){
			$iUserId = $v->user_id;
			$iMeetingId = $v->meeting_id;
			$oUser = User::find($iUserId);
			$oConUserReg[$k]->user_info = $oUser;
			$aUserAnswer = ConsultationTopicAnswer::where('meeting_id',$iMeetingId)
				->where('user_id',$iUserId)
				->lists('user_answer','topic_id');
			$oConUserReg[$k]->user_answer = $aUserAnswer;
		}
		return View::make('admin.consultation.apply')
			->with('oTopic',$oTopic)
			->with('oConUserReg',$oConUserReg)
			->with('aTopic',$aTopic)
			->with('iId',$iId);
	}
	
	public function getExportApply($iId){
		//获取所有话题
		$oTopic = ConsultationTopic::where('meeting_id',$iId)->orderBy('id','ASC')->get();
		$aTopic = ConsultationTopic::where('meeting_id',$iId)->orderBy('id','ASC')->lists('topic_title','id');
		$oConUserReg = ConsultationUserReg::where('meeting_id',$iId)->get();
		foreach($oConUserReg as $k=>$v){
			$iUserId = $v->user_id;
			$iMeetingId = $v->meeting_id;
			$oUser = User::find($iUserId);
			$oConUserReg[$k]->user_info = $oUser;
			$aUserAnswer = ConsultationTopicAnswer::where('meeting_id',$iMeetingId)
				->where('user_id',$iUserId)
				->lists('user_answer','topic_id');
			$oConUserReg[$k]->user_answer = $aUserAnswer;
		}
		//导出csv
		header( "Cache-Control: public" );
    	header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
    	header("Content-Disposition:attachment;filename=apply_".date('Ymdhis').".csv");
    	header('Content-Type:APPLICATION/OCTET-STREAM');
    	ob_start();
    	$header = '';
    	foreach($oTopic as $k=>$v){
    		$header .= "话题".($k+1)."：,".$v->topic_title."\n";
    	}
		$header .= "编号,用户名,用户姓名,邮箱,手机号码";
		foreach($oTopic as $k=>$v){
			$header .= ",话题".($k+1);
		}
		$header .= ",报名时间\n";
		$header_str =  iconv("utf-8",'gbk',$header);
		$line="";
		foreach($oConUserReg as $k=>$v){
         	$line .= ($k+1);
         	if($v->user_info){
         		$line .= ','. str_ireplace(',','，',$v->user_info->user_nick).','. str_ireplace(',','，',$v->user_info->user_name).','. str_ireplace(',','，',$v->user_info->user_email).','. str_ireplace(',','，',$v->user_info->user_tel);
         	}else{
         		$line .= ',,,,';
         	}
         	foreach($aTopic as $key=>$val){
         		$value = $v->user_answer;
         		if(isset($value[$key])){
         			$line .= ','.$value[$key];
         		}else{
         			$line .= ',';
         		}
         	}
         	$line .= ','.$v->created_at."\n";
		}
		$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	/**
	 * 获取专家大会诊用户信息
	 */
	public function getUserinfo($iType=0){
		set_time_limit(0);
		$oUser = User::select('id','user_regin','user_nick','user_area','user_name','invite_code','user_cwid','user_province','user_city')->where('role_id',2);
		if($iType==1){
			$oUser = $oUser->where('id','<','1000');
		}else if($iType==2){
			$oUser = $oUser->where('id','>=','1000');
		}
		$oUser = $oUser->get();
		$line="";
		$iKey = 0;
		$aReginData = DB::table('regin')->lists('regin_name','id');
		$aAreaData = DB::table('area')->lists('area_name','id');
		foreach($oUser as $k=> $user){
			$line_pre = '';
			$sR = $user->user_regin;
			$sA = $user->user_area;
			$cluster = User::getClusterByReginId2($sR);
			if(is_numeric($sR)){
				$user->user_regin = isset($aReginData[$sR])?$aReginData[$sR]:$sR;
		   	}
			if(is_numeric($sA)){
				$user->user_area = isset($aAreaData[$sA])?$aAreaData[$sA]:$sA;
		   	}
			$user = $this->formatUserInfo($user);
			$line .=  str_ireplace(',','，',$user->user_nick);
			$line .= ','. str_ireplace(',','，',$user->user_name);
			$line .= ','. str_ireplace(',','，',$user->invite_code);
			$line .= ',代表';
			$line .= ',';
			$line .= ','.str_ireplace(',','，',$user->user_regin);
			$line .= ','. str_ireplace(',','，',$user->user_area);
			$line .= ','.$cluster;
			$line_pre .= ','. str_ireplace(',','，',$user->user_name);
			$line_pre .= ','.str_ireplace(',','，',$user->user_regin);
			$line_pre .= ','. str_ireplace(',','，',$user->user_area);
			$line_pre .= ','.$cluster;
         	$line .= "\n";
         	$line .= $this->__getUserInfo($user->id,$line_pre);
         	$iKey++;
		}
		var_dump($line);exit;
		//导出csv
		header( "Cache-Control: public" );
    	header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
    	header("Content-Disposition:attachment;filename=".date('Ymdhis').".csv");
    	header('Content-Type:APPLICATION/OCTET-STREAM');
    	ob_start();
    	$header = '';
		$header .= "用户名,用户姓名,用户编码,身份,最终代表姓名,代表所属大区,代表所属地区,Cluster"."\n";
		$header_str =  iconv("utf-8",'gbk',$header);
		$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	public function __getUserInfo($iUid,$line_pre=''){
		$oUser = User::select('id','link_user_id','user_name','user_nick','invite_code','user_province','user_city','user_county','user_company','user_company_name','user_department')
			->where('link_user_id',$iUid)
			->get();
		if(!count($oUser)){
			return '';
		}
		$line = '';
		foreach($oUser as $key_user=> $user){
			//上级用户
			$oLinkUser = User::select('user_name','invite_code')->where('id',$user->link_user_id)->first();
			$user = $this->formatUserInfo($user);
			$line .=  str_ireplace(',','，',$user->user_nick);
			$line .= ','. str_ireplace(',','，',$user->user_name);
			$line .= ','. str_ireplace(',','，',$user->invite_code);
			$line .= ',医生';
			$line .= $line_pre;
	     	$line .= "\n";
	     	$line .= $this->__getUserInfo($user->id,$line_pre);
		}
		return $line;
	}
	
}