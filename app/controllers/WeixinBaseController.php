<?php

class WeixinBaseController
{
    private $echostr;
    private $signature;
    private $timestamp;
    private $nonce;
    private static $jsonUrl = __FILE__;

    public function __construct()
    {
    }

    public function makeInfo($info)
    {
        $this->echostr = isset($info['echostr']) ? $info['echostr'] : '';
        $this->signature = isset($info['signature']) ? $info['signature'] : '';
        $this->timestamp = isset($info['timestamp']) ? $info['timestamp'] : '';
        $this->nonce = isset($info['nonce']) ? $info['nonce'] : '';
    }

    public function valid()
    {
        $echoStr = $this->echostr;
        if ($this->checkSignature()) {
            echo $echoStr;
            exit;
        }
    }

    private function checkSignature()
    {
        $signature = $this->signature;
        $timestamp = $this->timestamp;
        $nonce = $this->nonce;
        $token = TOKEN;
        $tmpArr = array($token, $timestamp, $nonce);
        sort($tmpArr, SORT_STRING);
        $tmpStr = implode($tmpArr);
        $tmpStr = sha1($tmpStr);
        if ($tmpStr == $signature) {
            return true;
        } else {
            return false;
        }
    }

    public function getBasePreview($data, $newsType)
    {
        $accessToken = self::getAccessToken($newsType);
        $url = "https://api.weixin.qq.com/cgi-bin/message/mass/preview?access_token=" . $accessToken;
        $res = json_decode(self::httpGet($url, $data));
        return $res;
    }

    public function getBasePush($data, $newsType)
    {
        $accessToken = self::getAccessToken($newsType);
        $url = "https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=" . $accessToken;
        $res = json_decode(self::httpGet($url, $data));
        return $res;
    }

    public function getBaseAddTemporaryMaterial($data, $type, $newsType)
    {
        $accessToken = self::getAccessToken($newsType);
        $url = "https://api.weixin.qq.com/cgi-bin/media/upload?access_token=" . $accessToken . "&type=" . $type;
        $res = json_decode(self::httpGet($url, $data));
        return $res;
    }

    public function getBaseAddTemporaryMaterialNews($data, $newsType)
    {
        $accessToken = self::getAccessToken($newsType);
        $url = "https://api.weixin.qq.com/cgi-bin/media/uploadnews?access_token=" . $accessToken;
        $res = json_decode(self::httpGet($url, $data));
        return $res;
    }

    public static function getBaseAddMaterialContentImg($data)
    {
        //后台编辑器上传图片时不区分微信号，都用第一个accesstoken获取图片链接
        $type = 1;
        $accessToken = self::getAccessToken($type);
        $url = "https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=" . $accessToken;
        $res = json_decode(self::httpGet($url, $data));
        return $res;
    }

    public static function getAccessToken($newsType)
    {
        // access_token 应该全局存储与更新，以下代码以写入到文件中做示例  （三个微信号对应三个token文件）
        $baseurl = dirname(dirname(dirname(self::$jsonUrl))) . '/public/json/';
        /*******每次获取token时更新第一个文件中的token，保证始终为最新值 （编辑器上传图片时无法获取接口返回的token，用直接读取文件的方式）**/
        $file = $baseurl . "access_token1.json";
        if (file_exists($file)) {
            $data = json_decode(file_get_contents($file));
        }
        if (!isset($data) || $data->expire_time < time()) {
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" . APPID1 . "&secret=" . APPSECRET1;
            $res = json_decode(self::httpGet($url));
            $access_token = $res->access_token;
            if (!isset($data)) {
                $data = new stdClass();
            }
            if ($access_token) {
                $data->expire_time = time() + 7000;
                $data->access_token = $access_token;
                $fp = fopen($baseurl . "access_token1.json", "w");
                fwrite($fp, json_encode($data));
                fclose($fp);
            }
        }
        /*************/
        //慢性疾病与转化医学
        if ($newsType == 1) {
            $file = $baseurl . "access_token1.json";
            if (file_exists($file)) {
                $data = json_decode(file_get_contents($file));
            }
            $access_token = $data->access_token;
            //心内空间
        } elseif ($newsType == 2) {
            $file = $baseurl . "access_token2.json";
            if (file_exists($file)) {
                $data = json_decode(file_get_contents($file));
            }
            if (!isset($data) || $data->expire_time < time()) {
                $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" . APPID2 . "&secret=" . APPSECRET2;
                $res = json_decode(self::httpGet($url));
                $access_token = $res->access_token;
                if (!isset($data)) {
                    $data = new stdClass();
                }
                if ($access_token) {
                    $data->expire_time = time() + 7000;
                    $data->access_token = $access_token;
                    $fp = fopen($baseurl . "access_token2.json", "w");
                    fwrite($fp, json_encode($data));
                    fclose($fp);
                }
            } else {
                $access_token = $data->access_token;
            }
            //呼吸科空间
        } elseif ($newsType == 3) {
            $file = $baseurl . "access_token3.json";
            if (file_exists($file)) {
                $data = json_decode(file_get_contents($file));
            }
            if (!isset($data) || $data->expire_time < time()) {
                $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" . APPID3 . "&secret=" . APPSECRET3;
                $res = json_decode(self::httpGet($url));
                $access_token = $res->access_token;
                if (!isset($data)) {
                    $data = new stdClass();
                }
                if ($access_token) {
                    $data->expire_time = time() + 7000;
                    $data->access_token = $access_token;
                    $fp = fopen($baseurl . "access_token3.json", "w");
                    fwrite($fp, json_encode($data));
                    fclose($fp);
                }
            } else {
                $access_token = $data->access_token;
            }
        }
        return $access_token;
    }

    public static function httpGet($url, $data = '')
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 500);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        if ($data) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        $res = curl_exec($curl);
        curl_close($curl);

        return $res;
    }
}