<?php

class AdmAspirinEducationController extends BaseController
{
	
	CONST  PAGESIZE = 20;

	/**
	 * 列出所有PPT
	 */
	public function getList(){
		$oList = AspirinEducationPpt::orderBy('id','DESC')->paginate(self::PAGESIZE);
		return View::make('admin.aspirin.education.list')->with('oList',$oList);
	}
	
	/**
	 * 新增PPT
	 */
	public function getAddPpt(){
		return View::make('admin.aspirin.education.add');
	}
	
	/**
	 * 新增PPT提交
	 */
	public function postAddPptDo(){
		$sPptTitle = Input::get('ppt_title','');
		$sPptThumb = Input::get('ppt_thumb','');
		$iCatId = intval(Input::get('catid'));
		if($iCatId != 1 && $iCatId != 2){
			return $this->showMessage('请选择科室分类','/admaspirineducation/add-ppt');
		}
		if(empty($sPptTitle)||empty($sPptThumb)){
			return $this->showMessage('请将信息填写完整','/admaspirineducation/add-ppt');
		}
		$aPPt = array(
			'catid'     => $iCatId,
			'ppt_title'	=> trim($sPptTitle),
			'ppt_thumb'	=> trim($sPptThumb)
		);
		$oPpt = new AspirinEducationPpt($aPPt);
		$oPpt->save();
		return $this->showMessage('添加PPT成功！','/admaspirineducation/list');
	}
	
	/**
	 * 编辑PPT
	 * @param number $iPptId
	 */
	public function getEditPpt($iPptId=0){
		$oPpt = AspirinEducationPpt::find($iPptId);
		if(!$oPpt){
			return $this->showMessage('该PPT不存在或者已经被删除','/admaspirineducation/list');
		}
		return View::make('admin.aspirin.education.edit')->with('oPpt',$oPpt);
	}
	
	/**
	 * 编辑提交
	 * @param unknown $iPptId
	 */
	public function postEditPptDo($iPptId=0){
		$oPpt = AspirinEducationPpt::find($iPptId);
		if(!$oPpt){
			return $this->showMessage('该PPT不存在或者已经被删除','/admaspirineducation/list');
		}
		$sPptTitle = Input::get('ppt_title','');
		$sPptThumb = Input::get('ppt_thumb','');
		if(empty($sPptTitle)||empty($sPptThumb)){
			return $this->showMessage('请将信息填写完整','/admaspirineducation/edit-ppt/'.$iPptId);
		}
		$oPpt->ppt_title = $sPptTitle;
		$oPpt->ppt_thumb = $sPptThumb;
		$oPpt->save();
		return $this->showMessage('保存成功','/admaspirineducation/edit-ppt/'.$iPptId);
	}
	
	/**
	 * 删除PPT
	 * @param number $iPptId
	 */
	public function getDelPpt($iPptId=0){
		$oPpt = AspirinEducationPpt::find($iPptId);
		if(!$oPpt){
			return $this->showMessage('该PPT不存在或者已经被删除','/admaspirineducation/list');
		}
		$oEducation = AspirinEducation::where('ppt_id',$iPptId)->first();
		if($oEducation){
			return $this->showMessage('该PPT已经被用户制作成课件，暂不允许删除','/admaspirineducation/list');
		}
		$oPpt->delete();
		return $this->showMessage('删除PPT成功','/admaspirineducation/list');
	}
	
	/**
	 * 查看ppt多图片
	 */
	public function getPptImage($iPptId){
		$oImage = AspirinEducationPptImage::where('ppt_id',$iPptId)->orderBy('id','ASC')->get();
		return View::make('admin.aspirin.education.imagelist')->with('oImage',$oImage)->with('iPptId',$iPptId);
	}
	
	/**
	 * 新增PPT图片
	 * @param unknown $iPptId
	 */
	public function getAddPptImage($iPptId){
		return View::make('admin.aspirin.education.imageadd')->with('iPptId',$iPptId);
	}
	
	/**
	 * ppt多图片上传
	 */
	public function postUploadPptImage($iPptId = 0){
		if($_FILES['Filedata']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['Filedata']['name'];
			$attach_fileext = get_filetype($attach_filename);
			$rand_name = date('YmdHis', time()).rand(1000,9999);
			$sFileName = $rand_name.'.'.$attach_fileext;
			$sPath = "/upload/education/pptimage/".date('Ymd',time());
			$sRealPath = public_path().$sPath;
			mkdirs($sRealPath);
			move_uploaded_file($_FILES['Filedata']['tmp_name'], $sRealPath.DS.$sFileName);
			$sFileNameS = $rand_name . '_s.' . $attach_fileext;
			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 1000, 1000 );
			$sFileUrl = $sPath.'/'.$sFileName;
			//保存到数据库
			$aImage = array(
				'ppt_id'	=> $iPptId,
				'image_url'	=> $sFileUrl
			);
			$oImage = new AspirinEducationPptImage($aImage);
			$oImage->save();
			echo '成功';
			die;
		}
	}
	
	/**
	 * 删除PPT图片
	 * @param number $iPptId
	 */
	public function getDelPptImage($iImageId=0,$iPptId = 0){
		$oImage = AspirinEducationPptImage::find($iImageId);
		if(!$oImage){
			return $this->showMessage('该PPT图片不存在或者已经被删除','/admaspirineducation/ppt-image/'.$iPptId);
		}
		$oEducation = AspirinEducation::where('ppt_id',$iPptId)->first();
		if($oEducation){
			return $this->showMessage('该PPT已经被用户制作成课件，暂不允许删除','/admaspirineducation/ppt-image/'.$iPptId);
		}
		$oImage->delete();
		return $this->showMessage('删除PPT图片成功','/admaspirineducation/ppt-image/'.$iPptId);
	}
	
	/**
	 * 上传ppt缩略图
	 */
	public function postUploadPptThumb(){
		if($_FILES['upload_file']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['upload_file']['name'];
			$attach_fileext = get_filetype($attach_filename);
			$rand_name = date('YmdHis', time()).rand(1000,9999);
			$sFileName = $rand_name.'.'.$attach_fileext;
			$sPath = "/upload/education/pptthumb/".date('Ymd',time());
			$sRealPath = public_path().$sPath;
			mkdirs($sRealPath);
			move_uploaded_file($_FILES['upload_file']['tmp_name'], $sRealPath.DS.$sFileName);
			$sFileNameS = $rand_name . '_s.' . $attach_fileext;
			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 300, 300 );
			$sFileUrl = $sPath.'/'.$sFileNameS;
			$json = array('video_thumb'=>$sFileUrl);
			echo json_encode($json);
			die;
		}
	}
	
	/**
	 * 视频审核列表
	 */
	public function getVideoList($iTypeId = -1){
        $iTypeId = Input::get('type_id', $iTypeId);
	    $q = Input::get('q', '');

		$oEducation = AspirinEducation::where('video_flag',0)->orderBy('id','DESC');
		if($iTypeId != -1 && $iTypeId >= 0){
		    if($iTypeId == 3) { // 待签署讲课协议
                $oEducation
                    ->join('view_qrcode_video', 'view_qrcode_video.video_id', '=', 'aspirin_education.id')
                    ->join('view_education_agreement', 'view_education_agreement.detail_id', '=', 'aspirin_education.id')
                    ->whereRaw('aspirin_education.is_signed <> 1 and view_education_agreement.user_id = aspirin_education.user_id');
            } else if ($iTypeId == 4) { // 推送讲课协议
                $oEducation
                    ->join('view_qrcode_video', 'view_qrcode_video.video_id', '=', 'aspirin_education.id')
                    ->leftJoin('view_education_agreement', 'view_education_agreement.detail_id', '=', 'aspirin_education.id')
                    ->whereRaw('aspirin_education.is_signed <> 1 and view_education_agreement.detail_id is null');
            } else {
                $oEducation = $oEducation->where('education_type', $iTypeId);
            }
		}
		if (!empty($q)) {
            $oEducation = $oEducation->where('aspirin_education.user_name', 'like', "%{$q}%");
        }

        $oEducation = $oEducation->select(
                "aspirin_education.*",
                "user.user_province","user.user_city","user.user_county","user.user_department","user.user_position","user.user_company",
                "hospital.name as hospital_name", "user.user_company_name"
            )
            ->leftJoin('user','user.id','=','aspirin_education.user_id')
            ->leftJoin('hospital', 'user.user_company', '=', 'hospital.id')
            ->orWhere(function($query) use ($q) {
                $sqlarr = [];
                if (!empty($q)) {
                    $sqlarr[] = "hospital.name like '%{$q}%'";
                    $sqlarr[] = "user.user_company_name like '%{$q}%'";
                }
                if (!empty($sqlarr)) {
                    $query->whereRaw(join(' OR ', $sqlarr));
                }
            })
            ->orderby('aspirin_education.created_at','desc')
            ->paginate(self::PAGESIZE);

		//$oEducation = $oEducation->paginate(self::PAGESIZE);

        // 附加分页参数
        $params = ['type_id'=>$iTypeId,'q'=>$q];

		return View::make('admin.aspirin.education.videolist')
            ->with('oEducation',$oEducation)
            ->with('params',$params)
            ->with('iTypeId',$iTypeId);
	}
	
	/**
	 * 审核详细页面
	 * @param number $iVideoId
	 */
	public function getVideoShow($iVideoId=0){
		$oEducation = AspirinEducation::find($iVideoId);
		$oUser = User::find($oEducation->user_id);
		return View::make('admin.aspirin.education.videoshow')->with('oEducation',$oEducation)->with('oUser',$oUser);
	}

	/**
	 * 达到可签署协议条件，提送需签署协议内容提醒。
	 * @param number $iVideoId
	 */
	public function getPushNotice($iVideoId=0){
		$oEducation = AspirinEducation::find($iVideoId);
		$oUser = User::find($oEducation->user_id);
		// 通过视图获取银行信息，不能从表获取，表中有重复数据，有坑
        $ubank = DB::table('view_user_bank_account')->where('user_id', $oEducation->user_id)->first();

        // 处理付款金额数据
        $oUser->user_fee = 800.00;
        $aHospital = HospitalLectureCost::lists('price', 'hospital_name');
        // 处理医院名称
        if((int)$oUser->user_company>0) $oUser->user_company = Hospital::where("id", $oUser->user_company)->pluck("name");
        else $oUser->user_company = $oUser->user_company_name;
        if (isset($aHospital[$oUser->user_company])) {
            $oUser->user_fee = $aHospital[$oUser->user_company];
        }

        $noticeInfo = array(
            'video_id' => $iVideoId,
            'user_name' => $oUser->user_name,
            'card_number' => $ubank->bank_idcard_code,
            'user_address' => $oUser->user_company,
            'user_tel' => empty($oUser->user_tel) ? $oUser->user_mobile : $oUser->user_tel,
            'user_fee' => $oUser->user_fee,
            'bank_account' => $ubank->bank_account,
            'bank_name' => $ubank->bank_name,
            'bank_address' => $ubank->bank_address,
            'date'  => date('Y 年 m 月 d 日')
        );
        // 合并临时修改的数据
        $sessData = Session::get('notice_info' . $iVideoId);
        if ($sessData) {
            $sessData = json_decode($sessData, true);
            if (is_array($sessData)) {
                $noticeInfo = array_merge($noticeInfo, $sessData);
            }
        }

        //生成签名文件并保存
        $signurl = $oEducation->is_signed ? $oEducation->sign_icon :$this->createSignFile($noticeInfo);

		return View::make('admin.aspirin.education.pushnotice')
            ->with('oEducation',$oEducation)
            ->with('oUser',$oUser)
            ->with('signUrl',$signurl)
            ->with('noticeInfo',$noticeInfo)
            ->with('jUserInfo', json_encode($noticeInfo))
            ->with('uBank',$ubank);
	}

	public function getPushNoticeData($iVideoId=0)
    {
        $oEducation = AspirinEducation::find($iVideoId);
        $oUser = User::find($oEducation->user_id);
        // 通过视图获取银行信息，不能从表获取，表中有重复数据，有坑
        $ubank = DB::table('view_user_bank_account')->where('user_id', $oEducation->user_id)->first();

        // 处理付款金额数据
        $oUser->user_fee = 800.00;
        $aHospital = HospitalLectureCost::lists('price', 'hospital_name');
        // 处理医院名称
        if((int)$oUser->user_company>0) $oUser->user_company = Hospital::where("id", $oUser->user_company)->pluck("name");
        else $oUser->user_company = $oUser->user_company_name;
        if (isset($aHospital[$oUser->user_company])) {
            $oUser->user_fee = $aHospital[$oUser->user_company];
        }

        $noticeInfo = array(
            'video_id' => $iVideoId,
            'user_name' => $oUser->user_name,
            'card_number' => $ubank->bank_idcard_code,
            'user_address' => $oUser->user_company,
            'user_tel' => empty($oUser->user_tel) ? $oUser->user_mobile : $oUser->user_tel,
            'user_fee' => $oUser->user_fee,
            'bank_account' => $ubank->bank_account,
            'bank_name' => $ubank->bank_name,
            'bank_address' => $ubank->bank_address,
            'date'  => date('Y 年 m 月 d 日')
        );

        // 合并临时修改的数据
        $sessData = Session::get('notice_info' . $iVideoId);
        if ($sessData) {
            $sessData = json_decode($sessData, true);
            if (is_array($sessData)) {
                $noticeInfo = array_merge($noticeInfo, $sessData);
            }
        }

        return View::make('admin.aspirin.education.pushnoticedata')
            ->with('oEducation',$oEducation)
            ->with('noticeInfo',$noticeInfo)
            ->with('uBank',$ubank);
    }

    // 将保存的数据写入 session
    public function postPushNoticeData($iVideoId=0)
    {
        $data = Input::all();
        Session::put('notice_info' . $iVideoId, json_encode($data) );

        return Redirect::to('/admaspirineducation/push-notice/' . $iVideoId);
    }

    /**
     * 达到可签署协议条件，提送需签署协议内容提醒。
     * @param number $iVideoId
     */
    public function postPushNoticeCheck($iVideoId=0){
        $agreement_txt = trim(Input::get('agreement_txt',''));
        $agreement_img = trim(Input::get('agreement_img',''));
        if($agreement_txt == '' && $agreement_img == ''){
            return $this->showMessage('请填写协议内容','/admaspirineducation/push-notice/'.$iVideoId);
        }
        $iUid = intval(Input::get('user_id',0));
        $notice_type = intval(Input::get('notice_type',150));
        $detail_id = intval(Input::get('detail_id',$iVideoId));
        $notice_content = trim(Input::get('notice_content',''));
        $aNoticeInfo = AspirinUserNotice::where("user_id", $iUid)
            ->where("notice_type", 150)
            ->where("detail_id", $iVideoId)
            ->get()->count();
        if($aNoticeInfo) return $this->showMessage('请不要重复填写协议内容');

        $aNotice = array(
            'user_id'		=> $iUid,
            'notice_content'=> $notice_content,
            'notice_type'	=> $notice_type,
            'detail_id'		=> $iVideoId,
            'agreement_txt'    => $agreement_txt,
            'agreement_img'    => $agreement_img,
        );
        $oNotice = new AspirinUserNotice($aNotice);
        $oNotice->save();
        return $this->showMessage('提交成功','/admaspirineducation/video-list');
    }
	
	/**
	 * 提交审核结果
	 * @param unknown $iVideoId
	 */
	public function postVideoCheck($iVideoId){
		$iType = Input::get('education_type',AspirinEducation::CHECK_TYPE_DEFAULT);
		$sFailReason = trim(Input::get('fail_reason',''));
		if($iType == AspirinEducation::CHECK_TYPE_FAIL && $sFailReason == ''){
			return $this->showMessage('请填写审核不通过原因','/admaspirineducation/video-show/'.$iVideoId);
		}
		//存储审核结果
		$oEducation = AspirinEducation::find($iVideoId);
		//存储原始审核状态，若原始状态和当前的审核状态相同，则不发送推送通知
		$iOldType = $oEducation->education_type;
		$oEducation->education_type = $iType;
		if($iType == AspirinEducation::CHECK_TYPE_FAIL){
			$oEducation->fail_reason = $sFailReason;
		}else{
			$oEducation->fail_reason = '';
		}
		$oEducation->save();
		//如果审核通过
		if ($iType == AspirinEducation::CHECK_TYPE_SUCCESS) {
			//发送审核通过推送通知
			$sPushContent = PushService::EDUCATION_CHECK_SUCCESS;
			$sPushContent = str_replace('{{title}}', $oEducation->ppt_title, $sPushContent);
			//加麦粒
			// $sRes = GoldService::addGold($oEducation->user_id, 21, $iVideoId);
			//若原始状态和当前的审核状态不相同，则发送推送通知
			if($iType != $iOldType){
				PushService::postClient('国卫健康云',$sPushContent,21,$oEducation->user_id);
			}
		} elseif($iType == AspirinEducation::CHECK_TYPE_FAIL) {
			//发送审核失败推送通知
			$sPushContent = PushService::EDUCATION_CHECK_FAIL;
			$sPushContent = str_replace('{{title}}', $oEducation->ppt_title, $sPushContent);
			$sPushContent = str_replace('{{reason}}', $sFailReason, $sPushContent);
			//若原始状态和当前的审核状不相同，则发送推送通知
			if($iType != $iOldType){
				PushService::postClient('国卫健康云',$sPushContent,21,$oEducation->user_id);
			}
		}
		return $this->showMessage('审核提交成功','/admaspirineducation/video-list');
	}
	
	/*********************后台上传视频 s**************************/
	/**
	 * 列出后台上传的视频
	 */
	public function getEduList(){
		$oEdu = AspirinEducation::where('video_flag',1)->orderBy('id','desc')->paginate(self::PAGESIZE);
		return View::make('admin.aspirin.education.edulist')->with('oEdu',$oEdu);
	}
	
	/**
	 * 新增视频
	 */
	public function getEduAdd(){
		return View::make('admin.aspirin.education.eduadd');
	}
	
	/**
	 * 新增视频提交
	 */
	public function postEduAddDo(){
		$sPptTitle = Input::get('ppt_title','');
		$sPptThumb = Input::get('ppt_thumb','');
		$sVideoUrl = Input::get('video_url','');
		$sContent = Input::get('education_content','');
		$iCatId = intval(Input::get('ppt_catid'));
		if($iCatId != 1 && $iCatId != 2){
			return $this->showMessage('请选择科室分类','/admaspirineducation/edu-add');
		}
		if(empty($sPptTitle)||empty($sPptThumb)||empty($sVideoUrl)||empty($sContent)){
			return $this->showMessage('请将信息填写完整','/admaspirineducation/edu-add');
		}
		$aEdu = array(
			'video_flag'        => 1,
			'ppt_catid'         => $iCatId,
			'ppt_title'	        => trim($sPptTitle),
			'ppt_thumb'	        => trim($sPptThumb),
			'video_url'         => trim($sVideoUrl),
			'education_content' => $sContent,
			'education_type'    => 1,
			'created_at'        => date('Y-m-d H:i:s',time())
		);
		$oEdu = new AspirinEducation($aEdu);
		$oEdu->save();
		return $this->showMessage('添加成功！','/admaspirineducation/edu-list');
	}
	
	/**
	 * 编辑视频
	 */
	public function getEduEdit($iId){
		$oEdu = AspirinEducation::find($iId);
		if(!$oEdu){
			return $this->showMessage('该视频不存在或者已经被删除','/admaspirineducation/edu-list');
		}
		return View::make('admin.aspirin.education.eduedit')->with('oEdu',$oEdu);
	}
	
	/**
	 * 编辑视频提交
	 */
	public function postEduEditDo($iId){
		$oEdu = AspirinEducation::find($iId);
		if(!$oEdu){
			return $this->showMessage('该视频不存在或者已经被删除','/admaspirineducation/edu-list');
		}
		$sPptTitle = Input::get('ppt_title','');
		$sPptThumb = Input::get('ppt_thumb','');
		$sVideoUrl = Input::get('video_url','');
		$sContent = Input::get('education_content','');
		if(empty($sPptTitle)||empty($sPptThumb)||empty($sVideoUrl)||empty($sContent)){
			return $this->showMessage('请将信息填写完整','/admaspirineducation/edu-edit/'.$iId);
		}
		$oEdu->ppt_title = trim($sPptTitle);
		$oEdu->ppt_thumb = trim($sPptThumb);
		$oEdu->video_url = trim($sVideoUrl);
		$oEdu->education_content = $sContent;
		$oEdu->save();
		return $this->showMessage('保存成功','/admaspirineducation/edu-list');
	}
	
	/**
	 * 删除视频
	 */
	public function getEduDel($iId){
		$oEdu = AspirinEducation::find($iId);
		if(!$oEdu){
			return $this->showMessage('该视频不存在或者已经被删除','/admaspirineducation/edu-list');
		}
		$oEdu->delete();
		return $this->showMessage('删除成功','/admaspirineducation/edu-list');
	}

	
	/*
	 * 新版线下会议签到log
	 */
	public function getExportpd(){
		set_time_limit(0);
		$showtypes = Input::get('showtypes');
		$showtypes = $showtypes ? $showtypes : "CHINA";
		$filenames = "{$showtypes}项目";
		$oLog = AspirinEducation::select(
				"aspirin_education.id","aspirin_education.user_name","aspirin_education.ppt_title","aspirin_education.created_at","user.user_province","user.user_city","user.user_county","user.user_department","user.user_position","user.user_company","user.user_company_name",
                "user.user_area","user.link_rep_id","user.user_mobile","user.user_tel",
                "user_bank_account.bank_realname","user_bank_account.bank_idcard_code","user_bank_account.bank_name","user_bank_account.bank_account",
                "rep.user_name as rep_name","rep.invite_code as rep_invite_code","rep.user_mobile as rep_mobile","rep.user_tel as rep_tel",
                "aspirin_education.is_signed","area.area_name","regin.regin_name","cluster.cluster_name"
			)
			->where('aspirin_education.education_type',1)
			->where('aspirin_education.video_types',"{$showtypes}")
			->leftJoin('user','user.id','=','aspirin_education.user_id')
			->leftJoin('user_bank_account','user.id','=','user_bank_account.user_id')
			->leftJoin(DB::raw('user rep'),'rep.id','=','user.link_rep_id')
			->leftJoin('area','area.id','=','rep.user_area')
			->leftJoin('regin','regin.id','=','rep.user_regin')
			->leftJoin('cluster','cluster.id','=','regin.cluster_id')
			->orderby('aspirin_education.created_at','desc')
			->get();

		$aHospital = HospitalLectureCost::lists('price', 'hospital_name');

		if($oLog){
			//导出csv
			header( "Cache-Control: public" );
			header( "Pragma: public" );
			header("Content-type:application/vnd.ms-excel");
			header("Content-Disposition:attachment;filename={$filenames} ".date('Ymdhis').".csv");
			header('Content-Type:APPLICATION/OCTET-STREAM');
			ob_start();
			$header = '';
			$header .= "视频开播时间,视频议题,省份,城市,姓名,医院,科室,级别,手机号,付款姓名,身份证,开户银行,卡号,付款金额,所属代表,代表邀请码,代表手机号,31日内观看次数（二维码扫描次数）,累计扫码数,视频观看总时长,是否签回讲协议,地区,大区,Cluster"."\n";;
			$header_str =  iconv("utf-8",'gbk',$header);
			$line = "";
			$arrs = $oLog->toArray();
			// print_r($arrs);die;
			foreach ($arrs as $key => $value) {
				if((int)$value['user_company']>0) $arrs[$key]['user_company'] = Hospital::where("id",$value['user_company'])->pluck("name");
				else $arrs[$key]['user_company'] = $value['user_company_name'];
				if((int)$value['user_province']>0) $arrs[$key]['user_province'] = Hospital::where("id",$value['user_province'])->pluck("name");
				if((int)$value['user_city']>0) $arrs[$key]['user_city'] = Hospital::where("id",$value['user_city'])->pluck("name");
				if((int)$value['user_county']>0) $arrs[$key]['user_county'] = Hospital::where("id",$value['user_county'])->pluck("name");
				/*$views = VideoViewLog::select("id")
                    ->where('platform', 'qrcode')
					->where("video_id",$value['id'])
                    ->groupBy("openid")
					->whereBetween("created_at", [$value['created_at'], date("Y-m-d H:i:s",strtotime($value['created_at'])+86400*30)])
					->count();*/
				// update byron 2018-09-21 统一从 AspirinEducation 的 share_hits 访问器获取扫码观看数量
				$views = DB::table('view_qrcode_log')->where('video_id', $value['id'])->pluck('share_hits');
				$total_views = VideoViewLog::where('platform', 'qrcode')
                    ->where('video_id', $value['id'])
                    // ->where('openid', '<>', '') // 这里有个 bug 如果筛出 openid 为空的记录会影响一些旧数据
                    ->count();
				$view_times = (int)VideoViewLog::where('platform', 'qrcode')
                    ->where('video_id', $value['id'])
                    //->where('openid', '<>', '')  // 这里有个 bug 如果筛出 openid 为空的记录会影响一些旧数据
                    ->sum('view_times');
				// 处理付款金额数据
				$price = 800.00;
				if (isset($aHospital[$arrs[$key]['user_company']])) {
                    $price = $aHospital[$arrs[$key]['user_company']];
                }
                $user_mobile = empty($arrs[$key]['user_tel']) ? $arrs[$key]['user_mobile'] : $arrs[$key]['user_tel'];
                $rep_mobile = empty($arrs[$key]['rep_tel']) ? $arrs[$key]['rep_mobile'] : $arrs[$key]['rep_tel'];
				// $view_times = sec_to_time($view_times);
				// $log = DB::getQueryLog();
				// print_r($log);die;
				$arrs[$key]['views'] = $views;
				$line .= $value['created_at'];
				$line .= ",{$value['ppt_title']}";
				$line .= ",{$arrs[$key]['user_province']}";
				$line .= ",{$arrs[$key]['user_city']}";
				$line .= ",{$value['user_name']}";
				$line .= ",{$arrs[$key]['user_company']}";
				$line .= ",{$value['user_department']}";
				$line .= ",{$value['user_position']}";
                $line .= ",{$user_mobile}";
                $line .= ",{$arrs[$key]['bank_realname'] }";
                $line .= ",'{$arrs[$key]['bank_idcard_code']}";
                $line .= ",{$arrs[$key]['bank_name'] }";
                $line .= ",'{$arrs[$key]['bank_account']}";
                $line .= ",{$price}"; // 付款金额
                $line .= ",{$arrs[$key]['rep_name'] }";
                $line .= ",{$arrs[$key]['rep_invite_code']}";
				$line .= ",{$rep_mobile}";
				$line .= ",{$views}";
				$line .= ",{$total_views}";
				$line .= ",{$view_times}秒";
                $line .= "," . ($arrs[$key]['is_signed'] == 1 ? '是' : '否'); // 是否签回讲协议
                $line .= ",{$arrs[$key]['area_name']}";
                $line .= ",{$arrs[$key]['regin_name']}";
                $line .= ",{$arrs[$key]['cluster_name']}";
				$line .= "\n";
			}
			$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
			ob_end_clean();
			echo $header_str;
			echo $file_str;
			exit;
		}
	}

    /**
     * 批量导出签名图片
     */
    public function getExportsign()
    {
        return View::make('admin.aspirin.education.exportsign')->with('oEdu',[]);
    }

	public function postExportsign()
    {
        $dfile =  tempnam('/tmp', 'tmp');//产生一个临时文件，用于缓存下载文件

        include_once  app_path().'/include/zipfile.class.php';

        $zip = new zipfile();
        //----------------------
        $filename = 'sign_' . date('YmdHis') . '.zip'; //下载的默认文件名

        $oEducation = AspirinEducation::where('is_signed', 1)->where('education_type', 1);

        $start = Input::get('start', 0);
        $end = Input::get('end', 0);
        if ($start) {
            $oEducation->where('created_at', '>=', $start . ' 00:00:00');
        }
        if ($end) {
            $oEducation->where('created_at', '<=', $end . ' 23:59:59');
        }

        //以下是需要下载的图片数组信息，将需要下载的图片信息转化为类似即可
        $oList = $oEducation->get();

        foreach ($oList as $item) {
            $filePath = ROOT_DIR . $item['sign_icon'];
            if (file_exists($filePath)) {
                $name = $item->created_at->format('Y-m-d_H_i_s') . ' ' . $item['user_name'];
                $zip->add_file(file_get_contents($filePath), iconv('utf-8', 'gbk//ignore', $name) . '.jpg');
            }
        }
        //----------------------
        $zip->output($dfile);

        // 下载文件
        ob_clean();
        header('Pragma: public');
        header('Last-Modified:'.gmdate('D, d M Y H:i:s') . 'GMT');
        header('Cache-Control:no-store, no-cache, must-revalidate');
        header('Cache-Control:pre-check=0, post-check=0, max-age=0');
        header('Content-Transfer-Encoding:binary');
        header('Content-Encoding:none');
        header('Content-type:multipart/form-data');
        header('Content-Disposition:attachment; filename="'.$filename.'"'); //设置下载的默认文件名
        header('Content-length:'. filesize($dfile));
        $fp = fopen($dfile, 'r');
        while(connection_status() == 0 && $buf = @fread($fp, 8192)){
            echo $buf;
        }
        fclose($fp);
        @unlink($dfile);
        @flush();
        @ob_flush();
        exit();
    }

    /**
     * 生成协议文件（不带签名）
     * @param array $protocolInfo 协议信息
     * @param int $is_sign 0未签名  1已签名
     * @param string $sign_file
     * @return string
     */
    private function createSignFile($protocolInfo = array(), $is_sign=0, $sign_file=''){
        set_time_limit(0);
        @ini_set('memory_limit', '256M');
        $font =  public_path().'/assets/template/msyh.ttf';

        //处理图片
        $sNewName = $protocolInfo['video_id'] .'.jpg';//新文件名
        $sFilePath = '/upload/aspirineducation/agreement/';
        if(! public_path().$sFilePath){
            mkdirs(public_path().$sFilePath,0777);
        }

        // 如果文件存在则直接返回
        if (file_exists(public_path(). $sFilePath . $sNewName)) {
            //return $sFilePath . $sNewName;
        }

        $target = imagecreatetruecolor(2496, 7010);//创建画布
        $bc = imagecolorallocate($target, 0, 3, 51);//分配颜色
        $cc = imagecolorallocate($target, 240, 102, 0);
        $wc = imagecolorallocate($target, 255, 255, 255);
        $yc = imagecolorallocate($target, 255, 255, 0);
        $bg = imagecreatefromjpeg( public_path().PHP_IMG.'/aspirin/xy-video-01.jpg');//载入图像，返回图像资源

        imagecopy($target, $bg, 0, 0, 0, 0, 2496, 7010);//拷贝图像或图像的一部分（原始图片大小）
        imagedestroy($bg);//销毁图片
        $size = 32;

        $h = 500;
        $height = 72;

        $str1 = $protocolInfo['user_name'];
        imagettftext($target, $size, 0, 1580, $h, $bc, $font, $str1);

        $h = $h+$height;
        $str2 = $protocolInfo['card_number'];
        imagettftext($target, $size, 0, 1650, $h, $bc, $font, $str2);

        $h = $h+$height;
        $str3 = $protocolInfo['user_address'];
        imagettftext($target, $size, 0, 1650, $h, $bc, $font, $str3);

        $h = $h+$height;
        $str4 = $protocolInfo['user_tel'];
        imagettftext($target, $size, 0, 1650, $h, $bc, $font, $str4);

        $h = 4075;
        $str4 = $protocolInfo['user_fee'];
        imagettftext($target, 38, 0, 910, $h, $bc, $font, $str4);

        $h= 4370;
        $str4 = $protocolInfo['bank_account'];
        imagettftext($target, 36, 0, 730, $h, $bc, $font, $str4);

        $h = $h+$height;
        $str4 = $protocolInfo['bank_address'];
        imagettftext($target, $size, 0, 900, $h, $bc, $font, $str4);

        $h = 5938;
        $str4 = $protocolInfo['date'];
        imagettftext($target, 38, 0, 500, $h, $bc, $font, $str4);

        imagejpeg($target,  public_path().$sFilePath.$sNewName);
        imagedestroy($target);

        return $sFilePath.$sNewName;
    }
}