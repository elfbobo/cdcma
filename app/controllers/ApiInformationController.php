<?php 

class ApiInformationController extends BaseController
{
	
	const NETURL = 'http://cdcma.bizconf.cn';

	public function __construct()
	{
		$this->beforeFilter('apilogin');
	}
		 
	/**
	 *http://bayer.local/apiinformation?token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJ
	 *zOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ
	 *4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
	 *  
	 */	
	public function getIndex()
	{ 
		global $iUserId;
		$iUid = $iUserId; 
		$oAllInformation = Information::orderBy('created_at','desc')->paginate(10);
		$aAllMessage = array();
		$i=0;
		foreach($oAllInformation as $value){  
			$aAllMessage[$i]['info_thumb'] = self::NETURL.$value -> info_thumb;
			$aAllMessage[$i]['id'] = $value ->id; 
			$aAllMessage[$i]['info_title'] = $value ->info_title;
			$aAllMessage[$i]['info_hits'] = $value ->info_hits;
			$sTime = $value ->created_at;
			$sTime = strtotime($sTime);
			$sDate = date('Y-m-d',$sTime);
			$aAllMessage[$i]['created_at'] = $sDate;
			$i++;
		}
		$oUserLoginTime =UserInformationTime::where('user_id',$iUid)->first();
		if(count($oUserLoginTime)){
			$oUserLoginTime->user_time = time();
			$oUserLoginTime->save();
		}else{
			$oLoginTime = new UserInformationTime();
			$oLoginTime->user_id =$iUid;
			$oLoginTime->user_time = time();
			$oLoginTime->save();
		}
		$aData = array('success'=>true,'data'=>$aAllMessage);
		return json_encode($aData);
	}
			 
	/**
	 * http://bayer.local/apiinformation/skip?id=12&token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJ
	 *zOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ
	 *4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
	 *  
	 */	
	public function getSkip(){
		global $iUserId;
		$iUid = $iUserId; 
		$iId = Input::get('id');
		$oUrl = Information::find($iId);
		$sUrl =  $oUrl-> info_url;
		$aAllUrl = array('info_url'=>$sUrl);
		if(count($sUrl)){
			$oUserInformationLog = UserInformationLog::where('type',0)->where('user_id',$iUid)->where('information_id',$iId)->first();
			if(count($oUserInformationLog)){
				$oUserInformationLog->hits =$oUserInformationLog->hits +1;
				$oUserInformationLog->save();
			}else{
				$oInformationLog = new UserInformationLog();
				$oInformationLog->user_id =$iUid;
				$oInformationLog-> information_id = $iId;
				$oInformationLog ->hits =1;
				$oInformationLog->save();
			}
			$iHits = $oUrl -> info_hits;
			$oUrl -> info_hits = $iHits+1;
			$oUrl ->save();
			$aData = array('success'=>true,'data'=>$aAllUrl);
       	    return json_encode($aData);
		}else{
			$aAllUrl = array('info_url'=>'没有该链接');
			$aData = array('success'=>false,'data'=>$aAllUrl);
       	    return json_encode($aData);
		}
	}
 
}