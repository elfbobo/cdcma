<?php

class SurveyController extends BaseController {
	
	const STABLECOUNT = 4;
	const STABLETYPE = 1;
	const EARLYCOUNT = 3;
	const EARLYTYPE = 2;

	/**
	 * 平稳达标小调研
	 */
	public function getStable(){
		return View::make('front.survey.survey.stable');
	}
	
	/**
	 * 早期达标小调研
	 */
	public function getEarly(){
		return View::make('front.survey.survey.early');
	}
	
	/**
	 * 结果页
	 * @param number $iTypeId
	 */
	public function getResult($iTypeId = 1){
		$oResult = LittleSurveyStatistic::where('type',$iTypeId)->get();
		$aResult = array();
		if($oResult){
			foreach($oResult as $k=>$v){
				$aResult[$v->question_id][$v->option] = $v->count;
			}
		}
		foreach($aResult as $iQuestionId=>$aOption){
			$iTotal = array_sum($aOption);
			foreach($aOption as $sOption=>$iCount){
				$aResult[$iQuestionId][$sOption] = intval($iCount*100/$iTotal);
			}
		}
		//用户选择
		$sChoose = Input::get('choose','');
		$aChoose = array();
		if($sChoose){
			$aChoose = mb_unserialize($sChoose);
		}
		if($iTypeId==self::STABLETYPE){
			//平稳达标小调研结果页
			$sView = 'stableresult';
		}elseif($iTypeId==self::EARLYTYPE){
			//早期达标小调研结果页
			$sView = 'earlyresult';
		}
		return View::make('front.survey.survey.'.$sView)
			->with('aResult',$aResult)
			->with('aChoose',$aChoose);
	}
	
	public function postSubmit($iTypeId){
		$aInput = Input::get('survey',array());
		$iQuestionCount = self::STABLECOUNT;
		if($iTypeId==self::EARLYTYPE){
			$iQuestionCount = self::EARLYCOUNT;
		}
		if(count($aInput)<$iQuestionCount){
			return json_encode(array('success'=>false,'msg'=>'请检查答题结果是否有遗漏'));
		}
		$aResult = array();
		$aReturnChoose = array();
		foreach($aInput as $k=>$v){
			//记录答题结果
			$sR = '';
			if(is_array($v)){
				//多选
				foreach($v as $val){
					$sR .= $val;
					//给该选项选择人数+1
					$this->OptionAddCount($iTypeId, $k, $val);
				}
			}else{
				//单选
				$sR = $v;
				//给该选项选择人数+1
				$this->OptionAddCount($iTypeId, $k, $sR);
			}
			if(!$sR){
				return json_encode(array('success'=>false,'msg'=>'提交失败'));
			}
			$aResult[] = array(
				'type'			=> $iTypeId,
				'question_id'	=> $k,
				'option'		=> $sR,
				'ip'			=> GetIP(),
				'created_at'	=> date('Y-m-d H:i:s')
			);
			$aReturnChoose[intval($k)] = $sR;
		}
		LittleSurveyAnswer::insert($aResult);
		$sUrl = '/survey/result/'.$iTypeId.'?choose='.serialize($aReturnChoose);
		return json_encode(array('success'=>true,'url'=>$sUrl));
	}
	
	//给选择某选项的人数+1
	public function OptionAddCount($iTypeId,$iQuestionId,$sOption){
		$oSurveyStatistic = LittleSurveyStatistic::where('type',$iTypeId)
			->where('question_id',$iQuestionId)
			->where('option',$sOption)
			->first();
		if($oSurveyStatistic){
			$oSurveyStatistic->count = $oSurveyStatistic->count+1;
			$oSurveyStatistic->save();
		}else{
			$aSurveyStatistic = array(
				'type'			=> $iTypeId,
				'question_id'	=> $iQuestionId,
				'option'		=> $sOption,
				'count'			=> 1
			);
			$oSurveyStatistic = new LittleSurveyStatistic($aSurveyStatistic);
			$oSurveyStatistic->save();
		}
	}
	
	
	//进入调研页答题
	public function Question($iId)
	{
		$oResearchPeriod = ResearchPeriod::find($iId);
		$oResearchPeriodQuestion = ResearchPeriodQuestion::where('research_period_id',$iId)->orderBy('id')->get();
		$i=1;
		$aList = array();//获取试题次序
		foreach($oResearchPeriodQuestion as $v){
			$aList[$v->id] =$i;
			$i++;
		}
		$arr =array();//获取试题选项
		$oResearchPeriodItem = ResearchPeriodItem::where('research_period_id',$iId)->get();	
		foreach($oResearchPeriodItem as $v){
			$arr[$v->research_id][$v->research_option] = $v->research_option;
		}
		$aContent = array();//获取选项内容
		foreach($oResearchPeriodItem as $v){
			$aContent[$v->research_id][$v->research_option] = $v->research_content;
		}
		return View::make('front.survey.survey.survey')
			->with('oResearchPeriodQuestion',$oResearchPeriodQuestion)
			->with('oResearchPeriodItem',$oResearchPeriodItem)
			->with('oResearchPeriod',$oResearchPeriod)
			->with('arr',$arr)
			->with('aContent',$aContent)
			->with('aList',$aList);
	}
	
	public function postSubmits($iTypeId){
		$aInput = Input::get('survey',array());
		$oQuestion = ResearchPeriodQuestion::get();
	 	// $iQuestionCount = self::STABLECOUNT;
		// if($iTypeId==self::EARLYTYPE){
		// 	$iQuestionCount = self::EARLYCOUNT;
		// }
        $oQuestion = ResearchPeriodQuestion::where('research_period_id',$iTypeId)->get();
		$iQuestionCount =0;
		foreach($oQuestion as $v){
		    $iQuestionCount++;
		}
        //$iQuestionCount =$iQuestionCount +1;
		if(count($aInput)<$iQuestionCount){
			return json_encode(array('success'=>false,'msg'=>'请检查答题结果是否有遗漏'));
		}
		$aResult = array();
		$aReturnChoose = array();
		foreach($aInput as $k=>$v){
			//记录答题结果
			$sR = '';
			if(is_array($v)){
				//多选
				foreach($v as $val){
					$sR .= $val;
					//给该选项选择人数+1
					$this->OptionAddCounts($iTypeId, $k, $val);
				}
			}else{
				//单选
				$sR = $v;
				//给该选项选择人数+1
				$this->OptionAddCounts($iTypeId, $k, $sR);
			}
			if(!$sR){
				return json_encode(array('success'=>false,'msg'=>'提交失败'));
			}
			$aResult[] = array(
				'research_period_id'	=> $iTypeId,
				'research_question_id'	=> $k,
				'research_option'		=> $sR,
				'user_ip'				=>GetIP(),
				'created_at'			=> date('Y-m-d H:i:s')
			);
			$aReturnChoose[intval($k)] = $sR;
		}
		ResearchPeriodLog::insert($aResult);
		$sUrl = '/survey/results/'.$iTypeId.'?choose='.serialize($aReturnChoose);
		return json_encode(array('success'=>true,'url'=>$sUrl));
	}
	
	//给选择某选项的人数+1
	public function OptionAddCounts($iTypeId,$iQuestionId,$sOption){
		$ip = $_SERVER['REMOTE_ADDR'];
		$oResearchPeriodStatistical = ResearchPeriodStatistical::where('research_period_id',$iTypeId)
			->where('research_id',$iQuestionId)
			->where('research_option',$sOption)
			->first();
		if($oResearchPeriodStatistical){
			$oResearchPeriodStatistical->count = $oResearchPeriodStatistical->count + 1;
			$oResearchPeriodStatistical->save();
		}else{
			$aSurveyStatistic = array(
				'research_period_id'	=> $iTypeId,
				'research_id'			=> $iQuestionId,
				'research_option'		=> $sOption,
				'count'					=> 1
			);
			$oSurveyStatistic = new ResearchPeriodStatistical($aSurveyStatistic);
			$oSurveyStatistic->save();
		}
	}
	
	/**
	 * 结果页
	 * @param number $iTypeId
	 */
	public function getResults($iTypeId = 1){
		$oResult = ResearchPeriodStatistical::where('research_period_id',$iTypeId)->get();
		$aResult = array();
		if($oResult){
			foreach($oResult as $k=>$v){
				$aResult[$v->research_id][$v->research_option] = $v->count;
			}
		}
		foreach($aResult as $iQuestionId=>$aOption){
			$iTotal = array_sum($aOption);
			foreach($aOption as $sOption=>$iCount){
				$aResult[$iQuestionId][$sOption] = intval($iCount*100/$iTotal);
			}
		}
		//用户选择
		$sChoose = Input::get('choose','');
		$aChoose = array();
		if($sChoose){
			$aChoose = mb_unserialize($sChoose);
		}
		$sView = 'surveyresult';
		$oResearchPeriod = ResearchPeriod::find($iTypeId);
		 $oResearchPeriodQuestion = ResearchPeriodQuestion::where('research_period_id',$iTypeId)->orderBy('id')->get();
		 $i=1;
		 $aList = array();//获取试题次序
		 foreach($oResearchPeriodQuestion as $v){
		    $aList[$v->id] =$i;
		    $i++;
		 }
		 $arr =array();//获取试题选项
		 $oResearchPeriodItem = ResearchPeriodItem::where('research_period_id',$iTypeId)->get();	
		 foreach($oResearchPeriodItem as $v){
	         $arr[$v->research_id][$v->research_option] = $v->research_option;
		}
		$aContent = array();//获取选项内容
		foreach($oResearchPeriodItem as $v){
	         $aContent[$v->research_id][$v->research_option] = $v->research_content;
		}
		$apicture =array();
		foreach($oResearchPeriodQuestion as $v){
		    $apicture[$v->id] = $v->research_img;
		}
		return View::make('front.survey.survey.'.$sView)
			->with('aResult',$aResult)
			->with('aChoose',$aChoose)
			->with('oResearchPeriod',$oResearchPeriod)
			->with('oResearchPeriodQuestion',$oResearchPeriodQuestion)
			->with('aList',$aList)
			->with('arr',$arr)
			->with('apicture',$apicture);
	}
	
}