<?php

class AdmDocFaceRecController extends BaseController {

	CONST  PAGESIZE = 10;
	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	//专家列表
	public function Doc()
	{
		//取出所有专家
		$oDocs = FaceDoc::paginate(self::PAGESIZE);
		return  View::make('admin.facerec.doclist')->with('oDocs',$oDocs);
	}
	
	//新增专家
	public function AddDoc(){
		return  View::make('admin.facerec.docadd');
	}
	
	//新增专家do
	public function AddDocDo(){
		$aInput = Input::all();
		unset($aInput['upload_file']);
		$rule = FaceDoc::getDocRule();
		
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
		    return $this->showMessage('请填写必填字段');
		}
		
		$oFaceDoc = new FaceDoc();
		//验证成功
		if($oFaceDoc->addDoc($aInput)->save()){
			$this->clearDoctorsRedis();
			return $this->showMessage('新增专家成功','/admdocfacerec/doc');
		}else{
			return $this->showMessage('添加失败');
		}
	}
	
	//编辑用户
	public function EditDoc($id){
		$oFaceDoc = FaceDoc::find($id);
		return  View::make('admin.facerec.docedit')->with('oFaceDoc',$oFaceDoc);
	}
	
	//编辑提交
	public function EditDocDo($id){
		$aInput = Input::all();
		unset($aInput['upload_file']);
		$rule = FaceDoc::getDocRule();
		
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
		    return $this->showMessage('请填写必填字段');
		}
		
		$oFaceDoc = FaceDoc::find($id);
		if($oFaceDoc->addDoc($aInput)->save()){
			$this->clearDoctorsRedis();
			return $this->showMessage('编辑成功');
		}else{
			return $this->showMessage('编辑失败');
		}
	}
	
	//删除
	public function DelDoc($id){
		$oFaceDoc = FaceDoc::find($id);
		$oFaceDoc->delete();
		$this->clearDoctorsRedis();
		return $this->showMessage('删除专家成功','/admdocfacerec/doc');
	}
	
	//上传图片
	public function UploadDocThumb(){
		if($_FILES['upload_file']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['upload_file']['name'];
    		$attach_fileext = get_filetype($attach_filename);
		    $rand_name = date('YmdHis', time()).rand(1000,9999);
		    
		    $sFileName = $rand_name.'.'.$attach_fileext;   
		    
		    $sPath = "/upload/doc/$attach_fileext/".date('Ymd',time());
		    
		    $sRealPath = public_path().$sPath;
		    mkdirs($sRealPath);
			//$attach_filesize = filesize($_FILES['upload_file']['tmp_name']);
		    move_uploaded_file($_FILES['upload_file']['tmp_name'], $sRealPath.DS.$sFileName);
		    
		    $sFileNameS = $rand_name . '_s.' . $attach_fileext;
			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 1000, 1000 );
		    
		    $sFileUrl = $sPath.'/'.$sFileNameS;
		    
		    $json = array('doc_thumb'=>$sFileUrl);
		    echo json_encode($json);
		    die;
		}
	}
	
	//视频列表
	public function Video()
	{
		//累计观看直播医生数
		$iLiveCount = VideoLog::leftJoin('user','user.id','=','video_log.user_id')
				->where('user.role_id',3)
				->where('video_log.link_user_id',0)
				->where('video_log.video_type',2)
				->where('video_log.watch_minutes','>',0)
				->count();
		//累计观看录播医生数
		$iReviewCount = VideoLog::leftJoin('user','user.id','=','video_log.user_id')
				->where('user.role_id',3)
				->where('video_log.link_user_id',0)
				->where('video_log.video_type',1)
				->where('video_log.watch_minutes','>',0)
				->count();
		//取出所有视频
		$oVideos = FaceVideo::findAllVideo()->where("video_rec","1")->paginate(self::PAGESIZE);
		// $log = DB::getQueryLog();
		// print_r($log);die;
		//取得所有专家
		$oDocs = FaceDoc::orderByCreatedAt()->lists('doc_name','id');
		return  View::make('admin.facerec.videolist')
				->with('oVideos',$oVideos)
				->with('oDocs',$oDocs)
				->with('iLiveCount',$iLiveCount)
				->with('iReviewCount',$iReviewCount);
	}
	
	//新增视频
	public function AddVideo(){
		//取得所有专家
		$oDocs = FaceDoc::orderByCreatedAt()->lists('doc_name','id');
		return  View::make('admin.facerec.videoadd')
				->with('oDocs',$oDocs);
	}
	
	//新增视频do
	public function AddVideoDo(){
		$aInput = Input::all();
		unset($aInput['upload_file']);
		$rule = FaceVideo::getVideoRule();
		
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
		    return $this->showMessage('请填写必填字段');
		}
		
		$oFaceVideo = new FaceVideo();
		if(isset($aInput['department_id'])){
			$sDepartment = '';
			foreach($aInput['department_id'] as $v){
				$sDepartment .= $v.'|';
			}
			$aInput['department_id'] = $sDepartment;
		}else{
			$aInput['department_id'] = '';
		}
		
		//验证成功
		if($oFaceVideo->addVideo($aInput)->save()){
			$this->clearVdieoRedis();
			return $this->showMessage('新增视频成功','/admdocfacerec/video');
		}else{
			return $this->showMessage('添加失败');
		}
	}
	
	//编辑视频
	public function EditVideo($id){
		$oFaceVideo = FaceVideo::find($id);
		//取得所有专家
		$oDocs = FaceDoc::orderByCreatedAt()->lists('doc_name','id');
		return  View::make('admin.facerec.videoedit')
				->with('oFaceVideo',$oFaceVideo)
				->with('oDocs',$oDocs);
	}
	
	//编辑视频提交
	public function EditVideoDo($id){
		$aInput = Input::all();
		unset($aInput['upload_file']);
		unset($aInput['upload_banner']);
		$rule = FaceVideo::getVideoRule();
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
		    return $this->showMessage('请填写必填字段');
		}
		$oFaceVideo = FaceVideo::find($id);
		if(isset($aInput['department_id'])){
			$sDepartment = '';
			foreach($aInput['department_id'] as $v){
				$sDepartment .= $v.'|';
			}
			$aInput['department_id'] = $sDepartment;
		}else{
			$aInput['department_id'] = '';
		}
		if($oFaceVideo->addVideo($aInput)->save()){
			$this->clearVdieoRedis();
			return $this->showMessage('编辑成功');
		}else{
			return $this->showMessage('编辑失败');
		}
	}
	
	//删除
	public function DelVideo($id){
		$oFaceVideo = FaceVideo::find($id);
		$oFaceVideo->delete();
		$this->clearVdieoRedis();
		return $this->showMessage('删除视频成功','/admdocfacerec/video');
	}

	//新增音频
	public function AddAudio(){
		//取得所有专家
		$oDocs = FaceDoc::orderByCreatedAt()->lists('doc_name','id');
		return  View::make('admin.facerec.audioadd')
				->with('oDocs',$oDocs);
	}

	//新增音频do
	public function AddAudioDo(){
		$aInput = Input::all();
		unset($aInput['upload_file']);
		$rule = array(
            'video_title'			=> 'required',
            'video_type'			=> 'required',
            'doc_id'				=> 'min:0',
            'video_introduce'		=> 'required',
            // 'video_url'				=> 'required',
            'start_time'				=> 'required',
            'end_time'				=> 'required'
        );

		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
		    return $this->showMessage('请填写必填字段');
		}

		$oFaceVideo = new FaceVideo();
		if(isset($aInput['department_id'])){
			$sDepartment = '';
			foreach($aInput['department_id'] as $v){
				$sDepartment .= $v.'|';
			}
			$aInput['department_id'] = $sDepartment;
		}else{
			$aInput['department_id'] = '';
		}

		//验证成功
		if($oFaceVideo->addVideo($aInput)->save()){
			$this->clearVdieoRedis();
			return $this->showMessage('新增音频成功','/admdocfacerec/video');
		}else{
			return $this->showMessage('添加失败');
		}
	}

	//编辑视频
	public function EditAudio($id){
		$oFaceVideo = FaceVideo::find($id);
		//取得所有专家
		$oDocs = FaceDoc::orderByCreatedAt()->lists('doc_name','id');
		return  View::make('admin.facerec.audioedit')
				->with('oFaceVideo',$oFaceVideo)
				->with('oDocs',$oDocs);
	}

	//编辑视频提交
	public function EditAudioDo($id){
		$aInput = Input::all();
		unset($aInput['upload_file']);
		unset($aInput['upload_banner']);
		$rule = array(
            'video_title'			=> 'required',
            'video_type'			=> 'required',
            'doc_id'				=> 'min:0',
            'video_introduce'		=> 'required',
            // 'video_url'				=> 'required',
            'start_time'				=> 'required',
            'end_time'				=> 'required'
        );
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
		    return $this->showMessage('请填写必填字段');
		}
		$oFaceVideo = FaceVideo::find($id);
		if(isset($aInput['department_id'])){
			$sDepartment = '';
			foreach($aInput['department_id'] as $v){
				$sDepartment .= $v.'|';
			}
			$aInput['department_id'] = $sDepartment;
		}else{
			$aInput['department_id'] = '';
		}
		if($oFaceVideo->addVideo($aInput)->save()){
			$this->clearVdieoRedis();
			return $this->showMessage('编辑成功');
		}else{
			return $this->showMessage('编辑失败');
		}
	}

	//删除
	public function DelAudio($id){
		$oFaceVideo = FaceVideo::find($id);
		$oFaceVideo->delete();
		$this->clearVdieoRedis();
		return $this->showMessage('删除音频成功','/admdocfacerec/video');
	}
	
    public function clearDoctorsRedis(){
		if (Cache::has('doctorsRedis')){
	         Cache::forget('doctorsRedis');
		}
		return true;
	}
	
    public function clearVdieoRedis(){
		if (Cache::has('faceVideoRewiewRedis')){
			Cache::forget('faceVideoRewiewRedis');
		}
		return true;
	}

    //上传视频
    public function UploadVideoFile()
    {
        if ($_FILES['upload_file']['error'] > 0) {
            $error = $_FILES['thumb']['error'];
        } else {
            $attach_filename = $_FILES['upload_file']['name'];
            $attach_fileext = pathinfo($attach_filename, PATHINFO_EXTENSION);
            $rand_name = date('YmdHis', time()) . rand(1000, 9999);

            $sFileName = $rand_name . '.' . $attach_fileext;

            $sPath = "/upload/video/$attach_fileext/" . date('Ymd', time());

            $sRealPath = public_path() . $sPath;
            mkdirs($sRealPath);
            // $attach_filesize = filesize($_FILES['upload_file']['tmp_name']);
            move_uploaded_file($_FILES['upload_file']['tmp_name'], $sRealPath . DS . $sFileName);

            $sFileUrl = $sPath . '/' . $sFileName;

            $json = array('video_url' => $sFileUrl);
            echo json_encode($json);
            die;
        }
    }
	
	//上传图片
	public function UploadVideoThumb(){
		if($_FILES['upload_file']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['upload_file']['name'];
    		$attach_fileext = get_filetype($attach_filename);
		    $rand_name = date('YmdHis', time()).rand(1000,9999);
		    
		    $sFileName = $rand_name.'.'.$attach_fileext;   
		    
		    $sPath = "/upload/video/$attach_fileext/".date('Ymd',time());
		    
		    $sRealPath = public_path().$sPath;
		    mkdirs($sRealPath);
			// $attach_filesize = filesize($_FILES['upload_file']['tmp_name']);
		    move_uploaded_file($_FILES['upload_file']['tmp_name'], $sRealPath.DS.$sFileName);
		    
		    $sFileNameS = $rand_name . '_s.' . $attach_fileext;
			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 1000, 1000 );
		    
		    $sFileUrl = $sPath.'/'.$sFileNameS;
		    
		    $json = array('video_thumb'=>$sFileUrl);
		    echo json_encode($json);
		    die;
		}
	}
	
	//上传图片
	public function UploadVideoBanner(){
		if($_FILES['upload_banner']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['upload_banner']['name'];
    		$attach_fileext = get_filetype($attach_filename);
		    $rand_name = date('YmdHis', time()).rand(1000,9999);
		    $sFileName = $rand_name.'.'.$attach_fileext;   
		    $sPath = "/upload/video/$attach_fileext/".date('Ymd',time());
		    $sRealPath = public_path().$sPath;
		    mkdirs($sRealPath);
		    move_uploaded_file($_FILES['upload_banner']['tmp_name'], $sRealPath.DS.$sFileName);
		    $sFileNameS = $rand_name . '_s.' . $attach_fileext;
			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 1000, 1000 );
		    $sFileUrl = $sPath.'/'.$sFileNameS;
		    $json = array('video_thumb'=>$sFileUrl);
		    echo json_encode($json);
		    die;
		}
	}
	
	//试题列表
	public function SurveyList($id){
		$oFaceVideo = FaceVideo::find($id);
		$oQuestions = $oFaceVideo->SurveyQuestion()->orderBy('listorder','ASC')->get();
		$sVideoTitle = $oFaceVideo->video_title;
		return View::make('admin.facerec.survey.list')
				->with('oQuestions',$oQuestions)
				->with('iVid',$id)
				->with('sVideoTitle',$sVideoTitle);
	}
	
	//新增试题
	public function SurveyAdd($iVideoId){
		return View::make('admin.facerec.survey.add')
				->with('iVid',$iVideoId);
	}
	
	//新增试题
	public function SurveyAddDo($iVideoId){
		$input = Input::all();
		$aSurvey = array();
		$field = SurveyQuestion::getFieldSurvey();
		$aOption = array("A","B","C","D","E","F","G","H","I","J");
		foreach($field as $k=>$v){
			if(!isset($input[$k])){
				return $this->showMessage('*标字段必填');
			}
			$aSurvey[$k] = $input[$k];
		}
		$option_flg = 0;
		foreach ($aOption as $k=>$v){
			$key = 'optionvalue'.$v;
			if(isset($input[$key])){
				$option_flg = 1;
				break;
			}
		}
		if(!$option_flg){
			return $this->showMessage('请填写选项');
		}
		$aSurvey['face_video_id'] = $iVideoId;
		$oSurveyQuestion = new SurveyQuestion($aSurvey);
		$oSurveyQuestion->save();
		$iSusrveyId = $oSurveyQuestion->id;
		if($iSusrveyId){
			//添加题目成功，添加选项
			if($option_flg==1){
				//已经添加选项
				foreach ($aOption as $k=>$v){
					$key = 'optionvalue'.$v;
					if(isset($input[$key])){
						//添加选项开始
						$aOption = array(
							'survey_question_id'	=> $iSusrveyId,
							'option_title'			=> $input[$key],
							'option'				=> $v		
						);
						$oSurveyQuestionOption = new SurveyQuestionOption($aOption);
						$oSurveyQuestionOption->save();
					}
				}
				return $this->showMessage('添加调研试题成功','/admdocfacerec/survey/'.$iVideoId);
			}else{
				return $this->showMessage('请填写选项');
			}
			return $this->showMessage('添加调研试题成功','/admdocfacerec/survey/'.$iVideoId);
		}
		return $this->showMessage('添加调研试题成功','/admdocfacerec/survey/'.$iVideoId);
	}
	
	public function SurveyEdit($iId){
		$oSurvey = SurveyQuestion::find($iId);
		$oOption = $oSurvey->SurveyQuestionOption;
		return View::make('admin.facerec.survey.edit')
				->with('oSurvey',$oSurvey)
				->with('oOption',$oOption);
	}
	
	public function SurveyEditDo($iId){
		$input = Input::all();
		$aSurvey = array();
		$field = SurveyQuestion::getFieldSurvey();
		$aOption = array("A","B","C","D","E","F","G","H","I","J");
		$oSurveyQuestion =SurveyQuestion::find($iId);
		foreach($field as $k=>$v){
			if(!isset($input[$k])){
				return $this->showMessage('*标字段必填');
			}
			$oSurveyQuestion->$k = $input[$k];
		}
		$option_flg = 0;
		foreach ($aOption as $k=>$v){
			$key = 'optionvalue'.$v;
			if(isset($input[$key])){
				$option_flg = 1;
				break;
			}
		}
		if(!$option_flg){
			return $this->showMessage('请填写选项');
		}
		$oSurveyQuestion->save();
		$iSusrveyId = $oSurveyQuestion->id;
		if($iSusrveyId){
			//删除所有选项
			if($option_flg==1){
				//删除选项
				SurveyQuestionOption::where('survey_question_id',$iId)->delete();
				foreach ($aOption as $k=>$v){
					$key = 'optionvalue'.$v;
					if(isset($input[$key])){
						//添加选项开始
						$aOption = array(
							'survey_question_id'	=> $iSusrveyId,
							'option_title'			=> $input[$key],
							'option'				=> $v		
						);
						$oSurveyQuestionOption = new SurveyQuestionOption($aOption);
						$oSurveyQuestionOption->save();
					}
				}
				return $this->showMessage('编辑调研试题成功','/admdocfacerec/survey/'.$oSurveyQuestion->face_video_id);
			}
			return $this->showMessage('请添加选项');
		}
	}
	
	public function SurveyDel($iId){
		$oSurveyQuestion = SurveyQuestion::find($iId)->delete();
		return $this->showMessage('删除成功');
	}
	
	public function SignInLog($iId){
		set_time_limit(0);
		$oLog = SignInLog::select('sign_in_log.*','user.superior_ids','user.user_name','user.user_nick','user.user_tel','user.user_company','user.user_company_name','user.user_department','user.link_rep_id')
			->leftJoin('user','user.id','=','sign_in_log.user_id')
			->where('cat_id',1)
			->where('video_id',$iId)
			->get();
		//导出csv
		header( "Cache-Control: public" );
    	header( "Pragma: public" );
		header( "Content-type: application/vnd.ms-excel" );
    	header( "Content-Disposition: attachment;filename=".date('Ymdhis').".csv" );
    	header( "Content-Type: APPLICATION/OCTET-STREAM" );
    	ob_start();
    	$header = '';
		$header .= "大区,地区,用户编号,CWID,代表,组织者用户编码,组织者姓名,参会者姓名,手机号码,医院,科室,签到类型,签到科室会类型,签到得分,是否参与调研,签到时间,调研答案,医院所属标签"."\n";;
		$header_str =  iconv("utf-8",'gbk',$header);
		$line="";
		$aSurveyQuestionIds = SurveyQuestion::where('face_video_id',$iId)->lists('id','id');
        foreach($oLog as $k=>$v){
        	if($v->score>0){
        		$type="科室会签到";
        	}else{
        		$type="自行观看并签到";
        	}
        	if($v->video_type==1){
        		$sSignInType = "录播签到";
        	}else{
        		$sSignInType = "直播签到";
        	}
        	$oLogAn = null;
        	if($aSurveyQuestionIds){
        		$oLogAn = SurveyQuestionAnswer::whereIn('survey_question_id',$aSurveyQuestionIds)
    				->where('user_id',$v->user_id)
					->get();
        	}
			if(count($oLogAn)){
				$type_part = "是";
				$answer = '';
				foreach($oLogAn as $an){
					$answer .= $an->option.',';
				}
				$answer = substr($answer, 0,-1);
			}else{
				$type_part = "否";
				$answer = '';
			}
			//代表信息
//			$aSuperiorIds = unserialize($v->superior_ids);
			$iRepId = $v->link_rep_id;
			if($iRepId){
				$oRepInfo = User::find($iRepId);
				if($oRepInfo){
		        $sR = $oRepInfo->user_regin;
				$sA = $oRepInfo->user_area;
				if(is_numeric($sR)){
			   		$sRegin = User::getReginCache();
					$oRegin = json_decode($sRegin);
					$oRepInfo->user_regin = $oRegin->$sR;
			   	}
				if(is_numeric($sA)){
			   		$sArea = User::getAreaCache();
					$oArea = json_decode($sArea);
					$oRepInfo->user_area = !empty($oArea->$sR->$sA)?$oArea->$sR->$sA:'';
			   	}
				}
			}
			// $line .= ($k+1);
			// $line .= ','. str_ireplace(',','，',$v->user_nick);
			if($iRepId && $oRepInfo){
				$line .= $oRepInfo->user_regin;
				$line .= ','.$oRepInfo->user_area;
				$line .= ','.$oRepInfo->id;
				$line .= ','.$oRepInfo->user_cwid;
				$line .= ','.$oRepInfo->user_name;
			}else{
				$line .= ',,,,';
			}
        		$sUserCompany = $v->user_company;
				if(is_numeric($sUserCompany)&&$sUserCompany!=0){
					$sUserCompany = Hospital::where('id',$sUserCompany)->pluck('name');
				}
				if(!$sUserCompany){
					$sUserCompany = $v->user_company_name;
				}
			$sOrgazinerCode = '';
			$sOrgazinerName = '';
			$iOrganizerId = $v->organizer_id;
			if($iOrganizerId>0&&($iOrganizerId!=$iRepId)){
				$oOrganizerInfo = User::find($iOrganizerId);
				if($oOrganizerInfo&&$oOrganizerInfo->role_id==3){
					$sOrgazinerCode = "\t".$oOrganizerInfo->invite_code;
					$sOrgazinerName = $oOrganizerInfo->user_name;
				}
			}
			$line .= ','.str_ireplace(',','，',$sOrgazinerCode);
			$line .= ','.str_ireplace(',','，',$sOrgazinerName);
         	$line .= ','.str_ireplace(',','，',$v->user_name);
			// $line .= ','. str_ireplace(',','，',$v->user_email);
         	$line .= ','. str_ireplace(',','，',$v->user_tel);
			// $line .= ','. str_ireplace(',','，',$v->user_address);
			// $line .= ','. str_ireplace(',','，',$v->user_city);
			$line .= ','. str_ireplace(',','，',$sUserCompany);
         	$line .= ','. str_ireplace(',','，',$v->user_department);
			// $line .= ','. str_ireplace(',','，',$v->user_position);
         	$line .= ','.$type;
         	$line .= ','.$sSignInType;
         	$line .= ','.$v->score;
         	$line .= ','.$type_part;
         	$line .= ','.$v->created_at;
         	$line .= ','.$answer;
         	$sCommunityFlag = '';
         	if(is_numeric($v->user_company)&&$v->user_company!=0){
         		$oCommunityLog = HospitalCommunity::where('hospital_code',$v->user_company)->first();
         		if(count($oCommunityLog)){
         			$sCommunityFlag = '社区目标医院';
         		}
         	}
         	$line .= ','.$sCommunityFlag;
         	$line .= "\n";
         }
		
		$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	/*
	 * 线下会议签到log(已废弃)
	 */
	public function SignInLogOffline($iId){
		set_time_limit(0);
		$oLog = SignInLog::select('sign_in_log.*','user.superior_ids','user.user_name','user.user_nick','user.user_tel','user.user_county','user.user_company','user.user_company_name','user.user_department','user.link_rep_id')
				->leftJoin('user','user.id','=','sign_in_log.user_id')
				->where('cat_id',1)
				->where('video_id',$iId)
				->get();
		//导出csv
		header( "Cache-Control: public" );
		header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
		header("Content-Disposition:attachment;filename=".date('Ymdhis').".csv");
		header('Content-Type:APPLICATION/OCTET-STREAM');
		ob_start();
		$header = '';
		$header .= "大区,地区,用户编号,CWID,代表,组织者用户编码,组织者姓名,参会者id,参会者姓名,手机号码,区县,医院,科室,签到类型,签到科室会类型,签到得分,是否参与调研,签到时间,调研答案,线下会议签到次数"."\n";;
		$header_str =  iconv("utf-8",'gbk',$header);
		$line="";
		$aSurveyQuestionIds = SurveyQuestion::where('face_video_id',$iId)->lists('id','id');
		$aHospital = Hospital::lists('name','code');
		foreach($oLog as $k=>$v){
			if($v->score>0){
				$type="科室会签到";
			}else{
				$type="自行观看并签到";
			}
			if($v->video_type==1){
				$sSignInType = "录播签到";
			}else{
				$sSignInType = "直播签到";
			}
			$oLogAn = null;
			if($aSurveyQuestionIds){
				$oLogAn = SurveyQuestionAnswer::whereIn('survey_question_id',$aSurveyQuestionIds)
					->where('user_id',$v->user_id)
					->get();
			}
			if(count($oLogAn)){
				$type_part = "是";
				$answer = '';
				foreach($oLogAn as $an){
					$answer .= $an->option.',';
				}
				$answer = substr($answer, 0,-1);
			}else{
				$type_part = "否";
				$answer = '';
			}
			//代表信息
			// $aSuperiorIds = unserialize($v->superior_ids);
			$iRepId = $v->link_rep_id;
			if($iRepId){
				$oRepInfo = User::find($iRepId);
				$sR = $oRepInfo->user_regin;
				$sA = $oRepInfo->user_area;
				if(is_numeric($sR)){
					$sRegin = User::getReginCache();
					$oRegin = json_decode($sRegin);
					$oRepInfo->user_regin = $oRegin->$sR;
				}
				if(is_numeric($sA)){
					$sArea = User::getAreaCache();
					$oArea = json_decode($sArea);
					$oRepInfo->user_area = !empty($oArea->$sR->$sA)?$oArea->$sR->$sA:'';
				}
			}
			// $line .= ($k+1);
			// $line .= ','. str_ireplace(',','，',$v->user_nick);
			if($iRepId){
				$line .= $oRepInfo->user_regin;
				$line .= ','.$oRepInfo->user_area;
				$line .= ','.$oRepInfo->id;
				$line .= ','.$oRepInfo->user_cwid;
				$line .= ','.$oRepInfo->user_name;
			}else{
				$line .= ',,,,';
			}
			$sUserCompany = $v->user_company;
			if(is_numeric($sUserCompany)&&$sUserCompany!=0){
				$sUserCompany = Hospital::where('id',$sUserCompany)->pluck('name');
			}
			if(!$sUserCompany){
				$sUserCompany = $v->user_company_name;
			}
			$sOrgazinerCode = '';
			$sOrgazinerName = '';
			$iOrganizerId = $v->organizer_id;
			if($iOrganizerId>0&&($iOrganizerId!=$iRepId)){
				$oOrganizerInfo = User::find($iOrganizerId);
				if($oOrganizerInfo&&$oOrganizerInfo->role_id==3){
					$sOrgazinerCode = "\t".$oOrganizerInfo->invite_code;
					$sOrgazinerName = $oOrganizerInfo->user_name;
				}
			}
			$line .= ','.str_ireplace(',','，',$sOrgazinerCode);
			$line .= ','.str_ireplace(',','，',$sOrgazinerName);
			$line .= ','.str_ireplace(',','，',$v->user_id);
			$line .= ','.str_ireplace(',','，',$v->user_name);
			//         	$line .= ','. str_ireplace(',','，',$v->user_email);
			$line .= ','. str_ireplace(',','，',$v->user_tel);
			$line .= ','. str_ireplace(',','，',isset($aHospital[$v->user_county])?$aHospital[$v->user_county]:'');
			//         	$line .= ','. str_ireplace(',','，',$v->user_address);
			//         	$line .= ','. str_ireplace(',','，',$v->user_city);
			$line .= ','. str_ireplace(',','，',$sUserCompany);
			$line .= ','. str_ireplace(',','，',$v->user_department);
			//         	$line .= ','. str_ireplace(',','，',$v->user_position);
			$line .= ','.$type;
			$line .= ','.$sSignInType;
			$line .= ','.$v->score;
			$line .= ','.$type_part;
			$line .= ','.$v->created_at;
			$line .= ','.$answer;
			//线下会议签到次数统计
			$iOfflineSignInCount = SignInLog::where('user_id',$v->user_id)->where('video_id','>',100000)->count();
			$line .= ','.$iOfflineSignInCount;
			$line .= "\n";
		}
	
		$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	/*
	 * 新版线下会议签到log
	 */
	public function ExportOfflineSignInLog(){
		set_time_limit(0);
		$oLog = OfflineSignInLog::select('offline_sign_in_log.*','user.superior_ids','user.user_nick','user.user_name','user.user_email','user.user_tel','user.user_province','user.user_city','user.user_county','user.user_company','user.user_company_name','user.user_department','user.user_position','user.link_rep_id')
				->leftJoin('user','user.id','=','offline_sign_in_log.user_id')
				->orderby('created_at','desc')
				->get();
		//导出csv
		header( "Cache-Control: public" );
		header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
		header("Content-Disposition:attachment;filename=".date('Ymdhis').".csv");
		header('Content-Type:APPLICATION/OCTET-STREAM');
		ob_start();
		$header = '';
		$header .= "上级代表大区,上级代表地区,上级代表id,上级代表CWID,上级代表姓名,签到者id,签到者用户名,签到者姓名,邮箱,手机号码,省份,城市,区县,医院,科室,职称,会议码,是否新注册,签到时间"."\n";;
		$header_str =  iconv("utf-8",'gbk',$header);
		$line="";
		$aHospital = Hospital::lists('name','code');
		foreach($oLog as $k=>$v){
			//代表信息
			$iRepId = $v->link_rep_id;
			if($iRepId){
				$oRepInfo = User::find($iRepId);
				if($oRepInfo){
					$sR = $oRepInfo->user_regin;
					$sA = $oRepInfo->user_area;
					if(is_numeric($sR)){
						$sRegin = User::getReginCache();
						$oRegin = json_decode($sRegin);
						$oRepInfo->user_regin = $oRegin->$sR;
					}
					if(is_numeric($sA)){
						$sArea = User::getAreaCache();
						$oArea = json_decode($sArea);
						$oRepInfo->user_area = !empty($oArea->$sR->$sA)?$oArea->$sR->$sA:'';
					}
					$line .= $oRepInfo->user_regin;
					$line .= ','.$oRepInfo->user_area;
					$line .= ','.$oRepInfo->id;
					$line .= ','.$oRepInfo->user_cwid;
					$line .= ','.$oRepInfo->user_name;
				}else{
					$line .= ',,,,';
				}
			}else{
				$line .= ',,,,';
			}
			$sUserCompany = $v->user_company;
			if(is_numeric($sUserCompany)&&$sUserCompany!=0){
				$sUserCompany = Hospital::where('id',$sUserCompany)->pluck('name');
			}
			if(!$sUserCompany){
				$sUserCompany = $v->user_company_name;
			}
			$line .= ','.str_ireplace(',','，',$v->user_id);
			$line .= ','.str_ireplace(',','，',$v->user_nick);
			$line .= ','.str_ireplace(',','，',$v->user_name);
			$line .= ','. str_ireplace(',','，',$v->user_email);
			$line .= ','. str_ireplace(',','，',$v->user_tel);
			$line .= ','. str_ireplace(',','，',isset($aHospital[$v->user_province])?$aHospital[$v->user_province]:'');
			$line .= ','. str_ireplace(',','，',isset($aHospital[$v->user_city])?$aHospital[$v->user_city]:'');
			$line .= ','. str_ireplace(',','，',isset($aHospital[$v->user_county])?$aHospital[$v->user_county]:'');
			$line .= ','. str_ireplace(',','，',$sUserCompany);
			$line .= ','. str_ireplace(',','，',$v->user_department);
			$line .= ','. str_ireplace(',','，',$v->user_position);
			$line .= ','. $v->meeting_code;
			if($v->user_type == 1){
				$line .= ','. '旧用户签到';
			}else{
				$line .= ','. '新用户签到';
			}
			$line .= ','.$v->created_at;
			$line .= "\n";
		}
		$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	public function SurveyLog($iId){
		$oSurveyQuestionFirstId = SurveyQuestion::where('face_video_id',$iId)->first();
		$oLog = SurveyQuestionAnswer::select('survey_question_answer.user_id','survey_question_answer.survey_question_id','survey_question_answer.created_at','user.user_name','user.user_nick','user.user_tel','user.user_email','user.user_address','user.user_city','user.user_department','user.user_position','user.user_company','user.user_company_name')
				->leftJoin('user','user.id','=','survey_question_answer.user_id')
				->where('survey_question_id',$oSurveyQuestionFirstId->id)
				->get();
		//导出csv
		header( "Cache-Control: public" );
    	header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
    	header("Content-Disposition:attachment;filename=".date('Ymdhis').".csv");
    	header('Content-Type:APPLICATION/OCTET-STREAM');
    	ob_start();
    	$header = '';
		$header .= "编号,用户名,用户姓名,邮箱,手机号码,地址,城市,科室,职称,医院,题号,答案,调研时间"."\n";
		$header_str =  iconv("utf-8",'gbk',$header);
		$line="";
		$aSurveyQuestionIds = SurveyQuestion::where('face_video_id',$iId)->lists('id','id');
        foreach($oLog as $k=>$v){
        	$sUserCompany = $v->user_company;
        	if(is_numeric($sUserCompany)&&$sUserCompany!=0){
        		$sUserCompany = Hospital::where('id',$sUserCompany)->pluck('name');
        	}
        	if(!$sUserCompany){
        		$sUserCompany = $v->user_company_name;
        	}
        	$oLogAn = SurveyQuestionAnswer::whereIn('survey_question_id',$aSurveyQuestionIds)
    				->where('user_id',$v->user_id)
					->get();
			$answer = '';
			foreach($oLogAn as $k=> $an){
				// $answer .= $an->option.'/';
				$line .= ($k+1);
	         	$line .= ','. str_ireplace(',','，',$v->user_nick);
	         	$line .= ','. str_ireplace(',','，',$v->user_name);
	         	$line .= ','. str_ireplace(',','，',$v->user_email);
	         	$line .= ','. str_ireplace(',','，',$v->user_tel);
	         	$line .= ','. str_ireplace(',','，',$v->user_address);
	         	$line .= ','. str_ireplace(',','，',$v->user_city);
	         	$line .= ','. str_ireplace(',','，',$v->user_department);
	         	$line .= ','. str_ireplace(',','，',$v->user_position);
	         	$line .= ','. str_ireplace(',','，',$sUserCompany);
	         	$line .= ','.($k+1);
	         	$line .= ','.$an->option;
	         	$line .= ','.$v->created_at;
	         	$line .= "\n";
			}
			// $answer = substr($answer, 0,-1);
		}
		$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	/*public function ExportLog($iId){
		set_time_limit(0);
		$oUser = User::where('role_id',2)->get();
		//导出csv
		header( "Cache-Control: public" );
   		header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
	   	header("Content-Disposition:attachment;filename=".date('Ymdhis').".csv");
	   	header('Content-Type:APPLICATION/OCTET-STREAM');
	   	ob_start();
		$line="";
		foreach($oUser as $k=> $user){
			$oLog = VideoLog::where('cat_id',1)
				->where('user_id',$user->id)
				->where('video_id',$iId)
				->where('watch_minutes','>',0)
				->count();//录播
			if($oLog){
				$iMin1 = VideoLog::where('cat_id',1)
					->where('user_id',$user->id)
					->where('link_user_id',0)
					->where('video_id',$iId)
					->where('video_type',1)
					->pluck('watch_minutes');//录播
				$iMin2 = VideoLog::where('cat_id',1)
					->where('user_id',$user->id)
					->where('link_user_id',0)
					->where('video_id',$iId)
					->where('video_type',2)
					->pluck('watch_minutes');//直播
				$iMin1 = $iMin1?$iMin1:0;
				$iMin2 = $iMin2?$iMin2:0;
				$line .= $user->id;
	         	$line .= ','. str_ireplace(',','，',$user->user_regin);
	         	$line .= ','. str_ireplace(',','，',$user->user_area);
	         	$line .= ','. str_ireplace(',','，',$user->user_name);
	         	$line .= ','.$iMin2;
	         	$line .= ','.$iMin1;
	         	$line .= "\n";
	         	$line .= $this->getUserExportInfo($iId,$user->id,3);
			}
		}
		$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $file_str;
	}
	
	public function getUserExportInfo($iId,$iUid,$count){
		$oUser = User::where('link_user_id',$iUid)->get();
		if(!count($oUser)){
			return '';
		}
		$line = '';
		foreach($oUser as $user){
			$oLog = VideoLog::where('cat_id',1)
				->where('user_id',$user->id)
				->where('video_id',$iId)
				->where('watch_minutes','>',0)
				->count();//录播
			if($oLog){
				$iMin1 = VideoLog::where('cat_id',1)
					->where('user_id',$user->id)
					->where('link_user_id',0)
					->where('video_id',$iId)
					->where('video_type',1)
					->pluck('watch_minutes');//录播
				$iMin2 = VideoLog::where('cat_id',1)
					->where('user_id',$user->id)
					->where('link_user_id',0)
					->where('video_id',$iId)
					->where('video_type',2)
					->pluck('watch_minutes');//直播
				$iMin1 = $iMin1?$iMin1:0;
				$iMin2 = $iMin2?$iMin2:0;
				for($i=0;$i<$count;$i++){
		         	$line .= ',';
		        }
	         	$line .= ','. str_ireplace(',','，',$user->user_name);
	         	$line .= ','. str_ireplace(',','，',$user->user_company);
	         	$line .= ','.$iMin2;
	         	$line .= ','.$iMin1;
	         	$line .= "\n";
	         	$line .= $this->getUserExportInfo($iId,$user->id,$count+1);
			}
		}
		return $line;
	}*/

	public function ExportLog($iId){
		set_time_limit(0);
		$oUser = User::where('role_id',2)->get();
		//导出csv
		header( "Cache-Control: public" );
    	header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
    	header("Content-Disposition:attachment;filename=".date('Ymdhis').".csv");
    	header('Content-Type:APPLICATION/OCTET-STREAM');
    	ob_start();
    	$header = '';
		$header .= "直播时长,录播时长,用户编号,大区,地区,代表姓名,CWID,一级医生姓名,医院,二级医生姓名,医院,三级医生姓名,医院,四级医生姓名,医院"."\n";
		$header_str =  iconv("utf-8",'gbk',$header);
		$line="";
		$iKey = 0;
		foreach($oUser as $k=> $user){
			$line_pre = '';
			$sR = $user->user_regin;
			$sA = $user->user_area;
			if(is_numeric($sR)){
		   		$sRegin = User::getReginCache();
				$oRegin = json_decode($sRegin);
				$user->user_regin = $oRegin->$sR;
		   	}
			if(is_numeric($sA)){
		   		$sArea = User::getAreaCache();
				$oArea = json_decode($sArea);
				$user->user_area = $oArea->$sR->$sA;
		   	}
			$oLog = VideoLog::where('cat_id',1)
				->where('user_id',$user->id)
				->where('video_id',$iId)
				->where('watch_minutes','>',0)
				->count();//录播
			// if($oLog){
				$iMin1 = VideoLog::where('cat_id',1)
					->where('user_id',$user->id)
					->where('link_user_id',0)
					->where('video_id',$iId)
					->where('video_type',1)
					->pluck('watch_minutes');//录播
				$iMin2 = VideoLog::where('cat_id',1)
					->where('user_id',$user->id)
					->where('link_user_id',0)
					->where('video_id',$iId)
					->where('video_type',2)
					->pluck('watch_minutes');//直播
				$iMin1 = $iMin1?$iMin1:0;
				$iMin2 = $iMin2?$iMin2:0;
				$line .= $iMin2;
				$line .= ','.$iMin1;
				$line .= ','.($iKey+1);
				$line .= ','.str_ireplace(',','，',$user->user_regin);
	         	$line .= ','. str_ireplace(',','，',$user->user_area);
				$line .= ','. str_ireplace(',','，',$user->user_name);
				$line .= ','. str_ireplace(',','，',$user->user_cwid);
				$line_pre .= ','.str_ireplace(',','，',$user->user_regin);
	         	$line_pre .= ','. str_ireplace(',','，',$user->user_area);
				$line_pre .= ','. str_ireplace(',','，',$user->user_name);
				$line_pre .= ','. str_ireplace(',','，',$user->user_cwid);
	         	$line .= "\n";
	         	$line .= $this->getUserExportInfo($iId,$user->id,$iKey+1,1,$line_pre);
	         	$iKey++;
			// }
		}
		$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	public function getUserExportInfo($iId,$iUid,$str,$count,$line_pre=''){
		$oUser = User::where('link_user_id',$iUid)->get();
		if(!count($oUser)){
			return '';
		}
		$line = '';
		foreach($oUser as $key_user=> $user){
			$oLog = VideoLog::where('cat_id',1)
				->where('user_id',$user->id)
				->where('video_id',$iId)
				->where('watch_minutes','>',0)
				->count();//录播
			// if($oLog){
				$iMin1 = VideoLog::where('cat_id',1)
					->where('user_id',$user->id)
					->where('link_user_id',0)
					->where('video_id',$iId)
					->where('video_type',1)
					->pluck('watch_minutes');//录播
				$iMin2 = VideoLog::where('cat_id',1)
					->where('user_id',$user->id)
					->where('link_user_id',0)
					->where('video_id',$iId)
					->where('video_type',2)
					->pluck('watch_minutes');//直播
				$iMin1 = $iMin1?$iMin1:0;
				$iMin2 = $iMin2?$iMin2:0;
				$c = 0;
				$line .= $iMin2;
				$line .= ','.$iMin1;
				$line .= ','.$str.'.'.($key_user+1);
				// $line .= ',';$c++;
				// $line .= ',';$c++;
				// $line .= ',';$c++;
				// if($count==1){
				// $line .= ',';$c++;
				// }else{
				// $line .= ',';$c++;
				// for($i=0;$i<$count-1;$i++){
				// $line .= ',';$c++;
				// $line .= ',';$c++;
				// }
				// }
				$sUserCompany = $user->user_company;
				if(is_numeric($sUserCompany)&&$sUserCompany!=0){
					$sUserCompany = Hospital::where('id',$sUserCompany)->pluck('name');
				}
				if(!$sUserCompany){
					$sUserCompany = $user->user_company_name;
				}
				$line_pre2 = $line_pre;
				$line .= $line_pre2;
	         	$line .= ','. str_ireplace(',','，',$user->user_name);$c++;
	         	$line .= ','. str_ireplace(',','，',$sUserCompany);$c++;
	         	$line_pre2 .= ','. str_ireplace(',','，',$user->user_name);$c++;
	         	$line_pre2 .= ','. str_ireplace(',','，',$sUserCompany);$c++;
	         	$line .= "\n";
	         	$line .= $this->getUserExportInfo($iId,$user->id,$str.'.'.($key_user+1),$count+1,$line_pre2);
			// }
		}
		return $line;
	}
	
	public function ExportWeekLog($iId,$count=0){
        ini_set("memory_limit","512M");
		set_time_limit(0);
		$oUser = User::select('id','user_regin','link_rep_id','user_area','user_name','invite_code','user_cwid','user_province','user_city','user_tel','user_score','user_live_score','user_review_score','is_h5','user_position','created_at')
			->where('role_id',2)
			->where('is_locked','N');
			if($count){
				$oUser = $oUser->offset(($count-1)*250);//页数
				$oUser = $oUser->limit(250);
			}
		/*switch($count){
			case 1:
				$oUser = $oUser->where('id','<=','300');
				break;
			case 2:
				$oUser = $oUser->where('id','<=','600')->where('id','>=','301');
				break;
			case 3:
				$oUser = $oUser->where('id','<=','1000')->where('id','>=','601');
				break;
			case 4:
				$oUser = $oUser->where('id','<=','1700')->where('id','>=','1001');
				break;
			case 5:
				$oUser = $oUser->where('id','<=','2001')->where('id','>=','1701');
				break;
			case 6:
				$oUser = $oUser->where('id','>=','2001');
				break;
			default:
				break;
		}*/
		$oUser = $oUser->get();
		// echo count($oUser);die;
		// $log = DB::getQueryLog();
		// print_r($log);die;
		//导出csv
        $dayDate = "0".$count."-".date('Ymdhis');
		header('Content-Type:APPLICATION/OCTET-STREAM');
		header("Content-Disposition: attachment;filename = {$dayDate}.csv");  
		header('Cache-Control: max-age=0');
        $fp = fopen('php://output', 'a'); 
		// header("Content-type:application/vnd.ms-excel");
		// header("Content-Disposition:attachment;filename=0".$count."-".date('Ymdhis').".csv");
		// header('Content-Type:APPLICATION/OCTET-STREAM');
		ob_end_clean();
    	$header = '';
    	$sStartTime = FaceVideo::where('id',$iId)->pluck('start_time');
    	$aData = $this->getWeekRange($sStartTime);
    	$aVideo = FaceVideo::where('start_time','<',$aData[1])
			->where('start_time','>',$aData[0])
			->orderBy('start_time','ASC')
			->lists('id');
    	$iVideoId1 = $aVideo[0];
    	$iVideoId2 = 0;
    	$iVideoId3 = 0;
    	if(isset($aVideo[1])){
    		$iVideoId2 = $aVideo[1];
    	}
    	if(isset($aVideo[2])){
    		$iVideoId3 = $aVideo[2];
    	}
    	if($iVideoId2!=0){
	    	if($iVideoId1<$iVideoId2){
	    		$iVideoMinId = $iVideoId1;
	    	}else{
	    		$iVideoMinId = $iVideoId2;
	    	}
    	}else{
    		$iVideoMinId = $iVideoId1;
    	}
    	if($iVideoId3){
	    	if($iVideoId3<$iVideoMinId){
	    		$iVideoMinId = $iVideoId3;
	    	}
    	}
		$header .= "本周第一期上线方式,本周第二期上线方式,本周第三期上线方式,身份,用户姓名,用户编码,本周第一期直播时长,本周第一期录播时长,本周第二期直播时长,本周第二期录播时长,本周第三期直播时长,本周第三期录播时长,既往直播时长,既往录播时长,用户层级,上级用户姓名,上级用户编码,最终代表,代表CWID,地区,大区,cluster,省份,城市,区域,医院,科室,职称,注册时间,是否H5注册,医院所属标签";
		// $header .= "本周第一期上线方式,本周第二期上线方式,本周第三期上线方式,身份,用户姓名,用户编码,本周第一期直播时长,本周第一期录播时长,本周第二期直播时长,本周第二期录播时长,本周第三期直播时长,本周第三期录播时长,既往直播时长,既往录播时长,用户层级,上级用户姓名,上级用户编码,最终代表,代表CWID,地区,大区,cluster,省份,城市,区域,医院,科室,职称,注册时间,是否H5注册,医院所属标签"."\n";
		$header_str =  explode(",", iconv("utf-8",'gbk',$header));
		// print_r($header_str);die;
        fputcsv($fp, $header_str);
		$iKey = 0;
		$aReginData = DB::table('regin')->lists('regin_name','id');
		$aAreaData = DB::table('area')->lists('area_name','id');
		$oUids = array();
		// print_r(count($oUser)."<br />");
		foreach($oUser as $k=> $user){
			$line="";
			$line_pre = '';
			$sR = $user->user_regin;
			$sA = $user->user_area;
			$cluster = User::getClusterByReginId2($sR);
			if(is_numeric($sR)){
				// $sRegin = User::getReginCache();
				// $oRegin = json_decode($sRegin);
				// $user->user_regin = $oRegin->$sR;
				$user->user_regin = isset($aReginData[$sR])?$aReginData[$sR]:$sR;
		   	}
			if(is_numeric($sA)){
				// $sArea = User::getAreaCache();
				// $oArea = json_decode($sArea);
				// $oThisArea = $oArea->$sR;
				// if(isset($oThisArea->$sA))
				// $user->user_area = $oThisArea->$sA;
				$user->user_area = isset($aAreaData[$sA])?$aAreaData[$sA]:$sA;
		   	}
			$aMin1 = $this->getUserVideoMin($user->id, $iVideoId1);
			$iMin11 = $aMin1[0];//本周第1期录播
			$iMin12 = $aMin1[1];//本周第1期直播
			$aMin2 = $this->getUserVideoMin($user->id, $iVideoId2);
			$iMin21 = $aMin2[0];//本周第2期录播
			$iMin22 = $aMin2[1];//本周第2期直播
			$aMin3 = $this->getUserVideoMin($user->id, $iVideoId3);
			$iMin31 = $aMin3[0];//本周第3期录播
			$iMin32 = $aMin3[1];//本周第3期直播
			//本周第一期直播上线方式
			$sScoreWay1 = $this->getScoreWay($iMin12,$iVideoId1,$user->id);
			//本周第二期直播上线方式
			$sScoreWay2 = $this->getScoreWay($iMin22,$iVideoId2,$user->id);
			//本周第三期直播上线方式
			$sScoreWay3 = $this->getScoreWay($iMin32,$iVideoId3,$user->id);
			//既往录播时长
			$iOldMin1 = $this->getOldMin($user->id,$iVideoMinId,1);
			//既往直播时长
			$iOldMin2 = $this->getOldMin($user->id,$iVideoMinId,2);
			$user = $this->formatUserInfo($user);
			$line .= $sScoreWay1;
			$line .= ','.$sScoreWay2;
			$line .= ','.$sScoreWay3;
			$line .= ',代表';
			$line .= ','. str_ireplace(',','，',$user->user_name);
			$line .= ','. str_ireplace(',','，',"\t".$user->invite_code);
			$line .= ','.$iMin12;
			$line .= ','.$iMin11;
			$line .= ','.$iMin22;
			$line .= ','.$iMin21;
			$line .= ','.$iMin32;
			$line .= ','.$iMin31;
			$line .= ','.$iOldMin2;
			$line .= ','.$iOldMin1;
			$line .= ',0';
			$line .= ',';
			$line .= ',';
			$line .= ',';
			$line .= ','. str_ireplace(',','，',$user->user_cwid);
			$line .= ','. str_ireplace(',','，',$user->user_area);
			$line .= ','.str_ireplace(',','，',$user->user_regin);
			$line .= ','.$cluster;
			$line .= ','. str_ireplace(',','，',$user->user_province);
			$line .= ','. str_ireplace(',','，',$user->user_city);
			$line .= ',';
			$line .= ',';
			$line .= ',';
			$line_pre .= ','. str_ireplace(',','，',$user->user_name);
			$line_pre .= ','. str_ireplace(',','，',$user->user_cwid);
			$line_pre .= ','. str_ireplace(',','，',$user->user_area);
			$line_pre .= ','.str_ireplace(',','，',$user->user_regin);
			$line_pre .= ','.$cluster;
			$line .= ','.$user->user_position;
			$line .= ','.$user->created_at;
			if ($user->is_h5==1) {
				$line .= ',H5注册目标';
			} elseif($user->is_h5==2) {
				$line .= ',H5注册非目标';
			} elseif($user->is_h5==3) {
				$line .= ',H5注册商务';
			} elseif($user->is_h5==4) {
				$line .= ',H5注册专项基金';
			} elseif($user->is_h5==5) {
				$line .= ',H5注册四川全科骨干医生';
			} else {
				$line .= ',';
			}
			$line .= ',';
			// $line .= ','.$user->user_tel;
			// $line .= ','.$user->user_score;
			// $line .= ','.$user->user_live_score;
			// $line .= ','.$user->user_review_score;
         	// $line .= "\n";
			$line_str = explode(",", iconv("utf-8",'GBK//TRANSLIT',$line));
			fputcsv($fp, $line_str);
         	$iKey++;
			$this->getUserExportWeekInfo($iId,$user->id,$iKey+1,1,$line_pre,$iVideoId1,$iVideoId2,$iVideoId3,$iVideoMinId,0);
		}
		die;
		$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	public function getOldMin($iUId,$iVideoMinId,$iVideoType){
		$iMin = VideoLog::where('user_id',$iUId)
			->where('video_id','<',$iVideoMinId)
			->where('video_type',$iVideoType)
			->sum('watch_minutes');
		$oDownLoad = VideoDownloadMinutes::where('user_id',$iUId)->where('video_id',$iVideoMinId)->first();
		if($oDownLoad){
			$iMin = $iMin + $oDownLoad->video_minutes;
		}
		return $iMin;
	}
	
	public function getScoreWay($iMin,$iVideoId,$iUId){
		$sScoreWay = '';
		if($iMin>0){
			$oSignInLog = SignInLog::select('id')
				->where('user_id',$iUId)
				->where('video_id',$iVideoId)
				->where('score','>',0)
				->where('video_type',2)
				->first();
			if($oSignInLog){
				$sScoreWay = "扫码";
			}else{
				$sScoreWay = "终端";
			}
		}
		return $sScoreWay;
	}
	
	//获取用户观看视频录播和直播时长
	public function getUserVideoMin($iUId,$iVideoId){
		$iMin1 = VideoLog::where('cat_id',1)
			->where('user_id',$iUId)
			->where('link_user_id',0)
			->where('video_id',$iVideoId)
			->where('video_type',1)
			->pluck('watch_minutes');//本周第一期录播
		$iMin2 = VideoLog::where('cat_id',1)
			->where('user_id',$iUId)
			->where('link_user_id',0)
			->where('video_id',$iVideoId)
			->where('video_type',2)
			->pluck('watch_minutes');//本周第一期直播
		$oDownLoad = VideoDownloadMinutes::where('user_id',$iUId)->where('video_id',$iVideoId)->first();
		if($oDownLoad){
			$iMin1 = $iMin1 + $oDownLoad->video_minutes;
		}
		$iMin1 = $iMin1?$iMin1:0;
		$iMin2 = $iMin2?$iMin2:0;
		return array($iMin1,$iMin2);
	}
	
	public function getWeekRange($date){
       	$lastday=date('Y-m-d H:i:s',strtotime("$date Sunday")); 
    	$firstday=date('Y-m-d H:i:s',strtotime("$lastday -6 days")); 
    	return array($firstday,$lastday); 
	}
	
	public function getUserExportWeekInfo($iId,$iUid,$str,$count,$line_pre='',$iVideoId1,$iVideoId2,$iVideoId3,$iVideoMinId,$iUserLevel){
		$fp = fopen('php://output', 'a');
		$iUserLevel = $iUserLevel+1;
		$oUser = User::select('id','link_user_id','user_name','invite_code','user_province','user_city','user_county','user_company','user_company_name','user_department','user_tel','user_score','user_live_score','user_review_score','is_h5','user_position','created_at')
		// $oUser = User::select('id')
			->where('link_user_id',$iUid)
			->where('is_locked','N')
			->get();
		if(!count($oUser)){
			return '';
		}
		$oUserids = array();
		foreach($oUser as $key_user=> $user){
			$line = '';
			$sCommunityFlag = '';
			if(is_numeric($user->user_company)&&$user->user_company!=0){
				$oCommunityLog = HospitalCommunity::where('hospital_code',$user->user_company)->first();
				if(count($oCommunityLog)){
					$sCommunityFlag = '社区目标医院';
				}
			}
			$oUserids[] = $user->id;
			$aMin1 = $this->getUserVideoMin($user->id, $iVideoId1);
			$iMin11 = $aMin1[0];//本周第1期直播
			$iMin12 = $aMin1[1];//本周第1期录播
			$aMin2 = $this->getUserVideoMin($user->id, $iVideoId2);
			$iMin21 = $aMin2[0];//本周第2期直播
			$iMin22 = $aMin2[1];//本周第2期录播
			$aMin3 = $this->getUserVideoMin($user->id, $iVideoId3);
			$iMin31 = $aMin3[0];//本周第3期直播
			$iMin32 = $aMin3[1];//本周第3期录播
			//本周第一期直播上线方式
			$sScoreWay1 = $this->getScoreWay($iMin12,$iVideoId1,$user->id);
			//本周第二期直播上线方式
			$sScoreWay2 = $this->getScoreWay($iMin22,$iVideoId2,$user->id);
			//本周第二期直播上线方式
			$sScoreWay3 = $this->getScoreWay($iMin32,$iVideoId3,$user->id);
			//既往录播时长
			$iOldMin1 = $this->getOldMin($user->id,$iVideoMinId,1);
			//既往直播时长
			$iOldMin2 = $this->getOldMin($user->id,$iVideoMinId,2);
			//上级用户
			$oLinkUser = User::select('user_name','invite_code')->where('id',$user->link_user_id)->first();
			if($oLinkUser){
				$oLinkUserName = $oLinkUser->user_name;
				$oLinkUserCode = "\t".$oLinkUser->invite_code;
			}else{
				$oLinkUserName = '';
				$oLinkUserCode = '';
			}
			$user = $this->formatUserInfo($user);
			$line .= $sScoreWay1;
			$line .= ','.$sScoreWay2;
			$line .= ','.$sScoreWay3;
			$line .= ',医生';
			$line .= ','. str_ireplace(',','，',$user->user_name);
			$line .= ','. str_ireplace(',','，',"\t".$user->invite_code);
			$line .= ','.$iMin12;
			$line .= ','.$iMin11;
			$line .= ','.$iMin22;
			$line .= ','.$iMin21;
			$line .= ','.$iMin32;
			$line .= ','.$iMin31;
			$line .= ','.$iOldMin2;
			$line .= ','.$iOldMin1;
			$line .= ','.$iUserLevel;
			$line .= ','. str_ireplace(',','，',$oLinkUserName);
			$line .= ','. str_ireplace(',','，',$oLinkUserCode);
			$line .= $line_pre;
			$line .= ','. str_ireplace(',','，',$user->user_province);
			$line .= ','. str_ireplace(',','，',$user->user_city);
			$line .= ','. str_ireplace(',','，',$user->user_county);
			$line .= ','. str_ireplace(',','，',$user->user_company);
			$line .= ','. str_ireplace(',','，',$user->user_department);
			$line .= ','.$user->user_position;
			$line .= ','.$user->created_at;
			// $line .= ','.$user->user_tel;
			// $line .= ','.$user->user_score;
			// $line .= ','.$user->user_live_score;
			// $line .= ','.$user->user_review_score;
			if ($user->is_h5==1) {
				$line .= ',H5注册目标';
			} elseif($user->is_h5==2) {
				$line .= ',H5注册非目标';
			} elseif($user->is_h5==3) {
				$line .= ',H5注册商务';
			} elseif($user->is_h5==4) {
				$line .= ',H5注册专项基金';
			} elseif($user->is_h5==5) {
				$line .= ',H5注册四川全科骨干医生';
			} else {
				$line .= ',';
			}
			$line .= ','.$sCommunityFlag;
         	// $line .= "\n";
         	$line_str = explode(",", iconv("utf-8",'GBK//TRANSLIT',$line));
			fputcsv($fp,$line_str);
			$this->getUserExportWeekInfo($iId,$user->id,$str.'.'.($key_user+1),$count+1,$line_pre,$iVideoId1,$iVideoId2,$iVideoId3,$iVideoMinId,$iUserLevel);
			// }
		}
		// if($oUserids){
		// 	$this->getUserExportWeekInfo($iId,$user->id,$iKey+1,1,$line_pre,$iVideoId1,$iVideoId2,$iVideoId3,$iVideoMinId,0,$oUserids);
		// }
		return $line;
	}
	
	//清除某场直播积分
	public function ClearUserScore($iId){
		set_time_limit(0);
		$oVideoLog = VideoLog::where('cat_id',1)
			->where('video_id',$iId)
			->where('video_type','2')
			->where('watch_minutes','>',0)
			->get();
		foreach($oVideoLog as $k=> $v){
			$iScore = $v->watch_minutes;
			//用户减去相应积分
			$oUser = User::find($v->user_id);
			$iOldTotalScore = $oUser->user_score;
			$iOldLIveScore = $oUser->user_live_score;
			$iNewTotalSCore = ($iOldTotalScore-$iScore)>0?($iOldTotalScore-$iScore):0;
			$iNewLiveScore = ($iOldLIveScore-$iScore)>0?($iOldLIveScore-$iScore):0;
			$oUser->user_score = $iNewTotalSCore;
			$oUser->user_live_score = $iNewLiveScore;
			$oUser->save();
			//log时长清0；
			$oVideoLogOne = VideoLog::find($v->id);
			$oVideoLogOne->watch_minutes = 0;
			$oVideoLogOne->save();
		}
		return 'success';
	}
	
	public function CalcuUserScore(){
		set_time_limit(0);
		$oUser = User::get();
		foreach ($oUser as $k=>$v){
			$iUserId = $v->id;
			$oUserOne = User::find($iUserId);
			//直播时长
			$iUserTotalLiveMin = VideoLog::where('user_id',$iUserId)->where('video_type','2')->sum('watch_minutes');
			//录播时长
			$iUserTotalReviewMin = VideoLog::where('user_id',$iUserId)->where('video_type','1')->sum('watch_minutes');
			$iUserTotalLiveScore = $iUserTotalLiveMin;
			$iUserTotalReviewScore = $iUserTotalReviewMin;
			$oUserOne->user_live_score = $iUserTotalLiveScore;
			$oUserOne->user_review_score = $iUserTotalReviewScore;
			$oUserOne->user_score = $iUserTotalLiveScore+$iUserTotalReviewScore;
			$oUserOne->save();
		}
		return 'success';
	}
	
	public function getMonthRange($sMounth){
		$timestamp = strtotime($sMounth);
		$firstday=date('Y-m-d H:i:s',$timestamp); 
    	$lastday=date('Y-m-d H:i:s',strtotime("$firstday +1 months")); 
    	return array($firstday,$lastday); 
	}
	
	//月记录导出
	public function MounthLog($sMounth,$count=0){
		set_time_limit(0);
		$oUser = User::select('id','user_regin','user_area','user_name','invite_code','user_cwid','user_province','user_city','is_h5','created_at')
			->where('role_id',2)
			->where('is_locked','N');
		if($count){
			$oUser = $oUser->offset(($count-1)*250);//页数
			$oUser = $oUser->limit(250);
		}
		/*switch($count){
			case 1:
				$oUser = $oUser->where('id','<=','300');
				break;
			case 2:
				$oUser = $oUser->where('id','<=','600')->where('id','>=','301');
				break;
			case 3:
				$oUser = $oUser->where('id','<=','1000')->where('id','>=','601');
				break;
			case 4:
				$oUser = $oUser->where('id','<=','1700')->where('id','>=','1001');
				break;
			case 5:
				$oUser = $oUser->where('id','>=','1701');
				break;
			default:
				break;
		}*/
		$oUser = $oUser->get();
		//导出csv
		header( "Cache-Control: public" );
    	header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
    	header("Content-Disposition:attachment;filename=0".$count."-".$sMounth."-".date('Ymdhis').".csv");
    	header('Content-Type:APPLICATION/OCTET-STREAM');
    	ob_start();
    	$header = '';
    	$aData = $this->getMonthRange($sMounth);
    	$aVideo = FaceVideo::where('start_time','<',$aData[1])
			->where('start_time','>',$aData[0])
			->orderBy('start_time','ASC')
			->lists('id');
    	$iCountVideo = 0;
    	if($aVideo){
    		$iCountVideo = count($aVideo);
    		$arr = array();
    		foreach($aVideo as $k=>$v){
    			$arr[$v] = $v;
    		}
    		$iVideoMinId = array_search(min($arr), $arr);
			// dd($iVideoMinId);
    	}else{
    		return 'has no log';
    	}
		for($i=0;$i<$iCountVideo;$i++){
			if(!isset($aVideo[$i])){
				$aVideo[$i] = 0;
			}
		}
		for($i=1;$i<=$iCountVideo;$i++){
			$header .= "本月第".$i."期上线方式,";
		}
		$header .= "身份,用户姓名,用户编码,";
		for($i=1;$i<=$iCountVideo;$i++){
			$header .= "本月第".$i."期直播时长,本月第".$i."期录播时长,";
		}
		$header .= "既往直播时长,既往录播时长,用户层级,上级用户姓名,上级用户编码,最终代表,代表CWID,地区,大区,cluster,省份,城市,区域,医院,科室,是否H5注册,注册时间"."\n";
		$header_str =  iconv("utf-8",'gbk',$header);
		$line="";
		$iKey = 0;
		$aReginData = DB::table('regin')->lists('regin_name','id');
		$aAreaData = DB::table('area')->lists('area_name','id');
		foreach($oUser as $k=> $user){
			$line_pre = '';
			$sR = $user->user_regin;
			$sA = $user->user_area;
			$cluster = User::getClusterByReginId2($sR);
			if(is_numeric($sR)){
				// $sRegin = User::getReginCache();
				// $oRegin = json_decode($sRegin);
				// $user->user_regin = $oRegin->$sR;
				$user->user_regin = isset($aReginData[$sR])?$aReginData[$sR]:$sR;
		   	}
			if(is_numeric($sA)){
				// $sArea = User::getAreaCache();
				// $oArea = json_decode($sArea);
				// if(isset($oArea->$sA))
				// $user->user_area = $oArea->$sR->$sA;
				$user->user_area = isset($aAreaData[$sA])?$aAreaData[$sA]:$sA;
		   	}
		   	$iMin1 = $iMin2 = $sScoreWay = array();
		   	foreach($aVideo as $k=>$v){
				$aMin = $this->getUserVideoMin($user->id, $v);
				$iMin1[$k] = $aMin[0];//本月第1期录播
				$iMin2[$k] = $aMin[1];//本月第1期直播
				//直播上线方式
				$sScoreWay[$k] = $this->getScoreWay($aMin[1],$v,$user->id);
		   	}
			//既往录播时长
			$iOldMin1 = $this->getOldMin($user->id,$iVideoMinId,1);
			//既往直播时长
			$iOldMin2 = $this->getOldMin($user->id,$iVideoMinId,2);
			$user = $this->formatUserInfo($user);
			$line .= $sScoreWay[0];
			for($i=1;$i<$iCountVideo;$i++){
				$line .= ','.$sScoreWay[$i];
			}
			$line .= ',代表';
			$line .= ','. str_ireplace(',','，',$user->user_name);
			$line .= ','. str_ireplace(',','，',"\t".$user->invite_code);
			for($i=0;$i<$iCountVideo;$i++){
				$line .= ','.$iMin2[$i];
				$line .= ','.$iMin1[$i];
			}
			$line .= ','.$iOldMin2;
			$line .= ','.$iOldMin1;
			$line .= ',0';
			$line .= ',';
			$line .= ',';
			$line .= ',';
			$line .= ','. str_ireplace(',','，',$user->user_cwid);
			$line .= ','. str_ireplace(',','，',$user->user_area);
			$line .= ','.str_ireplace(',','，',$user->user_regin);
			$line .= ','.$cluster;
			$line .= ','. str_ireplace(',','，',$user->user_province);
			$line .= ','. str_ireplace(',','，',$user->user_city);
			$line .= ',';
			$line .= ',';
			$line .= ',';
			// $line .= ','.$user->is_h5;
			if ($user->is_h5==1) {
				$line .= ',H5注册目标';
			} elseif($user->is_h5==2) {
				$line .= ',H5注册非目标';
			} elseif($user->is_h5==3) {
				$line .= ',H5注册商务';
			} elseif($user->is_h5==4) {
				$line .= ',H5注册专项基金';
			} elseif($user->is_h5==5) {
				$line .= ',H5注册四川全科骨干医生';
			} else {
				$line .= ',';
			}
			$line_pre .= ','. str_ireplace(',','，',$user->user_name);
			$line_pre .= ','. str_ireplace(',','，',$user->user_cwid);
			$line_pre .= ','. str_ireplace(',','，',$user->user_area);
			$line_pre .= ','.str_ireplace(',','，',$user->user_regin);
			$line_pre .= ','.$cluster;
			$line .= ','.$user->created_at;
         	$line .= "\n";
         	$line .= $this->getUserExportMonthInfo($user->id,$iKey+1,1,$line_pre,$aVideo,$iVideoMinId,0,$iCountVideo);
         	$iKey++;
		}
		$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	
	//指定大区月报告
	public function MounthLogRegin($sMounth,$iRegin){
		set_time_limit(0);
		$oUser = User::select('id','user_regin','user_area','user_name','invite_code','user_cwid','user_province','user_city')
			->where('role_id',2)
			->where('user_regin',$iRegin)
			->get();
		//导出csv
		header( "Cache-Control: public" );
		header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
		header("Content-Disposition:attachment;filename=0".$iRegin."-".$sMounth."-".date('Ymdhis').".csv");
		header('Content-Type:APPLICATION/OCTET-STREAM');
		ob_start();
		$header = '';
		$aData = $this->getMonthRange($sMounth);
		$aVideo = FaceVideo::where('start_time','<',$aData[1])
			->where('start_time','>',$aData[0])
			->orderBy('start_time','ASC')
			->lists('id');
		$iCountVideo = 0;
		if($aVideo){
			$iCountVideo = count($aVideo);
			$arr = array();
			foreach($aVideo as $k=>$v){
				$arr[$v] = $v;
			}
			$iVideoMinId = array_search(min($arr), $arr);
			// dd($iVideoMinId);
		}else{
			return 'has no log';
		}
		for($i=0;$i<$iCountVideo;$i++){
			if(!isset($aVideo[$i])){
				$aVideo[$i] = 0;
			}
		}
		for($i=1;$i<=$iCountVideo;$i++){
			$header .= "本月第".$i."期上线方式,";
		}
		$header .= "身份,用户姓名,用户编码,";
		for($i=1;$i<=$iCountVideo;$i++){
			$header .= "本月第".$i."期直播时长,本月第".$i."期录播时长,";
		}
		$header .= "既往直播时长,既往录播时长,用户层级,上级用户姓名,上级用户编码,最终代表,代表CWID,地区,大区,cluster,省份,城市,区域,医院,科室"."\n";
		$header_str =  iconv("utf-8",'gbk',$header);
		$line="";
		$iKey = 0;
		$aReginData = DB::table('regin')->lists('regin_name','id');
		$aAreaData = DB::table('area')->lists('area_name','id');
		foreach($oUser as $k=> $user){
			$line_pre = '';
			$sR = $user->user_regin;
			$sA = $user->user_area;
			$cluster = User::getClusterByReginId2($sR);
			if(is_numeric($sR)){
				// $sRegin = User::getReginCache();
				// $oRegin = json_decode($sRegin);
				// $user->user_regin = $oRegin->$sR;
				$user->user_regin = isset($aReginData[$sR])?$aReginData[$sR]:$sR;
			}
			if(is_numeric($sA)){
				// $sArea = User::getAreaCache();
				// $oArea = json_decode($sArea);
				// if(isset($oArea->$sA))
				// $user->user_area = $oArea->$sR->$sA;
				$user->user_area = isset($aAreaData[$sA])?$aAreaData[$sA]:$sA;
			}
			$iMin1 = $iMin2 = $sScoreWay = array();
			foreach($aVideo as $k=>$v){
				$aMin = $this->getUserVideoMin($user->id, $v);
				$iMin1[$k] = $aMin[0];//本月第1期录播
				$iMin2[$k] = $aMin[1];//本月第1期直播
				//直播上线方式
				$sScoreWay[$k] = $this->getScoreWay($aMin[1],$v,$user->id);
			}
			//既往录播时长
			$iOldMin1 = $this->getOldMin($user->id,$iVideoMinId,1);
			//既往直播时长
			$iOldMin2 = $this->getOldMin($user->id,$iVideoMinId,2);
			$user = $this->formatUserInfo($user);
			$line .= $sScoreWay[0];
			for($i=1;$i<$iCountVideo;$i++){
				$line .= ','.$sScoreWay[$i];
			}
			$line .= ',代表';
			$line .= ','. str_ireplace(',','，',$user->user_name);
			$line .= ','. str_ireplace(',','，',"\t".$user->invite_code);
			for($i=0;$i<$iCountVideo;$i++){
				$line .= ','.$iMin2[$i];
				$line .= ','.$iMin1[$i];
			}
			$line .= ','.$iOldMin2;
			$line .= ','.$iOldMin1;
			$line .= ',0';
			$line .= ',';
			$line .= ',';
			$line .= ',';
			$line .= ','. str_ireplace(',','，',$user->user_cwid);
			$line .= ','. str_ireplace(',','，',$user->user_area);
			$line .= ','.str_ireplace(',','，',$user->user_regin);
			$line .= ','.$cluster;
			$line .= ','. str_ireplace(',','，',$user->user_province);
			$line .= ','. str_ireplace(',','，',$user->user_city);
			$line .= ',';
			$line .= ',';
			$line .= ',';
			$line_pre .= ','. str_ireplace(',','，',$user->user_name);
			$line_pre .= ','. str_ireplace(',','，',$user->user_cwid);
			$line_pre .= ','. str_ireplace(',','，',$user->user_area);
			$line_pre .= ','.str_ireplace(',','，',$user->user_regin);
			$line_pre .= ','.$cluster;
			$line .= "\n";
			$line .= $this->getUserExportMonthInfo($user->id,$iKey+1,1,$line_pre,$aVideo,$iVideoMinId,0,$iCountVideo);
			$iKey++;
		}
		$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	public function getUserExportMonthInfo($iUid,$str,$count,$line_pre='',$aVideo,$iVideoMinId,$iUserLevel,$iCountVideo){
		$iUserLevel = $iUserLevel+1;
		$oUser = User::select('id','link_user_id','user_name','invite_code','user_province','user_city','user_county','user_company','user_company_name','user_department','is_h5','created_at')
			->where('link_user_id',$iUid)
			->where('is_locked','N')
			->get();
		if(!count($oUser)){ return ''; }
		$line = '';
		foreach($oUser as $key_user=> $user){
			$iMin1 = $iMin2 = $sScoreWay = array();
		   	foreach($aVideo as $k=>$v){
				$aMin = $this->getUserVideoMin($user->id, $v);
				$iMin1[$k] = $aMin[0];//本月第1期录播
				$iMin2[$k] = $aMin[1];//本月第1期直播
				//直播上线方式
				$sScoreWay[$k] = $this->getScoreWay($aMin[1],$v,$user->id);
		   	}
			//既往录播时长
			$iOldMin1 = $this->getOldMin($user->id,$iVideoMinId,1);
			//既往直播时长
			$iOldMin2 = $this->getOldMin($user->id,$iVideoMinId,2);
			//上级用户
			$oLinkUser = User::select('user_name','invite_code')->where('id',$user->link_user_id)->first();
			if($oLinkUser){
				$oLinkUserName = $oLinkUser->user_name;
				$oLinkUserCode = "\t".$oLinkUser->invite_code;
			}else{
				$oLinkUserName = '';
				$oLinkUserCode = '';
			}
			$user = $this->formatUserInfo($user);
			$line .= $sScoreWay[0];
			for($i=1;$i<$iCountVideo;$i++){
				$line .= ','.$sScoreWay[$i];
			}
			$line .= ',医生';
			$line .= ','. str_ireplace(',','，',$user->user_name);
			$line .= ','. str_ireplace(',','，',"\t".$user->invite_code);
			for($i=0;$i<$iCountVideo;$i++){
				$line .= ','.$iMin2[$i];
				$line .= ','.$iMin1[$i];
			}
			$line .= ','.$iOldMin2;
			$line .= ','.$iOldMin1;
			$line .= ','.$iUserLevel;
			$line .= ','. str_ireplace(',','，',$oLinkUserName);
			$line .= ','. str_ireplace(',','，',$oLinkUserCode);
			$line .= $line_pre;
			$line .= ','. str_ireplace(',','，',$user->user_province);
			$line .= ','. str_ireplace(',','，',$user->user_city);
			$line .= ','. str_ireplace(',','，',$user->user_county);
			$line .= ','. str_ireplace(',','，',$user->user_company);
			$line .= ','. str_ireplace(',','，',$user->user_department);
			// $line .= ','.$user->is_h5;
			if ($user->is_h5==1) {
				$line .= ',H5注册目标';
			} elseif($user->is_h5==2) {
				$line .= ',H5注册非目标';
			} elseif($user->is_h5==3) {
				$line .= ',H5注册商务';
			} elseif($user->is_h5==4) {
				$line .= ',H5注册专项基金';
			} elseif($user->is_h5==5) {
				$line .= ',H5注册四川全科骨干医生';
			} else {
				$line .= ',';
			}
			$line .= ','.$user->created_at;
         	$line .= "\n";
         	$line .= $this->getUserExportMonthInfo($user->id,$str.'.'.($key_user+1),$count+1,$line_pre,$aVideo,$iVideoMinId,$iUserLevel,$iCountVideo);
		}
		return $line;
	}
	
		//视频列表
	public function LiveScore($id)
	{
		//取出所有视频
		$oVideos = FaceLiveScore::findScoreByiVideoId($id);
		//取得所有专家
		$oDocs = User::orderByCreatedAt()->lists('user_name','id');
		return  View::make('admin.facerec.livescorelist')
					->with('oVideos',$oVideos)
					->with('oDocs',$oDocs);
	}
	
	//月报告导出页面
	public function Month(){
		return  View::make('admin.facerec.month');
	}
	
	public function getVideoIdByMonth($sMonth){
		$aData = $this->getMonthRange($sMonth);
    	$aVideo = FaceVideo::where('start_time','<',$aData[1])
			->where('start_time','>',$aData[0])
			->orderBy('start_time','ASC')
			->lists('id');
    	return $aVideo;
	}
	
	public function getClusterRanking($aVideo,$aCluster,$header){
		$aTotalDocCount = array();//总观看医生数
    	$oVideoLog = VideoLog::wherein('video_id',$aVideo)
			->where('watch_minutes','>',0)
			->groupBy('user_id')
			->get();
    	foreach($oVideoLog as $log){
    		//查找当前用户
    		$oUser = User::find($log->user_id);
    		if($oUser&&$oUser->role_id==3){
    			//获取当前医生对应的代表id
				// $aSuperiorIds = unserialize($oUser->superior_ids);
				$iRepId = $oUser->link_rep_id;
				//获得代表对应cluster
				$sCluster = $this->getRepCluster($iRepId);
	   			/*if($oUser->id==4948){
	   				$sSuperiorIds = $oUser->superior_ids; 
	    			if(!$sSuperiorIds){
						return 0;
					}
					$aSuperiorIds = unserialize($sSuperiorIds);
					dd($aSuperiorIds);
					if(count($aSuperiorIds)==1){
						return $aSuperiorIds[0];
					}
					foreach($aSuperiorIds as $k=>$v){
						$oUser = User::find($v);
						if($oUser->role_id==2){
							return $v;
						}
					}
	   			}*/
				if(in_array($sCluster,$aCluster)){
					//找到当前cluster的id
					$iClusterId = array_search($sCluster,$aCluster); 
					if(!isset($aTotalDocCount[$iClusterId])){
						$aTotalDocCount[$iClusterId] = 0;
					}
					$aTotalDocCount[$iClusterId] = $aTotalDocCount[$iClusterId]+1;
				}else{
					$aCluster[] = $sCluster;
					$iClusterId = array_search($sCluster,$aCluster);
					$aTotalDocCount[$iClusterId] = 1;
				}
    		}
    	}
    	$oVideoLog = VideoLog::wherein('video_id',$aVideo)
			->where('video_type','2')
			->where('watch_minutes','>',0)
			->groupBy('user_id')
			->get();
		$aTotalLiveRepCount = array();//直播上线代表数
    	foreach($oVideoLog as $log){
    		//查找当前用户
    		$oUser = User::find($log->user_id);
    		if($oUser&&$oUser->role_id==2){
				$iRepId = $oUser->id;
				//获得代表对应cluster
				$sCluster = $this->getRepCluster($iRepId);
				if(in_array($sCluster,$aCluster)){
					//找到当前cluster的id
					$iClusterId = array_search($sCluster,$aCluster); 
					if(!isset($aTotalLiveRepCount[$iClusterId])){
						$aTotalLiveRepCount[$iClusterId] = 0;
					}
					$aTotalLiveRepCount[$iClusterId] = $aTotalLiveRepCount[$iClusterId]+1;
				}else{
					$aCluster[] = $sCluster;
					$iClusterId = array_search($sCluster,$aCluster);
					$aTotalLiveRepCount[$iClusterId] = 1;
				}
    		}
    	}
    	//直播上线医生数
		$oVideoLog = VideoLog::wherein('video_id',$aVideo)
			->where('video_type','2')
			->where('watch_minutes','>',0)
			->groupBy('user_id')
			->get();
		$aTotalLiveDocCount = array();//直播上线医生数
    	foreach($oVideoLog as $log){
    		//查找当前用户
    		$oUser = User::find($log->user_id);
    		if($oUser&&$oUser->role_id==3){
				// $aSuperiorIds = unserialize($oUser->superior_ids);
				$iRepId = $oUser->link_rep_id;
				//获得代表对应cluster
				$sCluster = $this->getRepCluster($iRepId);
				if(in_array($sCluster,$aCluster)){
					//找到当前cluster的id
					$iClusterId = array_search($sCluster,$aCluster); 
					if(!isset($aTotalLiveDocCount[$iClusterId])){
						$aTotalLiveDocCount[$iClusterId] = 0;
					}
					$aTotalLiveDocCount[$iClusterId] = $aTotalLiveDocCount[$iClusterId]+1;
				}else{
					$aCluster[] = $sCluster;
					$iClusterId = array_search($sCluster,$aCluster);
					$aTotalLiveDocCount[$iClusterId] = 1;
				}
    		}
    	}
    	//直播扫码医生数
		$oVideoLog = VideoLog::wherein('video_id',$aVideo)
			->where('video_type','2')
			->where('watch_minutes','>',0)
			->groupBy('user_id')
			->get();
		$aTotalLiveCodeDocCount = array();//直播扫码医生数
    	foreach($oVideoLog as $log){
    		//查找当前用户
    		$iUId = $log->user_id;
    		$oSignInLog = SignInLog::where('user_id',$iUId)
				->wherein('video_id',$aVideo)
				->where('video_type','2')
				->where('score','>',0)
				->get();
    		if(count($oSignInLog)){
	    		$oUser = User::find($iUId);
	    		if($oUser&&$oUser->role_id==3){
					// $aSuperiorIds = unserialize($oUser->superior_ids);
					$iRepId = $oUser->link_rep_id;
					//获得代表对应cluster
					$sCluster = $this->getRepCluster($iRepId);
					if(in_array($sCluster,$aCluster)){
						//找到当前cluster的id
						$iClusterId = array_search($sCluster,$aCluster); 
						if(!isset($aTotalLiveCodeDocCount[$iClusterId])){
							$aTotalLiveCodeDocCount[$iClusterId] = 0;
						}
						$aTotalLiveCodeDocCount[$iClusterId] = $aTotalLiveCodeDocCount[$iClusterId]+1;
					}else{
						$aCluster[] = $sCluster;
						$iClusterId = array_search($sCluster,$aCluster);
						$aTotalLiveCodeDocCount[$iClusterId] = 1;
					}
	    		}
    		}
    	}
		$oVideoLog = VideoLog::wherein('video_id',$aVideo)
			->where('video_type','1')
			->where('watch_minutes','>',0)
			->groupBy('user_id')
			->get();
		$aTotalReviewRepCount = array();//录播上线代表数
    	foreach($oVideoLog as $log){
    		//查找当前用户
    		$oUser = User::find($log->user_id);
    		if($oUser&&$oUser->role_id==2){
				//获取当前医生对应的代表id
				$iRepId = $oUser->id;
				//获得代表对应cluster
				$sCluster = $this->getRepCluster($iRepId);
				if(in_array($sCluster,$aCluster)){
					//找到当前cluster的id
					$iClusterId = array_search($sCluster,$aCluster); 
					if(!isset($aTotalReviewRepCount[$iClusterId])){
						$aTotalReviewRepCount[$iClusterId] = 0;
					}
					$aTotalReviewRepCount[$iClusterId] = $aTotalReviewRepCount[$iClusterId]+1;
				}else{
					$aCluster[] = $sCluster;
					$iClusterId = array_search($sCluster,$aCluster);
					$aTotalReviewRepCount[$iClusterId] = 1;
				}
    		}
    	}
    	//录播上线医生数
		$oVideoLog = VideoLog::wherein('video_id',$aVideo)
			->where('video_type','1')
			->where('watch_minutes','>',0)
			->groupBy('user_id')
			->get();
		$aTotalReviewDocCount = array();//录播上线医生数
    	foreach($oVideoLog as $log){
    		//查找当前用户
    		$oUser = User::find($log->user_id);
    		if($oUser&&$oUser->role_id==3){
				// $aSuperiorIds = unserialize($oUser->superior_ids);
				$iRepId = $oUser->link_rep_id;
				//获得代表对应cluster
				$sCluster = $this->getRepCluster($iRepId);
				if(in_array($sCluster,$aCluster)){
					//找到当前cluster的id
					$iClusterId = array_search($sCluster,$aCluster); 
					if(!isset($aTotalReviewDocCount[$iClusterId])){
						$aTotalReviewDocCount[$iClusterId] = 0;
					}
					$aTotalReviewDocCount[$iClusterId] = $aTotalReviewDocCount[$iClusterId]+1;
				}else{
					$aCluster[] = $sCluster;
					$iClusterId = array_search($sCluster,$aCluster);
					$aTotalReviewDocCount[$iClusterId] = 1;
				}
    		}
    	}
    	
    	//录播扫码医生数
		$oVideoLog = VideoLog::wherein('video_id',$aVideo)
			->where('video_type','1')
			->where('watch_minutes','>',0)
			->groupBy('user_id')
			->get();
		$aTotalReviewCodeDocCount = array();//录播扫码医生数
    	foreach($oVideoLog as $log){
    		//查找当前用户
    		$iUId = $log->user_id;
    		$oSignInLog = SignInLog::where('user_id',$iUId)
				->wherein('video_id',$aVideo)
				->where('video_type','1')
				->where('score','>',0)
				->get();
    		if(count($oSignInLog)){
	    		$oUser = User::find($iUId);
	    		if($oUser&&$oUser->role_id==3){
					// $aSuperiorIds = unserialize($oUser->superior_ids);
					$iRepId = $oUser->link_rep_id;
					//获得代表对应cluster
					$sCluster = $this->getRepCluster($iRepId);
					if(in_array($sCluster,$aCluster)){
						//找到当前cluster的id
						$iClusterId = array_search($sCluster,$aCluster); 
						if(!isset($aTotalReviewCodeDocCount[$iClusterId])){
							$aTotalReviewCodeDocCount[$iClusterId] = 0;
						}
						$aTotalReviewCodeDocCount[$iClusterId] = $aTotalReviewCodeDocCount[$iClusterId]+1;
					}else{
						$aCluster[] = $sCluster;
						$iClusterId = array_search($sCluster,$aCluster);
						$aTotalReviewCodeDocCount[$iClusterId] = 1;
					}
	    		}
    		}
    	}
		// $aTotalDocCount = array();//总观看医生数
		// $aTotalLiveRepCount = array();//直播上线代表数
		// $aTotalLiveDocCount = array();//直播上线医生数
		// $aTotalLiveCodeDocCount = array();//直播扫码医生数
		// $aTotalReviewRepCount = array();//录播上线代表数
		// $aTotalReviewDocCount = array();//录播上线医生数
		// $aTotalReviewCodeDocCount = array();//录播扫码医生数
		//导出csv
		header( "Cache-Control: public" );
    	header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
    	header("Content-Disposition:attachment;filename=".date('Ymdhis').".csv");
    	header('Content-Type:APPLICATION/OCTET-STREAM');
    	ob_start();
		$line="";
		foreach($aCluster as $k=>$v){
			$line .= $v;
			if(isset($aTotalDocCount[$k])){
				$line .= ','.$aTotalDocCount[$k];
			}else{
				$line .= ',0';
			}
			if(isset($aTotalLiveRepCount[$k])){
				$line .= ','.$aTotalLiveRepCount[$k];
			}else{
				$line .= ',0';
			}
			if(isset($aTotalLiveDocCount[$k])){
				$line .= ','.$aTotalLiveDocCount[$k];
			}else{
				$line .= ',0';
			}
			if(isset($aTotalLiveCodeDocCount[$k])){
				$line .= ','.$aTotalLiveCodeDocCount[$k];
			}else{
				$line .= ',0';
			}
			if(isset($aTotalReviewRepCount[$k])){
				$line .= ','.$aTotalReviewRepCount[$k];
			}else{
				$line .= ',0';
			}
			if(isset($aTotalReviewDocCount[$k])){
				$line .= ','.$aTotalReviewDocCount[$k];
			}else{
				$line .= ',0';
			}
			if(isset($aTotalReviewCodeDocCount[$k])){
				$line .= ','.$aTotalReviewCodeDocCount[$k];
			}else{
				$line .= ',0';
			}
			$line .= "\n";
		}
		$header_str =  iconv("utf-8",'gbk',$header);
		$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	public function getReginDocs($aVideo,$header){
		$aTotalDocCount = array();//总观看医生数
    	$oVideoLog = VideoLog::wherein('video_id',$aVideo)
			->where('watch_minutes','>',0)
			->groupBy('user_id')
			->get();
    	foreach($oVideoLog as $log){
    		//查找当前用户
    		$oUser = User::find($log->user_id);
    		if($oUser&&$oUser->role_id==3){
    			//获取当前医生对应的代表id
    			$iRepId = $oUser->link_rep_id;
				//获得代表对应cluster
				$iRepRegin = User::where('id',$iRepId)->pluck('user_regin');
				if(!isset($aTotalDocCount[$iRepRegin])){
					$aTotalDocCount[$iRepRegin] = 0;
				}
				$aTotalDocCount[$iRepRegin] = $aTotalDocCount[$iRepRegin]+1;
    		}
    	}
    	arsort($aTotalDocCount);
    	$line = '';
    	foreach($aTotalDocCount as $k=>$v){
    		$regin = '未格式化,'.$k;
    		if(is_numeric($k)){
    			$regin = DB::table('regin')->where('id',$k)->pluck('regin_name');
    			$regin = ','.$regin;
    		}
    		if($regin==','){
    			$regin = '未格式化,'.$k;
    		}
    		$line .= $regin;
    		$line .= ','.$v;
    		$line .= "\n";
    	}
    	header( "Cache-Control: public" );
    	header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
    	header("Content-Disposition:attachment;filename=".date('Ymdhis').".csv");
    	header('Content-Type:APPLICATION/OCTET-STREAM');
    	ob_start();
    	$header_str =  iconv("utf-8",'gbk',$header);
    	$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	public function getAreaDocs($aVideo,$header){
		$aTotalDocCount = array();//总观看医生数
    	$oVideoLog = VideoLog::wherein('video_id',$aVideo)
			->where('watch_minutes','>',0)
			->groupBy('user_id')
			->get();
    	foreach($oVideoLog as $log){
    		//查找当前用户
    		$oUser = User::find($log->user_id);
    		if($oUser&&$oUser->role_id==3){
    			//获取当前医生对应的代表id
				// $aSuperiorIds = unserialize($oUser->superior_ids);
				$iRepId = $oUser->link_rep_id;
				//获得代表对应cluster
				$iRepArea = User::where('id',$iRepId)->pluck('user_area');
				if(!isset($aTotalDocCount[$iRepArea])){
					$aTotalDocCount[$iRepArea] = 0;
				}
				$aTotalDocCount[$iRepArea] = $aTotalDocCount[$iRepArea]+1;
    		}
    	}
    	arsort($aTotalDocCount);
    	$line = '';
    	foreach($aTotalDocCount as $k=>$v){
    		$area = '未格式化,'.$k;
    		if(is_numeric($k)){
    			$area = DB::table('area')->where('id',$k)->pluck('area_name');
    			$area = ','.$area;
    		}
    		if($area==','){
    			$area = '未格式化,'.$k;
    		}
    		$line .= $area;
    		$line .= ','.$v;
    		$line .= "\n";
    	}
    	header( "Cache-Control: public" );
    	header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
    	header("Content-Disposition:attachment;filename=".date('Ymdhis').".csv");
    	header('Content-Type:APPLICATION/OCTET-STREAM');
    	ob_start();
    	$header_str =  iconv("utf-8",'gbk',$header);
    	$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	public function getRepDocs($aVideo,$header){
		$aTotalDocCount = array();//总观看医生数
    	$oVideoLog = VideoLog::wherein('video_id',$aVideo)
			->where('watch_minutes','>',0)
			->groupBy('user_id')
			->get();
    	foreach($oVideoLog as $log){
    		//查找当前用户
    		$oUser = User::find($log->user_id);
    		if($oUser&&$oUser->role_id==3){
    			//获取当前医生对应的代表id
    			$iRepId = $oUser->link_rep_id;
				if(!isset($aTotalDocCount[$iRepId])){
					$aTotalDocCount[$iRepId] = 0;
				}
				$aTotalDocCount[$iRepId] = $aTotalDocCount[$iRepId]+1;
    		}
    	}
    	arsort($aTotalDocCount);
    	$line = '';
    	foreach($aTotalDocCount as $k=>$v){
    		$oRepUser = User::find($k);
    		if($oRepUser){
	    		$sRegin = DB::table('regin')->where('id',$oRepUser->user_regin)->pluck('regin_name');
	    		if(!$sRegin) $sRegin = $oRepUser->user_regin;
	    		$sArea = DB::table('area')->where('id',$oRepUser->user_area)->pluck('area_name');
	    		if(!$sArea) $sArea = $oRepUser->user_area;
	    		$line .= $oRepUser->user_name;
	    		$line .= ','.$sRegin;
	    		$line .= ','.$sArea;
	    		$line .= ','.$oRepUser->user_cwid;
	    		$line .= ','."\t".$oRepUser->invite_code;
	    		$line .= ','.$v;
	    		$line .= "\n";
    		}
    	}
    	header( "Cache-Control: public" );
    	header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
    	header("Content-Disposition:attachment;filename=".date('Ymdhis').".csv");
    	header('Content-Type:APPLICATION/OCTET-STREAM');
    	ob_start();
    	$header_str =  iconv("utf-8",'gbk',$header);
    	$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	public function getBigdocDocs($aVideo,$header){
		$aTotalDocCount = array();//总观看医生数
    	$oSignInLog = SignInLog::wherein('video_id',$aVideo)
			->where('organizer_id','>',0)
				// ->where('score','>',0)
			->groupBy('user_id')
			->get();
		$key = 0;
    	foreach($oSignInLog as $log){
    		//查找当前用户
    		$oUser = User::find($log->user_id);
    		if($oUser&&$oUser->role_id==3){
    			//获取当前医生对应的组织者id
    			$iOrganizerId = $log->organizer_id;
    			$oOrganizer = User::find($iOrganizerId);
    			if($oOrganizer->role_id==3){
    				if(!isset($aTotalDocCount[$iOrganizerId])){
						$aTotalDocCount[$iOrganizerId] = 0;
					}
					$key++;
					$aTotalDocCount[$iOrganizerId] = $aTotalDocCount[$iOrganizerId]+1;
    			}
    		}
    	}
    	arsort($aTotalDocCount);
    	$line = '';
		foreach($aTotalDocCount as $k=>$v){
    		$oBigDocUser = User::find($k);
    		if($oBigDocUser){
    			$iRepId = $oBigDocUser->link_rep_id;
    			if($iRepId){
    				$oRepUser  = User::find($iRepId);
		    		$sRegin = DB::table('regin')->where('id',$oRepUser->user_regin)->pluck('regin_name');
		    		if(!$sRegin) $sRegin = $oRepUser->user_regin;
		    		$sArea = DB::table('area')->where('id',$oRepUser->user_area)->pluck('area_name');
		    		if(!$sArea) $sArea = $oRepUser->user_area;
		    		$sCluster = User::getClusterByReginId($oRepUser->user_regin);
		    		$line .= $oBigDocUser->user_name;
		    		$line .= ','.$v;
		    		$line .= ','.$oRepUser->user_name;
		    		$line .= ','.$sRegin;
		    		$line .= ','.$sArea;
		    		$line .= ','.$sCluster;
		    		$line .= "\n";
    			}
    		}
    	}
    	header( "Cache-Control: public" );
    	header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
    	header("Content-Disposition:attachment;filename=".date('Ymdhis').".csv");
    	header('Content-Type:APPLICATION/OCTET-STREAM');
    	ob_start();
    	$header_str =  iconv("utf-8",'gbk',$header);
    	$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	//cluster排名导出
	public function Cluster($sMonth){
		set_time_limit(0);
		$aVideo = $this->getVideoIdByMonth($sMonth);
    	$aCluster = User::getAllCluster();
    	$header = '';
		$header .= "各Cluster排名,本月总观看医生数,直播上线代表数,直播上线医生数,直播扫码医生数,录播上线代表数,录播上线医生数,录播扫码医生数"."\n";
		$this->getClusterRanking($aVideo,$aCluster,$header);
	}
	
	public function getRepCluster($iRepId){
		$oUser = User::find($iRepId);
		if(isset($oUser->user_regin)){
			$sCluster = User::getClusterByReginId($oUser->user_regin);
		}else{
			$sCluster = 'null';
		}
		return $sCluster;
	}
	
	//月大区总观看医生数
	public function ReginDocs($sMonth){
		set_time_limit(0);
		$aVideo = $this->getVideoIdByMonth($sMonth);
		$header = '';
		$header .= "是否格式化,大区,本月总观看医生数"."\n";
		$this->getReginDocs($aVideo,$header);
	}
	
	//月地区总观看医生数
	public function AreaDocs($sMonth){
		set_time_limit(0);
		$aVideo = $this->getVideoIdByMonth($sMonth);
		$header = '';
		$header .= "是否格式化,地区,本月总观看医生数"."\n";
		$this->getAreaDocs($aVideo,$header);
	}
	
	//月代表总观看医生数
	public function RepDocs($sMonth){
		set_time_limit(0);
		$aVideo = $this->getVideoIdByMonth($sMonth);
		$header = '';
		$header .= "代表姓名,代表大区,代表地区,代表CWID,代表用户编码,本月总观看医生数"."\n";
		$this->getRepDocs($aVideo,$header);
	}
	
	//月大医生组织观看医生数
	public function BigdocDocs($sMonth){
		set_time_limit(0);
		$aVideo = $this->getVideoIdByMonth($sMonth);
		$header = '';
		$header .= "大医生姓名,下级观看医生数,对应代表,对应地区,对应大区,对应Cluster"."\n";
		$this->getBigdocDocs($aVideo,$header);
	}
	
	//周报告导出页面
	public function WeekLog($iId){
		$sStartTime = FaceVideo::where('id',$iId)->pluck('start_time');
    	$aData = $this->getWeekRange($sStartTime);
    	$oVideo = FaceVideo::where('start_time','<',$aData[1])
			->where('start_time','>',$aData[0])
			->orderBy('start_time','ASC')
			->get();
		$oUser = User::select('id')
			->where('role_id',2)
			->where('is_locked','N')
			->get();
    	return  View::make('admin.facerec.week')
			->with('oVideo',$oVideo)
			->with('oUserNum',ceil(count($oUser)/250))
			->with('id',$iId);
	}
	
	public function getWeekVideoIds($iId){
		$sStartTime = FaceVideo::where('id',$iId)->pluck('start_time');
    	$aData = $this->getWeekRange($sStartTime);
    	$aVideo = FaceVideo::where('start_time','<',$aData[1])
			->where('start_time','>',$aData[0])
			->orderBy('start_time','ASC')
			->lists('id');
    	return $aVideo;
	}
	
	//cluster排名导出---周报告
	public function WeekCluster($iId){
		set_time_limit(0);
		$aVideo = $this->getWeekVideoIds($iId);
    	$aCluster = User::getAllCluster();
    	$header = '';
		$header .= "各Cluster排名,本月总观看医生数,直播上线代表数,直播上线医生数,直播扫码医生数,录播上线代表数,录播上线医生数,录播扫码医生数"."\n";
		$this->getClusterRanking($aVideo,$aCluster,$header);
	}
	
	//大区总观看医生数---周报告
	public function WeekReginDocs($iId){
		set_time_limit(0);
		$aVideo = $this->getWeekVideoIds($iId);
		$header = '';
		$header .= "是否格式化,大区,本周总观看医生数"."\n";
		$this->getReginDocs($aVideo,$header);
	}

	//地区总观看医生数---周报告
	public function WeekAreaDocs($iId){
		set_time_limit(0);
		$aVideo = $this->getWeekVideoIds($iId);
		$header = '';
		$header .= "是否格式化,地区,本周总观看医生数"."\n";
		$this->getAreaDocs($aVideo,$header);
	}
	
	//代表总观看医生数---周报告
	public function WeekRepDocs($iId){
		set_time_limit(0);
		$aVideo = $this->getWeekVideoIds($iId);
		$header = '';
		$header .= "代表姓名,代表大区,代表地区,代表CWID,代表用户编码,本周总观看医生数"."\n";
		$this->getRepDocs($aVideo,$header);
	}
	
	//大医生组织观看医生数---周报告
	public function WeekBigdocDocs($iId){
		set_time_limit(0);
		$aVideo = $this->getWeekVideoIds($iId);
		$header = '';
		$header .= "大医生姓名,下级观看医生数,对应代表,对应地区,对应大区,对应Cluster"."\n";
		$this->getBigdocDocs($aVideo,$header);
	}
	
	//视频观看log
	public function OneVideoLog($iId){
		$oVideoLog = VideoLog::where('video_id',$iId)->get()->toArray();
		dd($oVideoLog);
	}
	
	//导出录播观看情况页面
	public function ReivewLog(){
		return  View::make('admin.facerec.log.reviewlog');
	}
	
	public function ExportReivewLog(){
		set_time_limit(0);
		//取出该时间内的所有记录
		$sStartTime = Input::get('starttime');
		$sEndTime = Input::get('endtime');
		$aUserId= VideoReviewLog::where('created_at','>=',$sStartTime)
			->where('created_at','<=',$sEndTime)
			->groupBy('user_id')
			->lists('user_id');
		$header = "用户编码,观看录播次数"."\n";
		$line = '';
		foreach($aUserId as $k=>$v){
			$oUser = User::find($v);
			if($oUser&&$oUser->invite_code){
				$iCount = VideoReviewLog::where('created_at','>=',$sStartTime)
					->where('created_at','<=',$sEndTime)
					->where('user_id',$v)
					->count();
				$line .= "\t".$oUser->invite_code;
			    $line .= ','.$iCount;
			    $line .= "\n";
			}
			
		}
		header( "Cache-Control: public" );
    	header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
    	header("Content-Disposition:attachment;filename=".date('Ymdhis').".csv");
    	header('Content-Type:APPLICATION/OCTET-STREAM');
    	ob_start();
    	$header_str =  iconv("utf-8",'gbk',$header);
    	$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;						
	}
	
	//更新签到积分
	public function UpdateSignIn($iVideoId){
		set_time_limit(0);
		$dir = app_path().'/storage/logs/redislog';
		$content = file_get_contents($dir.'/'.$iVideoId.'.txt'); 
		$aData = explode(';',substr($content,0,-1)); 
		foreach($aData as $k=>$v){
			$sD = $v;
			list($iUId,$iOrgaId,$timestem) = explode(',',$sD);
			//加直播的积分
			$iVideoType = '2';
			//查看用户是否签到
			$oSign = SignInLog::where('user_id',$iUId)
				->where('video_id',$iVideoId)
				->where('video_type',$iVideoType)
				->first(); 
			if(!$oSign){
				//直播签到加积分操作
				$this->SignInAddScore($iUId,$iVideoId,$iOrgaId,$timestem );
			}
		}
		return 'success';
	}
	
	public function SignInAddScore($iUid,$iVideoId,$iUOrgaId,$timestem){
		//签到记录
		$aSign = array(
			'user_id'		=> $iUid,
			'video_id'		=> $iVideoId,
			'organizer_id'	=> $iUOrgaId,
			'created_at'	=> date('Y-m-d H:i:s',$timestem)
		);
		$oSign = new SignInLog($aSign);
		$oSign->save();
		$iVideoType = '2';
		$iAddMin = LIVE_LIMIT;
		$oVideoLog = VideoLog::HasLogSign(1,$iVideoId,$iVideoType,$iUid,0);
		if(!$oVideoLog){
			//加积分记录
			$creditIN = CreditController::getInstance($iVideoId,$iAddMin,$iUid,'signin');
			$creditIN->optSignInCreditAdmin($iVideoType,$iAddMin,$iUid);
			$oSign->score = $iAddMin;
			$oSign->save();
		}
	}
	
	//导出当前视频总的点击次数
	public function ExportVideoCount(){
		set_time_limit(0);
		$oVideoLog = VideoLog::where('link_user_id','0')->where('watch_minutes','>',0)->get();
		$aRegin = DB::table('regin')->lists('regin_name','id');
		$aReginCount1 = array();
		$aReginCount2 = array();
		$iTotalCount1 = 0;		//录播总的点击量
		$iTotalCount2 = 0;		//直播总的点击量
		foreach($oVideoLog as $k=>$v){
			//查看当前用户所属大区id
			$iReginId = $this->getUserReginByUserId($v->user_id);
			if($v->video_type=='1'){
				$iTotalCount1++;
				if($iReginId){
					if(isset($aReginCount1[$iReginId])){
						$aReginCount1[$iReginId]++;
					}else{
						$aReginCount1[$iReginId] = 1;
					}
				}
			}else{
				$iTotalCount2++;
				if($iReginId){
					if(isset($aReginCount2[$iReginId])){
						$aReginCount2[$iReginId]++;
					}else{
						$aReginCount2[$iReginId] = 1;
					}
				}
			}
		}
		$header = "大区名称,录播次数,直播次数"."\n";
		$line = '';
		$line .= '总数,';
		$line .= $iTotalCount1.',';
		$line .= $iTotalCount2."\n";
		foreach($aRegin as $k=>$v){
			$line .= $v.',';
			$line .= isset($aReginCount1[$k])?$aReginCount1[$k]:0;
			$line .= ',';
			$line .= isset($aReginCount2[$k])?$aReginCount2[$k]:0;
			$line .= "\n";
		}
		header( "Cache-Control: public" );
    	header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
    	header("Content-Disposition:attachment;filename=".date('Ymdhis').".csv");
    	header('Content-Type:APPLICATION/OCTET-STREAM');
    	ob_start();
    	$header_str =  iconv("utf-8",'gbk',$header);
    	$file_str=  iconv("utf-8",'GBK//TRANSLIT',$line);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
	public function getUserReginByUserId($iUserId){
		$iReginId = 0;
		$oUser = User::select('id','role_id','link_rep_id','user_regin','superior_ids')->where('id',$iUserId)->first();
		if($oUser&&$oUser->role_id==2){
			//代表，直接返回
			$sRegin = $oUser->user_regin;
			if(is_numeric($sRegin)){
				$iReginId = intval($sRegin);
			}
		}else if($oUser&&$oUser->role_id==3){
			//医生
			//当前医生对应的代表
			$iDaibiaoId = $oUser->link_rep_id;
			if($iDaibiaoId){
				$sRegin = User::where('id',$iDaibiaoId)->pluck('user_regin');
				if(is_numeric($sRegin)){
					$iReginId = intval($sRegin);
				}
			}
			
		}
		return $iReginId;
	}

	//统计备份首页
	public function Tongji(){
		return  View::make('admin.facerec.tongji');
	}

	//导出指定时间之内的录播视频观看详细记录
	// public function ExportReviewDetailLog(){}
		
    //导出
    public function ReviewExport($iId)
    {
		$oVideoReviewLog = VideoReviewLog::where('video_id',$iId)->join('user','video_review_log.user_id','=','user.id')->get();
		$sVideoWatchMessage ="用户名,用户昵称,用户性别,用户手机号,用户邮箱,用户地址,用户省份,用户城市,县/区,科室,职称,公司,医院名称,录播视频id,观看日期,观看时长,观看类型"."\n";
		foreach($oVideoReviewLog as $value){
			$sVideoWatchMessage.=$value-> user_name.",";
			$sVideoWatchMessage.=$value-> user_nick.",";
			$sVideoWatchMessage.=$value-> user_sex.",";
			$sVideoWatchMessage.=$value-> user_tel.",";
			$sVideoWatchMessage.=$value-> user_email.",";
			$sVideoWatchMessage.=$value-> user_address.",";
			$sVideoWatchMessage.=$value-> user_province.",";
			$sVideoWatchMessage.=$value-> user_city.",";
			$sVideoWatchMessage.=$value-> user_county.",";
			$sVideoWatchMessage.=$value-> user_department.",";
			$sVideoWatchMessage.=$value-> user_position.",";
			$sVideoWatchMessage.=$value-> user_company.",";
			$sVideoWatchMessage.=$value-> user_company_name.",";
			$sVideoWatchMessage.=$value-> id.",";
			$sVideoWatchMessage.=date("Y-m-d",strtotime($value-> created_at)).",";
			$sVideoWatchMessage.=$value-> watch_minutes.",";
			if($value-> type == 0){
				$sVideoWatchMessage.="自行观看".",";
			}else{
				$sVideoWatchMessage.="扫码观看".",";
			}
			$sVideoWatchMessage.="\n";
		}
		$sVideoWatchMessage = strval($sVideoWatchMessage);
		$sVideoWatchMessage = iconv("UTF-8",'GB2312//IGNORE',$sVideoWatchMessage);
		header("Content-type:text/csv");
		header("Content-Type: application/force-download");
		header("Content-Disposition: attachment; filename=".date('Y-m-d').".csv");
		header('Expires:0');
		header('Pragma:public');
		echo $sVideoWatchMessage;
    }
    
    /**
     * 20170102 移动端注册用户直播录播次数统计
     */
    public function H5userLog()
    {
    	$sHeader = "用户id,用户姓名,用户编码,直播自主观看,直播签到,录播自主观看,录播签到"."\n";
    	$oUser = User::select('id','user_name','invite_code')->where('is_h5','>',0)->get();
    	foreach ($oUser as $k=>$v) {
    		if (!isset($v->id) || !isset($v->user_name) || !isset($v->invite_code)) {
    			continue;
    		}
    		$iUserId = $v->id;
    		$iCount1 = VideoLog::where('user_id',$iUserId)
				->where('link_user_id',0)
				->where('video_type','2')
				->where('device_type','!=','signin')
				->count();
    		$iCount2 = VideoLog::where('user_id',$iUserId)
	    		->where('link_user_id',0)
	    		->where('video_type','2')
	    		->where('device_type','signin')
	    		->count();
    		$iCount3 = VideoLog::where('user_id',$iUserId)
	    		->where('link_user_id',0)
	    		->where('video_type','1')
	    		->where('device_type','!=','signin')
	    		->count();
    		$iCount4 = VideoLog::where('user_id',$iUserId)
	    		->where('link_user_id',0)
	    		->where('video_type','1')
	    		->where('device_type','signin')
	    		->count();
    		$sHeader .= $iUserId.",";
    		$sHeader .= $v->user_name.",";
    		$sHeader .= $v->invite_code.",";
    		$sHeader .= $iCount1.",";
    		$sHeader .= $iCount2.",";
    		$sHeader .= $iCount3.",";
    		$sHeader .= $iCount4.",";
    		$sHeader .= "\n";
    	}
		// $sHeader = strval($sHeader);
    	header( "Cache-Control: public" );
    	header( "Pragma: public" );
		header("Content-type:application/vnd.ms-excel");
    	header("Content-Disposition:attachment;filename=".date('Ymdhis').".csv");
    	header('Content-Type:APPLICATION/OCTET-STREAM');
    	ob_start();
    	$sHeader=  iconv("utf-8",'GBK//TRANSLIT',$sHeader);
    	ob_end_clean();
    	echo $sHeader;
    }
    
}