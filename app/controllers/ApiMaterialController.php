<?php

/**
 * API操作类
 * @author dll
 *
 */
use Illuminate\Support\Facades\Input;

class ApiMaterialController extends BaseController {
	
	CONST  PAGESIZE = 10;
	public $iUserId = 0 ;

	public function __construct()
	{
		$this->beforeFilter('apilogin');
		global $iUserId;
		$this->iUserId = &$iUserId;
	}

	/**
	 * 资讯列表页
	 * http://cdma.local/apimaterial/list?page=1&pagesize=10&type=1&token=
	 */
	public function getList(){
		if(!Input::has('type')){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$type = Input::get('type');
		$iPage = Input::get('page',1);
		$iPagesize = Input::get('pagesize',self::PAGESIZE);
		
		$aMaterialNews = array();
		$oMaterialNews = MaterialNews::leftJoin('material','material.id','=','material_news.material_id')
			->select('material_news.*','material.path')
			->where('news_type',$type)
			->orderBy("material_news.created_at","desc")
			->skip(($iPage-1)*$iPagesize)
			->take($iPagesize)
			->get();
		foreach($oMaterialNews as $k=>$v){
			$aMaterialNews[] = array(
				'id'    => $v->id,
				'title' => $v->title,
				'hits'  => $v->hits,
				'thumb' => Config::get('app.url').$v->path,
				'created_at' => substr($v->created_at,0,10),
			);
		}
		echo  $this->formatRet(true,"成功","success",$aMaterialNews);exit;
	}

	/**
	 * 资讯详情页
	 * http://cdma.local/apimaterial/show?id=2&token=
	 */
	public function getShow(){
		if(!Input::has('id')){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$iUid = $this->iUserId;
		$iId = intval(Input::get('id'));
		$oMaterialNews = MaterialNews::find($iId);
		if(!$oMaterialNews){
			echo $this->formatRet(false,"该资讯不存在");exit;
		}
		$type = $oMaterialNews->news_type;
		//浏览量+1
		$oMaterialNews->increment('hits');
		$oUserInformationLog = UserInformationLog::where('type',$type)->where('information_id',$iId)->where('user_id',$iUid)->first();
		if(count($oUserInformationLog)){
			$oUserInformationLog->increment('hits');
		}else{
			$aLog = array(
				'user_id'    => $iUid,
				'information_id'  => $iId,
				'hits'       => 1,
				'type'       => $type,
				'created_at' => date('Y-m-d H:i:s',time())
			);
			$oUserLog = new UserInformationLog($aLog);
			$oUserLog->save();
		}
		//解决微信图片无法显示问题，用本地路径（title）替换微信链接（src），后台上传时已将服务器地址赋值给了title
		$sContent = $oMaterialNews->content;
		$replaceSrc = '';
		$sImgInfo = preg_replace('#<img(.+?)src="([^"]+?)"([^>]*?)>#',"<img$1src=\"$replaceSrc\"$3>",$sContent);
		$sImgRes = str_replace('src=""', '', $sImgInfo);
		$sNewContent = str_replace('title="', 'src="'.Config::get('app.url'), $sImgRes);
		$aMaterialNews = array(
			'title'      => $oMaterialNews->title,
			'created_at' => substr($oMaterialNews->created_at,0,10),
			'content'    => $sNewContent
		);
		echo  $this->formatRet(true,"成功","success",$aMaterialNews);exit;
	}

}