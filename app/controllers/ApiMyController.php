<?php
/**
 * +----------------------------------------------------------------------
 * | 国卫健康云（cdcma）
 * +----------------------------------------------------------------------
 *  .--,       .--,             | FILE: ApiMyController.php
 * ( (  \.---./  ) )            | AUTHOR: byron sampson
 *  '.__/o   o\__.'             | EMAIL: xiaobo.sun@qq.com
 *     {=  ^  =}                | QQ: 150093589
 *      >  -  <                 | WECHAT: wx5ini99
 *     /       \                | DATETIME: 2018/8/20
 *    //       \\               |
 *   //|   .   |\\              |
 *   "'\       /'"_.-~^`'-.     |
 *      \  _  /--'         `    |
 *    ___)( )(___               |-----------------------------------------
 *   (((__) (__)))              | 高山仰止,景行行止.虽不能至,心向往之。
 * +----------------------------------------------------------------------
 * | Copyright (c) 2017 http://www.zzstudio.net All rights reserved.
 * +----------------------------------------------------------------------
 */

use Illuminate\Support\Facades\Input;

class ApiMyController extends BaseController
{
    const NETURL = 'http://cdcma.bizconf.cn';
    const PAGESIZE = 10;
    public $iUserId = 0;

    public function __construct()
    {
        $this->beforeFilter('apilogin');
        global $iUserId;
        $this->iUserId = &$iUserId;
    }

    /**
     * 获取我的医生列表
     * http://cdcma.local.org/apimy/doctors?is_speaker=1&page=1&pagesize=10&name=&mobile=&hospital=&token=
     */
    public function getDoctors()
    {
        $is_speaker = Input::get('is_speaker', 1);
        $iPage = Input::get('page', 1);
        $iPagesize = Input::get('pagesize', self::PAGESIZE);

        $oUser = User::find($this->iUserId);
        if ($oUser->role_id != 2) {
            echo $this->formatRet(false, "对不起，您无权查看。");
            exit;
        }

        $list = User::where(
            function ($query) use ($is_speaker) {
                if ($is_speaker) {
                    $query->where('user.card_auth_flag', 2);
                } else {
                    $query->where('user.card_auth_flag', "!=", 2);
                }
            })
            ->select([
                'user.*',
                'hospital.id as hospital_id',
                'hospital.name as hospital_name'
            ])
            ->leftJoin('hospital', 'user.user_company', '=', 'hospital.id')
            ->where('user.link_rep_id', $this->iUserId)
            ->orderBy("user.health_integral", "desc")
            ->skip(($iPage - 1) * $iPagesize)
            ->take($iPagesize)
            ->get();

        $alist = [];
        foreach ($list as $oUser) {
            //用户是否是副高及以上职称标志
            $iPositionFlag = 0;
            if ($oUser->role_id == 2) {
                $iPositionFlag = 1;
            } elseif ($oUser->role_id == 3) {
                /**
                 * '1' => '住院医师',
                 * '2' => '主治医师',
                 * '3' => '副主任医师',
                 * '4' => '主任医师',
                 * '5' => '其它' */
                $aPos = User::getPosition();
                if ($oUser->user_position == $aPos[2] || $oUser->user_position == $aPos[3] || $oUser->user_position == $aPos[4]) {
                    $iPositionFlag = 2;
                }
            }
            $alist[] = array(
                'user_id' => $oUser->id,
                'role_id' => $oUser->role_id,
                'user_nick' => $oUser->user_nick,
                'user_name' => $oUser->user_name,
                'user_sex' => $oUser->user_sex,
                'user_tel' => $oUser->user_tel,
                'user_email' => $oUser->user_email,
                'user_address' => $oUser->user_address,
                'user_city' => $oUser->user_city,
                'user_department' => $oUser->user_department,
                'user_position' => $oUser->user_position,
                'user_company' => $oUser->user_company,
                'user_thumb' => $oUser->user_thumb ? Config::get('app.url') . $oUser->user_thumb . "?t=" . time() : Config::get('app.url') . '/assets/images/dafault/default.jpg?t=' . time(),
                'card_auth_flag' => $oUser->card_auth_flag,
                'position_flag' => $iPositionFlag,
                'is_speaker' => $oUser->is_speaker,
                'hospital_id' => $oUser->hospital_id,
                'hospital_name' => $oUser->hospital_name ?: $oUser->user_company_name
            );
        }

        echo $this->formatRet(true, "成功", "success", $alist);
        exit;
    }

    /**
     * 加入我的医生列表
     * http://cdcma.local.org/apimy/doctor?user_doctor_id=&token=
     */
    public function postDoctor()
    {
        $user_doctor_id = Input::get('user_doctor_id');
        // 表单验证
        $validator = Validator::make(
            Input::all(),
            array(
                'user_doctor_id' => 'required|numeric'
            ),
            ['user_doctor_id.required' => '医生不能为空']
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $message) {
                echo $this->formatRet(false, $message);
                exit;
            }
        }

        $doctor = User::where('id', $user_doctor_id)->where('role_id', 3)->first();
        // 查看医生是否存在
        if (!$doctor) {
            echo $this->formatRet(false, '该医生不存在');
            exit;
        }

        // 存在的记录非本人的情况下，进行修改所属代表
        if ($doctor->link_rep_id != $this->iUserId) {
            $push_user_id = $doctor->link_rep_id;
            $doctor->link_rep_id = $this->iUserId;
            $doctor->save();
            // 推送消息
            if ($push_user_id) {
                $deputy_name = User::where('id', $this->iUserId)->pluck('user_name');
                if ($deputy_name) {
                    $content = "您名下的医生 {$doctor->user_name} 已被代表 {$deputy_name} 挖走喽~";
                    PushService::postClient('国卫健康云', $content, 51, $push_user_id, 5);
                }
            }
        }

        echo $this->formatRet(true, "成功", "success");
        exit;
    }

    /**
     * 移除我的医生列表
     * http://cdcma.local.org/apimy/remove-doctor?user_doctor_id=&token=
     */
    public function postRemoveDoctor()
    {
        $user_doctor_id = Input::get('user_doctor_id');
        // 表单验证
        $validator = Validator::make(
            Input::all(),
            array(
                'user_doctor_id' => 'required|numeric'
            ),
            ['user_doctor_id.required' => '医生不能为空']
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $message) {
                echo $this->formatRet(false, $message);
                exit;
            }
        }

        // 获取已存在的医生信息
        $doctor = User::where('id', $user_doctor_id)
            ->where('link_rep_id', $this->iUserId)
            ->where('role_id', 3)
            ->first();
        if ($doctor) {
            $doctor->link_rep_id = 0;
            $doctor->save();
        }

        echo $this->formatRet(true, "成功", "success");
        exit;
    }

    /**
     * 提交cme 会议资料
     * [post] /apimy/buycme-info?page=1&pagesize=10&token=&obj_id=&name=&workplace=&dept=&professional=&position=&telephone=&areacode=&postalcode=
     */
    public function postBuycmeInfo()
    {
        // 表单验证
        $validator = Validator::make(
            Input::all(),
            array(
                'obj_id' => 'required|numeric',
                'name' => 'required',
                'workplace' => 'required',
                'dept' => 'required',
                'professional' => 'required',
                'position' => 'required',
                'telephone' => 'required',
                'areacode' => 'required',
                'postalcode' => 'required',
            ),
            [
                'obj_id.required' => '购买标识不能为空',
                'name.required' => '姓名不能为空',
                'workplace.required' => '工作单位不能为空',
                'dept.required' => '科室不能为空',
                'professional.required' => '职称不能为空',
                'position.required' => '职务不能为空',
                'telephone.required' => '联系电话不能为空',
                'areacode.required' => '区号不能为空',
                'postalcode.required' => '邮政编码不能为空',
            ]
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $message) {
                echo $this->formatRet(false, $message);
                exit;
            }
        }

        // 查看记录是否存在
        $record = CmeInfo::where('obj_id', Input::get('obj_id', 0))
            ->where('user_id', $this->iUserId)
            ->count();
        if ($record) {
            echo $this->formatRet(false, '该购买记录的详细数据已存在');
            exit;
        }

        $info = new CmeInfo(Input::all());
        $info->user_id = $this->iUserId;
        $info->save();
        if ($info->id) {
            echo $this->formatRet(true, "保存成功", null, null, ['detail'=>'您的学分申请已提交，预计审核周期为20天', 'link'=>'http://www.cma-cmc.com.cn/cms/']);
            exit;
        }
        echo $this->formatRet(false, "保存失败");
        exit;
    }

    /**
     * 空方法
     * @param array $parameters
     */
    public function missingMethod($parameters = array())
    {
        //
    }
}