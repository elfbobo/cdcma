<?php 
class MobileAspirinFundController extends BaseController
{
	CONST  PAGESIZE = 10;
	public $iUserid;
	
	public function __construct(){
		$this->beforeFilter('mobileauth2');
		$this->beforeFilter('cityauth');
		$this->iUserid = Session::get('userid');
	}
	/**
	 * 专项基金风险筛查——风险筛查首页
	 * http://cdma.local/aspirinfund/screening
	 */
	public function getScreening()
	{
		return View::make("mobile.aspirin.screening.index");
	}
	
	/**
	 * 专项基金风险筛查——筛查填写页二维码
	 * http://cdma.local/aspirinfund/screening-ewm
	 */
	public function getScreeningEwm(){
		//$iUid = intval(Input::get('iUid'));
		$iUid = Session::get('userid');
		//获取二维码地址
		$sUrl = Config::get('app.url').'/aspirinfund/screening-form?iUid='.$iUid;
		$sEwmUrl = AspirinEwm::getErweima($sUrl);
		return View::make('mobile.aspirin.screening.ewm')->with('sEwmUrl',$sEwmUrl);
	}
	/**
	 * 专项基金风险筛查——筛查填写页
	 * http://cdma.local/aspirinfund/screening-form
	 */
	public function getScreeningForm(){
		
		$isWxFlag = user_agent_is_weixin();
		$wx_config = array();
		if ($isWxFlag){
			include_once  app_path().'/include/jssdk.class.php';  //先把类包含进来
			$oJssdk = new jssdk();
			$wx_config = $oJssdk->getSignPackage();
		}
		$iUid = Session::get('userid');
		$oUser = User::find($iUid);
		$iCardAuthFlag = $oUser->card_auth_flag;
		return View::make('mobile.aspirin.screening.form')
												->with('wx_config',$wx_config)
												->with('iCardAuthFlag',$iCardAuthFlag)
												->with('isWxFlag',$isWxFlag)
												->with('iUid',$iUid);
	}
	public function getScreeningForm2(){
		$isWxFlag = user_agent_is_weixin();
		$wx_config = array();
		if ($isWxFlag){
			include_once  app_path().'/include/jssdk.class.php';  //先把类包含进来
			$oJssdk = new jssdk();
			$wx_config = $oJssdk->getSignPackage();
		}
		$iId = Input::get('id');
		$oScreening = AspirinScreening::find($iId);
		$sex = $oScreening->sex;
		$iUid = Session::get('userid');
		$oUser = User::find($iUid);
		$iCardAuthFlag = $oUser->card_auth_flag;
		return View::make('mobile.aspirin.screening.form2')
												->with('wx_config',$wx_config)
												->with('iCardAuthFlag',$iCardAuthFlag)
												->with('isWxFlag',$isWxFlag)
												->with('iUid',$iUid)
												->with('iId',$iId)
												->with('sex',$sex);
	}
	public function getScreeningForm3(){
		$iId = Input::get('id');
		$isWxFlag = user_agent_is_weixin();
		$wx_config = array();
		if ($isWxFlag){
			include_once  app_path().'/include/jssdk.class.php';  //先把类包含进来
			$oJssdk = new jssdk();
			$wx_config = $oJssdk->getSignPackage();
		}
		$iUid = Session::get('userid');
		$oUser = User::find($iUid);
		$iCardAuthFlag = $oUser->card_auth_flag;
		return View::make('mobile.aspirin.screening.form3')
												->with('wx_config',$wx_config)
												->with('iCardAuthFlag',$iCardAuthFlag)
												->with('iUid',$iUid)
												->with('isWxFlag',$isWxFlag)
												->with('iId',$iId);
	}
	public function getScreeningForm4(){
		$iId = Input::get('id');
		$isWxFlag = user_agent_is_weixin();
		$wx_config = array();
		if ($isWxFlag){
			include_once  app_path().'/include/jssdk.class.php';  //先把类包含进来
			$oJssdk = new jssdk();
			$wx_config = $oJssdk->getSignPackage();
		}
		$iUid = Session::get('userid');
		$oUser = User::find($iUid);
		$iCardAuthFlag = $oUser->card_auth_flag;
		return View::make('mobile.aspirin.screening.form4')
												->with('wx_config',$wx_config)
												->with('iUid',$iUid)
												->with('iCardAuthFlag',$iCardAuthFlag)
												->with('isWxFlag',$isWxFlag)
												->with('iId',$iId);
	}
	public function postScreeningSubmit()
	{
		$iUid = Session::get('userid');
		if(Input::get('sex') == '女'){
			$sex = 0;
		}else{
			$sex = 1;
		}
		//根据年月计算真实年龄
		$age = AspirinScreening::getRealAge(Input::get('age'),'年','月');
		//筛查记录保存
		$aScreening = array(
				'user_id'     => $iUid,
				'sex'         => $sex,
				'age'         => $age,
				'weight'      => Input::get('weight'),
				'height'      => Input::get('height'),
				'device_type' => 1,          //分享页
				'created_at'  => date('Y-m-d H:i:s',time())
		);
		$oScreening = new AspirinScreening($aScreening);
		$oScreening->save();
		$iSourceId = $oScreening->id;
		return json_encode(array('success'=>true,'id'=>$iSourceId));
	}
	public function postScreeningSubmit2()
	{
		$iId   = intval(Input::get('iId'));
		
		$oScreening = AspirinScreening::find($iId);
		$oScreening->question1 = Input::get('question1');
		$oScreening->question2 = Input::get('question2');
		$oScreening->question3 = Input::get('question3');
		$oScreening->question31 = Input::get('question31')?intval(Input::get('question31')):0;
		$oScreening->question0 = Input::get('question0')?intval(Input::get('question0')):0;
		$oScreening->save();
				
		return json_encode(array('success'=>true,'id'=>$iId));
	}
	public function postScreeningSubmit3()
	{
		$iId   = intval(Input::get('iId'));
		
		$oScreening = AspirinScreening::find($iId);
		$oScreening->question4 = Input::get('question4');
		$oScreening->question41 = Input::get('question41')?intval(Input::get('question41')):0;
		$oScreening->question42 = Input::get('question42')?intval(Input::get('question42')):0;
		$oScreening->question5 = Input::get('question5');
		$oScreening->question51 = Input::get('question51')?intval(Input::get('question51')):0;
		$oScreening->question52 = Input::get('question52')?intval(Input::get('question52')):0;
		$oScreening->question53 = Input::get('question53')?intval(Input::get('question53')):0;
		$oScreening->question54 = Input::get('question54')?intval(Input::get('question54')):0;
		$oScreening->save();
		
		return json_encode(array('success'=>true,'id'=>$iId));
	}
	public function postScreeningSubmit4()
	{
		$iId   = intval(Input::get('iId'));
		
		$oScreening = AspirinScreening::find($iId);
		$iUid = $oScreening->user_id;
		
		$oScreening->question6 = Input::get('question6');
		$oScreening->question7 = Input::get('question7');
		$oScreening->question8 = Input::get('question8');
		$oScreening->question9 = Input::get('question9');
		$oScreening->question91 = Input::get('question91')?Input::get('question91'):'';
			
		$weight = $oScreening->weight;
		$height = $oScreening->height;
		$sex = $oScreening->sex;
		//BMI值
		$iBmi = $weight/($height*0.01*$height*0.01);
		$iBmi = round($iBmi,2);
			
		/************筛查公式计算得出结果s***************/
		//年龄得分
		$iAgeScore = AspirinScreening::getAgeScore($sex,$oScreening->age);
		//TC得分
		$iTcScore = AspirinScreening::getTcScore($sex,$oScreening->question42);
		//血压得分
		$iBpScore = AspirinScreening::getBpScore($sex,$oScreening->question52,$oScreening->question53);
		//糖尿病得分
		$iDmScore = AspirinScreening::getDmScore($sex,$oScreening->question3);
		//吸烟得分
		$iSmokeScore = AspirinScreening::getSmokeScore($oScreening->question1);
		//总分数
		$iScore = $iAgeScore+$iTcScore+$iBpScore+$iDmScore+$iSmokeScore;
		//评估CHD风险百分比
		$sPercent = AspirinScreening::getPercent($sex,$iScore);
		//存在的心血管危险因素
		$sDanger = '';
		$aDanger = AspirinScreening::getDangerElement($iId);
		if($aDanger){
			$sDanger = implode(',',$aDanger);
		}
		//获取界面呈现内容
		$aContent = AspirinScreening::getInfo($sPercent,$aDanger,$iBmi);
		/************筛查公式计算得出结果e***************/
			
		$oScreening->bmi       = $iBmi;
		$oScreening->score     = $iScore;
		$oScreening->percent   = $sPercent;
		$oScreening->flag      = $aContent[0];
		$oScreening->danger    = $sDanger;
		$oScreening->save();
			
		$oScreeningLog = AspirinScreeningLog::where('user_id',$iUid)->first();
		if(count($oScreeningLog)){
			$oScreeningLog->increment('web_use_count');
		}else{
			$aScreeningLog = array(
					'user_id'        => $iUid,
					'web_use_count'  => 1,
					'created_at'     => date('Y-m-d H:i:s',time())
			);
			$oScreeningLog = new AspirinScreeningLog($aScreeningLog);
			$oScreeningLog->save();
		}
			
		return json_encode(array('success'=>true,'id'=>$iId));
	}
	
	
	/**
	 * 专项基金风险筛查——筛查结果页
	 * http://cdma.local/aspirinfund/screening-result?id=
	 */
	public function getScreeningResult(){
		$iUid = Session::get('userid');
		$iId = Input::get('id');
		$oScreening = AspirinScreening::find($iId);
		$aDanger = array();
		if($oScreening->danger){
			$aDanger = explode(',',$oScreening->danger);
		}
		//获取界面呈现内容
		$aContent = AspirinScreening::getInfo($oScreening->percent,$aDanger,$oScreening->bmi);
		//获取结果页二维码链接地址
		$sUrl = '/aspirinshare/screening-result?id='.$iId;
		$sUrl = Config::get('app.url').$sUrl;
		$sEwmUrl = AspirinEwm::getErweima($sUrl);
		
		$aResult = array(
				'sex'         => $oScreening->sex,
				'age'         => $oScreening->age,
				'height'      => $oScreening->height,
				'weight'      => $oScreening->weight,
				'bmi'         => $oScreening->bmi,
				'flag'        => $oScreening->flag,
				'flag_text'   => $aContent[1],
				'suggestion'  => $aContent[2],
				'danger_element'=> $aDanger,
				'ewm_url'     => Config::get('app.url').$sEwmUrl
		
		);
		$isWxFlag = user_agent_is_weixin();
		$wx_config = array();
		if ($isWxFlag){
			include_once  app_path().'/include/jssdk.class.php';  //先把类包含进来
			$oJssdk = new jssdk();
			$wx_config = $oJssdk->getSignPackage();
		}
		
		return View::make('mobile.aspirin.screening.result')
														->with('wx_config',$wx_config)
														->with('iUid',$iUid)
														->with('iId',$iId)
														->with('isWxFlag',$isWxFlag)
														->with('aResult',$aResult);
	}
	/**
	 * 专项基金风险筛查——筛查记录列表
	 * http://cdma.local/aspirinfund/screening-log
	 */
	public function getScreeningLog(){
		$iUid = $this->iUserid;
		$oScreenings = AspirinScreening::where('user_id',25)->take(self::PAGESIZE)->get();
		return View::make('mobile.aspirin.screeninglog.index')->with('oScreenings',$oScreenings);
	}
	/**
	 * 专项基金风险筛查——筛查记录列表
	 * http://cdma.local/aspirinfund/screening-detail/id
	 */
	public function getScreeningDetail($iId){
		$oScreening = AspirinScreening::find($iId);
		$aDanger = array();
		if($oScreening->danger){
			$aDanger = explode(',',$oScreening->danger);
		}
		//获取界面呈现内容
		$aContent = AspirinScreening::getInfo($oScreening->percent,$aDanger,$oScreening->bmi);
		//获取结果页二维码链接地址
		$sUrl = '/aspirinshare/screening-result?id='.$iId;
		$sUrl = Config::get('app.url').$sUrl;
		$sEwmUrl = AspirinEwm::getErweima($sUrl);
		
		$aResult = array(
				'id'		  =>$oScreening->id,
				'sex'         => $oScreening->sex,
				'age'         => $oScreening->age,
				'height'      => $oScreening->height,
				'weight'      => $oScreening->weight,
				'bmi'         => $oScreening->bmi,
				'flag'        => $oScreening->flag,
				'flag_text'   => $aContent[1],
				'suggestion'  => $aContent[2],
				'danger_element'=> $aDanger,
				'ewm_url'     => Config::get('app.url').$sEwmUrl
		
		);
		
		return View::make('mobile.aspirin.screeninglog.screendetail')->with('aResult',$aResult);
	}
	/**
	 * 专项基金风险筛查——查看用户答题详细
	 * http://cdma.local/aspirinfund/screening-message/id
	 */
	public function getScreeningMessage($iId)
	{
		$oScreening = AspirinScreening::find($iId);
		return View::make('mobile.aspirin.screeninglog.screenmessage')->with('oScreening',$oScreening);
		
	}
	/**
	 * 进入--专项基金--科研申请首页
	 * http://cdma.local/aspirinfund/scientific-research 
	 */
	public function getScientificResearch()
	{
		$iUid = $this->iUserid;
		$oUser = User::find($iUid);
		return View::make("mobile.aspirin.scientificresearch.index")->with('oUser',$oUser);
	}
	/**
	 * 进入--专项基金--科研申请--我的申请列表页
	 * http://cdma.local/aspirinfund/application-researchlist
	 */
	public function getApplicationResearchlist()
	{
		$iUid = $this->iUserid;
		$oUser = User::find($iUid);
		if($oUser->role_id == 3){
			if($oUser->card_auth_flag == 0){
				return Redirect::to('/mobile-aspirin/auth');
			}
		}
		$oAspirinResearchApply = AspirinResearchApply::where('user_id',$iUid)->orderBy('created_at','desc')->paginate(self::PAGESIZE);
		return View::make("mobile.aspirin.scientificresearch.scientificlist")
													->with('iUid',$iUid)
													->with('oAspirinResearchApply',$oAspirinResearchApply);
	}
	/**
	 * 科研申请点击滑屏加载更多
	 * http://cdma.local/aspirinfund/application-researchadd
	 */
	public function postApplicationResearchadd($iPage)
	{
		$iUid = $this->iUserid;
		$iPageSize = self::PAGESIZE;
		$aAspirinResearchApply = array();
		$oAspirinResearchApply = AspirinResearchApply::where('user_id',$iUid)->orderBy('created_at','desc')->skip(($iPage-1)*$iPageSize)->take($iPageSize)->get();
		$ceil = 0;
		$iConut = AspirinResearchApply::where('user_id',$iUid)->count();
    	if(ceil($iConut/$iPageSize) <= $iPage){
    		$ceil = 1;
    	}
		$aAspirinResearchApply['ceil'] = $ceil;
		foreach($oAspirinResearchApply as $k=>$value){
				$aAspirinResearchApply[$k]['applyid'] = $value->id;
				$aAspirinResearchApply[$k]['applyname'] = $value-> apply_name;
				$iApplyType = $value-> apply_type;
				switch($iApplyType){
					case 0:
						$aAspirinResearchApply[$k]['applytype'] = '未审核';
						break;
					case 1:
						$aAspirinResearchApply[$k]['applytype'] = '审核通过';
						break;
				  	default:
				   		$aAspirinResearchApply[$k]['applytype'] = '审核失败';
						break;
				}
				$aAspirinResearchApply[$k]['applydate'] = date('Y-m-d',strtotime($value->created_at));
		}
		echo json_encode($aAspirinResearchApply);
		
	}
	/**
	 * 进入--专项基金--科研申请--科研申请详细页
	 * http://cdma.local/aspirinfund/application-researchdetail/id
	 */
	public function getApplicationResearchdetail($iId)
	{
		$oAspirinResearchApply = AspirinResearchApply::find($iId);
		return View::make("mobile.aspirin.scientificresearch.scientificdetail")->with('oAspirinResearchApply',$oAspirinResearchApply);
	}
	/**
	 * 进入--专项基金--科研申请--申请科研页面
	 * http://cdma.local/aspirinfund/apply-research-add
	 */
	public function getApplyResearchAdd()
	{
		$iUid = $this->iUserid;
		$oUser = User::find($iUid);
		$iCardAuthFlag = $oUser->card_auth_flag;
		$isWxFlag = user_agent_is_weixin();
		$wx_config = array();
		if ($isWxFlag){
			include_once  app_path().'/include/jssdk.class.php';  //先把类包含进来
			$oJssdk = new jssdk();
			$wx_config = $oJssdk->getSignPackage();
		}
		return View::make("mobile.aspirin.scientificresearch.applyresearch")
														->with('wx_config',$wx_config)
														->with('isWxFlag',$isWxFlag)
														->with('iCardAuthFlag',$iCardAuthFlag)
														->with('iUid',$iUid);
	}
	/**
	 * 进入--专项基金--科研申请--保存科研申请
	 * http://cdma.local/aspirinfund/apply-save
	 */
	public function postApplySave()
	{
		$iUid = $this->iUserid;
		$oUser = User::find($iUid);
		if($oUser->card_auth_flag == 2 ||$oUser->role_id == 2){
			$oAspirinResearchApply = new AspirinResearchApply();
			$oAspirinResearchApply->user_id = Input::get('user_id');
			$oAspirinResearchApply->apply_name = Input::get('apply_name');
			$oAspirinResearchApply->apply_city = Input::get('apply_city');
			$oAspirinResearchApply->apply_birthday = Input::get('apply_birthday');
			$oAspirinResearchApply->apply_title = Input::get('apply_title');
			$oAspirinResearchApply->apply_tel = Input::get('apply_tel');
			$oAspirinResearchApply->apply_email = Input::get('apply_email');
			$oAspirinResearchApply->apply_company = Input::get('apply_company');
			$oAspirinResearchApply->apply_address = Input::get('apply_address');
			$oAspirinResearchApply->apply_postcode = Input::get('apply_postcode');
			$oAspirinResearchApply->education_background = Input::get('education_background');
			$oAspirinResearchApply->work_experience = Input::get('work_experience');
			$oAspirinResearchApply->research_experience = Input::get('research_experience');
			$oAspirinResearchApply->question1 = Input::get('question1');
			$oAspirinResearchApply->question2 = Input::get('question2');
			$oAspirinResearchApply->question3 = Input::get('question3');
			$oAspirinResearchApply->save();
			return json_encode(array('success'=>true));
		}else{
			return json_encode(array('success'=>false,'mes'=>'请完成您的认证信息'));
		}
	}
	/**
	 * 进入--专项基金--科研申请--保存科研申请
	 * http://cdma.local/aspirinfund/application-result
	 */
	public function getApplicationResult()
	{
		$iUid = $this->iUserid;
		$iAspirinResearchApply = AspirinResearchApply::where('user_id',$iUid)->where('apply_type',1)->count();
		return View::make("mobile.aspirin.scientificresearch.congratulations")
								->with('iAspirinResearchApply',$iAspirinResearchApply);
// 		if($iAspirinResearchApply > 0){
// 			return View::make("mobile.aspirin.scientificresearch.congratulations")->with('iUid',$iUid);
// 		}else{
// 			$oAspirinResearchApply = AspirinResearchApply::where('apply_type',1)->orderBy('created_at','desc')->paginate(self::PAGESIZE);
// 			return View::make("mobile.aspirin.scientificresearch.applysuccess")->with('oAspirinResearchApply',$oAspirinResearchApply);
// 		}
	}
	/**
	 * 进入--专项基金--科研申请--成功申请科研列表
	 * http://cdma.local/aspirinfund/application-result
	 */
	 public function getApplicationList()
	 {
	 	$oAspirinResearchApply = AspirinResearchApply::where('apply_type',1)->orderBy('user_id','desc')->orderBy('created_at','desc')->paginate(13);
	 	return View::make("mobile.aspirin.scientificresearch.applysuccess")->with('oAspirinResearchApply',$oAspirinResearchApply);
	 }
	 /**
	  * 进入--专项基金--科研申请--成功申请科研名单分页加载
	  * http://cdma.local/aspirinfund/applicationsuccess-add
	  */
	 public function postApplicationsuccessAdd()
	 {
	 	$iPage = Input::get('page');
	 	$sKeyword = Input::get('keyword');
	 	$iPageSize = self::PAGESIZE;
	 	$aAspirinResearchApply = array();
	 	if($sKeyword){
	 		$oAspirinResearchApply = AspirinResearchApply::where('apply_type',1)
									 		->where('apply_name', 'LIKE', "%$sKeyword%")
									 		->orWhere('apply_company', 'LIKE', "%$sKeyword%")
									 		->orderBy('user_id','desc')
									 		->orderBy('created_at','desc')
									 		->skip(($iPage-1)*$iPageSize)
									 		->take($iPageSize)->get();
	 		$iConut = AspirinResearchApply::where('apply_type',1)
									 		->where('apply_name', 'LIKE', "%$sKeyword%")
									 		->orWhere('apply_company', 'LIKE', "%$sKeyword%")
									 		->count();
	 	}else{
	 		$oAspirinResearchApply = AspirinResearchApply::where('apply_type',1)->orderBy('user_id','desc')->orderBy('created_at','desc')->skip(($iPage-1)*$iPageSize)->take($iPageSize)->get();
	 		$iConut = AspirinResearchApply::where('apply_type',1)->count();
	 	}
	 	$ceil = 0;
	 	if(ceil($iConut/$iPageSize) <= $iPage){
	 		$ceil = 1;
	 	}
	 	$aAspirinResearchApply['ceil'] = $ceil;
	 	$iKey = ($iPage-1)*$iPageSize;
	 	foreach($oAspirinResearchApply as $k=>$value){
	 		$aAspirinResearchApply[$k]['applykey'] = $iKey + $k + 1;
	 		$aAspirinResearchApply[$k]['applyid'] = $value->id;
	 		$aAspirinResearchApply[$k]['applyname'] = $value-> apply_name;
	 		$aAspirinResearchApply[$k]['applycompany'] = $value-> apply_company;
	 		$aAspirinResearchApply[$k]['applytitle'] = $value-> apply_title;
	 	}
	 	echo json_encode($aAspirinResearchApply);
	 
	 }
	 /**
	  * 进入--专项基金--科研申请--成功申请科研-信息查看
	  * http://cdma.local/aspirinfund/applicationsuccess-message/id
	  */
	 public function getApplicationsuccessMessage($iId)
	 {
	 	$isWxFlag = user_agent_is_weixin();
	 	$wx_config = array();
	 	if ($isWxFlag){
	 		include_once  app_path().'/include/jssdk.class.php';  //先把类包含进来
	 		$oJssdk = new jssdk();
	 		$wx_config = $oJssdk->getSignPackage();
	 	}
	 	$oAspirinResearchApply = AspirinResearchApply::find($iId);
	 	return View::make("mobile.aspirin.scientificresearch.applysuccessmessage")
										 	->with('wx_config',$wx_config)
										 	->with('isWxFlag',$isWxFlag)
										 	->with('oAspirinResearchApply',$oAspirinResearchApply);
	 }
	 /**
	  * 进入--专项基金--科研申请--成功申请科研-信息查看
	  * http://cdma.local/aspirinfund/search-applicationsuccess
	  */
	 public function getSearchApplicationsuccess($sSearchKeyWord)
	 {
	 	$oAspirinResearchApply = AspirinResearchApply::where('apply_type',1)
						 	->orderBy('user_id','desc')
						 	->orderBy('created_at','desc')
						 	->where('apply_name', 'LIKE', "%$sSearchKeyWord%")
						 	->orWhere('apply_company', 'LIKE', "%$sSearchKeyWord%")
						 	->paginate(self::PAGESIZE);
	 	return View::make("mobile.aspirin.scientificresearch.applysuccess")
												 	->with('sSearchKeyWord',$sSearchKeyWord)
												 	->with('oAspirinResearchApply',$oAspirinResearchApply);
	 }
	 
}
?>