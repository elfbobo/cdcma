<?php

class AdmKypxController extends BaseController {
	
	CONST  PAGESIZE = 20;
	
	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	//好医生兑换记录列表
	public function OrderList()
	{
		//取出所有专家
		$input = Input::all();
		$oLists = IntegralLog::select('user_health_integral_log.*','user.user_name');
        $aLink = array();
		if(isset($input['user_name'])&&$input['user_name']){
			$oLists = $oLists->where('user.user_name','LIKE',"%{$input['user_name']}%");
			$aLink['user_name'] = $input['user_name'];
		}
		$oLists = $oLists->leftJoin('user','user.id','=','user_health_integral_log.user_id')
		->where('user_health_integral_log.obj_type', 'buyKypx');
		$oLists = $oLists->paginate(self::PAGESIZE);
		return  View::make('admin.integral.kypxorderlist')->with('oLists',$oLists)->with('aLink',$aLink);
	}
	


}