<?php 

/**
 * API操作类
 * @author dll
 *
 */
class ApiAspirinResearchController extends BaseController
{

	CONST  PAGESIZE = 10;
	public $iUserId = 0 ;

	public function __construct()
	{
		$this->beforeFilter('apilogin');
		$this->beforeFilter('apidoc', array('only' =>array('postApplySubmit')));
		global $iUserId;
		$this->iUserId = &$iUserId;
	}

	/**
	 * 返回专项基金科研申请——科研简介分享
	 * http://cdma.local/apiaspirinresearch/brief-share?token=
	 */
	public function getBriefShare(){
		$iInfo = AspirinResearchApply::getBriefInfo();
		$aShare = array(
			'html'        => $iInfo,
			'jump_url'    => 'http://e.dxy.cn/ccahouse/',
			'share_flag'  => 1,
			'share_title' => '科研培训简介',
			'share_des'   => '科研培训简介',
			'share_thumb' => Config::get('app.url').'/assets/images/mobile/logo.png',
			'share_url'   => Config::get('app.url').'/aspirinshare/research-brief'
		);
		echo  $this->formatRet(true,"成功","success",$aShare);exit;
	}

	/**
	 * 返回专项基金科研申请——申请表分享
	 * http://cdma.local/apiaspirinresearch/apply-share?token=
	 */
	public function getApplyShare(){
		$aShare = array(
			'share_flag'  => 1,
			'share_title' => '专项基金科研申请表',
			'share_des'   => '专项基金科研申请表',
			'share_thumb' => Config::get('app.url').'/assets/images/mobile/logo.png',
			'share_url'   => Config::get('app.url').'/aspirinshare/research-apply'
		);
		echo  $this->formatRet(true,"成功","success",$aShare);exit;
	}

	/**
	 * 返回专项基金科研申请——科研申请列表页（我的申请）
	 * http://cdma.local/apiaspirinresearch/apply-list?token=
	 */
	public function getApplyList(){
		$iUid = $this->iUserId;
		$oApply = AspirinResearchApply::where('user_id',$iUid)->orderby('created_at','desc')->get();
		$aApply = array();
		foreach($oApply as $k=>$v){
			$aApply[]= array(
				'id'          => $v->id,
				'apply_name'  => $v->apply_name,
				'apply_type'  => $v->apply_type,
				'created_at'  => substr($v->created_at,0,10)
			);
		}
		echo  $this->formatRet(true,"成功","success",$aApply);exit;
	}

	/**
	 * 返回专项基金科研申请——科研申请内容页
	 * http://cdma.local/apiaspirinresearch/apply-show?iApplyId=1&token=
	 */
	public function getApplyShow(){
		if(!Input::has('iApplyId')){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$iApplyId = intval(Input::get('iApplyId'));
		$iUid = $this->iUserId;
		$oApply = AspirinResearchApply::find($iApplyId);
		if(count($oApply)){
			$aApply = array(
				'apply_name'         	=> $oApply->apply_name,
				'apply_city'   		 	=> $oApply->apply_city,
				'apply_birthday'     	=> $oApply->apply_birthday,
				'apply_title'        	=> $oApply->apply_title,
				'apply_tel'          	=> $oApply->apply_tel,
				'apply_email'        	=> $oApply->apply_email,
				'apply_company'      	=> $oApply->apply_company,
				'apply_address'      	=> $oApply->apply_address,
				'apply_postcode'     	=> $oApply->apply_postcode,
				'education_background' 	=> $oApply->education_background,
				'work_experience'    	=> $oApply->work_experience,
				'research_experience' 	=> $oApply->research_experience,
				'question1'          	=> $oApply->question1,
				'question2'          	=> $oApply->question2,
				'question3'          	=> $oApply->question3,
				//	'question4'         => $oApply->question4,
				'apply_type'         	=> $oApply->apply_type
			);
			if($oApply->apply_type == 0 ||$oApply->apply_type == 2){
				$aApply['share_flag']  = 0;
			}else{
				$aApply['share_flag']  = 1;
				$aApply['share_title'] = '专项基金科研申请报告';
				$aApply['share_des'] = '专项基金科研申请报告';
				$aApply['share_thumb'] = Config::get('app.url').'/assets/images/mobile/logo.png';
				$aApply['share_url']   = Config::get('app.url').'/aspirinshare/research-result?iApplyId='.$iApplyId;
			}
			echo  $this->formatRet(true,"成功","success",$aApply);exit;
		}else{
			echo $this->formatRet(false, '该申请不存在');exit;
		}
	}

	/**
	 * 返回专项基金科研申请——科研申请提交   post
	 * http://cdma.local/apiaspirinresearch/apply-submit?apply_name=代路路&apply_city=深州&apply_birthday=921212&apply_title=助教&apply_tel=15028251028&apply_email=123@qq.com&apply_company=金叶&apply_address=朝阳&apply_postcode=010000&education_background=无&work_experience=无&research_experience=无&question1=无&question2=无&question3=无&token=
	 */
	public function postApplySubmit(){
		$iUid = $this->iUserId;
		$aInput = Input::all();
		$rule = array(
			'apply_name'         => 'required',
			'apply_city'   		 => 'required',
			'apply_birthday'     => 'required',
			'apply_title'        => 'required',
			'apply_tel'          => 'required',
			'apply_email'        => 'required',
			'apply_company'      => 'required',
			'apply_address'      => 'required',
			'apply_postcode'     => 'required',
			'education_background' => 'required',
			'work_experience'    => 'required',
			'research_experience' => 'required',
			// 'question1'          => 'required',
			// 'question2'          => 'required',
			'question3'          => 'required',
			// 'question4'          => 'required',
		);
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
			$msg = '请将申请信息填写完整，谢谢';
			echo $this->formatRet(false,$msg);exit;
		}
		if(empty($aInput['question1']) && empty($aInput['question2'])){
			$msg = '请选填任一方向的信息，谢谢';
			echo $this->formatRet(false,$msg);exit;
		}
		$mobile =  '/^1[34578][0-9]{9}$/';
		if(!preg_match($mobile,trim($aInput['apply_tel']))){
			$msg = "手机校验未通过";
			echo $this->formatRet(false,$msg);exit;
		}
		$email =  '/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/';
		if(!preg_match($email,trim($aInput['apply_email']))){
			$msg = "邮箱校验未通过";
			echo $this->formatRet(false,$msg);exit;
		}
		$code =  '/^[0-9][0-9]{5}$/';
		if(!preg_match($code,trim($aInput['apply_postcode']))){
			$msg = "邮编校验未通过";
			echo $this->formatRet(false,$msg);exit;
		}
		$aApply = array(
			'user_id'            => $iUid,
			'apply_name'         => trim($aInput['apply_name']),
			'apply_city'   		 => trim($aInput['apply_city']),
			'apply_birthday'     => trim($aInput['apply_birthday']),
			'apply_title'        => trim($aInput['apply_title']),
			'apply_tel'          => trim($aInput['apply_tel']),
			'apply_email'        => trim($aInput['apply_email']),
			'apply_company'      => trim($aInput['apply_company']),
			'apply_address'      => trim($aInput['apply_address']),
			'apply_postcode'     => trim($aInput['apply_postcode']),
			'education_background' => trim($aInput['education_background']),
			'work_experience'    => trim($aInput['work_experience']),
			'research_experience' => trim($aInput['research_experience']),
			'question1'          => isset($aInput['question1'])?trim($aInput['question1']):'',
			'question2'          => isset($aInput['question2'])?trim($aInput['question2']):'',
			'question3'          => trim($aInput['question3']),
			// 'question4'          => trim($aInput['question4']),
			'created_at'         => date('Y-m-d H:i:s',time())
		);
		$oApply = new AspirinResearchApply($aApply);
		$oApply->save();
		echo $this->formatRet(true,"成功","success");exit;
	}

	/**
	 * 返回专项基金科研申请——结果公示主页  get
	 * http://cdcma.local/apiaspirinresearch/apply-result?token=
	 */
	public function getApplyResult()
	{
		$iUid = $this->iUserId;
		$iFlag = 0;
		$oApplyLog = AspirinResearchApply::where('user_id',$iUid)->where('apply_type',1)->first();
		if($oApplyLog){
			$iFlag = 1;
		}
		$aApply = array('flag' => $iFlag);
		echo  $this->formatRet(true,"成功",$aApply,"success");exit;
	}

	/**
	 * 返回专项基金科研申请——结果公示列表页（入围名单）  get
	 * http://cdcma.local/apiaspirinresearch/apply-result-list?page=1&pagesize=10&keyword=&token=
	 */
	public function getApplyResultList()
	{
		$iPage = Input::get('page',1);
		$iPageSize = Input::get('pagesize',self::PAGESIZE);
		$sKeyword = Input::get('keyword','');
		$oApply = AspirinResearchApply::where('apply_type',1);
		if(!empty($sKeyword)){
			$oApply = $oApply->where(function($query) use($sKeyword){
				$query->where('apply_name','like','%'.$sKeyword.'%')->orWhere('apply_title','like','%'.$sKeyword.'%')->orWhere('apply_company','like','%'.$sKeyword.'%');
			});
		}
		$oApply = $oApply->orderby('updated_at','desc')->skip(($iPage-1)*$iPageSize)->take($iPageSize)->get();
		$aApply = array();
		foreach($oApply as $k=>$v){
			$aApply[]= array(
				'order_number' => $k+1+($iPage-1)*$iPageSize,
				'apply_id'     => $v->id,
				'apply_company'=> $v->apply_company,
				'apply_title'  => $v->apply_title,
				'apply_name'   => $v->apply_name
			);
		}
		echo  $this->formatRet(true,"成功","success",$aApply);exit;
	}

	/**
	 * 返回专项基金科研申请——上传集赞照片   post
	 * http://cdma.local/apiaspirinresearch/photo-submit?token=
	 */
	/*public function postPhotoSubmit(){
		$iUid = $this->iUserId;
		if( !Input::hasFile('file') ) {
			echo $this->formatRet(false, '请上传集赞照片');
			exit;
		}
		$file =  Input::file('file');
		$sImageUrl = $this->dealAvatar($file);
		$aZan = array(
				'user_id'  => $iUid,
				'image_url'=> $sImageUrl,
				'created_at'=> date('Y-m-d H:i:s',time())
		);
		$oZan = new AspirinResearchZan($aZan);
		$oZan->save();
		echo $this->formatRet(true,"上传成功","success");exit;
	}*/

	//====================通用   S=============================
	/**
	 * 通用，处理图片
	 * @param unknown_type $file
	 */
	/*private function dealAvatar($file){
		$sExt = $file->getClientOriginalExtension();//扩展名
		if( !in_array( $sExt, array('jpg', 'png', 'jpeg') ) ){
			echo $this->formatRet(false,'图片格式错误，目前支持jpg、png、jpeg');
			exit;
		}
		//处理图片
		$sNewName = date('His').rand(1,9999). '.' .$sExt;//新文件名
		$sFilePath = '/upload/jizan/'. date('Y'). '/' .date('md'). '/';
		mkdirs(public_path().$sFilePath);
		$file->move(public_path().$sFilePath , $sNewName);
		return $sFilePath.$sNewName;
	}*/

	//====================通用  E =============================
	/**
	 * 返回专项基金科研申请——培训课程列表页
	 * http://cdma.local/apiaspirinresearch/train-list?token=
	 */
	/*public function getTrainList(){
		$iUid = $this->iUserId;
		$aZanLog = AspirinResearchZan::where('user_id',$iUid)->lists('zan_type','id');
		//审核状态为1时表示审核通过，可以观看视频
		if(!in_array(AspirinResearchZan::CHECK_TYPE_SUCCESS, $aZanLog)){
			$msg = '观看培训课程需分享科研简介或申请表到微信朋友圈，集满20赞，截图上传审核通过即可观看，谢谢您的支持!';
			echo $this->formatRet(false,$msg);exit;
		}
		$iPagesize = Input::get('pagesize')?Input::get('pagesize'):self::PAGESIZE;
		
		$oTrain = AspirinResearchTrain::orderby('created_at','desc')->paginate($iPagesize);
		$aTrain = array();
		foreach($oTrain as $k=>$v){
			$aTrain[] = array(
					'id'               => $v->id,
					'train_title'      => $v->train_title,
					'train_thumb'      => Config::get('app.url').$v->train_thumb,
					'train_hits'       => $v->train_hits,
					'created_at'       => substr($v->created_at,0,10)
			);
		}
		echo  $this->formatRet(true,"成功","success",$aTrain);exit;
	}*/

	/**
	 * 返回专项基金科研申请——培训课程内容页
	 * http://cdma.local/apiaspirinresearch/train-show?iTrainId=1&token=
	 */
	/*public function getTrainShow(){
		if(!Input::has('iTrainId')){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$iTrainId = intval(Input::get('iTrainId'));
		$oTrain = AspirinResearchTrain::find($iTrainId);
		if(count($oTrain)){
			$oTrain->increment('train_hits');
			$aTrain = array(
					'train_title'    => $oTrain->train_title,
					'train_hits'     => $oTrain->train_hits,
					'video_url'      => $oTrain->video_url,
					'video_thumb'    => Config::get('app.url').$oTrain->video_thumb,
					'train_content'  => str_replace('/upload/ueditor/image/', Config::get('app.url').'/upload/ueditor/image/', $oTrain->train_content),
					'created_at'     => substr($oTrain->created_at,0,10)
			);
			echo  $this->formatRet(true,"成功","success",$aTrain);exit;
		}else{
			echo $this->formatRet(false,'该课程不存在');exit;
		}
	}*/

}