<?php 

/*
 * 全文求助
 * zgq 
 * */
class ApiHelpController extends BaseController
{
	 
	public function __construct()
	{
		$this->beforeFilter('apilogin'); 
	}

	public function getIndex()
	{ 
		global $iUserId;
		$iUid = $iUserId;
		$sMail = User::select("user_email")->where('id',$iUid)->pluck("user_email");
		$aMail = array(
		   "mail"=> $sMail,
		); 
		$aData = array('success'=>true,'data'=>$aMail);
       	return json_encode($aData); 
	}
	
	public function getMessage()
	{
		global $iUserId;
		$aMessage = Input::all();
		$iPmid =  $aMessage['pmid'];
		if(!is_numeric($iPmid)){
			$sMessage = '文献id请填整形数字';
			$aData = array('success'=>false,'msg'=>$sMessage);
			return json_encode($aData);
		}
		$iYear = $aMessage['year']; 
		if(!is_numeric($iYear)){
			$sMessage = '出版年请填整形数字';
			$aData = array('success'=>false,'msg'=>$sMessage);
	       	return json_encode($aData);
		}
		$iVolume = $aMessage['volume'];
		if(!is_numeric($iVolume)){
			$sMessage = '文章卷请填整形数字';
			$aData = array('success'=>false,'msg'=>$sMessage);
	       	return json_encode($aData);
		}
		$iIssue = $aMessage['issue'];
		if(!is_numeric($iIssue)){
			$sMessage = '文章期请填整形数字';
			$aData = array('success'=>false,'msg'=>$sMessage);
	       	return json_encode($aData);
		}
		if(empty($aMessage['title'])){
			$sMessage = "标题不能为空";
			$aData = array('success'=>false,'msg'=>$sMessage);
	       	return json_encode($aData);
		}
		if(empty($aMessage['journal'])){
			$sMessage = "期刊名不能为空";
			$aData = array('success'=>false,'msg'=>$sMessage);
	       	return json_encode($aData);
		}
  	   if(empty($aMessage['mail'])){
			$sMessage =  "邮箱不能为空";
			$aData = array('success'=>false,'msg'=>$sMessage);
	       	return json_encode($aData);
		}
		$sPre= "/^[a-z0-9\._-]+@[a-z0-9\.-]+\.[a-z]{2,4}$/";  
		if(!preg_match($sPre,$aMessage['mail'])){
			$sMessage =  "请检查您的邮箱是否正确";
			$aData = array('success'=>false,'msg'=>$sMessage);
	       	return json_encode($aData);
		} 
		$iUid = $iUserId;
		$iPub = $aMessage['pmid']; 
		$oPubmeHelps = PubmedHelpMail::where('userid','=',$iUid)->where('pmid',$iPub)->first();
		if(count($oPubmeHelps)){
			$sMessage = "您已经求助过该篇文章";
			$aData = array('success'=>false,'msg'=>$sMessage);
	       	return json_encode($aData);
		}else{
			$oPubmeHelp = new PubmedHelpMail();
			$oPubmeHelp -> userid  = isset($iUid)?trim($iUid):"";
			$oPubmeHelp -> email   = isset($aMessage['mail'])?trim($aMessage['mail']):"";
			$oPubmeHelp -> title   = isset($aMessage['title'])?trim($aMessage['title']):"";
			$oPubmeHelp -> journal = isset($aMessage['journal'])?trim($aMessage['journal']):"";
			$oPubmeHelp -> year    = isset($iYear)?trim($iYear):"";
			$oPubmeHelp -> volume  = isset($iVolume)?trim($iVolume):"";
			$oPubmeHelp -> issue   = isset($iIssue)?trim($iIssue):"";
			$oPubmeHelp -> pmid    = isset($iPmid)?trim($iPmid):"";
			$oPubmeHelp -> save();
			$sMessage = '文章的pmid为：'.$aMessage['pmid'].'|标题为：'.$aMessage['title'].'|刊名：'.$aMessage['journal'].'|出版在：'.$aMessage['year'].'年|第'.$aMessage['volume'].'卷|第'.$aMessage['issue'].'期';
			$subject = "国卫健康云全文求助"; 
			$content = "国卫健康云求助信息：$sMessage, 请将回复发送到邮箱：".$oPubmeHelp->email;
			$oPubmeHelp->SendEmail($subject,$content);
		    $sMessage =  "已经为您发送请求";
			$aData = array('success'=>true,'msg'=>$sMessage);
	       	return json_encode($aData);
		 }
	}
		
}