<?php 

/**
 * 在线会议
 * @author gaohongye
 * 定时推送通知：http://cdcma.medlive.cn/apiaspirinonline/listener-notice
 *            http://cdcma.medlive.cn/apiaspirinonline/speaker-notice
 * 申报验证，用户预置，会畅开会场数限制，每周一定时推送
 */
class ApiAspirinOnlineController extends BaseController
{

	//会议申请者可提前多长时间开启会议（单位：分钟）
	const ADVANCETIME = 10;
	//不足5人参加时取消会议
	const LIMIT_CANCEL_COUNT = 10;
	const CATID = 1;
	const  PAGESIZE = 10;
	public $iUserId = 0 ;
	
	public function __construct()
	{
		$this->beforeFilter('apilogin', array('except' =>array('getSpeakerNotice','getListenerNotice')));
		$this->beforeFilter('apidoc', array('only'=>array('postSpeakerEnter','getMyEnterList')));
		global $iUserId;
		$this->iUserId = &$iUserId;
	}

	/**
	 * 在线会议首页banner图
	 * http://2kghy.kydev.net/apiaspirinonline/banner?token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
	 */
	public function getBanner()
	{
		$aBannerInfo = array();
		$oBanner = AspirinOnlineBanner::orderby('created_at','desc')->first();
		if(count($oBanner)){
			$oPreOpen = AspirinOnlineSpeaker::where('open_flag',0)->where('cancel_flag',0)->get();
			$oHasOpen = AspirinOnlineSpeaker::where('open_flag',1)->get();
			$iListenerCount = 0;
			foreach($oHasOpen as $k=>$v){
				$iListenerCount+= $v->listener_count;
			}
			$iTotalCount = count($oHasOpen) + $iListenerCount;
			$aBannerInfo = array(
				'banner_title'  => $oBanner->banner_title,
				'banner_des'    => $oBanner->banner_des,
				'banner_thumb'  => Config::get('app.url').$oBanner->banner_thumb,
				'show_flag'     => $oBanner->show_flag,
				'open_count'    => count($oHasOpen)!=0?count($oHasOpen):0,     //成功举办的场数
				'attend_count'  => $iTotalCount!=0?$iTotalCount:0,         //参与人数
				'pre_count'     => count($oPreOpen)!=0?count($oPreOpen):0      //预告场数
			);
		}
		echo  $this->formatRet(true,"成功","success",$aBannerInfo);exit;
	}

	/**
	 * 在线会议主题ppt课件
	 * http://cdma.local/apiaspirinonline/ppt?catid=1&token=
	 */
	public function getPpt()
	{
		$iCatId = Input::get('catid');
		if(!in_array($iCatId, [1,2,3,4])){
			echo $this->formatRet(false,"未传科室分类参数","");exit;
		}
		$oPpt = AspirinOnlinePpt::where('catid',$iCatId)->orderby('id','asc')->get();
		$aPpt = array();
		if(count($oPpt)){
			foreach($oPpt as $k=>$v){
				$aPpt[] = array(
					'id'           => $v->id,
					'ppt_title'    => $v->ppt_title,
					'ppt_thumb'    => Config::get('app.url').$v->ppt_thumb,
					'created_at'   => substr($v->created_at,0,10)
				);
			}
		}
		echo  $this->formatRet(true,"成功",'',$aPpt);exit;
	}
	
	/**
	 * 在线会议主题ppt课件详情
	 * http://cdma.local/apiaspirinonline/ppt-show?iPptId=1&token=
	 */
	public function getPptShow()
	{
		if(!Input::has('iPptId')){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$iPptId = intval(Input::get('iPptId'));
		$oPpt = AspirinOnlinePpt::find($iPptId);
		$aPptUrl = array(
				'ppt_upload_url'  => isset($oPpt->ppt_url)?$oPpt->ppt_url:''
		);
		$oPptImage = AspirinOnlinePptImage::where('ppt_id',$iPptId)->orderby('id','asc')->get();
		if(count($oPptImage)){
			foreach($oPptImage as $k=>$v){
				$aPptImage[] = array(
					'id'		=> $v->id,
					'image_url'	=> Config::get('app.url').$v->image_url
				);
			}
			echo  $this->formatRet(true,"成功",$aPptUrl,$aPptImage);exit;
		}else{
			echo $this->formatRet(false,'该课件不存在');exit;
		}
	}

	/***********************************讲者报名参会************************/
	/**
	 * 在线会议主题可选时间列表——讲者报名列表页
	 * http://cdma.local/apiaspirinonline/enter-list?catid=1&pptid=&token=
	 */
	public function getEnterList()
	{
		$sNowTime = date('Y-m-d H:i:s',time());
		$iCatId = Input::get('catid');
		if(!in_array($iCatId, [1,2,3,4])){
			echo $this->formatRet(false,"未传科室分类参数","");exit;
		}

		// print_r($oOnlineList);die;
		$iPptId = Input::get('pptid');

		if(!$iPptId && $iPptId == 0){
			echo $this->formatRet(false,"未传所选课件参数","");exit;
		} else {
            // 根据 pptid 获取标题
            $ppt_title = AspirinOnlinePpt::where('id', $iPptId)->pluck('ppt_title');
        }
		//主題獲取
		$iOnlineId = Input::get('iOnlineId');
		if($iOnlineId){
			$oOnline = AspirinOnline::find($iOnlineId);
		}else{
		    // 通过 ppt 标题来获取主题 信息
		    if (!empty($ppt_title)) {
                $oOnline = AspirinOnline::where('catid',$iCatId)->where('online_title', $ppt_title)->orderby('created_at','desc')->first();
            }
            if (empty($oOnline))
			    $oOnline = AspirinOnline::where('catid',$iCatId)->where('show_end_time','>',$sNowTime)->orderby('created_at','desc')->first();
		}

        //获取所有有效的主题列表
        $oOnlineList = array();
        $oOnlineLists = AspirinOnline::where('catid',$iCatId)->where('show_end_time','>',$sNowTime)->orderby('created_at','desc')->get()->toArray();
        if($oOnlineLists){
            foreach ($oOnlineLists as $key => $value) {
                $oOnlineLists[$key]['show_start_time'] = date("Y-m-d", strtotime($value['show_start_time']));
                $oOnlineLists[$key]['show_end_time'] = date("Y-m-d", strtotime($value['show_end_time']));
                if ($value['online_title'] == $ppt_title) {
                    array_unshift($oOnlineList, $oOnlineLists[$key]);
                } else {
                    array_push($oOnlineList, $oOnlineLists[$key]);
                }
            }
        }

		// var_dump($oOnline);
		// $log = DB::getQueryLog();
		// print_r($log);die;
		if(count($oOnline)){
			$oPreOpen = AspirinOnlineSpeaker::where('online_catid',$iCatId)->where('open_flag',0)->where('cancel_flag',0)->get();
			$oHasOpen = AspirinOnlineSpeaker::where('online_catid',$iCatId)->where('open_flag',1)->get();
			$iListenerCount = 0;
			foreach($oHasOpen as $k=>$v){
				$iListenerCount+= $v->listener_count;
			}
			$iTotalCount = count($oHasOpen) + $iListenerCount;
			$aOnlineInfo = array(
				'id'				=> $oOnline->id,
				'online_title'  	=> $oOnline->online_title,
				'online_des'    	=> $oOnline->online_des,
				'online_banner' 	=> Config::get('app.url').$oOnline->online_banner,
				'show_flag'     	=> $oOnline->show_flag,
				'open_count'    	=> count($oHasOpen)!=0?count($oHasOpen):0,		//成功举办的场数
				'attend_count'  	=> $iTotalCount!=0?$iTotalCount:0,				//参与人数
				'pre_count'     	=> count($oPreOpen)!=0?count($oPreOpen):0,		//预告场数,
				'show_start_time'   => strtotime($oOnline->show_start_time),
				'show_end_time'		=> strtotime($oOnline->show_end_time),
				'ppthtml'       	=> Config::get('app.url').'/mobile/ppt/'.$iPptId
			);
			//列出上周已报名会议
			$aLastWeek = array();
			$oLastLog = AspirinOnlineSpeaker::where('online_id',$oOnline->id)
				->where('speaker_id',$this->iUserId)
				->where('cancel_flag',0)
				->orderby('created_at','desc')
				->first();
			$sNowTime = date('Y-m-d H:i',time());
			if($oLastLog){
					$oLastTime = AspirinOnlineTime::find($oLastLog->online_time_id);
					$sThisMon = date('Y-m-d',(time()-((date('w')==0?7:date('w'))-1)*24*3600)).' 00:00:00';
					$sNextMon = date('Y-m-d',(time()+(7-(date('w')==0?7:date('w')))*24*3600)).' 23:59:59';
					if($oLastTime->online_date < $sNextMon && $oLastTime->online_date > $sThisMon){
						$sStartTime = substr($oLastTime->online_date,0,10).' '.substr($oLastTime->time_period,0,5);
						$sEndTime = substr($oLastTime->online_date,0,10).' '.substr($oLastTime->time_period,6,5);
						//讲者可提前10分钟启动会议
						$sStart = date('Y-m-d H:i',(strtotime($sStartTime)-self::ADVANCETIME*60));
						if($sNowTime < $sStart){
							$aLastWeek[]= array(
								'online_id'     => $oOnline->id,
								'online_time_id'=> $oLastTime->id,
								'online_date'   => date('Y年m月d日',strtotime($oLastTime->online_date)),
								'time_period'   => $oLastTime->time_period,
								'speaker_count' => $oLastTime->speaker_count,
								'limit_count'   => Config::get('config.self_help_meeting_order_set'),
								'enter_flag'    => 2     //按钮显示为"启动会议"，不可点
							);
						}else if($sNowTime > $sStart && $sNowTime < $sEndTime){
							$aLastWeek[]= array(
								'online_id'     => $oOnline->id,
								'online_time_id'=> $oLastTime->id,
								'online_date'   => date('Y年m月d日',strtotime($oLastTime->online_date)),
								'time_period'   => $oLastTime->time_period,
								'speaker_count' => $oLastTime->speaker_count,
								'limit_count'   => Config::get('config.self_help_meeting_order_set'),
								'enter_flag'    => 3     //按钮显示为"启动会议"，可点
							);
						}
					}
			}
			//列出下一周的会议排期，时间依次递增，每周更新一次
			$sNextMon = date('Y-m-d',time()).' 00:00:00';
			$sNextMonReal = date('Y-m-d',(time()+(7-(date('w')==0?7:date('w')))*24*3600)).' 23:59:59';
			$sNextSun = date('Y-m-d',strtotime($sNextMonReal)+6*24*3600).' 23:59:59';
			$oOnlineTime = AspirinOnlineTime::where('online_id',$oOnline->id)
				->where('online_date','>',$sNextMon)
				->where('online_date','<=',$sNextSun)
				->where('speaker_count','<',Config::get('config.self_help_meeting_order_set'))
				->orderby('online_date','asc')
				->orderby('time_period','asc')
				->get();
			$aOnlineTime = array();
			foreach($oOnlineTime as $k=>$v){
				$oLog = AspirinOnlineSpeaker::where('online_id',$oOnline->id)
					->where('online_time_id',$v->id)
					->where('speaker_id',$this->iUserId)
					->where('cancel_flag',0)
					->first();
				if(!count($oLog)){
					$iFlag = 1;         //报名人数未满，可报名
				}else{
					$iFlag = 2;          //按钮显示为"启动会议"，不可点
				}
				$aOnlineTime[] = array(
					'online_id'     => $oOnline->id,
					'online_time_id'=> $v->id,
					'online_date'   => date('Y年m月d日',strtotime($v->online_date)),
					'time_period'   => $v->time_period,
					'speaker_count' => $v->speaker_count,
					'limit_count'   => Config::get('config.self_help_meeting_order_set'),
					'enter_flag'    => $iFlag         
				);
			}
			//报名者满25名后该时间段会议将至于最下方
			$oOnlineOverTime = AspirinOnlineTime::where('online_id',$oOnline->id)
				->where('online_date','>',$sNextMon)
				->where('online_date','<=',$sNextSun)
				->where('speaker_count','=',Config::get('config.self_help_meeting_order_set'))
				->orderby('online_date','asc')
				->orderby('time_period','asc')
				->get();
			// $log = DB::getQueryLog();
			// print_r($log);die;
			$aOnlineOverTime = array();
			foreach($oOnlineOverTime as $k=>$v){
				$aOnlineOverTime[] = array(
					'online_id'     => $oOnline->id,
					'online_time_id'=> $v->id,
					'online_date'   => date('Y年m月d日',strtotime($v->online_date)),
					'time_period'   => $v->time_period,
					'speaker_count' => $v->speaker_count,
					'limit_count'   => Config::get('config.self_help_meeting_order_set'),
					'enter_flag'    => 0          //报名人数已满，不可报名
				);
			}
			$aOnlineTime = array_merge($aLastWeek,$aOnlineTime,$aOnlineOverTime);
			echo  $this->formatRet(true,"成功",$aOnlineInfo,$aOnlineTime,array('oOnlineList'=>$oOnlineList));exit;
		}else{
			echo $this->formatRet(false,"该月份目前暂无会议，敬请期待");exit;
		}
	}

	/**
	 * 在线会议——讲者报名   post 提交
	 * http://cdma.local/apiaspirinonline/speaker-enter?iOnlineId=1&token=
	 */
	public function postSpeakerEnter()
	{
		$oUser = User::find($this->iUserId);
		$aInfo = array();
		//试点地区仅限召开102场
		/*$aPilotCountry = User::getPilotCountry();
		$aCityUser = User::where('user_city',$oUser->user_city)->lists('id');
		if(in_array($oUser->user_county,$aPilotCountry)){
			$aCityUser = User::where('user_county',$oUser->user_county)->lists('id');
		}
		if(!empty($aCityUser)){
			$iAllCount = AspirinOnlineSpeaker::where('cancel_flag',0)->whereIn('speaker_id',$aCityUser)->count();
			if($iAllCount && $iAllCount >= 200){
				echo $this->formatRet(false,"已达试点地区报名场次数上限");exit;
			}
		}*/
		//只有通过医师认证并且是副高及以上权限的医生才可以作为讲者报名（后改为主治及以上）
		$aPos = User::getPosition();
		if(($oUser->role_id == 3 && ($oUser->user_position == $aPos[2] || $oUser->user_position == $aPos[3] || $oUser->user_position == $aPos[4] )) || $oUser->role_id == 2){
			$oMonthLimit = AspirinOnlineSpeaker::where('speaker_id',$this->iUserId)->where('cancel_flag',0)->orderby('created_at','desc')->first();
			if($oMonthLimit){
				$aInfo = array(
					'declare_name' => $oMonthLimit->declare_name,
					'declare_id_num' => $oMonthLimit->declare_id_number,
					'declare_bank' => $oMonthLimit->declare_bank,
					'declare_bank_num' => $oMonthLimit->declare_bank_number,
				);
				$iNowMonth = date('Y-m',time());
				if($oUser->user_city != 320100){
					$iOnlineMonth = substr($oMonthLimit->created_at,0,7);
					if ($iOnlineMonth == $iNowMonth){
					    // 单个账号--南京每月限报2场，其他城市每月限报1场（取消）
						// echo $this->formatRet(false,"每月只可报名一场会议");exit;
					}
				}else{
					$oMonthLimit2 = AspirinOnlineSpeaker::where('speaker_id',$this->iUserId)->where('cancel_flag',0)->orderby('created_at','desc')->get();
					$iNum = 0;
					foreach($oMonthLimit2 as $m=>$n){
						$iOnlineMonth = substr($n->created_at,0,7);
						// echo $iOnlineMonth."=={$iNowMonth}<br />";
						if ($iOnlineMonth == $iNowMonth){
							$iNum++;
						}
					}
					// $log = DB::getQueryLog();
					// print_r($log);die;
					if($iNum>=2){
					    // 单个账号--南京每月限报2场，其他城市每月限报1场（取消）
						// echo $this->formatRet(false,"每月只可报名两场会议");exit;     //南京用户每月限报名两场会议
					}
				}
			}
			if(!$aInfo){
				$bankInfo = UserBankAccount::where("user_id",$this->iUserId)->first();
				if($bankInfo){
					$bankInfos = $bankInfo->toArray();
					$aInfo = array(
						'declare_name' => $bankInfo['bank_realname'],
						'declare_id_num' => $bankInfo['bank_idcard_code'],
						'declare_bank' => $bankInfo['bank_name'],
						'declare_bank_num' => $bankInfo['bank_account'],
					);
				}
			}
			// 始终查不到用户信息时提示错误
			/*
			if (!$aInfo) {
                echo $this->formatRet(false,"您帐号认证信息有误，请重新认证或联系管理人员");exit;
            } */
			
			/*$oTwiceLimit = AspirinOnlineSpeaker::where('speaker_id',$this->iUserId)->where('cancel_flag',0)->orderby('created_at','desc')->get();
			if(count($oTwiceLimit) >= 2){
				echo $this->formatRet(false,"试运营期内限每位讲者只能参与两次");exit;
			}*/
			
			//判断用户是否已经申报过该月会议（心内和神内只能报名一个）
			$iOnlineId = intval(Input::get('iOnlineId'));
			$sNowTime = date('Y-m-d H:i:s',time());
			$aOnline = AspirinOnline::where('show_start_time','<',$sNowTime)
				->where('show_end_time','>',$sNowTime)
				->orWhere('id',$iOnlineId)
				->orderby('created_at','desc')
				->lists('id');
			if(!empty($aOnline)){
				$oOnlineLog = AspirinOnlineSpeaker::whereIn('online_id',$aOnline)->where('speaker_id',$this->iUserId)->where('cancel_flag',0)->first();
				if(count($oOnlineLog)){
					echo $this->formatRet(false,"您已经申报过该月会议");exit;
				}else{
					echo $this->formatRet(true,"success", $aInfo);exit;
				}
			}else{
				echo $this->formatRet(false,"会议主题数据有误，请尝试重新进入");exit;
			}
			/*$iOnlineId = intval(Input::get('iOnlineId'));
			$oOnline = AspirinOnline::find($iOnlineId);
			if($oOnline){
				echo $this->formatRet(true,"success",$aInfo);exit;
			}else{
				echo $this->formatRet(false,"会议主题数据有误，请尝试重新进入");exit;
			}*/
		}else{
			echo $this->formatRet(false,"仅限副高以上职称用户报名","");exit;
		}
	}

	/**
	 * 在线会议——讲者申报确认   post 提交
	 * http://2kdll.kydev.net/apiaspirinonline/speaker-submit?iPptId=1&iOnlineId=1&timeid=55&declare_name=代路路&declare_id_num=131182199211112222&declare_bank=北京银行&declare_bank_num=6222100056753098&token=
	 */
	public function postSpeakerSubmit()
	{
		if(!Input::has('iPptId') || !Input::has('iOnlineId') ||!Input::has('timeid')){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		if(Input::get('iPptId')==0 || Input::get('iOnlineId')==0 ||Input::get('timeid')==0){
			echo  $this->formatRet(false,"会议或课件参数不正确");exit;
		}
		$iPptId = intval(Input::get('iPptId'));
		$iOnlineId = intval(Input::get('iOnlineId'));
		$iOnlineTimeId = intval(Input::get('timeid'));
		$sName = trim(Input::get('declare_name'));
		$sIdNum = trim(Input::get('declare_id_num'));
		$sBank = trim(Input::get('declare_bank'));
		$sBankNum = trim(Input::get('declare_bank_num'));
		if(!$sName){
			echo $this->formatRet(false,"请输入申报姓名");exit;
		}
		// if(!$sIdNum || !(validation_filter_id_card($sIdNum))){
		if(!$sIdNum){
			echo $this->formatRet(false,"请输入有效身份证号");exit;
		}
		if(!$sBank){
			echo $this->formatRet(false,"请输入开户行");exit;
		}
		// if(!$sBankNum || !(luhm($sBankNum))){
		if(!$sBankNum){
			echo $this->formatRet(false,"请输入有效银行账号");exit;
		}
		$oOnlineTime = AspirinOnlineTime::find($iOnlineTimeId);
		$oOnline = AspirinOnline::find($iOnlineId);
		//调用会畅接口相关准备s
		$sOnlineDate = $oOnlineTime->online_date;
		$sOnlineStartTime = substr($sOnlineDate,0,11).substr($oOnlineTime->time_period,0,5).':00';
		$sOnlineEndTime = substr($sOnlineDate,0,11).substr($oOnlineTime->time_period,6,5).':00';
		//根据会议的起始和结束时间返回时间段数组，和自助会议平台一样，以半小时计，例如：06:00为61,06:30为62（不足半点的按半点算，06:20也计为61）
		$aTimePeriod = AspirinOnline::getTimePeriod($sOnlineStartTime,$sOnlineEndTime);
		//判断时间段是否被占用,筛选出数组中同一时间段的个数
		$aTimeRecordLog = MeetingTime::where('meeting_date',$sOnlineDate)->lists('meeting_time_period','id');
		$aCountRecord = array_count_values($aTimeRecordLog);
		$iMaxNum = Config::get('config.self_help_meeting_order_set');
		$aTimeCode = array();
		foreach($aCountRecord as $k=>$v){
			if($v >= $iMaxNum){
				$aTimeCode[] = $k;
			}
		}
		if(!empty($aTimeCode)){
			foreach($aTimePeriod as $v){
				if(in_array($v,$aTimeCode)){
					echo  $this->formatRet(false,"该时间段已被占满");exit;
				}
			}
		}
		//获取会议编码
		$aRes = MeetingTime::where('meeting_date',$sOnlineDate)
			->whereIn('meeting_time_period',$aTimePeriod)
			->lists('meeting_time_period','meeting_code');
		if(!empty($aRes)){
			$aRes = array_keys($aRes);
			$oCode = MeetingCode::whereNotIn('code',$aRes)->orderby('id','asc')->first();
		}else{
			// $oCode = MeetingCode::orderby('id','desc')->first();
			$oCode = MeetingCode::orderBy(DB::raw('RAND()'))->first();
		}
		if($oCode){
			//调用会畅接口**********************start**********************/
			$sCode = $oCode->code;
			$sApiKey = Config::get('config.hc_api_key');
			$sApiUrl = Config::get('config.hc_api_url_v2');
			$sTimeStamp = time();
			$sToken = md5($oCode->name.'|'.$sApiKey.'|'.$sTimeStamp);
			$iDuration = (strtotime($sOnlineEndTime) - strtotime($sOnlineStartTime))/60;
			$data = array(
				'loginName'  => $oCode->name,
				'timeStamp'  => $sTimeStamp,
				'token'  	 => $sToken,
				'confName'   => $oOnline->online_title,
				'hostKey'    => $oCode->key,
				'startTime'  => $sOnlineStartTime,
				'duration'   => $iDuration,
				'optionJbh'  => 0,
				'optionStartType'  => 1
			);
			$aReturnInfo = requestApiByCurl($sApiUrl,$data,'POST');
			// print_r($aReturnInfo);die;
			//调用会畅接口**********************end*********************/
			//记录接口返回信息
			if($aReturnInfo['status'] == 100){
				//记录讲者申报信息
				$oSpeakerInfo = AspirinOnlineSpeaker::where('online_id',$iOnlineId)
					->where('online_time_id',$iOnlineTimeId)
					->where('speaker_id',$this->iUserId)
					->where('cancel_flag',1)
					->first();
				if(count($oSpeakerInfo)){
					$oSpeakerInfo->declare_name        = $sName;
					$oSpeakerInfo->declare_id_number   = $sIdNum;
					$oSpeakerInfo->declare_bank        = $sBank;
					$oSpeakerInfo->declare_bank_number = $sBankNum;
					$oSpeakerInfo->open_flag      = 0;
					$oSpeakerInfo->cancel_flag    = 0;
					$oSpeakerInfo->listener_count = 0;
					$oSpeakerInfo->save();
				}else{
					$aSpeakerInfo = array(
						'ppt_id'            => $iPptId,
						'online_catid'      => $oOnline->catid,
						'online_id'         => $iOnlineId,
						'online_time_id'    => $iOnlineTimeId,
						'speaker_id'	    => $this->iUserId,
						'declare_name'	    => $sName,
						'declare_id_number'	=> $sIdNum,
						'declare_bank'		=> $sBank,
						'declare_bank_number'=> $sBankNum,
						'created_at'        => date('Y-m-d H:i:s',time())
					);
					$oSpeakerInfo = new AspirinOnlineSpeaker($aSpeakerInfo);
					$oSpeakerInfo->save();
				}
				$iOnlineSpeakerId = $oSpeakerInfo->id;
				//报名总人数加1
				$oOnlineTime->increment('speaker_count');
				$aLog = array(
					'online_id'  	=>  $iOnlineSpeakerId,
					'audio'  		=>  $aReturnInfo['data']['audio'],
					'hoststarturl'  =>  $aReturnInfo['data']['hostStartUrl'],
					'starturl'  	=>  $aReturnInfo['data']['startUrl'],
					'confstatus'  	=>  $aReturnInfo['data']['confStatus'],
					'optionjbh'  	=>  $aReturnInfo['data']['optionJbh'],
					'meetingno'  	=>  $aReturnInfo['data']['meetingNo'],//$sCode
					'api_id'  		=>  $aReturnInfo['data']['id'],
					'starttime' 	=>  $aReturnInfo['data']['startTime'],
					'duration'  	=>  $aReturnInfo['data']['duration'],
					'meetingname'   =>  $aReturnInfo['data']['meetingName'],
					'token'  		=>  $aReturnInfo['data']['token'],
					'pwd'  			=>  $aReturnInfo['data']['h323pwd'],
					'userid'  		=>  $aReturnInfo['data']['userId'],
					'joinurl'  		=>  $aReturnInfo['data']['joinUrl'],
					'hostkey'  		=>  $aReturnInfo['data']['hostKey'],
					'maxcount'  	=>  $aReturnInfo['data']['maxCount'],
					'created_at'  	=>  date('Y-m-d H:i:s',time())
				);
				$oMeetingApiLog = new MeetingApiLog($aLog);
				$oMeetingApiLog->save();
				//记录会议时间段
				foreach($aTimePeriod as $val){
					$aPeriod = array(
						'online_id'           => $iOnlineSpeakerId,
						'meeting_date'        => $sOnlineDate,
						'meeting_time_period' => $val,
						'meeting_code'        => $sCode,
						'created_at'          => date('Y-m-d H:i:s',time())
					);
					$oMeetingTime = new MeetingTime($aPeriod);
					$oMeetingTime->save();
				}
				//提交成功返回信息
				$aInfo = array(
					'online_id'     => $iOnlineId,
					'online_time_id'=> $iOnlineTimeId,
					'online_title'  => $oOnline->online_title,
					'online_date'   => date('Y年m月d日',strtotime($oOnlineTime->online_date)),
					'time_period'   => $oOnlineTime->time_period,
					'cancel_type'   => 0,      //按钮状态（0：可点；1：不可点）
					'open_type'     => 1,
					'invite_type'   => 0
				);
				echo $this->formatRet(true,"申报成功","success",$aInfo);
				//健康云积分操作
				$IntegralLog = new IntegralLog();
				$IntegralLog->updateIntegral($this->iUserId,$iOnlineSpeakerId,'class');
				//讲者报名成功推送给该城市所有医生
				$oUser = User::find($this->iUserId);
				$sUserCompany = $oUser->user_company;
				if(is_numeric($sUserCompany)&&$sUserCompany!=0){
					$sUserCompany = Hospital::where('id',$sUserCompany)->pluck('name');
				}
				if(!$sUserCompany){
					$sUserCompany = $oUser->user_company_name;
				}
				$oDoctorUser = User::where('role_id',3)->where('card_auth_flag',2)->where('user_city',$oUser->user_city)->where('id','!=',$this->iUserId)->get();
				foreach($oDoctorUser as $k=>$v){
					//发送消息通知
					$sMeetingtime = date('Y年m月d日',strtotime($oOnlineTime->online_date)).$oOnlineTime->time_period;
					$sPushContent = PushService::ONLINE_LISTENER_ALL;
					$sPushContent = str_replace('{{userhospital}}', $sUserCompany, $sPushContent);
					$sPushContent = str_replace('{{username}}', $oUser->user_name, $sPushContent);
					$sPushContent = str_replace('{{meetingtime}}', $sMeetingtime, $sPushContent);
					// PushService::postClient('国卫健康云',$sPushContent,15,$v['id'],$oOnline->catid);
					$aNotice = array(
						'user_id'		=> $v['id'],
						'notice_content'=> $sPushContent,
						'notice_type'	=> 15,
						'detail_id'		=> $oOnline->catid
					);
					$oNotice = new AspirinUserNotice($aNotice);
					$oNotice->save();
				}
				exit;
			}else{
				echo  $this->formatRet(false,"获取会议链接失败");exit;
			}
			
		}else{
			echo  $this->formatRet(false,"未获取到会议编码，请重试");exit;
		}
	}

	/**
	 * 在线会议——讲者取消会议  
	 * http://cdma.local/apiaspirinonline/speaker-cancel?timeid=4&token=
	 */
	public function getSpeakerCancel()
	{
		$iOnlineTimeId = intval(Input::get('timeid'));
		$oSpeakerInfo = AspirinOnlineSpeaker::where('online_time_id',$iOnlineTimeId)->where('speaker_id',$this->iUserId)->where('cancel_flag',0)->first();
		$oSpeakerInfo->cancel_flag=1;
		$oSpeakerInfo->save();
		if(AspirinOnlineSpeaker::getCancel($oSpeakerInfo->id,$iOnlineTimeId,$this->iUserId)){
			$arr = array('online_catid' => $oSpeakerInfo->online_catid); 
			echo $this->formatRet(true,"成功取消该会议",$arr,'success');exit;
		}else{
			echo $this->formatRet(false,"未成功取消该会议");exit;
		}
	}

	/**
	 * 在线会议——讲者启动会议
	 * http://cdma.local/apiaspirinonline/speaker-open?timeid=4&token=
	 */
	public function getSpeakerOpen()
	{
		$iOnlineTimeId = intval(Input::get('timeid'));
		$oSpeakerInfo = AspirinOnlineSpeaker::where('online_time_id',$iOnlineTimeId)
			->where('speaker_id',$this->iUserId)
			->where('cancel_flag',0)
			->first();
		//会议时间
		$oOnlineTime = AspirinOnlineTime::find($iOnlineTimeId);
		$sNowTime = date('Y-m-d H:i',time());
		$sStartTime = substr($oOnlineTime->online_date,0,10).' '.substr($oOnlineTime->time_period,0,5);
		$sEndTime = substr($oOnlineTime->online_date,0,10).' '.substr($oOnlineTime->time_period,6,5);
		//讲者可提前10分钟启动会议
		$sStart = date('Y-m-d H:i',(strtotime($sStartTime)-self::ADVANCETIME*60));
		if($oSpeakerInfo->listener_count < self::LIMIT_CANCEL_COUNT && $sNowTime > $sStart && $sNowTime < $sStartTime){
			echo  $this->formatRet(false,"您的会议不足".self::LIMIT_CANCEL_COUNT."人参加，暂不能启动会议");exit;
		}
		if($oSpeakerInfo->listener_count < self::LIMIT_CANCEL_COUNT && $sNowTime >= $sStartTime){
			//会议开始，不足5人，取消
			$oSpeakerInfo->cancel_flag = 1;
			$oSpeakerInfo->save();
			if(AspirinOnlineSpeaker::getCancel($oSpeakerInfo->id,$iOnlineTimeId,$this->iUserId)){
				echo  $this->formatRet(false,"您的会议因不足".self::LIMIT_CANCEL_COUNT."人已取消会议。");exit;
			}
		}
		$oMeetingLog = MeetingApiLog::where('online_id',$oSpeakerInfo->id)->first();
		if($oMeetingLog){
			//开启标志置为1
			$oSpeakerInfo->open_flag=1;
			$oSpeakerInfo->save();
				
			$sUrl = $oMeetingLog->hoststarturl;
			$aUrl = array('url'=>$sUrl);
			echo $this->formatRet(true,"成功","success",$aUrl);exit;
		}else{
			echo  $this->formatRet(false,"启动失败");exit;
		}
	}

	/**
	 * 在线会议——讲者启动会议---会畅新接口
	 * http://cdma.local/apiaspirinonline/speaker-open-v2?timeid=4&token=
	 */
	public function getSpeakerOpenV2()
	{
		$iOnlineTimeId = intval(Input::get('timeid'));
		$oSpeakerInfo = AspirinOnlineSpeaker::where('online_time_id',$iOnlineTimeId)
			->where('speaker_id',$this->iUserId)
			->where('cancel_flag',0)
			->first();
		//会议时间
		$oOnlineTime = AspirinOnlineTime::find($iOnlineTimeId);
		$sNowTime = date('Y-m-d H:i',time());
		$sStartTime = substr($oOnlineTime->online_date,0,10).' '.substr($oOnlineTime->time_period,0,5);
		$sEndTime = substr($oOnlineTime->online_date,0,10).' '.substr($oOnlineTime->time_period,6,5);
		//讲者可提前10分钟启动会议
		$sStart = date('Y-m-d H:i',(strtotime($sStartTime)-self::ADVANCETIME*60));
		if($oSpeakerInfo->listener_count < self::LIMIT_CANCEL_COUNT && $sNowTime > $sStart && $sNowTime < $sStartTime){
			echo $this->formatRet(false,"您的会议不足".self::LIMIT_CANCEL_COUNT."人参加，暂不能启动会议");exit;
		}
		if($oSpeakerInfo->listener_count < self::LIMIT_CANCEL_COUNT && $sNowTime >= $sStartTime){
			//会议开始，不足10人，取消
			$oSpeakerInfo->cancel_flag = 1;
			$oSpeakerInfo->save();
			if(AspirinOnlineSpeaker::getCancel($oSpeakerInfo->id,$iOnlineTimeId,$this->iUserId)){
				echo  $this->formatRet(false,"您的会议因不足".self::LIMIT_CANCEL_COUNT."人已取消会议。");exit;
			}
		}
		$oMeetingLog = MeetingApiLog::where('online_id',$oSpeakerInfo->id)->orderBy('id','desc')->first();
		// print_r($oMeetingLog->toArray());DIE;
		// $log = DB::getQueryLog();
		// print_r($log);die;
		if($oMeetingLog){
			//开启标志置为1
			$oSpeakerInfo->open_flag=1;
			$oSpeakerInfo->save();
			$sName = User::where('id',$this->iUserId)->pluck('user_name');
			$aArr = array(
				'userId'  => $oMeetingLog->userid,
				'username'=> $sName,
				'Token'   => $oMeetingLog->token,
				'meetingNo' => $oMeetingLog->meetingno
			);
			echo $this->formatRet(true,"成功",$aArr,"success");exit;
		}else{
			echo  $this->formatRet(false,"启动失败");exit;
		}
	}
	
	/**
	 * 邀请医生     post提交
	 * http://cdma.local/apiaspirinonline/invite-submit?timeid=1&sDoctorInfo=[{"id":13108,"doctorname":"王玉祥"},{"id":13152,"doctorname":"邓佳"}]&token=
	 */
	public function postInviteSubmit()
	{
		if(!Input::has('sDoctorInfo')||!Input::has('timeid')){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$iOnlineTimeId = intval(Input::get('timeid'));
		$sDoctorInfo = Input::get('sDoctorInfo');
		$aDoctorInfo = json_decode($sDoctorInfo,true);
        $num = Config::get('config.self_help_meeting_attent_set');
		if(count($aDoctorInfo) > $num){
			echo  $this->formatRet(false,"您最多可邀请{$num}名医生参会");exit;
		}
		$sName  = User::where('id',$this->iUserId)->pluck('user_name');
		$oOnlineTime = AspirinOnlineTime::find($iOnlineTimeId);
		$oOnline = AspirinOnline::find($oOnlineTime->online_id);
		$sOnlineTitle = $oOnline->online_title;
		foreach($aDoctorInfo as $k=>$v){
			$aInvite = array(
				'online_time_id'=> $iOnlineTimeId,
				'speaker_id'    => $this->iUserId,
				'listener_id'   => $v['id'],
				'join_type'     => 1,
				'created_at'    => date('Y-m-d H:i:s',time())
			);
			$oInvite = new AspirinOnlineListener($aInvite);
			$oInvite->save();
			//发送消息通知
			$sPushContent = PushService::ONLINE_SPEAKER_INVITED;
			$sPushContent = str_replace('{{username}}', $sName, $sPushContent);
			$sPushContent = str_replace('{{title}}', $sOnlineTitle, $sPushContent);
			PushService::postClient('国卫健康云',$sPushContent,12,$v['id']);
		}
		echo  $this->formatRet(true,"邀请成功");exit;
	}
	
	/**
	 * 邀请医生列表
	 * http://cdma.local/apiaspirinonline/invite-list?timeid=1&page=1&pagesize=10&token=
	 */
	public function getInviteList()
	{
		$iPage = Input::get('page',1);
		$iPagesize = Input::get('pagesize',self::PAGESIZE);
		$iOnlineTimeId = intval(Input::get('timeid'));
		$oInvite = AspirinOnlineListener::where('online_time_id',$iOnlineTimeId)
			->where('speaker_id',$this->iUserId)
			// ->where('join_type',1)
			->orderBy('join_type','desc')
			->orderBy('created_at','desc')
			->skip(($iPage-1)*$iPagesize)
			->take($iPagesize)
			->get();
		$aInviteList = array();
		foreach($oInvite as $k=>$v){
			$aInvite = array();
			$oUser = User::select('user_name','user_company','user_company_name')->where('id',$v->listener_id)->first();
			if($oUser){
				$aInvite['doc_name'] = $oUser->user_name;
				$sUserCompany = $oUser->user_company;
				if(is_numeric($sUserCompany)&&$sUserCompany!=0){
					$sUserCompany = Hospital::where('id',$sUserCompany)->pluck('name');
				}
				if(!$sUserCompany){
					$sUserCompany = $oUser->user_company_name;
				}
				$aInvite['doc_hospital'] = $sUserCompany;
			}
			$aInvite['invite_id'] = $v->id;
			$aInvite['join_type'] = $v->join_type;
			$aInviteList[] = $aInvite;
		}
		echo $this->formatRet(true,"成功","success",$aInviteList);exit;
	}

	/**
	 * 删除邀请医生
	 * http://cdma.local/apiaspirinonline/invite-delete?id=1&token=
	 */
	public function getInviteDelete()
	{
		$iInviteId = intval(Input::get('id'));
		$oInvite = AspirinOnlineListener::find($iInviteId);
		if($oInvite){
			$oInvite->delete();
		}
		echo $this->formatRet(true,"删除成功");exit;
	}

    /**
     * 获取自主会议当前季度主题
     * @url /apiaspirinonline/meeting-subject?catid=1&token=
     */
	public function getMeetingSubject()
    {
        $iCatId = Input::get('catid');

        $aOnlineId = AspirinOnline::where('catid',$iCatId)->lists('id');
        if(empty($aOnlineId)){
            exit(
                $this->formatRet(false,"没有可报名会议","")
            );
        }

        $aOnlineSubject = array();
        $oInfo = AspirinOnline::where('catid', $iCatId);

        //本季度起始和结束日期
        $season = ceil(date('n') /3);
        $start_date = date('Y-m-01 00:00:00', mktime(0,0,0,($season - 1) *3 +1,1, date('Y')));
        $end_date = date('Y-m-t 23:59:59', mktime(0,0,0,$season * 3,1, date('Y')));

        $oInfo = $oInfo->where(function($query) use ($start_date, $end_date){
            $query->where('show_start_time','>=',"{$start_date}")->orWhere('show_end_time','<=',"{$end_date}");
        });

        //主题搜索
        $oInfo = $oInfo->get();
        foreach ($oInfo as $item) {
            $aOnlineSubject[$item['id']] = [
                'ppt_id' => $item['id'],
                'ppt_title' => $item['online_title'],
            ];
        }

        exit(
            $this->formatRet(true,"ok", array_values($aOnlineSubject))
        );
    }

	/***********************************讲者报名参会************************/
	/***********************************听者报名听会************************/
	/**
	 * 在线会议——听者报名参会列表页
	 * http://cdma.local/apiaspirinonline/join-list?catid=1&token=
	 */
	public function getJoinList()
	{
		$iCatId = Input::get('catid');
		if(!in_array($iCatId, [1,2,3,4])){
			echo $this->formatRet(false,"未传科室分类参数","");exit;
		}
		$aOnlineId = AspirinOnline::where('catid',$iCatId)->lists('id');
		if(empty($aOnlineId)){
			echo $this->formatRet(false,"没有可报名会议","");exit;
		}
		//显示本周及下周同城市讲者报名的会议
		$sThisMon = date('Y-m-d',(time()-((date('w')==0?7:date('w'))-1)*24*3600)).' 00:00:00';
		$sNextSun = date('Y-m-d',strtotime($sThisMon)+13*24*3600).' 23:59:59';
		$oUser = User::find($this->iUserId);
		$iCity = $oUser->user_city;
		//获取所有同城市讲者数组 (去除同城市限制)
		$aPos = User::getPosition();
		$aPos = array($aPos[2],$aPos[3],$aPos[4]);
		$aUser = User::where('id','!=',$this->iUserId)
            //->where('user_city',$iCity) // 参会列表只显示当前账号所属城市下可报名参会的会议列表（取消）
			->where(function($query) use($aPos){
				$query->where('role_id',3)->where('card_auth_flag',2)->whereIn('user_position',$aPos)->orWhere('role_id',2);
			})->lists('id','user_name');
		/*$aUser = User::where('role_id',3)->where('user_city',$iCity)->where('card_auth_flag',2)
			->whereIn('user_position',$aPos)
			->where('id','!=',$this->iUserId)
			->lists('id','user_name');*/
		$aOnlineInfo = array();
		$oInfo = AspirinOnlineSpeaker::leftJoin('aspirin_online_time','aspirin_online_time.id','=','aspirin_online_speaker.online_time_id')
			->leftJoin('aspirin_online_ppt','aspirin_online_ppt.id','=','aspirin_online_speaker.ppt_id')
			->select('aspirin_online_speaker.*','aspirin_online_time.online_date','aspirin_online_time.time_period','aspirin_online_time.speaker_count','aspirin_online_ppt.ppt_title')
			->where('cancel_flag',0)
			->whereIn('aspirin_online_speaker.online_id',$aOnlineId)
			->orderby('online_date','asc')
			->orderby('created_at','asc');
		//主题搜索
		if(Input::get('title')){
			$title = Input::get('title');
			$oInfo = $oInfo->where('aspirin_online_ppt.ppt_title','like', "%{$title}%");
		}
		//开会日期搜索
		$datastr = Input::get('datestr');
		if($datastr){
			$sThisMon = date('Y-m-d 00:00:00', strtotime($datastr));
			$sNextSun = date('Y-m-d 23:59:59', strtotime($datastr));
		}
		$oInfo = $oInfo->where('online_date','>=',"{$sThisMon}");
		$oInfo = $oInfo->where('online_date','<=',"{$sNextSun}");
		$oInfo = $oInfo->get();
		// $log = DB::getQueryLog();
		// print_r($log);die;
		foreach($oInfo as $key=>$val){
			$iBtnFlag = 0;
			$sNowTime = date('Y-m-d H:i',time());
			//判断是否已报名或已同意某讲者会议
			$oListenerLog = AspirinOnlineListener::where('online_time_id',$val->online_time_id)
				->where('speaker_id',$val->speaker_id)
				->where('listener_id',$this->iUserId)
				->where(function($query){
					$query->where('join_type',0)
					->orWhere('is_agree',1);
				})->first();
			if(count($oListenerLog)){
				$sStartTime = substr($val->online_date,0,10).' '.substr($val->time_period,0,5);
				if($sNowTime < $sStartTime){
					$iBtnFlag = 3;        //已报名，未开始
				}else{
					$iBtnFlag = 2;        //已报名、可参加(包含已报名、已同意,不可再次报名的情况，按钮显示为可点的“参加会议”)
				}
			}else{
				if($val->listener_count == Config::get('config.self_help_meeting_attent_set')){
					$iBtnFlag = 1;        //人数已满(已报名并且人数已满的情况按人满处理)
				}
			}
			$aRes = array();
			$sEndTime = substr($val->online_date,0,10).' '.substr($val->time_period,6,5);
			if(in_array($val->speaker_id,$aUser) && $sNowTime < $sEndTime){
				$oDoctorInfo = User::find($val->speaker_id);
				if(!$oDoctorInfo->user_company){
					$sHospital = $oDoctorInfo->user_company_name;
				}else{
					$sHospital = Hospital::where('id',$oDoctorInfo->user_company)->pluck('name');
				}
				$aRes[] = array(
					'speaker_id'       => $val->speaker_id,
					'doc_name'         => $oDoctorInfo->user_name,
					'doc_department'   => $oDoctorInfo->user_department,
					'doc_hospital'     => $sHospital,
					'listener_count'   => $val->listener_count,
					'listener_limit'   => Config::get('config.self_help_meeting_attent_set'),
					'btn_flag'         => $iBtnFlag
				);
			}
			if($aRes){
				$aOnlineInfo[] = array(
					'ppt_id'        => $val->ppt_id,
					'online_title'  => $val->ppt_title,
					'online_time_id'=> $val->online_time_id,
					'online_date'   => date('Y年m月d日',strtotime($val->online_date)),
					'time_period'   => $val->time_period,
					'speaker_info'  => $aRes
				);
			}
		}
		//同一主题同一时间段的讲者合并显示
		$resultArr = array();
		if($aOnlineInfo){
			$aOnlineInfoCount = count($aOnlineInfo);
			for($i=0;$i<$aOnlineInfoCount;$i++){
				$flag = 0;
				for($j=$i;$j<$aOnlineInfoCount;$j++){
					if($i!=$j&& !empty($aOnlineInfo[$i]) && !empty($aOnlineInfo[$j])&& $aOnlineInfo[$i]['ppt_id'] == $aOnlineInfo[$j]['ppt_id'] && $aOnlineInfo[$i]['online_time_id'] == $aOnlineInfo[$j]['online_time_id']){
						$aRes1 = array();
						if(count($aOnlineInfo[$i]['speaker_info'])==1){
							$aRes1[] = $aOnlineInfo[$i]['speaker_info'][0];
							$aRes1[] = $aOnlineInfo[$j]['speaker_info'][0];
						}else{
							$aRes1 = $aOnlineInfo[$i]['speaker_info'];
							$aRes1[] = $aOnlineInfo[$j]['speaker_info'][0];
						}
						$aOnlineInfo[$i]['speaker_info'] = $aRes1;
						//unset($aOnlineInfo[$j]);
				    	$aOnlineInfo[$j] = array();
						$flag = 1;
					}
				}
				if(!empty( $aOnlineInfo[$i])){
					$resultArr[] = $aOnlineInfo[$i];
				}
			}
		}
		echo $this->formatRet(true,"成功","success",$resultArr);exit;
	}

	/**
	 * 在线会议——听者报名参会   post提交
	 * http://cdma.local/apiaspirinonline/listener-enter?timeid=4&token=
	 */
	public function postListenerEnter()
	{
		$iOnlineTimeId = intval(Input::get('timeid'));
		$oOnlineTime = AspirinOnlineTime::find($iOnlineTimeId);
		$sOnlineTime = substr($oOnlineTime->online_date,0,11).substr($oOnlineTime->time_period,0,5).':00';
		$sOnlineEndTime = substr($oOnlineTime->online_date,0,11).substr($oOnlineTime->time_period,6,5);
		$sThisSun = date('Y-m-d',(time()+(7-(date('w')==0?7:date('w')))*24*3600)).' 23:59:59';
		$sNowTime = date('Y-m-d H:i',time());
		//听者只限报当周的会议，下周会议可看不可报
		if($sOnlineTime > $sThisSun){
			echo  $this->formatRet(false,"请在本周日24点以后至会议开始前报名此会，谢谢！");exit;
		}else{
			if($sNowTime > $sOnlineTime && $sNowTime <= $sOnlineEndTime){
				echo  $this->formatRet(false,"报名结束，会议已开始");exit;
			}
			if($sNowTime > $sOnlineEndTime){
				echo  $this->formatRet(false,"会议已结束");exit;
			}
			$oLog = AspirinOnlineListener::where('online_time_id',$iOnlineTimeId)->where('listener_id',$this->iUserId)->where(function($query){
				$query->where('join_type',0)->orWhere('is_agree',1);
			})->first();
			if(count($oLog)){
				echo  $this->formatRet(false,"同一时间段只允许报名一位专家的会议，谢谢！");exit;
			}
			echo $this->formatRet(true,"success");exit;
		}
	}

	/**
	 * 在线会议——听者报名确认   post提交
	 * http://cdma.local/apiaspirinonline/listener-submit?timeid=4&speakerid=4849&token=
	 */
	public function postListenerSubmit(){
		$iOnlineTimeId = intval(Input::get('timeid'));
		$iSpeakerId = intval(Input::get('speakerid'));
		//判断是否已报名该讲者会议
		$oListenerLog = AspirinOnlineListener::where('online_time_id',$iOnlineTimeId)
			->where('speaker_id',$iSpeakerId)
			->where('listener_id',$this->iUserId)
			->where('join_type',0)
			->first();
		if(count($oListenerLog)){
			echo  $this->formatRet(false,"您已报名该讲者会议");exit;
		}else{
            $applycount = AspirinOnlineListener::where("online_time_id",$iOnlineTimeId)->get()->count();
            // 参会人数大于24时表示人数已满
            if($applycount>=24){
                echo $this->formatRet(false,"参会人数已满");exit;
            }
			//判断是否有讲者邀请，如果有，报名时直接更改为同意状态
			$oInviteLog = AspirinOnlineListener::where('online_time_id',$iOnlineTimeId)
				->where('speaker_id',$iSpeakerId)
				->where('listener_id',$this->iUserId)
				->where('join_type',1)
				->first();
			if(count($oInviteLog)){
				$oInviteLog->is_agree = 1;
				$oInviteLog->save();
			}else{
				$aListenerLog = array(
					'online_time_id' => $iOnlineTimeId,
					'speaker_id'     => $iSpeakerId,
					'listener_id'    => $this->iUserId,
					'join_type'      => 0,
					'created_at'     => date('Y-m-d H:i:s',time())
				);
				$oListener = new AspirinOnlineListener($aListenerLog);
				$oListener->save();
			}
			//听者总数加1
			$oSpeakerInfo = AspirinOnlineSpeaker::where('online_time_id',$iOnlineTimeId)
				->where('speaker_id',$iSpeakerId)
				->where('cancel_flag',0)->first();
			$oSpeakerInfo->increment('listener_count');
			// print_r($oSpeakerInfo);die;
			//发送消息通知
			$sName  = User::where('id',$this->iUserId)->pluck('user_name');
			$oOnlineTime = AspirinOnlineTime::find($iOnlineTimeId);
			$oOnline = AspirinOnline::find($oOnlineTime->online_id);
			$sOnlineTitle = $oOnline->online_title;
			$sPushContent = PushService::ONLINE_LISTENER_JOIN;
			$sPushContent = str_replace('{{username}}', $sName, $sPushContent);
			$sPushContent = str_replace('{{title}}', $sOnlineTitle, $sPushContent);
			PushService::postClient('国卫健康云',$sPushContent,11,$iSpeakerId,$oOnline->catid);
			//提交成功返回信息
			$aInfo = AspirinOnlineListener::getJoinInfo($iOnlineTimeId, $iSpeakerId);
			//统计报名数量
			$applycount = AspirinOnlineListener::where("online_time_id",$iOnlineTimeId)->get()->count();
			// 参会人数10人更为最大限制为24人
			if($applycount>=10){
				/*$aNotice = array(
					'user_id'		=> $iSpeakerId,
					'notice_content'=> "已有10人报名参加了您于".date("Y年m月d日 ", strtotime($oOnlineTime->online_date))."{$oOnlineTime->time_period}的课程，特此邀您签署《讲师讲课服务协议》",
					'notice_type'	=> 160,
					'detail_id'		=> $iOnlineTimeId
				);
				$oNotice = new AspirinUserNotice($aNotice);
				$oNotice->save();*/
			}
			// 参会者每个主题积10分，不重复积分 健康云积分操作
            $IntegralLog = new IntegralLog();
            if (!$IntegralLog->where('user_id',$this->iUserId)
                ->where('obj_id', $iSpeakerId)
                ->where('obj_type', 'class')
                ->where('types', 'in')->count()) {
                $IntegralLog->updateIntegral($this->iUserId,$iSpeakerId,'class');
            }

			echo $this->formatRet(true,"成功","success",$aInfo);exit;
		}
	}

	/**
	 * 在线会议——听者参加会议
	 * http://cdma.local/apiaspirinonline/listener-attend?timeid=4&speakerid=4849&token=
	 */
	public function getListenerAttend()
	{
		$iOnlineTimeId = intval(Input::get('timeid'));
		$iSpeakerId = intval(Input::get('speakerid'));
		$oOnlineTime = AspirinOnlineTime::find($iOnlineTimeId);
		//会议时间
		$sNowTime = date('Y-m-d H:i',time());
		$sStartTime = substr($oOnlineTime->online_date,0,10).' '.substr($oOnlineTime->time_period,0,5);
		$sEndTime = substr($oOnlineTime->online_date,0,10).' '.substr($oOnlineTime->time_period,6,5);
		if($sNowTime < $sStartTime){
			echo  $this->formatRet(false,"该会议还未开始");exit;
		}elseif($sNowTime > $sEndTime){
			echo  $this->formatRet(false,"该会议已结束");exit;
		}else{
			$oSpeakerInfo = AspirinOnlineSpeaker::where('online_time_id',$iOnlineTimeId)
				->where('speaker_id',$iSpeakerId)
				->orderby('created_at','desc')
				->first();
			if($oSpeakerInfo->cancel_flag == 1){
				echo  $this->formatRet(false,"该会议已经取消");exit;
			}
			if($oSpeakerInfo->open_flag != 1){
				echo  $this->formatRet(false,"该会议还未启动");exit;
			}
			$oMeetingLog = MeetingApiLog::where('online_id',$oSpeakerInfo->id)->first();
			if($oMeetingLog){
				//更改为已参加
				$oAttendLog = AspirinOnlineListener::where('online_time_id',$iOnlineTimeId)
					->where('speaker_id',$iSpeakerId)
					->where('listener_id',$this->iUserId)
					->first();
				$oAttendLog->has_attend = 1;
				$oAttendLog->save();
				$sUrl = $oMeetingLog->joinurl;
				$aUrl = array('url'=>$sUrl);
				echo $this->formatRet(true,"成功","success",$aUrl);exit;
			}else{
				echo  $this->formatRet(false,"进入会议失败");exit;
			}
		}
	}

	/**
	 * 在线会议——听者参加会议----会畅新接口
	 * http://cdma.local/apiaspirinonline/listener-attend-v2?timeid=4&speakerid=4849&token=
	 */
	public function getListenerAttendV2()
	{
		$iOnlineTimeId = intval(Input::get('timeid'));
		$iSpeakerId = intval(Input::get('speakerid'));
		$oOnlineTime = AspirinOnlineTime::find($iOnlineTimeId);
		//会议时间
		$sNowTime = date('Y-m-d H:i',time());
		$sStartTime = substr($oOnlineTime->online_date,0,10).' '.substr($oOnlineTime->time_period,0,5);
		$sEndTime = substr($oOnlineTime->online_date,0,10).' '.substr($oOnlineTime->time_period,6,5);
		if($sNowTime < $sStartTime){
			echo  $this->formatRet(false,"该会议还未开始");exit;
		}elseif($sNowTime > $sEndTime){
			echo  $this->formatRet(false,"该会议已结束");exit;
		}else{
			$oSpeakerInfo = AspirinOnlineSpeaker::where('online_time_id',$iOnlineTimeId)
				->where('speaker_id',$iSpeakerId)
				->orderby('created_at','desc')
				->first();
			if(!$oSpeakerInfo){
				echo  $this->formatRet(false,"会议查询失败");exit;
			}
			if($oSpeakerInfo->cancel_flag == 1){
				echo  $this->formatRet(false,"该会议已经取消");exit;
			}
			if($oSpeakerInfo->open_flag != 1){
				echo  $this->formatRet(false,"该会议还未启动");exit;
			}
			$oMeetingLog = MeetingApiLog::where('online_id',$oSpeakerInfo->id)->orderBy('id', 'desc')->first();
			if($oMeetingLog){
				//更改为已参加
				$oAttendLog = AspirinOnlineListener::where('online_time_id',$iOnlineTimeId)
					->where('speaker_id',$iSpeakerId)
					->where('listener_id',$this->iUserId)
					->first();
				$oAttendLog->has_attend = 1;
				$oAttendLog->save();
				$sName = User::where('id',$this->iUserId)->pluck('user_name');
				$aArr = array(
					'userName'  => $sName,
					'meetingNo' => $oMeetingLog->meetingno,
					'hostkey'   => $oMeetingLog->hostkey
				);
				echo $this->formatRet(true,"成功","success",$aArr);exit;
			}else{
				echo  $this->formatRet(false,"进入会议失败");exit;
			}
		}
	}

	/**
	 * 在线会议——听者取消会议
	 * http://cdma.local/apiaspirinonline/listener-cancel?timeid=5&speakerid=1503&token=
	 */
	public function getListenerCancel()
	{
		$iOnlineTimeId = intval(Input::get('timeid'));
		$iSpeakerId = intval(Input::get('speakerid'));
		$oListener = AspirinOnlineListener::where('online_time_id',$iOnlineTimeId)
			->where('speaker_id',$iSpeakerId)
			->where('listener_id',$this->iUserId)
			->first();
		if(count($oListener)){
			$sName  = User::where('id',$this->iUserId)->pluck('user_name');
			$oOnlineTime = AspirinOnlineTime::find($iOnlineTimeId);
			$oOnline = AspirinOnline::find($oOnlineTime->online_id);
			$sOnlineTitle = $oOnline->online_title;
			$sPushContent = PushService::ONLINE_LISTENER_CANCEL;
			$sPushContent = str_replace('{{username}}', $sName, $sPushContent);
			$sPushContent = str_replace('{{title}}', $sOnlineTitle, $sPushContent);
			PushService::postClient('国卫健康云',$sPushContent,11,$iSpeakerId);
			$oOnlineSpeaker = AspirinOnlineSpeaker::where('online_time_id',$iOnlineTimeId)
				->where('speaker_id',$iSpeakerId)
				->where('cancel_flag',0)
				->first();
			if($oOnlineSpeaker && $oOnlineSpeaker->listener_count != 0){
				$oOnlineSpeaker->listener_count = $oOnlineSpeaker->listener_count - 1;
				$oOnlineSpeaker->save();
			}
			$oListener->delete();
			echo $this->formatRet(true,"取消成功","success");exit;
		}else{
			echo  $this->formatRet(false,"取消失败");exit;
		}
	}
	/***********************************听者报名听会************************/
	


	/***********************************个人中心-在线会议************************/
	/**
	 * 个人中心-我的会议-主讲会议
	 * http://cdma.local/apiaspirinonline/my-enter-list?token=
	 */
	public function getMyEnterList()
	{
		//只取主讲记录里最新的一条显示，过期的会议不再展示
		$oInfo = AspirinOnlineSpeaker::leftJoin('aspirin_online_time','aspirin_online_time.id','=','aspirin_online_speaker.online_time_id')
			->select('aspirin_online_speaker.speaker_id','aspirin_online_speaker.cancel_flag','aspirin_online_speaker.ppt_id','aspirin_online_time.*')
			->where('speaker_id',$this->iUserId)
			->where('cancel_flag',0)
			->orderby('aspirin_online_time.id','desc')
			// ->orderby('online_date','desc')
			->first();
		$aInfo = array();
		if($oInfo){
			$oOnlineInfo = AspirinOnline::find($oInfo->online_id);
			$oPpt = AspirinOnlinePpt::find($oInfo->ppt_id);
			//会议时间
			$sNowTime = date('Y-m-d H:i',time());
			$sStartTime = substr($oInfo->online_date,0,10).' '.substr($oInfo->time_period,0,5);
			$sEndTime = substr($oInfo->online_date,0,10).' '.substr($oInfo->time_period,6,5);
			//讲者可提前10分钟启动会议
			$sStart = date('Y-m-d H:i',(strtotime($sStartTime)-self::ADVANCETIME*60));
			if($sNowTime < $sStart){
				$iCancelFlag = 0;        //取消按钮可点
				$iOpenFlag = 1;          //启动会议不可点
				$iInviteFlag = 0;        //邀请医生可点
			}elseif($sNowTime > $sEndTime){
				$iCancelFlag = 2;        //取消按钮不可点,变成“会议结束”
				$iOpenFlag = 1;          //启动会议不可点
				$iInviteFlag = 1;        //邀请医生不可点
			}else{
				$iCancelFlag = 1;        //取消按钮不可点
				$iOpenFlag = 0;          //启动会议可点
				$iInviteFlag = 1;        //邀请医生不可点
			}
			$aInfo[] = array(
				'online_catid'  => @$oOnlineInfo->catid,
				'online_time_id'=> $oInfo->id,
				'online_date'  => date('Y年m月d日',strtotime($oInfo->online_date)),
				'time_period'  => $oInfo->time_period,
				'online_title' => $oPpt->ppt_title,
				'cancel_type'  => $iCancelFlag,
				'open_type'    => $iOpenFlag,
				'invite_type'  => $iInviteFlag,
				'ppt_show_url' => Config::get('app.url').'/mobile/ppt/'.$oInfo->ppt_id
			); 
		}
		echo $this->formatRet(true,"成功","success",$aInfo);exit;
	}
	
	/**
	 * 个人中心-我的会议-参加会议
	 * http://cdma.local/apiaspirinonline/my-attend-list?token=
	 */
	public function getMyAttendList()
	{
		//参加的会议
		$aInfo1 =array();
		$oListenerInfo = AspirinOnlineListener::where('listener_id',$this->iUserId)
			->where('join_type',0)
			->orderby('created_at','asc')
			->get();
		foreach($oListenerInfo as $k=>$v){
			$aInfo1[] = AspirinOnlineListener::getJoinInfo($v->online_time_id, $v->speaker_id);
		}
		//已同意的会议
		$aInfo2 =array();
		$oAgreeInfo = AspirinOnlineListener::where('listener_id',$this->iUserId)
			->where('join_type',1)
			->where('is_agree',1)
			->orderby('created_at','asc')
			->get();
		foreach($oAgreeInfo as $k=>$v){
			$aInfo2[] = AspirinOnlineListener::getJoinInfo($v->online_time_id, $v->speaker_id);
		}
		//所有可参加的会议
		$aInfo1 = array_filter($aInfo1);
		$aInfo2 = array_filter($aInfo2);
		$aAttendInfo = array_merge($aInfo1,$aInfo2);
		
		//被邀请的会议
		$aInfo3 =array();
		$oInviteInfo = AspirinOnlineListener::where('listener_id',$this->iUserId)
			->where('join_type',1)
			->where('is_agree',0)
			->orderby('created_at','asc')
			->get();
		foreach($oInviteInfo as $k=>$v){
			$aInfo3[] = AspirinOnlineListener::getInviteInfo($v->online_time_id, $v->speaker_id,0);
		}
		//忽略的会议
		$aInfo4 =array();
		$oIgnoreInfo = AspirinOnlineListener::where('listener_id',$this->iUserId)
			->where('join_type',1)
			->where('is_agree',2)
			->orderby('created_at','asc')
			->get();
		foreach($oIgnoreInfo as $k=>$v){
			$aInfo4[] = AspirinOnlineListener::getInviteInfo($v->online_time_id, $v->speaker_id,1);
		}
		//所有被邀请的会议
		$aInfo3 = array_filter($aInfo3);
		$aInfo4 = array_filter($aInfo4);
		$aInviteInfo = array_merge($aInfo3,$aInfo4);
		echo $this->formatRet(true,"成功",$aAttendInfo,$aInviteInfo);exit;
	}

	/**
	 * 个人中心-我的会议-同意会议
	 * http://cdma.local/apiaspirinonline/agree-attend?timeid=4&speakerid=4849&token=
	 */
	public function getAgreeAttend()
	{
		$iOnlineTimeId = intval(Input::get('timeid'));
		$iSpeakerId = intval(Input::get('speakerid'));
		$oInfo = AspirinOnlineListener::where('online_time_id',$iOnlineTimeId)
			->where('speaker_id',$iSpeakerId)
			->where('listener_id',$this->iUserId)
			->where('join_type',1)
			->first();
		if(count($oInfo)){
			$oInfo->is_agree = 1;
			$oInfo->save();
			$oCountLog = AspirinOnlineSpeaker::where('online_time_id',$iOnlineTimeId)->where('speaker_id',$iSpeakerId)->first();
			if(count($oCountLog)){
				$oCountLog->increment('listener_count');
			}
			//统计报名数量
			$oSpeakerInfo = AspirinOnlineSpeaker::where('online_time_id',$iOnlineTimeId)
				->where('speaker_id',$iSpeakerId)
				->where('cancel_flag',0)->first();
			$oOnlineTime = AspirinOnlineTime::find($iOnlineTimeId);
			$applycount = AspirinOnlineListener::where("online_time_id",$iOnlineTimeId)->get()->count();
			if($applycount>=5){
				/*$aNotice = array(
					'user_id'		=> $iSpeakerId,
					'notice_content'=> "已有5人报名参加了您于".date("Y年m月d日 ", strtotime($oOnlineTime->online_date))."{$oOnlineTime->time_period}的课程，特此邀您签署《讲师讲课服务协议》",
					'notice_type'	=> 160,
					'detail_id'		=> $iOnlineTimeId
				);
				$oNotice = new AspirinUserNotice($aNotice);
				$oNotice->save();*/
			}
			echo $this->formatRet(true,"success");exit;
		}else{
			echo  $this->formatRet(false,"不存在该会议邀请");exit;
		}
	}

	/**
	 * 个人中心-我的会议-忽略会议
	 * http://cdma.local/apiaspirinonline/ignore-attend?timeid=4&speakerid=4849&token=
	 */
	public function getIgnoreAttend()
	{
		$iOnlineTimeId = intval(Input::get('timeid'));
		$iSpeakerId = intval(Input::get('speakerid'));
		$oInfo = AspirinOnlineListener::where('online_time_id',$iOnlineTimeId)
			->where('speaker_id',$iSpeakerId)
			->where('listener_id',$this->iUserId)
			->where('join_type',1)
			->first();
		if(count($oInfo)){
			$oInfo->is_agree = 2;
			$oInfo->save();
			echo $this->formatRet(true,"success");exit;
		}else{
			echo  $this->formatRet(false,"不存在该会议邀请");exit;
		}
	}


	/***********************************个人中心-在线会议************************/
	/***********************************精彩回顾************************/
	/**
	 * 精彩回顾列表页
	 * http://cdma.local/apiaspirinonline/video-list?catid=1&page=1&pagesize=10&stitle=&sname=&token=
	 */
	public function getVideoList()
	{
		$iPage = Input::get('page',1);
		$iPagesize = Input::get('pagesize',self::PAGESIZE);
		$sTitle = Input::get('stitle','');
		$sName = Input::get('sname','');
		$iCatId = Input::get('catid', 3);
        if (0 == $iCatId) {
            $iCatId = 3;
        }
		//获取精彩回顾列表
		$aVideoList = AspirinOnlineSpeaker::getVideoList($iCatId,$sTitle,$sName,$iPage,$iPagesize);

		echo $this->formatRet(true,"成功","success",$aVideoList);exit;
	}

	/**
	 * 精彩回顾详细页
	 * http://cdma.local/apiaspirinonline/video-detail?id=1&page=1&pagesize=5&token=
	 */
	public function getVideoDetail(){
		$iPage = Input::get('page',1);
		$iPagesize = Input::get('pagesize',self::PAGESIZE);
		$iVideoId = Input::get('id',0);
		$oVideo = AspirinOnlineSpeaker::find($iVideoId);
		if(!$oVideo){
			echo $this->formatRet(false,"不存在该记录");exit;
		}
		//浏览量+1
		$oVideo->increment('video_hits');
		$oOnline = AspirinOnline::find($oVideo->online_id);
		$aVideo = array(
			'video_id'       => $iVideoId,
			'online_title'   => $oOnline->online_title,
			'online_banner'  => Config::get('app.url').$oOnline->online_banner,
			'video_url'      => $oVideo->video_url,
			'video_hits'     => $oVideo->video_hits,
			'video_zan'      => $oVideo->video_zan,
			'has_zan'		 => AspirinSupportLog::hasSupport(self::CATID, $this->iUserId, $iVideoId)
		);
		//专家信息
		$oUser = User::select('user_name','user_company','user_company_name')->where('id',$oVideo->speaker_id)->first();
		$aVideo['doc_name'] = $oUser->user_name;
		$sUserCompany = $oUser->user_company;
		if(is_numeric($sUserCompany)&&$sUserCompany!=0){
			$sUserCompany = Hospital::where('id',$sUserCompany)->pluck('name');
		}
		if(!$sUserCompany){
			$sUserCompany = $oUser->user_company_name;
		}
		$aVideo['doc_hospital'] = $sUserCompany;
		//星级评分
		$aVideo['score'] = array();
		$oScoreLog = AspirinOnlineVideoScore::where('video_id',$iVideoId)->where('user_id',$this->iUserId)->first();
		if(count($oScoreLog)){
			$aVideo['score'] = array(
				'score1'  => $oScoreLog->content_degree,
				'score2'  => $oScoreLog->class_level,
				'score3'  => $oScoreLog->overall_score
			);
		}
		//评论列表
		$oComments = AspirinOnlineComment::getComment($iVideoId,$iPage,$iPagesize);
		$aComments = array();
		foreach($oComments as $v){
			$aComments[] = array(
				'comment_id'    => $v->id,
				'user_nick'     => $v->user_nick,
				'user_thumb'    => Config::get('app.url').$v->user_thumb,
				'comment'       => str_replace('/assets/images/front/emotion/',Config::get('app.url').'/assets/images/front/emotion/',$v->comment),
				'zan_count'     => $v->zan_count,
				'created_at'    => substr($v->created_at,0,10)
			);
		}
		$aVideo['comments'] = $aComments;
		$aVideo['share_flag'] = 1;
		$aVideo['share_title'] = $oOnline->online_title;
		$aVideo['share_thumb'] = Config::get('app.url').'/assets/images/mobile/logo.png';
		$aVideo['share_url'] = Config::get('app.url').'/aspirinshare/online-show/'.$iVideoId;
		echo $this->formatRet(true,"成功","success",$aVideo);exit;
	}

	/**
	 * 精彩回顾星级评分   post提交
	 * http://cdma.local/apiaspirinonline/score?id=1&score1=1&score2=1&score3=1&token=
	 */
	public function postScore()
	{
		$iVideoId = Input::get('id',0);
		$oVideo = AspirinOnlineSpeaker::where('id',$iVideoId)->first();
		if(!$oVideo){
			echo $this->formatRet(false,"不存在该记录");exit;
		}
		$iScore1 = Input::get('score1',0);
		$iScore2 = Input::get('score2',0);
		$iScore3 = Input::get('score3',0);
		$oScoreLog = AspirinOnlineVideoScore::where('video_id',$iVideoId)->where('user_id',$this->iUserId)->first();
		if(count($oScoreLog)){
			if($iScore1 != 0){
				$oScoreLog->content_degree = $iScore1;
			}
			if($iScore2 != 0){
				$oScoreLog->class_level = $iScore2;
			}
			if($iScore3 != 0){
				$oScoreLog->overall_score = $iScore3;
			}
			$oScoreLog->save();
		}else{
			$aScore = array(
				'video_id'       => $iVideoId,
				'user_id'        => $this->iUserId,
				'content_degree' => isset($iScore1)?$iScore1:0,
				'class_level'    => isset($iScore2)?$iScore2:0,
				'overall_score'  => isset($iScore3)?$iScore3:0,
				'created_at'     => date('Y-m-d H:i:s',time())
			);
			$oScore = new AspirinOnlineVideoScore($aScore);
			$oScore->save();
		}
		echo  $this->formatRet(true,"成功","success");exit;
	}

	/**
	 * 精彩回顾评论   post提交
	 * http://cdma.local/apiaspirinonline/comment?id=1&comment=评论&token=
	 */
	public function postComment()
	{
		$iVideoId = Input::get('id',0);
		$oVideo = AspirinOnlineSpeaker::where('id',$iVideoId)->first();
		if(!$oVideo){
			echo $this->formatRet(false,"不存在该记录");exit;
		}
		$aComment = array(
			'video_id'  => $iVideoId,
			'user_id'   => $this->iUserId,
			'comment'   => Input::get('comment'),
			'created_at'=> date('Y-m-d H:i:s',time())
		);
		$oComment = new AspirinOnlineComment($aComment);
		$oComment->save();
		echo  $this->formatRet(true,'评论成功');exit;
	}

	/**
	 * 精彩回顾评论点赞   post提交
	 * http://cdma.local/apiaspirinonline/zan-comment?id=1&token=
	 */
	public function postZanComment()
	{
		$iCommentId = Input::get('id',0);
		$oComment = AspirinOnlineComment::find($iCommentId);
		if(!$oComment){
			echo $this->formatRet(false,"不存在该记录");exit;
		}
		//查找当前用户是否点赞
		$oZanLog = AspirinOnlineCommentZanLog::where('comment_id',$iCommentId)->where('user_id',$this->iUserId)->first();
		if($oZanLog){
			echo $this->formatRet(false,"您已经支持过了哦");exit;
		}
		//新增一条点赞记录
		$aZanLog = array(
			'comment_id'=> $iCommentId,
			'user_id'	=> $this->iUserId,
			'created_at'=> date('Y-m-d H:i:s',time())
		);
		$oZanLog = new AspirinOnlineCommentZanLog($aZanLog);
		$oZanLog->save();
		//更新当前评论的点赞总数
		$oComment->increment('zan_count');
		echo $this->formatRet(true,"点赞成功");exit;
	}

	/**
	 * 精彩回顾视频点赞   post提交
	 * http://cdma.local/apiaspirinonline/zan-video?id=1&token=
	 */
	public function postZanVideo()
	{
		$iVideoId = Input::get('id',0);
		$oVideo = AspirinOnlineSpeaker::find($iVideoId);
		if(!$oVideo){
			echo $this->formatRet(false,"不存在该记录");exit;
		}
		//查找当前用户是否点赞
		$iHasSupport = AspirinSupportLog::hasSupport(self::CATID,$this->iUserId,$iVideoId);
		if($iHasSupport){
			echo $this->formatRet(false,"您已经为当前视频点赞了哦");exit;
		}
		//新增一条点赞记录
		$aSupportLog = array(
			'cat_id'	=> self::CATID,
			'user_id'	=> $this->iUserId,
			'source_id'	=> $iVideoId,
			'created_at'=> date('Y-m-d H:i:s',time())
		);
		$oSupportLog = new AspirinSupportLog($aSupportLog);
		$oSupportLog->save();
		//更新当前视频的点赞总数
		$oVideo->increment('video_zan');
		echo $this->formatRet(true,"点赞成功");exit;
	}

	/***********************************精彩回顾************************/
	/**
	 * 获取省份
	 * http://cdma.local/apiaspirinonline/province?token=
	 */
	public function getProvince()
	{
		$aProvince = Hospital::where('parent_id',0)->lists('name','id');
		echo  $this->formatRet(true,"成功","success",$aProvince);exit;
	}

	/**
	 * 获取城市
	 * http://cdma.local/apiaspirinonline/city?iProvinceId=440000&token=
	 */
	public function getCity()
	{
		if(!Input::has('iProvinceId')){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$iProvinceId = intval(Input::get('iProvinceId'));
		$aCity = Hospital::where('parent_id',$iProvinceId)->lists('name','id');
		echo  $this->formatRet(true,"成功","success",$aCity);exit;
	}

	/**
	 * 获取下辖县
	 * http://cdma.local/apiaspirinonline/county?iCityId=440300&token=
	 */
	public function getCounty()
	{
		if(!Input::has('iCityId')){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$iCityId = intval(Input::get('iCityId'));
		$aCounty = Hospital::where('parent_id',$iCityId)->lists('name','id');
		echo  $this->formatRet(true,"成功","success",$aCounty);exit;
	}

	/**
	 * 获取医院
	 * http://cdma.local/apiaspirinonline/hospital?iCountyId=&iCityId=440300&token=
	 */
	public function getHospital()
	{
		$iCountyId = intval(Input::get('iCountyId',0));
		if($iCountyId != 0){
			$aHospital = Hospital::where('parent_id',$iCountyId)->lists('name','id');
		}else{
			$iPage = Input::get('page',1);
			$iPagesize = Input::get('pagesize',self::PAGESIZE);
			$iCityId = intval(Input::get('iCityId'),0);
			
			$aHospital = array();
			$aCountyId = Hospital::where('parent_id',$iCityId)->lists('id');
			foreach($aCountyId as $value){
				$aHospitalAll[] = Hospital::where('parent_id',$value)->lists('name','id');
			}
			foreach($aHospitalAll as $val){
				foreach($val as $k=>$v){
					$aHospital[$k] = $v;
				}
			}
		}
		echo  $this->formatRet(true,"成功","success",$aHospital);exit;
	}

	/**
	 * 获取医生用户
	 * http://cdma.local/apiaspirinonline/doctor?iHospitalId=10013881&timeid=1&token=
	 */
	public function getDoctor()
	{
		if(!Input::has('iHospitalId')||!Input::has('timeid')){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$iOnlineTimeId = intval(Input::get('timeid'));
		$iHospitalId = intval(Input::get('iHospitalId'));
		$sUserCompanyName = Hospital::where('id',$iHospitalId)->pluck('name');
		
		$aDoctorInfo = array();
		//只可邀请试点城市医生s
		/*$aPilotCity = User::getPilotCity();
		$aPilotCountry = User::getPilotCountry();
		$iCountryId = Hospital::where('id',$iHospitalId)->pluck('parent_id');
		if(!in_array($iCountryId,$aPilotCountry)){
			$iCityId = Hospital::where('id',$iCountryId)->pluck('parent_id');
			if(!in_array($iCityId,$aPilotCity)){
				echo  $this->formatRet(true,"成功","success",$aDoctorInfo);exit;
			}
		}*/
		//只可邀请试点城市医生e
		$aDoctor = User::where('role_id',3)->where('id','!=',$this->iUserId)->where(function($query) use($iHospitalId,$sUserCompanyName){
				$query->where('user_company',$iHospitalId)
				->orWhere('user_company_name',$sUserCompanyName);
			})->lists('user_name','id');
		$aLog = AspirinOnlineListener::where('online_time_id',$iOnlineTimeId)->where('speaker_id',$this->iUserId)->lists('listener_id','id');
		foreach($aDoctor as $k=>$v){
			if(!in_array($k,$aLog)){
				$aDoctorInfo[] = array(
					'id'		=> $k,
					'user_name' => $v
				);
			}
		}
		echo  $this->formatRet(true,"成功","success",$aDoctorInfo);exit;
	}

	/*******************************定时推送s*****************************/
	/**
	 * http://cdma.local/apiaspirinonline/speaker-notice
	 * 每周一早上六点给讲者推送可以报名讲课的通知
	 */
	public function getSpeakerNotice(){
		$aPos = User::getPosition();
		$aUserAll = User::where('role_id',3)->where('card_auth_flag',2)->where(function($query) use($aPos){
				$query->where('user_position',$aPos[2])->orWhere('user_position',$aPos[3])->orWhere('user_position',$aPos[4]);
			})->lists('id');
		$sNowTime = date('Y-m-d H:i:s',time());
		$oOnline = AspirinOnline::where('show_start_time','<',$sNowTime)->where('show_end_time','>',$sNowTime)->orderby('created_at','desc')->first();
		if(count($oOnline)){
			$aSpeakerLog = AspirinOnlineSpeaker::where('online_id',$oOnline->id)->where('cancel_flag',0)->lists('speaker_id');
			$aUser = array_diff($aUserAll,$aSpeakerLog);
			$sPushContent = PushService::ONLINE_SPEAKER_NOTICE;
			PushService::postClientBatch('国卫健康云',$sPushContent,13,$aUser);
			echo  $this->formatRet(true,"成功","success");exit;
		}
	}

	/**
	 * http://cdma.local/apiaspirinonline/listener-notice
	 * 每周一早上六点给听者推送可以报名参会的通知
	 */
	public function getListenerNotice(){
		$aPos = User::getPosition();
		$aUser = User::where('role_id',3)->where('card_auth_flag',2)->where(function($query) use($aPos){
				$query->where('user_position',$aPos[2])->orWhere('user_position',$aPos[3])->orWhere('user_position',$aPos[4]);
			})->lists('id');
		$sPushContent = PushService::ONLINE_LISTENER_NOTICE;
		PushService::postClientBatch('国卫健康云',$sPushContent,14,$aUser);
		echo  $this->formatRet(true,"成功","success");exit;
	}

	/*******************************定时推送e*****************************/
	/**
	 * http://2kghy.kydev.net/apiaspirinonline/time?id=1&second=1000&token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
	 * 记录参与直播会议时间(累加)
	 */
	public function getTime(){
		global $iUserId;
		$iUid = $iUserId;
		$iOnlineId = Input::get('id',0);
		$iSecond = Input::get('second',0);
		//记录时间，加麦粒
		AspirinOnlineLog::addTime($iUserId, $iOnlineId,$iSecond);
		echo $this->formatRet(true,"成功");exit;
	}

	/**
	 * 在线会议——讲者申报确认   get 提交
	 * http://2kdll.kydev.net/apiaspirinonline/speaker-submit?iPptId=1&iOnlineId=1&timeid=55&declare_name=代路路&declare_id_num=131182199211112222&declare_bank=北京银行&declare_bank_num=6222100056753098&token=
	 */
	public function getSpeakersettime()
	{
		if(!Input::get('iPptId') || !Input::get('iOnlineId')){
			echo  $this->formatRet(false,"缺少必要参数。");exit;
		}
		$iPptId = intval(Input::get('iPptId'));
		$iOnlineId = intval(Input::get('iOnlineId'));
		$sOnlineDate = trim(Input::get('sOnlineDate'));
		$sOnlineStartTime = trim(Input::get('sOnlineStartTime'));
		$sOnlineEndTime = trim(Input::get('sOnlineEndTime'));
		if(!$sOnlineDate){
			echo $this->formatRet(false,"请输入会议日期。");
			exit;
		}
		if(!$sOnlineStartTime){
			echo $this->formatRet(false,"请输入会议开始时间");
			exit;
		}
		if(!$sOnlineEndTime){
			echo $this->formatRet(false,"请输入会议结束时间。");exit;
		}
		$aOnlineTime = array(
			'online_id'   => $iOnlineId,
			'online_date' => $sOnlineDate,
			'time_period' => "{$sOnlineStartTime}-{$sOnlineEndTime}",
			'created_at'  => date('Y-m-d H:i:s',time())
		);
		//判断改时段是否存在
		$isHave = AspirinOnlineTime::where("online_id",$iOnlineId)->
			where("online_date","{$aOnlineTime['online_date']}")->
			where("time_period","{$aOnlineTime['time_period']}")->first();
		if($isHave){
			echo $this->formatRet(true, "", "", '', array("data"=>array('timeid'=>$isHave->id)));
			// echo $this->formatRet(false,"对不起，提交失败！");
			exit;
		}
		$oOnlineTime = new AspirinOnlineTime($aOnlineTime);
		$res = $oOnlineTime->save();
		if($res){
			echo $this->formatRet(true, "", "", '', array("data"=>array('timeid'=>$oOnlineTime->id)));
			exit;
		}else{
			echo $this->formatRet(false,"对不起，提交失败！");
			exit;
		}
	}

    /**
     * 获取 aspirin 自助会议的视频列表
     * @return mixed
     */
	public function getAdList()
    {
        global $iUserId;

        $aList = [];
        $oAdList = AspirinVideoAd::where('status', 1)->orderBy('index', 'asc')->get();

        // 获取上次观看日志
        $log = AspirinVideoAdLog::where('user_id', $iUserId)->orderBy('id', 'desc')->first();
        $index = empty($log->index) ? 1 : $log->index+1;
        $index = ($log->index == count($oAdList) ? 1 : $index);

        // 循环处理数据
        foreach ($oAdList as $item) {
            $aList[] = [
                'id' => $item->id,
                'title' => $item->title,
                'index' => $item->index,
                'url' => url($item->filepath),
                'created_at' => $item->created_at->format('Y-m-d H:i'),
            ];
        }

        return Response::make(['status' => true, 'msg' => 'ok', 'index'=>$index, 'list' => $aList]);
    }

    /**
     * 上报广告视频观看记录
     * @return mixed
     */
    public function postAdLog()
    {
        global $iUserId;

        $index = Input::get('index', 0);

        if ($index) {
            $oInfo = new AspirinVideoAdLog();
            $oInfo->index = $index;
            $oInfo->user_id = $iUserId;
            $oInfo->save();
        }

        return Response::make(['status' => true, 'msg' => '上报成功']);
    }
}