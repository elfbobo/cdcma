<?php

/**
 * 中文文献接口操作类
 * @author gk
 */
class ApiCnsearchController extends BaseController {

	CONST  PAGESIZE = 10;
	public $iUserid = 0;
	public $sAppPath;

	public function __construct(PubmedController $oPmd){
		$this->beforeFilter('apilogin');
		$this->oPmd = $oPmd;
		$this->sAppPath = app_path();
	}

	public function getIndex(){
		global $iUserId;
		$this->iUserid = $iUserId;
		if(!$this->iUserid){
			return json_encode(array('success'=>false,'msg'=>'您还没有登陆'));
		}
		$this->removeBadCharacters();
		$sAppPath = app_path(); 
		$sAllFields = Input::has('sAllFields') ? stripslashes(urldecode(trim(Input::get('sAllFields')))) : '';
		if($sAllFields){
			$q = "$sAllFields";
		}else{
			$sTitle = Input::has('sTitle') ? stripslashes(urldecode(trim(Input::get('sTitle')))) : '';
			$sAuthor = Input::has('sAuthor') ? stripslashes(urldecode(trim(Input::get('sAuthor')))) : '';
			$sJournalName = Input::has('sJournalName') ? stripslashes(urldecode(trim(Input::get('sJournalName')))) : '';
			$sKeyword = Input::has('sKeyword') ? stripslashes(urldecode(trim(Input::get('sKeyword')))) : '';
			$sAbstract = Input::has('sAbstract') ? stripslashes(urldecode(trim(Input::get('sAbstract')))) : '';
			$sBeginTime = Input::has('sBeginTime') ? stripslashes(urldecode(trim(Input::get('sBeginTime')))) : '';
			$sEndTime = Input::has('sEndTime') ? stripslashes(urldecode(trim(Input::get('sEndTime')))) : date('Y', time());;
			$sBool = "*";
			$q = '';
			if($sTitle){
				$q.='主题:';
				$q.='('.$sTitle.')';
				$q.=' '.$sBool.' ';
			}
			if($sAuthor){
				$q.='创作者:';
				$q.='('.$sAuthor.')';
				$q.=' '.$sBool.' ';
			}
			if($sJournalName){
				$q.='期刊—刊名:';
				$q.='('.$sJournalName.')';
				$q.=' '.$sBool.' ';
			}
			if($sKeyword){
				$q.='关键词:';
				$q.='('.$sKeyword.')';
				$q.=' '.$sBool.' ';
			}
			if($sAbstract){
				$q.='摘要:';
				$q.='('.$sAbstract.')';
				$q.=' '.$sBool.' ';
			}
			if ($sBeginTime){
				$q.='Date:'.$sBeginTime;
			}else{
				$q.='Date:';
			}
			if ($sEndTime){
				$q.='-'.$sEndTime;
			}
		}
		$q.=' '.'DBID:WF_QK';
		$page = Input::has('iPage') ? intval(trim(Input::get('iPage'))) : 1;
		$iPageSize = Input::has('iPageSize') ? intval(trim(Input::get('iPageSize'))) : self::PAGESIZE;
		if ($page){
			$url = WANFANGER_API_URL.'q='.rawurlencode($q).'&db=wf_qk&p='.$page.'&n='.$iPageSize;
		}else{
			$url = WANFANGER_API_URL.'q='.rawurlencode($q).'&db=wf_qk';
		}
		$str = $this->__requestApiByCurl($url);
		$results = $this->__wangFangMatch($str);
		$total = intval($results['totals']);
		$i=0;
		$aData=array();
		if (!empty($results)){
			foreach ($results as $key=>$result){
				if (is_numeric($key)){
					$aData[$key]['id']= $results['totals']>$iPageSize ? ++$i+($page-1)*$iPageSize:++$i;
					$aData[$key]['title']=isset($result['title']) ? $this->__filterContent(strip_tags($result['title'])) : '';
					$aData[$key]['source']=isset($result['source']) ? $this->__filterContent(strip_tags($result['source'])) : '';
					$aData[$key]['author']=isset($result['author']) ? $this->__filterContent(strip_tags($result['author'])) : '';
					$aData[$key]['description']=isset($result['description']) ? $this->__filterContent(strip_tags($result['description'])) : '';
					$aData[$key]['keywords']=isset($result['keywords']) ? $this->__filterContent(strip_tags($result['keywords'])) : '';
				}
			}
			return json_encode(array('success'=>true,'count'=>$total,'dataList'=>$aData));
		}else{
			return json_encode(array('success'=>false,'msg'=>'未命中记录','count'=>0,'dataList'=>array()));
		}
	}
	
    private function __filterContent($content){
		$replacement = "";
		$content = str_replace("&nbsp;", "", $content);
		$sSpace = "/(\s*(\r\n|\n\r|\n|\r)\s*)/";
		$content = preg_replace($sSpace, $replacement, $content);
		return $content;
	}
	
    /**
	 * 获取全文,求助全文
	 */
	public function getFulltextcn(){
		global $iUserId;
		$this->iUserid = $iUserId;
		if(!$this->iUserid){
			return json_encode(array('success'=>false,'msg'=>'您还没有登陆'));
		}
		if(Input::has('sTitle')){
			$sTitle = stripslashes(urldecode(trim(Input::get('sTitle'))));  
		}else{
			return json_encode(array('success'=>false,'msg'=>'文章标题不能为空'));
		}
		if(Input::has('sSource')){
			$sSource = stripslashes(urldecode(trim(Input::get('sSource'))));  
		}else{
			return json_encode(array('success'=>false,'msg'=>'出处不能为空'));
		}
		if(Input::has('sAuthor')){
			$sAuthor = stripslashes(urldecode(trim(Input::get('sAuthor'))));  
		}else{
			$sAuthor = '';
		}
		$oUser = User::where('id', $this->iUserid)->first();
		if(!$oUser->user_email){
			return json_encode(array('success'=>false,'msg'=>'用户邮箱不能为空'));
		}
		/*if(intval($oUser->user_credit_count) < ZH_HELP_SCORE){
			return json_encode(array('success'=>false,'msg'=>'用户积分不足'));
		}*/
		$sHelpText = $this->__filterContent(strip_tags($sTitle.",文章出处为：".$sSource.",作者为：".$sAuthor));
		//邮件主题
		$subject = "国卫健康云中文文献检索";
		$content = "国卫健康云中文文献检索需要获取全文,文章标题为：".$sHelpText.", 请将全文发送到邮箱：".$oUser->user_email;
		$iId = PubmedHelpMail::where('userid', $this->iUserid)->where('title', "$sTitle")->pluck('id');
		if(!$iId){
			$aHelpInfo['userid'] = $this->iUserid; 
			$aHelpInfo['email'] = addslashes($oUser->user_email);
			$aHelpInfo['title'] = addslashes($sTitle);
			$aHelpInfo['content'] = addslashes($content);
			$aHelpInfo['device_type'] = 1;
			$aHelpInfo['pub_type'] = 2;
			// $aHelpInfo['posttime'] = time();
			$oHelp = new PubmedHelpMail($aHelpInfo);
			$oHelp->save();
			PubmedHelpMail::SendEmail($subject,$content);
			//扣除积分
			// 				$this->oScoreService->getIndex(6,"PMD_ZH_HELP", 0, $this->iUserid, "app", $sTitle);
			return json_encode(array('success'=>true,'msg'=>'您所发起的全文申请已经成功发送至医学经理及产品经理。待相关人员确认后，您所申请的文献将发送至您的邮箱。'));
		}else{
			return json_encode(array('success'=>false,'msg'=>'您已经求助过该文献'));
		}	    	          
	}
	
    private function __requestApiByCurl($sUrl,$iTimeout = 60) {
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $sUrl );
		$iTimeout = intval ( $iTimeout );
		if ($iTimeout) {
			curl_setopt ( $ch, CURLOPT_TIMEOUT, $iTimeout );
		}
		ob_start ();
		curl_exec ( $ch );
		$sOut = ob_get_clean ();
		curl_close ( $ch );
		return $sOut;
		//return json_decode ($sOut, true);
	}
	
	private function __wangFangMatch($str){
		$arrData = array();
		preg_match_all("/<ul\s+class=[\"']list_ul[\"']\s*>(.*?)<\/ul>/is", $str, $arrMatch);
		if(!empty($arrMatch)){
		    foreach($arrMatch[1] as $key=>$data){
		    	preg_match_all("/<li.*?>(.*?)<\/li>/is", $data, $arrMatchLi);
		    	if(!empty($arrMatchLi)){
		    		foreach($arrMatchLi[1] as $k => $contentLi){
		    			if($k == 0){
			    			preg_match_all("/<a.*?>(.*?)<\/a>/is",$contentLi, $arrMatchTitle);
			    			if(!empty($arrMatchTitle[1][1])){
			    			    $arrData[$key]['title'] = $arrMatchTitle[1][1];
			    			}
		    			}
		    			if ($k == 1){
		    				preg_match_all("/<a.*?>(.*?)<\/a>/is",$contentLi, $arrMatchSource);
		    				$arrData[$key]['source'] = $arrMatchSource[1][0].",".$arrMatchSource[1][1];
		    				if (!empty($arrMatchSource[1])){
		    					unset($arrMatchSource[1][0],$arrMatchSource[1][1]);
		    				}
		    				if (!empty($arrMatchSource[1])){
		    			    	$arrData[$key]['author'] = implode(",", $arrMatchSource[1]);
		    				}
		    			}
		    			if ($k == 2){
		    				preg_match_all("/<span\s+style=[\"']display:inline[\"']\s*>(.*?)<\/span>/is",$contentLi, $arrMatchDesc);
		    				if(!empty($arrMatchDesc[1][0])){
		    				    $arrData[$key]['description'] = $arrMatchDesc[1][0];
		    				}
		    				preg_match_all("/<p\s+class=[\"']greencolor[\"']\s+style=[\"']display:inline[\"']\s*>(.*?)<\/p>/is",$contentLi, $arrMatchKeywords);
		    				if(!empty($arrMatchKeywords[1][0])){
		    				    $arrData[$key]['keywords'] = str_replace("关键词：", "", $arrMatchKeywords[1][0]); 
		    				}
		    			}
		    		 }
			    }
		    }
		}
		preg_match_all("/<span.*?\s+class=[\"']page_link[\"']\s*>(.*?)<\/span>/is", $str, $arrMatchPages);
		if(!empty($arrMatchPages[1][0])){
		   preg_match_all("/<\/t>(.*?)<t>/is", $arrMatchPages[1][0], $arrMatchPageNum);
		   $arrData['pageNum'] = $arrMatchPageNum[1][0];
		}else{
		   $arrData['pageNum'] = 0;
		}
		preg_match_all("/<td\s+class=[\"']space_td mode[\"']>(.*?)<td\s+class=[\"']sortby_td[\"']>/is", $str, $arrMatchTotalHtml);
		if(!empty($arrMatchTotalHtml[1][0])){
		    preg_match_all("/<td>(.*?)<\/td>/is", $arrMatchTotalHtml[1][0], $arrMatchTotal);
		}
        if(!empty($arrMatchTotal[1][0])){
		    preg_match_all("/\d/is", $arrMatchTotal[1][0], $arrMatchTotalNums);
		    $arrData['totals'] = implode('', $arrMatchTotalNums[0]);
		}else{
		    $arrData['totals'] = 0;
		}
		return $arrData;
	}
		
   /**
	 * @desc 过滤字符
	 * Enter description here ...
	 */
    public function removeBadCharacters(){
		global $bad_utf8_chars;
		$bad_utf8_chars = array(
			"\0", 
			"\xc2\xad",
			"\xcc\xb7", 
			"\xcc\xb8", 
			"\xe1\x85\x9F", 
			"\xe1\x85\xA0", 
			"\xe2\x80\x80", 
			"\xe2\x80\x81", 
			"\xe2\x80\x82", 
			"\xe2\x80\x83", 
			"\xe2\x80\x84", 
			"\xe2\x80\x85", 
			"\xe2\x80\x86", 
			"\xe2\x80\x87", 
			"\xe2\x80\x88", 
			"\xe2\x80\x89", 
			"\xe2\x80\x8a", 
			"\xe2\x80\x8b", 
			"\xe2\x80\x8e", 
			"\xe2\x80\x8f", 
			"\xe2\x80\xaa", 
			"\xe2\x80\xab", 
			"\xe2\x80\xac", 
			"\xe2\x80\xad", 
			"\xe2\x80\xae", 
			"\xe2\x80\xaf", 
			"\xe2\x81\x9f", 
			"\xe3\x80\x80", 
			"\xe3\x85\xa4", 
			"\xef\xbb\xbf", 
			"\xef\xbe\xa0", 
			"\xef\xbf\xb9", 
			"\xef\xbf\xba", 
			"\xef\xbf\xbb", 
			"\xE2\x80\x8D"
		);
		function _remove_bad_characters($array) {
			global $bad_utf8_chars;
			return is_array($array) ? array_map('_remove_bad_characters', $array) : str_replace($bad_utf8_chars, '', $array);
		}
		$_GET = _remove_bad_characters($_GET);
		$_POST = _remove_bad_characters($_POST);
		$_COOKIE = _remove_bad_characters($_COOKIE);
		$_REQUEST = _remove_bad_characters($_REQUEST);
	}
	
}