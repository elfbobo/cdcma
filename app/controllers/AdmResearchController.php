<?php

/*
|------------------------------
| @author zangguoqing
|调研管理
*/
class AdmResearchController extends BaseController {

	CONST  PAGESIZE = 10;

	//调研期列表
    public function ResearchList()
    {
		$sName = $_SERVER['SERVER_NAME'];
		$oResearchPeriod = ResearchPeriod::paginate(self::PAGESIZE);
		return View::make('admin.userresearch.researchperiod')
			->with('oResearchPeriod',$oResearchPeriod)
			->with('sName',$sName);
    }

    //调研期添加页
    public function AddPeriod()
    {
		$oResearchPeriod = ResearchPeriod::get();
		$i = 0;
		$aPeriod = array();
		foreach($oResearchPeriod as $v){
			$aPeriod[$i] = $v->id;
			$i++;         
		}
		return View::make('admin.userresearch.researchperiodadd')->with('aPeriod',$aPeriod);
    }

    //调研器添加
    public function DoPeriodAdd()
    {
		$aResearchPeriod = Input::all();
		$oResearchPeriod = new ResearchPeriod;
		$oResearchPeriod->research_title =  $aResearchPeriod['research_title'];
		$oResearchPeriod->research_imges =  $aResearchPeriod['doc_thumb'];
		$oResearchPeriod->save();
		return Redirect::to('/admmeeting');
    }

    //调研期编辑
    public function  EditResearch($iId)
    {
		$oResearPeriod = ResearchPeriod::find($iId);
		return View::make('admin.userresearch.researchperiodedit')->with('oResearPeriod',$oResearPeriod);
    }

    //编辑修改
    public function DoPeriodEdit()
    {
		$aResearchPeriod =Input::all();
		$oResearchPeriod =ResearchPeriod::find($aResearchPeriod['id']);
		$oResearchPeriod ->research_imges = $aResearchPeriod['doc_thumb'];
		$oResearchPeriod ->research_title = $aResearchPeriod['doc_name'];
		$oResearchPeriod ->save();
		return Redirect::to('/admresearch/research-list');
    }

    //
    public function DeleteResearch($iId)
    { 
		$oResearchPeriod = ResearchPeriod::find($iId);
		if($oResearchPeriod){
			$oResearchPeriod ->delete();
			$oResearchPeriodQuestion = ResearchPeriodQuestion::where('research_period_id',$iId)->delete();
			$oResearchPeriodItem = ResearchPeriodItem::where('research_period_id',$iId)->delete();
			$oResearchPeriodLog = ResearchPeriodLog::where('research_period_id',$iId)->delete();
			$oResearchPeriodStatistical = ResearchPeriodStatistical::where('research_period_id',$iId)->delete();
			return Redirect::to('/admresearch/research-list');
		}else{
			return Redirect::to('/admresearch/research-list');
		}
    }
    
	//题目列表 
	public function QuestionList($iId)
	{
		$oResearchPeriodQuestion =  ResearchPeriodQuestion::where('research_period_id',$iId)->paginate(10);
		$aResearchPeriod  = ResearchPeriod::lists('research_title','id');
		return View::make('admin.userresearch.questionlist')
			->with('oResearchPeriodQuestion',$oResearchPeriodQuestion)
			->with('aResearchPeriod',$aResearchPeriod)
			->with('iId',$iId);
	}

	//删除题目及选项
	public function DeleteSurvey($iId,$iPeriod)
	{
		ResearchPeriodQuestion::destroy($iId);
		$oResearchPeriodItem = ResearchPeriodItem::where('research_id',$iId);
		if(count($oResearchPeriodItem)){
			$oResearchPeriodItem->delete();
		}
		return Redirect::to('/admresearch/question-list/'.$iPeriod);
	}

	//进入题目编辑页
	public function EditSurvey($iId)
	{
		$oResearchPeriodQuestion = ResearchPeriodQuestion::where('id',$iId)->first();
		$oUserSurveyItem = ResearchPeriodItem::where('research_id','=',$iId)->get();
		return View::make('admin.userresearch.researchedit')->with('oResearchPeriodQuestion',$oResearchPeriodQuestion)->with('oUserSurveyItem',$oUserSurveyItem);
	}

	//进入题目添加页  
	public function  AddQuestion($iPeriod)
	{
		return View::make('admin.userresearch.add')->with('iPeriod',$iPeriod);
	}

	//新增调研题目 
	public function DoAdds()
	{
		$aMessage = Input::all();
		$iPeriod = $aMessage['iPeriod'];
		$oResearchPeriodQuestion = new ResearchPeriodQuestion;
		$oResearchPeriodQuestion->research_period_id = $aMessage['iPeriod'];
		$oResearchPeriodQuestion->research_title = $aMessage['q_title'];
		$oResearchPeriodQuestion->research_result = $aMessage['q_result'];
		$oResearchPeriodQuestion->research_type= isset($aMessage['q_type']) ? $aMessage['q_type']:1 ;
		$oResearchPeriodQuestion->research_img = $aMessage['doc_thumb'];
		$oResearchPeriodQuestion->save();
		$iQuestionId = $oResearchPeriodQuestion -> id;
		$aKey =array('A','B','C','D','E','F','G','H','I','J');
		foreach($aMessage as $key=>$value){
			$sNewKey = substr($key,0,11);
			if($sNewKey == 'optionvalue'){
				$k = substr($key,11,1);
				$aMessage[$k]=$value;
			}
		}
		for($i=0;$i<10;$i++){
			$sKey = $aKey[$i];
			if(!empty($aMessage[$sKey])){
				$oResearchPeriodItem = new ResearchPeriodItem;
				$oResearchPeriodItem -> research_period_id = $iPeriod;
				$oResearchPeriodItem -> research_id = $iQuestionId;
				$oResearchPeriodItem -> research_option  = $sKey;
				$oResearchPeriodItem -> research_content = $aMessage[$sKey];
				$oResearchPeriodItem ->save();
			}
		}
		return Redirect::to('/admresearch/question-list/'.$iPeriod);
	}

	//修改调研题目
	public function DoAdd()
	{
        $aMessage = Input::all();
        $iId =$aMessage['iId'];
	    $phase = $aMessage['iPeriod'];
	    $oResearchPeriodQuestion = ResearchPeriodQuestion::find($iId);
	    $oResearchPeriodQuestion->research_title = $aMessage['q_title'];
	    $oResearchPeriodQuestion->research_result = $aMessage['q_result'];
	    $oResearchPeriodQuestion->research_img = $aMessage['doc_thumb'];
	    $oResearchPeriodQuestion->research_type = $aMessage['q_type'];
	    $oResearchPeriodQuestion->save();
		foreach($aMessage as $key=>$value){
			$sNewKey = substr($key,0,11);
			if($sNewKey == 'optionvalue'){
				$k = substr($key,11,1);
				$aMessage[$k]=$value;
			}
		}
	    $aKeys = array_keys($aMessage);
        $aAlloption = ResearchPeriodItem::where('research_id','=',$iId)->where('research_period_id','=',$phase)->get();
        if($aAlloption){
			foreach($aAlloption as $value){
				$opton = $value->research_option;
				if(!in_array($opton,$aMessage)){
					ResearchPeriodItem::where('research_id','=',$iId)->where('research_period_id','=',$phase)->where('research_option',$opton)->delete();
				}
			}
        }
		$oResearchPeriodItem = ResearchPeriodItem::where('research_id','=',$iId)->where('research_period_id','=',$phase)->get();
		if(count($oResearchPeriodItem)){
			$aKey =array('A','B','C','D','E','F','G','H','I','J');
			foreach($aKeys as $value){
				if(in_array($value,$aKey)){
					$oChoice = ResearchPeriodItem::where('research_id','=',$iId)->where('research_period_id','=',$phase)->where('research_option','=',$value)->first();
					if(count($oChoice)){
						$oChoice->research_content = $aMessage[$value];
						$oChoice ->save();
					}else{
						if(!empty($aMessage[$value])){
							$oNewChoice = new ResearchPeriodItem;
							$oNewChoice-> research_period_id = $phase;
							$oNewChoice-> research_id = $iId;
							$oNewChoice-> research_option = $value;
							$oNewChoice-> research_content = $aMessage[$value];
							$oNewChoice->save();
						}
					}
				} 
			}
	   }else{
	       $aKey =array('A','B','C','D','E','F','G','H','I','J');
	       foreach ($aKeys as $value){
				if(in_array($value,$aKey)){
					if(!empty($aMessage[$value])){
						$oNewChoices = new ResearchPeriodItem;
						$oNewChoices-> research_period_id = $phase;
						$oNewChoices->research_id = $iId;
						$oNewChoices->research_option = $value;
						$oNewChoices->research_content = $aMessage[$value];
						$oNewChoices->save();
					}
				}
			}
		}
	    return Redirect::to('/admresearch/question-list/'.$phase);
	}
 
	public function  HasChoiceOrNot($iId,$phase_id)
	{
	    $oSurveyItem = UserSurveyItem::where('question_id','=',$iId)->where('phase_id','=',$phase_id)->get();
		$aChoice = array('A','B','C','D','E','F','G','H','I','J');
	    if(count($oSurveyItem)){
	        foreach($oSurveyItem as $value){
				$key = $value -> item;
				$val = $value ->item_title;
				$aChoice[$key] = $val;
			}           
			$aChoice['iId'] = $iId;
			$aChoice['phase'] = $phase_id;        
			return View::make('admin.usersurvey.survey')->with('aChoice',$aChoice);
	    }else{
			$aChoice['iId'] = $iId;
			$aChoice['phase'] = $phase_id;
			return View::make('admin.usersurvey.survey')->with('aChoice',$aChoice);
	    }
	}
	 
	//题目选项的添加和修改 
	public function DoAddChoice()
	{
		$aAllMessage = Input::all();
		$iId =$aAllMessage['iId'];
		$phase = $aAllMessage['phase'];
		$oSurveyItem = UserSurveyItem::where('question_id','=',$iId)->where('phase_id','=',$phase)->get();
		$aKeys = array_keys($aAllMessage); 
		if(count($oSurveyItem)){
	   		//题目选项的修改
			foreach($oSurveyItem as $value){
				$oKey = $value -> item; 
				if(in_array($oKey,$aKeys)){
					$value -> item_title = $aAllMessage[$oKey];
					$value -> save();
				}else{
					$value -> where('item','=',$oKey) ->delete();
				}
			}
			return Redirect::to('/admsurvey/question-list');
	   }else{
			//题目选项的添加
			$aAllKeys = array('A','B','C','D','E','F','G','H','I','J');
			for($i=0;$i<10;$i++){
				$sKey = $aAllKeys[$i];
				if(!empty($aAllMessage[$sKey])){
					$oNewSurveyItem = new UserSurveyItem();
					$oNewSurveyItem -> item_title =$aAllMessage[$sKey];
					$oNewSurveyItem -> item = $sKey;
					$oNewSurveyItem -> phase_id =$phase;
					$oNewSurveyItem -> question_id =$iId;
					$oNewSurveyItem -> save();
				}
			}
			return Redirect::to('/admsurvey/question-list');
		}
	}

	//上传图片
	public function UploadThumb(){
		if($_FILES['upload_file']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['upload_file']['name'];
			$attach_fileext = get_filetype($attach_filename);
			$rand_name = date('YmdHis', time()).rand(1000,9999);
			$sFileName = $rand_name.'.'.$attach_fileext;
			$sPath = "/upload/infor/$attach_fileext/".date('Ymd',time());
			$sRealPath = public_path().$sPath;
			mkdirs($sRealPath);
			// $attach_filesize = filesize($_FILES['upload_file']['tmp_name']);
			move_uploaded_file($_FILES['upload_file']['tmp_name'], $sRealPath.DS.$sFileName);
			$sFileNameS = $rand_name . '_s.' . $attach_fileext;
			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 1000, 1000 );
			$sFileUrl = $sPath.'/'.$sFileNameS;
			$json = array('doc_thumb'=>$sFileUrl);
			echo json_encode($json);
			die;
		}
	}

	//查看结果
	public function ResultList($iId)
	{
		$oResearchPeriod = ResearchPeriod::where('id',$iId)->first();
		$sTitle = $oResearchPeriod->research_title;
		$oResearchPeriodQuestion = ResearchPeriodQuestion::where('research_period_id',$iId)->paginate(10);
		$oResearchPeriodItem = ResearchPeriodItem::where('research_period_id',$iId)->get();
		$oResearchPeriodlog = ResearchPeriodLog::where('research_period_id',$iId)->get();
		$oResearchPeriodStatistical = ResearchPeriodStatistical::where('research_period_id',$iId)->get();
		$aQuestionRadio = array();
		foreach($oResearchPeriodStatistical as $k=>$v){
			$sOption = $v ->research_option;
			$iOrder = $v ->research_id;
			$oResearchPeriodStatistical = ResearchPeriodStatistical::where('research_id',$iOrder)->where('research_option',$sOption)->where('research_period_id',$iId)->get();
			$aQuestionRadio[$iOrder][$sOption] = $v->count;
		}
		$apercent =array();
		foreach($aQuestionRadio as $k=>$v){
			$arr=$v;
			$num = array_sum($arr);
			$apercent[$k]['A']=isset($v['A'])?floor(($v['A']/$num)*100):0;
			$apercent[$k]['B']=isset($v['B'])?floor(($v['B']/$num)*100):0;
			$apercent[$k]['C']=isset($v['C'])?floor(($v['C']/$num)*100): 0;
			$apercent[$k]['D']=isset($v['D'])? floor(($v['D']/$num)*100): 0;
		}
		return View::make('admin.userresearch.surveyresult')
			->with('oResearchPeriodStatistical',$oResearchPeriodStatistical)
			->with('oResearchPeriod',$oResearchPeriod)
			->with('oResearchPeriodQuestion',$oResearchPeriodQuestion)
			->with('oResearchPeriodItem',$oResearchPeriodItem)
			->with('oResearchPeriodlog',$oResearchPeriodlog)
			->with('sTitle',$sTitle)
			->with('apercent',$apercent)
			->with('aQuestionRadio',$aQuestionRadio)
			->with('iId',$iId);
	} 

	//导出调研结果
	public function ResearchResult($iId)
	{
		$oResearchPeriod = ResearchPeriod::find($iId)->first();
		$sTitle = $oResearchPeriod ->research_title;
		$oResearchPeriodLog = ResearchPeriodLog::where('research_period_id',$iId)->orderBy('research_question_id')->get();
		// $oResearchPeriodStatistical = ResearchPeriodStatistical::where('research_period_id',$iId)->orderBy('research_id')->get();
		$oResearchPeriodQuestion = ResearchPeriodQuestion::where('research_period_id',$iId)->get();
		$iNum = $oResearchPeriodQuestion ->count();
		$sResearchResult ='IP';
		for($i=1;$i<=$iNum;$i++){
			$sResearchResult.= ','.$i;
		}
		$sResearchResult .="\n";
		$aTime = array();
		$i =1;
		foreach($oResearchPeriodLog as $v){
			$sTime = $v ->created_at;
			if(!in_array($sTime,$aTime)){
				$aTime[$i] = $sTime;
				$i++;
				$oResearchPeriodLogs =  ResearchPeriodLog::where('research_period_id',$iId)->where('created_at',$sTime)->orderBy('research_question_id')->get();
				$sResearchResult.=$v->user_ip.',';
				foreach($oResearchPeriodLogs as $v){
					$sResearchResult.=$v->research_option.',';
				}
				$sResearchResult.="\n";
			}
		}
		$sResearchResult = strval($sResearchResult);
		$sResearchResult = iconv("UTF-8",'GB2312//IGNORE',$sResearchResult);
		header("Content-type:text/csv");
		header("Content-Type: application/force-download");
		header("Content-Disposition: attachment; filename=第{$iId}期调研.".date('Y-m-d').".csv");
		header('Expires:0');
		header('Pragma:public');
		echo $sResearchResult;
	}

    //导出所用没有上级的用户id
	public function UserId()
	{
		$oAllUser = User::get();
		$aAllId = array();
		$sUsers ='用户id'."\n";
		foreach($oAllUser as $k=>$v){
			$aAllId[$k]= $v->id;
		}
		$oAllUsers = User::where('role_id','!=','2')->get();
		foreach ($oAllUsers as $v){
			$iLinkUserId = $v->link_user_id;
			$iRoleId = $v ->role_id;
			if(!in_array($iLinkUserId,$aAllId) && $iRoleId != 1){
				$sUsers .= $v->id."\n";
				$this->users($v->id,$sUsers);
			}
		}
		$sUsers = strval($sUsers);
		$sUsers = iconv("UTF-8",'GB2312//IGNORE',$sUsers);
		header("Content-type:text/csv");
		header("Content-Type: application/force-download");
		header("Content-Disposition: attachment; filename=无关联用户的id".".csv");
		header('Expires:0');
		header('Pragma:public');
		echo $sUsers;
	}

	//查抄
	public function Users($id,$sUsers)
	{
		$oUser = User::where("link_user_id",$id)->get();
		if($oUser){
			foreach($oUser as $v){
				$sUsers .=$v->id."\n";
				$this->users($v->id,$sUsers);
			}
		} 
	}
  
}