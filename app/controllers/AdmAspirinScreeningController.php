<?php

class AdmAspirinScreeningController extends BaseController {
	
	CONST  PAGESIZE = 10;

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	//风险筛查列表
	public function getList()
	{
		$sName = Input::get('name');
		if(!empty($sName)){
			$oScreeningLog = AspirinScreeningLog::leftJoin('user','user.id','=','aspirin_screening_log.user_id')
				->select('user.user_cwid','user.user_name','aspirin_screening_log.*')
				->where('user_cwid','like','%'.$sName.'%')
				->orderBy("created_at","desc")
				->paginate(self::PAGESIZE);
		}else{
			$oScreeningLog = AspirinScreeningLog::leftJoin('user','user.id','=','aspirin_screening_log.user_id')
				->select('user.user_cwid','user.user_name','aspirin_screening_log.*')
				->orderBy("created_at","desc")
				->paginate(self::PAGESIZE);
		}
		Return View::make('admin.aspirin.screening.list')->with('oScreeningLog',$oScreeningLog);
	}

	//风险筛查详情
	public function getShow($iUserId)
	{
		$oScreening = AspirinScreening::where('user_id',$iUserId)->orderby("created_at","desc")->paginate(self::PAGESIZE);
		Return View::make('admin.aspirin.screening.show')->with('oScreening',$oScreening);
	}

	//风险筛查记录导出
	public function getExportScreening(){
		set_time_limit(0);
		header('Cache-control:public');
		header('Pragma:public');
		header('Content-type:application/vnd.ms-excel');
		header('Content-Disposition:attachment;filename=screening_'.date('His').'.csv');
		header('Content-Type:APPLICATION/OCTET-STREAM');
		ob_start();
		$header_str = iconv("utf-8",'gbk',"id,登录用户id,登录用户名,性别,年龄,体重(kg),身高(cm),是否吸烟(1:否;2:是;0:未选择;),是否绝经(1:否;2:是;0:未选择;),是否有心脑血管家庭史(1:否;2:是;0:未选择;),是否有确认的糖尿病(1:否;2:是;3:不清楚;0:未选择;),是否在服用降糖药(1:否;2:是;0:未选择;),是否有已诊断的高血脂(1:否;2:是;3:不清楚;0:未选择;),是否在用降脂药(1:否;2:是;0:未选择;),治疗前总胆固醇水平(按量表划分的6个范围),是否有已诊断的高血压(1:否;2:是;3:不清楚;0:未选择;),是否在用降压药(1:否;2:是;0:未选择;), 治疗前收缩压（高压mmHg）, 治疗前舒张压(按量表划分的6个范围),治疗后收缩压(按量表划分的2个范围),是否有冠心病或心肌梗死病史,是否有脑卒中病史,是否患有胃肠疾病,是否正在服用抗血小板药物,抗血小板药物名称(1:阿司匹林;2:氯吡格雷;3:其他;),BMI,百分比,风险等级,危险因素,使用来源,创建时间\n");
		$file_str="";
		$oScreening = AspirinScreening::join('user','aspirin_screening.user_id','=','user.id')
			->select('aspirin_screening.*','user.user_name')
			->orderBy('aspirin_screening.created_at','desc')
			->get();
		if($oScreening){
			foreach($oScreening as $v){
				if($v->sex == 1){
					$sSex = "男";
				}else{
					$sSex = "女";
				}
				if($v->flag == 1){
					$sFlag = "低";
				}elseif($v->flag == 2){
					$sFlag = "中";
				}elseif($v->flag == 3){
					$sFlag = "高";
				}else{
					$sFlag = "";
				}
				if($v->device_type == 1){
					$sDeviceType = "web分享页";
				}else{
					$sDeviceType = "app";
				}
				$file_str .= $v->id.',';
				$file_str .= $v->user_id.',';
				$file_str .= $v->user_name.',';
				$file_str .= $sSex.',';
				$file_str .= $v->age.',';
				$file_str .= $v->weight.',';
				$file_str .= $v->height.',';
				$file_str .= $v->question1.',';
				$file_str .= $v->question0.',';
				$file_str .= $v->question2.',';
				$file_str .= $v->question3.',';
				$file_str .= $v->question31.',';
				$file_str .= $v->question4.',';
				$file_str .= $v->question41.',';
				$file_str .= $v->question42.',';
				$file_str .= $v->question5.',';
				$file_str .= $v->question51.',';
				$file_str .= $v->question52.',';
				$file_str .= $v->question53.',';
				$file_str .= $v->question54.',';
				$file_str .= $v->question6.',';
				$file_str .= $v->question7.',';
				$file_str .= $v->question8.',';
				$file_str .= $v->question9.',';
				$file_str .= str_ireplace(',','，',$v->question91).',';
				$file_str .= $v->bmi.',';
				$file_str .= $v->percent.',';
				$file_str .= $sFlag.',';
				$file_str .= str_ireplace(',','，',$v->danger).',';
				$file_str .= $sDeviceType.',';
				$file_str .= $v->created_at."\n";
			}
		}
		$file_str= iconv("utf-8",'GBK//TRANSLIT',$file_str);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
}