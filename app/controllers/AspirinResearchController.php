<?php

//pc端专项基金科研申请

class AspirinResearchController extends BaseController {

	private $iUserId = 0;
	private $iRoleId = 0;

	public function __construct(){
		$this->iUserId = Session::get('userid');
		$this->iRoleId = Session::get('roleid');
	}

	//科研培训简介
	public function getBrief()
	{
		return View::make('front.aspirin.research.brief');
	}

	//我的科研培训列表
	public function getMyApplyList()
	{
		$iUid = $this->iUserId;
		$oUser = User::find($iUid);
		$oApply = AspirinResearchApply::where('user_id',$iUid)->orderby('created_at','desc')->get();
		return View::make('front.aspirin.research.myapplylist')->with('oApply',$oApply)->with('oUser',$oUser);
	}

	//我的科研培训详情
	public function getMyApplyShow($iId)
	{
		$oApply = AspirinResearchApply::find($iId);
		return View::make('front.aspirin.research.myapplyshow')->with('oApply',$oApply);
	}

	//科研培训申请
	public function getApply()
	{
		$oUser = User::find($this->iUserId);
		return View::make('front.aspirin.research.apply')->with('oUser',$oUser);
	}

	//科研培训申请提交
	public function postSubmitApply()
	{
		$oUser = User::find($this->iUserId);
		if($oUser->role_id == 2 || $oUser->card_auth_flag == 2){
			$aInput = Input::all();
			$aApply = array(
				'user_id'            => $this->iUserId,
				'apply_name'         => trim($aInput['apply_name']),
				'apply_city'   		 => trim($aInput['apply_city']),
				'apply_birthday'     => trim($aInput['apply_birthday']),
				'apply_title'        => trim($aInput['apply_title']),
				'apply_tel'          => trim($aInput['apply_tel']),
				'apply_email'        => trim($aInput['apply_email']),
				'apply_company'      => trim($aInput['apply_company']),
				'apply_address'      => trim($aInput['apply_address']),
				'apply_postcode'     => trim($aInput['apply_postcode']),
				'education_background' => trim($aInput['education_background']),
				'work_experience'    => trim($aInput['work_experience']),
				'research_experience' => trim($aInput['research_experience']),
				'question1'          => isset($aInput['question1'])?trim($aInput['question1']):'',
				'question2'          => isset($aInput['question2'])?trim($aInput['question2']):'',
				'question3'          => trim($aInput['question3']),
				'created_at'         => date('Y-m-d H:i:s',time())
			);
			$oApply = new AspirinResearchApply($aApply);
			$oApply->save();
			echo 'success';die;
		}else{
			echo 'error';die;
		}
	}

	//科研培训结果公示首页
	public function getResult()
	{
		$iUid = $this->iUserId;
		$iFlag = 0;
		$oApplyLog = AspirinResearchApply::where('user_id',$iUid)->where('apply_type',1)->first();
		if($oApplyLog){
			$iFlag = 1;     //已有入围的科研
		}
		return View::make('front.aspirin.research.result')->with('iFlag',$iFlag);
	}

	//科研培训结果公示列表页
	public function getResultList()
	{
		$sKeyword = Input::get('keyword','');
		$oApply = AspirinResearchApply::where('apply_type',1);
		if(!empty($sKeyword)){
			$oApply = $oApply->where(function($query) use($sKeyword)
			{
				$query->where('apply_name','like','%'.$sKeyword.'%')
				->orWhere('apply_title','like','%'.$sKeyword.'%')
				->orWhere('apply_company','like','%'.$sKeyword.'%');
			});
		}
		$oApply = $oApply->orderby('created_at','desc')->get();
		return View::make('front.aspirin.research.resultlist')->with('oApply',$oApply);
	}

	//科研培训结果公示详情
	public function getResultShow($iId)
	{
		$oApply = AspirinResearchApply::find($iId);
		return View::make('front.aspirin.research.resultshow')->with('oApply',$oApply);
	}

	//个人中心--科研申请
	public function getMyList()
	{
		$iUid = $this->iUserId;
		$oApply = AspirinResearchApply::where('user_id',$iUid)->orderby('created_at','desc')->get();
		return View::make('front.user.researchlist')->with('oApply',$oApply);
	}

	public function getMyShow($iId)
	{
		$oApply = AspirinResearchApply::find($iId);
		return View::make('front.user.researchshow')->with('oApply',$oApply);
	}

}