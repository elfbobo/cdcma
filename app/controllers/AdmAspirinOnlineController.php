<?php

class AdmAspirinOnlineController extends BaseController
{
	
	CONST  PAGESIZE = 10;

	/*******************在线会议主题**************************/
	/**
	 * 列出所有季度会议主题
	 */
	public function getOnlineList()
	{
		$oOnlineList = AspirinOnline::orderBy('id','desc')->paginate(self::PAGESIZE);
		return View::make('admin.aspirin.online.onlinelist')->with('oOnlineList',$oOnlineList);
	}

	public function getOnlineAdd()
	{
		return View::make('admin.aspirin.online.onlineadd');
	}

	public function postOnlineAdd()
	{
		$sOnlineTitle = Input::get('online_title','');
		$sOnlineDes = Input::get('online_des','');
		$iShowFlag = Input::get('show_flag',0);
		$sOnlineBanner = Input::get('online_banner','');
		$sOnlineThumb = Input::get('online_thumb','');
		$sStartTime = trim(Input::get('show_start_time',''));
		$sEndTime = trim(Input::get('show_end_time',''));
		$sPeriod = Input::get('time_period','');
		$sPeriod = str_replace('：',':',$sPeriod);
		$sPeriod = str_replace('，',',',$sPeriod);
		$iCatId = intval(Input::get('catid'));
		if(!in_array($iCatId, [1,2,3,4])){
			return $this->showMessage('请选择科室分类','/admaspirinonline/online-add');
		}
		if(empty($sOnlineTitle)||empty($sOnlineDes)||empty($sOnlineBanner)||empty($sOnlineThumb)
		||empty($sStartTime)||empty($sEndTime)){//||empty($sPeriod)
			return $this->showMessage('请将信息填写完整','/admaspirinonline/online-add');
		}
		//获取展示时间段内除周六日外的所有日期
		$aDate = AspirinOnline::getOnlineDate($sStartTime,$sEndTime);
		//获取时间段数组
		$aPeriod = explode(',',$sPeriod);
		$aPeriod =  array_filter($aPeriod);      //数组去空
		$aOnline = array(
			'catid'         => $iCatId,
			'online_title'	=> trim($sOnlineTitle),
			'online_des'	=> trim($sOnlineDes),
			'online_banner'	=> trim($sOnlineBanner),
			'online_thumb'  => trim($sOnlineThumb),
			'show_start_time'=> $sStartTime,
			'show_end_time' => $sEndTime,
			'show_flag'     => intval($iShowFlag),
			'created_at'    => date('Y-m-d H:i:s',time())
		);
		$oOnline = new AspirinOnline($aOnline);
		$oOnline->save();
		foreach($aDate as $date){
			foreach($aPeriod as $period){
				$aOnlineTime = array(
					'online_id'   => $oOnline->id,
					'online_date' => $date,
					'time_period' => $period,
					'created_at'  => date('Y-m-d H:i:s',time())
				);
				$oOnlineTime = new AspirinOnlineTime($aOnlineTime);
				$oOnlineTime->save();
			}
		}
		return $this->showMessage('新增成功！','/admaspirinonline/online-list');
	}

	public function getOnlineEdit($iId)
	{
		$oOnline = AspirinOnline::find($iId);
		if(!$oOnline){
			return $this->showMessage('该信息不存在或者已经被删除','/admaspirinonline/online-list');
		}
		//获取会议时间段
		$aOnlineTime = AspirinOnlineTime::where('online_id',$iId)->lists('time_period');
		$aOnlineTime = array_flip(array_flip($aOnlineTime)); 
		$sTimePeriod = implode(',', $aOnlineTime);
		return View::make('admin.aspirin.online.onlineedit')
			->with('oOnline',$oOnline)
			->with('sTimePeriod',$sTimePeriod)
			->with('iId',$iId);
	}

	public function postOnlineEdit()
	{
		$iId = Input::get('iId');
		$oOnline = AspirinOnline::find($iId);
		if(!$oOnline){
			return $this->showMessage('该信息不存在或者已经被删除','/admaspirinonline/online-list');
		}
		/*$oLog = AspirinOnlineSpeaker::where('online_id',$iId)->first();
		if(count($oLog)){
			return $this->showMessage('该会议已有讲者报名参加，信息不可修改','/admaspirinonline/online-list');
		}*/
		$sOnlineTitle = Input::get('online_title','');
		$sOnlineDes = Input::get('online_des','');
		$iShowFlag = Input::get('show_flag',0);
		$sOnlineBanner = Input::get('online_banner','');
		$sOnlineThumb = Input::get('online_thumb','');
		$sStartTime = trim(Input::get('show_start_time',''));
		$sEndTime = trim(Input::get('show_end_time',''));
		$sPeriod = Input::get('time_period','');
		$sPeriod = str_replace('：',':',$sPeriod);
		$sPeriod = str_replace('，',',',$sPeriod);
		if(empty($sOnlineTitle)||empty($sOnlineDes)||empty($sOnlineBanner)||empty($sOnlineThumb)){
			// ||empty($sStartTime)||empty($sEndTime)||empty($sPeriod)){
			return $this->showMessage('请将信息填写完整','/admaspirinonline/online-edit/'.$iId);
		}
		//获取展示时间段内除周六日外的所有日期
		$aDate = AspirinOnline::getOnlineDate($sStartTime,$sEndTime);
		//获取时间段数组
		$aPeriod = explode(',',$sPeriod);
		$aPeriod =  array_filter($aPeriod);      //数组去空
		$oOnline->online_title    = trim($sOnlineTitle);
		$oOnline->online_des      = trim($sOnlineDes);
		$oOnline->show_flag       = intval($iShowFlag);
		$oOnline->online_banner   = trim($sOnlineBanner);
		$oOnline->online_thumb    = trim($sOnlineThumb);
		//	$oOnline->show_start_time = $sStartTime;
		//	$oOnline->show_end_time   = $sEndTime;
		$oOnline->save();
		//删除原有时间段记录
		/*$oOnlineTime = AspirinOnlineTime::where('online_id',$iId)->get();
		foreach($oOnlineTime as $k=>$v){
			$v->delete();
		}
		foreach($aDate as $date){
			foreach($aPeriod as $period){
				$aOnlineTime = array(
						'online_id'   => $iId,
						'online_date' => $date,
						'time_period' => $period,
						'created_at'  => date('Y-m-d H:i:s',time())
				);
				$oOnlineTime = new AspirinOnlineTime($aOnlineTime);
				$oOnlineTime->save();
			}
		}*/
		return $this->showMessage('保存成功,如需修改时间信息请联系后台负责人','/admaspirinonline/online-list');
	}

	public function getOnlineDelete($iId)
	{
		$oOnline = AspirinOnline::find($iId);
		if(!$oOnline){
			return $this->showMessage('该信息不存在或者已经被删除','/admaspirinonline/online-list');
		}
		/*$oLog = AspirinOnlineSpeaker::where('online_id',$iId)->first();
		if(count($oLog)){
			return $this->showMessage('该会议已有讲者报名参加，信息不可删除','/admaspirinonline/online-list');
		}*/
		//删除原有时间段记录
		$oOnlineTime = AspirinOnlineTime::where('online_id',$iId)->get();
		foreach($oOnlineTime as $k=>$v){
			$oOnlineListener = AspirinOnlineListener::where('online_time_id',$v->id)->get();
			foreach($oOnlineListener as $m=>$n){
				$n->delete();
			}
			$v->delete();
		}
		//删除报名讲者
		$oOnlineSpeaker = AspirinOnlineSpeaker::where('online_id',$iId)->get();
		foreach($oOnlineSpeaker as $k=>$v){
			$v->delete();
		}
		$oOnline->delete();
		return $this->showMessage('删除成功','/admaspirinonline/online-list');
	}

	public function getTimeList($iId)
	{
		$oOnlineTime = AspirinOnlineTime::where('online_id',$iId)->orderBy('speaker_count','desc')->orderBy('id','asc')->paginate(self::PAGESIZE);
		return View::make('admin.aspirin.online.timelist')->with('oOnlineTime',$oOnlineTime);
	}

	public function getSpeakerList($iTimeId)
	{
		$oOnlineSpeaker = AspirinOnlineSpeaker::where('online_time_id',$iTimeId)->orderby('created_at','desc')->paginate(self::PAGESIZE);
		foreach($oOnlineSpeaker as $k=>$v){
			$v->doc_name = '';
			$v->doc_hospital = '';
			$oUser = User::select('user_name','user_company','user_company_name')->where('id',$v->speaker_id)->first();
			if($oUser){
				$v->doc_name = $oUser->user_name;
				$sUserCompany = $oUser->user_company;
				if(is_numeric($sUserCompany)&&$sUserCompany!=0){
					$sUserCompany = Hospital::where('id',$sUserCompany)->pluck('name');
				}
				if(!$sUserCompany){
					$sUserCompany = $oUser->user_company_name;
				}
				$v->doc_hospital = $sUserCompany;
			}
		}
		return View::make('admin.aspirin.online.speakerlist')->with('oOnlineSpeaker',$oOnlineSpeaker);
	}

	public function getSpeakerShow($iTimeId,$iId)
	{
		$oOnlineSpeaker = AspirinOnlineSpeaker::find($iId);
		if(!$oOnlineSpeaker){
			return $this->showMessage('该信息不存在或者已经被删除','/admaspirinonline/speaker-list/'.$iTimeId);
		}
		return View::make('admin.aspirin.online.speakershow')->with('oOnlineSpeaker',$oOnlineSpeaker)->with('iId',$iId);
	}

	public function postSpeakerEdit()
	{
		$iId = Input::get('iId');
		$iTimeId = Input::get('iTimeId');
		$sVideoUrl = Input::get('video_url','');
		
		$oOnlineSpeaker = AspirinOnlineSpeaker::find($iId);
		if(!$oOnlineSpeaker){
			return $this->showMessage('该信息不存在或者已经被删除','/admaspirinonline/speaker-list/'.$iTimeId);
		}
		$oOnlineSpeaker->video_url = $sVideoUrl;
		$oOnlineSpeaker->save();
		return $this->showMessage('保存成功','/admaspirinonline/speaker-list/'.$iTimeId);
	}

	public function getListenerList($iTimeId,$iSpeakerId)
	{
		$oOnlineListener = AspirinOnlineListener::where('online_time_id',$iTimeId)
			->where('speaker_id',$iSpeakerId)
			->orderby('created_at','desc')
			->paginate(self::PAGESIZE);
		foreach($oOnlineListener as $k=>$v){
			$v->doc_name = '';
			$v->doc_hospital = '';
			$oUser = User::select('user_name','user_company','user_company_name')->where('id',$v->listener_id)->first();
			if($oUser){
				$v->doc_name = $oUser->user_name;
				$sUserCompany = $oUser->user_company;
				if(is_numeric($sUserCompany)&&$sUserCompany!=0){
					$sUserCompany = Hospital::where('id',$sUserCompany)->pluck('name');
				}
				if(!$sUserCompany){
					$sUserCompany = $oUser->user_company_name;
				}
				$v->doc_hospital = $sUserCompany;
			}
		}
		return View::make('admin.aspirin.online.listenerlist')->with('oOnlineListener',$oOnlineListener);
	}
	/*******************在线会议主题**************************/


	/*******************在线会议首页banner********************/
	/**
	 * 在线会议首页banner
	 */
	public function getBannerList()
	{
		$oBannerList = AspirinOnlineBanner::orderBy('created_at','desc')->paginate(self::PAGESIZE);
		return View::make('admin.aspirin.online.bannerlist')->with('oBannerList',$oBannerList);
	}

	public function getBannerAdd()
	{
		return View::make('admin.aspirin.online.banneradd');
	}

	public function postBannerAdd()
	{
		$sBannerTitle = Input::get('banner_title','');
		$sBannerDes = Input::get('banner_des','');
		$sBannerThumb = Input::get('banner_thumb','');
		$iShowFlag = Input::get('show_flag',0);
		if(empty($sBannerTitle)||empty($sBannerDes)||empty($sBannerThumb)){
			return $this->showMessage('请将信息填写完整','/admaspirinonline/banner-add');
		}
		$aBanner = array(
			'banner_title'	=> trim($sBannerTitle),
			'banner_des'	=> trim($sBannerDes),
			'banner_thumb'	=> trim($sBannerThumb),
			'show_flag'     => $iShowFlag,
			'created_at'    => date('Y-m-d H:i:s',time())
		);
		$oBanner = new AspirinOnlineBanner($aBanner);
		$oBanner->save();
		return $this->showMessage('新增成功！','/admaspirinonline/banner-list');
	}

	public function getBannerEdit($iId)
	{
		$oBanner = AspirinOnlineBanner::find($iId);
		if(!$oBanner){
			return $this->showMessage('该信息不存在或者已经被删除','/admaspirinonline/banner-list');
		}
		return View::make('admin.aspirin.online.banneredit')->with('oBanner',$oBanner)->with('iId',$iId);
	}

	public function postBannerEdit()
	{
		$iId = Input::get('iId');
		$oBanner = AspirinOnlineBanner::find($iId);
		if(!$oBanner){
			return $this->showMessage('该信息不存在或者已经被删除','/admaspirinonline/banner-list');
		}
		$sBannerTitle = Input::get('banner_title','');
		$sBannerDes = Input::get('banner_des','');
		$sBannerThumb = Input::get('banner_thumb','');
		$iShowFlag = Input::get('show_flag',0);
		if(empty($sBannerTitle)||empty($sBannerDes)||empty($sBannerThumb)){
			return $this->showMessage('请将信息填写完整','/admaspirinonline/banner-edit/'.$iId);
		}
		$oBanner->banner_title = trim($sBannerTitle);
		$oBanner->banner_des = trim($sBannerDes);
		$oBanner->banner_thumb = trim($sBannerThumb);
		$oBanner->show_flag  = intval($iShowFlag);
		$oBanner->save();
		return $this->showMessage('保存成功','/admaspirinonline/banner-list');
	}

	public function getBannerDelete($iId)
	{
		$oBanner = AspirinOnlineBanner::find($iId);
		if(!$oBanner){
			return $this->showMessage('该信息不存在或者已经被删除','/admaspirinonline/banner-list');
		}
		$oBanner->delete();
		return $this->showMessage('删除成功','/admaspirinonline/banner-list');
	}
	/*******************在线会议首页banner********************/


	/*******************在线会议主题ppt课件********************/
	/**
	 * ppt多图片上传
	 */
	public function getPpt()
	{
		$oPpt = AspirinOnlinePpt::orderBy('id','asc')->paginate(self::PAGESIZE);
		return View::make('admin.aspirin.online.pptinfo')->with('oPpt',$oPpt);
	}

	public function getPptinfoAdd()
	{
		return View::make('admin.aspirin.online.pptinfoadd');
	}

	public function postPptinfoAddDo()
	{
		$sPptTitle = Input::get('ppt_title','');
		$sPptThumb = Input::get('ppt_thumb','');
		$sPptUrl = Input::get('ppt_url','');
		$iCatId = intval(Input::get('catid'));
		if(!in_array($iCatId, [1,2,3,4])){
			return $this->showMessage('请选择科室分类','/admaspirinonline/pptinfo-add');
		}
		if(empty($sPptTitle)||empty($sPptThumb)||empty($sPptUrl)){
			return $this->showMessage('请将信息填写完整','/admaspirinonline/pptinfo-add');
		}
		$aPPt = array(
			'catid'     => $iCatId,
			'ppt_title'	=> trim($sPptTitle),
			'ppt_thumb'	=> trim($sPptThumb),
			'ppt_url'	=> trim($sPptUrl),
			'created_at'    => date('Y-m-d H:i:s',time())
		);
		$oPpt = new AspirinOnlinePpt($aPPt);
		$oPpt->save();
		return $this->showMessage('添加PPT成功！','/admaspirinonline/ppt');
	}

	public function getPptinfoEdit($iPptId=0)
	{
		$oPpt = AspirinOnlinePpt::find($iPptId);
		if(!$oPpt){
			return $this->showMessage('该PPT不存在或者已经被删除','/admaspirinonline/ppt');
		}
		return View::make('admin.aspirin.online.pptinfoedit')->with('oPpt',$oPpt);
	}

	public function postPptinfoEditDo($iPptId=0)
	{
		$oPpt = AspirinOnlinePpt::find($iPptId);
		if(!$oPpt){
			return $this->showMessage('该PPT不存在或者已经被删除','/admaspirinonline/ppt');
		}
		$sPptTitle = Input::get('ppt_title','');
		$sPptThumb = Input::get('ppt_thumb','');
		$sPptUrl = Input::get('ppt_url','');
		$iCatId = intval(Input::get('catid'));
		if(!in_array($iCatId, [1,2,3,4])){
			return $this->showMessage('请选择科室分类','/admaspirinonline/pptinfo-edit/'.$iPptId);
		}
		if(empty($sPptTitle)||empty($sPptThumb)||empty($sPptUrl)){
			return $this->showMessage('请将信息填写完整','/admaspirinonline/pptinfo-edit/'.$iPptId);
		}
		$oPpt->catid = $iCatId;
		$oPpt->ppt_title = $sPptTitle;
		$oPpt->ppt_thumb = $sPptThumb;
		$oPpt->ppt_url = $sPptUrl;
		$oPpt->save();
		return $this->showMessage('保存成功','/admaspirinonline/ppt');
	}

	public function getPptinfoDelete($iPptId=0){
		$oPpt = AspirinOnlinePpt::find($iPptId);
		if(!$oPpt){
			return $this->showMessage('该PPT不存在或者已经被删除','/admaspirinonline/ppt');
		}
		$oPpt->delete();
		$oImages = AspirinOnlinePptImage::where('ppt_id',$iPptId)->get();
		if($oImages){
			foreach($oImages as $k=>$v){
				$v->delete();
			}
		}
		return $this->showMessage('删除成功','/admaspirinonline/ppt');
	}

	public function getPptList($iPptId)
	{
		$oPptImage = AspirinOnlinePptImage::where('ppt_id',$iPptId)->orderBy('id','asc')->get();
		return View::make('admin.aspirin.online.pptlist')->with('oPptImage',$oPptImage)->with('iPptId',$iPptId);
	}

	public function getPptAdd($iPptId)
	{
		return View::make('admin.aspirin.online.pptadd')->with('iPptId',$iPptId);
	}

	public function postUploadPptImage($iPptId)
	{
		if($_FILES['Filedata']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['Filedata']['name'];
			$attach_fileext = get_filetype($attach_filename);
			$rand_name = date('YmdHis', time()).rand(1000,9999);
			$sFileName = $rand_name.'.'.$attach_fileext;
			$sPath = "/upload/online/pptimage/".date('Ymd',time());
			$sRealPath = public_path().$sPath;
			mkdirs($sRealPath);
			move_uploaded_file($_FILES['Filedata']['tmp_name'], $sRealPath.DS.$sFileName);
			$sFileNameS = $rand_name . '_s.' . $attach_fileext;
			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 1000, 1000 );
			$sFileUrl = $sPath.'/'.$sFileName;
			//保存到数据库
			$aImage = array(
				'ppt_id' => $iPptId,
				'image_url'	=> $sFileUrl,
				'created_at'=> date('Y-m-d H:i:s',time())
			);
			$oImage = new AspirinOnlinePptImage($aImage);
			$oImage->save();
			echo '成功';
			die;
		}
	}
	
	/**
	 * 删除PPT图片
	 * @param number $iOnlineId
	 */
	public function getPptDelete($iImageId,$iPptId)
	{
		$oImage = AspirinOnlinePptImage::find($iImageId);
		if(!$oImage){
			return $this->showMessage('该PPT图片不存在或者已经被删除','/admaspirinonline/ppt-list/'.$iPptId);
		}
		$oImage->delete();
		return $this->showMessage('删除PPT图片成功','/admaspirinonline/ppt-list/'.$iPptId);
	}

	/*******************在线会议主题ppt课件********************/
	//上传会议主题图片
	public function postUploadOnlineBanner()
	{
		if($_FILES['upload_file']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['upload_file']['name'];
			$attach_fileext = get_filetype($attach_filename);
			$rand_name = date('YmdHis', time()).rand(1000,9999);
			$sFileName = $rand_name.'.'.$attach_fileext;
			$sPath = "/upload/online/".date('Ymd',time());
			$sRealPath = public_path().$sPath;
			mkdirs($sRealPath);
			move_uploaded_file($_FILES['upload_file']['tmp_name'], $sRealPath.DS.$sFileName);
			$sFileNameS = $rand_name . '_s.' . $attach_fileext;
			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 300, 180 );
			$sFileUrl = $sPath.'/'.$sFileNameS;
			$json = array('online_banner'=>$sFileUrl);
			echo json_encode($json);
			die;
		}
	}

	public function postUploadOnlineThumb()
	{
		if($_FILES['upload_file0']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['upload_file0']['name'];
			$attach_fileext = get_filetype($attach_filename);
			$rand_name = date('YmdHis', time()).rand(1000,9999);
			$sFileName = $rand_name.'.'.$attach_fileext;
			$sPath = "/upload/online/".date('Ymd',time());
			$sRealPath = public_path().$sPath;
			mkdirs($sRealPath);
			move_uploaded_file($_FILES['upload_file0']['tmp_name'], $sRealPath.DS.$sFileName);
			$sFileNameS = $rand_name . '_s.' . $attach_fileext;
			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 180, 180 );
			$sFileUrl = $sPath.'/'.$sFileNameS;
			$json = array('online_thumb'=>$sFileUrl);
			echo json_encode($json);
			die;
		}
	}

	//上传ppt课件
	public function postUploadPpt()
	{
		if($_FILES['upload_file_ppt']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['upload_file_ppt']['name'];
			$attach_fileext = get_filetype($attach_filename);
			$rand_name = date('YmdHis', time()).rand(1000,9999);
			$sFileName = $rand_name.'.'.$attach_fileext;
			$sPath = "/upload/online/ppt";
			// $sTempPath = "/upload/elearning/ppt/temp";
			$sRealPath = public_path().$sPath;
			// $sTempRealPath = public_path().$sTempPath;
			mkdirs($sRealPath);
			// mkdirs($sTempRealPath);
			move_uploaded_file($_FILES['upload_file_ppt']['tmp_name'], $sRealPath.DS.$sFileName);
			// copy($sRealPath.DS.$sFileName, $sTempRealPath.DS.$sFileName);
			$sFileUrl = $sPath.'/'.$sFileName;
			$json = array('url'=>Config::get('app.url').$sFileUrl);
			echo json_encode($json);
			die;
		}
	}

	//上传首页banner
	public function postUploadBanner()
	{
		if($_FILES['upload_file']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['upload_file']['name'];
			$attach_fileext = get_filetype($attach_filename);
			$rand_name = date('YmdHis', time()).rand(1000,9999);
			$sFileName = $rand_name.'.'.$attach_fileext;
			$sPath = "/upload/online/banner/".date('Ymd',time());
			$sRealPath = public_path().$sPath;
			mkdirs($sRealPath);
			move_uploaded_file($_FILES['upload_file']['tmp_name'], $sRealPath.DS.$sFileName);
			$sFileNameS = $rand_name . '_s.' . $attach_fileext;
			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 300, 180 );
			$sFileUrl = $sPath.'/'.$sFileNameS;
			$json = array('banner_thumb'=>$sFileUrl);
			echo json_encode($json);
			die;
		}
	}


	/**
	 * 达到可签署协议条件，提送需签署协议内容提醒。
	 * @param number $iVideoId
	 */
	public function getPushNotice($iId=0){
		$oOnline = AspirinOnlineSpeaker::where("online_time_id", $iId)->first();
		$oOnlineTime = AspirinOnlineTime::where('id',$iId)->first();
		$oUser = User::find($oOnline->speaker_id);
		return View::make('admin.aspirin.online.pushnotice')->with('oOnline',$oOnline)->with('oOnlineTime',$oOnlineTime)->with('oUser',$oUser);
	}

	/**
	 * 达到可签署协议条件，提送需签署协议内容提醒。
	 * @param number $iVideoId
	 */
	public function postPushNoticeCheck($iId=0){
		$agreement_txt = trim(Input::get('agreement_txt',''));
		if($agreement_txt == ''){
			return $this->showMessage('请填写协议内容','/admaspirinonline/push-notice/'.$iId);
		}
		$iUid = intval(Input::get('user_id',0));
		$notice_type = intval(Input::get('notice_type',160));
		$detail_id = intval(Input::get('detail_id',$iId));
		$notice_content = trim(Input::get('notice_content',''));
		$aNotice = array(
			'user_id'		=> $iUid,
			'notice_content'=> $notice_content,
			'notice_type'	=> $notice_type,
			'detail_id'		=> $iId,
			'agreement_txt'	=> $agreement_txt
		);
		$oNotice = new AspirinUserNotice($aNotice);
		$oNotice->save();
		return $this->showMessage('提交成功','/admaspirinonline/speaker-list/'.$iId);
	}

    /**
     * 获取广告列表
     * @return mixed
     */
	public function getAdList()
    {
        $oVideos = AspirinVideoAd::where('status', 1)
            ->orderBy('index', 'asc')
            ->orderBy('created_at', 'asc')
            ->paginate(self::PAGESIZE);

        return View::make('admin.aspirin.online.adlist')->with('oVideos', $oVideos);
    }

    /**
     * 广告视频上传
     * @return mixed
     */
    public function postAdUpload()
    {
        if($_FILES['upload_file']['error']>0){
            $error = $_FILES['thumb']['error'];
        }else{
            $attach_filename = $_FILES['upload_file']['name'];
            $attach_fileext = get_filetype($attach_filename);
            $rand_name = date('YmdHis', time()) . rand(1000, 9999);
            $sFileName = $rand_name . '.' . $attach_fileext;
            $sPath = "/upload/aspirin/online/ad/" . date('Ymd', time());
            $sRealPath = public_path() . $sPath;
            mkdirs($sRealPath);
            move_uploaded_file($_FILES['upload_file']['tmp_name'], $sRealPath . DS . $sFileName);

            $sFileUrl = $sPath . '/' . $sFileName;

            $json = ['status' => true, 'attach_filename'=>$attach_filename, 'file_url'=>$sFileUrl, 'file_name'=>pathinfo($attach_filename, PATHINFO_FILENAME)];
            echo json_encode($json);
            die;
        }

        echo json_encode(['status' => false, 'msg' => $error ?: '上传异常']);
        die;
    }

    /**
     * 编辑广告视频
     * @return mixed
     */
    public function getAdAdd()
    {
        $index = AspirinVideoAd::max('index') +1;
        return View::make('admin.aspirin.online.adadd')->with('index', $index);
    }

    /**
     * 保存添加的广告信息
     * @return mixed
     */
    public function postAdAdd()
    {
        $data = Input::all();
        $info = new AspirinVideoAd();
        unset($data['upload_file']);

        foreach ($data as $name => $val) {
            $info->$name = $val;
        }
        if (isset($data['filepath'])) {
            $info->ext = pathinfo($data['filepath'], PATHINFO_EXTENSION);
        }
        $info->save();

        return $this->showMessage('保存成功', '/admaspirinonline/ad-list');
    }

    /**
     * 删除广告视频
     * @return mixed
     */
    public function getAdDelete()
    {
        $id = Input::get('id');

        $info = AspirinVideoAd::where('id', $id)->first();
        if ($info) {
            $info->delete();
        }

        return $this->showMessage('删除成功','/admaspirinonline/ad-list');
    }

    /**
     * 编辑广告视频
     * @return mixed
     */
    public function getAdEdit()
    {
        $id = Input::get('id');

        $oInfo = AspirinVideoAd::where('id', $id)->first();

        return View::make('admin.aspirin.online.adedit')->with('oInfo', $oInfo);
    }

    /**
     * 编辑资料
     * @param int $id
     * @return mixed
     */
    public function postAdEdit()
    {
        $id = Input::get('id', '');
        $index = Input::get('index', '');
        $title = Input::get('title', '');
        $filepath = Input::get('filepath', '');
        $info = AspirinVideoAd::where('id', $id)->first();
        if ($index) {
            $info->index = $index;
        }
        if ($title) {
            $info->title = $title;
        }
        if ($filepath) {
            $info->filepath = $filepath;
        }
        $info->save();

        return $this->showMessage('保存成功', '/admaspirinonline/ad-list');
    }
}
?>