<?php

//pc端专项基金
use Illuminate\Support\Facades\Redirect;

class AspirinOfflineController extends BaseController {

	const PAGESIZE = 6;
	private $iUserId = 0;
	private $iRoleId = 0;

	public function __construct(){
		$this->iUserId = Session::get('userid');
		$this->iRoleId = Session::get('roleid');
	}

	// 线下会议列表页
	public function getIndex()
	{
		$iPagesize = self::PAGESIZE;
		$oOffline = AspirinOffline::orderby('created_at','desc')->paginate($iPagesize);
		foreach($oOffline as $k=>$v){
			$oApplyLog = AspirinOfflineApply::where('offline_id',$v->id)->where('user_id',$this->iUserId)->first();
			$iApplyNum = AspirinOfflineApply::where('offline_id',$v->id)->count();
			if(count($oApplyLog)){
				$v ->hasapply = 1;  //已报名
				$v ->hasapplynum = $iApplyNum;  //已报名人数
			}else{
				$v ->hasapply = 2;  //已报名
				$v ->hasapplynum =$iApplyNum;//
			}
		}
		return View::make('front.user.offlinelist')->with('oOffline',$oOffline);
	}

	// 线下会议详细页
	public function getDetail($iId){
		$oOffline = AspirinOffline::find($iId);
		$oApplyCount = AspirinOfflineApply::where('offline_id',$iId)->get();
		if(count($oApplyCount) >= $oOffline->offline_count && $oOffline->offline_count != 0){
			$result = 'limitcount';		  //已达报名人数上限
		}
		$iUid = $this->iUserId;
		$oUser = User::find($iUid);
		$oApplyLog = AspirinOfflineApply::where('offline_id',$iId)->where('user_id',$iUid)->first();
		if(count($oApplyLog)){
			$result = "hasjoin";         //已报名
		}
		if($oOffline->apply_start_time > date('Y-m-d H:i:s',time())){
			$result = "nostart";        //该会议报名未开始
		}
		if($oOffline->apply_end_time <= date('Y-m-d H:i:s',time())){
			$result = "hasend";         //该会议报名已结束
		}
		$iGolds = $oUser->user_score;
		if($iGolds < $oOffline->offline_least_gold){
			$result = "noscore";        //积分不足
		}
		return View::make('front.user.offlinedetail')->with('oOffline',$oOffline)->with('result',$result);
	}

	// 线下会议报名
	public function postApplyCheck(){
		$iUid = $this->iUserId;
		$iOfflineId = Input::get('id');
		$oOffline = AspirinOffline::find($iOfflineId);
		$oUser = User::find($iUid);
		$iGoldCount = $oOffline->offline_least_gold;
		$oUser->user_score = $oUser->user_score - $iGoldCount;
		$oUser->save();
		//保存报名信息
		$aApply = array(
			'user_id'		=> $iUid,
			'offline_id'	=> $iOfflineId,
			'created_at'	=> date('Y-m-d H:i:s',time())
		);
		$oApply = new AspirinOfflineApply($aApply);
		$oApply->save();
		//减积分记录（type类型为3）
		$aGoldLog = array(
			'user_id'	=> $iUid,
			'gold_type'	=> 3,
			'gold_count'=> $iGoldCount,
			'source_id'	=> $iOfflineId
		);
		$oGoldLog = new AspirinUserGoldLog($aGoldLog);
		$oGoldLog->save();
		//记录报名成功通知
		$sPushContent = PushService::OUTLINE_APPLY_SUCCESS;
		$sPushContent = str_replace('{{title}}', $oOffline->offline_title, $sPushContent);
		$aNotice = array(
			'user_id'		=> $iUid,
			'notice_content'=> $sPushContent,
			'notice_type'	=> 41,
			'detail_id'		=> $iOfflineId
		);
		$oNotice = new AspirinUserNotice($aNotice);
		$oNotice->save();
		echo 'success';die;
	}
	
}