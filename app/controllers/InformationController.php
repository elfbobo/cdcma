<?php

/**
 * 
 * @author zgq
 */
class  InformationController extends BaseController {

    public function getIndex()
	{ 
		$iUid = Session::get('userid'); 
		$oUserLoginTime =UserInformationTime::where('user_id',$iUid)->first();
		if(count($oUserLoginTime)){
			$oUserLoginTime->user_time = time();
			$oUserLoginTime->save();
		}else{
			$oLoginTime = new UserInformationTime();
			$oLoginTime->user_id =$iUid;
			$oLoginTime->user_time = time();
			$oLoginTime->save();
		}
		$oAllInformation = Information::orderBy("created_at","desc")->paginate(10); 
		return  View::make('front.inform.informlist')->with('oAllInformation',$oAllInformation);	 
	}

	//添加点击量
    public function postAdd($id)
    { 
		$iUid = Session::get('userid');
		$oUserInformationLog = UserInformationLog::where('type',0)->where('information_id',$id)->where('user_id',$iUid)->first();
		if(!count($oUserInformationLog)){
			$oUserLog = new UserInformationLog();
			$oUserLog->user_id=$iUid;
			$oUserLog->information_id = $id;
			$oUserLog -> hits =1;
			$oUserLog->save();
		}else{
			$iHits = $oUserInformationLog ->hits;
			$oUserInformationLog->hits = $iHits +1;
			$oUserInformationLog->save();
		}
		$oInformation = Information::find($id);
		$hits = $oInformation -> info_hits;
		$oInformation ->info_hits = $hits+1;
		$oInformation ->save();
		echo 'success';die;
	}
    
    //新增对接微信群发消息列表    20160520 dll  add
	public function getList($type){
		$oMaterialNews = MaterialNews::leftJoin('material','material.id','=','material_news.material_id')
			->select('material_news.*','material.path')
			->where('news_type',$type)
			->orderBy("material_news.created_at","desc")
			->paginate(10);
		return  View::make('front.inform.materiallist')->with('oMaterialNews',$oMaterialNews)->with('type',$type);
	}
	
	//详情页   20160520 dll  add
	public function getShow($iId){
		$oMaterialNews = MaterialNews::find($iId);
		$iUid = Session::get('userid');
		$type = $oMaterialNews->news_type;
		$sContent = $oMaterialNews->content;
		//解决微信图片无法显示问题，用本地路径（title）替换微信链接（src），后台上传时已将服务器地址赋值给了title
		$replaceSrc = '';
		$sImgInfo = preg_replace('#<img(.+?)src="([^"]+?)"([^>]*?)>#',"<img$1src=\"$replaceSrc\"$3>",$sContent);
		$sImgRes = str_replace('src=""', '', $sImgInfo);
		$sNewContent = str_replace('title="', 'src="'.Config::get('app.url'), $sImgRes);
		//浏览量+1
		$oMaterialNews->increment('hits');
		$oUserInformationLog = UserInformationLog::where('type',$type)->where('information_id',$iId)->where('user_id',$iUid)->first();
		if(count($oUserInformationLog)){
			$oUserInformationLog->increment('hits');
		}else{
			$aLog = array(
				'user_id'    => $iUid,
				'information_id'  => $iId,
				'hits'       => 1,
				'type'       => $type,
				'created_at' => date('Y-m-d H:i:s',time())
			);
			$oUserLog = new UserInformationLog($aLog);
			$oUserLog->save();
		}
		return  View::make('front.inform.materialshow')->with('oMaterialNews',$oMaterialNews)->with('sNewContent',$sNewContent);
	}
		
}