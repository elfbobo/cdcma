<?php

class AdmMeetingManagerController extends BaseController {
	
	CONST  PAGESIZE = 10;

	/*
	|--------------------------------------------------------------------------
	| 会议管理模块
	|--------------------------------------------------------------------------
	*/

    /**
     * 会议列表页
     * @return mixed
     */
	public function MeetingList()
	{   
        $type = Input::get('type', '');
        $title = Input::get('title', '');
        $date = Input::get('date', date('Y-m-d'));

		$oMeeting = MeetingManage::where(function($query) use ($type, $title){
                if (!empty($type)) {
                    $query->where('type', $type);
                }
                if (!empty($title)) {
                    $query->where('title', 'like', "%{$title}%");
                }
            })
            ->orderBy('created_at', 'desc')->paginate(self::PAGESIZE);

		return View::make('admin.meetingmanager.meetinglist')->with('oMeeting',$oMeeting);
	}

    /**
     * 会议创建页
     * @return mixed
     */
    public function MeetingCreate()
    {
        return View::make('admin.meetingmanager.create');
    }

    /**
     * 保存会议编辑信息
     * @return mixed
     */
    public function doMeetingCreate()
    {
        $data = Input::all();
        // 表单验证
        $validator = Validator::make(
            $data,
            [
                'type' => 'required|numeric',
                //'title' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
            ],
            [
                'type.required' => '会议类型不能为空',
                //'title.required' => '标题不能为空',
                'start_date.required' => '会议开始时间不能为空',
                'end_date.required' => '会议结束时间不能为空',
            ]
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $message) {
                return $this->showMessage($message);
            }
        }

        // 如果结束时间为数字或 float 时按小时增加时间
        if (is_float($data['end_date']) || is_numeric($data['end_date'])) {
            // 将小时转换为分钟，并且在开始时间上增加时间生成结束时间
            $data['end_date'] = date('Y-m-d H:i:s', strtotime("+" . ($data['end_date'] * 60) . " minute", strtotime($data['start_date'])));
        }
        /*if (strtotime($data['end_date']) < strtotime($data['start_date'])) {
            return $this->showMessage('结束时间不能小于开始时间');
        }*/

        $info = new MeetingManage($data);
        $info->user_id = Session::get('userid');
        $info->save();

        return $this->showMessage('创建成功', '/admmeeting-manager');
    }

    /**
     * 会议编辑页
     * @return mixed
     */
	public function MeetingEdit($id = 0)
    {
        $info = MeetingManage::where('id', $id)->first();

        $dateLen = floor((strtotime($info['end_date'])-strtotime($info['start_date']))/3600);

        return View::make('admin.meetingmanager.edit')->with('oInfo', $info)->with('dateLen', $dateLen);
    }

    /**
     * 保存会议编辑信息
     * @param int $id
     * @return mixed
     */
	public function doMeetingEdit($id = 0)
    {
        $data = Input::all();
        // 表单验证
        $validator = Validator::make(
            $data,
            [
                'type' => 'required|numeric',
                //'title' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
            ],
            [
                'type.required' => '会议类型不能为空',
                //'title.required' => '标题不能为空',
                'start_date.required' => '会议开始时间不能为空',
                'end_date.required' => '会议结束时间不能为空',
            ]
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $message) {
                return $this->showMessage($message);
            }
        }
        /*
        if (strtotime($data['end_date']) < strtotime($data['start_date'])) {
            return $this->showMessage('结束时间不能小于开始时间');
        }*/

        // 如果结束时间为数字或 float 时按小时增加时间
        if (is_float($data['end_date']) || is_numeric($data['end_date'])) {
            // 将小时转换为分钟，并且在开始时间上增加时间生成结束时间
            $data['end_date'] = date('Y-m-d H:i:s', strtotime("+" . ($data['end_date'] * 60) . " minute", strtotime($data['start_date'])));
        }

        $info = MeetingManage::where('id', $id)->first();
        if ($info) {
            foreach ($data as $key => $value) {
                $info->$key = $value;
            }
            $info->user_id = Session::get('userid');
            $info->save();

            return $this->showMessage('保存成功', '/admmeeting-manager');
        }
    }

    /**
     * 删除会议
     * @param $id
     * @return mixed
     */
    public function DeleteMetting($id)
    {
        $info = MeetingManage::where('id', $id)->first();
        if ($info) {
            MeetingManageSign::where('meeting_manager_id', $id)->delete();
            MeetingManageSubject::where('meeting_manager_id', $id)->delete();
            $info->delete();
            return $this->showMessage('删除成功', '/admmeeting-manager');
        }
    }

    /**
     * 报名用户列表
     * @param $id
     * @return mixed
     */
    public function SignMettingList($id)
    {
        $title = Input::get('title', '');

        $oMeeting = MeetingManageSign::select(['meeting_manage_sign.*', 'user.user_nick', 'user.user_name','province.name as province_name', 'city.name as city_name'])
            ->where('meeting_manage_id', $id)
            ->where(function($query) use ($title){
                if (!empty($title)) {
                    $query->where('user_name', 'like', "%{$title}%");
                }
            })
            ->leftJoin('user', 'user.id', '=', 'meeting_manage_sign.user_id')
            ->leftJoin(DB::raw('hospital province'), 'meeting_manage_sign.province', '=', 'province.id')
            ->leftJoin(DB::raw('hospital city'), 'meeting_manage_sign.city', '=', 'city.id')
            ->orderBy('created_at', 'desc')->paginate(self::PAGESIZE);

        return View::make('admin.meetingmanager.signmeetinglist')->with('oMeeting',$oMeeting)->with('id',$id);
    }

    /**
     * 会议管理资料
     * @return mixed
     */
    public function MaterialList()
    {
        $title = Input::get('title', '');
        $type = Input::get('type', '');

        $oMaterial = MeetingManageMaterial::where('status', 1)
            ->where(function($query) use ($title, $type){
                if (!empty($type)) {
                    $query->where('type', $type);
                }
                if (!empty($title)) {
                    $query->where('title', 'like', "%{$title}%");
                }
            })
            ->orderBy('created_at', 'desc')
            ->paginate(self::PAGESIZE);

        return View::make('admin.meetingmanager.materiallist')->with('oMaterial', $oMaterial);
    }

    /**
     * 资料上传页面
     * @return mixed
     */
    public function MaterialUpload()
    {
        return View::make('admin.meetingmanager.materialupload');
    }

    /**
     * 会议资料上传
     * @return mixed
     */
    public function MaterialUploadFile()
    {
        if($_FILES['upload_file']['error']>0){
            $error = $_FILES['thumb']['error'];
        }else{
            $attach_filename = $_FILES['upload_file']['name'];
            $attach_fileext = get_filetype($attach_filename);
            $rand_name = date('YmdHis', time()) . rand(1000, 9999);
            $sFileName = $rand_name . '.' . $attach_fileext;
            $sPath = "/upload/meeting/manager/" . date('Ymd', time());
            $sRealPath = public_path() . $sPath;
            mkdirs($sRealPath);
            move_uploaded_file($_FILES['upload_file']['tmp_name'], $sRealPath . DS . $sFileName);

            $sFileUrl = $sPath . '/' . $sFileName;

            $json = ['status' => true, 'attach_filename'=>$attach_filename, 'file_url'=>$sFileUrl, 'file_name'=>pathinfo($attach_filename, PATHINFO_FILENAME)];
            echo json_encode($json);
            die;
        }

        echo json_encode(['status' => false, 'msg' => $error ?: '上传异常']);
        die;
    }

    /**
     * 保存上传的附件信息
     * @return mixed
     */
    public function doMaterialUpload()
    {
        $data = Input::all();
        $info = new MeetingManageMaterial();
        unset($data['upload_file']);

        foreach ($data as $name => $val) {
            $info->$name = $val;
        }
        if (isset($data['filepath'])) {
            $info->ext = pathinfo($data['filepath'], PATHINFO_EXTENSION);
        }
        $info->save();

        return $this->showMessage('保存成功', '/admmeeting-manager/material');
    }

    /**
     * 编辑资料
     * @param int $id
     * @return mixed
     */
    public function MaterialEdit($id = 0)
    {
        $info = MeetingManageMaterial::where('id', $id)->first();

        return View::make('admin.meetingmanager.materialedit')->with('oInfo', $info)->with('id', $id);
    }

    /**
     * 编辑资料
     * @param int $id
     * @return mixed
     */
    public function doMaterialEdit($id = 0)
    {
        $type = Input::get('type', '');
        $title = Input::get('title', '');
        $filepath = Input::get('filepath', '');
        $info = MeetingManageMaterial::where('id', $id)->first();
        if ($type) {
            $info->type = $type;
        }
        if ($title) {
            $info->title = $title;
        }
        if ($filepath) {
            $info->filepath = $filepath;
        }
        $info->save();

        return $this->showMessage('保存成功', '/admmeeting-manager/material');
    }

    /**
     * 删除资料
     * @param int $id
     * @return mixed
     */
    public function MaterialDelete($id = 0)
    {
        $info = MeetingManageMaterial::where('id', $id)->first();
        if ($info) {
            $info->delete();
        }

        return $this->showMessage('删除成功', '/admmeeting-manager/material');
    }
}