<?php

use Illuminate\Support\Facades\Redirect;

class ConsultationController extends BaseController {
	
	CONST  PAGESIZE = 6;
	CONST  CATID = 2;

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function Index(){
		
		$Consultation1 = Consultation::getLatestConsultation(1);
		$oTopic = null;
		$iRegFlag = 0;//当前用户积分排行榜前一百，不能报名
		//获取积分排行榜前一百医生
		$oUser = User::getUserScoreListObj(3);
		foreach($oUser as $k=>$v){
			if($v->id==Session::get('userid')){
				$iRegFlag = 1;//当前用户在积分排行榜前100，未报名
				break;
			}
		}
		if($iRegFlag==1&&$Consultation1){
			//判断当前用户是否已经报名
			$oReg = ConsultationUserReg::where('meeting_id',$Consultation1->id)
				->where('user_id',Session::get('userid'))
				->first();
			if($oReg){
				$iRegFlag = 2;//当前用户在积分排行榜前100，已报名
			}
		}
		if($Consultation1){
			$oTopic = ConsultationTopic::where('meeting_id',$Consultation1->id)->get();
		}
		//redis操作=======Start
		if (Cache::has('Consultation2')){
		   $Consultation2 = Cache::get('Consultation2');
		}else{
			$Consultation2 = Consultation::getLatestConsultation(2);
		    Cache::put('Consultation2',$Consultation2,120);
		}
	    if (Cache::has('Consultation3')){
		   $Consultation3 = Cache::get('Consultation3');
		}else{
			$Consultation3 = Consultation::getLatestConsultation(3);
		    Cache::put('Consultation3',$Consultation3,120);
		}
	    if (Cache::has('Consultation4')){
		   $Consultation4 = Cache::get('Consultation4');
		}else{
		    $Consultation4 = Consultation::findAllVideoCount(4,3);
		    Cache::put('Consultation4',$Consultation4,120);
		}
	    if (Cache::has('Consultation5')){
		   $Consultation5 = Cache::get('Consultation5');
		}else{
		    $Consultation5 = Consultation::getLatestConsultation(5);
		    Cache::put('Consultation5',$Consultation5,120);
		}
		//END====
		//病例征集列表
		$caseList = CaseInfo::where('status','=',2)
			->where('voteable','=',1)
			->OrderByCreatedAt()
			->paginate(6)->toArray();
		return View::make('front.consultation.index')
			->with('Consultation1',$Consultation1)
			->with('Consultation2',$Consultation2)
			->with('Consultation3',$Consultation3)
			->with('Consultation4',$Consultation4)
			->with('Consultation5',$Consultation5)
			->with('oTopic',$oTopic)
			->with('iRegFlag',$iRegFlag)
			->with('caseList',$caseList);
	}
	
	public function getList(){
		$oConsultation = Consultation::findAllVideo(4)->paginate(self::PAGESIZE);
		return View::make('front.consultation.list')->with('oConsultation',$oConsultation);
	}
	
	public function getShow($iId){
		$oReview = Consultation::getShowById($iId);
		//点击量+1
		$oReview->increment('meeting_hits');
		//获取所有评论
		$iCatId = self::CATID;
		$oComments = Comment::getComment($iCatId,$iId);
		$sImgLi = Images();
		return View::make('front.consultation.show')
			->with('oReview',$oReview)
			->with('oComments',$oComments)
			->with('sImgLi',$sImgLi);
	}
	
	//报名
	public function Apply(){
		$input = Input::all();
		$iConId = $input['meeting_id'];
		if(!$iConId){
			echo 'error';die;
		}
		//判断当前用户是否已经报名
		$oReg = ConsultationUserReg::where('meeting_id',$iConId)
			->where('user_id',Session::get('userid'))
			->first();
		if($oReg){
			echo 'repeat';die;
		}
		if(!isset($input['topic'])){
			echo 'noanswer';die;
		}
		$aTopic = $input['topic'];
		$count = 0;
		foreach($aTopic as $k=>$v){
			if(trim($v)!=''){
				$count++;
			}
		}
		if($count<2){
			echo 'noanswer';die;
		}
		$oConsultation = Consultation::find($iConId);
		if($oConsultation->reg_num>=$oConsultation->reg_setmax_nums){
			echo 'full';die;
		}
		//保存报名信息
		$aUserReg = array(
			'meeting_id'	=> $iConId,
			'user_id'		=> Session::get('userid'),
			'reg_time'		=> date('Y-m-d H:i:s')
		);
		$oUserReg = new ConsultationUserReg($aUserReg);
		$oUserReg->save();
		//保存观点
		foreach($aTopic as $k=>$v){
			if(trim($v)){
				//阐述观点了，存数据库
				$aTopicAnswer = array(
					'meeting_id'	=> $iConId,
					'user_id'		=> Session::get('userid'),	
					'topic_id'		=> $k,
					'user_answer'	=> $v
				);
				$oTopicAnswer = new ConsultationTopicAnswer($aTopicAnswer);
				$oTopicAnswer->save();
			}
		}
		//报名人数加1
		$oConsultation->increment('reg_num');
		echo 'success';die;
	}
	
	//进入直播
	public function Enter($iVideoId,$iType){
		//type为1表示观看直播,为2表示参与会议
		$oConsultation = Consultation::find($iVideoId);
		if(!$oConsultation){
			return $this->showMessage('该直播不存在');
		}
		//存log
		$aLog = array(
			'consultation_id'	=> $iVideoId,
			'user_id'			=> Session::get('userid'),
			'enter_type'		=> $iType
		);
		$oLog = new ConsultationEnterLog($aLog);
		$oLog->save();
		if($iType==1){
			$url = 'meeting_url';
			$sUrl = $oConsultation->$url.'?nickName='.urlencode(Auth::User()->user_nick);
		}else{
			$url = 'meeting_video_url';
			$sUrl = $oConsultation->$url;
		}
		// echo "<script>window.open('".$sUrl."');window.history.go(-1)</script>";
		return Redirect::to($sUrl);
	}
	
}