<?php

class MobileMyTrainController extends BaseController {
	
	CONST  PAGESIZE = 10;
    private $iUserId = 0;
    private $iRoleId = 0;
	
	public function __construct(){
		$this->beforeFilter('mobileauth');
		$this->iUserId = Session::get('userid');
		$this->iRoleId = Session::get('roleid');
	}

	public function getIndex(){
		if($this->iRoleId != 2){
			return Redirect::to('/mobile');
		}
		$oTrain = UserTrain::getAllTrain();
		return View::make('mobile.mytrain.index')->with('oTrain',$oTrain);
	}
	
	//进入培训直播
	public function getEnterLive($iId){
		if($this->iRoleId != 2){
			return Redirect::to('/mobile');
		}
		$oTrain = UserTrain::find($iId);
		if($oTrain){
			//用户信息存数据库操作
			$aTrainLog = array(
				'train_id'	=> $iId,
				'user_id'	=> $this->iUserId,
				'device'    => 2     //移动端微信
			);
			$oUserTrainLog = new UserTrainLog($aTrainLog);
			$oUserTrainLog->save();
			return Redirect::to($oTrain->train_url.'?nickName='.Auth::User()->user_nick);
		}else{
			return Redirect::to('/mobile');
		}
	}
	
	//进入培训录播
	public function getEnterReview($iId){
		if($this->iRoleId != 2){
			return Redirect::to('/mobile');
		}
		$oTrain = UserTrain::find($iId);
		if($oTrain){
			//点击量+1
			$oTrain->increment('train_review_hits');
			return View::make('mobile.mytrain.review')->with('oTrain',$oTrain);
		}else{
			return Redirect::to('/mobile');
		}
	}
	//观看录播时长统计操作
	public function postVideoView($iVideoId,$iUserId,$iMin){
		$oUserTrainReviewLogExists = UserTrainReviewLog::where('train_id',$iVideoId)->where('user_id',$iUserId)->first();
		if(count($oUserTrainReviewLogExists)){
			if($oUserTrainReviewLogExists->watch_minutes < $iMin){
				$oUserTrainReviewLogExists->watch_minutes=$iMin;
				$oUserTrainReviewLogExists->device= 2;  //移动端微信
				$oUserTrainReviewLogExists->save();
				echo 'success';die;
			}else{
				echo 'success';die;
			}
		}else{
			$oUserTrainReviewLog = new UserTrainReviewLog;
			$oUserTrainReviewLog->train_id =$iVideoId;
			$oUserTrainReviewLog->user_id=$iUserId;
			$oUserTrainReviewLog->watch_minutes=$iMin;
			$oUserTrainReviewLog->device= 2;  //移动端微信
			$oUserTrainReviewLog->save();
			echo 'success';die;
		}
	}
	
}