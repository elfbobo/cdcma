<?php

class AdmTrainVideoController extends BaseController
{

    CONST  PAGESIZE = 10;

    //视频列表
    public function Video()
    {
        //取出所有视频
        $oVideos = TrainVideo::findAllVideo()->paginate(self::PAGESIZE);
        //取得所有专家
        $oDocs = FaceDoc::orderByCreatedAt()->lists('doc_name', 'id');
        return View::make('admin.train.videolist')
            ->with('oVideos', $oVideos)
            ->with('oDocs', $oDocs);
    }

    //新增视频
    public function AddVideo()
    {
        //取得所有专家
        $oDocs = FaceDoc::orderByCreatedAt()->lists('doc_name', 'id');
        return View::make('admin.train.videoadd')
            ->with('oDocs', $oDocs);
    }

    //新增视频do
    public function AddVideoDo()
    {
        $aInput = Input::all();
        unset($aInput['upload_file']);
        unset($aInput['upload_video']);
        $rule = TrainVideo::getVideoRule();

        $validator = Validator::make($aInput, $rule);
        if ($validator->fails()) {
            return $this->showMessage('请填写必填字段');
        }

        $oTrainVideo = new TrainVideo();
        if (isset($aInput['department_id'])) {
            $aInput['department_id'] = join(',', $aInput['department_id']);
        } else {
            $aInput['department_id'] = '';
        }

        //验证成功
        if ($oTrainVideo->addVideo($aInput)->save()) {
            return $this->showMessage('新增视频成功', '/admtrainvideo/video');
        } else {
            return $this->showMessage('添加失败');
        }
    }

    //编辑视频
    public function EditVideo($id)
    {
        $oFaceVideo = TrainVideo::find($id);
        //取得所有专家
        $oDocs = FaceDoc::orderByCreatedAt()->lists('doc_name', 'id');
        return View::make('admin.train.videoedit')
            ->with('oFaceVideo', $oFaceVideo)
            ->with('oDocs', $oDocs);
    }

    //编辑视频提交
    public function EditVideoDo($id)
    {
        $aInput = Input::all();
        unset($aInput['upload_file']);
        unset($aInput['upload_video']);
        unset($aInput['upload_banner']);
        $rule = TrainVideo::getVideoRule();
        $validator = Validator::make($aInput, $rule);
        if ($validator->fails()) {
            return $this->showMessage('请填写必填字段');
        }
        $oFaceVideo = TrainVideo::find($id);
        if (isset($aInput['department_id'])) {
            $aInput['department_id'] = join(',', $aInput['department_id']);
        } else {
            $aInput['department_id'] = '';
        }
        if ($oFaceVideo->addVideo($aInput)->save()) {
            return $this->showMessage('编辑成功');
        } else {
            return $this->showMessage('编辑失败');
        }
    }

    //删除
    public function DelVideo($id)
    {
        $oFaceVideo = TrainVideo::find($id);
        $oFaceVideo->delete();

        return $this->showMessage('删除视频成功', '/admtrainvideo/video');
    }

    //上传视频
    public function UploadVideoFile()
    {
        if ($_FILES['upload_file']['error'] > 0) {
            $error = $_FILES['thumb']['error'];
        } else {
            $attach_filename = $_FILES['upload_video']['name'];
            $attach_fileext = pathinfo($attach_filename, PATHINFO_EXTENSION);
            $rand_name = date('YmdHis', time()) . rand(1000, 9999);

            $sFileName = $rand_name . '.' . $attach_fileext;

            $sPath = "/upload/video/$attach_fileext/" . date('Ymd', time());

            $sRealPath = public_path() . $sPath;
            mkdirs($sRealPath);
            // $attach_filesize = filesize($_FILES['upload_file']['tmp_name']);
            move_uploaded_file($_FILES['upload_file']['tmp_name'], $sRealPath . DS . $sFileName);

            $sFileNameS = $rand_name . '_s.' . $attach_fileext;
            resizeImage($sRealPath . DS . $sFileName, $sRealPath . DS . $sFileNameS, 1000, 1000);

            $sFileUrl = $sPath . '/' . $sFileNameS;

            $json = array('video_url' => $sFileUrl);
            echo json_encode($json);
            die;
        }
    }

}