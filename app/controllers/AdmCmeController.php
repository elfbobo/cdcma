<?php

class AdmCmeController extends BaseController
{
    CONST  PAGESIZE = 20;

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    //好医生兑换记录列表
    public function OrderList()
    {
        //取出所有专家
        $input = Input::all();
        $oLists = IntegralLog::select(
            'user_health_integral_log.user_id',
            'user_health_integral_log.obj_type',
            'user_health_integral_log.obj_id',
            'user_health_integral_log.obj_title',
            'user_health_integral_log.types',
            'user_health_integral_log.number',
            'user_health_integral_log.balance',
            'user_health_integral_log.content',
            'user_health_integral_log.created_at',
            'user.user_name',
            'cme_info.id',
            'cme_info.status',
            'cme_info.updated_at as check_time'
        );
        $aLink = array();
        if (isset($input['user_name']) && $input['user_name']) {
            $oLists = $oLists->where('user.user_name', 'LIKE', "%{$input['user_name']}%");
            $aLink['user_name'] = $input['user_name'];
        }
        $oLists = $oLists->leftJoin('user', 'user.id', '=', 'user_health_integral_log.user_id')
            ->join('cme_info', 'cme_info.obj_id', '=', 'user_health_integral_log.obj_id')
            ->where('user_health_integral_log.obj_type', 'buycme');
        $oLists = $oLists->paginate(self::PAGESIZE);

        return View::make('admin.integral.cmeorderlist')->with('oLists', $oLists)->with('aLink', $aLink);
    }

    /**
     * 导出未兑换好医生列表
     */
    public function exportList()
    {
        $list = CmeInfo::where('status', 0)->get();
        $data = $list->toArray();
        if (empty($data)) {
            return $this->showMessage('没有可导出的信息');
        }

        $objPHPExcel = new \PHPExcel();  //创建PHPExcel实例

        $title = '好医生CME课程兑换列表';

        /*--------------设置表头信息------------------*/
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', '序号')
            ->setCellValue('B1', '姓名')
            ->setCellValue('C1', '工作单位')
            ->setCellValue('D1', '科室')
            ->setCellValue('E1', '职称')
            ->setCellValue('F1', '职务')
            ->setCellValue('G1', '联系电话')
            ->setCellValue('H1', '区号')
            ->setCellValue('I1', '邮政编码');

        /*--------------开始从数据库提取信息插入Excel表中------------------*/
        foreach($list as $key => $item){
            $i = $key + 2;
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("A".$i, $item->id)
                ->setCellValue("B".$i, $item->name)
                ->setCellValue("C".$i, $item->workplace)
                ->setCellValue("D".$i, $item->dept)
                ->setCellValue("E".$i, $item->professional)
                ->setCellValue("F".$i, $item->position)
                ->setCellValueExplicit("G".$i, $item->telephone, \PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValueExplicit("H".$i, $item->areacode, \PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValueExplicit("I".$i, $item->postalcode, \PHPExcel_Cell_DataType::TYPE_STRING);
        }

        $objPHPExcel->getActiveSheet()->setTitle('Sheet1');      //设置sheet的名称

        /*--------------下面是设置其他信息------------------*/
        $objPHPExcel->setActiveSheetIndex(0);   //设置sheet的起始位置
        // $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(19);    //设置excel：月份的宽度

        /*--------------下面是设置保存路径------------------*/
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');   //通过PHPExcel_IOFactory的写函数将上面数据写出来
        date_default_timezone_set("Asia/Shanghai");

        $date = date('YmdHis');
        $filename = $title . '-' . $date . '.xls';

        $ua = $_SERVER["HTTP_USER_AGENT"];
        if (preg_match("/MSIE/i", $ua)) {
            $filename = iconv( "UTF-8", "GBK//TRANSLIT", $filename);
            header('Content-Disposition: attachment; filename="' . $filename . '"');
        } else if (preg_match("/Firefox/", $ua)) {
            header('Content-Disposition: attachment; filename*="utf8\'\'' . $filename . '"');
        } else {
            header('Content-Disposition: attachment; filename="' . $filename . '"');
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
        exit;
    }

    public function checkInfo()
    {
        $id = Input::get('id', 0);
        if (!$id) {
            return $this->showMessage('参数错误');
        }
        $info = CmeInfo::where('id', $id)->first();

        // 当提交数据时
        if (isset($_POST['submit'])) {
            $status = Input::get('status', 0);
            if (is_numeric($status)) {
                if ($status) {
                    $info->status = 1;
                } else {
                    $info->status = 0;
                    $info->updated_at = null;
                }
                $info->save();
                return $this->showMessage('保存成功', '/admcmeorder');
            }
        }

        return View::make('admin.integral.cmecheckinfo')->with('info', $info);
    }
}