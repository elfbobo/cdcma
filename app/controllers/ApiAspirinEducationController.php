<?php 

class ApiAspirinEducationController extends BaseController
{

	const CATID = 2;

	public function __construct()
	{
		$this->beforeFilter('apilogin');
		$this->beforeFilter('apidoc', array('only'=>array('postProduceVideo','postSaveEducation')));
	}
	
	/**
	 * http://2kghy.kydev.net/apiaspirineducation/list?catid=1&page=1&pagesize=3&order=hits&keyword=&token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
	 * 健康教育列表页（精彩内容）
	 */
	public function getList(){
		$iPage = Input::get('page',1);
		$iPageSize = Input::get('pagesize',10);
		$sInputOrder = Input::get('order','');
		$sKeyword = Input::get('keyword','');
		$iCatId = Input::get('catid',0);
		/*if($iCatId != 1 && $iCatId != 2){
			echo $this->formatRet(false,"未传科室分类参数","");exit;
		}*/
		$sOrder = 'created_at';
        $oEducation = AspirinEducation::where('education_type', '=', 1);

		if($iCatId == 1 || $iCatId == 2){
			$oEducation = $oEducation->where('ppt_catid',$iCatId);
		}
		if(!empty($sKeyword)){
			$oEducation = $oEducation->where(function($query) use($sKeyword){
                $query->where('ppt_title','LIKE','%'.$sKeyword.'%')->orWhere('user_name','LIKE','%'.$sKeyword.'%');
            });
		}
        $oEducation = $oEducation->select([
            'aspirin_education.*',
            'view_qrcode_log.share_hits as view_share_hits'
        ])
            ->leftJoin('view_qrcode_log', 'aspirin_education.id', '=', 'view_qrcode_log.video_id');

        switch ($sInputOrder){
            case 'hits':
                $oEducation->orderBy('view_qrcode_log.share_hits', 'desc');
                break;
            case 'support':
                $oEducation = $oEducation->orderBy('support_count','DESC');
                break;
            default :
                $sOrder = 'created_at';
                break;
        }

		if($sOrder!='created_at'){
			$oEducation = $oEducation->orderBy('created_at','DESC');
		}
		$oEducation = $oEducation->skip(($iPage-1)*$iPageSize)->take($iPageSize)->get();
		$aEducation = array();
		foreach($oEducation as $v){
			$sTitle = $v->ppt_title;
			if(!empty($v->user_name)){
				$sTitle = "【".$v->user_name."】".$v->ppt_title;
			}
			$aEducation[] = array(
				'id'				=> $v->id,
				'video_flag'        => $v->video_flag,
				'title'				=> $sTitle,
				'thumb'			    => Config::get('app.url').$v->ppt_thumb,
				'education_hits'	=> $v->education_hits,
				'share_hits'	    => (int)$v->view_share_hits,
				'support_count'		=> $v->support_count,
				'created_at'		=> substr($v->created_at,0,10),
			);
		}
		echo $this->formatRet(true,"成功","success",$aEducation);exit;
	}
	
	/**
	 * http://2kghy.kydev.net/apiaspirineducation/my-list?catid=1&token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
	 * 我的课程列表
	 */
	public function getMyList(){
		global $iUserId;
		$iUid = $iUserId;
		$oUser = User::find($iUid);
		$aPos = User::getPosition();
		if($oUser->role_id==3 && ($oUser->user_position == $aPos[2] || $oUser->user_position == $aPos[3] || $oUser->user_position == $aPos[4] )){
			$iCatId = Input::get('catid');
			if($iCatId != 1 && $iCatId != 2){
				echo $this->formatRet(false,"未传科室分类参数","");exit;
			}
			$oEducation = AspirinEducation::where('video_flag',0)
				->where('ppt_catid',$iCatId)
				->where('user_id',$iUid)
				->orderBy('education_type','ASC')
				->orderBy('created_at','desc')
				->get();
			$aEducation = array();
			foreach($oEducation as $v){
				$aEducation[] = array(
					'id'				=> $v->id,
					'title'				=> $v->ppt_title,
					'thumb'				=> Config::get('app.url').$v->ppt_thumb,
					'type'				=> $v->education_type,
					'created_at'		=> substr($v->created_at,0,10),
				);
			}
			echo $this->formatRet(true,"成功","success",$aEducation);exit;
		}else{
			echo $this->formatRet(false,"仅限主治以上职称用户查看","");exit;
		}
	}
	
	/**
	 * http://2kghy.kydev.net/apiaspirineducation/show?id=2&token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
	 * 健康教育详细页面
	 */
	public function getShow(){
		global $iUserId;
		$iUid = $iUserId;
		$iEducationId = Input::get('id',0);
		$oEducation = AspirinEducation::find($iEducationId);
		if(!$oEducation){
			echo $this->formatRet(false,"该健康教育视频不存在","");exit;
		}
		// $log = DB::getQueryLog();
		// print_r($log);die;
		// print_r($viewLogRs);die;
		$oEducation->increment('education_hits');
		//查找当前用户是否点赞
		$iHasSupport = AspirinSupportLog::hasSupport(self::CATID,$iUid,$iEducationId);
		//是否可以分享
		$iShareFlag = 0;
		if($oEducation->education_type==1){
			$iShareFlag = 1;
		}
		//判断是用户录制还是后台上传，后台上传显示图文内容，用户录制显示专家信息
		if($oEducation->video_flag == 0 && $oEducation->user_id != 0){
			//获取上传者用户信息
			$oUser = User::select('id','user_name','user_thumb','user_position','user_department','user_company')
				->where('id',$oEducation->user_id)->first();
			$sUserCompany = $oUser->user_company;
			if(is_numeric($sUserCompany)&&$sUserCompany!=0){
				$sUserCompany = Hospital::where('id',$sUserCompany)->pluck('name');
			}
			if(!$sUserCompany){
				$sUserCompany = $oUser->user_company_name;
			}
			$aEducation = array(
				'id'				=> $oEducation->id,
				'video_flag'        => $oEducation->video_flag,
				'title'				=> $oEducation->ppt_title,
				'hits'				=> '--',//$oEducation->education_hits,
				'content'			=> $oEducation->education_content,
				'type'              => $oEducation->education_type,
				'video_url'			=> $oEducation->video_url,
				'video_thumb'		=> Config::get('app.url').$oEducation->ppt_thumb,
				'thumb'				=> Config::get('app.url').(!empty($oEducation->education_thumb)?$oEducation->education_thumb:$oUser->user_thumb),
				'user_name'			=> $oUser->user_name,
				'user_position'		=> $oUser->user_position,
				'user_department'	=> $oUser->user_department,
				'user_company'		=> $sUserCompany,
				'supports'			=> $oEducation->support_count,
				'created_at'		=> substr($oEducation->created_at,0,10),
				'support_flag'		=> $iHasSupport,
				'pic_html'          => "http://{$_SERVER['HTTP_HOST']}/aspirinshare/authdocclassshare?doc_id={$oEducation->user_id}&video_id={$oEducation->id}&t=".time(),
				// 'pic_html'          => Config::get('app.url').'/aspirinshare/education-save-pic/'.$oEducation->id
				// 'share_flag'		=> $iShareFlag,
				// 'share_title'	=> $oEducation->ppt_title,
				// 'share_thumb'	=> Config::get('app.url').$oEducation->ppt_thumb,
				// 'share_url'		=> Config::get('app.url').'/aspirinshare/education-ewm/'.$oEducation->id
			);
		}else{
			$paths = "/upload/ueditor/image/";
			$aEducation = array(
				'id'				=> $oEducation->id,
				'video_flag'        => $oEducation->video_flag,
				'title'				=> $oEducation->ppt_title,
				'hits'				=> '--',//$oEducation->education_hits,
				'content'			=> str_replace($paths, Config::get('app.url').$paths, $oEducation->education_content),
				'type'              => $oEducation->education_type,
				'video_url'			=> $oEducation->video_url,
				'video_thumb'		=> Config::get('app.url').$oEducation->ppt_thumb,
				'thumb'				=> '',
				'user_name'			=> '',
				'user_position'		=> '',
				'user_department'	=> '',
				'user_company'		=> '',
				'supports'			=> $oEducation->support_count,
				'created_at'		=> substr($oEducation->created_at,0,10),
				'support_flag'		=> $iHasSupport,
				'pic_html'          => ''
				// 'share_flag'		=> $iShareFlag,
				// 'share_title'	=> $oEducation->ppt_title,
				// 'share_thumb'	=> Config::get('app.url').$oEducation->ppt_thumb,
				// 'share_url'		=> Config::get('app.url').'/aspirinshare/education-ewm/'.$oEducation->id
			);
		}
		//健康云积分操作
		$IntegralLog = new IntegralLog();
		$IntegralLog->updateIntegral($iUid,$iEducationId,'view');
		//插入健康教育视频观看日志
		$VideoViewLog = new VideoViewLog();
		$vVlog = array(
			'user_id'=>$iUid, 'types'=>1, 'video_id'=>$aEducation['id'], 'video_title'=>$aEducation['title'],
			'video_thumb'=>$aEducation['video_thumb'], 'ip'=>$VideoViewLog->getIP(), 'groupstr'=>"{$iUid}_".$VideoViewLog->getIP()
		);
		$VideoViewLog->addItem($vVlog)->save();
		//判断是否已签名
		/*if(!$oEducation->is_signed){
			//本月已查看次数
			$thismonthviewcount = $VideoViewLog::whereBetween("created_at", array(date("Y-m-01"), date("Y-m-d 23:59:59")))
				->where("video_id",$iEducationId)
				->get()->count();
			// $log = DB::getQueryLog();
			// print_r($log);
			// echo $thismonthviewcount;die;
			if($thismonthviewcount>=10){
				$aNotice = array(
					'user_id'		=> $iUid,
					'notice_content'=> "您发布的健康教育视频课件本月累计观看量已达到10次，特此邀您签署《健康教育服务协议》",
					'notice_type'	=> 150,
					'detail_id'		=> $aEducation['id']
				);
				$oNotice = new AspirinUserNotice($aNotice);
				$oNotice->save();
			}
		}*/
		echo $this->formatRet(true,"成功","success",$aEducation);exit;
	}

	/**
	 * 健康教育海报图片
	 * http://2kghy.kydev.net/apiaspirineducation/pic?id=2&token=
	 */
	public function getPic()
	{
		$iEducationId = Input::get('id',0);
		$oEducation = AspirinEducation::find($iEducationId);
		if(!$oEducation){
			echo $this->formatRet(false,"该健康教育视频不存在","");exit;
		}
		if($oEducation->print_pic){
			$aEducation = array('picurl' => Config::get('app.url').$oEducation->print_pic);
			echo $this->formatRet(true,"成功","success",$aEducation);exit;
		}else{
			echo $this->formatRet(false,"未生成海报图片，请重新进入");exit;
		}
	}

	/**
	 * 健康教育视频点赞
	 * http://2kghy.kydev.net/apiaspirineducation/support?id=2&token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
	 */
	public function getSupport(){
		global $iUserId;
		$iUid = $iUserId;
		$iEducationId = Input::get('id',0);
		$oEducation = AspirinEducation::find($iEducationId);
		if(!$oEducation||$oEducation->education_type!=1){
			echo $this->formatRet(false,"该视频未审核通过，无法点赞");exit;
		}
		//查找当前用户是否点赞
		$iHasSupport = AspirinSupportLog::hasSupport(self::CATID,$iUid,$iEducationId);
		
		if($iHasSupport){
			echo $this->formatRet(false,"您已经为当前视频点赞了哦");exit;
		}
		//新增一条点赞记录
		$aSupportLog = array(
			'cat_id'	=> self::CATID,
			'user_id'	=> $iUid,
			'source_id'	=> $iEducationId
		);
		$oSupportLog = new AspirinSupportLog($aSupportLog);
		$oSupportLog->save();
		//更新当前教育视频的点赞记录
		$oEducation = AspirinEducation::find($iEducationId);
		if($oEducation){
			$oEducation->increment('support_count');
		}
		echo $this->formatRet(true,"点赞成功");exit;
	}
	
	/**
	 * http://2kghy.kydev.net/apiaspirineducation/ppt-list?catid=1&flag=0&token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
	 * 点击课件制作进入ppt列表
	 */
	public function getPptList(){
		global $iUserId;
		$iUid = $iUserId;
		$oUser = User::find($iUid);
		$aPos = User::getPosition();
		//试点地区仅限录制60次
		/*$iAllCount = AspirinEducation::where('video_flag',0)->count();
		if($iAllCount && $iAllCount >= 200){
			echo $this->formatRet(false,"已达试点地区录制次数上限");exit;
		}*/
		if($oUser->role_id==3 && ($oUser->user_position == $aPos[2] || $oUser->user_position == $aPos[3] || $oUser->user_position == $aPos[4] )){
			if($oUser->card_auth_flag !=2){
				echo $this->formatRet(false,"您还没有通过医师认证，无法参与录制","");exit;
			}
            $oEducationNum = AspirinEducation::where('video_flag',0)
                ->where('user_id',$iUid)
                ->where('education_type', 1)
                ->whereRaw("DATE_FORMAT(created_at, '%Y%m') = DATE_FORMAT(CURDATE(), '%Y%m')")
                ->count();
            if ($oEducationNum >= 1){            //改为每月限录一次(审核不通过的可以再次录制)
                echo $this->formatRet(false,"每位讲者每月限录一次","");exit;
            }
			// 医院每年限12次
            $user_company = $oUser->user_company;
            $user_company_name = $oUser->user_company_name;
            $nCount = AspirinEducation::where('video_flag',0)
                ->leftJoin('user', 'user.id', '=', 'aspirin_education.user_id')
                ->where('aspirin_education.education_type', 1)
                ->where(function($query){
                    $query->where('aspirin_education.created_at', '>=', date('Y-01-01 00:00:00'));
                })
                ->where(function($query) use ($user_company, $user_company_name){
                    if (empty($user_company_name)) {
                        $query->where('user.user_company', $user_company);
                    } else {
                        $query->where('user.user_company_name', $user_company_name);
                    }
                })
                ->count();

            // 每个医院每年限录12次(审核不通过的可以再次录制)
            if ($nCount > 12){
                echo $this->formatRet(false,"每个医院每年限录12次","");exit;
            }

			/*$oOther = AspirinEducation::where('video_flag',0)->where('user_id','!=',$iUid)->where('education_type','!=',2)->orderby('created_at','desc')->get();
			foreach($oOther as $k=>$v){
				if($v->user_id != $iUid){
					$oOtherUser = User::find($v->user_id);
					if($oOtherUser && $oOtherUser->user_company == $oUser->user_company &&($oOtherUser->user_position == $aPos[3] || $oOtherUser->user_position == $aPos[4])){
						echo $this->formatRet(false,"每家医院只能有一位副高以上职称的用户录制","");exit;
					}
				}
			}
			$oTwiceLimit = AspirinEducation::where('video_flag',0)->where('user_id',$iUid)->where('education_type','!=',2)->get();
			if(count($oTwiceLimit) >= 2){
				echo $this->formatRet(false,"试运营期内限每位用户只能参与两次");exit;
			}*/
			$iFlag = intval(Input::get('flag',0));
			if($iFlag == 1){                                //点击健康教育时调用此接口
				echo $this->formatRet(true,"成功","","");exit;
			}else{
				$iCatId = Input::get('catid');
				if($iCatId != 1 && $iCatId != 2){
					echo $this->formatRet(false,"未传科室分类参数","");exit;
				}
				$oPpt = AspirinEducationPpt::where('catid',$iCatId)->orderBy('id','desc')->get();
				$aPpt = array();
				foreach($oPpt as $v){
					$aArr = array(
						'id'		=> $v->id,
						'ppt_title'	=> $v->ppt_title,
						'ppt_thumb'	=> Config::get('app.url').$v->ppt_thumb,
						'created_at'=> substr($v->created_at,0,10)
					);
					$aPpt[] = $aArr;
				}
				echo $this->formatRet(true,"成功","", $aPpt);exit;
			}
		}else{
			echo $this->formatRet(false,"仅限主治以上职称用户查看","");exit;
		}
	}
	
	/**
	 * http://2kghy.kydev.net/apiaspirineducation/ppt-detail?id=2&token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
	 * 课件制作页面
	 * @param unknown $iPptId
	 */
	public function getPptDetail(){
		$iPptId = Input::get('id',0);
		$oPpt = AspirinEducationPpt::find($iPptId);
		if(!$oPpt){
			echo $this->formatRet(false,"该PPT不存在或已经被删除","");exit;
		}
		$aImage = AspirinEducationPptImage::where('ppt_id',$iPptId)->lists('image_url');
		if(!count($aImage)){
			echo $this->formatRet(false,"管理员未上传PPT内容","");exit;
		}
		foreach ($aImage as $k=>$v){
			$aImage[$k] = Config::get('app.url').$v;
		}
		$aPpt['id'] = $oPpt->id;
		$aPpt['ppt_title'] = $oPpt->ppt_title;
		$aPpt['images'] = $aImage;
		$Configs = new Configs();
		$configRs = $Configs::find(1)->toArray();
		$aPpt['msgtxt'] = "温馨提示：录制时长达到{$configRs['video_audit_times']}分钟以上为有效录制";
		echo $this->formatRet(true,"成功",$aPpt,"success");
		exit;
	}
	
	
	/**
	 * post
	 * 视频合成
	 * http://2kghy.kydev.net/apiaspirineducation/produce-video?imagetime=
	 * [
	 * {"time":50,"url":"http:\/\/2kghy.kydev.net\/upload\/video\/jpg\/20160829\/201608291611386901.jpg"},
	 * {"time":50,"url":"http:\/\/2kghy.kydev.net\/upload\/video\/jpg\/20160829\/201608291611438914.jpg"},
	 * {"time":50,"url":"http:\/\/2kghy.kydev.net\/upload\/video\/jpg\/20160829\/201608291611474312.jpg"},
	 * {"time":50,"url":"http:\/\/2kghy.kydev.net\/upload\/video\/jpg\/20160829\/201608291611526002.jpg"},
	 * {"time":50,"url":"http:\/\/2kghy.kydev.net\/upload\/video\/jpg\/20160829\/201608291611565568.jpg"},
	 * {"time":50,"url":"http:\/\/2kghy.kydev.net\/upload\/video\/jpg\/20160829\/201608291611591468.jpg"}
	 * ]&token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
	 */
	public function postProduceVideo(){
		set_time_limit(0);
		ini_set("memory_limit","512M");
		global $iUserId;
		$iUid = $iUserId;
		$sImageTimeJson = Input::get('imagetime','');
		$aImageTime = json_decode($sImageTimeJson,true);
		//去掉图片前面的域名
		foreach($aImageTime as $k=>$v){
			$aImageTime[$k]['url'] = str_replace(Config::get('app.url'), '', $v['url']);
		}
		/*$aImageTime = array(
			array(
				'time'	=> 50,
				'url'	=> 'http://2kghy.kydev.net/upload/video/jpg/20160829/201608291611386901.jpg'
			),
			array(
				'time'	=> 50,
				'url'	=> 'http://2kghy.kydev.net/upload/video/jpg/20160829/201608291611438914.jpg'
			),
			array(
				'time'	=> 50,
				'url'	=> 'http://2kghy.kydev.net/upload/video/jpg/20160829/201608291611474312.jpg'
			),
			array(
				'time'	=> 50,
				'url'	=> 'http://2kghy.kydev.net/upload/video/jpg/20160829/201608291611526002.jpg'
			),
			array(
				'time'	=> 50,
				'url'	=> 'http://2kghy.kydev.net/upload/video/jpg/20160829/201608291611565568.jpg'
			),
			array(
				'time'	=> 50,
				'url'	=> 'http://2kghy.kydev.net/upload/video/jpg/20160829/201608291611591468.jpg'
			),
		);
		$sAudioUrl = '/upload/audio/201608291616413322.mp3';*/
		//音频处理
		if ( !Input::hasFile('file') ){
			echo $this->formatRet(false,"未上传音频","");exit;
		}
		$sAudioUrl = '';
		$file = Input::file('file');
		if( $file ){
			$sAudioUrl = $this->dealAudio($file);
		}


		// 判断音频长度是否合格
/*
        include_once  app_path().'/include/MP3File.class.php';
        // 调用方法：
        $mp3 = new MP3File(public_path() . $sAudioUrl);
        $b = $mp3->getDuration();
        $duration = $mp3::formatTime($b);
        if (!isset($duration['minutes']) || ($duration['hours'] > 0 || $duration['minutes'] < 20)) {
            echo $this->formatRet(false, "音频时长不足20分钟");exit;
        } */

        //开始合成视频
        $sOutPutUrl = VideoProduceService::operate($aImageTime, $sAudioUrl, $iUid);

        echo $this->formatRet(true, "成功", "success", array('video_url' => $sOutPutUrl));exit;

	}
	
	/**
	 * post
	 * 保存录制好的视频
	 * http://2kghy.kydev.net/apiaspirineducation/save-education?pptid=1&videourl=123&content=223344&token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
	 */
	public function postSaveEducation(){
		global $iUserId;
		$iUid = $iUserId;
		$oEducationNum = AspirinEducation::where('video_flag',0)
            ->where('user_id',$iUid)
            ->where('education_type', 1)
            ->whereRaw("DATE_FORMAT(created_at, '%Y%m') = DATE_FORMAT(CURDATE(), '%Y%m')")
            ->count();
        if ($oEducationNum >= 1){            //改为每月限录一次(审核不通过的可以再次录制)
            echo $this->formatRet(false,"每位讲者每月限录一次","");exit;
        }
        // 医院每年限12次
        $oUser = User::find($iUid);
        $user_company = $oUser->user_company;
        $user_company_name = $oUser->user_company_name;
        $nCount = AspirinEducation::where('video_flag',0)
            ->leftJoin('user', 'user.id', '=', 'aspirin_education.user_id')
            ->where('aspirin_education.education_type', 1)
            ->where(function($query){
                $query->where('aspirin_education.created_at', '>=', date('Y-01-01 00:00:00'));
            })
            ->where(function($query) use ($user_company, $user_company_name){
                if (empty($user_company_name)) {
                    $query->where('user.user_company', $user_company);
                } else {
                    $query->where('user.user_company_name', $user_company_name);
                }
                // $query->where('user.user_company', $user_company)->orWhere('user.user_company_name', $user_company_name);
            })
            ->count();
        // 每个医院每月限录12次(审核不通过的可以再次录制)
        if ($nCount >= 12){
            echo $this->formatRet(false,"每个医院每年限录12次","");exit;
        }

		//缩略图处理
		$sThumb = '';
		if ( Input::hasFile('file') ){
			$file = Input::file('file');
			if( $file ){
				$sThumb = $this->dealAvatar($file);
			}
		}
		$sContent = Input::get('content','');
		$sUserName = User::where('id',$iUid)->pluck('user_name');
		$iPptId = Input::get('pptid',0);
		if(!$iPptId){
			echo $this->formatRet(false,'缺少pptid参数');
			exit;
		}
		$sVideoUrl = Input::get('videourl','');
		if(!$sVideoUrl){
			echo $this->formatRet(false,'您还未制作视频');
			exit;
		}
		$iVideoTime = Input::get('videotime',0);
		$iVideoTimes = floor($iVideoTime/60);
		/*if(!$iVideoTime){
			echo $this->formatRet(false,'缺少视频时长参数');
			exit;
		}*/

        // 判断音频长度是否满足要求
        include_once  app_path().'/include/MP3File.class.php';
        $sAudioUrl = dirname($sVideoUrl) . '/audio.mp3';
        $sAudioUrl = str_replace(Config::get('app.url'), '', $sAudioUrl);
        // 调用方法：
        $mp3 = new MP3File(public_path() . $sAudioUrl);
        $b = $mp3->getDuration();
        $duration = $mp3::formatTime($b);

        // 如果音频时长小于20分钟且文件大小小于1M时需开启手工审核
        if ($duration['minutes'] < 20 && filesize(public_path() . $sAudioUrl) < 1024000) {
            $iVideoTimes = 0;
        }

		$video_types = Input::get('video_types');
		$oPpt = AspirinEducationPpt::where('id',$iPptId)->first();
		$oEducation = AspirinEducation::where('video_url',$sVideoUrl)->first();
		$Configs = new Configs();
		$configRs = $Configs::find(1);
		$video_audit_times = @$configRs->video_audit_times ? (int)$configRs->video_audit_times : 10;
		if($oEducation){
			//重复保存，则更新
			$oEducation->video_flag = 0;             //视频类型（0：专家录制；1：后台上传；）
			$oEducation->ppt_catid = $oPpt->catid;     //课件所属分类
			$oEducation->user_id = $iUid;
			$oEducation->user_name = $sUserName;
			$oEducation->ppt_title = $oPpt->ppt_title;
			$oEducation->ppt_thumb = $oPpt->ppt_thumb;
			$oEducation->ppt_id = $iPptId;
			$oEducation->education_thumb = $sThumb;
			$oEducation->education_content = $sContent;
			$oEducation->video_types = $video_types;
			$oEducation->video_time = isset($iVideoTime)?$iVideoTime:0;
			if($iVideoTimes>=$video_audit_times) $oEducation->education_type = 1;
			$oEducation->save();
		}else{
			$aEducation = array(
				'video_flag'        => 0,               //视频类型（0：专家录制；1：后台上传；）
				'ppt_catid'         => $oPpt->catid,    //课件所属分类
				'user_id'			=> $iUid,
				'user_name'			=> $sUserName,
				'ppt_title'			=> $oPpt->ppt_title,
				'ppt_thumb'			=> $oPpt->ppt_thumb,
				'ppt_id'			=> $iPptId,
				'education_thumb'	=> $sThumb,
				'education_content'	=> $sContent,
				'video_url'			=> $sVideoUrl,
				'video_types'		=> $video_types,
				'video_time'        => isset($iVideoTime)?$iVideoTime:0
			);
			if($iVideoTimes>=$video_audit_times) $aEducation['education_type'] = 1;
			$oEducation = new AspirinEducation($aEducation);
			$oEducation->save();
		}
		echo $this->formatRet(true,"保存成功");exit;
	}
	
	/**
	 * 删除已录制的视频
	 * http://2kghy.kydev.net/apiaspirineducation/delete?id=12&token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
	 */
	public function getDelete(){
		global $iUserId;
		$iUid = $iUserId;
		$iEducationId = Input::get('id',0);
		$oEducation = AspirinEducation::find($iEducationId);
		if(!$oEducation||$oEducation->user_id!=$iUid){
			echo $this->formatRet(false,"该健康教育视频不存在","");exit;
		}
		$oEducation->delete();
		echo $this->formatRet(true,"删除成功");exit;
	}
	
	
	/**
	 * 图片处理
	 * @param unknown_type $file
	 */
	private function dealAvatar( $file ){
		$sExt = $file->getClientOriginalExtension();//扩展名
		if( !in_array( $sExt, array('jpg', 'png', 'jpeg') ) ){
			echo $this->formatRet(false,'图片格式错误，目前支持jpg、png、jpeg');
			exit;
		}
		//处理图片
		$sNewName = date('YmdHis').rand(1,9999). '.' .$sExt;//新文件名
		$sFilePath = '/upload/image/'. date('Ymd'). '/' ;
		mkdirs(public_path().$sFilePath);
		$file->move(public_path().$sFilePath , $sNewName);
		return $sFilePath.$sNewName;
	}
	
	/**
	 * 音频处理
	 * @param unknown_type $file
	 */
	private function dealAudio( $file ){
		$sExt = $file->getClientOriginalExtension();//扩展名
		if( !in_array( $sExt, array('mp3') ) ){
			echo $this->formatRet(false,'请上传mp3格式的音频');
			exit;
		}
		//处理图片
		$sNewName = date('YmdHis').rand(1,9999). '.' .$sExt;//新文件名
		$sFilePath = '/upload/education/audio/'. date('Ymd'). '/';
		mkdirs(public_path().$sFilePath);
		$file->move(public_path().$sFilePath , $sNewName);
		return $sFilePath.$sNewName;
	}
	
	/**
	 * 健康教育首页——查看课件      dll  20170405
	 * /apiaspirineducation/courseware?catid=1&token=
	 */
	public function getCourseware(){
		global $iUserId;
		$iUid = $iUserId;
		$oUser = User::find($iUid);
		$aPos = User::getPosition();
		// 新增代表可看
		if($oUser->role_id == 2 || ($oUser->role_id==3 && ($oUser->user_position == $aPos[2] || $oUser->user_position == $aPos[3] || $oUser->user_position == $aPos[4] ))){
			$iCatId = Input::get('catid');
			if($iCatId != 1 && $iCatId != 2){
				echo $this->formatRet(false,"未传科室分类参数","");exit;
			}
			$oPpt = AspirinEducationPpt::where('catid',$iCatId)->orderBy('id','desc')->get();
			$aPpt = array();
			foreach($oPpt as $v){
				$aPpt[] = array(
					'id'		=> $v->id,
					'ppt_title'	=> $v->ppt_title,
					'ppt_thumb'	=> Config::get('app.url').$v->ppt_thumb,
					'created_at'=> substr($v->created_at,0,10)
				);
			}
			echo $this->formatRet(true,"成功","success",$aPpt);exit;
		}else{
			echo $this->formatRet(false,"仅限代表或副高以上职称用户查看","");exit;
		}
	}
	
	/**
	 * 健康教育首页——查看课件详情页   dll  20170405
	 * /apiaspirineducation/courseware-show?id=&token=
	 */
	public function getCoursewareShow(){
		$iId = Input::get('id',0);
		$oPpt = AspirinEducationPpt::find($iId);
		if(!$oPpt){
			echo $this->formatRet(false,"该PPT不存在或已经被删除","");exit;
		}
		$aImage = AspirinEducationPptImage::where('ppt_id',$iId)->orderby('id','asc')->lists('image_url');
		if(!count($aImage)){
			echo $this->formatRet(false,"管理员未上传PPT内容","");exit;
		}
		foreach ($aImage as $k=>$v){
			$aImage[$k] = Config::get('app.url').$v;
		}
		$aPpt['ppt_id'] = $oPpt->id;
		$aPpt['ppt_title'] = $oPpt->ppt_title;
		$aPpt['images'] = $aImage;
		echo $this->formatRet(true,"成功",$aPpt,"success");exit;
	}
	
}