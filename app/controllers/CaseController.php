<?php

use Illuminate\Support\Facades\Redirect;

class CaseController extends BaseController {
	
	CONST  PAGESIZE = 5;
	CONST  STATUS_W = 0;//未审核
	CONST  STATUS_Z = 1;//审核中
	CONST  STATUS_Y = 2;//通过
	CONST  STATUS_N = -1;//未通过
	
	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	/*
	 * 病例征集-征集规则
	 */
	public function Rule(){
		return View::make('front.case.rule');
	}

	/*
	 * 病例征集-病例上传
	 */
	public function getCaseUpload(){
		return View::make('front.case.upload');
	}

	/*
	 * 病例征集-病例上传
	 */
	public function postCaseUpload(){
		$input = Input::all();
		$baseArr = array();//基本信息
		foreach($input as $key => $value){
			if(!in_array($key,array('dosubmit','sub_exam'))){
				$baseArr[$key] = addslashes($value);
			}
		}
		$baseArr['userId'] = Session::get('userid');
		$baseArr['uploadType']=2;
		$caseInfo = new CaseInfo($baseArr);
		$caseInfo->save();
		$caseId = $caseInfo->id;
		return View::make('front.case.upload');
		
	}

	/*
	 * 病例征集-修改病例
	 */
	public function getCaseEdit($id){
		$oCaseInfo = CaseInfo::where('id','=',$id)
			->where('userId','=',Session::get('userid'))
		 	->where('status','=',self::STATUS_W)
		 	->get();
		if($oCaseInfo[0]['uploadType']==2){
			$picNum = 1;//记录附件图片数量，数量超过3则页面中不允许上传
			for($i=1;$i<4;$i++){
				if($oCaseInfo[0]['sub_exam_fileUrl'.$i]){
					$picNum++;
				}
			}
			return View::make('front.case.edit')->with('picNum',$picNum)->with('oCaseInfo',$oCaseInfo);
		}else{
			return View::make('front.case.edit1')->with('oCaseInfo',$oCaseInfo);
		}
	}

	/*
	 * 病例征集-修改病例
	 */
	public function postCaseEdit(){
		$input = Input::all();
		$case = CaseInfo::find($input['id']);
		foreach($input as $k=>$v){
			if(!in_array($k,array('id','dosubmit','sub_exam'))){
				$case->$k = addslashes($v);
			}
		}
		$case->save();
		return self::getCaseEdit($input['id']);
	}

	/*
	 * 病例征集-获取病例列表
	 * id=0 所有审核通过病例；id=1所有自己上传的病例
	 */
	public function getCaseList($id=0){
		if($id){
			$userid = Session::get('userid');
			$oCase = CaseInfo::where('userId','=',$userid)
				->OrderByCreatedAt()
				->paginate(10);
			$statusArr = array('0'=>'未审核','1'=>'审核中','2'=>'审核通过','-1'=>'审核未通过');
			return View::make('front.user.caselist')
				->with('statusArr',$statusArr)
				->with('oCase',$oCase);
		}else{
			$oCase = CaseInfo::where('status','=',self::STATUS_Y)
				->OrderByCreatedAt()
				->paginate(self::PAGESIZE);
			return View::make('front.case.list')->with('oCase',$oCase);
		}
	}

	/*
	 * 病例征集-获取病例详细信息
	 */
	public function getCaseShow($id=0){
		if($id){
			$oCaseInfo = CaseInfo::where('id','=',$id)->where(function($query){
		 		$query->where('status','=',2)->orWhere('userId','=',Session::get('userid'));
		 	})->get();
		}
		return View::make('front.case.show')->with('oCaseInfo',$oCaseInfo);
	}

	/*
	 * 病例征集-获取病例详细信息-不可投票
	 */
	public function getCaseShowInfo($id=0){
		if($id){
			$oCaseInfo = CaseInfo::where('id','=',$id)->where(function($query){
		 		$query->where('status','=',2)->orWhere('userId','=',Session::get('userid'));
		 	})->get();
		}
		return View::make('front.case.showInfo')->with('oCaseInfo',$oCaseInfo);
	}

	//上传文件
	public function uploadFile(){
		if($_FILES['fileField']['error']>0){
			$json = array('error'=>'上传失败');
		    echo json_encode($json);
		    die;
		}else{
			$attach_filename = $_FILES['fileField']['name'];
    		$attach_fileext = get_filetype($attach_filename);
    		if(!in_array($attach_fileext,array('pdf','doc','docx'))){
    			$json = array('error'=>'只支持pdf和Word文档上传');
		    	echo json_encode($json);
		    	die;
    		}
		    $rand_name = date('YmdHis', time()).rand(1000,9999);
		    $sFileName = $rand_name.'.'.$attach_fileext;   
		    $sPath = "/upload/case/$attach_fileext/".date('Ymd',time());
		    $sRealPath = public_path().$sPath;
		    mkdirs($sRealPath);
		    move_uploaded_file($_FILES['fileField']['tmp_name'], $sRealPath.DS.$sFileName);
		    $sFileUrl = $sPath.'/'.$sFileName;
		    //存数据库
		    $oCaseFile = array();
		    $oCaseFile['userId']=Session::get('userid');
		    $oCaseFile['fileUrl']=$sFileUrl;
		    $oCaseFile['case_name']=str_replace('.'.$attach_fileext,'',$attach_filename);
		    $oCaseFile['uploadType']=1;
		    $caseFile = new CaseInfo($oCaseFile);
		    $caseFile->save();
		    $json = array('error'=>'0','fileUrl'=>$sFileUrl,'fileName'=>str_replace('.'.$attach_fileext,'',$attach_filename));
		    echo json_encode($json);
		    die;
		}
	}

	//上传文件
	public function uploadFileEdit($id){
		if($_FILES['fileField']['error']>0){
			$json = array('error'=>'上传失败');
		    echo json_encode($json);
		    die;
		}else{
			$attach_filename = $_FILES['fileField']['name'];
    		$attach_fileext = get_filetype($attach_filename);
    		if(!in_array($attach_fileext,array('ppt','pptx','doc','docx'))){
    			$json = array('error'=>'只支持PPT和Word文档上传');
		    	echo json_encode($json);
		    	die;
    		}
		    $rand_name = date('YmdHis', time()).rand(1000,9999);
		    $sFileName = $rand_name.'.'.$attach_fileext;   
		    $sPath = "/upload/case/$attach_fileext/".date('Ymd',time());
		    $sRealPath = public_path().$sPath;
		    mkdirs($sRealPath);
		    move_uploaded_file($_FILES['fileField']['tmp_name'], $sRealPath.DS.$sFileName);
		    $sFileUrl = $sPath.'/'.$sFileName;
		    //存数据库
		    $oCaseFile = CaseInfo::find($id);
		    $oCaseFile['fileUrl']=$sFileUrl;
		    $oCaseFile['case_name']=str_replace('.'.$attach_fileext,'',$attach_filename);
		    $oCaseFile->save();
		    $json = array('error'=>'0','fileUrl'=>$sFileUrl,'fileName'=>str_replace('.'.$attach_fileext,'',$attach_filename));
		    echo json_encode($json);
		    die;
		}
	}

    //上传图片
	public function uploadPic($name = ''){
		if($_FILES[$name]['error']>0){
			$json = array('error'=>'上传失败');
		    echo json_encode($json);
		    die;
		}else{
			$attach_filename = $_FILES[$name]['name'];
    		$attach_fileext = get_filetype($attach_filename);
			if(!in_array($attach_fileext,array('jpg','png'))){
    			$json = array('error'=>'请提供JPG或PNG格式');
		    	echo json_encode($json);
		    	die;
    		}
		    $rand_name = date('YmdHis', time()).rand(1000,9999);
		    $sFileName = $rand_name.'.'.$attach_fileext;   
		    $sPath = "/upload/case/$attach_fileext/".date('Ymd',time());
		    $sRealPath = public_path().$sPath;
		    mkdirs($sRealPath);
		    move_uploaded_file($_FILES[$name]['tmp_name'], $sRealPath.DS.$sFileName);
		    $sFileNameS = $rand_name . '_s.' . $attach_fileext;
			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 1000, 1000 );
		    $sFileUrl = $sPath.'/'.$sFileNameS;
		    $json = array('error'=>'0','fileUrl'=>$sFileUrl);
		    echo json_encode($json);
		    die;
		}
	}
	
	//病例解读-列表
	public function getCaseExplainList(){
		//取出所有视频
		$oRecVideo = Consultation::where('meeting_type','=','3')
			->where('recommend','=','1')
			->first();
		//取出所有视频
		$oVideos = Consultation::findAllVideo(4)
			->where('column','!=','2')
			->paginate(4);
		//取得所有专家
		$oDocs = FaceDoc::orderByCreatedAt()->lists('doc_name','id');
		return View::make('front.case.explain_list')
			->with('oRecVideo',$oRecVideo)
			->with('oVideos',$oVideos)
			->with('oDocs',$oDocs);
	}

	//病例解读-详细
	public function getCaseExplainShow($id){
		$oReview = Consultation::where('id','=',$id)->first();
		return View::make('front.case.explain_show')->with('oReview',$oReview);
	}

	//投票
	public function postCaseVote(){
		$caseId = $_POST['caseId'];
		$userId = Session::get('userid');
		if($userId && $caseId){
			$voteInfo = CaseVote::where('caseId','=',$caseId)->where('userId','=',$userId)->get();
			if(empty($voteInfo[0])){
				$caseVote = array();
				$caseVote['caseId'] = $caseId;
				$caseVote['userId'] = $userId;
				$voteCase = new CaseVote($caseVote);
				$voteCase->save();
				//更新病例投票数量
				$caseInfo = CaseInfo::find($caseId);
				$caseInfo['votes'] = $caseInfo['votes']+1;
				$caseInfo->save();
				echo 1;//未投票
			}else{
				echo 0;//已投票
			}
		}else{
			echo -1;//参数错误
		}
		die;
	}

	//下载模板
	public function downModel(){
		$file =  public_path() .'/upload/model/model.rar';
		return Response::download( $file ,'cdma-download.rar', array(
			"Content-Description" 	=> "File Transfer",
			"Accept-Ranges" 		=> " bytes",
			"content-type" 			=> "application/octet-stream"
		));
	}

	//下载病例
	public function downCase($id){
		$oCaseInfo = CaseInfo::find($id);
		if($oCaseInfo['status']==self::STATUS_W && $oCaseInfo['userId']==Session::get('userid')){
			$file =  public_path() .$oCaseInfo->fileUrl;
			$extend = explode("." , $file);
		    $ext = strtolower($extend[count($extend) - 1]);
			return Response::download( $file ,$oCaseInfo->case_name.'.'.$ext, array(
				"Content-Description" 	=> "File Transfer",
				"Accept-Ranges"			=> " bytes",
				"content-type" 			=> "application/octet-stream"
			));
		}
	}

}
