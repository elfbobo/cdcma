<?php

/***
 * 资讯对接微信号发布推送  20160513  dll  add s
 * 慢性疾病与转化医学 cdtm2015
 * 心内空间 cmacardio
 * 呼吸科空间 cmaresp
*/
use Illuminate\Support\Facades\Validator;

class AdmMaterialController extends BaseController{

	const iPagesize = 10;
	private $weixin;

	public function __construct(WeixinController $weixin){
		$this->weixin = $weixin;
	}

	public function MaterialList($type){
		/*$a = WeixinMsgIn::orderby('CreateTime','desc')->get();
		foreach($a as $k=>$v){
			var_dump($v->ToUserName);
		}*/
		$title = trim(Input::get('title'));
		if($title){
			$oMaterialNews = MaterialWxNews::where('material_wx_news.title','like','%'.$title.'%')
				->leftJoin('material_news','material_news.id','=','material_wx_news.news_id')
				->select('material_wx_news.*','material_news.news_type')
				->where('material_news.news_type','=',$type)
				->orderBy('material_wx_news.updated_at','desc')
				->paginate(self::iPagesize);
		}else{
			$oMaterialNews = MaterialWxNews::leftJoin('material_news','material_news.id','=','material_wx_news.news_id')
				->select('material_wx_news.*','material_news.news_type')
				->where('material_news.news_type','=',$type)
				->orderBy('material_wx_news.updated_at','desc')
				->paginate(self::iPagesize);
		}
		return View::make('admin.material.list')->with('oMaterialNews',$oMaterialNews)->with('type',$type);
	}
	
	//添加图文素材get
	public function MaterialAdd($type){
		return View::make('admin.material.add')->with('type',$type);
	}

	//添加图文素材post
	public function MaterialAddDo(){
		$aInput = Input::all();
		if(isset($aInput)){
			$newsType = intval($aInput['news_type']);
			//图文消息验证，保存到数据库
			$aArr = array(
				'title'              => trim($aInput['title']),
				'news_type'     	 => $newsType,
				'temporary_flag'     => 0,
				'material_id'        => $aInput['material_id'],
				'author'             => trim($aInput['author']),
				'digest'             => trim($aInput['digest']),
				'show_cover_pic'     => intval($aInput['show_cover_pic']),
				'content'            => $aInput['content0'],
				'content_source_url' => trim($aInput['content_source_url']),
				'created_at'         => date('Y-m-d H:i:s',time())
			);
			$oValidator = Validator::make($aArr,MaterialNews::rules(),MaterialNews::errorMessages());
			if($oValidator->fails()){
				$msg = $this->message($oValidator);
				return View::make('errors.showMessage')->with('msg',$msg)->with('url','/admmaterial/add/'.$newsType);
			}else{
				$oMaterial = new MaterialNews($aArr);
				$oMaterial->save();
				$iMaterialId = $oMaterial->id;
			}
			//调用微信高级群发接口---上传图文消息素材
			$aArticles = array();
			$aArticles[] = array(
				'title'            => urlencode($aInput['title']),
				'thumb_media_id'   => $aInput['media_id'],
				'author'           => urlencode($aInput['author']),
				'digest'           => $aInput['digest']?urlencode($aInput['digest']):'',
				'show_cover_pic'   => urlencode($aInput['show_cover_pic']),
				'content'          => urlencode(htmlspecialchars(str_replace("\"","'",$aInput['content0']))),
				'content_source_url'=> urlencode($aInput['content_source_url'])
			);
			$data = htmlspecialchars_decode(urldecode(json_encode(array('articles'=>$aArticles))));
			$res = $this->weixin->getAddTemporaryMaterialNews($data,$newsType);
			// 没有appid 暂时写死*******************
			/*$res = (object)array(
					'type' =>'news',
					'media_id'=> 'hu1mf2AUqTsP5akTD5ar7ec9bW7tIpQhtiDrpNrHawKPug_zCBftEbVEnjfj_0H3',
					'created_at' => date('Y-m-d H:i:s',time())
			);*/
			if(!isset($res->errcode)||(isset($res->errcode)&&$res->errcode==0)){
				//接口返回数据存数据库
				$aArr1 = array(
					'news_id'=>$iMaterialId,
					'title' => isset($aInput['title'])?$aInput['title']:'',
					'temporary_flag'=>0,
					'media_id'=>$res->media_id,
					'created_at'=>date('Y-m-d H:i:s',time())
				);
				$oMaterialWxNews = new MaterialWxNews($aArr1);
				$oMaterialWxNews->save();
				return View::make('errors.showMessage')->with('msg','添加成功')->with('url','/admmaterial/list/'.$newsType);
			}else{
				return View::make('errors.showMessage')->with('msg',$res->errcode)->with('url','/admmaterial/add/'.$newsType);
			}
		}else{
			return View::make('errors.showMessage')->with('msg','请添加图文内容')->with('url','/admmaterial/add/'.$newsType);
		}
	}

	//图文素材修改get
	public function MaterialEdit($iId){
		$oMaterialNews = MaterialWxNews::find($iId);
		if($oMaterialNews){
			$oNews = MaterialNews::where('material_news.id',$oMaterialNews->news_id)
				->leftJoin('material','material_news.material_id','=','material.id')
				->select('material_news.*','material.media_id','material.path')
				->first();
			if($oNews){
				return View::make('admin.material.edit')->with('oMaterialNews',$oMaterialNews)->with('oNews',$oNews);
			}else{
				return View::make('errors.showMessage')->with('msg','该记录不存在')->with('url','/admmaterial/list/1');
			}
		}else{
			return View::make('errors.showMessage')->with('msg','该记录不存在')->with('url','/admmaterial/list/1');
		}
	}

	//图文素材修改post
	public function MaterialEditDo($iId){
		$oMaterialWxNews = MaterialWxNews::find($iId);
		if($oMaterialWxNews){
			$aInput = Input::all();
			$aArticles = array();
			$aMaterialNews = array(
				'title'              => trim($aInput['title']),
				'material_id'        => $aInput['material_id'],
				'temporary_flag'     => 0,
				'author'             => trim($aInput['author']),
				'digest'             => trim($aInput['digest']),
				'show_cover_pic'     => intval($aInput['show_cover_pic']),
				'content'            => $aInput['content0'],
				'content_source_url' => trim($aInput['content_source_url'])
			);
			$oValidator = Validator::make($aMaterialNews,MaterialNews::rules(),MaterialNews::errorMessages());
			if($oValidator->fails()){
				$msg = $this->message($oValidator);
				return View::make('errors.showMessage')->with('msg',$msg)->with('url','/admmaterial/edit/'.$iId);
			}else{
				$oMaterialNews = MaterialNews::find($oMaterialWxNews->news_id);
				if($oMaterialNews){
					$article = array(
						'title'            => urlencode($aInput['title']),
						'thumb_media_id'   => $aInput['media_id'],
						'author'           => urlencode($aInput['author']),
						'digest'           => $aInput['digest']?urlencode($aInput['digest']):'',
						'show_cover_pic'   => urlencode($aInput['show_cover_pic']),
						'content'          => urlencode(htmlspecialchars(str_replace("\"","'",$aInput['content0']))),
						'content_source_url'=> urlencode($aInput['content_source_url'])
					);
					$oMaterial = Material::find($oMaterialNews->material_id);
					if($oMaterial){
						//素材失效
						if(strtotime($oMaterial->updated_at)+3*24*60*60<time()){
							$data = array('media'=>'@'.$oMaterial->path);
							//上传图文消息素材
							$res = $this->weixin->getAddTemporaryMaterial($data,$oMaterial->type,$oMaterialNews->news_type);
							// 没有appid 暂时写死*******************
							/*$res = (object)array(
									'type' =>'news',
									'media_id'=> 'hu2mf2AUqTsP5akTD5ar7ec9bW7tIpQhtiDrpNrHawKPug_zCBftEbVEnjfj_0H3',
									'created_at' => date('Y-m-d H:i:s',time())
							);*/
							if(!isset($res->errcode)||(isset($res->errcode)&&$res->errcode==0)){
								$oMaterial->media_id = $res->media_id;
								$oMaterial->save();
								$article['thumb_media_id'] = $res->media_id;
							}else{
								return View::make('errors.showMessage')->with('msg',$res->errmsg)->with('url','/admmaterial/list/'.$oMaterialNews->news_type);
							}
						}
					}else{
						return View::make('errors.showMessage')->with('msg','参数错误')->with('url','/admmaterial/list/'.$oMaterialNews->news_type);
					}
					$oMaterialNews->title       = $aMaterialNews['title'];
					$oMaterialNews->material_id = $aMaterialNews['material_id'];
					$oMaterialNews->author 		= $aMaterialNews['author'];
					$oMaterialNews->digest 		= $aMaterialNews['digest'];
					$oMaterialNews->show_cover_pic = $aMaterialNews['show_cover_pic'];
					$oMaterialNews->content 	= $aMaterialNews['content'];
					$oMaterialNews->content_source_url = $aMaterialNews['content_source_url'];
					$oMaterialNews->save();
					$aArticles[] = $article;
				}else{
					return View::make('errors.showMessage')->with('msg','参数错误')->with('url','/admmaterial/list/1');
				}
				
				$data = htmlspecialchars_decode(urldecode(json_encode(array('articles'=>$aArticles))));
				$res = $this->weixin->getAddTemporaryMaterialNews($data,$oMaterialNews->news_type);
				// 没有appid 暂时写死*******************
				/*$res = (object)array(
						'type' =>'news',
						'media_id'=> 'hu2mf2AUqTsP5akTD5ar7ec9bW7tIpQhtiDrpNrHawKPug_zCBftEbVEnjfj_0H3',
						'created_at' => date('Y-m-d H:i:s',time())
				);*/
				if(!isset($res->errcode)||(isset($res->errcode)&&$res->errcode==0)){
					$oMaterialWxNews->title = $aMaterialNews['title'];
					$oMaterialWxNews->media_id = $res->media_id;
					$oMaterialWxNews->save();
				}else{
					return View::make('errors.showMessage')->with('msg',$res->errmsg)->with('url','/admmaterial/list/1');
				}
				return View::make('errors.showMessage')->with('msg','修改成功')->with('url','/admmaterial/list/'.$oMaterialNews->news_type);
			}
		}else{
			return View::make('errors.showMessage')->with('msg','该记录不存在')->with('url','/admmaterial/list/1');
		}
	}

	//删除图文消息
	public function MaterialDelete($iId){
		$oMaterialNews = MaterialWxNews::find($iId);
		if($oMaterialNews){
			$oNews = MaterialNews::find($oMaterialNews->news_id);
			if($oNews){
				$oNews->delete();
				$oMaterialNews->delete();
				return View::make('errors.showMessage')->with('msg','删除成功')->with('url','/admmaterial/list/1');
			}else{
				return View::make('errors.showMessage')->with('msg','该记录不存在')->with('url','/admmaterial/list/1');
			}
		}else{
			return View::make('errors.showMessage')->with('msg','该记录不存在')->with('url','/admmaterial/list/1');
		}
	}
	
	//上传图片素材
	public function ImageUpload(){
		if(Input::hasFile('file_upload')){
			$newsType = Input::get('type');
			$file = Input::file('file_upload');
			$fileType = strtolower($file->getClientOriginalExtension());
			$msg = self::checkMaterial($file, 'image',$fileType);
			if($msg){
				echo json_encode(array('success'=>false,'msg'=>$msg));
				exit();
			}else{
				$aInfo = array('type'=>'image','temporary_flag'=>0);
				//将素材保存到服务器
				$res = $this->saveMaterial($file, $aInfo, $fileType, $newsType);
				// 没有appid 暂时写死******************* 
				/*$res = (object)array(
						'errcode' => 0,
						'media_id'=> 'hu1mf2AUqTsP5akTD5ar7ec9bW7tIpQhtiDrpNrHawKPug_zCBftEbVEnjfj_0H3',
						'path' =>'/upload/material/0517/3398ec616c57bbab34c446a85ad119b8.jpg',
						'url' =>'',
						'created_at' => date('Y-m-d H:i:s',time())
				);*/
				if(!isset($res->errcode)||(isset($res->errcode)&&$res->errcode==0)){
					$aInfo['media_id'] = $res->media_id;
					$aInfo['path'] = $res->path;
					$aInfo['wx_url'] = isset($res->url)?$res->url:'';
					$aInfo['created_at'] = $aInfo['temporary_flag']==1?date('Y-m-d H:i:s',time()):$res->created_at;
					$oMaterial = new Material($aInfo);
					$oMaterial->save();
					echo json_encode(array('success'=>true,'material_id'=>$oMaterial->id,'media_id'=>$res->media_id,'path'=>$res->path));
					exit();
				}else{
					echo json_encode(array('success'=>false,'msg'=>$res->errmsg));
					exit();
				}
			}
		}else{
			echo json_encode(array('success'=>false,'msg'=>'请上传资料'));
			exit();
		}
	}
	
	//普通素材格式以及大小验证
	private static function checkMaterial($file,$type,$fileType,$temporart_type = 0){
		$size = $file->getSize();
		$msg = '';
		switch($type){
			case 'image':
				$typeArray = array('png','jpg','gif','jpeg');
				$sizeLimit = $temporart_type==0?1*1024*1024:2*1024*1024;
				break;
			case 'voice':
				$typeArray = array('mp3','amr');
				$sizeLimit = $temporart_type==0?2*1024*1024:5*1024*1024;
				break;
			case 'video':
				$typeArray = array('mp4');
				$sizeLimit = 10*1024*1024;
				break;
			case 'thumb':
				$typeArray = array('png','jpg','gif');
				$sizeLimit = $temporart_type==0?64*1024:2*1024*1024;
				break;
		}
		if(!in_array($fileType,$typeArray)){
			$msg .= '素材格式不正确';
		}
		if($size>$sizeLimit){
			$msg .= '素材大小超出限制';
		}
		return $msg;
	}

	private function saveMaterial($file, $aInfo, $fileType, $newsType){
		$reName = md5(date('YmdHis'));
		$path = 'upload/material/'.date('md').'/';
		$file->move(public_path($path),$reName.'.'.$fileType);
		//将素材上传到腾讯服务器
		if($aInfo['type']=='video'){
			$data = array('media'=>'@'.public_path($path).$reName.'.'.$fileType,'description'=>json_encode(array('title'=>$aInfo['title'],'introduction'=>$aInfo['description'])));
		}else{
			$data = array('media'=>'@'.public_path($path).$reName.'.'.$fileType);
		}
		//上传临时素材
		$res = $this->weixin->getAddTemporaryMaterial($data,$aInfo['type'],$newsType);
		$res->path = '/'.$path.$reName.'.'.$fileType;
		return $res;
	}
	
	//图文消息正式推送
	public function MaterialPush(){
		$iId = Input::get('id');
		$iType = Input::get('type');
		$oMaterial = MaterialWxNews::find($iId);
		if($oMaterial){
			if(strtotime($oMaterial->updated_at)+3*24*60*60<time()){
				return json_encode(array('success'=>false,'msg'=>'素材已过期，请重新添加'));
			}
			$data = json_encode(array('filter'=>array('is_to_all'=>true,'tag_id'=>''),'mpnews'=>array('media_id'=>$oMaterial->media_id),'msgtype'=>'mpnews'));
			//群发消息
			$res = $this->weixin->getPush($data,$iType);
			/*$res = (object)array(
					'errcode' => 0
			);*/
			if(!isset($res->errcode)||(isset($res->errcode)&&$res->errcode==0)){
				$oMaterial->push_flag = 1;
				$oMaterial->save();
				return json_encode(array('success'=>true));
			}else{
				return json_encode(array('success'=>false,'msg'=>$res->errmsg));
			}
		}else{
			return View::make('errors.showMessage')->with('msg','该图文素材不存在')->with('url','/admmaterial/list/'.$iType);
		}
	}
	
	//图文推送预览
	public function MaterialPreview($iId,$iType){
		if($iId && $iType){
			return View::make('admin.material.preview')->with('iId',$iId)->with('iType',$iType);
		}else{
			return View::make('errors.showMessage')->with('msg','该图文素材不存在')->with('url','/admmaterial/list/'.$iType);
		}
	
	}

	public function MaterialPreviewDo(){
		$iId = Input::get('id');
		$iType = Input::get('type');
		$oMaterial = MaterialWxNews::find($iId);
		if($oMaterial){
			if(strtotime($oMaterial->updated_at)+3*24*60*60<time()){
				return json_encode(array('success'=>false,'msg'=>'素材已过期，请重新添加'));
			}
			//预览者的微信号 可以通过关注订阅号获取
			$towxname = Input::get('wxnumber');
			if($towxname){
				$data = json_encode(array('towxname'=>$towxname,'mpnews'=>array('media_id'=>$oMaterial->media_id),'msgtype'=>'mpnews'));
				$res = $this->weixin->getPreview($data,$iType);
				if(!isset($res->errcode)||(isset($res->errcode)&&$res->errcode==0)){
					return json_encode(array('success'=>true));
				}else{
					return json_encode(array('success'=>false,'msg'=>'请确认您已关注该公众号'));
				}
			}else{
				return json_encode(array('success'=>false,'msg'=>'请填写微信号接收预览消息'));
			}
		}else{
			return json_encode(array('success'=>false,'msg'=>'该图文素材不存在'));
		}
	}
	
}