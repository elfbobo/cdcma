<?php

/*
|------------------------------
| @author zangguoqing
|调研题目管理
*/
class AdmSurveyController extends BaseController {
	
	//题目列表 
	public function QuestionList()
	{
		$iPhase = Input::get('phase');
		if(!empty($iPhase)){
			$oUserSurveyQuestion =  UserSurveyQuestion::where('phase_id','=',$iPhase) ->paginate(10);
		}else{
			$oUserSurveyQuestion =  UserSurveyQuestion::paginate(10);
		}
		return View::make('admin.usersurvey.surveylist')->with('oUserSurveyQuestion',$oUserSurveyQuestion);
	}

	//删除题目及选项
	public function DeleteSurvey($iId)
	{
		UserSurveyQuestion::destroy($iId);
		$oSurveyItem = UserSurveyItem::where('question_id','=',$iId);
		if(count($oSurveyItem)){
			$oSurveyItem->delete();
		}
		return Redirect::to('/admsurvey/question-list');
	}

	//进入题目编辑页
	public function EditSurvey($iId)
	{
		$oUserSurveyQuestion = UserSurveyQuestion::where('id','=',$iId)->first();
		$oUserSurveyItem = UserSurveyItem::where('question_id','=',$iId)->get();
		return View::make('admin.usersurvey.surveyedit')->with('oUserSurveyQuestion',$oUserSurveyQuestion)->with('oUserSurveyItem',$oUserSurveyItem);
	}

	//进入题目添加页  
	public function  AddQuestion()
	{
		return View::make('admin.usersurvey.surveyadd');
	}

	//新增调研题目 
	public function DoAdds()
	{
		$phase = Config::get('config.phase_id');
		$aMessage = Input::all();
		$oUserServeyQuestion = new UserSurveyQuestion;
		$oUserServeyQuestion->phase_id = $phase;
		$oUserServeyQuestion->q_title = $aMessage['q_title'];
		$oUserServeyQuestion->q_result = $aMessage['q_result'];
		$oUserServeyQuestion->q_order = $aMessage['q_order'];
		$oUserServeyQuestion->q_type = isset($aMessage['q_type'])? $aMessage['q_type']: 1 ;
		$oUserServeyQuestion->save();
		$oUserServeyItem = new UserSurveyItem;
		$iQuestionId =$oUserServeyQuestion -> id;
		$aKey =array('A','B','C','D','E','F','G','H','I','J');
		foreach($aMessage as $key=>$value){
			$sNewKey = substr($key,0,11);
			if($sNewKey == 'optionvalue'){
				$k = substr($key,11,1);
				$aMessage[$k]=$value;
			}
		}
		for($i=0;$i<10;$i++){
			$sKey = $aKey[$i];
			if(!empty($aMessage[$sKey])){
				$oUserServeyItem = new UserSurveyItem;
				$oUserServeyItem -> phase_id = $phase;
				$oUserServeyItem -> question_id = $iQuestionId;
				$oUserServeyItem -> item  = $sKey;
				$oUserServeyItem -> item_title = $aMessage[$sKey];
				$oUserServeyItem ->save();
			}
		}
		return Redirect::to('/admsurvey/question-list');
	}

	//修改调研题目
	public function DoAdd()
	{
		$iId = Input::get('iId');
		$aMessage = Input::all();
		$oUserServeyQuestion = UserSurveyQuestion::find($iId);
		// $oUserServeyQuestion->phase_id = $aMessage['phase_id'];
		$oUserServeyQuestion->q_title = $aMessage['q_title'];
		$oUserServeyQuestion->q_result = $aMessage['q_result'];
		$oUserServeyQuestion->q_order = $aMessage['q_order'];
		$oUserServeyQuestion->q_type = $aMessage['q_type'];
		$oUserServeyQuestion->save();
		$iId =$aMessage['iId'];
		$phase = $aMessage['phase'];
		$oSurveyItem = UserSurveyItem::where('question_id','=',$iId)->where('phase_id','=',$phase)->get();
		foreach($aMessage as $key=>$value){
			$sNewKey = substr($key,0,11);
			if($sNewKey == 'optionvalue'){
				$k = substr($key,11,1);
				$aMessage[$k]=$value;
			}
		}
		$aKeys = array_keys($aMessage);
		if(count($oSurveyItem)){
	   		//题目选项的修改
	   	    /*foreach($oSurveyItem as $value){
		   	        $oKey = $value -> item; 
		   	         
		   	        if(in_array($oKey,$aKeys)){
		   	        	 
		   	            $value -> item_title = $aMessage[$oKey];
		   	            $value -> save();
		   	        }else{
		   	        	$value = UserSurveyItem::where('question_id','=',$iId)->where('phase_id','=',$phase)->get();
		   	            $value -> where('item','=',$oKey) ->delete();
		   	        }
	   	    }*/
	   	    $aKey =array('A','B','C','D','E','F','G','H','I','J');
	   	    foreach($aKeys as $value){
	   	        if(in_array($value,$aKey)){
					$oChoice = UserSurveyItem::where('question_id','=',$iId)->where('phase_id','=',$phase)->where('item','=',$value)->get();
					if(count($oChoice)){
						$oChoice->item_title = $aMessage[$value];
					}else{
						if(!empty($aMessage[$value])){
							$oNewChoice = new UserSurveyItem;
							$oNewChoice-> phase_id = $phase;
							$oNewChoice->question_id = $iId;
							$oNewChoice->item = $value;
							$oNewChoice->item_title = $aMessage[$value];
							$oNewChoice->save();
						}
					}
	   	        } 
	   	    }
		}else{
			$aKey =array('A','B','C','D','E','F','G','H','I','J');
			foreach ($aKeys as $value){
				if(in_array($value,$aKey)){
					if(!empty($aMessage[$value])){
						$oNewChoices = new UserSurveyItem;
						$oNewChoices-> phase_id = $phase;
						$oNewChoices->question_id = $iId;
						$oNewChoices->item = $value;
						$oNewChoices->item_title = $aMessage[$value];
						$oNewChoices->save();
					}
				}
			}
		}
	    return Redirect::to('/admsurvey/question-list');
	}
 
	public function  HasChoiceOrNot($iId,$phase_id)
	{
		$oSurveyItem = UserSurveyItem::where('question_id','=',$iId)->where('phase_id','=',$phase_id)->get();
		$aChoice = array('A','B','C','D','E','F','G','H','I','J');
		if(count($oSurveyItem)){
			foreach($oSurveyItem as $value){
				$key = $value -> item;
				$val = $value ->item_title;
				$aChoice[$key] = $val;
			}           
			$aChoice['iId'] = $iId;
			$aChoice['phase'] = $phase_id;        
			return View::make('admin.usersurvey.survey')->with('aChoice',$aChoice);
		}else{
			$aChoice['iId'] = $iId;
			$aChoice['phase'] = $phase_id;
			return View::make('admin.usersurvey.survey')->with('aChoice',$aChoice);
		}
	}
	 
	//题目选项的添加和修改 
	public function DoAddChoice()
	{
		$aAllMessage = Input::all();
		$iId =$aAllMessage['iId'];
		$phase = $aAllMessage['phase'];
		$oSurveyItem = UserSurveyItem::where('question_id','=',$iId)->where('phase_id','=',$phase)->get();
		$aKeys = array_keys($aAllMessage); 
		if(count($oSurveyItem)){
			//题目选项的修改
			foreach($oSurveyItem as $value){
				$oKey = $value -> item; 
				if(in_array($oKey,$aKeys)){
					$value -> item_title = $aAllMessage[$oKey];
					$value -> save();
				}else{
					$value -> where('item','=',$oKey) ->delete();
				}
			}
			return Redirect::to('/admsurvey/question-list');
		}else{
			//题目选项的添加
			$aAllKeys = array('A','B','C','D','E','F','G','H','I','J');
			for($i=0;$i<10;$i++){
				$sKey = $aAllKeys[$i];
				if(!empty($aAllMessage[$sKey])){
					$oNewSurveyItem = new UserSurveyItem();
					$oNewSurveyItem -> item_title =$aAllMessage[$sKey];
					$oNewSurveyItem -> item = $sKey;
					$oNewSurveyItem -> phase_id =$phase;
					$oNewSurveyItem -> question_id =$iId;
					$oNewSurveyItem -> save();
				}
			}
			return Redirect::to('/admsurvey/question-list');
		}
	}
	
	public function SurveyResult()
	{
		$sSurveyResult = "用户id,用户类型,用户麦粒,推送那期调研,是否同意为其注册医脉通,用户爱好,是否关闭调研框,是否参与调研,用户编码,姓名,省份,城市,医院,科室,代表姓名,代表用户编码,调研结果"."\n"; 
		$phase = Config::get('config.phase_id');
		$oUserSurveySub = UserSurveySub::where('phase_id','=',$phase)->get();
		$aHospital = Hospital::lists('name','id');
		foreach($oUserSurveySub as $value){
			$iGold = $value -> gold;
			$sAgreeRegis = $value -> agree_register_medlive_flag;
			switch ($sAgreeRegis){
				case 0:
					$sAgreeRegis = '默认';
					break;
				case 1:
					$sAgreeRegis = '同意';
					break;
				case 2:
					$sAgreeRegis = '以后考虑';
					break;
				case 3:
					$sAgreeRegis = '关闭弹框';
					break;
			}
			$uId = $value -> user_id;
			$sUserLike = $value -> adalate_type;
			switch ($sUserLike){
			case 1:
				$iUserLike ="拜新同偏好者";
				break;
			case 2:
				$iUserLike ="拜新同中立者";
				break;
			}
			$sCloseOrOpen = $value -> is_refuse_survey;
			switch ($sCloseOrOpen){
				case 0:
					$sCloseOrOpen ="否";
					break;
				case 1:
					$sCloseOrOpen ="是";
					break;
			}
			$sHasSurvey = $value -> has_survey;
			switch ($sHasSurvey){
				case 0:
					$sHasSurvey ="否";
					break;
				case 1:
					$sHasSurvey ="是";
					break; 
			}
			$oUser = User::where('id','=',$uId)->get();
			foreach ($oUser as $value){
				$iUserId = isset($value ->id)? $value ->id :'null';
				$sUserCwid = isset($value -> invite_code)? $value -> invite_code :'null';
				$sName = isset($value->user_name)? $value->user_name : 'null';
				$sProvince =  $value->user_province;
				if(array_key_exists($sProvince,$aHospital)){
					$sProvince = $aHospital[$sProvince];
				}else{
					$sProvince = '';
				}
				$sCity =   $value ->user_city ;
				if(array_key_exists($sCity,$aHospital)){
					$sCity = $aHospital[$sCity];
				}else{
					$sCity = '';
				}
				$shospital =  $value->user_company;
				if(is_numeric($shospital)&&$shospital!=0){
					$shospital = $aHospital[$shospital];
				}else{
					if(empty($shospital)){
						$shospital = $value -> user_company_name;
					}else{
						$shospital = $value -> user_company;
					}
				}
				$sdepartment = isset($value->user_department)? $value->user_department : 'null';
				$iLinkRepId = isset($value -> link_rep_id)? $value -> link_rep_id : 'null';
				$iUserType =$value -> user_type;
				switch ($iUserType){
					case 0:
						$iUserKind = "尚未分类";
						break;
					case 1:
						$iUserKind ="拜新同用户";
						break;
					case 2:
						$iUserKind ="拜阿用户";
						break;
					case 3:
						$iUserKind ="拜新同和拜阿用户";
						break;
				}
				$oDoctor = User::where('id','=',$iLinkRepId)->first();
				if(count($oDoctor)){
					$sRepName = $oDoctor ->user_name;
					$iRepCwid = $oDoctor ->user_cwid;
				}else{
					$sRepName = 'null';
					$iRepCwid = 'null';
				}
				$sSurveyResult .= $iUserId.',';
				$sSurveyResult .= $iUserKind.',';
				$sSurveyResult .= $iGold.',';
				$sSurveyResult .= $phase.',';
				$sSurveyResult .= $sAgreeRegis.',';
				$sSurveyResult .=$iUserLike.',';
				$sSurveyResult .= $sCloseOrOpen.',';
				$sSurveyResult .=$sHasSurvey.',';
				$sSurveyResult .= "\t".$sUserCwid.',';
				$sSurveyResult .= $sName.',';
				$sSurveyResult .=  $sProvince.',';
				$sSurveyResult .= $sCity.',';
				$sSurveyResult .= $shospital.',';
				$sSurveyResult .= $sdepartment.',';
				$sSurveyResult .= $sRepName.',';
				$sSurveyResult .= $iRepCwid.',';
				$oSurveyResult = UserSurveyAnswer::where('user_id','=',$iUserId)->where('phase_id','=',$phase)->orderBy('question_id')->get();  
				if(count($oSurveyResult)){
					foreach($oSurveyResult as $value){
						$oOPtion = $value-> option;
						$sSurveyResult.=$oOPtion.',';
					}
				}else{
					$oOPtion = 'null';
					$sSurveyResult.=$oOPtion.',';
				} 
				$sSurveyResult = $sSurveyResult."\n";
			}
		}
		$sSurveyResult = strval($sSurveyResult);
		$sSurveyResult = iconv("UTF-8",'GB2312//IGNORE',$sSurveyResult);
		header("Content-type:text/csv");
		header("Content-Type: application/force-download");
		header("Content-Disposition: attachment; filename=user_survey_result.".date('Y-m-d').".csv");
		header('Expires:0');
		header('Pragma:public');
		echo $sSurveyResult;
	}
 
}