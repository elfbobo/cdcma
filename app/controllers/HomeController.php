<?php

use Illuminate\Support\Facades\Redirect;

class HomeController extends BaseController {

     const REDIS_CACHE_TIME = 300;
	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	
	/**
	 * 设置未审核视频的状态
	 */
	public function Videoauditauto(){
		/*$Configs = new Configs();
		$configRs = $Configs::find(1);
		$video_audit_times = @$configRs->video_audit_times ? (int)$configRs->video_audit_times : 10;
		//->where('id',"<",85)
		$oEducation = AspirinEducation::select("id","ppt_title","created_at")->where('education_type',0)->get()->toArray();
		if($oEducation){
			foreach($oEducation as $rs=>$v){
				$createtime = strtotime($v['created_at']);
				if(((time()-$createtime)/60)>$video_audit_times){
					$oEducations = AspirinEducation::find($v['id']);
					$oEducations->education_type = 1;
					$oEducations->save();
				}
			}
		}*/
	}

	public function Index()
	{
		if(user_agent_is_mobile()){
			return Redirect::to('/mobile');
		}
		//取名医面对面精彩预告最新的一条
		$oFaceVideoAdvance = FaceVideo::getLatestFaceVideoAdvance();
		if(!$oFaceVideoAdvance){
			$oFaceVideoReview = FaceVideo::getLatestFaceVideoReview(1);
		}
		//资讯中心前五条
		$oInformation = Information::orderBy("created_at","desc")->limit(5)->get();
		$aNewOrNot =array();
		foreach($oInformation as $v){
			$iTime = strtotime($v ->created_at);
			$iNowTime = time();
			$num = 7*24*60*60;
			$iId = $v->id;
		    if($iNowTime-$iTime <= $num){
		    	$iUid = Session::get('userid');
		    	$oUserInformationLog= UserInformationLog::where('type',0)->where('information_id',$iId)->where('user_id',$iUid)->first();
		    	if(count($oUserInformationLog)){
		    	   $aNewOrNot[$iId]=0;//不显示new
		    	}else{
		    	   $aNewOrNot[$iId]=1;//显示new
		    	}
		    }else{
		         $aNewOrNot[$iId]=0;
		    }
		}
		//资讯中心不足5条时显示微信号内容
		$iMaterialCount = 5-count($oInformation);
		$aMaterialNews = array();
		if($iMaterialCount != 0){
			$oMaterialNews = MaterialNews::orderBy("created_at","desc")->take($iMaterialCount)->get();
			foreach($oMaterialNews as $k=>$v){
				if(time()-strtotime($v->created_at) <= 7*24*60*60){
					$iUid = Session::get('userid');
					$oUserInformationLog= UserInformationLog::where('type','!=',0)->where('information_id',$v->id)->where('user_id',$iUid)->first();
					if(count($oUserInformationLog)){
						$iFlag=0;//不显示new
					}else{
						$iFlag=1;//显示new
					}
				}else{
					$iFlag=0;
				}
				$aMaterialNews[] = array(
					'id'  =>  $v->id,
					'title' => $v->title,
					'created_at' => substr($v->created_at,0,10),
					'newsflag' => $iFlag
				);
			}
		}
		
		//取名医面对面精彩回顾最新的三条
		$iCount = 3;
	    $sFaceVideoRewiewRedis = 'faceVideoRewiewRedis';
		// if (Cache::has($sFaceVideoRewiewRedis)){
		//     $oFaceVideoReview = Cache::get($sFaceVideoRewiewRedis);
		// }else{
	     	$oFaceVideoReview = FaceVideo::getLatestFaceVideoReview($iCount);
	 	//     	Cache::put($sFaceVideoRewiewRedis,$oFaceVideoReview,self::REDIS_CACHE_TIME);
		// }
		//专家风采
		$iCountDoc = 5;
	    $sDocsRedis = 'doctorsRedis';
	 	// if (Cache::has($sDocsRedis)){
		//     $oDocs = Cache::get($sDocsRedis);
		// }else{
	     	$oDocs = FaceDoc::findAllDoc($iCountDoc);
 		// Cache::put($sDocsRedis,$oDocs,self::REDIS_CACHE_TIME);
		// }
		$Consultation4 = Consultation::findAllVideoCount(4,3);
		//跨领域学术交流预告
		$Consultation1 = Consultation::getLatestConsultation(1);
		$oTopic = null;
		if($Consultation1){
			$oTopic = ConsultationTopic::where('meeting_id',$Consultation1->id)->get();
		}
		$Consultation2 = Consultation::getLatestConsultation(2);
		$Consultation3 = Consultation::getLatestConsultation(3);
		//*******调研功能start**********
		$aQuestion = array();
		//调研功能是否开启
		$iCloseSurvey = Config::get('config.is_survey_open');
		$iSurveySet = 1;		//调研赠送的麦粒是否达到指定份数（0：否；1：是）
		if($iCloseSurvey==1&&Session::get('userid')&&Session::get('roleid')==3){
			//若开启调研，则判断当前用户是否可以参加调研
			$oUserSurveySub = UserSurveySub::where('user_id',Session::get('userid'))->where('phase_id',Config::get('config.phase_id'))->first();
			//第一期特殊处理，给所有人推送调研 start
			if(Config::get('config.phase_id')==1){
				if(!$oUserSurveySub){
					//若未给当前用户推送调研，主动给期推送当前期
					$aUserSurveySub = array(
						'user_id'	=> Session::get('userid'),
						'phase_id'	=> Config::get('config.phase_id')
					);
					$oUserSurveySub = new UserSurveySub($aUserSurveySub);
					$oUserSurveySub->save();
				}
			}
			//第一期特殊处理，给所有人推送调研 end
			if($oUserSurveySub
				&&$oUserSurveySub->is_refuse_survey==0
				&&$oUserSurveySub->has_survey==0){
				//用户可以参加调研,取出当前期的调研题
				$aQuestion = UserSurveyQuestion::getAllQuestionAndItem(Config::get('config.phase_id'));
			}
			$iCountGold = $oUserSurveySub = UserSurveySub::where('phase_id',Config::get('config.phase_id'))
				->where('gold','>',0)
				->count();
			if($iCountGold<Config::get('config.survey_gold_set')){
				$iSurveySet = 0;	//未达到限制
			}
			
		}
		//获取推荐精品课堂
     	$recordfqlistRs = FaceVideo::getLatestFaceVideoReview($iCount,"1");
		/*$recordfqlistRs = FaceVideo::select('face_video.*','face_doc.doc_name','face_doc.doc_thumb','face_doc.doc_position','face_doc.doc_department','face_doc.doc_hospital','face_doc.doc_introduction')
			->leftJoin('face_doc','face_video.doc_id','=','face_doc.id')
			// ->where('video_type','=','1')
			->where('video_rec','=','1')
			->orderBy('start_time','DESC')
			->take(3)
			->get();*/
		//*******调研功能start**********
		return View::make('front.index')
			->with('oFaceVideoAdvance',$oFaceVideoAdvance)
			->with('oFaceVideoReview',$oFaceVideoReview)
			->with('oDocs',$oDocs)
			->with('Consultation4',$Consultation4)
			->with('Consultation1',$Consultation1)
			->with('Consultation2',$Consultation2)
			->with('Consultation3',$Consultation3)
			->with('oTopic',$oTopic)
			->with('oInformation',$oInformation)
			->with('aMaterialNews',$aMaterialNews)
			->with('aQuestion',$aQuestion)
			->with('iSurveySet',$iSurveySet)
			->with('aNewOrNot',$aNewOrNot)
			->with('recordfqlistRs',$recordfqlistRs);
	}
	
	//登录页面
	public function Login(){
		//if(Auth::check()){
		if(Session::has('userid')){
			return Redirect::to('/');
			//返回到上一页面
			// echo "<script>history.go(-1);</script>";
		}else{
			return  View::make('front.login');
		}
	}

	//登陆页面
	public function LoginDo(){
		/*if(strtoupper(Session::get("authnum_session"))!=strtoupper(Input::get('verifi_code'))){
			// return 'code_err';
			return json_encode(array('success'=>'code_err','data'=>''));
		}*/
		$rules = array(
			'user_nick' => 'required',
			'password'  => 'required'
		);
		$input = array(
			'user_nick'	=> Input::get('user_nick'),
			'password'	=> Input::get('password'),
		);
		$input2 = array(
			'user_tel'	=> Input::get('user_nick'),
			'password'	=> Input::get('password'),
		);
		$input3 = array(
			'user_email'=> Input::get('user_nick'),
			'password'	=> Input::get('password'),
		);
		$validator = Validator::make(Input::all(), $rules);
		if($validator->passes()) {
			if (Auth::attempt($input) ||Auth::attempt($input2) ||Auth::attempt($input3)) {
				//登陆成功
				Session::put('roleid',Auth::User()->role_id);//用户角色
				Session::put('userid',Auth::User()->id);//用户id
				//所有用户都要先同意免责条款
				$iUserDoctorId = Auth::User()->id;
				$oUserDisclaimerLog = UserDisclaimerLog::where('user_id',$iUserDoctorId)->first();
				if(count($oUserDisclaimerLog) < 1){
					//退出登录并清理session
					Auth::logout();
					Session::flush();
					return json_encode(array('success'=>'agreeterms','data'=>'','userid'=>$iUserDoctorId));
				}
				if(Auth::User()->role_id==2){
					//若角色为代表
					$oUser = User::find(Auth::User()->id);
					//判断代表是否是扫专项基金统一二维码进入的，如果是，就给代表生成一个专项基金专属二维码，扫此码注册的医生视为专项基金用户  dll 0411
					$iAspirinFlag = intval(Input::get('is_aspirin',0));
					if($iAspirinFlag == 4){
						if($oUser->invite_code == ''){
							//生成邀请码
							$iInvitedCode = User::MakeInviteCode();
							$oUser->invite_code = $iInvitedCode;
							$oUser->save();
						}
						//生成当前用户的专项基金注册二维码
						$sEwmUrl = AspirinEwm::getErweima('http://cdcma.bizconf.cn/mobile/register/'.$oUser->invite_code.'/'.$iAspirinFlag);
						return json_encode(array('success'=>'success','data'=>'','url'=>$sEwmUrl));
					}
					if($oUser->invite_code == ''){
						//生成邀请码
						$iInvitedCode = User::MakeInviteCode();
						$oUser->invite_code = $iInvitedCode;
						$oUser->save();
						// echo "<script>alert('您的用户编码为".$iInvitedCode."');window.location.href='/'</script>";die;
						return json_encode(array('success'=>'success','data'=>$iInvitedCode,'url'=>''));
					}
				}
				// return Redirect::to('/');
				//返回到上一页面
				// echo "<script>history.go(-2);</script>";
				//登录成功
				// return 'success';
				return json_encode(array('success'=>'success','data'=>'','url'=>''));
			} else {
				// return Redirect::to('/login');
				// return 'error';
				return json_encode(array('success'=>'error','data'=>'','url'=>''));
			}
		}
		return json_encode(array('success'=>'error','data'=>'','url'=>''));
		// return Redirect::to('/login');
		// return 'error';
	}
	
	public function getCode(){
		include_once  app_path().'/include/ValidateCode.class.php';  //先把类包含进来，实际路径根据实际情况进行修改。
		$_vc = new ValidateCode();		//实例化一个对象
		$_vc->doimg();	
		Session::put('authnum_session',$_vc->getCode() );//验证码保存到SESSION中
	}
	
	/**
	 * 获取验证码
	 */
	public function AuthCode(){
		include_once  app_path().'/include/ValidateCode.class.php';  //先把类包含进来，实际路径根据实际情况进行修改。
		$_vc = new ValidateCode();		//实例化一个对象
		$_vc->doimg();	
		Session::put('authnum_session',$_vc->getCode() );//验证码保存到SESSION中
	}
	
	//退出
	public function LoginOut(){
		Auth::logout();
		Session::flush();
		if(user_agent_is_mobile()){
			return Redirect::to('/mobile-user');
		}
		return Redirect::to('/');
	}
	
	//注册
	public function Register(){
		if(Session::has('userid')){
			return Redirect::to('/');
		}else{
			//获取所有省份
			$aHosp = Hospital::getChildren();
			return  View::make('front.register')->with('aHosp',$aHosp);
		}
	}
	
	//验证邀请码是否正确
	public function RegisterCode($sCode){
		$sCode = strtoupper(trim($sCode));
		//判断是否是拜耳代表注册
		if($sCode=='BAYER1' || $sCode=='BAYER2'){
			return $sCode;
		}
		//该邀请码是否存在
		$oUser = User::where('invite_code',$sCode)->first();
		if($oUser){
			return $oUser->id;
		}else{
			return 'no_code';
		}
	}
	
	//验证昵称是否存在
	public function RegisterCheck($sNick,$sTel,$sEmail=''){
		$iCount = User::where('user_nick',$sNick)->count();
		if($iCount){
			return 'nick_exist';
		}
		$iCount = User::where('user_tel',$sTel)->count();
		if($iCount){
			return 'tel_exist';
		}
		if ($sEmail) {
			$iCount = User::where('user_email',$sEmail)->count();
			if($iCount){
				return 'email_exist';
			}
		}
	}

	/** 
	 * 
	 * */
	public function postAgreeDisclaimer()
	{
		$iUserId = Input::get('old_user_id');
		$oUserDisclaimerLogExists = UserDisclaimerLog::where('user_id',$iUserId)->first();
		if(count($oUserDisclaimerLogExists) > 0){
			return json_encode(array('success'=>'success'));
		}else{
			$oUserDisclaimerLog = new UserDisclaimerLog;
			$oUserDisclaimerLog ->user_id = $iUserId;
			$oUserDisclaimerLog -> save();
			return json_encode(array('success'=>'success'));
		}
	}
	
	/**
	 * @param number $iIsH5默认为0，表示非H5,为1表示H5注册，为2，表示非目标用户注册； 为3，表示ME商务标签医生；为4，表示专项基金用户；为5，表示四川全科骨干医生用户；
	 */
	public function RegisterDo ($iIsH5=0) {
		$aIsH5 = array(0,1,2,3,4,5);
		if (!in_array($iIsH5,$aIsH5)) {
			die('error');
		}
		$aInput = Input::all();
		//验证是否重复注册
		$res = $this->RegisterCheck($aInput['user_nick'],$aInput['user_tel'],$aInput['user_email']);
		if($res){
			return Redirect::to('/');
		}
		unset($aInput['upload_file']);
		unset($aInput['upload_card_file']);
		$oUser = new User();
		//验证成功
		$iRoleId = $aInput['rep_type']>0?2:3;
		if($iRoleId==3){
			//医生，判断是否提交上级id
			if($aInput['link_user_id']==0){
				return 'error';
			}
		}
		if($oUser->Register($aInput,$iRoleId)->save()){
			$sRegisterCode = User::MakeInviteCode();
			$oUser->invite_code = $sRegisterCode;
			//当前注册者关联的所有的上级ids（gk）
			if (isset($aInput['link_user_id'])&&$aInput['link_user_id']!=0){
			    $oSuperior = User::where('id',$aInput['link_user_id'])->first();
			    //如果上级是非目标用户，则该用户为非目标用户
			    if ($oSuperior->is_h5==2) {
			    	$iIsH5 = 2;
			    }
			    //如果上级是ME商务标签用户，则该用户为商务标签用户   dll
			    if ($oSuperior->is_h5==3 ||$oSuperior->id==40628) {
			    	$iIsH5 = 3;
			    }
			    //如果上级是专项基金标签用户，则该用户为专项基金标签用户   dll
			    if ($oSuperior->is_h5==4) {
			    	$iIsH5 = 4;
			    }
			    //如果上级是四川全科骨干医生标签用户，则该用户为四川全科骨干医生标签用户   dll
			    if ($oSuperior->is_h5==5) {
			    	$iIsH5 = 5;
			    }
			    if ($oSuperior->superior_ids){
			        $aSuperiorIds = unserialize($oSuperior->superior_ids);
			        array_push($aSuperiorIds, $aInput['link_user_id']);
			        $oUser->superior_ids = serialize($aSuperiorIds);
			    }else{
			        $oUser->superior_ids = serialize(array($aInput['link_user_id']));
			    } 
			    //获取当前用户关联代表的id
			    $iRepId = User::getRepId($oUser->superior_ids);
			    $oUser->link_rep_id = $iRepId;
			}
			$oUser->is_h5 = $iIsH5;
			//如果上传了医师证号和证照，flag置为1
			if(!empty($oUser->card_number) && !empty($oUser->card_thumb) ){
				$oUser->card_auth_flag = 1;
			}
			$oUser->created_at = date('Y-m-d H:i:s',time());
			$oUser->save();
			$oUser = User::where('user_nick',$aInput['user_nick'])->first();
			Auth::loginUsingId($oUser->id);
			if($oUser->role_id==3){
				//为用户注册医脉通
				User::RegMedlive($oUser);
			}
			//保存同意免责条款
			$iUserId = $oUser->id;
			$oUserDisclaimerLog = new UserDisclaimerLog;
			$oUserDisclaimerLog ->user_id = $iUserId;
			$oUserDisclaimerLog ->save();
			if($iIsH5){
				if($iIsH5 == 4){
					return Redirect::to('/mobile/asp');
				}
				return View::make('mobile.registersuccess')->with('code',$sRegisterCode);
			}
			return View::make('errors.registersuccess')->with('code',$sRegisterCode);
		}else{
			return 'error';
		}
	}
	
	/**
	 * 手机版调研登录
	 * 高红叶 2016-11-14备注
	 * id>100000的签到为线下会议的签到，区别于之前的签到逻辑
	 */
	public function LoginMobileDo(){
		$rules = array(
			'user_nick'    	 => 'required',
			// 'password'  => 'required'
		);
		$input = array(
			'user_nick' => Input::get('user_nick'),
			'password'	=> '',
		);
		$oUserOrganizer = User::select('id')->where('invite_code',strtoupper(Input::get('organizer_code')))->first();
		if(!$oUserOrganizer){
			return 'code_err';
		}
		$validator = Validator::make(Input::all(), $rules);
		if($validator->passes()) {
			$oUser = User::select('id','role_id')->where('user_nick',Input::get('user_nick'))->orWhere('user_tel',Input::get('user_nick'))->first();
			if($oUser){
				if($oUser->role_id!=3){
					return 'rep';
				}
				// Auth::loginUsingId($oUser->id);
				Session::put('cdma_user_id',$oUser->id);
				$iVideoId = Session::get('survey_video_id');
				//判断当前时间是否在直播20分钟这个时间段之前
				$oVideo = FaceVideo::select('end_time','video_url')->where('id',$iVideoId)->first();
				//id>100000的签到为线下会议的签到，区别于之前的签到逻辑
				if($iVideoId>100000){
					//默认为录播签到
					$iVideoType = '1';
					//查看用户是否签到
					$oSign = SignInLog::where('cat_id',1)
						->where('user_id',$oUser->id)
						->where('video_id',$iVideoId)
						->where('video_type',$iVideoType)
						->first();
					if($oSign){
						return 'repeat';
					}
					//新增签到记录
					//签到记录
					$aSign = array(
						'user_id'		=> $oUser->id,
						'video_id'		=> $iVideoId,
						'organizer_id'	=> $oUserOrganizer->id,
						'video_type'    => $iVideoType
					);
					$oSign = new SignInLog($aSign);
					$oSign->save();
					return 'success';
				}
				$iNowTimestamp = time();
				$iVideoEndTime = strtotime($oVideo->end_time);
				//加直播积分和加录播积分中间时间戳
				$iMiddleTimestamp = $iVideoEndTime+20*60;
				if($iNowTimestamp<$iMiddleTimestamp){
					//加直播的积分
					$iVideoType = '2';
					//直播查看用户是否签到
					$sSessionName = 'sign_in_session_'.$iVideoId.'_'.$oUser->id;
					if(Session::has($sSessionName)){
						return 'repeat';
					}else{
						//写文件
						$dir = app_path().'/storage/logs/redislog';
						$sLog =  $oUser->id.','.$oUserOrganizer->id.','.time().';';
						recordLog($iVideoId, $sLog,$dir);
						Session::put($sSessionName,1);
						//存数据库备份
						$aLog = array(
							'user_id'	=> $oUser->id,
							'video_id'	=> $iVideoId,
							'organizer'	=> $oUserOrganizer->id,
							'time'		=> time()
						);
						$oLog = new SignInLogBackup($aLog);
						$oLog->save();
					}
					return 'success';
				}else{
					//直播签到是否结束
					if(!strstr($oVideo->video_url, 'upload')){
						return 'live_sign_stop';
						die;
					}
					//加录播的积分
					$iVideoType = '1';
					//查看用户是否签到
					$oSign = SignInLog::where('cat_id',1)
						->where('user_id',$oUser->id)
						->where('video_id',$iVideoId)
						->where('video_type',$iVideoType)
						->first();
					if($oSign){
						return 'repeat';
					}
					//录播签到加积分操作
					$this->SignInAddScore($oUser->id,$iVideoId,$oUserOrganizer->id,$iNowTimestamp );
					return 'success';
				}
			}else{
				return 'error';
			}
		}
		//登录失败
		return 'error';
	}
	
	
	//签到加积分操作（若时间为直播结束后20分钟之前的加直播积分，否则加录播积分）
	public function SignInAddScore($iUid,$iVideoId,$iUOrgaId,$iNowTimestamp){
		//判断当前时间是否在直播20分钟这个时间段之前
		$oVideo = FaceVideo::find($iVideoId);
		$iVideoEndTime = strtotime($oVideo->end_time);
		//加直播积分和加录播积分中间时间戳
		$iMiddleTimestamp = $iVideoEndTime+20*60;
		if($iNowTimestamp>=$iMiddleTimestamp&&!strstr($oVideo->video_url, 'upload')){
			return 'live_sign_stop';
			die;
		}
		//签到记录
		$aSign = array(
			'user_id'		=> $iUid,
			'video_id'		=> $iVideoId,
			'organizer_id'	=> $iUOrgaId
		);
		$oSign = new SignInLog($aSign);
		$oSign->save();
		if($iNowTimestamp<$iMiddleTimestamp){
			//加直播的积分
			$iVideoType = '2';
			$iAddMin = LIVE_LIMIT;
		}else{
			//加录播的积分
			$iVideoType = '1';
			$iAddMin = REVIEW_LIMIT;
		}
		$oVideoLog = VideoLog::HasLogSign(1,$iVideoId,$iVideoType,$iUid,0);
		if(!$oVideoLog){
			//不存在log,加积分
			// FaceVideo::addUserScore($iVideoId,$iAddMin,$iUid,$iVideoType);
			//加积分记录
			$creditIN = CreditController::getInstance($iVideoId,$iAddMin,$iUid,'signin');
			$creditIN->optSignInCredit($iVideoType);
			$oSign->score = $iAddMin;
			$oSign->save();
		}
		if($iNowTimestamp>=$iMiddleTimestamp){
			//加录播的积分,更新签到标记位
			$oSign->video_type = 1;
			$oSign->save();
			//记录用户看录播一次
			VideoReviewLog::addUserReviewLog($iVideoId,$iUid,$oSign->score,1);
		}
	}
	
	//手机版注册
	public function RegisterMobile($code=''){
		if(Session::has('cdma_user_id')){
			$iVideoId = Session::get('survey_video_id');
			if($iVideoId){
				return Redirect::to('/docface/survey/'.$iVideoId);
			}
		}else{
			if(!$code){
				return View::make('front.survey.code');
			}else{
				return View::make('front.survey.register')->with('code',$code);
			}
		}
	}

	//手机版注册--新用户注册---线下会议签到---跳转注册固定邀请码 8TTP3Q
	public function RegisterMobilenewuser($code='',$iMeetingCode){
		return View::make('front.survey.registernewuser')
			->with('iMeetingCode',$iMeetingCode)
			->with('code',$code);
	}
	
	//手机版注册do
	public function RegisterMobileDo($code=''){
		if(!$code){
			return View::make('front.survey.code');
		}else{
			$aInput = Input::all();
			//验证是否重复注册
			$res = $this->RegisterCheck($aInput['user_nick'],$aInput['user_tel'],$aInput['user_email']);
			if($res){
				return Redirect::to('/');
			}
			$oUserOrganizer = User::where('invite_code',$code)->first();
			$iVideoId = Session::get('survey_video_id');
			if(!$oUserOrganizer){
				//返回登录页面
				if($iVideoId){
					return Redirect::to('/docface/survey/'.$iVideoId);
				}
			}
			unset($aInput['upload_file']);
			unset($aInput['upload_file_certificate']);
			if(isset($aInput['user_thumb_certificate'])){
				$sCardThum = $aInput['user_thumb_certificate'];
				unset($aInput['user_thumb_certificate']);
				$aInput['card_thumb'] = $sCardThum;
			}
			$aInput['link_user_id'] = $oUserOrganizer->id;
			$oUser = new User();
			//医生，判断是否提交上级id
			if($aInput['link_user_id']==0){
				return 'error';
			}
			//验证成功
			if($oUser->Register($aInput)->save()){
				$sRegisterCode = User::MakeInviteCode();
				$oUser->invite_code = $sRegisterCode;
				//当前注册者关联的所有的上级ids（gk）
				if (isset($aInput['link_user_id'])){
				    $oSuperior = User::where('id',$aInput['link_user_id'])->first();
				    if ($oSuperior->superior_ids){
				        $aSuperiorIds = unserialize($oSuperior->superior_ids);
				        array_push($aSuperiorIds, $aInput['link_user_id']);
				        $oUser->superior_ids = serialize($aSuperiorIds);
				    }else{
				        $oUser->superior_ids = serialize(array($aInput['link_user_id']));
				    }
				    //获取当前用户关联代表的id
				    $iRepId = User::getRepId($oUser->superior_ids);
				    $oUser->link_rep_id = $iRepId;
				}
				$oUser->created_at = date('Y-m-d H:i:s',time());
				$oUser->save();
				if($oUser->role_id==3){
					//为用户注册医脉通
					User::RegMedlive($oUser);
					$iDoctorId = $oUser->id;
					$oUserDisclaimerLog = new UserDisclaimerLog();
					$oUserDisclaimerLog -> user_id= $iDoctorId;
					$oUserDisclaimerLog -> save();
				}
				// Auth::loginUsingId($oUser->id);
				Session::put('cdma_user_id',$oUser->id);
				$iNowTimestamp = time();
				//签到成功功能******************start***************************
				$this->SignInAddScore($oUser->id,$iVideoId,$oUserOrganizer->id,$iNowTimestamp);
				//签到成功功能****************** end ***************************
				return View::make('errors.mregistersuccess')->with('code',$sRegisterCode);
			}else{
				return 'error';
			}
		}
	}
	
	//手机版注册do
	public function RegisterMobileDonewuser($code='',$iMeetingCode){
	if(!$code){
			return View::make('front.survey.code');
		}else{
			$aInput = Input::all();
			//验证是否重复注册
			$res = $this->RegisterCheck($aInput['user_nick'],$aInput['user_tel'],$aInput['user_email']);
			if($res){
				return Redirect::to('/');
			}
			
			$oUserOrganizer = User::where('invite_code',$code)->first();
			/*$iVideoId = Session::get('survey_video_id');
			if(!$oUserOrganizer){
				//返回登录页面
				if($iVideoId){
					return Redirect::to("/docface/offline-sign");
				}
			}*/
			unset($aInput['upload_file']);
			unset($aInput['upload_file_certificate']);
			if(isset($aInput['user_thumb_certificate'])){
				$sCardThum = $aInput['user_thumb_certificate'];
				unset($aInput['user_thumb_certificate']);
				$aInput['card_thumb'] = $sCardThum;
			}
			$aInput['link_user_id'] = $oUserOrganizer->id;
			$oUser = new User();
			//医生，判断是否提交上级id
			if($aInput['link_user_id']==0){
				return 'error';
			}
			//验证成功
			if($oUser->Register($aInput)->save()){
				$sRegisterCode = User::MakeInviteCode();
				$oUser->invite_code = $sRegisterCode;
				//当前注册者关联的所有的上级ids（gk）
				if (isset($aInput['link_user_id'])){
				    $oSuperior = User::where('id',$aInput['link_user_id'])->first();
				    if ($oSuperior->superior_ids){
				        $aSuperiorIds = unserialize($oSuperior->superior_ids);
				        array_push($aSuperiorIds, $aInput['link_user_id']);
				        $oUser->superior_ids = serialize($aSuperiorIds);
				    }else{
				        $oUser->superior_ids = serialize(array($aInput['link_user_id']));
				    }
				    //获取当前用户关联代表的id
				    $iRepId = User::getRepId($oUser->superior_ids);
				    $oUser->link_rep_id = $iRepId;
				}
				$oUser->created_at = date('Y-m-d H:i:s',time());
				$oUser->is_h5 = 2;
				$oUser->save();
				$iDoctorId = $oUser->id;
				if($oUser->role_id==3){
					//为用户注册医脉通
					User::RegMedlive($oUser);
					$oUserDisclaimerLog = new UserDisclaimerLog();
					$oUserDisclaimerLog -> user_id= $iDoctorId;
					$oUserDisclaimerLog -> save();
				}
				// Auth::loginUsingId($oUser->id);
				Session::put('cdma_user_id',$oUser->id);
				$iNowTimestamp = time();
				//签到成功功能******************start***************************
				$sSignName = $oUser ->user_tel;
				$iUserId   = $oUser ->id;
				$this->OfflineSignDo($sSignName,$iUserId,$iMeetingCode);
				//签到成功功能****************** end ***************************
				return View::make('errors.mregistersuccessnewuser')
					->with('iMeetingCode',$iMeetingCode)
					->with('code',$sRegisterCode);
			}else{
				return 'error';
			}
		}
	}

	public function OfflineSignDo($sSignName,$iUserId,$iMeetingCode){
		$sSignName = intval($sSignName);
			$aSign = array(
				'user_id'	 => $iUserId,
				'created_at' => date('Y-m-d H:i:s',time()),
				'user_type'=>2,
				'meeting_code'=>$iMeetingCode
			);
			$oSign = new OfflineSignInLog($aSign);
			$oSign->save();
	}
	
	//忘记密码
	public function Forget(){
		if(Session::has('userid')){
			return Redirect::to('/');
		}
		return View::make('front.user.forget');
	}
	
	//忘记密码提交
	public function postForget(){ 
		$sEmail = trim(Input::get('email'));
		//查看当前邮箱是否存在
		$oUser = User::where('user_email',$sEmail)->first();
		if(!$oUser){
			//邮箱不存在
			return 'not_exist';
		}
		//激活链接
		$code = md5(SORT.$sEmail);
		$sUrl = Config::get('app.url').'/user/resetpwd?user='.urlencode($sEmail).'&code='.$code;
		$data = array('url'=>$sUrl);
		Mail::send('emails.auth.reminder', $data, function($message)
		{
			$message->to(Input::get('email'), Input::get('email'))->subject('国卫健康云忘记密码');
		});
		return 'success';
	}
	
	//执行修改密码
	public function ResetPwd(){
		if(Session::has('userid')){
			return Redirect::to('/');
		}
		$sEmail = trim(Input::get('user'));
		$sCode = Input::has('code')?Input::get('code'):'';
		//查看当前邮箱是否存在
		$oUser = User::where('user_email',$sEmail)->first();
		if(!$oUser){
			//邮箱不存在
			return Redirect::to('/');
		}
		//激活链接
		if($sCode != md5(SORT.$sEmail)){
			return Redirect::to('/');
		}
		return View::make('front.user.resetpwd');
	}
	
	//修改密码--新密码提交
	public function ResetPwdDo(){
		$sEmail = trim(Input::get('email'));
		$sCode = Input::has('code')?Input::get('code'):'';
		$sPwd = Input::has('new_pwd')?Input::get('new_pwd'):'';
		if(!$sPwd){
			return 'error';
		}
		//查看当前邮箱是否存在
		$oUser = User::where('user_email',$sEmail)->first();
		if(!$oUser){
			//邮箱不存在
			return 'error';
		}
		//激活链接
		if($sCode != md5(SORT.$sEmail)){
			return 'error';
		}
		$oUser->password = Hash::make($sPwd);
		$oUser->save();
		return 'success';
	}

	//积分排行测试
	public function TestScoreList($iRoleId){
		$aUser = User::getUserScoreList($iRoleId)->toArray();
		return 'success';
	}
	
	// 测试不用登陆加积分操作
	public function TestAddScore(){
		$rules = array(
			'user_nick'    	 => 'required',
			// 'password'  => 'required'
		);
		$input = array(
			'user_nick'		=> Input::get('user_nick'),
			'password'	=> '',
		);
		$oUserOrganizer = User::select('id')->where('invite_code',strtoupper(Input::get('organizer_code')))->first();
		if(!$oUserOrganizer){
			return 'code_err';
		}
		$validator = Validator::make(Input::all(), $rules);
		if($validator->passes()) {
			$oUser = User::select('id','role_id')->where('user_nick',Input::get('user_nick'))->first();
			if($oUser){
				if($oUser->role_id!=3){
					return 'rep';
				}
				Session::put('cdma_user_id',$oUser->id);
				$iVideoId = 25;
				//判断当前时间是否在直播20分钟这个时间段之前
				$oVideo = FaceVideo::select('end_time','video_url')->where('id',$iVideoId)->first();
				$iNowTimestamp = time();
				$iVideoEndTime = strtotime($oVideo->end_time);
				//加直播积分和加录播积分中间时间戳
				$iMiddleTimestamp = $iVideoEndTime+20*60;
				if($iNowTimestamp<$iMiddleTimestamp){
					//加直播的积分
					$iVideoType = '2';
					//直播查看用户是否签到
					$sCacheName = 'sign_in_cache_'.$iVideoId;
					if(Cache::has($sCacheName)){
						$sSignInCache = Cache::get($sCacheName);
						$aSignInCahce = mb_unserialize($sSignInCache);
						if(in_array($oUser->id,$aSignInCahce)){
							// return 'repeat';
							array_push($aSignInCahce,$oUser->id);
							//写文件
							$dir = app_path().'/storage/logs/redislog';
							$sLog =  $oUser->id.','.time().';';
							recordLog($iVideoId, $sLog,$dir);
						}else{
							array_push($aSignInCahce,$oUser->id);
							//写文件
							$dir = app_path().'/storage/logs/redislog';
							$sLog =  $oUser->id.','.time().';';
							recordLog($iVideoId, $sLog,$dir);
						}
					}else{
						$aSignInCahce = array();
						array_push($aSignInCahce,$oUser->id);
						//写文件
						$dir = app_path().'/storage/logs/redislog';
						$sLog =  $oUser->id.','.time().';';
						recordLog($iVideoId, $sLog,$dir);
					}
					$sSignInCache = serialize($aSignInCahce);
					//重新存入缓存
					Cache::forever($sCacheName, $sSignInCache);
					return 'success';
				}else{
					//直播签到是否结束
					if(!strstr($oVideo->video_url, 'upload')){
						return 'live_sign_stop';
						die;
					}
					//加录播的积分
					$iVideoType = '1';
					//查看用户是否签到
					$oSign = SignInLog::where('cat_id',1)
							->where('user_id',$oUser->id)
							->where('video_id',$iVideoId)
							->where('video_type',$iVideoType)
							->first();
					if($oSign){
						return 'repeat';
					}
					//录播签到加积分操作
					$this->SignInAddScore($oUser->id,$iVideoId,$oUserOrganizer->id,$iNowTimestamp );
					return 'success';
				}
				
				//签到成功功能******************start***************************
				/*if($this->SignInAddScore($oUser->id,$iVideoId,$oUserOrganizer->id,$iNowTimestamp )=='live_sign_stop'){
					return 'live_sign_stop';
				}*/
				//签到成功功能****************** end ***************************
			}else{
				return 'error';
			}
		}
		//登录失败
		return 'error';
	}
	
	//签到加积分操作（若时间为直播结束后20分钟之前的加直播积分，否则加录播积分）
	public function testSignInAddScore($iUid,$iVideoId,$iUOrgaId,$iNowTimestamp){
		//判断当前时间是否在直播20分钟这个时间段之前
		$oVideo = FaceVideo::find($iVideoId);
		
		$iVideoEndTime = strtotime($oVideo->end_time);
		//加直播积分和加录播积分中间时间戳
		$iMiddleTimestamp = $iVideoEndTime+20*60;
		if($iNowTimestamp>=$iMiddleTimestamp&&!strstr($oVideo->video_url, 'upload')){
			return 'live_sign_stop';
			die;
		}
		//签到记录
		$aSign = array(
			'user_id'		=> $iUid,
			'video_id'		=> $iVideoId,
			'organizer_id'	=> $iUOrgaId
		);
		$oSign = new SignInLog($aSign);
		$oSign->save();
		if($iNowTimestamp<$iMiddleTimestamp){
			//加直播的积分
			$iVideoType = '2';
			$iAddMin = 0;
		}else{
			//加录播的积分
			$iVideoType = '1';
			$iAddMin = 0;
		}
		$oVideoLog = VideoLog::HasLogSign(1,$iVideoId,$iVideoType,$iUid,0);
		if(1){
			//不存在log,加积分
			// FaceVideo::addUserScore($iVideoId,$iAddMin,$iUid,$iVideoType);
			//加积分记录
			$creditIN = TestCreditController::getInstance($iVideoId,$iAddMin,$iUid);
			$creditIN->optSignInCredit($iVideoType);
			$oSign->score = $iAddMin;
			$oSign->save();
		}
		if($iNowTimestamp>=$iMiddleTimestamp){
			//加录播的积分,更新签到标记位
			$oSign->video_type = 1;
			$oSign->save();
			//记录用户看录播一次
			VideoReviewLog::addUserReviewLog($iVideoId,$iUid,$oSign->score,1);
		}
	}
	
	//下载视频统计通用
	public function DownloadVideo($iCatId,$iVideoId){
		$iUserId = Session::get('userid');
		if($iCatId == 1){
			//首次下载视为完整观看录播
			$oDownLoad = VideoDownloadMinutes::where('user_id',$iUserId)->where('video_id',$iVideoId)->first();
			if(!$oDownLoad){
				$aDownload = array(
					'user_id'		=> $iUserId,
					'video_id'		=> $iVideoId,
					'video_minutes' => 40,
					'created_at'    => date('Y-m-d H:i:s',time())
				);
				$oDownload = new VideoDownloadMinutes($aDownload);
				$oDownload->save();
			}
		}
		$aVideoDownloadLog = array(
			'cat_id'		=> $iCatId,
			'video_id'		=> $iVideoId,
			'user_id'		=> $iUserId
		);
		$oVideoDownloadLog = new VideoDownloadLog($aVideoDownloadLog);
		$oVideoDownloadLog->save();
		return 'success';
	}
	
	//定时任务，每天自动发送短信
	//http://cdma.local/meeting-auto-sms
	//http://cdcma.medlive.cn/meeting-auto-sms
	public function MeetingAutoSms(){
		$sDate = date('Y-m-d',time());
		$oMeetingPerformLog = MeetingPerformLog::where('date',$sDate)->first();
		$oMeetingPerformLogAdd = new MeetingPerformLog;
		$oMeetingPerformLogAdd -> date = $sDate;
		$oMeetingPerformLogAdd -> save(); 
		if(count($oMeetingPerformLog)){
			return 'Today you have carried out the operation ';
		}
		set_time_limit(0);
		//会议开始前10/8天提醒七天未确定取消会议
		$this->MeetingbeforeTenday(10);
		$this->MeetingbeforeTenday(8);
		//取消会议开始时间还有一周有预约并且创建方还未确认的会议
		$this->CancelMeetingNotConfirm();
		//会议开始前一周未预约成功短信提醒-----模板三短信
		$this->NotsuccessAppoint();
		//创建会议离开始时间还有一周时间还没有预约的时候取消会议
		$this->CancelMeeting();
		return 'success';
	}
	
	//会议开始前10/8天提醒七天未确定取消会议
	public function MeetingbeforeTenday($day=10)
	{
		//提醒会议开始前十天还没确认的会议创建方确认会议
	  	if($day==10){
			$sStarttenTime = time() + 10*24*60*60;
			$sDate = date("Y-m-d",$sStarttenTime);
			$sSmsStartTime = $sDate.' 00:00:00';
			$sSmsEndTime = $sDate.' 23:59:59';
			$oMeeting = Meeting::where('meeting_start_time','<',$sSmsEndTime)
				->where('meeting_start_time','>',$sSmsStartTime)
				->where('order_count','>',0)
				->where('meeting_type_id',0)
				->where('meeting_ten_day',0)
				->where('status','!=',1)->get();
			foreach($oMeeting as $value){
				$value->meeting_ten_day =1;
				$value->save();
				$iTelephone = $value ->creator_tel;
				$sMeetingStart = $value ->meeting_start_time;
				$sStart = substr($sMeetingStart,0,16);
				$sMeetingEnd = $value ->meeting_end_time;
				$sEndTime = substr($sMeetingEnd,11,5);
				$sValuetime =$sStart.'-'.$sEndTime;
				$sValuetitle =$value->meeting_title;
				$value1 =$sValuetime.$sValuetitle;
				$value2 =$value->order_count;
				$oMessageten = MeetingMessageLog::sendMessage($iTelephone,102291,$value1,$value2);
			}
		} 
		//提醒会议开始前八天还没确认的会议创建方确认会议
		if($day==8){
			$sStartsevenTime = time() + 8*24*60*60;
			$sDate = date("Y-m-d",$sStartsevenTime);
			$sSmsStartTime = $sDate.' 00:00:00';
			$sSmsEndTime = $sDate.' 23:59:59';
			$oMeeting = Meeting::where('meeting_start_time','<',$sSmsEndTime)->where('meeting_start_time','>',$sSmsStartTime)->where('order_count','>',0)->where('meeting_type_id',0)->where('status','!=',1)->get();   
			foreach($oMeeting as $value){
				$iTelephone = $value ->creator_tel;
				$sMeetingStart = $value ->meeting_start_time;
				$sStart = substr($sMeetingStart,0,16);
				$sMeetingEnd = $value ->meeting_end_time;
				$sEndTime = substr($sMeetingEnd,11,5);
				$sValuetime =$sStart.'-'.$sEndTime;
				$sValuetitle =$value->meeting_title;
				$value1 = $sValuetime.$sValuetitle;
				$value2 =$value->order_count;
				$oMessageeight = MeetingMessageLog::sendMessage($iTelephone,102291,$value1,$value2);
			}
		} 
		return 'success';
	}

	//会议开始前一周未预约成功短信提醒(预约失败)-----模板三短信
	public function NotsuccessAppoint()
	{
		$sSmsStartTime = date("Y-m-d H:i:s",(time() + 6*24*60*60));
		$sSmsEndTime = date("Y-m-d H:i:s",(time() + 7*24*60*60));
		$oMeeting = Meeting::where('meeting_start_time','<=',$sSmsEndTime)
			->where('meeting_start_time','>=',$sSmsStartTime)
			->where('meeting_type_id',1)
			->where('order_count','>',0)
			->where('status','!=',1)
			->get();
		$aMeetingInweek = array();
		foreach($oMeeting as $value){
			$aMeetingInweek[] = $value->id;
		}
		if(!$aMeetingInweek){
			return;
		}
		//获取待确认的预约
		$oMeetingOrder = MeetingOrder::whereIn('order_type_id',array(0,1))->whereIn('meeting_id',$aMeetingInweek)->get();
		foreach($oMeetingOrder as $value){
			$iMeetingId =  $value->meeting_id;
			$value -> order_type_id = 1;
			$value -> save();
			$oMeetingMessage = Meeting::find($iMeetingId);
			$sMeetingStart = $oMeetingMessage ->meeting_start_time;
			$sStart = substr($sMeetingStart,0,16);
			$sMeetingEnd = $oMeetingMessage ->meeting_end_time;
			$sEndTime = substr($sMeetingEnd,11,5);
			$sValuetime =$sStart.'-'.$sEndTime;
			$iTel = $oMeetingMessage ->creator_tel;
			$sValuetitle = $oMeetingMessage ->meeting_title;
			$value1 = $sValuetime.$sValuetitle;
			$oMessage = MeetingMessageLog::sendMessage($value->order_tel,102293,$value1,'');
		}
		return 'success';
	}

	//创建会议离开始时间还有一周时间还没有预约的时候取消会议
	public function CancelMeeting()
	{
		// $sSmsStartTime = date("Y-m-d H:i:s",(time() + 6*24*60*60));
		$sSmsEndTime = date("Y-m-d H:i:s",(time() + 7*24*60*60));
		$oMeeting = Meeting::where('meeting_start_time','<=',$sSmsEndTime)
			// ->where('meeting_start_time','>=',$sSmsStartTime)
			->where('order_count',0)
			->where('status','!=',1)
			->get();
		foreach($oMeeting as $value){
	        $iId = $value->id;
	        $oMeetingFall = $value;
	        $oMeetingFall -> status = 1;
	        $oMeetingFall ->save();
	        $telephonecall = $oMeetingFall ->creator_tel;
	        $sMeetingTitle = $oMeetingFall -> meeting_title;
	        $sMeetingStart = $oMeetingFall ->meeting_start_time;
			$sStart = substr($sMeetingStart,0,16);
			$sMeetingEnd = $oMeetingFall ->meeting_end_time;
			$sEndTime = substr($sMeetingEnd,11,5);
			$sValuetime =$sStart.'-'.$sEndTime;
	        $value1= $sValuetime.$sMeetingTitle;
	        $oMessage = MeetingMessageLog::sendMessage($telephonecall,102294,$value1,'');
		}
		return 'success';
	}

 	//取消会议开始时间还有一周并且创建方还未确认的会议
	public function CancelMeetingNotConfirm(){
		// $sSmsStartTime = date("Y-m-d H:i:s",(time() + 6*24*60*60));
		$sSmsEndTime = date("Y-m-d H:i:s",(time() + 7*24*60*60));
		$oMeetingWeek = Meeting::where('meeting_start_time','<=',$sSmsEndTime)
			// ->where('meeting_start_time','>=',$sSmsStartTime)
			->where('order_count','>',0)
			->where('meeting_type_id',0)
			->where('status','!=',1)
			->get();
		foreach($oMeetingWeek as $value){
			//发送短信
			$sMeetingStart = $value ->meeting_start_time;
			$sStart = substr($sMeetingStart,0,16);
			$sMeetingEnd = $value ->meeting_end_time;
			$sEndTime = substr($sMeetingEnd,11,5);
			$sValuetime =$sStart.'-'.$sEndTime;
			$sValuetitle = $value ->meeting_title;
			$value1 = $sValuetime.$sValuetitle;
			$phone = $value->creator_tel;
			$oMessageseven = MeetingMessageLog::sendMessage($phone,102294,$value1,'');
			$iId = $value->id;
			$oMeetingAppoint =MeetingOrder::where('meeting_id',$iId)->where('order_type_id',0)->get();
			if(count($oMeetingAppoint)){//会议有预约但是七天未确认
				//此处向会议创建者和预约人发短信提示
				foreach($oMeetingAppoint as $values){
					$telephone = $values->order_tel;
					$oMessageseven = MeetingMessageLog::sendMessage($telephone,102295,$value1,'');
				}
			}
			$value->status =1;
			$value->save();
		}
		return 'success';
	}
	
	/**
	 * 阿司匹林专项基金，加麦粒失败的重新请求一次接口
	 * 测试：http://2kghy.kydev.net/fail-gold-repeat
	 */
	public function getFailGoldRepeat(){
		$oGoldLog = AspirinUserGoldLog::where('gold_type',1)
			->where('return_msg','!=','success')
			->get();
		if(count($oGoldLog)){
			foreach($oGoldLog as $log){
				$iUserId = $log->user_id;
				$iSourceType = $log->source_type;
				$iSourceId = $log->source_id;
				$log->delete();
				GoldService::addGold($iUserId, $iSourceType, $iSourceId);
			}
		}
	}

}