<?php
class AdmAspirinUserController extends BaseController {
	
	CONST  PAGESIZE = 10;

	//列表
	public function getList()
	{
	    $user_name = Input::get('user_name', '');
	    $user_tel = Input::get('user_tel', '');
	    $user_company = Input::get('user_company', '');

		$oUser = User::select(['user.*','hospital.name as hospital_name'])
            ->leftJoin('hospital', 'user.user_company', '=', 'hospital.id')
            ->where('user.role_id',3)
            ->where('user.card_auth_flag','!=',0)
            ->where(function($query) use ($user_name, $user_tel, $user_company) {
                if (!empty($user_name)) {
                    $query->where('user_name', 'like', "%{$user_name}%");
                }
                if (!empty($user_tel)) {
                    $query->where('user_mobile', 'like', "%{$user_tel}%")->orWhere('user_tel', 'like', "%{$user_tel}%");
                }
                if (!empty($user_company)) {
                    $query->where(function($query1) use ($user_company) {
                        $query1->where('user.user_company_name', 'like', "%{$user_company}%")
                            ->orWhere('hospital.name', 'like', "%{$user_company}%");
                    });
                }
            })
			->orderby('user.card_auth_flag','asc')
			->orderby('user.created_at','desc')
			->paginate(self::PAGESIZE);

		foreach($oUser as $k=>$v){
			if(is_numeric($v->user_province)){
				$sProvince = Hospital::where('id',$v->user_province)->pluck('name');
				if($sProvince){
					$v->user_province = $sProvince;
				}
			}
			if(is_numeric($v->user_city)){
				$sCity = Hospital::where('id',$v->user_city)->pluck('name');
				if($sCity){
					$v->user_city = $sCity;
				}
			}
			if(is_numeric($v->user_county)){
				$sCounty = Hospital::where('id',$v->user_county)->pluck('name');
				if($sCounty){
					$v->user_county = $sCounty;
				}
			}
			if (empty($v->user_mobile)) {
			    $v->user_mobile = $v->user_tel;
            }
	    }

	    $params = ['user_name' => $user_name, 'user_tel' => $user_tel, 'user_company' => $user_company];

		Return View::make('admin.aspirin.user.list')
            ->with('params', $params)
            ->with('oUser',$oUser);
	}

	//认证审核页
	public function getDetail($iUserId)
	{
		$oUser = User::find($iUserId);
		if($oUser && $oUser->card_auth_flag != 0){
			$sProvince = '';
			$aProvince = Hospital::getChildren();
			foreach($aProvince as $k=>$v){
				if($k == $oUser->user_province){
					$sProvince = $v;
				}
			}
			$sCity = '';
			if($oUser->user_province!=0){
				$aCity = Hospital::getChildren($oUser->user_province);
				foreach($aCity as $k=>$v){
					if($k == $oUser->user_city){
						$sCity = $v;
					}
				}
			}
			$sCounty = '';
			if($oUser->user_city!=0){
				$aCounty = Hospital::getChildren($oUser->user_city);
				foreach($aCounty as $k=>$v){
					if($k == $oUser->user_county){
						$sCounty = $v;
					}
				}
			}
			$sHospital = '';
			$aHospital = Hospital::getChildren($oUser->user_county);
			if(is_numeric($oUser->user_company) && $oUser->user_company > 0){
				foreach($aHospital as $k=>$v){
					if($k == $oUser->user_company){
						$sHospital = $v;
					}
				}
			}else{
				$sHospital = $oUser->user_company_name;
			}
			//获取银行收款信息
			$UserBankAccount = new UserBankAccount();
			$sBankinfo = array();
			$sBankinfo = $UserBankAccount::where("user_id","=",$iUserId)->first();
			// $log = DB::getQueryLog();
			// print_r($log);die;
			Return View::make('admin.aspirin.user.detail')->with('oUser',$oUser)
				->with('sProvince',$sProvince)
				->with('sCity',$sCity)
				->with('sCounty',$sCounty)
				->with('aHospital',$aHospital)
				->with('sHospital',$sHospital)
				->with('sBankinfo',$sBankinfo);
		}else{
			return $this->showMessage('该记录不存在','/admaspirinuser/list');
		}
	}

	//提交审核
	public function postCheck($iUserId){
		$iFlag = intval(Input::get('card_auth_flag'));
		if($iFlag != 2 && $iFlag != 3){
			return $this->showMessage('请选择审核结果','/admaspirinuser/detail/'.$iUserId);
		}
		$oUser = User::find($iUserId);
		if($oUser){
			//规范用户医院信息
			$iCompany = intval(Input::get('change_hospital',0));
			if($iCompany != 0 && $iCompany != $oUser->user_company){
				$oUser->user_company = $iCompany;
			}
			$oUser->card_auth_flag = $iFlag;
			$oUser->save();
			return $this->showMessage('审核提交成功','/admaspirinuser/list');
		}else{
			return $this->showMessage('该记录不存在','/admaspirinuser/list');
		}
	}
	
}
