<?php
/**
 * 通用积分操作日志
 * @author kk
 */
class  TestCreditController {

	protected $_minute = 0;//观看时长
	
	protected $_catId = 1;	//栏目id
	
	protected $_userId = 0;	//用户id
	
	protected $_userRole = 3;	//用户角色
	
	protected $_videoId = 0;	//直播或录播id
	
	protected $_video = NULL;	//直播或录播对象
	
	protected $_videoType = '1';	//1:录播，2：直播
	
	protected $_limitReview = REVIEW_LIMIT;//录播积分限制 
	
	protected $_limitLive = LIVE_LIMIT;//直播积分限制 
	
	protected $_user = NULL;
	
	private static $_instance = NULL; 
	
	
	/**
	 * 初始化各参数
	 */
	private function __construct($iId=0,$iMinutes=0,$iUid=0){  
		$this->_minute = intval($iMinutes);
		$this->_userId = $iUid;
		$this->_videoId = $iId;
		$oVideo = FaceVideo::find($iId);
		$this->_video = $oVideo;
		$this->_videoType = $oVideo->video_type;
		$oUser = User::find($iUid);
		$this->_user = $oUser;
		$this->_userRole = $oUser->role_id;
		
	} 
	
	public static function getInstance($iId=0,$iMinutes=0,$iUid=0){
	
		if(!(self::$_instance instanceof self)){
			self::$_instance = new self($iId,$iMinutes,$iUid);
		}
		return self::$_instance;
	}
	


	/**
	 * 
	 * 观看直播或录播加积分操作
	 */
	public function optCredit(){
		if($this->_video->video_permission==1){
			//仅代表可见，不加积分
			return ;
		}
		if($this->_videoType==2){			
			$this->addLiveLogAndCredit();	//直播加积分操作
		}else{
			$this->addReviewLogAndCredit();		//录播加积分操作
		}
	}
	
	/**
	 * 签到加积分操作
	 * 
	 */
	public function optSignInCredit($iVideoType){
		$this->_videoType = $iVideoType;
		$this->optCredit();
	}
	/**
	 * 直播加积分操作
	 * Enter description here ...
	 */
	protected function addLiveLogAndCredit(){
		//查看是否有该用户看该直播的记录
		$oVideoLog = VideoLog::HasLog($this->_catId,$this->_videoId,$this->_videoType,$this->_userId,0);
		//如果没有用户查看直播记录则存log,加积分操作
		if(1){
			$this->_minute = $this->_minute>$this->_limitLive ? $this->_limitLive : $this->_minute;
			//用户加积分log
			$this->addVideoLog();
			//用户积分+
			$this->addUserScore();
			if($this->_userRole==3){
				//该医生所有的上级加积分操作
				$aSuperior = mb_unserialize($this->_user->superior_ids);
				$this->addUserSuperiorScore($aSuperior);
			}
		}else{
			//有该条记录
			$iWatchMinutes = $oVideoLog->watch_minutes;
			if($iWatchMinutes>=$this->_limitLive){
				//记录时长超过规定的最高积分
				return ;
			}
			//最多还能加多少分
			$iCanAddMin = $this->_limitLive - $iWatchMinutes;
			if($this->_minute > $iCanAddMin){
				$this->_minute = $iCanAddMin;
			}
			//更新加分log
			$oVideoLog->watch_minutes = $oVideoLog->watch_minutes+$this->_minute;
			$oVideoLog->save();
			//用户加积分log
			$this->addUserScore();
			if($this->_userRole==3){
				//该医生所有的上级加积分操作
                $aSuperior = mb_unserialize($this->_user->superior_ids);
                $this->addUserSuperiorScoreUpdate($aSuperior);
			}
		}
	}
	
	/**
	 * 录播加积分操作
	 * Enter description here ...
	 */
	protected function addReviewLogAndCredit(){
		//限制时长
		$this->_minute = $this->_minute>$this->_limitReview ? $this->_limitReview : $this->_minute;
		$oVideoLog = VideoLog::HasLog($this->_catId,$this->_videoId,$this->_videoType,$this->_userId,0);
		if(!$oVideoLog){
			//用户加积分log
			$this->addVideoLog();
			//用户积分+
			$this->addUserScore('user_review_score');
			if($this->_userRole==3){
				//该医生所有的上级加积分操作（gk）
				$aSuperior = mb_unserialize($this->_user->superior_ids);
				$this->addUserSuperiorScore($aSuperior,'user_review_score');
			}
		}else{
			//存在该条记录，更新
			$iOldMin = $oVideoLog->watch_minutes;
			if($this->_minute>$iOldMin){
				//当当前积分高于数据库记录积分时，执行代码
				$oVideoLog->watch_minutes = $this->_minute;
				$oVideoLog->save();
					
				//用户积分
				$iReduceUserScore = $iOldMin;
				$iAddUserScore = $this->_minute;
				$this->_minute = intval($iAddUserScore-$iReduceUserScore);
				$this->addUserScore('user_review_score');
				if($this->_userRole==3){
					//该医生所有的上级加积分操作（gk）
                    $aSuperior = mb_unserialize($this->_user->superior_ids);
                    $this->addUserSuperiorScoreUpdate($aSuperior,'user_review_score');
						
				}
					
			}
		}
	}
	
	/**
	 * 不存在log时加积分操作
	 * Enter description here ...
	 */
	protected  function addVideoLog(){
		//存log
		$aVideoLog = array(
							'cat_id'		=> $this->_catId,
							'user_id'		=> $this->_userId,
							'video_id'		=> $this->_videoId,
							'video_type'	=> $this->_videoType,
							'watch_minutes'	=> $this->_minute
		);
		$oVideoLog = new VideoLog($aVideoLog);
		$oVideoLog->save();
	}
	
	/**
	 * 用户加积分操作
	 * Enter description here ...
	 */
	protected function addUserScore($type='user_live_score'){
		$this->_user->user_score = $this->_user->user_score+$this->_minute;
		$this->_user->$type = $this->_user->$type+$this->_minute;
		$this->_user->save();
	}
	
	/**
	 * 用户上级加积分及log操作
	 */
	protected function addUserSuperiorScore($aSuperior,$type='user_live_score'){
		if (!empty($aSuperior)){
			$aVideoLog = array();
	       foreach ($aSuperior as $key=>$val){
	            $aVideoLog[] = array(
								'cat_id'		=> $this->_catId,
								'link_user_id'	=> $this->_userId,
								'user_id'		=> $val,
								'video_id'		=> $this->_videoId,
								'video_type'	=> $this->_videoType,
								'watch_minutes'	=> $this->_minute
						      );
	       }
	       //上级存积分log
	       VideoLog::insert($aVideoLog);
	       //上级+积分
	       $this->AddSuperiorScore($aSuperior,$type);
		}
	}
	
	/**
	 * 上级积分增加
	 * Enter description here ...
	 * @param unknown_type $aSuperior
	 * @param unknown_type $type
	 */
	protected function AddSuperiorScore($aSuperior,$type){
		$oUser = User::whereIn('id',$aSuperior);
		$oUser->increment('user_score',$this->_minute);
		$oUser->increment($type,$this->_minute);
	}
	
	/**
	 * 上级积分log更新
	 * 
	 */
	protected function UpdateSuperiorLog($aSuperior){
		$oVideoLog = VideoLog::where('cat_id',$this->_catId)
    							->where('video_id',$this->_videoId)
    							->where('video_type',$this->_videoType)
    							->where('link_user_id',$this->_userId);
    	$oVideoLog->increment('watch_minutes',$this->_minute);
	}
	/**
	 * 用户上级修改积分及log操作
	 */
	protected function addUserSuperiorScoreUpdate($aSuperior,$type='user_live_score'){
		if (!empty($aSuperior)){
             //上级更新积分
             $this->UpdateSuperiorLog($aSuperior);
             //上级+积分
	       	 $this->AddSuperiorScore($aSuperior,$type);
        }
	}
	
}