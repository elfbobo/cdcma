<?php

class AdmAspirinResearchController extends BaseController {
	
	CONST  PAGESIZE = 10;

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	public function getIndex()
	{
		Return View::make('admin.aspirin.research.index');
	}

	//科研申请列表
	public function getApplyList()
	{
		$sName = Input::get('name');
		if(!empty($sName)){
			$oApply = AspirinResearchApply::where('apply_name','like','%'.$sName.'%')
				->orderBy("created_at","desc")
				->paginate(self::PAGESIZE);
		}else{
			$oApply = AspirinResearchApply::orderBy("created_at","desc")->paginate(self::PAGESIZE);
		}
		Return View::make('admin.aspirin.research.applylist')->with('oApply',$oApply);
	} 

	//科研申请详情页
	public function getApplyShow($iApplyId)
	{
		$oApply = AspirinResearchApply::find($iApplyId);
		if($oApply){
			Return View::make('admin.aspirin.research.applyshow')->with('oApply',$oApply)->with('iApplyId',$iApplyId);
		}else{
			return $this->showMessage('该内容不存在','/admaspirinresearch/apply-list');
		}
	}

	//科研申请审核
	public function postApplyCheck()
	{
		$iApplyId = Input::get('iApplyId');
		$aInput = Input::all();
		$iType = intval($aInput['apply_type']);
		if($iType == AspirinResearchApply::CHECK_TYPE_FAIL){
			$sFailReason = trim($aInput['fail_reason']);
		}
		if($iType == AspirinResearchApply::CHECK_TYPE_FAIL && $sFailReason == ''){
			return $this->showMessage('请填写审核不通过原因','/admaspirinresearch/apply-show/'.$iApplyId);
		}
		
		$oApply = AspirinResearchApply::find($iApplyId);
		if($oApply){
			//原始状态
			$iOldType = $oApply->apply_type;
			$oApply->apply_type  = $iType;
			$oApply->fail_reason = isset($sFailReason)?$sFailReason:'';
			$oApply->save();
			//发送消息通知
			if($iType == AspirinResearchApply::CHECK_TYPE_SUCCESS){
				//发送审核通过推送通知
				$sPushContent = PushService::RESEARCH_CHECK_SUCCESS;
				$sPushContent = str_replace('{{name}}', $oApply->apply_name, $sPushContent);
				//加麦粒
				// $sRes = GoldService::addGold($oApply->user_id,31,$iApplyId);
				//若原始状态和当前的审核状态不相同，则发送推送通知
				if($iType != $iOldType){
					PushService::postClient('国卫健康云',$sPushContent,31,$oApply->user_id);
				}
			}elseif($iType == AspirinResearchApply::CHECK_TYPE_FAIL){
				//发送审核不通过推送通知
				$sPushContent = PushService::RESEARCH_CHECK_FAIL;
				$sPushContent = str_replace('{{name}}', $oApply->apply_name, $sPushContent);
				$sPushContent = str_replace('{{reason}}', $sFailReason, $sPushContent);
				//若原始状态和当前的审核状态不相同，则发送推送通知
				if($iType != $iOldType){
					PushService::postClient('国卫健康云',$sPushContent,31,$oApply->user_id);
				}
			}
			return $this->showMessage('操作成功','/admaspirinresearch/apply-list');
		}else{
			return $this->showMessage('该内容不存在','/admaspirinresearch/apply-list');
		}
	}
	//集赞照片列表
	// 	public function getZanList()
	// 	{
	// 		$sName = Input::get('name');
	// 		if(!empty($sName)){
	// 			$oZan = AspirinResearchZan::leftJoin('user','user.id','=','aspirin_research_zan.user_id')
	// 										->select('user.user_cwid','user.user_name','aspirin_research_zan.*')
	// 										->where('user_name','like','%'.$sName.'%')
	// 										->orderBy("created_at","desc")
	// 										->paginate(self::PAGESIZE);
	// 		}else{
	// 			$oZan = AspirinResearchZan::leftJoin('user','user.id','=','aspirin_research_zan.user_id')
	// 										->select('user.user_cwid','user.user_name','aspirin_research_zan.*')
	// 										->orderBy("id","desc")
	// 										->paginate(self::PAGESIZE);
	// 		}
			
	// 		Return View::make('admin.aspirin.research.zanlist')->with('oZan',$oZan);
	// 	}
	// 	//集赞照片详情页
	// 	public function getZanShow($iId)
	// 	{
	// 		$oZan = AspirinResearchZan::find($iId);
	// 		if($oZan){
	// 			Return View::make('admin.aspirin.research.zanshow')->with('oZan',$oZan)->with('iId',$iId);
	// 		}else{
	// 			return $this->showMessage('该内容不存在','/admaspirinresearch/zan-list');
	// 		}
	// 	}
	// 	//集赞照片审核
	// 	public function postZanCheck()
	// 	{
	// 		$iId = Input::get('iId');
	// 		$aInput = Input::all();
	// 		$iType = intval($aInput['zan_type']);
	// 		if($iType == AspirinResearchZan::CHECK_TYPE_FAIL){
	// 			$sFailReason = trim($aInput['fail_reason']);
	// 		}
	// 		if($iType == AspirinResearchZan::CHECK_TYPE_FAIL && $sFailReason == ''){
	// 			return $this->showMessage('请填写审核不通过原因','/admaspirinresearch/zan-show/'.$iId);
	// 		}
			
	// 		$oZan = AspirinResearchZan::find($iId);
	// 		if($oZan){
	// 			//原始状态
	// 			$iOldType = $oZan->zan_type;
				
	// 			$oZan->zan_type  = $iType;
	// 			$oZan->fail_reason = isset($sFailReason)?$sFailReason:'';
	// 			$oZan->save();
				
	// 			//发送消息通知
	// 			if($iType == AspirinResearchZan::CHECK_TYPE_SUCCESS){
	// 				//发送审核通过推送通知
	// 				$sPushContent = PushService::RESEARCH_ZANPHOTO_CHECK_SUCCESS;
	// 				//若原始状态和当前的审核状态不相同，则发送推送通知
	// 				if($iType != $iOldType){
	// 					PushService::postClient('国卫健康云',$sPushContent,32,$oZan->user_id);
	// 				}
	// 			}elseif($iType == AspirinResearchZan::CHECK_TYPE_FAIL){
	// 				//发送审核不通过推送通知
	// 				$sPushContent = PushService::RESEARCH_ZANPHOTO_CHECK_FAIL;
	// 				$sPushContent = str_replace('{{reason}}', $sFailReason, $sPushContent);
	// 				//若原始状态和当前的审核状态不相同，则发送推送通知
	// 				if($iType != $iOldType){
	// 					PushService::postClient('国卫健康云',$sPushContent,33,$oZan->user_id);
	// 				}
	// 			}
				
	// 			return $this->showMessage('操作成功','/admaspirinresearch/zan-list');
	// 		}else{
	// 			return $this->showMessage('该内容不存在','/admaspirinresearch/zan-list');
	// 		}
	// 	}
		
	// 	//培训课程列表
	// 	public function getTrainList()
	// 	{
	// 		$sTitle = Input::get('title');
	// 		if(!empty($sTitle)){
	// 			$oTrain = AspirinResearchTrain::where('train_title','like','%'.$sTitle.'%')
	// 															->orderBy("created_at","desc")
	// 															->paginate(self::PAGESIZE);
	// 		}else{
	// 			$oTrain = AspirinResearchTrain::orderBy("created_at","desc")->paginate(self::PAGESIZE);
	// 		}
	// 		Return View::make('admin.aspirin.research.trainlist')->with('oTrain',$oTrain);;
	// 	}
	// 	//培训课程添加
	// 	public function getTrainAdd()
	// 	{
	// 		Return View::make('admin.aspirin.research.trainadd');
	// 	}
	// 	public function postTrainAddDo()
	// 	{
	// 		$aInput = Input::all();
	// 		$rule = array(
	// 				'train_title'	 => 'required',
	// 				'train_thumb'	 => 'required',
	// 				'train_content'	 => 'required',
	// // 				'video_thumb'	 => 'required',
	// // 				'video_url'	     => 'required'
	// 		);
	// 		$validator = Validator::make($aInput,$rule);
	// 		if ($validator->fails()){
	// 			return $this->showMessage('请填写必填字段');
	// 		}
	// 		$aTrain = array(
	// 				'train_title'       => trim($aInput['train_title']),
	// 				'train_thumb'       => trim($aInput['train_thumb']),
	// 				'train_content'     => trim($aInput['train_content']),
	// 				'video_thumb'       => trim($aInput['video_thumb']),
	// 				'video_url'         => trim($aInput['video_url']),
	// 				'created_at'        => date('Y-m-d H:i:s',time())
	// 		);
	// 		$oTrain = new AspirinResearchTrain($aTrain);
	// 		$oTrain->save();
	// 		return $this->showMessage('添加成功','/admaspirinresearch/train-list');
	// 	}
	// 	//培训课程修改
	// 	public function getTrainEdit($iId)
	// 	{
	// 		$oTrain = AspirinResearchTrain::find($iId);
	// 		if($oTrain){
	// 			Return View::make('admin.aspirin.research.trainedit')->with('oTrain',$oTrain)->with('iId',$iId);
	// 		}else{
	// 			return $this->showMessage('该内容不存在','/admaspirinresearch/train-list');
	// 		}
	// 	}
	// 	public function postTrainEditDo()
	// 	{
	// 		$iId = Input::get('iId');
	// 		$aInput = Input::all();
	// 		$rule = array(
	// 				'train_title'	 => 'required',
	// 				'train_thumb'	 => 'required',
	// 				'train_content'	 => 'required',
	// // 				'video_thumb'	 => 'required',
	// // 				'video_url'	     => 'required'
	// 		);
	// 		$validator = Validator::make($aInput,$rule);
	// 		if ($validator->fails()){
	// 			return $this->showMessage('请填写必填字段');
	// 		}
	// 		$oTrain = AspirinResearchTrain::find($iId);
	// 		if($oTrain){
	// 			$oTrain->train_title    = trim($aInput['train_title']);
	// 			$oTrain->train_thumb    = trim($aInput['train_thumb']);
	// 			$oTrain->train_content  = trim($aInput['train_content']);
	// 			$oTrain->video_thumb    = trim($aInput['video_thumb']);
	// 			$oTrain->video_url      = trim($aInput['video_url']);
	// 			$oTrain->save();
	// 			return $this->showMessage('修改成功','/admaspirinresearch/train-list');
	// 		}else{
	// 			return $this->showMessage('修改失败','/admaspirinresearch/train-list');
	// 		}
	// 	}
	// 	//培训课程删除
	// 	public function getTrainDelete($iId)
	// 	{
	// 		$oTrain = AspirinResearchTrain::find($iId);
	// 		if($oTrain){
	// 			$oTrain->delete();
	// 			return $this->showMessage('删除成功','/admaspirinresearch/train-list');
	// 		}else{
	// 			return $this->showMessage('删除失败','/admaspirinresearch/train-list');
	// 		}
	// 	}
	// 	//上传培训课程缩略图
	// 	public function postTrainThumb(){
	// 		if($_FILES['upload_file']['error']>0){
	// 			$error = $_FILES['thumb']['error'];
	// 		}else{
	// 			$attach_filename = $_FILES['upload_file']['name'];
	// 			$attach_fileext = get_filetype($attach_filename);
	// 			$rand_name = date('His', time()).rand(1000,9999);
		
	// 			$sFileName = $rand_name.'.'.$attach_fileext;
		
	// 			$sPath = "/upload/train/$attach_fileext/".date('Ymd',time());
		
	// 			$sRealPath = public_path().$sPath;
	// 			mkdirs($sRealPath);
	// 			//		    $attach_filesize = filesize($_FILES['upload_file']['tmp_name']);
	// 			move_uploaded_file($_FILES['upload_file']['tmp_name'], $sRealPath.DS.$sFileName);
		
	// 			$sFileNameS = $rand_name . '_s.' . $attach_fileext;
	// 			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 1000, 1000 );
				
	// 			$sFileUrl = $sPath.'/'.$sFileNameS;
		
	// 			$json = array('train_thumb'=>$sFileUrl);
	// 			echo json_encode($json);
	// 			die;
	// 		}
	// 	}
	// 	//上传视频缩略图
	// 	public function postVideoThumb(){
	// 		if($_FILES['upload_file0']['error']>0){
	// 			$error = $_FILES['thumb']['error'];
	// 		}else{
	// 			$attach_filename = $_FILES['upload_file0']['name'];
	// 			$attach_fileext = get_filetype($attach_filename);
	// 			$rand_name = date('His', time()).rand(1000,9999);
		
	// 			$sFileName = $rand_name.'.'.$attach_fileext;
		
	// 			$sPath = "/upload/train/video/$attach_fileext/".date('Ymd',time());
		
	// 			$sRealPath = public_path().$sPath;
	// 			mkdirs($sRealPath);
	// 			//		    $attach_filesize = filesize($_FILES['upload_file']['tmp_name']);
	// 			move_uploaded_file($_FILES['upload_file0']['tmp_name'], $sRealPath.DS.$sFileName);
		
	// 			$sFileNameS = $rand_name . '_s.' . $attach_fileext;
	// 			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 1000, 1000 );
					
	// 			$sFileUrl = $sPath.'/'.$sFileNameS;
		
	// 			$json = array('video_thumb'=>$sFileUrl);
	// 			echo json_encode($json);
	// 			die;
	// 		}
	// 	}


	//科研申请记录导出
	public function getExportResearch(){
		set_time_limit(0);
		header('Cache-control:public');
		header('Pragma:public');
		header('Content-type:application/vnd.ms-excel');
		header('Content-Disposition:attachment;filename=research_apply_log_'.date('His').'.csv');
		header('Content-Type:APPLICATION/OCTET-STREAM');
		ob_start();
		$header_str = iconv("utf-8",'gbk',"登录用户id,登录用户名,姓名,城市,出生日期,职称,联系电话,电子邮箱,单位名称,地址,邮编,教育背景,工作经历,科研经历,问题1结论,问题2结论,问题3结论,问题4结论,审核状态,审核失败原因,创建时间\n");
 		$file_str="";
		$oApplyLog = AspirinResearchApply::join('user','aspirin_research_apply.user_id','=','user.id')
			->select('aspirin_research_apply.*','user.user_name')
			->orderBy('aspirin_research_apply.created_at','desc')
			->get();
 		if($oApplyLog){
 			foreach($oApplyLog as $v){
 				$iApplyType = $v->apply_type;
 				switch ($iApplyType){
 					case 0:
 						$sApplyType = "未审核";
 						break;
 					case 1:
 						$sApplyType ="审核通过";
 						break;
 					case 2:
 						$sApplyType ="审核未通过";
 						break;
 				}
 				$file_str .= $v->user_id.',';
 				$file_str .= $v->user_name.',';
 				$file_str .= $v->apply_name.',';
 				$file_str .= $v->apply_city.',';
 				$file_str .= $v->apply_birthday.',';
 				$file_str .= $v->apply_title.',';
 				$file_str .= $v->apply_tel.',';
 				$file_str .= $v->apply_email.',';
 				$file_str .= $v->apply_company.',';
 				$file_str .= $v->apply_address.',';
 				$file_str .= $v->apply_postcode."\t".',';
 				$file_str .= $v->education_background.',';
 				$file_str .= $v->work_experience.',';
 				$file_str .= $v->research_experience.',';
 				$file_str .= $v->question1.',';
 				$file_str .= $v->question2.',';
 				$file_str .= $v->question3.',';
 				$file_str .= $v->question4.',';
 				$file_str .= $sApplyType.',';
 				$file_str .= $v->fail_reason.',';
 				$file_str .= $v->created_at."\n";
 			}
 		}
		$file_str= iconv("utf-8",'GBK//TRANSLIT',$file_str);
		ob_end_clean();
		echo $header_str;
		echo $file_str;
	}
	
}
