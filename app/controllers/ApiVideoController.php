<?php

class ApiVideoController extends BaseController
{

	const NETURL = 'http://cdcma.bizconf.cn';

	public function getList(){//获取初始的直播、预告、回顾
		if(!Input::has('token')){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$token = Input::get('token');
		$token = Crypt::decrypt($token);
		$aUserinfo = explode('\t',$token);
		$aUserinfo = explode('\t',$token);
		$iUid = $aUserinfo[0];
		if($iUid){
			$oUser = User::find($iUid);
		}
		//取名医面对面精彩预告最新的一条
		$oFaceVideoAdvances = FaceVideo::getLatestFaceVideoAdvanceAPI('',$oUser->role_id);
		// print_r($oBannerRs);		
		// $log = DB::getQueryLog();
		// print_r($oBannerRs);die;
		// print_r($oFaceVideoAdvances);die;
		$oFaceVideoAdvance = isset($oFaceVideoAdvances[0])?$oFaceVideoAdvances[0]:'';
		if($oFaceVideoAdvance){
			$oFaceVideoAdvance1 = FaceVideo::getLatestFaceVideoAdvanceAPI($oFaceVideoAdvance->id,$oUser->role_id);
			if(!$oFaceVideoAdvance1){
				$oFaceVideoAdvance1 = $oFaceVideoAdvance;
			}
			/*if(!count($oFaceVideoAdvance1)){
				$oFaceVideoAdvance1 = $oFaceVideoAdvances;
			}*/
		}else{
			$oFaceVideoAdvance1=array();
		}
		$aFocus = array();
		$banner = array(); //精品课程banner
		$aLiveshows = array();
		//直播和预告
		if($oFaceVideoAdvance){
			$sThumb = '/assets/images/weixin/img01.jpg';
			$aDepart = explode('|',$oFaceVideoAdvance->department_id);
			if(in_array(5, $aDepart)){
				$sThumb = '/assets/images/weixin/img001.jpg';     // 社区专用图
			}
			$aFocus = array(
				'id'=>$oFaceVideoAdvance->id,
				'video_title'=>$oFaceVideoAdvance->video_title,
				'video_url'=>'http://cdcma.bizconf.cn/mobile-docface/enter-live/'.$oFaceVideoAdvance->id,
				'doc_name'=>$oFaceVideoAdvance->doc_name,
				'doc_hospital'=>$oFaceVideoAdvance->doc_hospital,
				'start_time' => strtotime($oFaceVideoAdvance->start_time),
				'end_time'=> strtotime($oFaceVideoAdvance->end_time),
				'thumb' => self::NETURL.$sThumb,
			);
		}
		// foreach($oFaceVideoAdvance1 as $obj){
		// 	$aLiveshows[] = array(
		// 		'video_title'=>$obj['video_title'],
		// 		'doc_name'=>$obj['doc_name'],
		// 		'doc_hospital'=>$obj['doc_hospital'],
		// 		'start_time' => $obj['start_time'],
		// 		'end_time'=> $obj['end_time'],
		// 	);
		// }
		$aLiveshows[] = array(
			'id'=>$oFaceVideoAdvance1->id,
			'video_title'=>$oFaceVideoAdvance1['video_title'],
			'doc_name'=>$oFaceVideoAdvance1['doc_name'],
			'doc_hospital'=>$oFaceVideoAdvance1['doc_hospital'],
			'start_time' => $oFaceVideoAdvance1['start_time'],
			'end_time'=> $oFaceVideoAdvance1['end_time'],
			'doc_url'=> "http://cdcma.bizconf.cn/mobile-docface/doc-show/{$oFaceVideoAdvance1['doc_id']}?token=".Input::get('token'),
		);
		$oBannerRs = FaceVideo::where('video_type',1)->where('video_rec','=','1')->get();
		if(@$oBannerRs[0]){
			$oBannerRow = $oBannerRs[0];
			if($oBannerRow->doc_id){
				$oDocInfo = FaceDoc::find($oBannerRow->doc_id);
			}
			// print_r($oBannerRow);die;
			$banner[] = array(
				'id'=>$oBannerRow->id,
				'video_title'=>$oBannerRow->video_title,
				'video_url'=>'http://cdcma.bizconf.cn/mobile-docface/enter-live/'.$oBannerRow->id,
				'doc_name'=>$oDocInfo->doc_name,
				'doc_hospital'=>$oDocInfo->doc_hospital,
				'start_time' => strtotime($oBannerRow->start_time),
				'end_time'=> strtotime($oBannerRow->end_time),
				'thumb' => self::NETURL.$oBannerRow->video_banner
			);
		}
		if($oUser&&$oUser->role_id!=2){
			$oRecordshows = FaceVideo::select('face_video.*','face_doc.doc_name','face_doc.doc_thumb','face_doc.doc_position','face_doc.doc_department','face_doc.doc_hospital','face_doc.doc_introduction')
				->leftJoin('face_doc','face_video.doc_id','=','face_doc.id')
				->where('video_permission','=','0')
				->where('video_type','=','1')
				->where('video_rec','=','0')
				->orderBy('start_time','DESC')
				->take(10)
				->get();
		}else{
			$oRecordshows = FaceVideo::select('face_video.*','face_doc.doc_name','face_doc.doc_thumb','face_doc.doc_position','face_doc.doc_department','face_doc.doc_hospital','face_doc.doc_introduction')
				->leftJoin('face_doc','face_video.doc_id','=','face_doc.id')
				->where('video_type','=','1')
				->where('video_rec','=','0')
				->orderBy('start_time','DESC')
				->take(10)
				->get();
		}
		$aRecordshows = array();
		foreach($oRecordshows as $obj){
			// $oDoc = FaceDoc::find($obj->doc_id);
			$aRecordshows[] = array(
				'id'=>$obj->id,
				'video_title'=>$obj->video_title,
				'video_thumb'=>self::NETURL.$obj->video_thumb,
				'doc_name'=>$obj->doc_name,
				'doc_hospital'=>$obj->doc_hospital,
				'created_at' => $obj->created_at
			);
		} 
		//获取推荐精品课堂
		$recordfqlistRs = FaceVideo::select('face_video.*','face_doc.doc_name','face_doc.doc_thumb','face_doc.doc_position','face_doc.doc_department','face_doc.doc_hospital','face_doc.doc_introduction')
			->leftJoin('face_doc','face_video.doc_id','=','face_doc.id')
			// ->where('video_type','=','1')
			->where('video_rec','=','1')
			->orderBy('start_time','DESC')
			->take(5)
			->get();
		$recordfqlist = array();
		foreach($recordfqlistRs as $obj){
			// $oDoc = FaceDoc::find($obj->doc_id);
			$recordfqlist[] = array(
				'id'=>$obj->id,
				'video_title'=>$obj->video_title,
				'video_thumb'=>self::NETURL.$obj->video_thumb,
				'doc_name'=>$obj->doc_name,
				'doc_hospital'=>$obj->doc_hospital,
				'created_at' => $obj->created_at
			);
		}
		$data = array(
			'afocus'=>$aFocus,
			'banner'=>$banner,
			'aliveshows'=>$aLiveshows,
			'aRecordshows'=>$aRecordshows,
			'recordfqlist'=>$recordfqlist
		);
		echo $this->formatRet(true,"成功","success",$data);exit;
	}

	public function getRecordlist(){//获取分页的精彩回顾
		if(!Input::has('token')){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$token = Input::get('token');
		$token = Crypt::decrypt($token);
		$aUserinfo = explode('\t',$token);
		$aUserinfo = explode('\t',$token);
		$iUid = $aUserinfo[0];
		if($iUid){
			$oUser = User::find($iUid);
		}
		$page = Input::get('page');
		$skip = ($page-1)*10;
		if($oUser&&$oUser->role_id!=2){
			$oRecordshows = FaceVideo::select('face_video.*','face_doc.doc_name','face_doc.doc_thumb','face_doc.doc_position','face_doc.doc_department','face_doc.doc_hospital','face_doc.doc_introduction')
				->leftJoin('face_doc','face_video.doc_id','=','face_doc.id')
				->where('video_type','=','1')->where('video_permission','=','0')->where('video_rec','=','0');
		}else{
			$oRecordshows = FaceVideo::select('face_video.*','face_doc.doc_name','face_doc.doc_thumb','face_doc.doc_position','face_doc.doc_department','face_doc.doc_hospital','face_doc.doc_introduction')
				->leftJoin('face_doc','face_video.doc_id','=','face_doc.id')
				->where('video_type','=','1')->where('video_rec','=','0');
		}
		
		//科室检索
		$iDepartmentId = Input::get('departmentid',0);
		if($iDepartmentId){
			$oRecordshows = $oRecordshows->where('department_id','LIKE',"%".$iDepartmentId."|%");
		}
		$oRecordshows = $oRecordshows->orderBy('start_time','DESC')->skip($skip)->take(10)->get();
		$aRecordshows = array();
		foreach($oRecordshows as $obj){
			// $oDoc = FaceDoc::find($obj->doc_id);
			$aRecordshows[] = array(
				'id'=>$obj->id,
				'video_title'=>$obj->video_title,
				'video_thumb'=>self::NETURL.$obj->video_thumb,
				'doc_name'=>$obj->doc_name,
				'doc_hospital'=>$obj->doc_hospital,
				'created_at' => $obj->created_at
			);
		} 
		echo  $this->formatRet(true,"成功","success",$aRecordshows);exit;
	}

	//获取精品课堂
	public function getRecordfqlist(){
		if(!Input::has('token')){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$token = Input::get('token');
		$token = Crypt::decrypt($token);
		$aUserinfo = explode('\t',$token);
		$iUid = $aUserinfo[0];
		if($iUid){
			$oUser = User::find($iUid);
		}
		$page = Input::get('page', 1);
		$pagesize = Input::get('pagesize', 10);
		$pagesize = $pagesize?$pagesize:10;
		$skip = ($page-1)*$pagesize;
		//->where('video_type','=','1')
		if($oUser&&$oUser->role_id!=2){
			$oRecordshows = FaceVideo::select('face_video.*','face_doc.doc_name','face_doc.doc_thumb','face_doc.doc_position','face_doc.doc_department','face_doc.doc_hospital','face_doc.doc_introduction')
				->leftJoin('face_doc','face_video.doc_id','=','face_doc.id')
				->where('video_permission','=','0')->where('video_rec','=','1');
		}else{
			$oRecordshows = FaceVideo::select('face_video.*','face_doc.doc_name','face_doc.doc_thumb','face_doc.doc_position','face_doc.doc_department','face_doc.doc_hospital','face_doc.doc_introduction')
				->leftJoin('face_doc','face_video.doc_id','=','face_doc.id')
				->where('video_rec','=','1');
		}

		//科室检索
		$iDepartmentId = Input::get('departmentid',0);
		if($iDepartmentId){
			$oRecordshows = $oRecordshows->where('department_id','LIKE',"%".$iDepartmentId."|%");
		}
		$oRecordshows = $oRecordshows->orderBy('start_time','DESC')->skip($skip)->take($pagesize)->get();
		// $log = DB::getQueryLog();
		// print_r($log);die;
		// print_r($oRecordshows);die;
		$aRecordshows = array();
		foreach($oRecordshows as $obj){
			// $oDoc = FaceDoc::find($obj->doc_id);
			$aRecordshows[] = array(
				'id'=>$obj->id,
				'video_title'=>$obj->video_title,
				'video_type'=>$obj->video_type,
				'video_thumb'=>self::NETURL.$obj->video_thumb,
				'doc_name'=>$obj->doc_name,
				'doc_hospital'=>$obj->doc_hospital,
				'created_at' => $obj->created_at
			);
		}
		$data['list'] = $aRecordshows;
		echo $this->formatRet( true, "成功", "success", "", array("data"=>$data) );
		exit;
	}

	//详细页
	public function getShow(){
		if(!Input::has('token')||!Input::has('videoid')){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$token = Input::get('token');
		$token = Crypt::decrypt($token);
		$aUserinfo = explode('\t',$token);
		$iUid = $aUserinfo[0];
		$iVideoid = Input::get('videoid');
		$oVideo = FaceVideo::getFaceVideoReviewById($iVideoid);
		$oZanlog = FaceVideoZanLog::selzanlog($iVideoid)->get();
		$iZancount = count($oZanlog);
		$iszan = false;
		if($iZancount>0){
			foreach($oZanlog as $v){
				if($v->user_id == $aUserinfo[0]){
					$iszan = true;
				}
			}
		}
		if(count($oVideo)){
			$oVideo->increment('video_hits');
			$oDoc = $oVideo->doc_info;
			$open_type = 1;
			if($oVideo->channel_type == 2){
				$open_type = 2;
			}
			if($oVideo->channel_type == 2){
				$video_url = $oVideo->video_url;
			}else{
				$video_url = url($oVideo->video_url);
			}
			$aVideo = array(
				'video_title' => $oVideo->video_title,
				'video_type' => $oVideo->video_type,
				'video_thumb'=> url($oVideo->video_thumb),
				'video_url' => $video_url,
				'video_introduce' => $oVideo->video_introduce,
				'video_hits' => $oVideo->video_hits,
				'zancount'=>$iZancount,
				'iszan'=>$iszan,
				'start_time' => $oVideo->start_time,
				'end_time' => $oVideo->end_time,
				'doc_name' => $oDoc->doc_name,
				'doc_thumb' => url($oDoc->doc_thumb),
				'doc_position' => $oDoc->doc_position,
				'doc_department' => $oDoc->doc_department,
				'doc_hospital' => $oDoc->doc_hospital,
				'doc_introduction' => $oDoc->doc_introduction,
				'open_type' => $open_type	//目睹 浏览器打开 
			);
			//星级评分
			$aVideo['score'] = array();
			$oScoreLog = FaceVideoScore::where('face_video_id',$iVideoid)->where('user_id',$aUserinfo[0])->first();
			if(count($oScoreLog)){
				$aVideo['score'] = array(
					'score1'  => $oScoreLog->content_degree,
					'score2'  => $oScoreLog->class_level,
					'score3'  => $oScoreLog->overall_score
				);
			}
			//评论列表
			$oComments = Comment::getComment(1,$iVideoid);
			$aComments = array();
			foreach($oComments as $obj){
				$aComments[] = array(
					'comment_id' => $obj->id,
					'user_nick'  => $obj->user_nick,
					'user_thumb' => Config::get('app.url').$obj->user_thumb,
					'comment'    => str_replace('/assets/images/front/emotion/',Config::get('config.rooturl').'/assets/images/front/emotion/',$obj->comment),
					'zan_count'  => $obj->zan_count,
					'device_type'=> $obj->device_type,
					'created_at' => substr($obj->created_at,0,10)
				);
			}
			$aVideo['comments'] = $aComments;
			//加积分记录
			$creditIN = CreditController::getInstance($iVideoid,0,$aUserinfo[0],'ios');
			$aVideo['share_flag'] = 1;
			$aVideo['share_title'] = $oVideo->video_title;
			$aVideo['share_thumb'] = Config::get('app.url').'/assets/images/mobile/logo.png';
			$aVideo['share_url'] = Config::get('app.url').'/mobile-docface/review-show/'.$iVideoid;
			$creditIN->optCredit();
			// FaceVideo::addUserScoreAndViewLog($iVideoid,0,$aUserinfo[0]);//添加记录
			//精品课程弹窗是否显示
			$Configs = new Configs();
			$configRs = $Configs::find(1)->toArray();
			$aVideo['buyTips'] = 0;
			$aVideo['buyTipsTxt'] = "观看此视频需消耗{$configRs['view_video_deduct_integral']}积分，是否观看？";
			if($oVideo->video_rec==1 && $configRs['view_video_deduct_integral']){
				//获取积分日志列表
				$IntegralLog = new IntegralLog();
				$purchased = IntegralLog::where('user_id',$aUserinfo[0])->
					where('obj_type','=','video')->
					where('obj_id','=',$iVideoid)->first();
				if(!$purchased){
					$aVideo['buyTips'] = 1;
				}
			}
			//插入健康教育视频观看日志
			$VideoViewLog = new VideoViewLog();
			$vVlog = array(
				'user_id'=>$iUid, 'video_id'=>$iVideoid, 'video_title'=>$aVideo['video_title'], 'video_thumb'=>$aVideo['video_thumb']
			);
			$VideoViewLog->addItem($vVlog)->save();
			echo  $this->formatRet(true,"成功","success",$aVideo);exit;
		}else{
			echo  $this->formatRet(false,'视频不存在');exit;
		}
	}

	/**
	 * 录播视频星级评分   post提交
	 * /apivideo/score?videoid=1&score1=1&score2=1&score3=1&device=2&token=
	 */
	public function postScore()
	{
		$iVideoId = Input::get('videoid',0);
		$oVideo = FaceVideo::find($iVideoId);
		if(!$oVideo){
			echo $this->formatRet(false,"不存在该视频");exit;
		}
		$iScore1 = Input::get('score1',0);
		$iScore2 = Input::get('score2',0);
		$iScore3 = Input::get('score3',0);
		$token = Input::get('token');
		$token = Crypt::decrypt($token);
		$aUserinfo = explode('\t',$token);
		$iUserId = $aUserinfo[0];
		$iDevice = Input::get('device',2);
		FaceVideoScore::addScoreLog($iVideoId,$iUserId,$iScore1,$iScore2,$iScore3,$iDevice);
		echo  $this->formatRet(true,"成功","success");exit;
	}
	
	public function getCommentList(){
		$iVideoid = Input::get('videoid');
		$oComments = Comment::getComment(1,$iVideoid);
		$aComments = array();
		foreach($oComments as $obj){
			$aComments[] = array(
				'id'		=> $obj->id,
				'user_nick' => $obj->user_nick,
				'user_thumb' => $obj->user_thumb,
				'comment' => str_replace('/assets/images/front/emotion/',Config::get('config.rooturl').'/assets/images/front/emotion/',$obj->comment),
				'zan_count' =>$obj->zan_count,
				'device_type' => $obj->device_type,
				'created_at' => $obj->created_at
			);
		}
		echo  $this->formatRet(true,'获取记录成功','success',$aComments);exit;
	}

	public function postComment(){
		$iVideoid = Input::get('videoid');
		$token = Input::get('token');
		$token = Crypt::decrypt($token);
		$aUserinfo = explode('\t',$token);
		$aComment = array(
			'user_id'=>$aUserinfo[0],
			'cat_id' => 1,
			'source_id'=>$iVideoid,
			'comment' => Input::get('comment'),
			'device_type'=> 1,
			'created_at' => date('Y-m-d H:i:s',time())
		);
		$oComment = new Comment($aComment);
		$oComment->save();
		echo  $this->formatRet(true,'评论成功');exit;
	}

	public function postSurpport(){
		$iVideoid = Input::get('videoid');
		$token = Input::get('token');
		$token = Crypt::decrypt($token);
		$aUserinfo = explode('\t',$token);
		$aUserinfo = explode('\t',$token);
		$iUid = $aUserinfo[0];
		if(FaceVideoZanLog::isUserZan($iVideoid, $iUid)){
			//已赞
			echo  $this->formatRet(false,'您已支持过该视频，无需重复支持！');exit;
		}
		$oZanlog = FaceVideoZanLog::UserZan($iVideoid, $iUid);
		echo  $this->formatRet(true,'支持成功');exit;
	}

	//&token=eyJpdiI6InRFdStHcm5XU3IrUVwvXC9PNTVkQTE1SCs0eVpGbTZGelhcL085N3BzODVydEU9IiwidmFsdWUiOiJzOUx6T2pqaUh5cFhGWUxYNlBHZEQwZEVkOWRHT05xYWh0NWV3UlRMUGhZPSIsIm1hYyI6ImY1MDdiYmUzZDVhNTVhNjExNmI2MGE3YzRlZTgxZTM0ZDUxOWM0ZDdhZGQ4MmJjNzc3OGExNjgwNDc1NTcyZmYifQ==
	public function postTime(){//时间记录
		$token = Input::get('token');
		$token = Crypt::decrypt($token);
		$aUserinfo = explode('\t',$token);
		$aData = Input::all();
		$time = $aData['time'];
		$time = $time?$time:0;
		//加积分记录
		$creditIN = CreditController::getInstance($aData['videoid'],$time,$aUserinfo[0],'ios');
		$creditIN->optCredit();
		// FaceVideo::addUserScoreAndViewLog($aData['videoid'],$time,$aUserinfo[0]);
		$oFaceVideo = FaceVideo::find($aData['videoid']);
		$iMin = $time;
		if($oFaceVideo->video_type=='1'&&$iMin>=20){
			$iVideoId = $aData['videoid'];
			$iUserId = $aUserinfo[0];
			//详细记录用户观看该录播的情况
			VideoReviewLog::addUserReviewLog($iVideoId,$iUserId,$iMin);
		}
		echo  $this->formatRet(true,'成功');exit;
	}
	
	/**
	 * 返回所有科室
	 * http://cdma.local/apivideo/department?token=eyJpdiI6InQzTHZBQTBkVjBHWDdqcXZxcyt5YUM5TXU1eGsraENBRkNXbkJSWmRJQU09IiwidmFsdWUiOiJITHQ1V2ozTmJnaU5mS1RTczk4emRBazIyYXJGaUtJdmNoVklZOEFBTGlJPSIsIm1hYyI6ImI2YWY1MjY4NzNkYWQ1MzExODMxNTJiOTU3Yjc1ZTNkM2U2ZTc2MzJlMWY5YTk5YzM1NTU5NDE3Zjk5MmUzNjQifQ==
	 */
	public function getDepartment(){
		if(!Input::has('token')){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$token = Input::get('token');
		$token = Crypt::decrypt($token);
		$aDepartment = Config::get('config.department');
		$arr = array();
		foreach($aDepartment as $k=>$v){
			$arr[] = array(
				'id'		=> $k,
				'name'		=> $v
			);
		}
		echo $this->formatRet(true,"","success",$arr);exit;
	}

	/**
	 * 评论点赞   /apivideo/comment-surpport?commentid=250&token=
	 */
	public function postCommentSurpport(){
		$iCatId =1;
		$iCommentId = Input::get('commentid');
		$token = Input::get('token');
		$token = Crypt::decrypt($token);
		$aUserinfo = explode('\t',$token);
		$iUid = $aUserinfo[0];
		$oComment = Comment::find($iCommentId);
		if(!$oComment){
			echo  $this->formatRet(false,'该评论不存在');exit;
		}
		$oCommentZanLog = CommentZanLog::getCommentZanLog($iCommentId,$iUid);
		if($oCommentZanLog){
			echo  $this->formatRet(false,'您已经赞过了！');exit;
		}
		$oCommentZanLog = new CommentZanLog(array('comment_id'=>$iCommentId,'user_id'=>$iUid,'cat_id'=>$iCatId,'created_at'=>date('Y-m-d H:i:s',time())));
		$oCommentZanLog->save();
		$oComment->increment('zan_count');
		echo  $this->formatRet(true,'点赞成功');exit;
	}

}