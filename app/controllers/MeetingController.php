<?php

use Illuminate\Support\Facades\Redirect;

class MeetingController extends BaseController {
	
	CONST  PAGESIZE = 10;
	CONST  CATID = 1;
    private $iUserId = 0;
    private $iRoleId = 0;
	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	public function __construct(){
		$this->iUserId = Session::get('userid');
		$this->iRoleId = Session::get('roleid');
	}

	public function Test(){
		$aTimeCode = array('61','62','71','72','81','82','91','92','101','102',
		'111','112','121','122','131','132','141','142','151','152',
		'161','162','171','172','181','182','191','192','201','202','211','212');
		//传入数值 返回所有时间点
		$aTime = Meeting::getTime($aTimeCode);
		return View::make('front.meeting.test')->with('aTime',$aTime);
	}

	public function Index(){
		$aTimeCode = array('61','62','71','72','81','82','91','92','101','102',
		'111','112','121','122','131','132','141','142','151','152',
		'161','162','171','172','181','182','191','192','201','202','211','212');
		//传入数值 返回所有时间点
		$aTime = Meeting::getTime($aTimeCode);
		return View::make('front.meeting.index')->with('aTime',$aTime);
	}

	public function Ajax(){
		$aTime = array('61','62','71','72','81','82','91','92','101','102',
		'111','112','121','122','131','132','141','142','151','152',
		'161','162','171','172','181','182','191','192','201','202','211','212');
		$type = Input::get('type');
	    if ($type != 'ajax'){
	      return false;
	    }
	    $date = Input::get('date');
	    $meetingdate = $date.' 00:00:00';
	    $starttime = strtotime($meetingdate);
		$nowtime = strtotime(date('Y-m-d H:i:s',time()));
		$days = ($starttime-$nowtime)/3600/24 ;
		if($days<10){
			return json_encode(array('success'=>'success','aTime'=>$aTime,'aTimeCode'=>'000'));
		}else{
			$aTimeRecord = MeetingTime::where('meeting_date',$meetingdate)->lists('meeting_time_period','id');
			//筛选出数组中同一时间段的个数
			$aCountRecord = array_count_values($aTimeRecord);
			$iMaxNum = Config::get('config.self_help_meeting_order_set');
			$aTimeCode = array();
			foreach($aCountRecord as $k=>$v){
				if($v >= $iMaxNum){
					$aTimeCode[] = $k;
				}
			}
		    return json_encode(array('success'=>'success','aTime'=>$aTime,'aTimeCode'=>$aTimeCode));
		}
	}

	//创建会议
	public function CreateDo(){
		$aInput = Input::all();
		$date = $aInput['meeting_time'];
		//创建会议必须10天以上
	 	$meetingdate = $date.' 00:00:00';
	    $starttime = strtotime($meetingdate);
		$nowtime = strtotime(date('Y-m-d H:i:s',time()));
		$days = ($starttime-$nowtime)/3600/24 ;
		if($days<10){
			echo 'time error';die;
		}
		// 页面提交一个时间段的字符串  拼接成数组用
		$sTimePeriod = substr($aInput['time_period'],0,-1);
		$aTimePeriod = explode(',',$sTimePeriod);
		//筛选出数组中同一时间段的个数
		$aTimeRecordLog = MeetingTime::where('meeting_date',$meetingdate)->lists('meeting_time_period','id');
		$aCountRecord = array_count_values($aTimeRecordLog);
		$iMaxNum = Config::get('config.self_help_meeting_order_set');
		$aTimeCode = array();
		foreach($aCountRecord as $k=>$v){
			if($v >= $iMaxNum){
				$aTimeCode[] = $k;
			}
		}
		if(!empty($aTimeCode)){
			foreach($aTimePeriod as $v){
				if(in_array($v,$aTimeCode)){
					echo 'error';die;
				}
			}
		}
		$max=$min=$aTimePeriod[0];
		foreach($aTimePeriod as $n){
			$max=($n>$max)?$n:$max;
			$min=($n<$min)?$n:$min;
		}
		$aTime = Meeting::getTimeRule($date,$min,$max);
		if(isset($aInput)){
			$aMeeting = array(
				'meeting_start_time' => $aTime[0],
				'meeting_end_time'   => $aTime[1],
				'expert_company' => $aInput['expert_company'],
				'expert_subject' => $aInput['expert_subject'],
				'expert_name' 	 => $aInput['expert_name'],
				'meeting_title'  => $aInput['meeting_title'],
				'creator_name'   => $aInput['creator_name'],
				'creator_tel'    => $aInput['creator_tel'],
				'creator_email'  => $aInput['creator_email'],
				'representative' => $this->iUserId,
				'created_at'     => date('Y-m-d H:i:s',time())
			);
			$oMeeting = new Meeting($aMeeting);
			$oMeeting->save();
			$id = $oMeeting->id;
			foreach($aTimePeriod as $val){
				$aPeriod = array(
					'meeting_id'  => $id,
					'meeting_date'  => $aInput['meeting_time'],
					'meeting_time_period'  =>$val,
					'created_at'     => date('Y-m-d H:i:s',time())
				);
				$oMeetingTime = new MeetingTime($aPeriod);
				$oMeetingTime->save();
			}
			return 'success';
		}else{
			echo 'error';die;
		}
	}
	
	//我的会议
	public function MyMeeting(){
		$iUid = Session::get('userid'); 
	    $oMeeting = Meeting::where('representative',$iUid)
		    ->orderBy("created_at","desc")
		    ->paginate(self::PAGESIZE);
		foreach($oMeeting as $key=>$val){
			//判断会议状态
			$oOrder = MeetingOrder::where('meeting_id',$val->id)->first();
			if($oOrder && $val->meeting_type_id != 1){        //会议被预约
				$val->order = 1;
			}elseif($oOrder && $val->meeting_type_id == 1){   //会议被确认
				$val->order = 2;
			}else{
				$val->order = 0;
			}
			//根据会议开始时间判断取消按钮的状态
			$starttime = strtotime($val->meeting_start_time);
			$nowtime = strtotime(date('Y-m-d H:i:s',time()));
			$days = ($starttime-$nowtime)/3600/24 ;
			if($days>7){
				$val->cancel = 1;      //会议开始一周前可以取消
			}elseif($val->meeting_start_time<=date('Y-m-d H:i:s',time())&&$val->meeting_end_time>=date('Y-m-d H:i:s',time())){
				$val->cancel = 2;      //会议开始时按钮变为“加入会议”
			}else{
				$val->cancel = 0;      //会议开始一周内不可以取消
			}
		}						    
		return View::make('front.meeting.mymeetinglist')->with('oMeeting',$oMeeting);
	}
	
	//编辑会议
	public function Edit($id){
		$aTimeCode = array('61','62','71','72','81','82','91','92','101','102',
		'111','112','121','122','131','132','141','142','151','152',
		'161','162','171','172','181','182','191','192','201','202','211','212');
		//传入数值 返回所有时间点
		$aTime = Meeting::getTime($aTimeCode);
		$oMeeting = Meeting::find($id);
		$date = substr($oMeeting->meeting_start_time,0,10);
		$meetingdate = $date.' 00:00:00';
		//所选时间段色块
		$aMeetingTime = MeetingTime::where('meeting_id',$id)->lists('meeting_time_period','id');
		//不可选色块
		$aTimeRecord = MeetingTime::where('meeting_date',$meetingdate)->lists('meeting_time_period','id');
		//筛选出数组中同一时间段的个数
		$aCountRecord = array_count_values($aTimeRecord);
		$iMaxNum = Config::get('config.self_help_meeting_order_set');
		$aTimeCode = array();
		foreach($aCountRecord as $k=>$v){
			if($v >= $iMaxNum){
				$aTimeCode[] = $k;
			}
		}
		return View::make('front.meeting.mymeetingedit')
			->with('oMeeting',$oMeeting)
			->with('aMeetingTime',$aMeetingTime)
			->with('aTimeCode',$aTimeCode)
			->with('aTime',$aTime);
	}

	public function EditDo($id){
		$aInput = Input::all();
		$date = $aInput['meeting_time'];
		//创建会议必须10天以上
	 	$meetingdate = $date.' 00:00:00';
	    $starttime = strtotime($meetingdate);
		$nowtime = strtotime(date('Y-m-d H:i:s',time()));
		$days = ($starttime-$nowtime)/3600/24 ;
		if($days<10){
			echo 'time error';die;
		}
		//页面提交一个时间段的字符串  拼接成数组用
		$sTimePeriod = substr($aInput['time_period'],0,-1);
		$aTimePeriod = explode(',',$sTimePeriod);
		//筛选出数组中同一时间段的个数
		$aTimeRecordLog = MeetingTime::where('meeting_date',$meetingdate)->lists('meeting_time_period','id');
		$aCountRecord = array_count_values($aTimeRecordLog);
		$iMaxNum = Config::get('config.self_help_meeting_order_set');
		$aTimeCode = array();
		foreach($aCountRecord as $k=>$v){
			if($v > $iMaxNum){
				$aTimeCode[] = $k;
			}
		}
		if(!empty($aTimeCode)){
			foreach($aTimePeriod as $v){
				if(in_array($v,$aTimeCode)){
					echo 'error';die;
				}
			}
		}
		$max=$min=$aTimePeriod[0];
		foreach($aTimePeriod as $n){
			$max=($n>$max)?$n:$max;
			$min=($n<$min)?$n:$min;
		}
		$aTime = Meeting::getTimeRule($date,$min,$max);
		$oMeeting = Meeting::find($id);
		if($oMeeting){
			$oMeeting->meeting_start_time = $aTime[0];
			$oMeeting->meeting_end_time   = $aTime[1];
			$oMeeting->expert_company  = $aInput['expert_company'];
			$oMeeting->expert_subject  = $aInput['expert_subject'];
			$oMeeting->expert_name  = $aInput['expert_name'];
			$oMeeting->meeting_title  = $aInput['meeting_title'];
			$oMeeting->creator_name  = $aInput['creator_name'];
			$oMeeting->creator_tel  = $aInput['creator_tel'];
			$oMeeting->creator_email  = $aInput['creator_email'];
			$oMeeting->save();
			//更新时间段数据库
			$oDelTime = MeetingTime::where('meeting_id',$id)->get();
			foreach($oDelTime as $v){
				$v->delete();
			}
			// $oDelTime = MeetingTime::where('meeting_id',$id)->where('status',0)->update(array('status' => 1));
			foreach($aTimePeriod as $val){
				$aPeriod = array(
					'meeting_id'  => $id,
					'meeting_date'  => $aInput['meeting_time'],
					'meeting_time_period'  => $val,
					'created_at'     => date('Y-m-d H:i:s',time())
				);
				$oMeetingTime = new MeetingTime($aPeriod);
				$oMeetingTime->save();
			}
			return 'success';
		}else{
			echo 'error';die;
		}
	}

	//取消会议
	public function Delete($id){
		$oMeeting = Meeting::find($id);
		if($oMeeting){
			//已确认 只 给预约成功的人发短信
			if($oMeeting->meeting_type_id == 1){
				$oOrder = MeetingOrder::where('meeting_id',$oMeeting->id)->where('order_type_id',2)->first();
				if(count($oOrder)){
					$sMobile = $oOrder->order_tel;
					$sMeetingTitle = $oMeeting->meeting_title;
					$sStartTime = substr($oMeeting->meeting_start_time,5,2).'月'.substr($oMeeting->meeting_start_time,8,2).'日'.substr($oMeeting->meeting_start_time,11,5);
					$sEndTime = substr($oMeeting->meeting_end_time,11,5);
					$sValue = $sStartTime.'-'.$sEndTime.$sMeetingTitle;
					$sValue1 = '预约的'.$sValue;
					$sValue2 = '会议创建人';
					$iModelId = '102292';
					//发送短信
					$oMessage = MeetingMessageLog::sendMessage($sMobile,$iModelId,$sValue1,$sValue2);
					/*************/
				}
			}elseif($oMeeting->meeting_type_id == 0){
			// 会议被预约待确认 给所有预约人发短信
				$oOrder = MeetingOrder::where('meeting_id',$oMeeting->id)->where('order_type_id',0)->get();
				if($oOrder){
					foreach($oOrder as $k=>$v){
						$sMobile = $v->order_tel;
						$sMeetingTitle = $oMeeting->meeting_title;
						$sStartTime = substr($oMeeting->meeting_start_time,5,2).'月'.substr($oMeeting->meeting_start_time,8,2).'日'.substr($oMeeting->meeting_start_time,11,5);
						$sEndTime = substr($oMeeting->meeting_end_time,11,5);
						$sValue = $sStartTime.'-'.$sEndTime.$sMeetingTitle;
						$sValue1 = $sValue;
						$iModelId = '102295';
						//发送短信
						$oMessage = MeetingMessageLog::sendMessage($sMobile,$iModelId,$sValue1,'');
					}
				}
			}
			$oMeeting->status = 1;
			$oMeeting->save();
			return 'success';
		}else{
			echo 'error';die;
		}
	}

	//查看预约会议
	public function OrderList($id){
		$oOrder = MeetingOrder::where('meeting_id',$id)->get();
		return View::make('front.meeting.orderlist')->with('oOrder',$oOrder);
	}

	//接受预约会议
	public function OrderDo(){
		$iMeetingId = Input::get('meetingid');
		$iOrderId = Input::get('orderid');
		$oMeeting = Meeting::find($iMeetingId);
		$oOrder = MeetingOrder::find($iOrderId);
		if($oMeeting && $oOrder){
			$oApiLog = MeetingApiLog::where('meeting_id',$iMeetingId)->first();
			//如果会议已经确认过 则不再分配新的会议编码 直接更改状态 并发短信通知
			if(count($oApiLog)){
				//会议状态改为已确认预约
				$oMeeting->meeting_type_id = 1;
				$oMeeting->save();
				//更改所有人的预约状态
				$oOrder->order_type_id = 2;
				$oOrder->save();
				$oOrderFail = MeetingOrder::where('meeting_id',$iMeetingId)->where('id','<>',$iOrderId)->update(array('order_type_id' => 1));
				$sMobile = $oOrder->order_tel;
				$sMeetingTitle = $oMeeting->meeting_title;
				$sStartTime = substr($oMeeting->meeting_start_time,5,2).'月'.substr($oMeeting->meeting_start_time,8,2).'日'.substr($oMeeting->meeting_start_time,11,5);
				$sEndTime = substr($oMeeting->meeting_end_time,11,5);
				$sValue1 = $sStartTime.'-'.$sEndTime.$sMeetingTitle;
				$iModelId = '102290';
				//发送短信
				$oMessage = MeetingMessageLog::sendMessage($sMobile,$iModelId,$sValue1,'');
				return json_encode(array('success'=>'success','code'=>$iMeetingId));
			}else{
				//为创建的会议分配会议编码(一个时间段一个会议编码只能用一次)
				/*************s****/
				$date = substr($oMeeting->meeting_start_time,0,10);
				$meetingdate = $date.' 00:00:00';
				$aTimePeriod = MeetingTime::where('meeting_date',$meetingdate)->lists('meeting_time_period','id');
				$aRes = MeetingTime::where('meeting_date',$meetingdate)
					->whereIn('meeting_time_period',$aTimePeriod)
					->lists('meeting_time_period','meeting_code');
				if(!empty($aRes)){								
					$aRes = array_keys($aRes);
					$oCode = MeetingCode::whereNotIn('code',$aRes)->orderby('id','asc')->first();
				}else{
					$oCode = MeetingCode::orderby('id','asc')->first();
				}
				if($oCode){
					$sMeetingCode = $oCode->code;
					$oMeeting->meeting_code = $sMeetingCode;
					$oMeeting->save();
					$oTime = MeetingTime::where('meeting_id',$iMeetingId)
						->update(array('meeting_code' => $sMeetingCode));
					//调用会畅接口**********************start**********************/
					$oMeetingCode = MeetingCode::where('code',$oMeeting->meeting_code)->first();
					$sApiKey = Config::get('config.hc_api_key');
					$sApiUrl = Config::get('config.hc_api_url');
					$sTimeStamp = time();
					$sToken = md5($oCode->name.'|'.$sApiKey.'|'.$sTimeStamp);
					$iDuration = (strtotime($oMeeting->meeting_end_time) - strtotime($oMeeting->meeting_start_time))/60;
					$data = array(
						'loginName'  => $oCode->name,
						'timeStamp'  => $sTimeStamp,
						'token'  	 => $sToken,
						'confName'   => $oMeeting->meeting_title,
						'hostKey'    => $oCode->key,
						'startTime'  => $oMeeting->meeting_start_time,
						'duration'   => $iDuration,
						'optionJbh'  => 0
					);
					$aReturnInfo = requestApiByCurl($sApiUrl,$data,'POST');
					//记录接口返回信息
					if($aReturnInfo['status'] == 100){
						$aLog = array(
							'meeting_id'  	=>  $oMeeting->id,
							'audio'  		=>  $aReturnInfo['data']['audio'],
							'hoststarturl'  =>  $aReturnInfo['data']['hostStartUrl'],
							'starturl'  	=>  $aReturnInfo['data']['startUrl'],
							'confstatus'  	=>  $aReturnInfo['data']['confStatus'],
							'optionjbh'  	=>  $aReturnInfo['data']['optionJbh'],
							'meetingno'  	=>  $aReturnInfo['data']['meetingNo'],
							'api_id'  		=>  $aReturnInfo['data']['id'],
							'starttime' 	=>  $aReturnInfo['data']['startTime'],
							'duration'  	=>  $aReturnInfo['data']['duration'],
							'meetingname'   =>  $aReturnInfo['data']['meetingName'],
							'token'  		=>  $aReturnInfo['data']['token'],
							'pwd'  			=>  $aReturnInfo['data']['h323pwd'],
							'userid'  		=>  $aReturnInfo['data']['userId'],
							'joinurl'  		=>  $aReturnInfo['data']['joinUrl'],
							'hostkey'  		=>  $aReturnInfo['data']['hostKey'],
							'maxcount'  	=>  $aReturnInfo['data']['maxCount'],
							'created_at'  	=>  date('Y-m-d H:i:s',time())
						);
						$oMeetingApiLog = new MeetingApiLog($aLog);
						$oMeetingApiLog->save();
						//调用会畅接口**********************end*********************/
						//会议状态改为已确认预约
						$oMeeting->meeting_type_id = 1;
						$oMeeting->save();
						//更改所有人的预约状态
						$oOrder->order_type_id = 2;
						$oOrder->save();
						$oOrderFail = MeetingOrder::where('meeting_id',$iMeetingId)
							->where('id','<>',$iOrderId)
							->update(array('order_type_id' => 1));
						//会议确认，以短信的方式通知预约成功方
						$sMobile = $oOrder->order_tel;
						$sMeetingTitle = $oMeeting->meeting_title;
						$sStartTime = substr($oMeeting->meeting_start_time,5,2).'月'.substr($oMeeting->meeting_start_time,8,2).'日'.substr($oMeeting->meeting_start_time,11,5);
						$sEndTime = substr($oMeeting->meeting_end_time,11,5);
						$sValue1 = $sStartTime.'-'.$sEndTime.$sMeetingTitle;
						$iModelId = '102290';
						//发送短信
						$oMessage = MeetingMessageLog::sendMessage($sMobile,$iModelId,$sValue1,'');
						return json_encode(array('success'=>'success','code'=>$iMeetingId));
					}else{
						return json_encode(array('success'=>'error'));
					}
				}else{
					return json_encode(array('success'=>'error'));
				}
			}
		}else{
			return json_encode(array('success'=>'error'));
		}	
	}

	//主持人进入会议
	public function EnterDo(){
		$iMeetingId = Input::get('meetingid');
		$oMeetingLog = MeetingApiLog::where('meeting_id',$iMeetingId)->first();
		if($oMeetingLog){
			$aLog = array(
				'user_id'   => $this->iUserId,
				'user_type' => 2,
				'meeting_id'=> $iMeetingId,
				'created_at'=> date('Y-m-d H:i:s',time())
			);
			$oLog = new MeetingEnterLog($aLog);
			$oLog->save();
			$sUrl = $oMeetingLog->hoststarturl;
			return json_encode(array('success'=>'success','url'=>$sUrl));
		}else{
			return json_encode(array('success'=>'error'));
		}
	}

	//观看会议----进入会议
	public function JoinDo(){
		$iMeetingId = Input::get('meetingid');
		$oMeeting = Meeting::find($iMeetingId);
		//验证输入的会议编码
		if($oMeeting){
			$iMeetingType = $oMeeting->meeting_type_id;
			//会议已确认
			if($iMeetingType == 1){
				$sNowTime = date('Y-m-d H:i:s',time());
				//会议已开始
				if($oMeeting->meeting_start_time <= $sNowTime){
					$oMeetingLog = MeetingApiLog::where('meeting_id',$iMeetingId)->first();
					if($oMeetingLog){
						if($this->iUserId == $oMeeting->representative){
							$iUserType = 2;
						}else{
							$iUserType = 1;
						}
						$aLog = array(
							'user_id'   => $this->iUserId,
							'user_type' => $iUserType,
							'meeting_id'=> $oMeetingLog->meeting_id,
							'created_at'=> date('Y-m-d H:i:s',time())
						);
						$oLog = new MeetingEnterLog($aLog);
						$oLog->save();
						$sUrl1 = $oMeetingLog->joinurl;
						$sUrl2 = $oMeetingLog->hoststarturl;
						return json_encode(array('success'=>'success','usertype'=>$iUserType,'url1'=>$sUrl1,'url2'=>$sUrl2));
					}else{
						return json_encode(array('success'=>'error'));
					}
				}else{
					return json_encode(array('success'=>'time error'));
				}
			}else{
				return json_encode(array('success'=>'no confirm'));
			}
		}else{
			return json_encode(array('success'=>'no code'));
		}
	}

	//观看会议
	public function JoinMeeting(){
		return View::make('front.meeting.joinmeeting');
	}

	//预约会议列表
	public function AppointList()
	{ 
		//$iUserId = $this->iUserId;
		//dd($iUserId);
		$aHasMakeSureMeeting =array();
		$iMaxNum = Config::get('config.self_help_meeting_order_set');//获取会议的最大预约量
		$sDate = date("Y-m-d H:i:s");
		$aSelfMeetingId = array();
        $oMeeting = Meeting::where('meeting_start_time','>',$sDate)->where('status','!=',1)->get();
        foreach($oMeeting as $value){
        	//拼接时间格式
            if($value->representative == $this->iUserId){
        	   $aSelfMeetingId[] = $value->id;
        	}
        	if($value ->meeting_type_id == 1){
        	     $aHasMakeSureMeeting[] = $value ->id;
        	}
            $sMeetingStart = $value ->meeting_start_time;
            $sMeetingEnd = $value ->meeting_end_time;
            $sStart = substr($sMeetingStart,0,16);
            $sEndTime = substr($sMeetingEnd,11,5);
            $value->meeting_start_time =$sStart.'-'.$sEndTime;
        }
        $oMeetingOrder = MeetingOrder::where('order_user_id','=',$this->iUserId)->get();
        $aHasAppointmeeting =array();
        foreach($oMeetingOrder as $value){
          $aHasAppointmeeting[] = $value->meeting_id;
        }
        return  View::make('front.meeting.appointmessage')->with('oMeeting',$oMeeting)
			->with('aSelfMeetingId',$aSelfMeetingId)
			->with('aHasMakeSureMeeting',$aHasMakeSureMeeting)
			->with('aHasAppointmeeting',$aHasAppointmeeting)
			->with('iMaxNum',$iMaxNum);	  
	}

	//预约表单页面
	public function AppointMessage($id)
	{
	   return View::make('front.meeting.appointmeeting')->with('id',$id);
	}

	//确认是否已经预约
	public function Makesure()
	{
	   $iMeeetingId = Input::get('meeting_id');
	   $iUid =$this->iUserId;
	   $oMeetingOrder = MeetingOrder::where('meeting_id',$iMeeetingId)->where('order_user_id',$iUid)->get();
	   if(count($oMeetingOrder)){
	   	     echo 'error';die; 
	   }else{
	       return 'success';
	   }
	}

	//添加预约
	public function Appoint()
	{
		$iMaxNum = Config::get('config.self_help_meeting_order_set');//获取会议的最大预约量
		$aMessage =  Input::all();
		if(isset($aMessage)){
			$iMeetingId = $aMessage['meeting_id'];
			$sOrderName = $aMessage['order_name'];
			$sOrderTel = $aMessage['order_tel'];
			$sOrderEmail= $aMessage['order_email'];
			$sOrderHospital = $aMessage['order_hospital'];
			$sOrderDepartment= $aMessage['order_department'];
			$iOrderNum = $aMessage['order_num'];
			//察看当前用户是否已经预约该会议
			$oMeetingOrderLog = MeetingOrder::where('meeting_id',$iMeetingId)->where('order_user_id',$this->iUserId)->first();
			if(count($oMeetingOrderLog)){
			    echo 'error';die; 
			}
		    $oMeeting = Meeting::where('id',$iMeetingId)->first();
			$iOrderCount = $oMeeting ->order_count;
			if($iOrderCount < $iMaxNum){
			   $oMeeting ->order_count = $iOrderCount + 1;
			   $oMeeting ->save();
			}else{
			   echo 'error';die; 
			}
			$oMeetingOrder = new MeetingOrder;
			$oMeetingOrder ->meeting_id = $iMeetingId;
			$oMeetingOrder ->order_name = $sOrderName;
			$oMeetingOrder ->order_tel = $sOrderTel;
			$oMeetingOrder ->order_email = $sOrderEmail;
			$oMeetingOrder ->order_department = $sOrderDepartment;
			$oMeetingOrder ->order_hospital = $sOrderHospital;
			$oMeetingOrder ->order_user_id = $this->iUserId;
			$oMeetingOrder ->order_num = $iOrderNum;
			$oMeetingOrder -> save();
	  	    return 'success';
		}else{
		    echo 'error';die; 
		}
	}

	//我预约的会议
	public function HasAppointedMeeting()
	{
	   $iMaxNum = Config::get('config.self_help_meeting_order_set');//获取会议的最大预约量
	   $iUid =$this->iUserId;
	   $sOneweekDate = date("Y-m-d H:i:s");
	   $aStartingMeeting = array();//记录当前用户正在进行直播的会议id
	   $aJoinMeetingMessage = array();
	   $aMeetingDetail = array();
       $aMeetingDelete = array();
	   $oMeetingOrderAppoint = MeetingOrder::where('order_type_id',2)->where('order_user_id',$iUid)->get();
	   if(count($oMeetingOrderAppoint)){
	   	   foreach($oMeetingOrderAppoint as $value){
	   	      $iMeetingId = $value ->meeting_id;
	   	      $oMeetingJoin = Meeting::find($iMeetingId);
	   	      if($oMeetingJoin->meeting_start_time <$sOneweekDate && $oMeetingJoin->status ==0){
                  $aStartingMeeting[] = $iMeetingId;
	   	      }
	   	   }
	   	   
	   }
       $oMeeting = MeetingOrder::join('meeting','meeting_order.meeting_id','=','meeting.id')->where('order_user_id',$iUid)->orderby('meeting_order.created_at','desc')->get();
       foreach($oMeeting as $value){
       	//拼接时间
	            $sMeetingStart = $value ->meeting_start_time;
	            $sStart = substr($sMeetingStart,0,16);
	            $sMeetingEnd = $value ->meeting_end_time;
	            $sEndTime = substr($sMeetingEnd,11,5);
	            $value->meeting_start_time =$sStart.'-'.$sEndTime;
	            $iOrderTypeId = $value ->  order_type_id;
	            if(strtotime($sMeetingStart) - time() < 7*24*60*60 && strtotime($sMeetingStart) - time() > 0){
	              $aMeetingDelete[$value->id]=1;
	            }else{
	              $aMeetingDelete[$value->id]=0;
	            }
	            if(strtotime($value->meeting_start_time) - time() < 24*7*60*60 && $iOrderTypeId == 2 && strtotime($value->meeting_start_time) - time() > 0){
                  	if($value->status == 1){
						$value ->meeting_status = '创建人已取消本次会议';
						$aMeetingDetail[$value->id]=0;
                 	}else{
                     	$value ->meeting_status = '预约会议成功';
                      	$aMeetingDetail[$value->id]=1;
                  	}
	            }else{
               		switch ($iOrderTypeId){
		               	case 0:
		               		if(strtotime($value->meeting_start_time) > time()){
		               			if($value->status == 0){
									$value ->meeting_status = '待确定';
									$aMeetingDetail[$value->id]=0;
		               			}else{
									$value ->meeting_status = '创建人已取消本次会议';
									$aMeetingDetail[$value->id]=0;
		               			}
		               		}else{
		               			if($value->status == 0){
									$value ->meeting_status = '会议过期';
									$aMeetingDetail[$value->id]=0;
		               			}else{
									$value ->meeting_status = '创建人已取消本次会议';
									$aMeetingDetail[$value->id]=0;
		               			}
		               		}
		               		break;
		               	case 1:
	                       if(strtotime($value->meeting_start_time) - time() < 24*7*60*60){
	                       	    if($value->status == 0){ 
									$value ->meeting_status = '预约失败';
									$aMeetingDetail[$value->id]=0;
	                        	}else{
									$value ->meeting_status = '创建人已取消本次会议';
									$aMeetingDetail[$value->id]=0;
	                        	}
		               		}else{
		               			if($value->status == 0){ 
									$value ->meeting_status = '待确认';
									$aMeetingDetail[$value->id]=0;
		               			}else{
									$value ->meeting_status = '创建人已取消本次会议';
									$aMeetingDetail[$value->id]=0;
		               			}
		               		}
		               		break;
		               	case 2:
		               		if($value->status == 0){ 
								$value ->meeting_status = '预约会议成功';
								$aMeetingDetail[$value->id]=1;
		               		}else{
								$value ->meeting_status = '创建人已取消本次会议';
								$aMeetingDetail[$value->id]=0;
		               		}
		               		break;
	               }
	            
	            }
	    }
	    return  View::make('front.meeting.hasappointmeeting')->with('oMeeting',$oMeeting)
			->with('iUid',$iUid)
			->with('aStartingMeeting',$aStartingMeeting)
			->with('aJoinMeetingMessage',$aJoinMeetingMessage)
			->with('aMeetingDelete',$aMeetingDelete)
			->with('iMaxNum'.$iMaxNum)
			->with('aMeetingDetail',$aMeetingDetail);	  
	
	}

	//会议详情
	public function MeetingDetail($id)
	{
		$iUid =$this->iUserId;//获得用户id
		$oMeetingDetail = Meeting::find($id);
		//拼接时间格式
		$sMeetingStart = $oMeetingDetail ->meeting_start_time;
		$sMeetingEnd = $oMeetingDetail ->meeting_end_time;
		$sStart = substr($sMeetingStart,0,16);
		$sEndTime = substr($sMeetingEnd,11,5);
		$oMeetingDetail->meeting_start_time =$sStart.'-'.$sEndTime;
		return  View::make('front.meeting.appointdetail')->with('oMeetingDetail',$oMeetingDetail);
	}

	//取消预约
	public function DeleteAppointment()
	{
		$id = Input::get('meeting_id');
		$iUid =$this->iUserId;//获得用户id
		$oMeetingDetail = MeetingOrder::where('meeting_id',$id)->where('order_user_id',$iUid)->first();
		if(count($oMeetingDetail)){
			$oMeetingmess =Meeting::find($id);
			$sMeetingStart = $oMeetingmess ->meeting_start_time;
			$sStart = substr($sMeetingStart,0,16);
			$sMeetingEnd = $oMeetingmess ->meeting_end_time;
			$sEndTime = substr($sMeetingEnd,11,5);
			$valuetime =$sStart.'-'.$sEndTime;
			$telephoneNum = $oMeetingmess->creator_tel;
			$sMeetingtitle = $oMeetingmess->meeting_title;
			$value1 ="创建的".$valuetime.$sMeetingtitle;
			$value2 = "会议预约人";
			$sStartTime = $oMeetingmess ->meeting_start_time;
			$iTime = strtotime($sStartTime);
			$iNowTime = time();
			$inum = $iTime - $iNowTime;
			if($inum < 7*24*60*60 && $inum > 0 ){
		  	    //if($oMeetingmess->status == 1){
		  	       //如果用户取消了会议，七天之内删除用户
		  	    //}
		  	    if($oMeetingDetail ->order_type_id == 1){
		  	    	if($oMeetingmess->meeting_type_id == 1){
	                    $oMessageten = MeetingMessageLog::sendMessage($telephoneNum, 102292,$value1,$value2);
		  	    	}
		  	    	if($oMeetingDetail->order_type_id == 2){
		  	         	$oMeetingDetail->order_type_id =1;
					    $oMeetingDetail ->save();
					    $oMeetingmess->meeting_type_id = 0;
					    $oMeetingmess->save();
				  	    $oMeetingOtherOrder = MeetingOrder::where('meeting_id',$id)->where('order_user_id','!=',$iUid)->get();
						if(count($oMeetingOtherOrder)){
						   foreach($oMeetingOtherOrder as $value){
						      $value -> order_type_id = 0;
						      $value ->save();
						   }
						}
		  	    	}else{
		  	    	    $oMeetingDetail->order_type_id =1;
					    $oMeetingDetail ->save();
		  	    	}
					if($oMeetingmess -> order_count >=1){
				         $oMeetingmess ->order_count -=1;  
				     	 $oMeetingmess ->save();
					}else{
				     	 $oMeetingmess ->order_count = 0;  
				     	 $oMeetingmess ->save();
					}
					return 'success';
		  	    }else{
		  	        echo 'error';die; 
		  	    }
			}else{
		     	if($oMeetingmess->meeting_type_id == 1){
				    $oMessageten = MeetingMessageLog::sendMessage($telephoneNum, 102292,$value1,$value2);
		     	}
		     	if($oMeetingDetail->order_type_id == 2){
		     	  	$oMeetingDetail->order_type_id =1;
					$oMeetingDetail ->save();
					$oMeetingmess->meeting_type_id = 0;
					$oMeetingmess->save();
					$oMeetingOtherOrder = MeetingOrder::where('meeting_id',$id)->where('order_user_id','!=',$iUid)->get();
					if(count($oMeetingOtherOrder)){
					   foreach($oMeetingOtherOrder as $value){
					      $value -> order_type_id = 0;
					      $value ->save();
					   }
					}
		     	}else{
		     	   $oMeetingDetail->order_type_id =1;
				   $oMeetingDetail ->save();
		     	}
				/*$oMeetingDetail->order_type_id =1;
				$oMeetingDetail ->save();
				$oMeetingOtherOrder = MeetingOrder::where('meeting_id',$id)->where('order_user_id','!=',$iUid)->get();
				if(count($oMeetingOtherOrder)){
				   foreach($oMeetingOtherOrder as $value){
				      $value -> order_type_id = 0;
				      $value ->save();
				   }
				}*/
			    if($oMeetingmess -> order_count >=1){
					$oMeetingmess ->order_count -=1;  
					$oMeetingmess ->save();
			    }else{
					$oMeetingmess ->order_count = 0;  
					$oMeetingmess ->save();
			    }
		       	return 'success';
		 	}
		}else{
			echo 'error';die; 
		}
	}

	//预约会议列表观看会议 
	public function WatchMeeting($iId)
	{
		$iUid = Session::get('userid');
		$iMeetingId = $iId;
		$iUserType = 1;
		$oMeetingEnterLog = new MeetingEnterLog;
		$oMeetingEnterLog -> user_id = $iUid;
		$oMeetingEnterLog -> meeting_id = $iMeetingId;
		$oMeetingEnterLog -> user_type = $iUserType;
		$oMeetingEnterLog -> save();
		$oMeetingApiLog = MeetingApiLog::where('meeting_id',$iMeetingId)->first();
		if(count($oMeetingApiLog)){
			$oJoinMeetingUrl = $oMeetingApiLog -> joinurl;
			return Redirect::to($oJoinMeetingUrl);
		}else{
			return Redirect::to('/meeting/has-appointed-meeting');
		}
	}

}