<?php

use Illuminate\Support\Facades\Redirect;

class DocFaceController extends BaseController {

	CONST  PAGESIZE = 9;
	CONST  CATID = 1;
    public $iCacheTime;
    private $iUserId = 0;
    private $iRoleId = 0;


	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	public function __construct(){
		$this->iCacheTime = 300; //5小时
		$this->iUserId = Session::get('userid');
		$this->iRoleId = Session::get('roleid');
	}

	
	public function Index(){
		//取名医面对面精彩预告最新的一条
		$oFaceVideoAdvance = FaceVideo::getLatestFaceVideoAdvance();
		if($oFaceVideoAdvance){
			$oFaceVideoAdvance1 = FaceVideo::getLatestFaceVideoAdvance($oFaceVideoAdvance->id);
			if(!$oFaceVideoAdvance1){
				$oFaceVideoAdvance1 = $oFaceVideoAdvance;
			}
		}else{
			$oFaceVideoReview = FaceVideo::getLatestFaceVideoReview(1); 
			$oFaceVideoAdvance1='';
		}
		//取名医面对面精彩回顾最新的三条(加入redis操作)
		$iCount = 3;
		$sFaceVideoRewiewRedis = 'faceVideoRewiewRedis';
		// if (Cache::has($sFaceVideoRewiewRedis)){
		//     $oFaceVideoReview = Cache::get($sFaceVideoRewiewRedis);
		// }else{
	     	$oFaceVideoReview = FaceVideo::getLatestFaceVideoReview($iCount);
		// 	Cache::put($sFaceVideoRewiewRedis,$oFaceVideoReview,$this->iCacheTime);
		// }
		//专家风采(加入redis操作)
		$iCountDoc = 5;
		$sDocsRedis = 'doctorsRedis';
	 	// if (Cache::has($sDocsRedis)){
		//     $oDocs = Cache::get($sDocsRedis);
		// }else{
	     	$oDocs = FaceDoc::findAllDoc($iCountDoc);
	 	//     	Cache::put($sDocsRedis,$oDocs,$this->iCacheTime);
		// }
		//病例征集列表
		/*$caseList = CaseInfo::where('status','=',2)
			 ->where('voteable','=',1)
			 ->OrderByCreatedAt()
			 ->paginate(6)->toArray();*/
		return View::make('front.docface.index')
			->with('oFaceVideoAdvance',$oFaceVideoAdvance)
			->with('oFaceVideoReview',$oFaceVideoReview)
			->with('oDocs',$oDocs)
			->with('oFaceVideoAdvance1',$oFaceVideoAdvance1);
			// ->with('caseList',$caseList);
	}
	
	//进入直播
	public function EnterLive($iId){
		//观看直播时间存入数据库
		$oFaceVideo = FaceVideo::find($iId);
		if($oFaceVideo->video_permission==1 && $this->iRoleId!=2){
		//if($oFaceVideo->video_permission==1 && $this->iRoleId != 2){
			return Redirect::to('/');
		}
		//点击量+1
		$oFaceVideo->increment('video_hits');
		$iEndTime = strtotime($oFaceVideo->end_time);
		$iStartTime = strtotime($oFaceVideo->start_time);
		$iNowTime = time();
		if(!($iEndTime>$iNowTime&&$iNowTime>($iStartTime-20*60))){		//如果不是直播正在进行中
			return Redirect::to('/');
		}
		//数据库该直播该用户已经观看的时间存数据库、
		$oVideoLog = VideoLog::HasLog(self::CATID, $iId, 2, $this->iUserId, 0);
		$sUserName = User::where('id',$this->iUserId)->pluck('user_name');
		$sUserthumb = User::where('id',$this->iUserId)->pluck('user_thumb');
		$url = 'http://api.mudu.tv/v1/activities/'.$oFaceVideo->video_id;
		$header = array();
	    $header[] = "Authorization:Bearer teqnwp95zey37n97zquzhyr01joxo9pp";
	    $header[] = 'Content-Type:application/json';
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    $response = curl_exec($ch);
	    curl_close($ch);
	    $videodetail = json_decode($response,true);
		$videoname = $videodetail['name'];
		$videopic = $videodetail['page']['cover_image'];
		return View::make('front.docface.liveshow')
			->with('sUserName',$sUserName)
			->with('sUserthumb',$sUserthumb)
			->with('videopic',$videopic)
			->with('videoname',$videoname)
			->with('iUserId',$this->iUserId)
			->with('oFaceVideo',$oFaceVideo);
	}

	//精彩回顾列表
	public function Review($iDepartmentId = 0){
		if(!$this->iUserId || $this->iRoleId!=2){
		//if(!$this->iUserId || $this->iRoleId != 2){
			$oVideoReview = FaceVideo::select('face_video.*','face_doc.doc_name','face_doc.doc_thumb','face_doc.doc_position','face_doc.doc_department','face_doc.doc_hospital','face_doc.doc_introduction')
				->leftJoin('face_doc','face_video.doc_id','=','face_doc.id')
				->where('video_type',1)->where('video_permission','0');
		}else{
			$oVideoReview = FaceVideo::select('face_video.*','face_doc.doc_name','face_doc.doc_thumb','face_doc.doc_position','face_doc.doc_department','face_doc.doc_hospital','face_doc.doc_introduction')
				->leftJoin('face_doc','face_video.doc_id','=','face_doc.id')
				->where('video_type',1);
		}
		if($iDepartmentId){
			$oVideoReview = $oVideoReview->where('department_id','LIKE',"%".$iDepartmentId."|%");
		}
		$oVideoReview = $oVideoReview->OrderByCreatedAt()->paginate(self::PAGESIZE);
		return View::make('front.docface.reviewlist')
			->with('oVideoReview',$oVideoReview)
			->with('iDepartmentId',$iDepartmentId);
	}
	
	//精彩回顾详细
	public function ReviewShow($iId){
		$oReview = FaceVideo::getFaceVideoReviewById($iId);
		//点击量+1
		$oReview->increment('video_hits');
		//获取星级评分
		$oScoreLog = FaceVideoScore::where('face_video_id',$iId)->where('user_id',$this->iUserId)->first();
		//获取所有评论
		$iCatId = 1;
		$oComments = Comment::getComment($iCatId,$iId);
		$sImgLi = Images();
		return View::make('front.docface.reviewshow')
			->with('iUserId',$this->iUserId)
			->with('oReview',$oReview)
			->with('oScoreLog',$oScoreLog)
			->with('oComments',$oComments)
			->with('sImgLi',$sImgLi);
	}

	//精品课堂列表
	public function Recview($iDepartmentId = 0){
		if(!$this->iUserId || $this->iRoleId!=2){
		//if(!$this->iUserId || $this->iRoleId != 2){
			$oVideoReview = FaceVideo::select('face_video.*','face_doc.doc_name','face_doc.doc_thumb','face_doc.doc_position','face_doc.doc_department','face_doc.doc_hospital','face_doc.doc_introduction')
				->leftJoin('face_doc','face_video.doc_id','=','face_doc.id')
				->where('video_rec',1)->where('video_permission','0');
		}else{
			$oVideoReview = FaceVideo::select('face_video.*','face_doc.doc_name','face_doc.doc_thumb','face_doc.doc_position','face_doc.doc_department','face_doc.doc_hospital','face_doc.doc_introduction')
				->leftJoin('face_doc','face_video.doc_id','=','face_doc.id')
				->where('video_rec',1);
		}
		if($iDepartmentId){
			$oVideoReview = $oVideoReview->where('department_id','LIKE',"%".$iDepartmentId."|%");
		}
		$oVideoReview = $oVideoReview->OrderByCreatedAt()->paginate(self::PAGESIZE);
		return View::make('front.docface.recviewlist')
			->with('oVideoReview',$oVideoReview)
			->with('iDepartmentId',$iDepartmentId);
	}
	
	//精品课程详细
	public function RecviewShow($iId){
		$oReview = FaceVideo::getFaceVideoReviewById($iId);
		//点击量+1
		$oReview->increment('video_hits');
		//获取星级评分
		$oScoreLog = FaceVideoScore::where('face_video_id',$iId)->where('user_id',$this->iUserId)->first();
		//获取所有评论
		$iCatId = 1;
		$oComments = Comment::getComment($iCatId,$iId);
		$sImgLi = Images();

		//精品课程弹窗是否显示============================
		$Configs = new Configs();
		$configRs = $Configs::find(1)->toArray();
		$aVideo = $oReview->toArray();
		$aVideo['buyTips'] = 0;
		$aVideo['buyTipsTxt'] = "观看此视频需消耗{$configRs['view_video_deduct_integral']}积分，是否观看？";
		if($configRs['view_video_deduct_integral']){
			//获取积分日志列表
			$IntegralLog = new IntegralLog();
			$purchased = IntegralLog::where('user_id',$this->iUserId)->
				where('obj_type','=','video')->
				where('obj_id','=',$iId)->first();
			if(!$purchased){
				$aVideo['buyTips'] = 1;
			}
		}
		//插入健康教育视频观看日志
		$VideoViewLog = new VideoViewLog();
		$vVlog = array(
			'user_id'=>$this->iUserId, 'video_id'=>$iId, 'video_title'=>$aVideo['video_title'], 'video_thumb'=>$aVideo['video_thumb']
		);
		$VideoViewLog->addItem($vVlog)->save();
		return View::make('front.docface.recviewshow')
			->with('iUserId',$this->iUserId)
			->with('iId',$iId)
			->with('buyTips',$aVideo['buyTips'])
			->with('buyTipsTxt',$aVideo['buyTipsTxt'])
			->with('oReview',$oReview)
			->with('oScoreLog',$oScoreLog)
			->with('oComments',$oComments)
			->with('sImgLi',$sImgLi);
	}
	
	//评论
	public function CommentInfo($iId,$iCatId,$iDevice = 0){
		$sComment=Input::get('sComment');
		$aComment=array(
			'user_id' => $this->iUserId,
			'cat_id' => $iCatId,
			'source_id' => $iId,
			'comment' => trim($sComment),
			'device_type'=>$iDevice
		);
		$oComment = new Comment($aComment);
		$oComment->save();
		return 'success';
	}

	//评论点赞
	public function CommentZan($comment_id,$cat_id=1){
		$oComment = Comment::find($comment_id);
		if(!$oComment){
			return json_encode(array('success'=>false,'msg'=>'该评论不存在'));
		}
		$oCommentZanLog = CommentZanLog::getCommentZanLog($comment_id,$this->iUserId);
		//$oCommentZanLog = CommentZanLog::getCommentZanLog($comment_id,$this->iUserId);
		if($oCommentZanLog){
			return json_encode(array('success'=>false,'msg'=>'您已经支持过了'));
		}
		$oCommentZanLog = new CommentZanLog(array('comment_id'=>$comment_id,'user_id'=>$this->iUserId,'cat_id'=>$cat_id));
		//$oCommentZanLog = new CommentZanLog(array('comment_id'=>$comment_id,'user_id'=>$this->iUserId,'cat_id'=>$cat_id));
		$oCommentZanLog->save();
		$oComment->increment('zan_count');
		return json_encode(array('success'=>true,'count'=>++$oComment->zan_count));
	}
	
	//精彩回顾点赞
	public function VideoZan($iVideoId,$iUserId,$iCatId){
		//该用户是否已经给该视频点赞
		$isZan = FaceVideoZanLog::isUserZan($iVideoId,$iUserId,$iCatId);
		if($isZan){
			echo 'is_zan';die;
		}		
		//给当前视频点赞
		FaceVideoZanLog::UserZan($iVideoId,$iUserId,$iCatId);
		echo 'success';die;
	}

	//精彩回顾星级评分
	public function LiveScore(){
		$iVideoId = Input::get('videoid',0);
		$oVideo = FaceVideo::find($iVideoId);
		if(!$oVideo){ echo 'error';die; }
		$iScore1 = Input::get('score1',0);
		$iScore2 = Input::get('score2',0);
		$iScore3 = Input::get('score3',0);
		$iScore4 = Input::get('score4',0);
		$iScore5 = Input::get('score5',0);
		$iScore6 = Input::get('score6',0);
		$iDevice = Input::get('device',0);
		FaceLiveScore::addScoreLog($iVideoId,Input::get('userid'),$iScore1,$iScore2,$iScore3,$iScore4,$iScore5,$iScore6,$iDevice);
		echo 'success';die;
	}

	//精彩回顾星级评分
	public function VideoScore(){
		$iVideoId = Input::get('videoid',0);
		$oVideo = FaceVideo::find($iVideoId);
		if(!$oVideo){ echo 'error';die; }
		$iScore1 = Input::get('score1',0);
		$iScore2 = Input::get('score2',0);
		$iScore3 = Input::get('score3',0);
		$iDevice = Input::get('device',0);
		FaceVideoScore::addScoreLog($iVideoId,Input::get('userid'),$iScore1,$iScore2,$iScore3,$iDevice);
		echo 'success';die;
	}

	//观看录播积分操作
	public function VideoView($iVideoId,$iUserId,$iMin,$sDevice='web'){
		//操作
		$creditIN = CreditController::getInstance($iVideoId,$iMin,$this->iUserId,$sDevice);
		$creditIN->optCredit();
		if($iMin>=20){
			//详细记录用户观看该录播的情况
			VideoReviewLog::addUserReviewLog($iVideoId,$iUserId,$iMin);
		}
		echo 'success';die;
	}
	
	public function VideoViewLive($iVideoId,$iUserId,$iTimeStamp,$sDevice='web'){
		$oFaceVideo = FaceVideo::find($iVideoId);
		if($oFaceVideo->video_type=='2'){
			$iNowTime = time();
			$iStartTime = strtotime($oFaceVideo->start_time);
			if($iNowTime>$iStartTime){	
				$iAddMin = 3;//修改为3分钟加3分，不考虑多设备登录观看
				//FaceVideo::addUserScoreAndViewLog($iVideoId,$iAddMin,Auth::User()->id);
				//$creditIN = CreditController::getInstance($iVideoId,$iAddMin,Auth::User()->id);
				$creditIN = CreditController::getInstance($iVideoId,$iAddMin,$this->iUserId,$sDevice);
				$creditIN->optCredit();
			}
			
		}
		return 'success';die;
	}
	
	//专家风采列表
	public function DocList(){
		$oDoc = FaceDoc::where('id','!=',17)->OrderByCreatedAt()->paginate(self::PAGESIZE);
		return View::make('front.docface.doclist')->with('oDoc',$oDoc);
	}
	
	//专家风采详细
	public function DocShow($iId){
		$oDoc = FaceDoc::find($iId);
		//获取相关课堂
		$oDocFaceVideo = FaceVideo::findVideoByDocId($iId)->paginate(6);
		return View::make('front.docface.docshow')
			->with('oDoc',$oDoc)
			->with('oDocFaceVideo',$oDocFaceVideo);
	}
	
	public function SurveyPre($id){
		// Auth::logout();
		if(Session::has('cdma_user_id')){
			Session::forget('cdma_user_id');
		}
		return Redirect::to('/docface/survey/'.$id);
	}
	
	/**
	 * 调研功能
	 * 高红叶 2016-11-14备注
	 * id>100000的签到为线下会议的签到，区别于之前的签到逻辑
	 * @param unknown $iId
	 */
	public function Survey($iId){
		//调研id存session
		if (Session::has('survey_video_id'))
		{
		    Session::forget('survey_video_id');
		}
		Session::put('survey_video_id',$iId);
		$oFaceVideo = FaceVideo::find($iId);
		if($oFaceVideo){
			if(!Session::has('cdma_user_id')){
				//本期直播签到是否结束
				$iVideoEndTime = strtotime($oFaceVideo->end_time);
				//加直播积分和加录播积分中间时间戳
				$iMiddleTimestamp = $iVideoEndTime+20*60;
				$iEndFlag = 0;
				if(time()>=$iMiddleTimestamp&&!strstr($oFaceVideo->video_url, 'upload')){
					$iEndFlag = 1;
				}
				return View::make('front.survey.login')->with('iEndFlag',$iEndFlag);
			}
			//用户已登录
			//$oFaceVideo = FaceVideo::find($iId);
			$oQuestions = $oFaceVideo->SurveyQuestion()->orderBy('listorder','ASC')->get();
			foreach($oQuestions as $k=>$v){
				$oOptions = SurveyQuestion::find($v->id)->SurveyQuestionOption()->orderBy('option','ASC')->get();
				$oQuestions[$k]->options = $oOptions;
			}
			$sVideoTitle = $oFaceVideo->video_title;
			return View::make('front.survey.index')
				->with('oQuestions',$oQuestions)
				->with('iVid',$iId)
				->with('sVideoTitle',$sVideoTitle);
		}elseif($iId>100000){
			$oUser = Auth::User();
			if(!$oUser){
				//未登录
				return View::make('front.survey.offline')->with('haslogin',0);
			}
			//用户已登录
			$iVideoId = $iId;
			//默认为录播签到
			$iVideoType = '1';
			//查看用户是否签到
			$oSign = SignInLog::where('cat_id',1)
				->where('user_id',$oUser->id)
				->where('video_id',$iVideoId)
				->where('video_type',$iVideoType)
				->first();
			//是否重复签到
			$iRepeat = 0;
			if($oSign){
				$iRepeat = 1;
			}else{
				//新增签到记录
				//签到记录
				$aSign = array(
					'user_id'		=> $oUser->id,
					'video_id'		=> $iVideoId,
					'organizer_id'	=> 0,
					'video_type'	=> $iVideoType
				);
				$oSign = new SignInLog($aSign);
				$oSign->save();
			}
			return View::make('front.survey.offline')
				->with('iRepeat',$iRepeat)
				->with('haslogin',1);
		}else{
			echo "无会议信息";
		}
	}
	
	//调研提交
	public function SurveyDo($iId){
		$input = Input::all();
		if(!isset($input['surveys'])){
			echo 'noanswer';die;
		}
		$iCountSurvey = $input['survey_count'];
		$aSurvey = $input['surveys'];
		if(!$aSurvey||count($aSurvey)<$iCountSurvey){
			echo 'noanswer';die;
		}
		$iUId = Session::get('cdma_user_id');
		foreach ($aSurvey as $k=>$v){
			//该用户是否参加过该调研
			$count = SurveyQuestionAnswer::where('user_id',$iUId)->where('survey_question_id',$k)->count();
			if($count){
				echo 'repeat';die;
			}else{
				//获得用户答案
				$sAnswer = '';
				if(is_array($v)){
					foreach($v as $val){
						if($sAnswer){
							$sAnswer .= $val;
						}else{
							$sAnswer .= $val;
						}
					}
				}else{
					$sAnswer = $v;
				}
				//新增用户答题记录
				$aSurveyAnswer = array(
					'user_id'				=> $iUId,
					'survey_question_id'	=> $k,
					'option'				=> $sAnswer,
					'created_at'			=> date('Y-m-d H:i:s')
				);
				$oSurveyQuestionAnswer = new SurveyQuestionAnswer($aSurveyAnswer);
				$oSurveyQuestionAnswer->save();
			}
		}
		echo 'success';
		die;
	}

	/**
	 * 新增(非目标医生)线下会议签到，不限次数，不计会议id，依据签到时间判断（原逻辑废弃） 20170502  dll
	 * /docface/offline-sign
	 */
	public function OfflineSign($iMeetingCode){
		return View::make('front.survey.offlinesign')->with('iMeetingCode',$iMeetingCode);
	}

	public function OfflineSignDo(){
		$sSignName = Input::get('sign_name');
		$iMeetingCode = Input::get('meeting_code');
		$iUserId = User::where('user_tel',$sSignName)->pluck('id');
		if(isset($iUserId) && $iUserId != 0){
			$aSign = array(
				'user_id'	 => $iUserId,
				'meeting_code'=>$iMeetingCode,
				'created_at' => date('Y-m-d H:i:s',time())
			);
			$oOfflinesignInLogExists = OfflineSignInLog::where('meeting_code',$iMeetingCode)->where('user_id',$iUserId)->first();
			if(!count($oOfflinesignInLogExists)){
				$oSign = new OfflineSignInLog($aSign);
				$oSign->save();
			}
			echo 'success';die;
		}else{
			echo 'nouser';die;
		}
	}

}