<?php 

class ApiConsultationController extends BaseController
{
	
	/*
	 * 获取用户id
	 */
	public function getUserId(){
		$token = Input::get('token');
		if(!$token){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$token = Crypt::decrypt($token);
		$aUserinfo = explode('\t',$token);
		$iUid = $aUserinfo[0];
		// $iUid = 2;
		return $iUid;
	}

	public function getList(){//获取初始的直播、预告、回顾
		$iUid = $this->getUserId();
		$Consultation1 = Consultation::getLatestConsultation(1);
		$oTopic = null;
		$iRegFlag = 0;//当前用户积分排行榜前一百，不能报名
		//获取积分排行榜前一百医生
		$oUser = User::getUserScoreListObj(3);
		foreach($oUser as $k=>$v){
			if($v->id==$iUid){
				$iRegFlag = 1;//当前用户在积分排行榜前100，未报名
				break;
			}
		}
		if($iRegFlag==1&& count($Consultation1)>0){
			//判断当前用户是否已经报名
			$oReg = ConsultationUserReg::where('meeting_id',$Consultation1->id)->where('user_id',$iUid)->first();
			if($oReg){
				$iRegFlag = 2;//当前用户在积分排行榜前100，已报名
			}
		}
		if(count($Consultation1)>0){
			if(time()<strtotime($Consultation1->reg_starttime)||time()>strtotime($Consultation1->reg_endtime)){
				$iRegFlag = -1;
			}
		}
		if($iRegFlag<=0){
			//判断当前用户如果是代表，置为-2
			$oUser = User::find($iUid);
			if($oUser->role_id==2||$oUser->role_id==1){
				$iRegFlag = -2;
			}
		}
		if(count($Consultation1)>0){
			$oTopic = ConsultationTopic::where('meeting_id',$Consultation1->id)->get();
		}
		$Consultation2 = Consultation::getLatestConsultation(2);
		$Consultation3 = Consultation::getLatestConsultation(3);
		$Consultation5 = Consultation::getLatestConsultation(5);
		$Consultation4 = Consultation::findAllVideoCount(4,999);
		$aConsultation1 = array();
		$aConsultation2 = array();
		$aConsultation3 = array();
		$aConsultation5 = array();
		$aConsultation4 = array();
		$aTopic = array();
		if(count($Consultation1)>0){
			$aConsultation1 = array(
				'id'				=> $Consultation1->id,
				'doc_name'			=> $Consultation1->doc_name,
				'meeting_title'		=> $Consultation1->meeting_title,
				'meeting_time'		=> substr($Consultation1->meeting_starttime,0,16).'-'. substr($Consultation1->meeting_endtime,11,5),
				'meeting_starttime'	=> strtotime($Consultation1->meeting_starttime),
				'meeting_endtime'	=> strtotime($Consultation1->meeting_endtime),
				'meeting_url'		=> $Consultation1->meeting_url,			//观看直播
				'meeting_video_url'	=> $Consultation1->meeting_video_url,	//参与直播
				'reg_starttime'		=> substr($Consultation1->reg_starttime,0,16),
				'reg_endtime'		=> substr($Consultation1->reg_endtime,0,16),
				'reg_num'			=> $Consultation1->reg_num,
				'reg_setmax_nums'	=> $Consultation1->reg_setmax_nums,
				'reg_flag'			=> $iRegFlag		//-2:代表不能报名；-1:未在报名时间范围内；0:医生积分不在前100不能报名；1：可以报名，未报名，2：已报名
			);
			foreach($oTopic as $k=>$v){
				$aTopic[] = array(
					'id'			=> $v->id,
					'topic_title'	=> $v->topic_title
				);
			}
			if ($aConsultation1){
				$data['aConsultation1'] = $aConsultation1;
			}
		}
		if(count($Consultation2)>0){
			$aConsultation2 = array(
				'id'				=> $Consultation2->id,
				'doc_name'			=> $Consultation2->doc_name,
				'meeting_title'		=> $Consultation2->meeting_title,
				'meeting_time'		=> substr($Consultation2->meeting_starttime,0,16).'-'. substr($Consultation2->meeting_endtime,11,5),
				'meeting_starttime'	=> strtotime($Consultation2->meeting_starttime),
				'meeting_endtime'	=> strtotime($Consultation2->meeting_endtime),
				'meeting_url'		=> $Consultation2->meeting_url,			//观看直播
				'meeting_video_url'	=> $Consultation2->meeting_video_url,	//参与直播
			);
		    if ($aConsultation2){
				$data['aConsultation2']=$aConsultation2;
			}
		}
		if(count($Consultation3)>0){
			$aConsultation3 = array(
				'id'				=> $Consultation3->id,
				'doc_name'			=> $Consultation3->doc_name,
				'meeting_title'		=> $Consultation3->meeting_title,
				'meeting_time'		=> substr($Consultation3->meeting_starttime,0,16).'-'. substr($Consultation3->meeting_endtime,11,5),
				'meeting_starttime'	=> strtotime($Consultation3->meeting_starttime),
				'meeting_endtime'	=> strtotime($Consultation3->meeting_endtime),
				'meeting_url'		=> $Consultation3->meeting_url,			//观看直播
				'meeting_video_url'	=> $Consultation3->meeting_video_url,	//参与直播
			);
		    if ($aConsultation3){
				$data['aConsultation3']=$aConsultation3;
			}
		}
		if(count($Consultation5)>0){
			$aConsultation5 = array(
				'id'				=> $Consultation5->id,
				'doc_name'			=> $Consultation5->doc_name,
				'meeting_title'		=> $Consultation5->meeting_title,
				'meeting_time'		=> substr($Consultation5->meeting_starttime,0,16).'-'. substr($Consultation5->meeting_endtime,11,5),
				'meeting_starttime'	=> strtotime($Consultation5->meeting_starttime),
				'meeting_endtime'	=> strtotime($Consultation5->meeting_endtime),
				'meeting_url'		=> $Consultation5->meeting_url,			//观看直播
				'meeting_video_url'	=> $Consultation5->meeting_video_url,	//参与直播
			);
		    if ($aConsultation5){
				$data['aConsultation5']=$aConsultation5;
			}
		}
		if(count($Consultation4)>0){
			foreach($Consultation4 as $k=>$v){
				$aConsultation4[] = array(
					'id'			=> $v->id,
					'meeting_title'	=> $v->meeting_title,	
					'doc_name'		=> $v->doc_name,
					'doc_hospital'	=> $v->doc_hospital,
				);
			}
		}
		$data['aConsultation4']=$aConsultation4;
		$data['aTopic']=$aTopic;
		/*$data = array(
			'aConsultation4'	=> $aConsultation4,
			'aTopic'			=> $aTopic
		);*/
		echo $this->formatRet(true,"成功","success",$data);exit;
	}
	
	//跨领域学术交流报名页面
	public function getApply(){
		$iId = Input::get('videoid');
		$oTopic = ConsultationTopic::where('meeting_id',$iId)->get();
		foreach($oTopic as $k=>$v){
			$aTopic[$k+1] = array(
				'id'			=> $v->id,
				'topic_title'	=> $v->topic_title
			);
		}
		$data = $aTopic;
		echo $this->formatRet(true,"成功","success",$data);exit;
	}
	
	/*
	 * 跨领域学术交流报名提交
	 * http://cdma.local/apicon/apply-do?videoid=3&topic[18]=123&topic[19]=234
	 */
	public function postApplyDo(){
		$iConId = Input::get('videoid');
		$iUid = $this->getUserId();
		$input = Input::all();
		if(!$iConId){
			echo  $this->formatRet(false,'视频不存在');exit;
		}
		//判断当前用户是否已经报名
		$oReg = ConsultationUserReg::where('meeting_id',$iConId)->where('user_id',$iUid)->first();
		if($oReg){
			echo  $this->formatRet(false,'您已经报名，无需重复报名');exit;
		}
		if(!isset($input['topic'])){
			echo  $this->formatRet(false,'请您对本期话题任选2-3个提出您的观点进行阐述');exit;
		}
		$aTopic = $input['topic'];
		$count = 0;
		foreach($aTopic as $k=>$v){
			if(trim($v)!=''){
				$count++;
			}
		}
		if($count<2){
			echo  $this->formatRet(false,'请您对本期话题任选2-3个提出您的观点进行阐述');exit;
		}
		$oConsultation = Consultation::find($iConId);
		if($oConsultation->reg_num>=$oConsultation->reg_setmax_nums){
			echo  $this->formatRet(false,'报名人数已满，下次再来吧');exit;
		}
		//保存报名信息
		$aUserReg = array(
			'meeting_id'	=> $iConId,
			'user_id'		=> $iUid,
			'reg_time'		=> date('Y-m-d H:i:s')
		);
		$oUserReg = new ConsultationUserReg($aUserReg);
		$oUserReg->save();
		//保存观点
		foreach($aTopic as $k=>$v){
			if(trim($v)){
				//阐述观点了，存数据库
				$aTopicAnswer = array(
					'meeting_id'	=> $iConId,
					'user_id'		=> $iUid,	
					'topic_id'		=> $k,
					'user_answer'	=> $v
				);
				$oTopicAnswer = new ConsultationTopicAnswer($aTopicAnswer);
				$oTopicAnswer->save();
			}
		}
		//报名人数加1
		$oConsultation->increment('reg_num');
		echo $this->formatRet(true,"成功","success");exit;
	}
	
	/*
	 * 观看直播，参与直播
	 */
	public function getEnter(){		//type为1表示观看直播,为2表示参与直播
		$iUid = $this->getUserId();
		$iVideoId = Input::get('videoid');
		$iType = Input::has('type')?Input::get('type'):1;
		$sCode = Input::has('code')?Input::get('code'):'';
		$oConsultation = Consultation::find($iVideoId);
		if(!$oConsultation){
			return $this->showMessage('该直播不存在');
		}
		if(time()>strtotime($oConsultation->meeting_endtime)){
			echo  $this->formatRet(false,'该直播已结束');exit;
		}
		if(time()<strtotime($oConsultation->meeting_starttime)){
			echo  $this->formatRet(false,'该直播尚未开始');exit;
		}
		if($iType==2){
			//参与直播，查看邀请码是否输入正确
			if(strtoupper($oConsultation->meeting_code)!=strtoupper($sCode)){
				echo  $this->formatRet(false,'邀请码输入有误，请您重新输入');exit;
			}
		}
		//存log
		$aLog = array(
			'consultation_id'	=> $iVideoId,
			'user_id'			=> $iUid,
			'enter_type'		=> $iType,
			'device_type'		=> 2
		);
		$oLog = new ConsultationEnterLog($aLog);
		$oLog->save();
		if($iType==1){
			$url = 'meeting_url';
			$oUser = User::find($iUid);
			$sUrl = $oConsultation->$url.'?nickName='.urlencode($oUser->user_nick);
		}else{
			$url = 'meeting_video_url';
			$sUrl = $oConsultation->$url;
		}
		$data = array('url'=>$sUrl);
		echo $this->formatRet(true,"成功","success",$data);exit;
	}
	
	/*
	 * 录播详细页
	 */
	public function getShow(){//详细页
		$iUid = $this->getUserId();
		$iVideoid = Input::get('videoid');
		$oVideo = Consultation::getShowById($iVideoid);
		$oZanlog = FaceVideoZanLog::selzanlog($iVideoid,2)->get();
		$iZancount = count($oZanlog);
		$iszan = false;
		if($iZancount>0){
			foreach($oZanlog as $v){
				if($v->user_id == $iUid){
					$iszan = true;
				}
			}
		}
		if(count($oVideo)){
			$oDoc = $oVideo->doc_info;
			$aVideo = array(
				'video_title' => $oVideo->meeting_title,
				'video_url' => Config::get('app.url').$oVideo->meeting_url,
				'video_introduce' => $oVideo->meeting_description,
				'zancount'=>$iZancount,
				'iszan'=>$iszan,
				'start_time' => $oVideo->meeting_starttime,
				'end_time' => $oVideo->meeting_endtime,
				'doc_name' => $oDoc->doc_name,
				'doc_thumb' => Config::get('app.url').$oDoc->doc_thumb,
				'doc_position' => $oDoc->doc_position,
				'doc_department' => $oDoc->doc_department,
				'doc_hospital' => $oDoc->doc_hospital,
				'doc_introduction' => $oDoc->doc_introduction
			);
			echo  $this->formatRet(true,"成功","success",$aVideo);exit;
		}else{
			echo  $this->formatRet(false,'视频不存在');exit;
		}
	}
	
	public function getCommentList(){
		$iVideoid = Input::get('videoid');
		$iCatId = Input::get('catid',2);
		$oComments = Comment::getComment($iCatId,$iVideoid);
		$aComments = array();
		foreach($oComments as $obj){
			$aComments[] = array(
				'id'		=> $obj->id,
				'user_nick' => $obj->user_nick,
				'user_thumb' => $obj->user_thumb,
				'comment' => str_replace('/assets/images/front/emotion/',Config::get('config.rooturl').'/assets/images/front/emotion/',$obj->comment),
				'zan_count' =>$obj->zan_count,
				'device_type' => $obj->device_type,
				'created_at' => $obj->created_at
			);
		}
		echo  $this->formatRet(true,'获取记录成功','success',$aComments);exit;
	}

	public function postComment(){
		$iVideoid = Input::get('videoid');
		$iUid = $this->getUserId();
		$iCatId = Input::get('catid',2);
		$aComment = array(
			'user_id'=>$iUid,
			'cat_id' => $iCatId,
			'source_id'=>$iVideoid,
			'comment' => Input::get('comment'),
			'device_type'=> 1,
			'created_at' => date('Y-m-d H:i:s',time())
		);
		$oComment = new Comment($aComment);
		$oComment->save();
		echo  $this->formatRet(true,'评论成功');exit;
	}
	
	public function postSupport(){
		$iVideoid = Input::get('videoid');
		$iUid = $this->getUserId();
		$iCatId = Input::get('catid',2);
		if(FaceVideoZanLog::isUserZan($iVideoid, $iUid, $iCatId)){
			//已赞
			echo  $this->formatRet(false,'您已支持过该视频，无需重复支持！');exit;
		}
		$oZanlog = FaceVideoZanLog::UserZan($iVideoid, $iUid,$iCatId);
		echo  $this->formatRet(true,'支持成功');exit;
	}
	
	public function postCommentZan(){
		$iCommentId = Input::get('commentid');
		$iUid = $this->getUserId();
		$oComment = Comment::find($iCommentId);
		if(!$oComment){
			echo  $this->formatRet(false,'该评论不存在');exit;
		}
		$isZan = CommentZanLog::getCommentZanLog($iCommentId, $iUid);
		if($isZan){
			echo  $this->formatRet(false,'您已支持过该评论，无需重复支持！');exit;
		}
		$oCommentZanLog = new CommentZanLog(array('comment_id'=>$iCommentId,'user_id'=>$iUid,'cat_id'=>$oComment->cat_id));
		$oCommentZanLog->save();
		$oComment->increment('zan_count');
		echo  $this->formatRet(true,'支持成功');exit;
	}
	
}