<?php

class AdmCaseController extends BaseController {
	
	CONST  PAGESIZE = 10;
	CONST  STATUS_W = '0';//未审核
	CONST  STATUS_Z = '1';//审核中
	CONST  STATUS_Y = '2';//通过
	CONST  STATUS_N = '-1';//未通过
	
	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	/*
	 * 病例征集-审核病例
	 */
	public function getCaseCheck($id){
		$oCaseInfo = CaseInfo::find($id);
		if($oCaseInfo['uploadType']==2){
			return View::make('admin.case.check2')
				->with('oCaseInfo',$oCaseInfo);
		}else{
			$picNum = 1;//记录附件图片数量，数量超过3则页面中不允许上传
			for($i=1;$i<4;$i++){
				if($oCaseInfo['sub_exam_fileUrl'.$i]){
					$picNum++;
				}
			}
			return View::make('admin.case.check1')
				->with('picNum',$picNum)
				->with('oCaseInfo',$oCaseInfo);
		}
	}

	public function change($id){
	   $oCaseInfo =  CaseInfo::find($id);
	   if($oCaseInfo ->voteable == 1){
	      $oCaseInfo ->voteable =0;
	   }else{
	      $oCaseInfo ->voteable =1;
	   }
	   $oCaseInfo->save();
	   return  Redirect::to("/admcase/list");
	}

	/*
	 * 病例征集-审核病例
	 */
	public function postCaseCheck($id){
		$input = Input::all();
		if(!isset($input['voteable'])){//参与投票
			$input['voteable'] = 0;
		}
		$case = CaseInfo::find($id);
		$iStatus = $case->status;
		if($case->uploadType == 1){
			foreach($input as $k=>$v){
				if(!in_array($k,array('sub_exam'))){
					$case->$k = addslashes($v);
				}
			}
			if($iStatus!=$case->status){
				$case->check_at = date('Y-m-d H:i:s');
			}
			$case->save();
		}else{
			$case->voteable = $input['voteable'];
			$case->status = $input['status'];
			$case->case_lightspot = $input['case_lightspot'];
			$case->nopass_reason = $input['nopass_reason'];
			if($iStatus!=$case->status){
				$case->check_at = date('Y-m-d H:i:s');
			}
			$case->save();
		}
		return self::getCaseCheck($id);
	}

	/*
	 * 病例征集-获取病例列表
	 */
	public function getCaseList(){
		//递归删除文件-下载病例时产生的临时文件
		$path = public_path().'/downZip';
		$this->deldir($path);
		$aLink = Input::all();
		$case = CaseInfo::OrderByCreatedAt();
		$input = Input::all();
		if(isset($input['downzip']) && $input['downzip']){
			return self::downZip($input);
			die;
		}
		if(isset($input['uploadType'])){
			if($input['uploadType']){
				$case = $case->where('uploadType','=',$input['uploadType']);
			}
		}
		if(isset($input['status'])){
			if($input['status'] !=4 ){
				$case = $case->where('status','=',$input['status']);
			}
			 
		}
		if(isset($input['voteable'])){
			if($input['voteable'] !=2 ){
				$case = $case->where('voteable','=',$input['voteable']);
			}
		}
		if(isset($input['case_name'])&&$input['case_name']){
			$case = $case->where('case_name','LIKE','%'.$input['case_name'].'%');
		}
		$oCase = $case->paginate(self::PAGESIZE);
		foreach($oCase  as $k=> $v){
			$oUserInfo = User::select('id','user_name','user_tel')->where('id',$v->userId)->first();
			$oCase[$k]->user_name = $oUserInfo->user_name;
			$oCase[$k]->user_tel = $oUserInfo->user_tel;
		}
		$statusArr = array('0'=>'未审核','1'=>'审核中','2'=>'审核通过','-1'=>'审核未通过');
		return View::make('admin.case.list')
			->with('oCase',$oCase)
			->with('aLink',$aLink)
			->with('statusArr',$statusArr);
	}

	//下载病例
	public function downloadCase($id){
		$oCaseInfo = CaseInfo::find($id);
		//导出内容后，未审核病例设置为审核中状态
		if($oCaseInfo['status']==0){
			$oCaseInfo['status'] = 1;
			$oCaseInfo->save();
		}
		$file =  public_path() .$oCaseInfo->fileUrl;
		$extend = explode("." , $file);
	    $ext = strtolower($extend[count($extend) - 1]);
		return Response::download( $file ,$oCaseInfo->case_name.'.'.$ext, array(
			"Content-Description" => "File Transfer",
			"Accept-Ranges"=>" bytes",
			"content-type" => "application/octet-stream"
		));
	}

	//现在所有病例zip包
	public function downZip($input){
		//获取符合条件的上传的文档
		$oCase = CaseInfo::where('uploadType','=','1');
		if(isset($input['status'])){
			if($input['status'] !=4 ){
				$oCase = $oCase->where('status','=',$input['status']);
			}
		}
		if(isset($input['voteable'])){
			if($input['voteable'] !=2 ){
				$oCase = $oCase->where('voteable','=',$input['voteable']);
			}
		}
		if(isset($input['case_name'])&&$input['case_name']){
			$oCase = $oCase->where('case_name','LIKE','%'.$input['case_name'].'%');
		}
		$oCase = $oCase->OrderByCreatedAt()->get();
		$now = date('YmdHis',time());
		$downZip = public_path().'/downZip/'.$now;
		$downZip = iconv('utf-8', 'gbk',$downZip);
		mkdirs($downZip);
		foreach($oCase as $k => $v){
			if(!empty($v) && file_exists(public_path().$v['fileUrl'])){
				//新路径	
				$path = public_path().'/downZip/'.$now.'/'.$v['userId'].'/'.preg_replace('/(:| |-)/','',$v['updated_at']);
				$path = iconv('utf-8', 'gbk',$path);
				mkdirs($path);
				//移动文件
				copy(public_path().$v['fileUrl'],$path.'/'.iconv('utf-8', 'gbk',$v['case_name']).'.'.get_filetype($v['fileUrl']));
				//导出内容后，未审核病例设置为审核中状态
				if($v['status']==0){
					$uCase = CaseInfo::find($v['id']);
					$uCase['status'] = 1;
					$uCase->save();
				}
			}
		}
		$zip = new PHPZip();
		$zip -> Zip('downZip/'.$now ,public_path().'/downZip/'.$now.'.zip'); //打包并下载 		
		$file =  public_path().'/downZip/'.$now.'.zip';
		return Response::download( $file ,'cdma-case.zip', array(
			"Content-Description" => "File Transfer",
			"Accept-Ranges"=>" bytes",
			"content-type" => "application/octet-stream"
		));
	}

	//递归删除目录
	public function deldir($path){  
        //给定的目录不是一个文件夹  
        if(!is_dir($path)){  
            return null;  
        }
        $fh = opendir($path);  
        while(($row = readdir($fh)) !== false){  
            //过滤掉虚拟目录  
            if($row == '.' || $row == '..'){  
                continue;  
            }  
            if(!is_dir($path.'/'.$row)){  
                unlink($path.'/'.$row);  
            }  
            $this->deldir($path.'/'.$row);  
        }
        //关闭目录句柄，否则出Permission denied  
        closedir($fh);  
        //删除文件之后再删除自身  
        if(!rmdir($path)){  
            echo $path.'无权限删除<br>';  
        }  
        //return true;  
    }

    //导出全部病例
    public function downAllCase(){
		/*实例化excel类*/
		$excel = new PHPExcel();
		/*实例化excel图片处理类*/
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		/*设置文本对齐方式*/
		$excel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objActSheet = $excel->getActiveSheet();
		$letter = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI');
		/*设置表头数据*/
		$tableheader = array(
			'cluster','大区','地区','代表CWID','代表姓名','医生姓名',
			'病例名称','上传者姓名','上传者手机号码', '医生姓名', '电子邮箱', '科室', '医院', '联系电话', '年龄', '性别',
			'主诉','现病史','既往史','实验室检查','辅助检查','诊断','治疗','随访',
			'化验单等辅助检查报告复印件','化验单等辅助检查报告复印件','化验单等辅助检查报告复印件', '病例的血压特点或疑难问题',
			'审核状态','审核时间','上传方式','病例亮点','审核未通过原因','投票数','上传时间'
		);
		/*填充表格表头*/
		$count = count($tableheader);
    	for($i = 0;$i < $count;$i++) {
			$excel->getActiveSheet()->setCellValue("$letter[$i]1","$tableheader[$i]");
		}
		/*设置表格数据*/
		$data = array();
		$fieldArr = array(
			'cluster','user_regin','user_area','rep_cwid','rep_name','doc_name',
			'case_name','user_name','user_tel','doctor_name','email','department','hospital','contact_num','patient_sex','patient_age',
			'self_reported','hpi','pmh','lab_exam_text','sub_exam_text','diagnose','cure','follow_view',
			'sub_exam_fileUrl1','sub_exam_fileUrl2','sub_exam_fileUrl3','dis_qustion',
			'status','check_at','uploadType','case_lightspot','nopass_reason','votes','created_at'
		);
		$case = CaseInfo::OrderByCreatedAt()->get();
		foreach($case  as $k=> $v){
			$oUserInfo = User::select('id','user_name','user_tel','role_id','user_cwid','user_regin','user_area','superior_ids')->where('id',$v->userId)->first();
			if(!$oUserInfo) continue;
			$case[$k]->user_name = $oUserInfo->user_name;
			$case[$k]->user_tel = $oUserInfo->user_tel;
			$iRoleId = $oUserInfo->role_id;
			$case[$k]->cluster = '';
			$case[$k]->user_regin = '';
			$case[$k]->user_area = '';
			$case[$k]->rep_cwid = '';
			$case[$k]->rep_name = '';
			$case[$k]->doc_name = '';
			if($iRoleId==3){	//医生
				$iRepId = User::getRepId($oUserInfo->superior_ids);
				if($iRepId){
					$oRepInfo = User::find($iRepId);
					$case[$k]->cluster = User::getClusterByReginId($oRepInfo->user_regin);
					$sR = $oRepInfo->user_regin;
					$sA = $oRepInfo->user_area;
					if(is_numeric($sR)){
						$sRegin = User::getReginCache();
						$oRegin = json_decode($sRegin);
						$oRepInfo->user_regin = $oRegin->$sR;
					}
					if(is_numeric($sA)){
						$sArea = User::getAreaCache();
						$oArea = json_decode($sArea);
						$oRepInfo->user_area = $oArea->$sR->$sA;
					}
					$case[$k]->user_regin = $oRepInfo->user_regin;
					$case[$k]->user_area = $oRepInfo->user_area;
					$case[$k]->rep_cwid = $oRepInfo->user_cwid;
					$case[$k]->rep_name = $oRepInfo->user_name;
					$case[$k]->doc_name = $oUserInfo->user_name;
				}
			}else{
				//代表
				$case[$k]->cluster = User::getClusterByReginId($oUserInfo->user_regin);
				$sR = $oUserInfo->user_regin;
				$sA = $oUserInfo->user_area;
				if(is_numeric($sR)){
					$sRegin = User::getReginCache();
					$oRegin = json_decode($sRegin);
					$oUserInfo->user_regin = $oRegin->$sR;
				}
				if(is_numeric($sA)){
					$sArea = User::getAreaCache();
					$oArea = json_decode($sArea);
					$oUserInfo->user_area = $oArea->$sR->$sA;
				}
				$case[$k]->user_regin = $oUserInfo->user_regin;
				$case[$k]->user_area = $oUserInfo->user_area;
				$case[$k]->rep_cwid = $oUserInfo->user_cwid;
				$case[$k]->rep_name = $oUserInfo->user_name;
				$case[$k]->doc_name = '';
			}
		}
		$statusArr = array('0'=>'未审核','1'=>'审核中','2'=>'审核通过','-1'=>'审核未通过');
		foreach($case as $key => $value){
			foreach($fieldArr as $k => $v){
				if($v == 'status'){
					$data[$key][$v] = $statusArr[$value->$v];
				}else if($v == 'uploadType'){
					$data[$key][$v] = $value->$v==1?'方式一:在线上传':'方式二:在线填写';
				}else if($v == 'created_at'){
					foreach($case[$key]['created_at'] as $sk => $sv){
						if($sk == 'date'){
							$data[$key]['created_at'] = $sv;break;
						}
					}
				}else{
					$data[$key][$v] = $value->$v;
				}
			}
		}
		/*填充表格内容*/
		$count = count($data);
		for ($i = 0;$i < $count;$i++) {
			$j = $i + 2;
			/*设置表格高度*/
			$excel->getActiveSheet()->getRowDimension($j)->setRowHeight(80);
			/*向每行单元格插入数据*/
			$countRow = count($data[$i]);
			for ($row = 0;$row < $countRow;$row++) {
				/*设置表格宽度*/
				$objActSheet->getColumnDimension("$letter[$row]")->setWidth(40);
				if ($fieldArr[$row]=='sub_exam_fileUrl1' || $fieldArr[$row]=='sub_exam_fileUrl2' || $fieldArr[$row]=='sub_exam_fileUrl3') {
					if(file_exists($data[$i][$fieldArr[$row]])){
						/*实例化插入图片类*/
						$objDrawing = new PHPExcel_Worksheet_Drawing();
						/*设置图片路径 切记：只能是本地图片*/
						$objDrawing->setPath(public_path().$data[$i][$fieldArr[$row]]);
						/*设置图片高度*/
						$objDrawing->setHeight(100);
						/*设置图片要插入的单元格*/
						$objDrawing->setCoordinates("$letter[$row]$j");
						/*设置图片所在单元格的格式*/
						$objDrawing->setOffsetX(80);
						$objDrawing->setRotation(20);
						$objDrawing->getShadow()->setVisible(true);
						$objDrawing->getShadow()->setDirection(50);
						$objDrawing->setWorksheet($excel->getActiveSheet());
						continue;
					}
				}
				$excel->getActiveSheet()->setCellValue("$letter[$row]$j",$data[$i][$fieldArr[$row]]);
			}
		}
		/*实例化excel输入类并完成输出excel文件*/
		$write = new PHPExcel_Writer_Excel5($excel);
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
		header("Content-Type:application/force-download");
		header("Content-Type:application/vnd.ms-execl");
		header("Content-Type:application/octet-stream");
		header("Content-Type:application/download");;
		header('Content-Disposition:attachment;filename="cdma-case.xls"');
		header("Content-Transfer-Encoding:binary");
		$write->save('php://output');
    }
    
}