<?php

class MobileController extends BaseController {
	
	public function __construct(){
		$this->beforeFilter('mobileauth', array('except' => array('getLogin','getRegister','postRegister','getSao','getAsp','getPpt')));
	}

	/**
	 * 移动端大首页
	 */
	public function getIndex(){
		//微信扫一扫参与签到调研
		$isWxFlag = user_agent_is_weixin();
		$wx_config = array();
		if ($isWxFlag){
			include_once  app_path().'/include/jssdk.class.php';  //先把类包含进来
			$oJssdk = new jssdk();
			$wx_config = $oJssdk->getSignPackage();
		}
		$iRoleId = Session::get('roleid');
		return View::make('mobile.index')->with('wx_config',$wx_config)->with('isWxFlag',$isWxFlag)->with('iRoleId',$iRoleId);
	}
	
	/**
	 * 登录页面
	 * $iAspirinFlag 为1 表示从微信菜单空中课堂登录，为2表示从微信菜单专项基金登录，为0表示从微信菜单个人中心登录
	 */
	public function getLogin($iAspirinFlag=0){
		if (Auth::check()) {
			$iUserId = Session::get('userid');
			$oUser = User::find($iUserId);
			//如果代表代表是扫专项基金统一二维码进入的 并且之前登陆过，则直接显示专属二维码
			if($iAspirinFlag == 4 && $oUser){
				if($oUser->invite_code == ''){
					//生成邀请码
					$iInvitedCode = User::MakeInviteCode();
					$oUser->invite_code = $iInvitedCode;
					$oUser->save();
				}
				//生成当前用户的专项基金注册二维码
				$sEwmUrl = AspirinEwm::getErweima('http://cdcma.bizconf.cn/mobile/register/'.$oUser->invite_code.'/'.$iAspirinFlag);
				return View::make('mobile.loginaspirin')->with('sEwmUrl',$sEwmUrl);
			}
			if($iAspirinFlag==1){
				return Redirect::to('/mobile');
			}elseif($iAspirinFlag==2){
				return Redirect::to('/mobile-aspirin');
			}else{
				return Redirect::to('/mobile-user');
			}
		}
		return View::make('mobile.login')->with('iAspirinFlag',$iAspirinFlag);
	}

	/**
	 * 注册页面,$iFlag默认为1，表示目标用户的H5注册，为2时表示非目标用户，为3时表示商务标签用户，为4时表示专项基金标签用户，为5时表示四川全科骨干医生标签用户
	 * 目标用户：/mobile/register/邀请码/1   （1可以省略）
	 * 非目标用户：/mobile/register/邀请码/2
	 * 商务用户：/mobile/register/固定邀请码/3  （用的代表12的邀请码OQK8B）
	 * 专项基金用户：/mobile/register/邀请码/4  （进入方式是 代表扫统一二维码【域名/mobile/login/4】登录，生成专项基金注册用二维码，扫此码注册即为此类用户）
	 * 四川全科骨干医生用户：/mobile/register/固定邀请码/5   （用的代表12的邀请码OQK8B,逻辑同商务标签）
	 */
	public function getRegister($sInviteCode='',$iFlag=1){
		if (Auth::check()) {
			return Redirect::to('/mobile');
		}
		if(!$sInviteCode){
			return View::make('mobile.registerCode')->with('error','');
		}
		$aFlag = array(1,2,3,4,5);
		if(!in_array($iFlag, $aFlag)){
			die('error');
		}
		$iLinkUserId = 0;
		//查找是否有该邀请码
		$sInviteCode = strtoupper(trim($sInviteCode));
		$oUser = User::where('invite_code',$sInviteCode)->first();
		if($oUser){
			$iLinkUserId = $oUser->id;
		}
		//不存在该邀请码用户且不是代表的邀请码
		if(!$oUser && $sInviteCode!='BAYER1' && $sInviteCode!='BAYER2'){
			return View::make('mobile.registerCode')->with('error','error');
		}
		$aHosp = Hospital::getChildren();
		return View::make('mobile.register')
			->with('aHosp',$aHosp)
			->with('code',$sInviteCode)
			->with('iLinkUserId',$iLinkUserId)
			->with('iFlag',$iFlag);
	}
	
	//专项基金注册成功显示页
	public function getAsp() {
		return View::make('mobile.registeraspirinsuccess');
	}

	//专项基金在线会议启动会议时返回ppt展示页（用于会畅开会打开ppt课件）   链接地址：/mobile/ppt/1
	public function getPpt($iPptId){
		$oPptImage = AspirinOnlinePptImage::where('ppt_id',$iPptId)->orderby('id','asc')->get();
		return View::make('mobile.aspirin.online.ppthtml')->with('oPptImage',$oPptImage);
	}

	//讲师课程-精彩内容
	public function getAuthdocclassshare(){
		$params = Input::all();
		//医生信息
		$doctorInfo = User::find($params['doc_id']);
		//视频信息获取
		$videoInfo = AspirinEducation::find($params['video_id']);
		$sProvince = Hospital::where("id",$doctorInfo->user_province)->pluck("name");
		$sCity = Hospital::where("id",$doctorInfo->user_city)->pluck("name");
		// $log = DB::getQueryLog();
		// print_r($log);die;
		//获取二维码地址
		$sUrl = Config::get('app.url').'/aspirinshare/education-detail/'.$params['video_id'];
		$sEwmUrl = AspirinEwm::getErweima($sUrl);
		// echo $sEwmUrl;die;
		return View::make('share.aspirin.education.sharedocvideo')
			->with('domain',"http://cdcma.bizconf.cn")
			->with('p_c_str',"{$sProvince}{$sCity}<br />{$doctorInfo->user_address}")
			// ->with('domain',"http://{$_SERVER['HTTP_HOST']}")
			->with('sEwmUrl',$sEwmUrl)
			->with('doctorInfo',$doctorInfo)
			->with('videoInfo',$videoInfo);
	}

}