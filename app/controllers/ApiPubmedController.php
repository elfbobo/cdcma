<?php

/**
 * Pubmed接口操作类
 * @author ghy
 *
 */
class ApiPubmedController extends BaseController {

	CONST  PAGESIZE = 10;
	public $sChar = '';
	public $iUserid = 0;
	public $term = '';
	public $sWhere = '';
	public $iPageSize = 10;
	public $sAppPath;

	public function __construct(){
	   	$this->beforeFilter('apilogin');
		$this->sAppPath = app_path();
	}

	public function getIndex(){
		global $iUserId;
		$this->iUserid = $iUserId;
		if(!$this->iUserid){
			return json_encode(array('success'=>false,'msg'=>'您还没有登陆'));
		}
		$this->removeBadCharacters();
		require ( $this->sAppPath.'/extends/pubmed/PubMedAPI.php' );
		$PubMedAPI = new PubMedAPI();
		$sKeyword = Input::has('sKeyword') ? stripslashes(urldecode(trim(Input::get('sKeyword')))) : '';
		$sMeshTerms = Input::has('sMeshTerms') ? stripslashes(urldecode(trim(Input::get('sMeshTerms')))) : '';
		$sTitle = Input::has('sTitle') ? stripslashes(urldecode(trim(Input::get('sTitle')))) : '';
		$sAbstract = Input::has('sAbstract') ? stripslashes(urldecode(trim(Input::get('sAbstract')))) : '';
		$sAuthor = Input::has('sAuthor') ? stripslashes(urldecode(trim(Input::get('sAuthor')))) : '';
		$sAuthorUnit = Input::has('sAuthorUnit') ? stripslashes(urldecode(trim(Input::get('sAuthorUnit')))) : '';
		$sJournalName = Input::has('sJournalName') ? stripslashes(urldecode(trim(Input::get('sJournalName')))) : '';
		$sStartDate = Input::has('sBeginTime') ? stripslashes(urldecode(trim(Input::get('sBeginTime')))) : 1;
		$sEndDate = Input::has('sEndTime') ? stripslashes(urldecode(trim(Input::get('sEndTime')))) : 3000;
		$sAllFields = Input::has('sAllFields') ? stripslashes(urldecode(trim(Input::get('sAllFields')))) : '';
		//关键词
		if($sKeyword){
			if($this->term){
				$this->term.="AND" . " (".$sKeyword."[Text Word]) ";
			}else{
				$this->term.="(".$sKeyword."[Text Word]) ";
			}
		}
		//MeSH主题词
		if($sMeshTerms){
			if($this->term){
				$this->term.="AND" . " (".$sMeshTerms."[MeSH Terms]) ";
			}else{
				$this->term.="(".$sMeshTerms."[MeSH Terms]) ";
			}
		}
		//标题
		if($sTitle){
			if($this->term){
				$this->term.="AND" . " (".$sTitle."[Title]) ";
			}else{
				$this->term.="(".$sTitle."[Title]) ";
			}
		}
		//标题/摘要
		if($sAbstract){
			if($this->term){
				$this->term.="AND" . " (".$sAbstract."[Title/Abstract]) ";
			}else{
				$this->term.="(".$sAbstract."[Title/Abstract]) ";
			}
		}
		//作者
		if($sAuthor){
			if($this->term){
				$this->term.="AND" . " (".$sAuthor."[Author]) ";
			}else{
				$this->term.="(".$sAuthor."[Author]) ";
			}
		}
		//作者单位
		if($sAuthorUnit){
			if($this->term){
				$this->term.="AND" . " (".$sAuthorUnit."[First Author]) ";
			}else{
				$this->term.="(".$sAuthorUnit."[First Author]) ";
			}
		}
		//期刊名称
		if($sJournalName){
			if($this->term){
				$this->term.="AND" . " (".$sJournalName."[Journal]) ";
			}else{
				$this->term.="(".$sJournalName."[Journal]) ";
			}
		}
		//起始出版日期条件处理
		$sStartDate = $this->__formatDate($sStartDate);
		$sEndDate = $this->__formatDate($sEndDate);
		if ($sStartDate != 1 || $sEndDate != 3000){
			if($this->term){
				$this->term.="AND (\"" . $sStartDate."\"[PDat]:\"".$sEndDate."\"[PDat]) ";
			}else{
				$this->term.="(\"" . $sStartDate."\"[PDat]:\"".$sEndDate."\"[PDat]) ";
			}
		}
		//所有字段
		if($sAllFields){
			if($this->term){
				// $this->term.="AND" . " (".$sAllFields."[All Fields]) ";
				$this->term.="AND" . " (".$sAllFields.") ";
			}else{
				// $this->term.="(".$sAllFields."[All Fields]) ";
				$this->term.="(".$sAllFields.") ";
			}
		}
		if(Input::has('iPageSize')){
			$this->iPageSize = intval(Input::get('iPageSize'));
		}
		$iPage = intval(Input::get('iPage'));
		$PubMedAPI->retmax = $this->iPageSize;
		$this->sWhere.= $this->iPageSize;
		if (!empty($iPage)) {
			$PubMedAPI->retstart = $PubMedAPI->retmax*(intval($iPage) - 1)+1;
			if ($PubMedAPI->retstart == 1){
				$PubMedAPI->retstart = 0;
			}else{
				$PubMedAPI->retstart = intval($PubMedAPI->retstart) -1;
			}
		}
		$results = array();
		$total = 0;
		if($this->term){
			$results = $PubMedAPI->query($this->term, false, false, $this->sWhere);
			$total = $PubMedAPI->count;
		}
		if($results){
			foreach ($results as $key=>$result){
				$aData[$key]['id']=$result['pmid'];
				$aData[$key]['pmid']=$result['pmid'];
				$aData[$key]['title']=isset($result['title']) ? $result['title'] : '';
				$aData[$key]['journal']=isset($result['journal']) ? $result['journal'] : '';
				$aData[$key]['year']=isset($result['year']) ? $result['year'] : '';
				$aData[$key]['month']=isset($result['month']) ? $result['month'] : '';
				$aData[$key]['volume']=isset($result['volume']) ? $result['volume'] : '';
				$aData[$key]['issue']=isset($result['issue']) ? $result['issue'] : '';
				$aData[$key]['pages']=isset($result['pages']) ? $result['pages'] : '';
				$aData[$key]['authors']=isset($result['authors']) ? implode(", ",$result['authors']) : '';
				//$aData[$key]['content_url']=CONTENT_URL.$result['pmid'];
			}
			return json_encode(array('success'=>true,'count'=>$total,'dataList'=>$aData));
		}else{
			return json_encode(array('success'=>false,'msg'=>'未命中记录','count'=>0,'dataList'=>array()));
		}
	}
    
    private function __formatDate($date){
		$str = str_replace('-', '/', $date);
		return $str;
	}
	
    /**
     * http://eisai.dev/apipmd/show?iPubmedId=19350666&token=AQZrUQ==
     * 获取文章详细
     */
	public function getShow(){
		global $iUserId;
		$this->iUserid = $iUserId;
		if(Input::has('iPubmedId')){
			$iPmid = Input::get('iPubmedId');
		}else{
			return json_encode(array('success'=>false,'msg'=>'pubmedId不能为空'));
		}
		if(!$this->iUserid){
			return json_encode(array('success'=>false,'msg'=>'您还没有登陆'));
		}
		$this->removeBadCharacters();
		require ( $this->sAppPath.'/extends/pubmed/PubMedAPI.php' );
		$oPmd = new PubMedAPI();
		$aData = array();
		if (!empty($iPmid)){
			$result = $oPmd->query_pmid(trim($iPmid));
			if (!empty($result)){
				$aData['title'] = isset($result[0]['title']) ? $result[0]['title'] : '';
				$aData['journalabbrev'] = isset($result[0]['journalabbrev']) ? $result[0]['journalabbrev']:'';
				$aData['year'] = isset($result[0]['year']) ? $result[0]['year']:'';
				$aData['month'] = isset($result[0]['month']) ? $result[0]['month']:'';
				$aData['volume'] = isset($result[0]['volume']) ? $result[0]['volume']:'';
				$aData['issue'] = isset($result[0]['issue']) ? $result[0]['issue']:'';
				$aData['pages'] = isset($result[0]['pages']) ? $result[0]['pages']:'';
				if (is_array($result[0]['authors'])){ 
					$aData['authors']=implode(', ', $result[0]['authors']);
				}else{
					$aData['authors']=isset($result[0]['authors']) ? $result[0]['authors'] : '';
				}
				$aData['affiliation'] = isset($result[0]['affiliation'])?$result[0]['affiliation']:'';
				if (is_array($result[0]['keywords'])){ 
					$aData['keywords']=implode(', ', $result[0]['keywords']);
				}else{
					$aData['keywords']=isset($result[0]['keywords']) ? $result[0]['keywords'] : '';
				}
				$aData['abstract'] = isset($result[0]['abstract']) ? $result[0]['abstract'] : '';
				return json_encode(array('success'=>true,'content'=>$aData));
			}else{
				return json_encode(array('success'=>false,'msg'=>'未获取文章详细', content=>''));
			}
	    }
	}
		
	/**
	 * 获取全文,求助全文
	 */
	public function getFulltextpmd(){
		global $iUserId;
		$this->iUserid = $iUserId;
		if(!$this->iUserid){
			return json_encode(array('success'=>false,'msg'=>'您还没有登陆'));
		}
		if(Input::has('iPubmedId')){
			$iPubmedId = trim(Input::get('iPubmedId'));  
		}else{
			return json_encode(array('success'=>false,'msg'=>'pubmedId不能为空'));
		}
		$oUser = User::where('id', $this->iUserid)->first();
		if(!$oUser->user_email){
			return json_encode(array('success'=>false,'msg'=>'用户邮箱不能为空'));
		}
		/* if(intval($oUser->user_credit_count) < EN_HELP_SCORE){
			return json_encode(array('success'=>false,'msg'=>'用户积分不足'));
		}*/
		require ( $this->sAppPath.'/extends/pubmed/PubMedAPI.php' );
		$PubMedAPI = new PubMedAPI();
		$result = $PubMedAPI->query_pmid($iPubmedId);
		$result[0]['authors'] = isset($result[0]['authors']) ? implode(", ",$result[0]['authors']) : '';
		//邮件主题
		$sHelpText = $result[0]['pmid'].",文章标题为：".
		$result[0]['title'].",文章出处为：".
		$result[0]['journal'].",".
		$result[0]['year'].$result[0]['month'].",".
		$result[0]['volume'].":".
		$result[0]['pages'].",作者为: ".$result[0]['authors'];
		$subject = "国卫健康云Pubmed求助全文";
		$content = "国卫健康云Pumed检索需要获取全文,文献PMID为：".$sHelpText.", 请将全文发送到邮箱：".$oUser->user_email;
		$pmid = PubmedHelpMail::where('userid', $this->iUserid)->where('pmid', $iPubmedId)->pluck('pmid');
		if(!$pmid){	
			$aHelpInfo['userid'] = $this->iUserid; 
			$aHelpInfo['email'] = addslashes($oUser->user_email);
			$aHelpInfo['pmid'] = $iPubmedId;
			$aHelpInfo['title'] = addslashes($subject);
			$aHelpInfo['content'] = addslashes($content);
			$aHelpInfo['device_type'] = 1;
			$aHelpInfo['pub_type'] = 1;
			// $aHelpInfo['posttime'] = time();
			$oPmdHelpMail = new PubmedHelpMail($aHelpInfo);
			$oPmdHelpMail->save();
			PubmedHelpMail::SendEmail($subject,$content);
			//扣除积分
			// $this->oScoreService->getIndex(6,"PMD_EN_HELP",$iPubmedId, $this->iUserid, "app");
			return json_encode(array('success'=>true,'msg'=>'您所发起的全文申请已经成功发送至医学经理及产品经理。待相关人员确认后，您所申请的文献将发送至您的邮箱。'));
		}else{
			return json_encode(array('success'=>false,'msg'=>'您已经求助过该文献')); //已经求助过此文献
		}        	   
	}
		
    /**
	 * @desc 过滤字符
	 * Enter description here ...
	 */
    public function removeBadCharacters(){
		global $bad_utf8_chars;
		$bad_utf8_chars = array(
			"\0", 
			"\xc2\xad",
			"\xcc\xb7", 
			"\xcc\xb8", 
			"\xe1\x85\x9F", 
			"\xe1\x85\xA0", 
			"\xe2\x80\x80", 
			"\xe2\x80\x81", 
			"\xe2\x80\x82", 
			"\xe2\x80\x83", 
			"\xe2\x80\x84", 
			"\xe2\x80\x85", 
			"\xe2\x80\x86", 
			"\xe2\x80\x87", 
			"\xe2\x80\x88", 
			"\xe2\x80\x89", 
			"\xe2\x80\x8a", 
			"\xe2\x80\x8b", 
			"\xe2\x80\x8e", 
			"\xe2\x80\x8f", 
			"\xe2\x80\xaa", 
			"\xe2\x80\xab", 
			"\xe2\x80\xac", 
			"\xe2\x80\xad", 
			"\xe2\x80\xae", 
			"\xe2\x80\xaf", 
			"\xe2\x81\x9f", 
			"\xe3\x80\x80", 
			"\xe3\x85\xa4", 
			"\xef\xbb\xbf", 
			"\xef\xbe\xa0", 
			"\xef\xbf\xb9", 
			"\xef\xbf\xba", 
			"\xef\xbf\xbb", 
			"\xE2\x80\x8D"
		);
		function _remove_bad_characters($array) {
			global $bad_utf8_chars;
			return is_array($array) ? array_map('_remove_bad_characters', $array) : str_replace($bad_utf8_chars, '', $array);
		}
		$_GET = _remove_bad_characters($_GET);
		$_POST = _remove_bad_characters($_POST);
		$_COOKIE = _remove_bad_characters($_COOKIE);
		$_REQUEST = _remove_bad_characters($_REQUEST);
	}
	
}