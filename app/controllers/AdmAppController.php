<?php

class AdmAppController extends BaseController {

	CONST  PAGESIZE = 10;
	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	//专家列表
	public function Version()
	{
		//取出所有专家
		$oDocs = Version::paginate(self::PAGESIZE);
		return  View::make('admin.version.list')
			->with('oDocs',$oDocs);
	}
	
	//新增专家
	public function AddVersion(){
		return  View::make('admin.version.add');
	}
	
	//新增专家do
	public function AddVersionDo(){
		$aInput = Input::all();
		unset($aInput['upload_file']);
		$rule = Version::getRule();
		
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
		    return $this->showMessage('请填写必填字段');
		}
		
		$oVersion = new Version();
		//验证成功
		if($oVersion->addVersion($aInput)->save()){
			return $this->showMessage('新增版本成功','/admapp/version');
		}else{
			return $this->showMessage('添加失败');
		}
	}
	
	//编辑用户
	public function EditVersion($id){
		$oVersion = Version::find($id);
		return  View::make('admin.version.edit')
			->with('oVersion',$oVersion);
	}
	
	//编辑提交
	public function EditVersionDo($id){
		$aInput = Input::all();
		unset($aInput['upload_file']);
		$rule = Version::getRule();
		
		$validator = Validator::make($aInput,$rule);
		if ($validator->fails()){
		    return $this->showMessage('请填写必填字段');
		}
		
		$oVersion = Version::find($id);
		if($oVersion->addVersion($aInput)->save()){
			return $this->showMessage('编辑成功');
		}else{
			return $this->showMessage('编辑失败');
		}
	}
	
	//删除
	public function DelVersion($id){
		$oVersion = Version::find($id);
		$oVersion->delete();
		return $this->showMessage('删除版本成功','/admapp/version');
	}
	
}