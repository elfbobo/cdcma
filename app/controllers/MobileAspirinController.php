<?php

class MobileAspirinController extends BaseController {
	
	public function __construct(){
		$this->beforeFilter('mobileauth2');
		// $this->beforeFilter('cityauth');
	}

	//专项基金提示页
	public function getWarning()
	{
		return View::make('mobile.aspirin.warning');
	}

	//移动端专项基金首页
	public function getIndex($iFlag=0)
	{
		$oUser = User::find(Session::get('userid'));
		//当flag为1时表示未提交认证直接关闭认证框返回当前页面
		/*$aPilotCity = User::getPilotCity();
		$aPilotCountry = User::getPilotCountry();
		if($iFlag==0 && $oUser->role_id == 3){
			if(!in_array($oUser->user_city,$aPilotCity) && !in_array($oUser->user_county,$aPilotCountry)){
				//非试点城市无法进入专项基金
				return Redirect::to('/mobile-aspirin/warning');
			}
			// if($oUser->card_auth_flag == 0){
			// 	return Redirect::to('/mobile-aspirin/auth');
			// }
		}*/
		$oGroup = AspirinInfoGroup::orderby('id','ASC')->get();
		return View::make('mobile.aspirin.index')->with('oUser',$oUser)->with('oGroup',$oGroup);
	}

	//轮播图列表页
	public function getInfoList($iGroupId)
	{
		$oInfo = AspirinInfo::where('group_id',$iGroupId)->orderby('created_at','desc')->get();
		$oGroup = AspirinInfoGroup::find($iGroupId);
		$sGroupTitle = $oGroup->info_group_title;
		return View::make('mobile.aspirin.info.list')->with('oInfo',$oInfo)->with('sGroupTitle',$sGroupTitle);
	}

	//轮播图详情页
	public function getInfoShow($iId)
	{
		$oInfo = AspirinInfo::find($iId);
		if(count($oInfo)){
			$oInfo->increment('info_hits');
			$oGroup = AspirinInfoGroup::find($oInfo->group_id);
			$sGroupTitle = $oGroup->info_group_title;
			return View::make('mobile.aspirin.info.show')->with('oInfo',$oInfo)->with('sGroupTitle',$sGroupTitle);
		}
	}

	//个人中心
	public function getAuth()
	{
		$oUser = User::find(Session::get('userid'));
		/*$aPilotCity = User::getPilotCity();
		$aPilotCountry = User::getPilotCountry();
		if($oUser->role_id == 3){
			if(!in_array($oUser->user_city,$aPilotCity) && !in_array($oUser->user_county,$aPilotCountry)){
				//非试点城市无法进入专项基金
				return Redirect::to('/mobile-aspirin/warning');
			}
		}*/
		if($oUser->user_company != 0 && is_numeric($oUser->user_company)){
			$sHospitalName = Hospital::where('id',$oUser->user_company)->pluck('name');
		}else{
			$sHospitalName = $oUser->user_company_name;
		}
		return View::make('mobile.aspirin.myauth')->with('sHospitalName',$sHospitalName)->with('oUser',$oUser);
	}

	//个人认证图片上传
	public function postUploadUserThumb()
	{
		if($_FILES['file-pic']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['file-pic']['name'];
			$attach_fileext = get_filetype($attach_filename);
			$rand_name = date('YmdHis', time()).rand(1000,9999);
			$sFileName = $rand_name.'.'.$attach_fileext;
			$sPath = "/upload/user/$attach_fileext/".date('Ymd',time());
			$sRealPath = public_path().$sPath;
			mkdirs($sRealPath);
			// $attach_filesize = filesize($_FILES['file-pic']['tmp_name']);
			move_uploaded_file($_FILES['file-pic']['tmp_name'], $sRealPath.DS.$sFileName);
			$sFileNameS = $rand_name . '_s.' . $attach_fileext;
			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 1000, 1000 );
			$sFileUrl = $sPath.'/'.$sFileNameS;
			$json = array('user_thumb'=>$sFileUrl);
			echo json_encode($json);
			die;
		}
	}

	//保存认证
	public function postAuthSave()
	{
		$iUid = Session::get('userid');
		$oUser = User::find($iUid);
		$oUser->user_position = Input::get('user_position');
		$oUser->card_number = Input::get('card_number');
		$oUser->card_thumb = Input::get('thumb');
		$oUser->card_auth_flag = 1;
		$oUser->save();
		return json_encode(array('success'=>true));
	}

	//风险筛查点击统计
	public function postScreeningHit()
	{
		$aLog = array(
			'user_id'     => Session::get('userid'),
			'device'      => 'weixin',
			'created_at'  => date('Y-m-d H:i:s',time())
		);
		$oLog = new AspirinScreeningHitLog($aLog);
		$oLog->save();
		return json_encode(array('success'=>true,'url'=>'http://whder.mymeeting.com.cn'));
	}

}