<?php

class TestController extends BaseController
{

    /**
     * 重新合成丢失的视频
     */
    public function getComplex()
    {
        return true;
        $isBuild = Input::get('build', 0);
        $sUserTmpUrl = public_path() . '/upload/education/produce/47704/201809261621417136';

        VideoProduceService::$sFFmpegCommand = '/usr/local/bin/ffmpeg';

        if (!$isBuild) {
            //图片停留时间数据备份
            $image_and_time = json_decode(file_get_contents($sUserTmpUrl . '/image_and_time.txt'), true);
            $images = [];
            $imgPath = '';
            foreach ($image_and_time as $item) {
                $images[] = 'http://cdcma.bizconf.cn' . $item['url'];
                $imgPath = dirname($item['url']);
            }
            file_put_contents(public_path() . $imgPath . '/dl.txt', join(PHP_EOL, $images));

            exit(public_path() . $imgPath);
        }

        $result = VideoProduceService::reOperate($sUserTmpUrl);
        exit($result);
    }

    /**
     * 获取某目录下所有 jpeg 文件
     * @param $path
     * @return array
     */
    private function getPathList($path)
    {
        $result = [];
        $files = scandir($path);

        foreach ($files as $file) {
            //$file = str_replace(' ', '\\ ', $file);
            if (!in_array($file, ['.', '..'])) {
                if (is_dir($path . $file)) {
                    $result = array_merge($result, $this->getPathList($path . $file . '/'));
                } else {
                    if (pathinfo($file, PATHINFO_EXTENSION) == 'jpeg') {
                        $result[] = $path . $file;
                    }
                }
            }
        }

        return $result;
    }

    public function getPptImg()
    {
        set_time_limit(0);
        return true;
        $path = '/Users/byron/Works/project/国卫健康云/9月/';
        $list = $this->getPathList($path);
        dump(count($list));
        $aSql = [];
        $aCmd = [];

        $map = AspirinOnlinePpt::lists('ppt_title', 'id');
        foreach ($list as $filepath) {
            $info = pathinfo($filepath);
            $fnames = explode('.', $info['filename']);
            $dir = $fnames[0];
            $file = $fnames[count($fnames) - 1];

            if ($file == 'CN') {
                dump($info['filename']);
            }
            // 生成 sql 语句
            $ppt_title = trim($dir);
            $pptid = AspirinOnlinePpt::whereRaw("ppt_title = '$ppt_title'")->pluck('id');
            if (empty($pptid)) {
                $ids = [];
                foreach ($map as $id => $title) {
                    if (false !== stripos($ppt_title, $title)) {
                        $ids[] = $id;
                    }
                }
                $diff = [
                    '卒中患者二级预防策略0524 L' => 26,
                    '时长-ACS患者双抗治疗最佳时长是多久PP-ASP-CN-0241-1' => 23,
                    '稳定型冠心病临床路径0525 L' => 22,
                    '选择-ACS患者双抗治疗如何选择ADP受体拮抗剂PP-ASP-CN-0242-1' => 25, // 32
                    '阿司匹林是否被新型的抗血小板药物所取代-卒中？180827' => 18,
                ];
                if (count($ids) > 1) {
                    if (isset($diff[$ppt_title])) {
                        $pptid = $diff[$ppt_title];
                    } else {
                        echo $ppt_title;
                        dump($ids);
                        exit;
                    }
                } else {
                    $pptid = $ids[0];
                }
            }
            /*if (!in_array($pptid, [18,23,25,28,29])) {
                continue;
            }*/

            $savepath = "/upload/online/pptimage/20180928/{$pptid}-{$file}.jpg";
            $created_at = date('Y-m-d H:i:s');
            $updated_at = date('Y-m-d H:i:s');
            $aSql[] = "INSERT INTO `aspirin_online_ppt_image`(`ppt_id`, `online_id`, `image_url`, `created_at`, `updated_at`) VALUES ({$pptid}, 0, '{$savepath}', '{$created_at}', '{$updated_at}');";
            $filepath = str_replace(' ', "\\ ", $filepath);
            $cmd = "cp {$filepath} /Users/byron/Works/project/国卫健康云/9月/{$savepath}";

            exec($cmd, $output);
            if (!empty($output)) {
                dump($output);
            }
        }
        echo join(';', $aSql);
    }

    /**
     * 下载所有视频签名文件
     * @return string
     */
    public function getDownloadSignIcon()
    {
        $i = Input::get('i', 0);
        $item = AspirinEducation::where('is_signed', 1)->orderby('id', 'asc')
            ->skip($i)
            ->take(1)
            ->first();

        if (empty($item->id)) {
            return '全部下载完成';
        }

        $sign_icon = $item->sign_icon;
        if (empty($sign_icon)) {
            $str = '文件不存在 ' . $sign_icon;
        } else {
            if (!file_exists(public_path() . $sign_icon)) {
                $dir = pathinfo($sign_icon, PATHINFO_DIRNAME);
                mkdirs(public_path() . $dir);

                file_put_contents(public_path() . $sign_icon,
                    file_get_contents('http://cdcma.bizconf.cn' . $sign_icon));
                $str = $sign_icon . ' 下载完成';
            } else {
                $str = '文件已存在';
            }
        }
        $url = url('/test/download-sign-icon?i=' . ++$i);
        header("refresh:0; url={$url}");

        return $str;
    }

    public function getParseNotice()
    {
        set_time_limit(0);
        header("Content-type: text/html; charset=utf-8");

        $iPage = Input::get('p', 0);
        $id = Input::get('id', 0);
        $iPagesize = 1;

        // 没有分页时统计总数
        if (0 == $iPage && 0 == $id) {
            $num = AspirinUserNotice::where('notice_type',
                150)->whereRaw('(agreement_img=\'\' or agreement_img is null)')->count();
            exit("系统中有{$num}条数据待处理~");
        }

        $data = [];
        $list = AspirinUserNotice::select([
            'aspirin_user_notice.*',
            'aspirin_education.is_signed',
            'aspirin_education.sign_icon'
        ])->join('aspirin_education', 'aspirin_user_notice.detail_id', '=', 'aspirin_education.id')
            ->where('aspirin_user_notice.notice_type', 150)
            ->whereRaw('(aspirin_user_notice.agreement_img=\'\' or aspirin_user_notice.agreement_img is null)');
        if ($id) {
            $list = $list
                ->where('detail_id', $id)
                ->get();
        } else {
            $list = $list
                ->skip(($iPage - 1) * $iPagesize)
                ->take($iPagesize)
                ->get();
        }

        if (!$list->count()) {
            $num = AspirinUserNotice::where('notice_type',
                150)->whereRaw('(agreement_img=\'\' or agreement_img is null)')->count();
            echo '处理完成 ' . $num . ' 条数据未处理';
            exit;
        }
        foreach ($list as $item) {
            if (!empty($item->agreement_img)) {
                Redirect::to('/test/parse-notice?p=' . ++$iPage);
                break;
            }
            $txt = $item->agreement_txt;
            $arr = explode(PHP_EOL, $txt);
            for ($i = 0; $i < count($arr); $i++) {
                if (stripos($arr[$i], '乙方：') !== false) {
                    $str = explode('乙方：', $arr[$i])[1];
                    $data['user_name'] = trim($str);
                } elseif (stripos($arr[$i], '证件号码：') !== false) {
                    $str = explode('证件号码：', $arr[$i])[1];
                    $data['card_number'] = trim($str);
                } elseif (stripos($arr[$i], '联系地址：') !== false) {
                    $str = explode('联系地址：', $arr[$i])[1];
                    $data['user_address'] = trim($str);
                } elseif (stripos($arr[$i], '联系电话：') !== false) {
                    $str = explode('联系电话：', $arr[$i])[2];
                    $data['user_tel'] = trim($str);
                } elseif (stripos($arr[$i], '协议费用总额：') !== false) {
                    $str = explode('协议费用总额：', $arr[$i])[1];
                    $data['user_fee'] = trim(trim($str), '_。');
                } elseif (stripos($arr[$i], '银行账号：') !== false) {
                    $str = explode('银行账号：', $arr[$i])[1];
                    $data['bank_account'] = trim($str);
                } elseif (stripos($arr[$i], '开户银行详细地址：') !== false) {
                    $str = explode('开户银行详细地址：', $arr[$i])[1];
                    $data['bank_address'] = trim($str);
                } elseif (stripos($arr[$i], '日期：') !== false) {
                    $str = explode('日期：', $arr[$i]);
                    $data['date'] = trim($str[1]) != '' ? trim($str[1]) : trim($str[2]);
                }
            }

            // 如果始终没获取到签字日期
            if (empty($data['date'])) {
                $data['date'] = $item->created_at->format('Y/m/d');
            }

            $data['video_id'] = $item->detail_id;
            // 当数据完整的情况下生成签名图片
            if (8 <= count(array_filter($data))) {
                if ($item->is_signed) {
                    $sign_icon = $item->sign_icon;
                    $ext = '.' . pathinfo($sign_icon, PATHINFO_EXTENSION);
                    $sign_path = trim($sign_icon, $ext);
                    $sign_png = $sign_path . '.png';
                    $sign_jpg = '/upload/aspirineducation/sign/' . $data['video_id'] . '.jpg';
                    $agreement_img = $this->createSignFile($data);

                    if (!$id && file_exists(public_path() . $sign_jpg)) {
                        $str = '[' . $data['video_id'] . ']文件已存在<br/>' . url($sign_jpg) . '<br/>';
                    } else {
                        //载入图像，返回图像资源
                        $sign = imagecreatefromjpeg(public_path() . $sign_icon);

                        // 去除护眼色
                        //imagecolortransparent($sign,imagecolorallocate($sign,247,252,246));
                        //imagecolortransparent($sign, imagecolorallocatealpha($sign,247,252,246, 127));

                        // 查看图片0坐标的颜色
                        $rgb = imagecolorat($sign, 0, 0);
                        $r = ($rgb >> 16) & 0xFF;
                        $g = ($rgb >> 8) & 0xFF;
                        $b = $rgb & 0xFF;
                        // 0坐标颜色非白色时处理
                        if ($r != 255 && $g != 255 && $b != 255) {
                            //要处理的色阶起始值
                            $begin_r = 247;
                            $begin_g = 252;
                            $begin_b = 246;
                            list($src_w, $src_h) = getimagesize(public_path() . $sign_icon);// 获取原图像信息

                            $i = 0;
                            $src_white = imagecolorallocate($sign, 255, 255, 255);
                            for ($x = 0; $x < $src_w; $x++) {
                                for ($y = 0; $y < $src_h; $y++) {
                                    $rgb = imagecolorat($sign, $x, $y);
                                    $r = ($rgb >> 16) & 0xFF;
                                    $g = ($rgb >> 8) & 0xFF;
                                    $b = $rgb & 0xFF;
                                    if ($r == 255 && $g == 255 && $b == 255) {
                                        $i++;
                                        continue;
                                    }
                                    if (($r > 66 && $g > 66 && $b > 66) && ($r <= $begin_r && $g <= $begin_g && $b <= $begin_b)) {
                                        imagefill($sign, $x, $y, $src_white);//替换成白色
                                    }
                                }
                            }
                        }

                        //$sign = $src_im;
                        // 使图片透明
                        imagecolortransparent($sign, imagecolorallocatealpha($sign, 255, 255, 255, 127));

                        list($width, $height, $type, $attr) = getimagesize(public_path() . $sign_icon);
                        $_w = $height < 3200 ? $width * 0.28 : $width * 0.3;
                        $_h = $height < 400 ? $height / 1.5 : min($height < 3200 ? 380 : 200, $height);
                        $target = imagecreatetruecolor($_w, $_h);//创建画布

                        imagecopy($target, $sign, 0, 0, $width - $_w, $height - $_h, $_w, $_h);//拷贝图像或图像的一部分（原始图片大小）
                        imagedestroy($sign);//销毁图片

                        imagepng($target, public_path() . $sign_png);
                        imagedestroy($target);

                        // 合并签名
                        $origin_file = imagecreatefromjpeg(public_path() . $agreement_img);
                        $sign_file = imagecreatefrompng(public_path() . $sign_png);
                        imagesavealpha($sign_file, true);
                        imagecopymerge($origin_file, $sign_file, 1580, 5670, 0, 0, imagesx($sign_file),
                            imagesy($sign_file), 100);

                        imagejpeg($origin_file, public_path() . $sign_jpg);

                        $info = AspirinUserNotice::where('id', $item->id)->first();
                        $info->agreement_img = $agreement_img;
                        $info->save();

                        if ($id) {
                            echo "<img src='" . url($sign_jpg) . "' width=\"100%\">";
                        }

                        $str = '[' . $data['video_id'] . ']处理完成<br/>' . url($agreement_img) . '<br/>';
                    }
                } else {
                    $agreement_img = $this->createSignFile($data);
                    $info = AspirinUserNotice::where('id', $item->id)->first();
                    $info->agreement_img = $agreement_img;
                    $info->save();

                    $str = '[' . $data['video_id'] . ']无需处理<br/>';
                }
            } else {
                $str = '不满足条件 <br/><pre>';
                foreach ($data as $key => $val) {
                    if (empty($val)) {
                        $key = "<font color=red>{$key}</font>";
                    }
                    $str .= "{$key}:{$val}<br/>";
                }
                $str .= "</pre>";
            }
            $data['count'] = count(array_filter($data));
        }

        $url = url("/test/parse-notice?p=" . ++$iPage);
        header("refresh:0; url={$url}");

        return $str . ' next：' . $url;
    }

    public function getExportSign()
    {
        $sFilePath = '/upload/aspirineducation/sign/';
        $sTargetPath = '/upload/aspirineducation/zip/';

        $list = AspirinUserNotice::select([
            'aspirin_user_notice.*',
            'aspirin_education.user_name',
            'aspirin_education.is_signed',
            'aspirin_education.sign_icon',
            'aspirin_education.created_at as created_time'
        ])->join('aspirin_education', 'aspirin_user_notice.detail_id', '=', 'aspirin_education.id')
            ->where('aspirin_user_notice.notice_type', 150)
            //->whereRaw('(aspirin_user_notice.agreement_img=\'\' or aspirin_user_notice.agreement_img is null)')
            ->get();

        foreach ($list as $item) {
            $video_id = $item->detail_id;
            $user_name = $item->user_name;
            $created_at = date('Ymd', strtotime($item->created_time));
            $source_file = $sFilePath . $video_id . '.jpg';
            $target_file = $sTargetPath . $created_at . '_' . $user_name . '.jpg';

            if (file_exists(public_path() . $source_file)) {
                copy(public_path() . $source_file, public_path() . $target_file);
            }
        }

        return '文件处理完成';
    }

    /**
     * 生成协议文件（不带签名）
     * @param array $protocolInfo 协议信息
     * @param int $is_sign 0未签名  1已签名
     * @param string $sign_file
     * @return string
     */
    private function createSignFile($protocolInfo = array(), $is_sign = 0, $sign_file = '')
    {
        set_time_limit(0);
        @ini_set('memory_limit', '256M');
        $font = public_path() . '/assets/template/msyh.ttf';

        //处理图片
        $sNewName = $protocolInfo['video_id'] . '.jpg';//新文件名
        $sFilePath = '/upload/aspirineducation/agreement/';
        if (!public_path() . $sFilePath) {
            mkdirs(public_path() . $sFilePath, 0777);
        }

        // 如果文件存在则直接返回
        if (file_exists(public_path() . $sFilePath . $sNewName)) {
            //return $sFilePath . $sNewName;
        }

        $target = imagecreatetruecolor(2496, 7010);//创建画布
        $bc = imagecolorallocate($target, 0, 3, 51);//分配颜色
        $cc = imagecolorallocate($target, 240, 102, 0);
        $wc = imagecolorallocate($target, 255, 255, 255);
        $yc = imagecolorallocate($target, 255, 255, 0);
        $bg = imagecreatefromjpeg(public_path() . PHP_IMG . '/aspirin/xy-video-01.jpg');//载入图像，返回图像资源

        imagecopy($target, $bg, 0, 0, 0, 0, 2496, 7010);//拷贝图像或图像的一部分（原始图片大小）
        imagedestroy($bg);//销毁图片
        $size = 32;

        $h = 500;
        $height = 72;

        $str1 = $protocolInfo['user_name'];
        imagettftext($target, $size, 0, 1580, $h, $bc, $font, $str1);

        $h = $h + $height;
        $str2 = $protocolInfo['card_number'];
        imagettftext($target, $size, 0, 1650, $h, $bc, $font, $str2);

        $h = $h + $height;
        $str3 = $protocolInfo['user_address'];
        imagettftext($target, $size, 0, 1650, $h, $bc, $font, $str3);

        $h = $h + $height;
        $str4 = $protocolInfo['user_tel'];
        imagettftext($target, $size, 0, 1650, $h, $bc, $font, $str4);

        $h = 4075;
        $str4 = $protocolInfo['user_fee'];
        imagettftext($target, 38, 0, 910, $h, $bc, $font, $str4);

        $h = 4370;
        $str4 = $protocolInfo['bank_account'];
        imagettftext($target, 36, 0, 730, $h, $bc, $font, $str4);

        $h = $h + $height;
        $str4 = $protocolInfo['bank_address'];
        imagettftext($target, $size, 0, 900, $h, $bc, $font, $str4);

        $h = 5938;
        $str4 = $protocolInfo['date'];
        imagettftext($target, 38, 0, 500, $h, $bc, $font, $str4);

        imagejpeg($target, public_path() . $sFilePath . $sNewName);
        imagedestroy($target);

        return $sFilePath . $sNewName;
    }

    /**
     * 处理用户异常认证图片
     */
    public function getUserSign()
    {
        $id = Input::get('id', 0);
        if ($id && $oInfo = User::where('id', $id)->first()) {
            // 医师认证信息 bugfix
            list($width, $height, $type, $attr) = getimagesize(public_path() . $oInfo->sign_icon);
            // 如果宽大于高时表示为小图片，则需要图片合成
            if ($width > $height) {
                $image_source = imagecreatefromjpeg(public_path(). '/assets/images/aspirin/ysrz.jpg');
                // 使图片背景透明
                $image_dest = image2transparent(public_path(). $oInfo->sign_icon);

                // 缩放比例
                $per = round(95/$height,3);
                $n_w = $width*$per;
                $n_h = $height*$per;
                $new_dest = imagecreatetruecolor($n_w, $n_h);
                // copy部分图像并调整
                imagecopyresized($new_dest, $image_dest,0, 0,0, 0,$n_w, $n_h, $width, $height);
                // 合并图片
                imagecopymerge($image_source, $new_dest, 480, 3330, 0, 0, imagesx($new_dest), imagesy($new_dest), 100);
                // 备份原文件
                copy(public_path(). $oInfo->sign_icon, public_path(). $oInfo->sign_icon . '.bak');
                // 覆盖原文件
                imagejpeg($image_source,public_path() . $oInfo->sign_icon);

                echo '<img src="' . $oInfo->sign_icon . '"/>';exit;
            } else {
                echo '无需处理';exit;
            }
        } else {
            echo '参数错误';exit;
        }
    }
}