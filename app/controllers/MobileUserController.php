<?php

class MobileUserController extends BaseController {

	CONST  PAGESIZE = 10;

	public function __construct(){
		$this->beforeFilter('mobileauth3');
	}

	/**
	 * 个人中心--首页
	 */
	public function getIndex()
	{
		//取得当前用户个人信息
		$oUser = User::find(Session::get('userid'));
		$iRoleId = Session::get('roleid');
		return View::make('mobile.user.index')->with('iRoleId',$iRoleId)->with('oUser',$oUser);
	}

	/**
	 * 个人中心-我的资料
	 */
	public function getUserInfo(){
		//取得当前用户个人信息
		$oUser = User::find(Session::get('userid'));
		//生成当前用户的注册二维码
		$sEwmUrl = AspirinEwm::getErweima('http://cdcma.bizconf.cn/mobile/register/'.$oUser->invite_code);
		if(!$oUser->user_company){
			$oUser->user_company = $oUser->user_company_name;
		}
		if($oUser->role_id==2){			//代表
			$aHosp1 = Hospital::getChildren();
			$aHosp2 = Hospital::getChildren($oUser->user_province);
			$sR = $oUser->user_regin;
			$sArea = User::getAreaCache();
			$oAreaAll = json_decode($sArea);
			$oArea = $oAreaAll->$sR;
			return View::make('mobile.user.userinfod')
				->with('oUser',$oUser)
				->with('aHosp1',$aHosp1)
				->with('aHosp2',$aHosp2)
				->with('oArea',$oArea)
				->with('ewm',$sEwmUrl);
		}elseif($oUser->role_id==3){		//医生
			$aHosp1 = Hospital::getChildren();
			$aHosp2 = Hospital::getChildren($oUser->user_province);
			$aHosp3 = Hospital::getChildren($oUser->user_city);
			$aHosp4 = Hospital::getChildren($oUser->user_county);
			return View::make('mobile.user.userinfo')
				->with('oUser',$oUser)
				->with('aHosp1',$aHosp1)
				->with('aHosp2',$aHosp2)
				->with('aHosp3',$aHosp3)
				->with('aHosp4',$aHosp4)
				->with('ewm',$sEwmUrl);
		}else{
			$sR = $oUser->user_regin;
			$sA = $oUser->user_area;
			if(is_numeric($sR)){
				$sRegin = User::getReginCache();
				$oRegin = json_decode($sRegin);
				$oUser->user_regin = $oRegin->$sR;
			}
			if(is_numeric($sA)){
				$sArea = User::getAreaCache();
				$oArea = json_decode($sArea);
				$oUser->user_area = $oArea->$sR->$sA;
			}
			$iHospitalId = $oUser->user_company;
			if(is_numeric($iHospitalId)){
				$sHospital = Hospital::where('id',$iHospitalId)->pluck('name');
				if($sHospital){
					$oUser->user_company = $sHospital;
				}
			}
			return View::make('mobile.user.userinfoa')
				->with('oUser',$oUser)
				->with('ewm',$sEwmUrl);
		}
		return View::make('mobile.user.userinfo')
			->with('oUser',$oUser)
			->with('ewm',$sEwmUrl);
	}
	
	/**
	 * 修改资料页面
	 */
	public function getUserInfoChange(){
		//取得当前用户个人信息
		$oUser = User::find(Session::get('userid'));
		if(!$oUser->user_company){
			$oUser->user_company = $oUser->user_company_name;
		}
		if($oUser->role_id==2){			//代表
			$aHosp1 = Hospital::getChildren();
			$aHosp2 = Hospital::getChildren($oUser->user_province);
			$sR = $oUser->user_regin;
			$sArea = User::getAreaCache();
			$oAreaAll = json_decode($sArea);
			$oArea = $oAreaAll->$sR;
			return View::make('mobile.user.editinfod')
				->with('oUser',$oUser)
				->with('aHosp1',$aHosp1)
				->with('aHosp2',$aHosp2)
				->with('oArea',$oArea);
		}elseif($oUser->role_id==3){		//医生
			$aHosp1 = Hospital::getChildren();
			$aHosp2 = Hospital::getChildren($oUser->user_province);
			$aHosp3 = Hospital::getChildren($oUser->user_city);
			$aHosp4 = Hospital::getChildren($oUser->user_county);
			return View::make('mobile.user.editinfo')
				->with('oUser',$oUser)
				->with('aHosp1',$aHosp1)
				->with('aHosp2',$aHosp2)
				->with('aHosp3',$aHosp3)
				->with('aHosp4',$aHosp4);
		}else{
			$sR = $oUser->user_regin;
			$sA = $oUser->user_area;
			if(is_numeric($sR)){
				$sRegin = User::getReginCache();
				$oRegin = json_decode($sRegin);
				$oUser->user_regin = $oRegin->$sR;
			}
			if(is_numeric($sA)){
				$sArea = User::getAreaCache();
				$oArea = json_decode($sArea);
				$oUser->user_area = $oArea->$sR->$sA;
			}
			$iHospitalId = $oUser->user_company;
			if(is_numeric($iHospitalId)){
				$sHospital = Hospital::where('id',$iHospitalId)->pluck('name');
				if($sHospital){
					$oUser->user_company = $sHospital;
				}
			}
			return View::make('mobile.user.editinfoa')
				->with('oUser',$oUser);
		}
		return View::make('mobile.user.editinfo')
			->with('oUser',$oUser);
	}
	
	/**
	 * 修改资料页面
	 */
	public function getUserPwdChange()
	{
		return View::make('mobile.user.editpwd');
	}

	/**
	 * 联系客服页面
	 */
	public function getContact()
	{
		return View::make('mobile.user.contact');
	}
	/**
	 * 我的积分
	 */
	public function getMyscore()
	{
		$iUid = Session::get('userid');
		$oUser = User::find($iUid);
		$iUserScore = $oUser->user_score;
		//累计观看直播时长
		$iLiveMin = VideoLog::getUserMinByType($iUid,2);
		//录播
		$iReviewMin = VideoLog::getUserMinByType($iUid,1);
		//获得所有该用户观看的录播和直播
		$oVideoLog = VideoLog::getUserVideoLog($iUid);
		return View::make('mobile.user.myscore')
			->with('oUser',$oUser)
			->with('iLiveMin',$iLiveMin)
			->with('iReviewMin',$iReviewMin)
			->with('oVideoLog',$oVideoLog);
	}

	public function getScoreList($iRoleId)
	{
		$oUser = User::find(Session::get('userid'));
		if (!in_array($iRoleId, array(2,3))){
			//取当前用户的role_id
			$iRoleId = $oUser->role_id;
		}
		//获取当前用户积分排行
		$iUserRank = User::getUserRankById($oUser->role_id,Session::get('userid'));
		//获取前一百名用户的积分排行
		$aUser = User::getUserScoreList($iRoleId,100,Session::get('userid'));
		return View::make('mobile.user.scorelist')
			->with('iUserRank',$iUserRank)
			->with('aUser',$aUser)
			->with('iRoleId',$iRoleId);
	}

	/**
	 * 线下会议列表页
	 * http://cdma.local/mobile-user/list
	 */
	public function getList(){
		$iUid = Session::get('userid');
		$iPagesize = self::PAGESIZE;
		$oOffline = AspirinOffline::orderby('created_at','desc')->paginate($iPagesize);
		foreach($oOffline as $k=>$v){
			$oApplyLog = AspirinOfflineApply::where('offline_id',$v->id)->where('user_id',$iUid)->first();
			$iApplyNum = AspirinOfflineApply::where('offline_id',$v->id)->count();
			if(count($oApplyLog)){
				$v ->hasapply = 1;  //已报名
				$v ->hasapplynum = $iApplyNum;  //已报名人数
			}else{
				$v ->hasapply = 2;  //已报名
				$v ->hasapplynum =$iApplyNum;//
			}
		}
		return View::make('mobile.user.offlinelist')->with('oOffline',$oOffline);
	}

	/*
	 * 线下会议加载更多
	 * /mobile-user/offline-add
	 * 
	 * */
	public function postOfflineAdd()
	{
		$iPage = Input::get('page');
		$iUid = Session::get('userid');
		$iPagesize = self::PAGESIZE;
		$iCount = AspirinOffline::count();
		$oOffline = AspirinOffline::orderby('created_at','desc')->skip(($iPage-1)*$iPagesize)->take($iPagesize)->get();
		$ceil = 0;
		$iConut = AspirinResearchApply::all()->count();
		if(ceil($iConut/$iPagesize) <= $iPage){
			$ceil = 1;
		}
		$aAspirinResearchApply = array();
		$aAspirinResearchApply['ceil'] = $ceil;
		foreach($oOffline as $k=>$value){
			$aAspirinResearchApply[$k]['applyid'] = $value->id;
			$aAspirinResearchApply[$k]['applytitle'] = $value-> offline_title;
			$aAspirinResearchApply[$k]['applydate'] = date('Y-m-d',strtotime($value->offline_start_time));
			$oApplyLog = AspirinOfflineApply::where('offline_id',$value->id)->where('user_id',$iUid)->first();
			$iApplyNum = $value->offline_least_gold;//消耗积分数
			if(count($oApplyLog)){
				$aAspirinResearchApply[$k]['applystatus'] = 1;
				$aAspirinResearchApply[$k]['applynum'] = $iApplyNum;
			}else{
				$aAspirinResearchApply[$k]['applystatus'] = 2;
				$aAspirinResearchApply[$k]['applynum'] = $iApplyNum;
			}
		}
		echo json_encode($aAspirinResearchApply);
	}

	/**
	 * 线下会议列表页
	 * http://cdma.local/mobile-user/offline-detail/id
	 */
	public function getOfflineDetail($iId){
		$oOffline = AspirinOffline::find($iId);
		$iUid = Session::get('userid');
		$oAspirinOfflineApply = AspirinOfflineApply::where('user_id',$iUid)->where('offline_id',$iId)->first();
		if(count($oAspirinOfflineApply)){
			$result = "hasjoin";
		}else{
			$result = "hasnojoin";
		}
		return View::make('mobile.user.offlinedetail')->with('oOffline',$oOffline)->with('result',$result);
	}

	/*
	 * 先下会议报名
	 * 
	 * /mobile-user/apply-check
	 * */
	public function postApplyCheck(){
		$iUid = Session::get('userid');
		$oUser = User::find($iUid);
		$iOfflineId = Input::get('id');
		$aMessage = array();
		$oAspirinOffline = AspirinOffline::find($iOfflineId);
		$iNeedScore = $oAspirinOffline->offline_least_gold;
		$iSelfScore = $oUser->user_score;
		if($iSelfScore < $iNeedScore){
			$aMessage = array('msg'=>'delete','show'=>'您的积分不足');
			return json_encode($aMessage);
		}else{
			$sNowTime = date('Y-m-d H:i:s',time());
			$oAspirinOfflineApply = AspirinOfflineApply::where('user_id',$iUid)->where('offline_id',$iOfflineId)->first();
			if(count($oAspirinOfflineApply)){
				//$oAspirinOfflineApply->delete();
				$aMessage = array('msg'=>'delete','show'=>'您已经参加会议');
				return json_encode($aMessage);
			}else{
				$sAspirinOfflineTime =  $oAspirinOffline ->apply_end_time;
				if(time() > strtotime($sAspirinOfflineTime)){
					$aMessage = array('msg'=>'false','show'=>'该会议的会议报名时间已经结束');
					return json_encode($aMessage);
				}
				$sAspirinOfflineTime =  $oAspirinOffline ->apply_start_time;
				if(time() < strtotime($sAspirinOfflineTime)){
					$aMessage = array('msg'=>'false','show'=>'该会议的会议报名尚未开始');
					return json_encode($aMessage);
				}
				$oAspirinOfflineApply = new AspirinOfflineApply();
				$oAspirinOfflineApply -> user_id= $iUid;
				$oAspirinOfflineApply -> offline_id= $iOfflineId;
				$oAspirinOfflineApply -> save();
				$oUser->user_score = $iSelfScore - $iNeedScore;
				$oUser-> save();
				$oAspirinUserGoldLog = new AspirinUserGoldLog;
				$oAspirinUserGoldLog-> user_id= $iUid;
				$oAspirinUserGoldLog-> gold_type =  3;
				$oAspirinUserGoldLog-> gold_count= $iNeedScore;
				$oAspirinUserGoldLog-> source_id= $iOfflineId;
				$oAspirinUserGoldLog-> save();
				$aMessage = array('msg'=>'success','show'=>'您已经成功完成报名');
				return json_encode($aMessage);
			}
		}
	}

	/*
	 * 个人中心--消息通知--列表
	 * /mobile-user/notice-list
	 * 
	 * 
	 * */
	public function getNoticeList()
	{
		$iUid = Session::get('userid');
		//$iUid = 25;
		$oAspirinUserNotice = AspirinUserNotice::where('user_id',$iUid)->orderBy('created_at','desc')->take(self::PAGESIZE)->get();
		return View::make("mobile.user.noticelist")->with('iUid',$iUid)->with('oAspirinUserNotice',$oAspirinUserNotice);
	}

	/*
	 * /mobile-user/notice-add
	 * 个人中心--消息通知---滑屏加载功能
	 * */
	public function postNoticeAdd(){
		$iPage = Input::get('page');
		$iPagesize = self::PAGESIZE;
		$iUid = Session::get('userid');
		//$iUid = 25;
		$iCount = AspirinUserNotice::where('user_id',$iUid)->count();
		$oAspirinUserNotice = AspirinUserNotice::where('user_id',$iUid)->orderby('created_at','desc')->skip(($iPage-1)*$iPagesize)->take($iPagesize)->get();
		$ceil = 0;
		if(ceil($iCount/$iPagesize) <= $iPage){
			$ceil = 1;
		}
		$aAspirinUserNotice = array();
		$aAspirinUserNotice['ceil'] = $ceil;
		foreach($oAspirinUserNotice as $k=>$value){
			$aAspirinUserNotice[$k]['id'] = $value-> id;
			$aAspirinUserNotice[$k]['notice_content'] = $value-> notice_content;
			$aAspirinUserNotice[$k]['date'] = date('Y-m-d',strtotime($value->created_at));
			$aAspirinUserNotice[$k]['has_read'] = $value->has_read;
		}
		echo json_encode($aAspirinUserNotice);
	}

	/*
	 * /mobile-user/notice-jump
	 * 
	 * */
	public function getNoticeJump($iId)
	{
		$oAspirinUserNotice = AspirinUserNotice::find($iId);
		$oAspirinUserNotice ->has_read = 1;
		$oAspirinUserNotice -> save();
		$iNoticeType = $oAspirinUserNotice->notice_type;
		$aType = array('11','12','13','14','15','31','41','42');
		if(in_array($iNoticeType,$aType)){
			switch($iNoticeType){
				case 11:
					//在线会议，听者报名参会或取消参会给讲者发送推送通知，点击进入我的会议-主讲会议  列表
					return Redirect::to("/mobile-aspirin-online/my-enter-list");
					break;
				case 12:
					//在线会议，讲者邀请医生参加会议，给听者发送推送通知，点击进入我的会议-参加会议  列表
					return Redirect::to("/mobile-aspirin-online/my-attend-list");
					break;
				case 13:
					//在线会议，每周一早上六点给讲者推送可以报名讲课的通知，点击进入在线会议首页
					return Redirect::to("/mobile-aspirin-online/index");
					break;
				case 14:
					//在线会议，每周一早上六点给听者推送可以报名参会的通知，点击进入在线会议首页 ; 或者，讲者取消会议，给听者发送推送通知  点击进入听者报名列表页
					if($oAspirinUserNotice->detail_id != 0){
						return Redirect::to("/mobile-aspirin-online/listener-list/".$oAspirinUserNotice->detail_id);
					}else{
						return Redirect::to("/mobile-aspirin-online/index");
					}
					break;
				case 15:
					//在线会议，讲者报名成功给同城市所有医生发送推送通知，点击进入听者报名列表页
					if($oAspirinUserNotice->detail_id != 0){
						return Redirect::to("/mobile-aspirin-online/listener-list/".$oAspirinUserNotice->detail_id);
					}else{
						return Redirect::to("/mobile-aspirin-online/index");
					}
					break;
				case 31:
					//科研申请，审核成功或者失败发送推送通知，点击进入科研申请  列表
					return Redirect::to("/aspirinfund/application-researchlist");
					break;
				case 41:
					//线下会议，报名成功，点击进入线下会议列表
					return Redirect::to("/mobile-user/list");
					break;
				case 42:
					//线下会议，报名失败，说明失败原因，点击进入线下会议列表
					return Redirect::to("/mobile-user/list");
					break;
			}
		}else{
			return Redirect::to("/mobile-aspirin");
		}
	}
	
}