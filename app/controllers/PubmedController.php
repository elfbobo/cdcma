<?php
/**
 * Pubmed操作类
 * @author ghy
 *
 */
class PubmedController extends BaseController {
	CONST  PAGESIZE = 10;
	public $sChar = '';
	public $iUserid;
	public $sAppPath;
	public function __construct(){
	   	$this->iUserid = Session::get('userid');
		$this->sAppPath = app_path();
	}
	public function getIndex(){
		//初始化操作符（and or not）
	    foreach ($this->__getBoolean() as $key=>$val){
	       $this->sChar.= '<option value="'.$key.'">'.$val.'</option>';
	    }
	    //初始化select选择
	    $fi_f0 = "MeSH Terms";
		$fi_f1 = "Journal";
		$fi_f2 = "Title";
		$fi_f3 = "Title/Abstract";
		$fi_f4 = "Author";
		$sel0 = $sel1 = $sel2 = $sel3 = $sel4 = "";
	    foreach ($this->__getAllFields() as $key=>$val){
	        if($key == $fi_f0){
		        $sel0.='<option value="'.$key.'" selected>'.$val.'</option>';
			}else{
				$sel0.='<option value="'.$key.'">'.$val.'</option>';
			}
			if($key == $fi_f1){
				$sel1.='<option value="'.$key.'" selected>'.$val.'</option>';
			}else{
				$sel1.='<option value="'.$key.'">'.$val.'</option>';
			}
		
			if($key == $fi_f2){
				$sel2.='<option value="'.$key.'" selected>'.$val.'</option>';
			}else{
				$sel2.='<option value="'.$key.'">'.$val.'</option>';
			}
		
			if($key == $fi_f3){
				$sel3.='<option value="'.$key.'" selected>'.$val.'</option>';
			}else{
				$sel3.='<option value="'.$key.'">'.$val.'</option>';
			}
		
			if($key == $fi_f4){
				$sel4.='<option value="'.$key.'" selected>'.$val.'</option>';
			}else{
				$sel4.='<option value="'.$key.'">'.$val.'</option>';
			}
	    }
	    
	    //文献类型限定
		$sDocType = '';
		foreach ($this->__getDocType() as $key=>$val){
			if ($key == 'Review'){
				$sDocType.= '<div><input type="checkbox" value="'.$key.'" name="arctcle_type">'.$val.'</div>';
				$sDocType.= '<h5>更多...</h5>';
			}else{
				$sDocType.= '<div><input type="checkbox" value="'.$key.'" name="arctcle_type">'.$val.'</div>';
			}
		}
		
	    //语种限定多选框初始化
		$sLang = '';
		foreach ($this->__getLanguage() as $key=>$val){
			$sLang.='<div><input type="checkbox" value="'.$key.'" name="language">'.$val.'</div>';
		}
		
	    //子文档限定多选框初始化
		$sSubset = '<h5>期刊群组</h5>';
		foreach ($this->__getSubDocType() as $key=>$val){
			if ($key == 'tox[sb]'){
				$sSubset.='<div><input type="checkbox" value="'.$key.'" name="subset">'.$val.'</div>';
				$sSubset.= '<h5>更多...</h5>';
			}elseif ($key == 'jsubsetd[text]'){
				$sSubset.='<div><input type="checkbox" value="'.$key.'" name="subset">牙医学期刊</div>';
				$sSubset.='<div><input type="checkbox" value="'.$key.'" name="subset">'.$val.'</div>';
				$sSubset.= '<h5>主题分类</h5>';
			}else{
				$sSubset.='<div><input type="checkbox" value="'.$key.'" name="subset">'.$val.'</div>';
			}
		}
		
		//研究对象年龄限定多选框初始化
		$sAge = '';
		foreach ($this->__getAge() as $key=>$val){
			$sAge.='<div><input type="checkbox" value="'.$key.'" name="age">'.$val.'</div>';
		}
		return View::make('front.pubmed.index')
							->with('sChar',$this->sChar)
							->with('sel0',$sel0)
							->with('sel1',$sel1)
							->with('sel2',$sel2)
							->with('sel3',$sel3)
							->with('sel4',$sel4)
							->with('sDocType',$sDocType)
							->with('sLang',$sLang)
							->with('sAge',$sAge)
							->with('sSubset',$sSubset);
	}
	
	 /**
	 * @desc ajax操作
	 */
    public function postAjax(){
        //$this->removeBadCharacters();
        $type = Input::get('type');
	    if ($type != 'ajax'){
	      return false;
	    }
	    $action = trim(Input::get('action'));
    	require ( $this->sAppPath.'/extends/pubmed/PubMedAPI.php' );
    	$PubMedAPI = new PubMedAPI();
	    switch ($action){
	      case 'advancedSearch':
	        //$search_term = !empty($_POST['search_term']) ? stripslashes(urldecode(trim($_POST['search_term']))) : '' ;
        	$r_f0 = trim($_POST['r_f0']);
			$fi_f0 = trim($_POST['fi_f0']);
			$keyword_fi0 = !empty($_POST['keyword_fi0']) ? stripslashes(urldecode(trim($_POST['keyword_fi0']))) : '' ;
			
			$r_f1 = trim($_POST['r_f1']);
			$fi_f1 = trim($_POST['fi_f1']);
			$keyword_fi1 = !empty($_POST['keyword_fi1']) ? stripslashes(urldecode(trim($_POST['keyword_fi1']))) : '' ;
			
			$r_f2 = trim($_POST['r_f2']);
			$fi_f2 = trim($_POST['fi_f2']);
			$keyword_fi2 = !empty($_POST['keyword_fi2']) ? stripslashes(urldecode(trim($_POST['keyword_fi2']))) : '' ;
			
			$r_f3 = trim($_POST['r_f3']);
			$fi_f3 = trim($_POST['fi_f3']);
			$keyword_fi3 = !empty($_POST['keyword_fi3']) ? stripslashes(urldecode(trim($_POST['keyword_fi3']))) : '' ;
			
			$r_f4 = trim($_POST['r_f4']);
			$fi_f4 = trim($_POST['fi_f4']);
			$keyword_fi4 = !empty($_POST['keyword_fi4']) ? stripslashes(urldecode(trim($_POST['keyword_fi4']))) : '' ;
			
			$jour_cn = !empty($_POST['jour_cn']) ? stripslashes(urldecode(trim($_POST['jour_cn']))) : '' ;
			$auth_cn = !empty($_POST['auth_cn']) ? stripslashes(urldecode(trim($_POST['auth_cn']))) : '' ;
			
			$term_hasabstract = !empty($_POST['term_hasabstract']) ? stripslashes(urldecode(trim($_POST['term_hasabstract']))) : '' ;
			$term_loattrfull = !empty($_POST['term_loattrfull']) ? stripslashes(urldecode(trim($_POST['term_loattrfull']))) : '' ;
			$term_loattrfree = !empty($_POST['term_loattrfree']) ? stripslashes(urldecode(trim($_POST['term_loattrfree']))) : '' ;
			$term_isReview = !empty($_POST['term_isReview']) ? stripslashes(urldecode(trim($_POST['term_isReview']))) : '' ;
			$term_isF1000M = !empty($_POST['term_isF1000M']) ? stripslashes(urldecode(trim($_POST['term_isF1000M']))) : '' ;
			
			$fromDate = !empty($_POST['fromDate']) ? stripslashes(urldecode(trim($_POST['fromDate']))) : 1 ;
			$toDate = !empty($_POST['toDate']) ? stripslashes(urldecode(trim($_POST['toDate']))) : 3000 ;
			
			$updateDate = !empty($_POST['updateDate']) ? trim($_POST['updateDate']) : 0 ;
		
			$display_sort = !empty($_POST['display_sort']) ? stripslashes(trim($_POST['display_sort'])) : '' ;
			 
			$display_rowsPerPage = !empty($_POST['display_rowsPerPage']) ? trim($_POST['display_rowsPerPage']) : 0 ;
			
			$humans_animals = !empty($_POST['humans_animals']) ? stripslashes(trim($_POST['humans_animals'])) : '' ;
			
			$gender = !empty($_POST['gender']) ? stripslashes(trim($_POST['gender'])) : '' ;
			
			$arctcle_type = !empty($_POST['arctcle_type']) ? stripslashes(trim($_POST['arctcle_type'])) : '' ;
			
			$language = !empty($_POST['language']) ? stripslashes(trim($_POST['language'])) : '' ;
			
			$subset = !empty($_POST['subset']) ? stripslashes(trim($_POST['subset'])) : '' ;
			
			$age = !empty($_POST['age']) ? stripslashes(trim($_POST['age'])) : '' ;
			
			$term = '';
			
			//快速检索框输入条件处理
//			if ($search_term){
//				if($term){
//					$term.="AND" . " (".$search_term.") ";
//			    }else{
//					$term.="(".$search_term.") ";
//				}
//			}
			//中国期刊中文名称条件处理 
			if ($jour_cn){
			     $oJournal = PubmedJournalName::where('cn_name', "$jour_cn")->first();
			     if(count($oJournal)>0){
				     $cn_name = $oJournal->en_name;
				     if (!empty($cn_name)){
					        $cn_name = stripslashes($cn_name);
					        if($term){
							   $term.="AND\"".$cn_name."\"[Journal] ";
					        }else{
							   $term.="\"".$cn_name."\"[Journal] ";
						    }
				     }
			     }
			}
			
			//中国作者中文名称条件处理
            if (!empty($auth_cn)){
                require ( $this->sAppPath.'/extends/pinyin/ConPinYin.php' );
			    $oPinYin = new ConPinYin();
                $auth_en = iconv('gbk','utf-8//IGNORE',strtoupper($oPinYin->PinYin(mb_convert_encoding($auth_cn,'gbk','utf-8'))));
			}
			
			if (!empty($auth_en)){
				if($term){
					$term.="AND" . " (".$auth_en."[Author]) ";
			    }else{
					$term.="(".$auth_en."[Author]) ";
				}
			}
			//第一个检索词框处理
		    if($keyword_fi0){
		    	if($fi_f0 == "All Fields"){
			    	if($term){
						$term.=$r_f0 . " (".$keyword_fi0.") ";
					}else{
						$term.="(".$keyword_fi0.") ";
					}
		    	}else{
			        if($term){
						$term.=$r_f0 . " (".$keyword_fi0."[".$fi_f0."]) ";
					}else{
						$term.="(".$keyword_fi0."[".$fi_f0."]) ";
					}
		    	}
			}
			//第二个检索词框处理
	    	if($keyword_fi1){
		    	if($fi_f1 == "All Fields"){
			    	if($term){
						$term.=$r_f1 . " (".$keyword_fi1.") ";
					}else{
						$term.="(".$keyword_fi1.") ";
					}
		    	}else{
			        if($term){
						$term.=$r_f1 . " (".$keyword_fi1."[".$fi_f1."]) ";
					}else{
						$term.="(".$keyword_fi1."[".$fi_f1."]) ";
					}
		    	}
			}
			//第三个检索词框处理
	    	if($keyword_fi2){
		    	if($fi_f2 == "All Fields"){
			    	if($term){
						$term.=$r_f2 . " (".$keyword_fi2.") ";
					}else{
						$term.="(".$keyword_fi2.") ";
					}
		    	}else{
			        if($term){
						$term.=$r_f2 . " (".$keyword_fi2."[".$fi_f2."]) ";
					}else{
						$term.="(".$keyword_fi2."[".$fi_f2."]) ";
					}
		    	}
			}
			//第四个检索词框处理
	    	if($keyword_fi3){
		    	if($fi_f3 == "All Fields"){
			    	if($term){
						$term.=$r_f3 . " (".$keyword_fi3.") ";
					}else{
						$term.="(".$keyword_fi3.") ";
					}
		    	}else{
			        if($term){
						$term.=$r_f3 . " (".$keyword_fi3."[".$fi_f3."]) ";
					}else{
						$term.="(".$keyword_fi3."[".$fi_f3."]) ";
					}
		    	}
			}
			//第五个检索词框处理
	    	if($keyword_fi4){
		    	if($fi_f4 == "All Fields"){
			    	if($term){
						$term.=$r_f4 . " (".$keyword_fi4.") ";
					}else{
						$term.="(".$keyword_fi4.") ";
					}
		    	}else{
			        if($term){
						$term.=$r_f4 . " (".$keyword_fi4."[".$fi_f4."]) ";
					}else{
						$term.="(".$keyword_fi4."[".$fi_f4."]) ";
					}
		    	}
			}
			//选择有摘要处理
            if ($term_hasabstract){
				if($term){
					$term.="AND " . $term_hasabstract." ";
				}else{
					$term.=$term_hasabstract." ";
				}
			}
			//选择有全文链接处理
            if ($term_loattrfull){
				if($term){
					$term.="AND " . $term_loattrfull." ";
				}else{
					$term.=$term_loattrfull." ";
				}
			}
			 //选择有免费全文链接处理 
            if ($term_loattrfree){
				if($term){
					$term.="AND " . $term_loattrfree." ";
				}else{
					$term.=$term_loattrfree." ";
				}
			}
			
			 //选择文献类型为综述 处理	
            if ($term_isReview){
				if($term){
					$term.="AND " . $term_isReview." ";
				}else{
					$term.=$term_isReview." ";
				}
			} 
		    //选择被F1000M收录处理	
			if ($term_isF1000M){
				if($term){
					$term.="AND " . $term_isF1000M." ";
				}else{
					$term.=$term_isF1000M." ";
				}
			} 
			
			//出版日期条件处理
		    $fromDate = $this->__formatDate($fromDate);
		    $toDate = $this->__formatDate($toDate);
		    if ($fromDate != 1 || $toDate != 3000){
				if($term){
					$term.="AND (\"" . $fromDate."\"[PDat]:\"".$toDate."\"[PDat]) ";
				}else{
					$term.="(\"" . $fromDate."\"[PDat]:\"".$toDate."\"[PDat]) ";
				}
		    }
			
			//PubMed 收录日期条件处理
			if ($updateDate){
				if ($updateDate ==30 || $updateDate == 60 || $updateDate == 90 || $updateDate == 180){
				    $startDate = mktime(0,0,0,date('m') ,date('d')-$updateDate,date('Y'));
				    $endDate = mktime(0,0,0,date('m') ,date('d'),date('Y'));
				}else{
				    $startDate = mktime(0,0,0,date('m') ,date('d'),date('Y')-$updateDate);
				    $endDate = mktime(0,0,0,date('m') ,date('d'),date('Y'));
				}
				$startDate = $this->__formatDate(date('Y-m-d', $startDate));
				$endDate = $this->__formatDate(date('Y-m-d', $endDate));
				
			    if($term){
					$term.="AND (\"" . $startDate."\"[EDat]:\"".$endDate."\"[EDat]) ";
				}else{
					$term.="(\"" . $startDate."\"[EDat]:\"".$endDate."\"[EDat]) ";
				}
				
			}
			$where='';
			//排序条件处理
			if ($display_sort){
				 
			     $PubMedAPI->sort = $display_sort;
			     //$PubMedAPI->use_cache = FALSE;
			     $where.= $display_sort;
			}
			
			//每页显示的记录数处理
			if ($display_rowsPerPage && $display_rowsPerPage != 20){
			    
				$pageSize = intval($display_rowsPerPage);
				$PubMedAPI->retmax = $pageSize;
				//$PubMedAPI->use_cache = FALSE;
				$where.= $pageSize;
			}
			//研究对象人类、动物限定
			if ($humans_animals){
			    $sH_a = "";
			    $aH_a = explode("|", $humans_animals);
			    foreach ($aH_a as $key=>$val){
			    	if ($key == 0){
			    	   $tag = "MeSH Terms";
			    	}else{
			    	   $tag = "MeSH Terms:noexp";
			    	}
			    	if ($sH_a){
			    	    $sH_a.=" OR ";
			    	}
			    	$sH_a.="\"".$val."\"[".$tag."]";
			    }
			    
        		if($term){
					$term.="AND (".$sH_a.") ";
				}else{
					$term.="(".$sH_a.") ";
				}
			}
			//研究对象性别限定
     	    if ($gender){
			    $sGender = "";
			    $aGender = explode("|", $gender);
			    $tag = 'MeSH Terms';
			    foreach ($aGender as $key=>$val){
			    	if ($sGender){
			    	    $sGender.=" OR ";
			    	}
			    	$sGender.="\"".$val."\"[".$tag."] ";
			    }
			    
        		if($term){
					$term.="AND (".$sGender.") ";
				}else{
					$term.="(".$sGender.") ";
				}
			}
			//文献类型限定条件
       	    if ($arctcle_type){
			    $sA_t = "";
			    $aA_t = explode("|", $arctcle_type);
			    $tag = 'ptyp';
			    foreach ($aA_t as $key=>$val){
			    	if ($sA_t){
			    	    $sA_t.=" OR ";
			    	}
			    	$sA_t.= $val."[".$tag."]";
			    }
			    
        		if($term){
					$term.="AND (".$sA_t.") ";
				}else{
					$term.="(".$sA_t.") ";
				}
			}
			//语种限定条件
            if ($language){
			    $sLan = "";
			    $aLan = explode("|", $language);
			    $tag = 'lang';
			    foreach ($aLan as $key=>$val){
			    	if ($sLan){
			    	    $sLan.=" OR ";
			    	}
			    	$sLan.= $val."[".$tag."]";
			    }
			    
        		if($term){
					$term.="AND (".$sLan.") ";
				}else{
					$term.="(".$sLan.") ";
				}
			}
			
			//子文档限定条件
            if ($subset){
			    $sSubset = "";
			    $aSubset = explode("|", $subset);
			    $tag = '';
			    foreach ($aSubset as $key=>$val){
			    	if ($sSubset){
			    	    $sSubset.=" OR ";
			    	}
			    	$sSubset.= $val;
			    }
			    
        		if($term){
					$term.="AND (".$sSubset.") ";
				}else{
					$term.="(".$sSubset.") ";
				}
			}
		   //研究对象年龄限定条件
            if ($age){
			    $sAge = "";
			    $aAge = explode("|", $age);
			    $tag = '';
			    foreach ($aAge as $key=>$val){
			    	if ($sAge){
			    	    $sAge.=" OR ";
			    	}
			    	$sAge.= $val;
			    }
			    
        		if($term){
					$term.="AND (".$sAge.") ";
				}else{
					$term.="(".$sAge.") ";
				}
			}
		    $page = (int)$_POST['page'];
			if (!empty($page)) {
				$PubMedAPI->retstart = $PubMedAPI->retmax*((int)$page - 1)+1;
				if ($PubMedAPI->retstart == 1){
				   $PubMedAPI->retstart = 0;
				}else{
				  $PubMedAPI->retstart = intval($PubMedAPI->retstart) -1;
				}
			}
			$results = array();
            $loattrfreeTotal = $reviewTotal = $total = 0;
            $aData = array();
			if($term){
			    $results = $PubMedAPI->query($term, false, false, $where);
				if($results){
				   foreach ($results as $key=>$result){
					        $aData[$key]['pmid']=$result['pmid'];
					        $aData[$key]['title']=isset($result['title']) ? $result['title'] : '';
					        $aData[$key]['journal']=isset($result['journal']) ? $result['journal'] : '';
					        $aData[$key]['year']=isset($result['year']) ? $result['year'] : '';
					        $aData[$key]['month']=isset($result['month']) ? $result['month'] : '';
					        $aData[$key]['volume']=isset($result['volume']) ? $result['volume'] : '';
					        $aData[$key]['issue']=isset($result['issue']) ? $result['issue'] : '';
					        $aData[$key]['pages']=isset($result['pages']) ? $result['pages'] : '';
					        $aData[$key]['authors']=isset($result['authors']) ? implode(", ",$result['authors']) : '';
					        $aData[$key]['helptext'] = $result['pmid']."||||,文章标题为：".
					                                $aData[$key]['title'].",文章出处为：".
					                                $aData[$key]['journal'].",".
					                                $aData[$key]['year'].$aData[$key]['month'].",".
					                                $aData[$key]['volume'].":".
					                                $aData[$key]['pages'].",作者为: ".$aData[$key]['authors'];
		            }
				}
			    $total = $PubMedAPI->count;
			}
			//printr($aData);die;
			$pageSize = 0;
			$pageSize = $pageSize ? $pageSize : $PubMedAPI->retmax;
			
			$pages = $this->ajaxPages($total, $pageSize, $page);
		    return View::make('front.pubmed.list')
		                        ->with('results',$aData)
		                        ->with('total',$total)
		                        ->with('pageSize',$pageSize)
		                        ->with('page',$page)
		                        ->with('pages',$pages)
		                        ->with('term',$term);
		    break;
		    
		    case 'help_one_text':
		    	  $aHelpInfo = array();
		          $oUser = User::where('id', $this->iUserid)->first();
		          //用户邮箱地址为空
	              if(!$oUser->user_email){
		             echo 1;die;  
	              } 
	              //用户积分不足
// 	              if(intval($oUser->user_credit_count) < EN_HELP_SCORE){
// 		             echo 2;die;  
// 	              }
	             $sHelpText = Input::get('helptext');
	             $sMatch = '||||';
	             $replace = ""; 
	             $aHelpText = explode($sMatch, $sHelpText);
	             $iPubmedId = intval($aHelpText[0]);
	             $sHelpText = str_replace($sMatch, $replace, $sHelpText);
				 //邮件主题
				 $subject = "国卫健康云Pubmed求助全文";
				 $content = "国卫健康云Pumed检索需要获取全文,文献PMID为：".$sHelpText.", 请将全文发送到邮箱：".$oUser->user_email;
				 $pmid = PubmedHelpMail::where('userid', $this->iUserid)->where('pmid', $iPubmedId)->pluck('pmid');
		         if(!$pmid){	
                        //插入求助记录
                  	    $aHelpInfo['userid'] = $this->iUserid; 
			            $aHelpInfo['email'] = addslashes($oUser->user_email);
						$aHelpInfo['pmid'] = $iPubmedId;
						$aHelpInfo['title'] = addslashes($subject);
						$aHelpInfo['content'] = addslashes($content);
						$aHelpInfo['device_type'] = 0;
						$aHelpInfo['pub_type'] = 1;
// 						$aHelpInfo['posttime'] = time();
           			    $oPmdHelpMail = new PubmedHelpMail($aHelpInfo);
					    $oPmdHelpMail->save();
					    PubmedHelpMail::SendEmail($subject,$content);
					    //扣除积分
// 					    $this->oScoreService->getIndex(6,"PMD_EN_HELP",$iPubmedId, $this->iUserid, "web");
				 }else{
				    echo 3;die; //已经求助过此文献
				 }
				 echo 4; die;
				 break;
			
		    case 'helpText':
		    	$aHelpInfo = array();
	            $oUser = User::where('id', $this->iUserid)->first();
			    if(!$oUser->user_email){
	                  echo 1;  //用户邮箱地址为空
	                  die;
	            } 
	            $sHelpTextsMatch = '||||||';
	            $aHelpTexts = explode($sHelpTextsMatch, trim(Input::get('helptexts')));
// 	            if(intval($oUser->user_credit_count) < intval(EN_HELP_SCORE*count($aHelpTexts))){
//                       echo 2; die; //用户积分不足
//                 }
                $sMatch = '||||';
	            $replace = ""; 
	                foreach ($aHelpTexts as $key=>$sHelpText){
					     $aHelpText = explode($sMatch, $sHelpText);
			             $iPubmedId = intval($aHelpText[0]);
			             $sHelpText = str_replace($sMatch, $replace, $sHelpText);
						 //邮件主题
						 $subject = "国卫健康云Pubmed求助全文";
						 $content = "国卫健康云Pumed检索需要获取全文,文献PMID为：".$sHelpText.", 请将全文发送到邮箱：".$oUser->user_email;
						 $pmid = PubmedHelpMail::where('userid', $this->iUserid)->where('pmid', $iPubmedId)->pluck('pmid');
				         if(!$pmid){	
		                        //插入求助记录
		                  	    $aHelpInfo['userid'] = $this->iUserid; 
					            $aHelpInfo['email'] = addslashes($oUser->user_email);
								$aHelpInfo['pmid'] = $iPubmedId;
								$aHelpInfo['title'] = addslashes($subject);
								$aHelpInfo['content'] = addslashes($content);
								$aHelpInfo['device_type'] = 0;
								$aHelpInfo['pub_type'] = 1;
// 								$aHelpInfo['posttime'] = time();
		           			    $oPmdHelpMail = new PubmedHelpMail($aHelpInfo);
							    $oPmdHelpMail->save();
							    PubmedHelpMail::SendEmail($subject,$content);
							    //扣除积分
// 							    $this->oScoreService->getIndex(6,"PMD_EN_HELP",$iPubmedId, $this->iUserid, "web");
						 }
				    }
				    echo 4; 
				    die;
				 break;
            }
    }
    
    private function  __mbStringtoArray($str,$charset) {
        $strlen=mb_strlen($str);
        while($strlen){
            $array[]=mb_substr($str,0,1,$charset);
            $str=mb_substr($str,1,$strlen,$charset);
            $strlen=mb_strlen($str);
        }
        return $array;
    }
    
    private function __formatDate($date){
		   $str = str_replace('-', '/', $date);
		   return $str;
	}
	
	public	function ajaxPages($num, $perpage, $curpage) {
			$multipage = '';
			if($num > $perpage) {
				$page = 7;
				$offset = 2;
				$pages = @ceil($num / $perpage);
				$from = $curpage - $offset;
				$to = $curpage + $offset;
				$more = 0;
				if($page > $pages) {
					$from = 1;
					$to = $pages-1;
				} else {
					if($from <= 1)
						{
							$to = $page;
							$from = 1;
						}
						elseif($to >= $pages)
						{
							$from = $pages-($page-2);
							$to = $pages;
						}
						$more = 1;
				}
				$multipage.="<ul>";
				
				if($curpage == 1){
// 				    $multipage .= "<li><span class='sele' title='首页'>首页</span> </li>";
				    $multipage .= "<li><a class='sele' href='#'>上一页</a> </li>";
				}else{
// 				   $multipage .= "<li><a href='javascript:void(0);' rel='1'>首页</a> </li>";
				   $multipage .= "<li><a href='javascript:void(0);' rel='".($curpage-1)."'>上一页</a> </li>";
				}
				if ($curpage>6 && $more){
				    $multipage .= "<li><a href='javascript:void(0);' rel='1'>1</a>.. </li>";
				}
				for($i = $from; $i <= $to; $i++) {
					if($i != $curpage) {
						   $multipage .= "<li><a href='javascript:void(0);' rel='".$i."'>$i</a></li> ";
					} else {
						$multipage .= "<li><a class='sele' href='#'>$i</a></li> ";
					}
				}
				if($curpage <= $pages) {
					if($curpage<$pages-5 && $more){
						$multipage .= "<li>..<a href='javascript:void(0);' rel='".($pages)."'> $pages</a> </li>";
					}else{
						if($curpage!=$pages){
						    $multipage .= "<li><a href='javascript:void(0);' rel='".($pages)."'>$pages</a> </li>";
						}
					}
				}
				
			    if($curpage == $pages){
				    $multipage .= "<li><a class='sele' href='#'>下一页</a> </li>";
// 				    $multipage .= "<li><span class='sele' title='尾页'>尾页</span> </li>";
				}else{
				   $multipage .= "<li><a href='javascript:void(0);' rel='".($curpage+1)."'>下一页</a></li>";
// 				   $multipage .= "<li><a href='javascript:void(0);' rel='".($pages)."'>尾页</a> </li>";
				}
//				if($to = $pages) {
//					$multipage .= "<a href='javascript:void(0);' rel='$pages' class='bk'>尾页</a> ";
//				}
				$multipage .="</ul>";
// 				$multipage .="<span class='flip_total'>共{$pages}页{$num}条</span> ";
				$multipage .="<div class='clear'></div>";
			}
			return $multipage;
		}
		
		public function getShow($iPmid){
		     $this->removeBadCharacters();
		     $sAppPath = app_path(); 
	    	 require ( $sAppPath.'/extends/pubmed/PubMedAPI.php' );
		     $oPmd = new PubMedAPI();
		     if (!empty($iPmid)){
					 $result = $oPmd->query_pmid(trim($iPmid));
				     if (!empty($result)){
					          $aData['pmid'] = isset($result[0]['pmid']) ? $result[0]['pmid'] : '';
					          $aData['title'] = isset($result[0]['title']) ? $result[0]['title'] : '';
					          $aData['journalabbrev'] = isset($result[0]['journalabbrev']) ? $result[0]['journalabbrev']:'';
					          $aData['year'] = isset($result[0]['year']) ? $result[0]['year']:'';
					          $aData['month'] = isset($result[0]['month']) ? $result[0]['month']:'';
					          $aData['volume'] = isset($result[0]['volume']) ? $result[0]['volume']:'';
					          $aData['day'] = isset($result[0]['day']) ? $result[0]['day']:'';
					          $aData['issue'] = isset($result[0]['issue']) ? $result[0]['issue']:'';
					          $aData['pages'] = isset($result[0]['pages']) ? $result[0]['pages']:'';
					          $aData['journal'] = isset($result[0]['journal']) ? $result[0]['journal']:'';
					          if (is_array($result[0]['authors'])){ 
						  	         $aData['authors']=implode(', ', $result[0]['authors']);
						      }else{
						        	 $aData['authors']=isset($result[0]['authors']) ? $result[0]['authors'] : '';
						      }
					          $aData['affiliation'] = isset($result[0]['affiliation'])?$result[0]['affiliation']:'';
					          if (is_array($result[0]['keywords'])){ 
						  	         $aData['keywords']=implode(', ', $result[0]['keywords']);
						      }else{
						        	 $aData['keywords']=isset($result[0]['keywords']) ? $result[0]['keywords'] : '';
						      }
					          $aData['abstract'] = isset($result[0]['abstract']) ? $result[0]['abstract'] : '';
					          $aData['helptext'] = $iPmid."||||,文章标题为：".
					                                $aData['title'].",文章出处为：".
					                                $aData['journal'].",".
					                                $aData['year'].$aData['month'].",".
					                                $aData['volume'].":".
					                                $aData['pages'].",作者为: ".$aData['authors'];
					         $aData['helptext'] = addslashes($aData['helptext']);
		              }
		    }
		    return View::make('front.pubmed.show')
		                        ->with('result',$aData);
		}
		
/**
	 * @desc 过滤字符
	 * Enter description here ...
	 */
    public function removeBadCharacters(){
		global $bad_utf8_chars;
		$bad_utf8_chars = array(
		                       "\0", 
		                       "\xc2\xad",
		                       "\xcc\xb7", 
		                       "\xcc\xb8", 
		                       "\xe1\x85\x9F", 
		                       "\xe1\x85\xA0", 
		                       "\xe2\x80\x80", 
		                       "\xe2\x80\x81", 
		                       "\xe2\x80\x82", 
		                       "\xe2\x80\x83", 
		                       "\xe2\x80\x84", 
		                       "\xe2\x80\x85", 
		                       "\xe2\x80\x86", 
		                       "\xe2\x80\x87", 
		                       "\xe2\x80\x88", 
		                       "\xe2\x80\x89", 
		                       "\xe2\x80\x8a", 
		                       "\xe2\x80\x8b", 
		                       "\xe2\x80\x8e", 
		                       "\xe2\x80\x8f", 
		                       "\xe2\x80\xaa", 
		                       "\xe2\x80\xab", 
		                       "\xe2\x80\xac", 
		                       "\xe2\x80\xad", 
		                       "\xe2\x80\xae", 
		                       "\xe2\x80\xaf", 
		                       "\xe2\x81\x9f", 
		                       "\xe3\x80\x80", 
		                       "\xe3\x85\xa4", 
		                       "\xef\xbb\xbf", 
		                       "\xef\xbe\xa0", 
		                       "\xef\xbf\xb9", 
		                       "\xef\xbf\xba", 
		                       "\xef\xbf\xbb", 
		                       "\xE2\x80\x8D"
		                       );
		function _remove_bad_characters($array) {
			global $bad_utf8_chars;
			return is_array($array) ? array_map('_remove_bad_characters', $array) : str_replace($bad_utf8_chars, '', $array);
		}

		$_GET = _remove_bad_characters($_GET);
		$_POST = _remove_bad_characters($_POST);
		$_COOKIE = _remove_bad_characters($_COOKIE);
		$_REQUEST = _remove_bad_characters($_REQUEST);
	}
	
	/**
	 * @desc 定义检索字段
	 * @return  array;
	 */
	private function __getAllFields(){
	    return array(
                "Affiliation"=>"Affiliation(第一作者单位地址)",
                "All Fields"=>"All Fields(所有字段)",
                "Author"=>"Author(作者)",
                "Corporate Author"=>"Corporate Author(协作者)",
                "EC/RN Number"=>"EC/RN Number(酶代码及CAS登记号)",
                "Entrez Date"=>"Entrez Date(Entrez日期)",
                "Filter"=>"Filter(过滤器)",
                "First Author"=>"First Author(第一作者)",
                "Full Author Name"=>"Full Author Name(作者全名)",
                "Full Investigator Name"=>"Full Investigator Name(研究者全名)",
                "Grant Number"=>"Grant Number(资助号)",
                "Investigator"=>"Investigator(研究者)",
                "Issue"=>"Issue(期)",
                "Journal"=>"Journal(期刊名称)",
                "Language"=>"Language(语种)",
                "Last Author"=>"Last Author(最后作者)",
                "Location ID"=>"Location ID(DOI及出版号)",
                "MeSH Date"=>"MeSH Date(MeSH词确立日期)",
                "MeSH Major Topic"=>"MeSH Major Topic(MeSH主要主题词)",
                "MeSH Subheading"=>"MeSH Subheading(MeSH副主题词)",
                "MeSH Terms"=>"MeSH Terms(MeSH词表词)",
                "Pagination"=>"Pagination(页码)",
                "Pharmacological Action"=>"Pharmacological Action(药理作用)",
                "Publication Date"=>"Publication Date(出版日期)",
                "Publication Type"=>"Publication Type(文献类型)",
                "Secondary Source ID"=>"Secondary Source ID(第二来源标识)",
                "Substance Name"=>"Substance Name(化学物质名称)",
                "Text Word"=>"Text Word(文本词)",
                "Title"=>"Title(标题)",
                "Title/Abstract"=>"Title/Abstract(标题/摘要)",
                "Transliterated Title"=>"Transliterated Title(标题音译)",
                "Volume"=>"Volume(卷)"
             );
	}
	
	/**
	 * @desc 语种限定
	 * @return array;
	 */
	private function __getLanguage(){
        return array(
	                "English"=>"英语",
					"Chinese"=>"中文",
					"French"=>"法语",
				    "German"=>"德语",
				    "Italian"=>"意大利语",
					"Japanese"=>"日语",
				    "Russian"=>"俄文",
				    "Spanish"=>"西班牙语",
				    "Afrikaans"=>"Afrikaans",
					"Albanian"=>"Albanian",
					"Arabic"=>"Arabic",
					"Armenian"=>"亚美尼亚语",
					"Azerbaijani"=>"Azerbaijani",
					"Bosnian"=>"Bosnian",
					"Bulgarian"=>"Bulgarian",
					"Catalan"=>"Catalan",
					"Croatian"=>"Croatian",
					"Czech"=>"Czech",
					"Danish"=>"Danish",
					"Dutch"=>"Dutch",
					"Esperanto"=>"Esperanto",
					"Estonian"=>"Estonian",
					"Finnish"=>"Finnish",
					"Georgian"=>"Georgian",
					"Greek, Modern"=>"Greek, Modern",
					"Hebrew"=>"Hebrew",
					"Hindi"=>"Hindi",
					"Hungarian"=>"Hungarian",
					"Icelandic"=>"Icelandic",
					"Indonesian"=>"Indonesian",
					"Kinyarwanda"=>"Kinyarwanda",
					"Korean"=>"Korean",
					"Latin"=>"Latin",
					"Latvian"=>"Latvian",
					"Lithuanian"=>"Lithuanian",
					"Macedonian"=>"Macedonian",
					"Malay"=>"Malay",
					"Malayalam"=>"Malayalam",
					"Maori"=>"Maori",
					"Multiple Languages"=>"Multiple Languages",
					"Norwegian"=>"Norwegian",
					"Persian"=>"Persian",
					"Polish"=>"波兰文",
					"Portuguese"=>"Portuguese",
					"Pushto"=>"Pushto",
					"Romanian"=>"Romanian",
					"Sanskrit"=>"Sanskrit",
					"Scottish gaelic"=>"Scottish gaelic",
					"Serbian"=>"Serbian",
					"Slovak"=>"斯洛伐克语",
					"Slovenian"=>"斯洛文尼亚语",
					"Swedish"=>"瑞典语",
					"Thai"=>"泰文",
					"Turkish"=>"土耳其语",
					"Ukrainian"=>"乌克兰语",
					"Undetermined"=>"未确定",
					"Vietnamese"=>"越南语"
	           );
	}
	
	/**
	 * @desc 高级检索_文献类型限定
	 * @return array;
	 */
	private function __getDocType(){
	     return array(
			    "Clinical Trial"=>"Clinical Trial(临床试验)",
			    "Editorial"=>"Editorial(评论)",
			    "Letter"=>"Letter(信件)",
			    "Meta-Analysis"=>"Meta-Analysis(趋势分析)",
			    "Practice Guideline"=>"Practice Guideline(医疗指南)",
			    "Randomized Controlled Trial"=>"Randomized Controlled Trial(随机对照试验)",
			    "Review"=>"Review(综述)",
			    "Addresses"=>"Addresses(演讲)",
			    "Bibliography"=>"Bibliography(书目)",
			    "Biography"=>"Biography(传记)",
			    "Case Reports"=>"Case Reports(病例报告)",
			    "Classical Article"=>"Classical Article(经典论文)",
			    "Clinical Conference"=>"Clinical Conference(临床讨论会)",
				"Clinical Trial, Phase I"=>"Clinical Trial, Phase I(I期临床试验)",
				"Clinical Trial, Phase II"=>"Clinical Trial, Phase II(II期临床试验)",
				"Clinical Trial, Phase III"=>"Clinical Trial, Phase III(III期临床试验)",
				"Clinical Trial, Phase IV"=>"Clinical Trial, Phase IV(IV期临床试验)",
				"Comment"=>"Comment(评论)",
				"Comparative Study"=>"Comparative Study(对比研究)",
				"Consensus Development Conference"=>"Consensus Development Conference(总结性报告)",
				"Consensus Development Conference, NIH"=>"Consensus Development Conference, NIH(国立卫生研究院总结性报告)",
				"Controlled Clinical Trial"=>"Controlled Clinical Trial(临床对照试验)",
				"Corrected and Republished Article"=>"Corrected and Republished Article(校正的和再版的论文)",
				"Dictionary"=>"Dictionary(词典)",
				"Directory"=>"Directory(名录)",
				"Duplicate Publication"=>"Duplicate Publication(重复出版物)",
				"English Abstract"=>"English Abstract(英文文摘)",
				"Evaluation Studies"=>"Evaluation Studies(评价研究)",
				"Festschrift"=>"Festschrift(纪念文集)",
				"Government Publications"=>"Government Publications(政府出版物)",
				"Guideline"=>"Guideline(指南)",
			    "Historical Article"=>"Historical Article(历史文献)",
				"Interview"=>"Interview(采访)",
			    "In Vitro"=>"In Vitro(体外研究)",
				"Journal Article"=>"Journal Article(期刊论文)",
				"Lectures"=>"Lectures(专题讲座)",
				"Legal Cases"=>"Legal Cases(诉讼案例)",
				"Legislation"=>"Legislation(法律法规)",
				"Multicenter Study"=>"Multicenter Study(多中心研究)",
				"News"=>"News(新闻)",
				"Newspaper Article"=>"Newspaper Article(报纸文章)",
				"Overall"=>"Overall(专辑)",
				"Patient Education Handout"=>"Patient Education Handout(病人教育印刷品)",
				"Periodical Index"=>"Periodical Index(期刊索引)",
				"Published Erratum"=>"Published Erratum(出版更正)",
				"Retracted Publication"=>"Retracted Publication(已撤销出版物)",
				"Research Support, N I H, Extramural"=>"Research Support, N I H, Extramural(国立卫生研究院校外研究资助)",
				"Research Support, N I H, Intramural"=>"Research Support, N I H, Intramural(国立卫生研究院校内研究资助)",
				"Research Support, Non U S Gov't"=>"Research Support, Non U S Gov't(非美国政府研究资助)",
				"Research Support, U S Gov't, Non P H S"=>"Research Support, U S Gov't, Non P H S(美国政府非公共卫生署研究资助)",
				"Research Support, U S Gov't, P H S"=>"Research Support, U S Gov't, P H S(美国政府公共卫生署研究资助)",
				"Retraction of Publication"=>"Retraction of Publication(出版撤销声明)",
				"Scientific Integrity Review"=>"Scientific Integrity Review(科学笃实评议)",
				"Technical Report"=>"Technical Report(技术报告)",
				"Twin Study"=>"Twin Study(配对研究)",
				"Validation Studies"=>"Validation Studies(有效性研究)"
             );
	 }
	 
	 /**
	 * @desc 高级检索_子文档限定
	 * @return array;
	 */
	private function __getSubDocType(){
	    return array(
                "jsubsetaim[text]"=>"核心临床医学期刊",
				"jsubsetd[text]"=>"牙医学期刊",
			    "jsubsetd[text]"=>"护理学期刊",
			    "AIDS[sb]"=>"AIDS",
				"bioethics[sb]"=>"Bioethics",
			    "Cancer[sb]"=>"Cancer",
			    "cam[sb]"=>"Complementary Medicine",
			    "history[sb]"=>"History of Medicine",
				"space[sb]"=>"Space Life Sciences",
				"systematic[sb]"=>"Systematic Reviews",
				"tox[sb]"=>"Toxicology",
				"medline[sb]"=>"MEDLINE",
				"pubmed pmc local[sb]"=>"PubMed Central"
             );
	}
	
   /**
	 * @desc 高级检索_研究对象年龄限定
	 * @return array;
	 */
	private function __getAge(){
	    return array(
                "infant[MeSH Terms]"=>"所有婴儿：出生-23个月",
				"infant[MeSH Terms] OR child[MeSH Terms] OR adolescent[MeSH Terms]"=>"所有儿童：0-18岁",
			    "adult[MeSH Terms]"=>"所有成人：19岁以上",
			    "infant, newborn[MeSH Terms]"=>"新生儿：出生-1个月",
				"infant[MeSH Terms:noexp]"=>"婴儿：1-23个月",
			    "child, preschool[MeSH Terms]"=>"学龄前儿童：2-5岁",
			    "child[MeSH Terms:noexp]"=>"儿童：6-12岁",
			    "adolescent[MeSH Terms]"=>"青少年：13-18岁",
				"adult[MeSH Terms:noexp]"=>"成人：19-44岁",
				"middle aged[MeSH Terms]"=>"中年：45-64岁",
				"middle aged[MeSH Terms] OR aged[MeSH Terms]"=>"中年+老年：45岁以上",
				"aged[MeSH Terms]"=>"老年：65岁以上",
				"aged, 80 and over[MeSH Terms]"=>"80岁和80岁以上"
             );
	}
	
   /**
	 * @desc 高级检索_逻辑运算符
	 * @return array;
	 */
	private function __getBoolean(){
	    return array(
                 "AND"=>"AND",
		          "OR"=>"OR",
		          "NOT"=>"NOT"
             );
	}
}
