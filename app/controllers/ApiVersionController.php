<?php 

class ApiVersionController extends BaseController
{

	const NETURL = 'http://cdcma.bizconf.cn';

	public function getVersion(){//获取初始的直播、预告、回顾
		$type = Input::get('type');
		$number = Input::get('number');
		if(!$type){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$version = Version::getNewVersion($type);
		if($version){
			if($version->number <= $number){
	        	echo  $this->formatRet(false,"暂无新版本！");exit;
	        }
	        $newversion['number'] = $version->number;
	        $newversion['types'] = $version->types;
	        $newversion['url'] = $version->url;
	        $newversion['content'] = $version->content;
	        $newversion['forces'] = $version->forces;
	        $newversion['is_push'] = $version->is_push;
			$data = array( 'version'=>$newversion );
			echo $this->formatRet(true,"新版本提示","success",$data);exit;
		}else{
			echo  $this->formatRet(false,"暂无新版本！");exit;
		}
	}

}