<?php
// 错误提示去除 notice
ini_set("error_reporting","E_ALL & ~E_NOTICE");

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function showMessage($msg='', $url = 'goback', $ms = 1250)
	{
		return View::make('errors.showMessage')->with('msg',$msg)->with('url',$url)->with('ms',$ms);
	}

	/**
	 * 格式化返回结果
	 * @param unknown_type $status
	 * @param unknown_type $msg
	 * @param unknown_type $detail
	 * @param unknown_type $datalist
	 * @param unknown_type $aExt  扩展，其他要返回的内容 eg: array('files' => array(...))
	 */
    protected function formatRet($status = false,$msg='',$detail = NULL, $datalist =  NULL, $aExt = array()){
    	$arr = array(
			'success' => $status,
			'msg' => $msg
		);
    	if( is_array($detail)){
    		$detail = $this->dealNull($detail);//处理null值，置为 ''
    		$arr['detail'] = $detail;
    	}
    	if( is_array($datalist)){
    		$datalist = $this->dealNull($datalist);//处理null值，置为 ''
    		$arr['datalist'] = $datalist;
    	}
    	if($aExt){
    		foreach ($aExt as $k => $v){
    			$arr[$k] = $v;
    		}
    	}
    
    	return json_encode($arr);
    }
    
    protected function message($oValidator){
    	$msg = '验证未通过！';
    	$messages = $oValidator->messages()->toArray();
    	foreach (  $messages as $k => $v){
    		foreach ($v as $n => $m){
    			$msg.= $m.'，';
    		}
    	}
    	return $msg;
    }
    
    /**
     * 递归处理空值
     * @param unknown_type $arr
     */
    protected function dealNull( $arr){
    	$aResult = array();
    	foreach ( $arr as $k => $v){
    		$aTemp;
    		if( is_array($v)){
    			$aTemp = $this->dealNull($v);
    		}else{
    			$aTemp = ( $v === null) ? '' : $v;
    		}
    		$aResult[$k] = $aTemp;
    	}
    	return $aResult;
    }
    
    public function formatUserInfo($oUser){
    	$iHospitalId = $oUser->user_company;
    	if(is_numeric($iHospitalId)){
    		$sHospital = Hospital::where('id',$iHospitalId)->pluck('name');
    		if($sHospital){
    			$oUser->user_company = $sHospital;
    		}
    	}
    	if(!$oUser->user_company){
    		$oUser->user_company = $oUser->user_company_name;
    	}
    	if(is_numeric($oUser->user_province)){
    		$sHospital = Hospital::where('id',$oUser->user_province)->pluck('name');
    		if($sHospital){
    			$oUser->user_province = $sHospital;
    		}
    	}
    	if(is_numeric($oUser->user_city)){
    		$sHospital = Hospital::where('id',$oUser->user_city)->pluck('name');
    		if($sHospital){
    			$oUser->user_city = $sHospital;
    		}
    	}
    	if(is_numeric($oUser->user_county)){
    		$sHospital = Hospital::where('id',$oUser->user_county)->pluck('name');
    		if($sHospital){
    			$oUser->user_county = $sHospital;
    		}
    	}
    	return $oUser;
    }

}