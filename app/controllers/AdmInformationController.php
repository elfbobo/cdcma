<?php

class AdmInformationController extends BaseController {
	
	CONST  PAGESIZE = 10;

	/*
	|--------------------------------------------------------------------------
	| 信息管理
	|--------------------------------------------------------------------------
	|zgq
	*/

	 
	public function InformationList()
	{
		$sTitle = Input::get('title');
		if(!empty($sTitle)){
			$oInfromation = Information::where('info_title','like','%'.$sTitle.'%')->paginate(10); 
		}else{
			$oInfromation = Information::orderBy("created_at","desc")->paginate(10); 
		}
		Return View::make('admin.inform.informlist')->with('oInfromation',$oInfromation);
	}

	public function AddInformation()
	{
		Return View::make('admin.inform.informadd');
	}
	  
	public function DoInformationAdd()
	{
		$oInformationUpdateTime =InformationUpdateTime::first();
		if(count($oInformationUpdateTime)){
			$oInformationUpdateTime ->update_time = time();
			$oInformationUpdateTime ->save();
		}else{
			$oInformationTiem = new InformationUpdateTime();
			$oInformationTiem -> update_time = time();
			$oInformationTiem ->save();
		}
		$aAdd = Input::all();
		$oInformationAdd = new Information();
		$oInformationAdd -> info_title = $aAdd['doc_name'];
		$oInformationAdd -> info_thumb = $aAdd['doc_thumb']; 
		$oInformationAdd -> info_url = $aAdd['doc_position'];
		$oInformationAdd -> save();
		return Redirect::to('/adminformation/information-list');
	}

	//删除资讯   Route::get('/adminformation/do-dele',array('uses'=>'AdmInformationController@DoDele')); 
	public function DoDele($iId)
	{
		$oInformation =  Information::find($iId);
		$oInformation -> delete();
		return Redirect::to('/adminformation/information-list');
	}

	public function DoEdit($iId)
	{
		$oInformation = Information::find($iId);
		return View::make('admin.inform.edit')->with('oInformation',$oInformation)->with('iId',$iId);
	}

	///adminformation/do-information-edit
	public function DoInformationEdit()
	{
		$iId = Input::get('iId');
		$aMessage = Input::all();
		$oInformation = Information::find($iId);
		$oInformation -> info_title = $aMessage['doc_name'];
		$oInformation -> info_url = $aMessage['doc_position'];
		$oInformation -> info_thumb = $aMessage['doc_thumb'];
		$oInformation -> save();
		return  Redirect::to('/adminformation/information-list');
	}
	 
	//上传图片
	public function UploadDocThumb(){
		if($_FILES['upload_file']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['upload_file']['name'];
    		$attach_fileext = get_filetype($attach_filename);
		    $rand_name = date('YmdHis', time()).rand(1000,9999);
		    $sFileName = $rand_name.'.'.$attach_fileext;   
		    $sPath = "/upload/inform/$attach_fileext/".date('Ymd',time());
		    $sRealPath = public_path().$sPath;
		    mkdirs($sRealPath);
			// $attach_filesize = filesize($_FILES['upload_file']['tmp_name']);
		    move_uploaded_file($_FILES['upload_file']['tmp_name'], $sRealPath.DS.$sFileName);
		    $sFileNameS = $rand_name . '_s.' . $attach_fileext;
			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 1000, 1000 );
		    $sFileUrl = $sPath.'/'.$sFileNameS;
		    $json = array('doc_thumb'=>$sFileUrl);
		    echo json_encode($json);
		    die;
		}
	}
	
	//上传图片
	public function UploadThumb(){
		if($_FILES['upload_file']['error']>0){
			$error = $_FILES['thumb']['error'];
		}else{
			$attach_filename = $_FILES['upload_file']['name'];
			$attach_fileext = get_filetype($attach_filename);
			$rand_name = date('YmdHis', time()).rand(1000,9999);
			$sFileName = $rand_name.'.'.$attach_fileext;
			$sPath = "/upload/infor/$attach_fileext/".date('Ymd',time());
			$sRealPath = public_path().$sPath;
			mkdirs($sRealPath);
			// $attach_filesize = filesize($_FILES['upload_file']['tmp_name']);
			move_uploaded_file($_FILES['upload_file']['tmp_name'], $sRealPath.DS.$sFileName);
			$sFileNameS = $rand_name . '_s.' . $attach_fileext;
			resizeImage ( $sRealPath.DS.$sFileName, $sRealPath.DS.$sFileNameS, 1000, 1000 );
			$sFileUrl = $sPath.'/'.$sFileNameS;
			$json = array('doc_thumb'=>$sFileUrl);
			echo json_encode($json);
			die;
		}
	}
	
}