<?php

class AspirinShareController extends BaseController
{
    /**
     * 个人中心分享下载APP
     * http://2kghy.kydev.net/aspirinshare/download/IBTGP
     * @param unknown $sCode
     */
    public function getDownload($sCode)
    {
        return View::make('share.user.index')
            ->with('sCode', $sCode);
    }

    /**
     * 健康教育详细页--把本页面保存成海报图片
     * http://2kghy.kydev.net/aspirinshare/education-save-pic/2
     * @param unknown $iEducationId
     */
    public function getEducationSavePic($iEducationId = 0)
    {
        $oEducation = AspirinEducation::find($iEducationId);
        if (!$oEducation || $oEducation->education_type != 1) {
            echo '该视频审核未通过或者已经被删除';
            die;
        }
        //获取上传者用户信息
        $oUser = User::select('id', 'user_name', 'user_thumb', 'user_position', 'user_department',
            'user_company')->where('id', $oEducation->user_id)->first();
        $sUserCompany = $oUser->user_company;
        if (is_numeric($sUserCompany) && $sUserCompany != 0) {
            $sUserCompany = Hospital::where('id', $sUserCompany)->pluck('name');
        }
        if (!$sUserCompany) {
            $sUserCompany = $oUser->user_company_name;
        }
        //获取二维码地址
        $sUrl = Config::get('app.url') . '/aspirinshare/education-detail/' . $iEducationId;
        $sEwmUrl = AspirinEwm::getErweima($sUrl);
        return View::make('share.aspirin.education.savepic')
            ->with('oEducation', $oEducation)
            ->with('sEwmUrl', $sEwmUrl)
            ->with('oUser', $oUser)
            ->with('sUserCompany', $sUserCompany);
    }

    public function postEducationSavePic()
    {
        $iEducationId = intval(Input::get('id'));
        $sPicUrl = trim(Input::get('picurl'));
        $sPath = 'upload/haibao/' . date('Ymd', time()) . '/';
        $sRealPath = public_path() . '/' . $sPath;
        mkdirs($sRealPath);
        $img = str_replace('data:image/png;base64,', '', $sPicUrl);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $fileName = uniqid() . '.png';
        $filePath = $sRealPath . $fileName;
        $file = $sPath . $fileName;
        $success = file_put_contents($filePath, $data);
        if ($success) {
            $oEducation = AspirinEducation::find($iEducationId);
            if (!$oEducation || $oEducation->education_type != 1) {
                echo '该视频审核未通过或者已经被删除';
                die;
            }
            $oEducation->print_pic = '/' . $file;
            $oEducation->save();
        }
    }

    /**
     * 健康教育详细页面二维码
     * http://2kghy.kydev.net/aspirinshare/education-ewm/1
     * @param unknown $iEducationId
     */
    public function getEducationEwm($iEducationId = 0)
    {
        $oEducation = AspirinEducation::find($iEducationId);
        if (!$oEducation || $oEducation->education_type != 1) {
            echo '该视频审核未通过或者已经被删除';
            die;
        }
        //获取二维码地址
        $sUrl = Config::get('app.url') . '/aspirinshare/education-detail/' . $iEducationId;
        $sEwmUrl = AspirinEwm::getErweima($sUrl);
        return View::make('share.aspirin.education.ewm')
            ->with('sEwmUrl', $sEwmUrl);
    }

    /**
     * 健康教育详细页面分享
     * http://2kghy.kydev.net/aspirinshare/education-detail/1
     * @param unknown $iEducationId
     */
    public function getEducationDetail($iEducationId = 0)
    {
        header("Content-type: text/html; charset=utf-8");
        $oEducation = AspirinEducation::find($iEducationId);
        if (!$oEducation || $oEducation->education_type != 1) {
            echo '该视频审核未通过或者已经被删除';
            die;
        }

        // 如果微信平台打开
        if (user_agent_is_weixin()) {
            $config = [
                'appid' => 'wx7c3e4eecbbadc619',
                'appsecret' => 'b6752c8e50b8c2aeb688d2eaead79f10'
            ];
            // 获取微信用户信息
            if (!Session::has('openid')) {
                // 如果存在 code 则说明是授权后的页面
                if (Input::has('code')) {
                    $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . $config['appid'] . '&secret=' . $config['appsecret'] . '&code=' . Input::get('code') . '&grant_type=authorization_code';
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    //curl_setopt($curl, CURLOPT_TIMEOUT, 500);
                    curl_setopt($curl, CURLOPT_TIMEOUT, 2);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($curl, CURLOPT_URL, $url);

                    $res = curl_exec($curl);
                    curl_close($curl);
                    $result = json_decode($res, true);
                    if ($result['openid']) {
                        session::put('openid', $result['openid']);
                    }
                } else {
                    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
                    $uri = urlencode("$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");

                    return Redirect::to("https://open.weixin.qq.com/connect/oauth2/authorize?appid={$config['appid']}&redirect_uri={$uri}&response_type=code&scope=snsapi_base&state=1");
                }
            }
        }
        $openid = Session::get('openid', null);
        $ip = GetIP();
        $platform = $_REQUEST['platform'];
        $VideoViewLog = VideoViewLog::where(function ($query) use ($openid, $ip) {
            $query->where('openid', $openid);
        })->where('platform', $platform)->where('types', 1)->where('video_id', $oEducation->id)->first();
        $vVlog = array(
            'user_id' => 0,
            'platform' => $platform,
            'types' => 1,
            'ip' => $ip,
            'video_id' => $oEducation->id,
            'video_title' => $oEducation->ppt_title,
            'video_thumb' => Config::get('app.url') . $oEducation->ppt_thumb
        );

        if (!$VideoViewLog) {
            $VideoViewLog = new VideoViewLog($vVlog);
            $oEducation->increment('share_hits');
        } else {
            foreach ($vVlog as $key => $value) {
                $VideoViewLog->$key = $value;
            }
        }
        $VideoViewLog->openid = $openid;
        //浏览量+1
        $oEducation->increment('education_hits');
        //查找当前用户是否点赞
        $iHasSupport = AspirinSupportLog::hasSupport(2, 0, $iEducationId);
        //获取上传者用户信息
        $oUser = User::select('id', 'user_name', 'user_thumb', 'user_position', 'user_department', 'user_company')
            ->where('id', $oEducation->user_id)->first();
        $sUserCompany = $oUser->user_company;
        if (is_numeric($sUserCompany) && $sUserCompany != 0) {
            $sUserCompany = Hospital::where('id', $sUserCompany)->pluck('name');
        }
        if (!$sUserCompany) {
            $sUserCompany = $oUser->user_company_name;
        }
        //插入健康教育视频观看日志
        $VideoViewLog->save();

        return View::make('share.aspirin.education.detail')
            ->with('oEducation', $oEducation)
            ->with('oVideoLog', $VideoViewLog)
            ->with('iHasSupport', $iHasSupport)
            ->with('oUser', $oUser)
            ->with('sUserCompany', $sUserCompany);
    }

    /**
     * 发送观看视频心跳包，用于记录观看时长
     * @return mixed
     */
    public function postShowTimeDelay()
    {
        $id = Input::get('id', 0);
        $time = Input::get('time', 300);
        $info = VideoViewLog::where('id', $id)->first();
        $info->view_times = (int)$info->view_times + $time;
        $info->save();

        return $this->showMessage('观看视频心跳包操作完毕');
    }

    public function postEducationSupport($iId)
    {
        $oEducation = AspirinEducation::find($iId);
        if (!$oEducation || $oEducation->education_type != 1) {
            return false;
        }
        //查找当前用户是否点赞
        $iHasSupport = AspirinSupportLog::hasSupport(2, 0, $iId);
        if ($iHasSupport) {
            return 'repeat';
        }
        //新增一条点赞记录
        $ip = GetIP();
        $sUserAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
        $aLog = array(
            'cat_id' => 2,
            'user_id' => 0,
            'source_id' => $iId,
            'user_agent' => $sUserAgent,
            'ip' => $ip
        );
        $oLog = new AspirinSupportLog($aLog);
        $oLog->save();
        //浏览量+1
        $oEducation->increment('support_count');
        return 'success';
    }

    /**
     * 在线会议往期回顾视频详细
     * http://2kghy.kydev.net/aspirinshare/online-show/1
     * @param unknown $iVideoId
     */
    public function getOnlineShow($iVideoId)
    {
        $oVideo = AspirinOnlineSpeaker::find($iVideoId);
        if (!$oVideo) {
            echo $this->formatRet(false, "不存在该记录");
            exit;
        }
        //浏览量+1
        $oVideo->increment('video_hits');
        $oOnline = AspirinOnline::find($oVideo->online_id);
        $aVideo = array(
            'video_id' => $iVideoId,
            'online_title' => $oOnline->online_title,
            'online_banner' => Config::get('app.url') . $oOnline->online_banner,
            'video_url' => $oVideo->video_url,
            'video_hits' => $oVideo->video_hits,
            'video_zan' => $oVideo->video_zan
        );
        //专家信息
        $oUser = User::select('user_name', 'user_company', 'user_company_name')
            ->where('id', $oVideo->speaker_id)
            ->first();
        $aVideo['doc_name'] = $oUser->user_name;
        $sUserCompany = $oUser->user_company;
        if (is_numeric($sUserCompany) && $sUserCompany != 0) {
            $sUserCompany = Hospital::where('id', $sUserCompany)->pluck('name');
        }
        if (!$sUserCompany) {
            $sUserCompany = $oUser->user_company_name;
        }
        $aVideo['doc_hospital'] = $sUserCompany;
        //评论
        $oComments = AspirinOnlineComment::where('video_id', $iVideoId)->orderBy('created_at', 'desc')->get();
        foreach ($oComments as $k => $v) {
            $iUid = $v->user_id;
            $sUnick = User::where('id', $iUid)->pluck('user_nick');
            $sUthumb = User::where('id', $iUid)->pluck('user_thumb');
            $oComments[$k]->user_nick = $sUnick;
            $oComments[$k]->user_thumb = $sUthumb;
        }
        $aComments = array();
        foreach ($oComments as $v) {
            $aComments[] = array(
                'comment_id' => $v->id,
                'user_nick' => $v->user_nick,
                'user_thumb' => Config::get('app.url') . $v->user_thumb,
                'comment' => str_replace('/assets/images/front/emotion/',
                    Config::get('app.url') . '/assets/images/front/emotion/', $v->comment),
                'zan_count' => $v->zan_count,
                'created_at' => substr($v->created_at, 0, 10)
            );
        }
        $aVideo['comments'] = $aComments;
        return View::make('share.aspirin.online.detail')->with('aVideo', $aVideo)->with('iVideoId', $iVideoId);
    }

    //视频全屏播放无法remove掉，跳转新页面
    public function getOnlineShowCopy($iVideoId)
    {
        $oVideo = AspirinOnlineSpeaker::find($iVideoId);
        if (!$oVideo) {
            echo $this->formatRet(false, "不存在该记录");
            exit;
        }
        $oOnline = AspirinOnline::find($oVideo->online_id);
        $aVideo = array(
            'video_id' => $iVideoId,
            'online_title' => $oOnline->online_title,
            'online_banner' => Config::get('app.url') . $oOnline->online_banner,
            'video_url' => $oVideo->video_url,
            'video_hits' => $oVideo->video_hits,
            'video_zan' => $oVideo->video_zan
        );
        //专家信息
        $oUser = User::select('user_name', 'user_company', 'user_company_name')
            ->where('id', $oVideo->speaker_id)
            ->first();
        $aVideo['doc_name'] = $oUser->user_name;
        $sUserCompany = $oUser->user_company;
        if (is_numeric($sUserCompany) && $sUserCompany != 0) {
            $sUserCompany = Hospital::where('id', $sUserCompany)->pluck('name');
        }
        if (!$sUserCompany) {
            $sUserCompany = $oUser->user_company_name;
        }
        $aVideo['doc_hospital'] = $sUserCompany;
        //评论
        $oComments = AspirinOnlineComment::where('video_id', $iVideoId)->orderBy('created_at', 'desc')->get();
        foreach ($oComments as $k => $v) {
            $iUid = $v->user_id;
            $sUnick = User::where('id', $iUid)->pluck('user_nick');
            $sUthumb = User::where('id', $iUid)->pluck('user_thumb');
            $oComments[$k]->user_nick = $sUnick;
            $oComments[$k]->user_thumb = $sUthumb;
        }
        $aComments = array();
        foreach ($oComments as $v) {
            $aComments[] = array(
                'comment_id' => $v->id,
                'user_nick' => $v->user_nick,
                'user_thumb' => Config::get('app.url') . $v->user_thumb,
                'comment' => str_replace('/assets/images/front/emotion/',
                    Config::get('app.url') . '/assets/images/front/emotion/', $v->comment),
                'zan_count' => $v->zan_count,
                'created_at' => substr($v->created_at, 0, 10)
            );
        }
        $aVideo['comments'] = $aComments;
        return View::make('share.aspirin.online.copydetail')->with('aVideo', $aVideo);
    }

    /**
     * 专项基金简介页
     * http://cdma.local/aspirinshare/info-brief
     */
    public function getInfoBrief()
    {
        return View::make('share.aspirin.info.brief');
    }

    /**
     * 专项基金科研申请——科研简介页
     * http://cdma.local/aspirinshare/research-brief
     */
    public function getResearchBrief()
    {
        return View::make('share.aspirin.research.brief');
    }

    /**
     * 专项基金科研申请——申请表页面
     * http://cdma.local/aspirinshare/research-apply
     */
    public function getResearchApply()
    {
        return View::make('share.aspirin.research.apply');
    }

    /**
     * 专项基金科研申请——申请结果页
     * http://cdma.local/aspirinshare/research-result?iApplyId=1
     */
    public function getResearchResult()
    {
        $iApplyId = intval(Input::get('iApplyId'));
        $oApply = AspirinResearchApply::find($iApplyId);
        if (!$oApply) {
            echo '该结果不存在';
            die;
        }
        return View::make('share.aspirin.research.applyresult')->with('oApply', $oApply);
    }

    /**
     * 专项基金风险筛查——筛查填写页二维码
     * http://cdma.local/aspirinshare/screening-ewm
     */
    public function getScreeningEwm()
    {
        $iUid = intval(Input::get('iUid'));
        //获取二维码地址
        $sUrl = Config::get('app.url') . '/aspirinshare/screening-form?iUid=' . $iUid;
        $sEwmUrl = AspirinEwm::getErweima($sUrl);
        return View::make('share.aspirin.screening.ewm')->with('sEwmUrl', $sEwmUrl);
    }

    /**
     * 专项基金风险筛查——筛查填写页
     * http://cdma.local/aspirinshare/screening-form?iUid=
     */
    public function getScreeningForm()
    {
        $iUid = intval(Input::get('iUid'));
        return View::make('share.aspirin.screening.form')->with('iUid', $iUid);
    }

    public function getScreeningForm2()
    {
        $iId = Input::get('id');
        $oScreening = AspirinScreening::find($iId);
        $sex = $oScreening->sex;
        return View::make('share.aspirin.screening.form2')->with('iId', $iId)->with('sex', $sex);
    }

    public function getScreeningForm3()
    {
        $iId = Input::get('id');
        return View::make('share.aspirin.screening.form3')->with('iId', $iId);
    }

    public function getScreeningForm4()
    {
        $iId = Input::get('id');
        return View::make('share.aspirin.screening.form4')->with('iId', $iId);
    }

    public function postScreeningSubmit()
    {
        if (Input::get('sex') == '女') {
            $sex = 0;
        } else {
            $sex = 1;
        }
        //根据年月计算真实年龄
        $age = AspirinScreening::getRealAge(Input::get('age'), '年', '月');
        //筛查记录保存
        $aScreening = array(
            'user_id' => intval(Input::get('uid')),
            'sex' => $sex,
            'age' => $age,
            'weight' => Input::get('weight'),
            'height' => Input::get('height'),
            'device_type' => 1,          //分享页
            'created_at' => date('Y-m-d H:i:s', time())
        );
        $oScreening = new AspirinScreening($aScreening);
        $oScreening->save();
        $iSourceId = $oScreening->id;
        return json_encode(array('success' => true, 'id' => $iSourceId));
    }

    public function postScreeningSubmit2()
    {
        $iId = intval(Input::get('iId'));
        $oScreening = AspirinScreening::find($iId);
        $oScreening->question1 = Input::get('question1');
        $oScreening->question2 = Input::get('question2');
        $oScreening->question3 = Input::get('question3');
        $oScreening->question31 = Input::get('question31') ? intval(Input::get('question31')) : 0;
        $oScreening->question0 = Input::get('question0') ? intval(Input::get('question0')) : 0;
        $oScreening->save();
        return json_encode(array('success' => true, 'id' => $iId));
    }

    public function postScreeningSubmit3()
    {
        $iId = intval(Input::get('iId'));
        $oScreening = AspirinScreening::find($iId);
        $oScreening->question4 = Input::get('question4');
        $oScreening->question41 = Input::get('question41') ? intval(Input::get('question41')) : 0;
        $oScreening->question42 = Input::get('question42') ? intval(Input::get('question42')) : 0;
        $oScreening->question5 = Input::get('question5');
        $oScreening->question51 = Input::get('question51') ? intval(Input::get('question51')) : 0;
        $oScreening->question52 = Input::get('question52') ? intval(Input::get('question52')) : 0;
        $oScreening->question53 = Input::get('question53') ? intval(Input::get('question53')) : 0;
        $oScreening->question54 = Input::get('question54') ? intval(Input::get('question54')) : 0;
        $oScreening->save();
        return json_encode(array('success' => true, 'id' => $iId));
    }

    public function postScreeningSubmit4()
    {
        $iId = intval(Input::get('iId'));
        $oScreening = AspirinScreening::find($iId);
        $iUid = $oScreening->user_id;
        $oScreening->question6 = Input::get('question6');
        $oScreening->question7 = Input::get('question7');
        $oScreening->question8 = Input::get('question8');
        $oScreening->question9 = Input::get('question9');
        $oScreening->question91 = Input::get('question91') ? Input::get('question91') : '';
        $weight = $oScreening->weight;
        $height = $oScreening->height;
        $sex = $oScreening->sex;
        //BMI值
        $iBmi = $weight / ($height * 0.01 * $height * 0.01);
        $iBmi = round($iBmi, 2);
        /************筛查公式计算得出结果s***************/
        //年龄得分
        $iAgeScore = AspirinScreening::getAgeScore($sex, $oScreening->age);
        //TC得分
        $iTcScore = AspirinScreening::getTcScore($sex, $oScreening->question42);
        //血压得分
        $iBpScore = AspirinScreening::getBpScore($sex, $oScreening->question52, $oScreening->question53);
        //糖尿病得分
        $iDmScore = AspirinScreening::getDmScore($sex, $oScreening->question3);
        //吸烟得分
        $iSmokeScore = AspirinScreening::getSmokeScore($oScreening->question1);
        //总分数
        $iScore = $iAgeScore + $iTcScore + $iBpScore + $iDmScore + $iSmokeScore;
        //评估CHD风险百分比
        $sPercent = AspirinScreening::getPercent($sex, $iScore);
        //存在的心血管危险因素
        $sDanger = '';
        $aDanger = AspirinScreening::getDangerElement($iId);
        if ($aDanger) {
            $sDanger = implode(',', $aDanger);
        }
        //获取界面呈现内容
        $aContent = AspirinScreening::getInfo($sPercent, $aDanger, $iBmi);
        /************筛查公式计算得出结果e***************/
        $oScreening->bmi = $iBmi;
        $oScreening->score = $iScore;
        $oScreening->percent = $sPercent;
        $oScreening->flag = $aContent[0];
        $oScreening->danger = $sDanger;
        $oScreening->save();
        $oScreeningLog = AspirinScreeningLog::where('user_id', $iUid)->first();
        if (count($oScreeningLog)) {
            $oScreeningLog->increment('share_use_count');
        } else {
            $aScreeningLog = array(
                'user_id' => $iUid,
                'share_use_count' => 1,
                'created_at' => date('Y-m-d H:i:s', time())
            );
            $oScreeningLog = new AspirinScreeningLog($aScreeningLog);
            $oScreeningLog->save();
        }
        return json_encode(array('success' => true, 'id' => $iId));
    }

    /**
     * 专项基金风险筛查——筛查结果页
     * http://cdma.local/aspirinshare/screening-result?id=
     */
    public function getScreeningResult()
    {
        $iId = Input::get('id');
        $oScreening = AspirinScreening::find($iId);
        $aDanger = array();
        if ($oScreening->danger) {
            $aDanger = explode(',', $oScreening->danger);
        }
        //获取界面呈现内容
        $aContent = AspirinScreening::getInfo($oScreening->percent, $aDanger, $oScreening->bmi);
        //获取结果页二维码链接地址
        $sUrl = '/aspirinshare/screening-result?id=' . $iId;
        $sUrl = Config::get('app.url') . $sUrl;
        $sEwmUrl = AspirinEwm::getErweima($sUrl);
        $aResult = array(
            'sex' => $oScreening->sex,
            'age' => $oScreening->age,
            'height' => $oScreening->height,
            'weight' => $oScreening->weight,
            'bmi' => $oScreening->bmi,
            'flag' => $oScreening->flag,
            'flag_text' => $aContent[1],
            'suggestion' => $aContent[2],
            'danger_element' => $aDanger,
            'ewm_url' => Config::get('app.url') . $sEwmUrl
        );
        return View::make('share.aspirin.screening.result')->with('aResult', $aResult);
    }

    //讲师课程-精彩内容
    public function getAuthdocclassshare()
    {
        $params = Input::all();
        //医生信息
        $doctorInfo = User::find($params['doc_id']);
        if ($doctorInfo->user_company) {
            if ((int)$doctorInfo->user_company) {
                $doctorInfo->company = Hospital::where("id", $doctorInfo->user_company)->pluck("name");
            } else {
                $doctorInfo->company = $doctorInfo->user_company;
            }
        }
        //视频信息获取
        $videoInfo = AspirinEducation::find($params['video_id']);
        $sProvince = Hospital::where("id", $doctorInfo->user_province)->pluck("name");
        $sCity = Hospital::where("id", $doctorInfo->user_city)->pluck("name");
        // $log = DB::getQueryLog();
        // print_r($log);die;
        //获取二维码地址
        $sUrl = Config::get('app.url') . '/aspirinshare/education-detail/' . $params['video_id'] . '?platform=qrcode';
        $sEwmUrl = AspirinEwm::getErweima($sUrl);
        // echo $sEwmUrl;die;
        return View::make('share.aspirin.education.sharedocvideo')
            ->with('domain', "http://cdcma.bizconf.cn")
            ->with('p_c_str', "{$sProvince}{$sCity}<br />{$doctorInfo->user_address}")
            // ->with('domain',"http://{$_SERVER['HTTP_HOST']}")
            ->with('sEwmUrl', $sEwmUrl)
            ->with('doctorInfo', $doctorInfo)
            ->with('videoInfo', $videoInfo);
    }

}