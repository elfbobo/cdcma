<?php 

/**
 * API操作类
 * @author dll
 *
 */

class ApiAspirinInfoController extends BaseController
{

	CONST  PAGESIZE = 10;
	public $iUserId = 0 ;

	public function __construct()
	{
		$this->beforeFilter('apilogin');
		global $iUserId;
		$this->iUserId = &$iUserId;
	}

	/**
	 * 返回专项基金首页轮播图
	 * http://cdma.local/apiaspirininfo/index?token=eyJpdiI6InQzTHZBQTBkVjBHWDdqcXZxcyt5YUM5TXU1eGsraENBRkNXbkJSWmRJQU09IiwidmFsdWUiOiJITHQ1V2ozTmJnaU5mS1RTczk4emRBazIyYXJGaUtJdmNoVklZOEFBTGlJPSIsIm1hYyI6ImI2YWY1MjY4NzNkYWQ1MzExODMxNTJiOTU3Yjc1ZTNkM2U2ZTc2MzJlMWY5YTk5YzM1NTU5NDE3Zjk5MmUzNjQifQ==
	 */
	public function getIndex(){
		$oGroup = AspirinInfoGroup::orderby('id','ASC')->get();
		$aGroup = array();
		foreach($oGroup as $k=>$v){
			$aGroup[] = array(
				'group_id'   =>$v->id,
				'info_group_title'=>$v->info_group_title,
				'info_group_thumb'=>Config::get('app.url').$v->info_group_thumb
			);
		} 
		$aInfo = array(
			'first_thumb_url' =>Config::get('app.url').'/aspirinshare/info-brief',
			'share_flag'  => 1,
			'share_title' => '专项基金简介',
			'share_des'   => '专项基金简介',
			'share_thumb' => Config::get('app.url').'/assets/images/mobile/logo.png',
			'share_url'   => Config::get('app.url').'/aspirinshare/info-brief'
		);
		echo  $this->formatRet(true,"成功",$aInfo,$aGroup);exit;
	}

	/**
	 * 返回专项基金轮播图列表页
	 * http://cdma.local/apiaspirininfo/list?iGroupId=1&token=
	 */
	public function getList(){
		if(!Input::has('iGroupId')){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$iGroupId = Input::get('iGroupId');
		$iPagesize = Input::get('pagesize')?Input::get('pagesize'):self::PAGESIZE;
		$oGroup = AspirinInfoGroup::find($iGroupId);
		$sGroupTitle = $oGroup->info_group_title;
		$aGroupTitle = array('info_group_title'=>$sGroupTitle);
		$oInfo = AspirinInfo::where('group_id',$iGroupId)->orderby('created_at','desc')->paginate($iPagesize);
		$aInfo = array();
		foreach($oInfo as $k=>$v){
			$aInfo[] = array(
				'id'			=> $v->id,
				'info_title'	=> $v->info_title,
				'info_thumb'	=> Config::get('app.url').$v->info_thumb,
				'created_at'	=> substr($v->created_at,0,10)
			);
		}
		echo  $this->formatRet(true,"成功",$aGroupTitle,$aInfo);exit;
	}

	/**
	 * 返回专项基金轮播图详情页
	 * http://cdma.local/apiaspirininfo/show?iId=1&token=
	 */
	public function getShow(){
		if(!Input::has('iId')){
			echo  $this->formatRet(false,"传递参数不正确");exit;
		}
		$iId = Input::get('iId');
		$oInfo = AspirinInfo::find($iId);
		if(count($oInfo)){
			$oInfo->increment('info_hits');
			$oGroup = AspirinInfoGroup::find($oInfo->group_id);
			$sGroupTitle = $oGroup->info_group_title;
			$aInfo = array(
				'info_group_title' => $sGroupTitle,
				'info_title'       => $oInfo->info_title,
				'info_subtitle'    => $oInfo->info_subtitle,
				'created_at'       => substr($oInfo->created_at,0,10),
				'info_video_thumb' => Config::get('app.url').$oInfo->info_video_thumb,
				'info_video'       => $oInfo->info_video,
				'info_content'     => str_replace('/upload/ueditor/image/', Config::get('app.url').'/upload/ueditor/image/', $oInfo->info_content)
			);
			echo  $this->formatRet(true,"成功","success",$aInfo);exit;
		}else{
			echo $this->formatRet(false, '该资讯不存在');exit;
		}
	}
	
}