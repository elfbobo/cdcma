<?php

/**
 * cnsearch操作类
 * @author gk
 *
 */
class CnsearchController extends BaseController {

	CONST  PAGESIZE = 10;
	public $sChar = '';
	public $iUserid;
	
	public function __construct(PubmedController $oPmd){
	    $this->iUserid = Session::get('userid');
	   	$this->oPmd = $oPmd;
	}

	public function getIndex(){
	   //初始化select选择框
		$selOptionTerm = $selOptionDE = $selOptionYHF = $selOptionYear = "";
		foreach ($this->__getCnAllFields() as $key=>$val){
			$selOptionTerm.='<option value="'.$val.'">'.$val.'</option>';
		}
		foreach ($this->__getCnMode() as $key=>$val){
	     	$selOptionDE.='<option value="'.$key.'">'.$val.'</option>';
		}
		foreach ($this->__getCnYuHuoFei() as $key=>$val){
	     	$selOptionYHF.='<option value="'.$key.'">'.$val.'</option>';
		}
		$arrYear = range(1990, (int)date('Y',time()));
		$selOptionStartYear = '';
		$selOptionStartYear.= '<option value="" selected>不限</option>';
		foreach ($arrYear as $key=>$val){
		     $selOptionStartYear.='<option value="'.$val.'">'.$val.'年</option>';
		}
		$selOptionEndYear= '';
		foreach ($arrYear as $key=>$val){
			if ($val == (int)date('Y',time())){
		         $selOptionEndYear.='<option value="'.$val.'" selected>'.$val.'年</option>';
			}else{
			     $selOptionEndYear.='<option value="'.$val.'">'.$val.'年</option>';
			}
		}
		return View::make('front.cnsearch.index')
			->with('selOptionTerm',$selOptionTerm)
			->with('selOptionDE',$selOptionDE)
			->with('selOptionYHF',$selOptionYHF)
			->with('selOptionStartYear',$selOptionStartYear)
			->with('selOptionEndYear',$selOptionEndYear);
	}

	//大首页检索结果页   dll 20170328
	public function getResult($term_one,$keyword_one='')
	{
		return View::make('front.cnsearch.result')
			->with('term_one',$term_one)
			->with('keyword_one',$keyword_one);
	}

	public function postAjax(){
		$this->oPmd->removeBadCharacters();
		$type = Input::get('type');
		if ($type != 'ajax'){
			return false;
		}
	    $action = trim(Input::get('action'));
	    switch ($action){
			case 'cnsearch':
		      	$term_one = !empty($_POST['term_one']) ? stripslashes(urldecode(trim($_POST['term_one']))) : '' ;
		    	$dark_exact_one = !empty($_POST['dark_exact_one']) ? trim($_POST['dark_exact_one']) : '' ;
		    	$keyword_one = !empty($_POST['keyword_one']) ? stripslashes(urldecode(trim($_POST['keyword_one']))) : '' ;
		    	$yhf_one = !empty($_POST['yhf_one']) ? trim($_POST['yhf_one']) : '' ;

	        	$term_two = !empty($_POST['term_two']) ? stripslashes(urldecode(trim($_POST['term_two']))) : '' ;
	        	$dark_exact_two = !empty($_POST['dark_exact_two']) ? trim($_POST['dark_exact_two']) : '' ;
	        	$keyword_two = !empty($_POST['keyword_two']) ? stripslashes(urldecode(trim($_POST['keyword_two']))) : '' ;
	        	$yhf_two = !empty($_POST['yhf_two']) ? trim($_POST['yhf_two']) : '' ;
        	
	        	$term_three = !empty($_POST['term_three']) ? stripslashes(urldecode(trim($_POST['term_three']))) : '' ;
	        	$dark_exact_three = !empty($_POST['dark_exact_three']) ? trim($_POST['dark_exact_three']) : '' ;
	        	$keyword_three = !empty($_POST['keyword_three']) ? stripslashes(urldecode(trim($_POST['keyword_three']))) : '' ;
	        	$yhf_three = !empty($_POST['yhf_three']) ? trim($_POST['yhf_three']) : '' ;
        	
	        	$term_four = !empty($_POST['term_four']) ? stripslashes(urldecode(trim($_POST['term_four']))) : '' ;
	        	$dark_exact_four = !empty($_POST['dark_exact_four']) ? trim($_POST['dark_exact_four']) : '' ;
	        	$keyword_four = !empty($_POST['keyword_four']) ? stripslashes(urldecode(trim($_POST['keyword_four']))) : '' ;
	        	$yhf_four = !empty($_POST['yhf_four']) ? trim($_POST['yhf_four']) : '' ;
	        	
	        	$term_five = !empty($_POST['term_five']) ? stripslashes(urldecode(trim($_POST['term_five']))) : '' ;
	        	$dark_exact_five = !empty($_POST['dark_exact_five']) ? trim($_POST['dark_exact_five']) : '' ;
	        	$keyword_five = !empty($_POST['keyword_five']) ? stripslashes(urldecode(trim($_POST['keyword_five']))) : '' ;
	        	$yhf_five = !empty($_POST['yhf_five']) ? trim($_POST['yhf_five']) : '' ;
	        	
	        	$term_six = !empty($_POST['term_six']) ? stripslashes(urldecode(trim($_POST['term_six']))) : '' ;
	        	$dark_exact_six = !empty($_POST['dark_exact_six']) ? trim($_POST['dark_exact_six']) : '' ;
	        	$keyword_six = !empty($_POST['keyword_six']) ? stripslashes(urldecode(trim($_POST['keyword_six']))) : '' ;
	        	$yhf_six = !empty($_POST['yhf_six']) ? trim($_POST['yhf_six']) : '' ;
	        	
	        	$startYear= !empty($_POST['startYear']) ? trim($_POST['startYear']) : '' ;
	        	$endYear= !empty($_POST['endYear']) ? trim($_POST['endYear']) : '' ;
	        	$db= !empty($_POST['db']) ? trim($_POST['db']) : '' ;
	        	
				$q = '';
				//第一个检索词框处理
			    if($keyword_one){
			    	if($term_one != '全部'){
			          	$q.=$term_one.':';
						if($dark_exact_one != 'dark'){
							$q.='("'.$keyword_one.'")';
						}else{
							$q.='('.$keyword_one.')';
						}
			        }else{
						if($dark_exact_one != 'dark'){
							$q.='"'.$keyword_one.'"';
						}else{
							$q.=$keyword_one;
						}
			        }
			        $q.=' '.$yhf_one.' ';
				}
				//第二个检索词框处理
	            if($keyword_two){
			    	if($term_two != '全部'){
						$q.=$term_two.':';
						if($dark_exact_two != 'dark'){
							$q.='("'.$keyword_two.'")';
						}else{
							$q.='('.$keyword_two.')';
						}
			        }else{
						if($dark_exact_two != 'dark'){
							$q.='"'.$keyword_two.'"';
						}else{
							$q.=$keyword_two;
						}
			        }
			        $q.=' '.$yhf_two.' ';
				}
				
		     	//第三个检索词框处理
		        if($keyword_three){
			    	if($term_three != '全部'){
						$q.=$term_three.':';
						if($dark_exact_three != 'dark'){
							$q.='("'.$keyword_three.'")';
						}else{
							$q.='('.$keyword_three.')';
						}
			        }else{
						if($dark_exact_three != 'dark'){
							$q.='"'.$keyword_three.'"';
						}else{
							$q.=$keyword_three;
						}
			        }
			        $q.=' '.$yhf_three.' ';
				}
				
	           	//第四个检索词框处理
	            if($keyword_four){
			    	if($term_four != '全部'){
						$q.=$term_four.':';
						if($dark_exact_four != 'dark'){
							$q.='("'.$keyword_four.'")';
						}else{
							$q.='('.$keyword_four.')';
						}
			        }else{
						if($dark_exact_four != 'dark'){
							$q.='"'.$keyword_four.'"';
						}else{
							$q.=$keyword_four;
						}
			        }
			        $q.=' '.$yhf_four.' ';
				}
			
				//第五个检索词框处理
	            if($keyword_five){
			    	if($term_five != '全部'){
						$q.=$term_five.':';
						if($dark_exact_five != 'dark'){
							$q.='("'.$keyword_five.'")';
						}else{
							$q.='('.$keyword_five.')';
						}
			        }else{
						if($dark_exact_five != 'dark'){
							$q.='"'.$keyword_five.'"';
						}else{
							$q.=$keyword_five;
						}
			        }
			        $q.=' '.$yhf_five.' ';
				}
			
	            //第六个检索词框处理
	            if($keyword_six){
			    	if($term_six != '全部'){
						$q.=$term_six.':';
						if($dark_exact_six != 'dark'){
							$q.='("'.$keyword_six.'")';
						}else{
							$q.='('.$keyword_six.')';
						}
			        }else{
						if($dark_exact_six != 'dark'){
							$q.='"'.$keyword_six.'"';
						}else{
							$q.=$keyword_six;
						}
			        }
			        $q.=' '.$yhf_six.' ';
				}
			
				if ($startYear){
				    $q.='Date:'.$startYear;
				}else{
				    $q.='Date:';
				}
				
	            if ($endYear){
				   $q.='-'.$endYear;
				}
				
				if ($db){
				   $q.=' '.'DBID:WF_QK';
				}
				$pageSize = 10;
				$page = !empty($_POST['page']) ? intval(trim($_POST['page'])) : 1;
				if ($page){
				    $url = WANFANGER_API_URL.'q='.rawurlencode($q).'&db=wf_qk&p='.$page.'&n='.$pageSize;
				}else{
				    $url = WANFANGER_API_URL.'q='.rawurlencode($q).'&db=wf_qk';
				}
				//$str =file_get_contents($url);
				$aData=array();
				$results = array();
				$str = $this->__requestApiByCurl($url);
				$results = $this->__wangFangMatch($str);
		        if (!empty($results)){
				    foreach ($results as $key=>$result){
				    	if (is_numeric($key)){
					        $aData[$key]['title']=isset($result['title']) ? $result['title'] : '';
					        $aData[$key]['source']=isset($result['source']) ? $result['source'] : '';
					        $aData[$key]['author']=isset($result['author']) ? $result['author'] : '';
					        $aData[$key]['description']=isset($result['description']) ? $this->__filterContent(strip_tags($result['description'])) : '';
					        $aData[$key]['keywords']=isset($result['keywords']) ? $this->__filterContent(strip_tags($result['keywords'])) : '';
					        $aData[$key]['helptext'] = $this->__filterContent(strip_tags($aData[$key]['title']."||||,文章出处为：".$aData[$key]['source'].",作者为：".$aData[$key]['author']));
				    	}
				    }
				}
				$pageNum = str_replace('&nbsp;', '', $results['pageNum']);
				$pageNum = intval($pageNum);
				$total = intval($results['totals']);
				$pages = $this->oPmd->ajaxPages($total, $pageSize, $page);
				return View::make('front.cnsearch.list')
                    ->with('results',$aData)
                    ->with('total',$total)
                    ->with('pageSize',$pageSize)
                    ->with('page',$page)
                    ->with('pages',$pages)
                    ->with('pageNum',$pageNum)
                    ->with('q',$q);
		      	break;
	      	
	        case 'helpOneCnText':
				$aHelpInfo = array();
				$oUser = User::where('id', $this->iUserid)->first();
				//用户邮箱地址为空
				if(!$oUser->user_email){
					echo '-1';die;  
				} 
				//用户积分不足
				/*if(intval($oUser->user_credit_count) < ZH_HELP_SCORE){
					echo '-2';die;  
				}*/
				$sHelpText = Input::get('helptext');
				$sMatch = '||||';
				$replace = ""; 
				$aHelpText = explode($sMatch, $sHelpText);
				$sTitle = $aHelpText[0];
				$sHelpText = str_replace($sMatch, $replace, $sHelpText);
				//邮件主题
				$subject = "国卫健康云中文文献检索";
				$content = "国卫健康云中文文献检索需要获取全文,文章标题为：".$sHelpText.", 请将全文发送到邮箱：".$oUser->user_email;
				$iId = PubmedHelpMail::where('userid', $this->iUserid)->where('title', "$sTitle")->pluck('id');
				if(!$iId){
					$aHelpInfo['userid'] = $this->iUserid; 
					$aHelpInfo['email'] = addslashes($oUser->user_email);
					$aHelpInfo['title'] = addslashes($sTitle);
					$aHelpInfo['content'] = addslashes($content);
					$aHelpInfo['device_type'] = 0;
					$aHelpInfo['pub_type'] = 2;
					// $aHelpInfo['posttime'] = time();
					$oHelp = new PubmedHelpMail($aHelpInfo);
					$oHelp->save();
					PubmedHelpMail::SendEmail($subject,$content);
					//扣除积分
					// $this->oScoreService->getIndex(6,"PMD_ZH_HELP", 0, $this->iUserid, "web", $sTitle);
				}else{
					echo '-3'; //已经求助过此文献
					die;
				}
				echo 1;
				die;
				break;
	      	
			case 'helpCnText':
	      	 	$aHelpInfo = array();
	      	 	$oUser = User::where('id', $this->iUserid)->first();
			    if(!$oUser->user_email){
					echo '-1';  //用户邮箱地址为空
					die;
	            } 
	            $sHelpTextsMatch = '||||||';
	            $aCnHelpTexts = explode($sHelpTextsMatch, trim(Input::get('cnhelptexts')));
	            /*if(intval($oUser->user_credit_count) < intval(ZH_HELP_SCORE*count($aCnHelpTexts))){
                      echo '-2'; die; //用户积分不足
                }*/
                $sMatch = '||||';
	            $replace = "";
	            foreach ($aCnHelpTexts as $key=>$sCnHelpText){
					$aCnHelpText = explode($sMatch, $sCnHelpText);
					$sTitle = $aCnHelpText[0];
					$sHelpText = str_replace($sMatch, $replace, $sCnHelpText);
					//邮件主题
					$subject = "国卫健康云中文文献检索";
					$content = "国卫健康云中文文献检索需要获取全文,文章标题为：".$sHelpText.", 请将全文发送到邮箱：".$oUser->user_email;
					$iId = PubmedHelpMail::where('userid', $this->iUserid)->where('title', "$sTitle")->pluck('id');
					if(!$iId){
						$aHelpInfo['userid'] = $this->iUserid; 
						$aHelpInfo['email'] = addslashes($oUser->user_email);
						$aHelpInfo['title'] = addslashes($sTitle);
						$aHelpInfo['content'] = addslashes($content);
						$aHelpInfo['device_type'] = 0;
						$aHelpInfo['pub_type'] = 2;
						// $aHelpInfo['posttime'] = time();
						$oHelp = new PubmedHelpMail($aHelpInfo);
						$oHelp->save();
						PubmedHelpMail::SendEmail($subject,$content);
						//扣除积分
						// $this->oScoreService->getIndex(6,"PMD_ZH_HELP", 0, $this->iUserid, "web", $sTitle);
					}
				}
				echo 1;
				die;
	      		break;
	    }
	}
	
    private function __filterContent($content){
		 $replacement = "";
		 $content = str_replace("&nbsp;", "", $content);
		 $sSpace = "/(\s*(\r\n|\n\r|\n|\r)\s*)/";
		 $content = preg_replace($sSpace, $replacement, $content);
		 return $content;
	}
	
	private function __requestApiByCurl($sUrl,$iTimeout = 60) {
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $sUrl );
		$iTimeout = intval ( $iTimeout );
		if ($iTimeout) {
			curl_setopt ( $ch, CURLOPT_TIMEOUT, $iTimeout );
		}
		ob_start ();
		curl_exec ( $ch );
		$sOut = ob_get_clean ();
		curl_close ( $ch );
		return $sOut;
		//return json_decode ($sOut, true);
	}
	
	private function __wangFangMatch($str){
		$arrData = array();
		preg_match_all("/<ul\s+class=[\"']list_ul[\"']\s*>(.*?)<\/ul>/is", $str, $arrMatch);
		if(!empty($arrMatch)){
		    foreach($arrMatch[1] as $key=>$data){
		    	preg_match_all("/<li.*?>(.*?)<\/li>/is", $data, $arrMatchLi);
		    	if(!empty($arrMatchLi)){
		    		foreach($arrMatchLi[1] as $k => $contentLi){
		    			if($k == 0){
			    			preg_match_all("/<a.*?>(.*?)<\/a>/is",$contentLi, $arrMatchTitle);
			    			if(!empty($arrMatchTitle[1][1])){
			    			    $arrData[$key]['title'] = $arrMatchTitle[1][1];
			    			}
		    			}
		    			if ($k == 1){
		    				preg_match_all("/<a.*?>(.*?)<\/a>/is",$contentLi, $arrMatchSource);
		    				$arrData[$key]['source'] = $arrMatchSource[1][0].",".$arrMatchSource[1][1];
		    				if (!empty($arrMatchSource[1])){
		    					unset($arrMatchSource[1][0],$arrMatchSource[1][1]);
		    				}
		    				if (!empty($arrMatchSource[1])){
		    			    	$arrData[$key]['author'] = implode(",", $arrMatchSource[1]);
		    				}
		    			}
		    			if ($k == 2){
		    				
		    				preg_match_all("/<span\s+style=[\"']display:inline[\"']\s*>(.*?)<\/span>/is",$contentLi, $arrMatchDesc);
		    				if(!empty($arrMatchDesc[1][0])){
		    				    $arrData[$key]['description'] = $arrMatchDesc[1][0];
		    				}
		    				
		    				preg_match_all("/<p\s+class=[\"']greencolor[\"']\s+style=[\"']display:inline[\"']\s*>(.*?)<\/p>/is",$contentLi, $arrMatchKeywords);
		    				if(!empty($arrMatchKeywords[1][0])){
		    				    $arrData[$key]['keywords'] = str_replace("关键词：", "", $arrMatchKeywords[1][0]); 
		    				}
		    			}
		    		 }
			    }
		    }
		}
		preg_match_all("/<span.*?\s+class=[\"']page_link[\"']\s*>(.*?)<\/span>/is", $str, $arrMatchPages);
		if(!empty($arrMatchPages[1][0])){
		   preg_match_all("/<\/t>(.*?)<t>/is", $arrMatchPages[1][0], $arrMatchPageNum);
		   $arrData['pageNum'] = $arrMatchPageNum[1][0];
		}else{
		   $arrData['pageNum'] = 0;
		}
		preg_match_all("/<td\s+class=[\"']space_td mode[\"']>(.*?)<td\s+class=[\"']sortby_td[\"']>/is", $str, $arrMatchTotalHtml);
		if(!empty($arrMatchTotalHtml[1][0])){
		    preg_match_all("/<td>(.*?)<\/td>/is", $arrMatchTotalHtml[1][0], $arrMatchTotal);
		}
        if(!empty($arrMatchTotal[1][0])){
		    preg_match_all("/\d/is", $arrMatchTotal[1][0], $arrMatchTotalNums);
		    $arrData['totals'] = implode('', $arrMatchTotalNums[0]);
		}else{
		    $arrData['totals'] = 0;
		}
		return $arrData;
	}
	
	private function __getCnAllFields(){
	    return array(
			"0"=>"全部",
			"1"=>"主题",
			"2"=>"题名或关键词",
			"3"=>"题名",
			"4"=>"创作者",
			"5"=>"作者单位",
			"6"=>"关键词",
			"7"=>"摘要",
			"8"=>"日期",
			"9"=>"期刊—刊名",
			"10"=>"期刊—期"
		);
	}
	
	private function __getCnMode(){
	    return  array(
			"dark"=>"模糊",
			"exact"=>"精确"
		);
	}
	
	private function __getCnYuHuoFei(){
		return  array(
            "*"=>"与",
            "+"=>"或",
            "^"=>"非"
		);
	}

}