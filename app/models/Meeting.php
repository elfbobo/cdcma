<?php
class  Meeting extends Eloquent {
	protected $table = 'meeting';
	public $timestamps = true;
	protected $guarded = array('id');
	
    
	//验证字段
	public static function getMeetingRule(){
		return array(
				'meeting_start_time'	=> 'required',
				'meeting_end_time'		=> 'required',
				'expert_company'		=> 'required',
				'expert_name'			=> 'required',
				'meeting_title'			=> 'required',
				'creator_name'			=> 'required',
				'creator_tel'			=> 'required',
				'creator_email'			=> 'required',
		);
	}
	//根据传入最大值和最小值获取时间端段
	public static function getTimeRule($date='',$start='',$end=''){
		
		//开始时间
		if(strlen($start) == 2){
			$time1 = substr($start,0,1);   // 截取时间点
		}elseif(strlen($start) == 3){
			$time1 = substr($start,0,2); 
		}
		$flag1 = substr($start,-1);      //截取标志位
		if($flag1 == 1){
			$starttime = $date.' '.$time1.':00:00';
		}elseif($flag1 == 2){
			$starttime = $date.' '.$time1.':30:00';
		}
		
		//结束时间
		if(strlen($end) == 2){
			$time2 = substr($end,0,1);   // 截取时间点
		}elseif(strlen($end) == 3){
			$time2 = substr($end,0,2); 
		}
		$flag2 = substr($end,-1);      // 截取标志位
		if($flag2 == 1){
			$endtime = $date.' '.$time2.':30:00';
		}elseif($flag2 == 2){
			$endtime = $date.' '.($time2+1).':00:00';
		}
		
		return array($starttime,$endtime);
	}
	
	//根据传入值获取时间
	public static function getTime($aTimeCode){
		
		$aNowTime = array();
		foreach($aTimeCode as $k=>$v){
			if(strlen($v) == 2){
				$time = substr($v,0,1);    // 截取时间点
			}elseif(strlen($v) == 3){
				$time = substr($v,0,2);   
			}
			$flag = substr($v,-1);      //截取标志位
			if($flag == 1){
				$aNowTime[$v] = $time.':00';
			}elseif($flag == 2){
				$aNowTime[$v] = $time.':30';
			}
		}
		
		return $aNowTime;
	}
}