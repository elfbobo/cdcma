<?php
/**
 * +----------------------------------------------------------------------
 * | 国卫健康云（cdcma）
 * +----------------------------------------------------------------------
 *  .--,       .--,             | FILE: ApiMyController.php
 * ( (  \.---./  ) )            | AUTHOR: byron sampson
 *  '.__/o   o\__.'             | EMAIL: xiaobo.sun@qq.com
 *     {=  ^  =}                | QQ: 150093589
 *      >  -  <                 | WECHAT: wx5ini99
 *     /       \                | DATETIME: 2018/8/20
 *    //       \\               |
 *   //|   .   |\\              |
 *   "'\       /'"_.-~^`'-.     |
 *      \  _  /--'         `    |
 *    ___)( )(___               |-----------------------------------------
 *   (((__) (__)))              | 高山仰止,景行行止.虽不能至,心向往之。
 * +----------------------------------------------------------------------
 * | Copyright (c) 2017 http://www.zzstudio.net All rights reserved.
 * +----------------------------------------------------------------------
 */

class UserDoctor extends Eloquent
{
    protected $table = 'user_doctor';
    protected $guarded = array('id');
    public $timestamps = true;

    public static function getValidateRule()
    {
        return array(
            'user_id' => 'required',
            'user_doctor_id' => 'required',
        );
    }

    public static function getValidateError()
    {
        return array(
            'user_id.required' => '代表不可为空！',
            'user_doctor_id.required' => '医生不可为空！',
        );
    }
}