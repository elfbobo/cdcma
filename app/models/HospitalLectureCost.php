<?php

class HospitalLectureCost extends Eloquent
{
    protected $table = 'hospital_lecture_cost';
    public $timestamps = true;
    protected $guarded = array('id');
}