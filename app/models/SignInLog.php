<?php
class SignInLog  extends Eloquent {
	protected $table = 'sign_in_log';
	public $timestamps = true;
	protected $guarded = array('id');
	
}