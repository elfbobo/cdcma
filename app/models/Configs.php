<?php
class Configs extends Eloquent {

	protected $table='configs';
	protected $guarded = array('id');
	public $timestamps = false;
    
	public function AddDoc($aDoc){
		foreach($aDoc as $k=>$v){
			$this->$k = $v;
		}
		return $this;
	}

}