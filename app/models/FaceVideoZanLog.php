<?php
class FaceVideoZanLog  extends Eloquent {


	protected $table='face_video_zan_log';
	protected $guarded = array('id');
	public $timestamps = true;
//    protected $softDelete = true;
    
    //查看用户是否点赞
    public static function isUserZan($iVideoId,$iUserId,$iCatId=1){
    	$oFaceVideoZanLog = FaceVideoZanLog::where('video_id',$iVideoId)
    									->where('user_id',$iUserId)
    									->where('cat_id',$iCatId)
    									->first();
    	if($oFaceVideoZanLog){
    		return true;
    	}else{
    		return false;
    	}
    }
    public function scopeSelzanlog($query,$iVideoId,$iCatId=1){
    	return $query->where('video_id','=',$iVideoId)->where('cat_id',$iCatId);
    }
    //点赞
    public static function UserZan($iVideoId,$iUserId,$iCatId=1){
    	if($iCatId==1){
	    	$oFaceVideo = FaceVideo::find($iVideoId);
	    	$oFaceVideo->increment('video_support');
    	}elseif($iCatId==2){
    		$oConsultation = Consultation::find($iVideoId);
	    	$oConsultation->increment('meeting_support');
    	}else{
    		$oTrain = UserTrain::find($iVideoId);
    		$oTrain->increment('train_support');
    	}
    	$aFaceVideoZan = array(
    			'video_id'		=> $iVideoId,
    			'user_id'		=> $iUserId,
    			'cat_id'		=> $iCatId
    	);
    	$oFaceVideoZan = new FaceVideoZanLog($aFaceVideoZan);
    	$oFaceVideoZan->save();
    }
}