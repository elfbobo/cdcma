<?php
class Hospital  extends Eloquent {


	protected $table='hospital';
	protected $guarded = array('id');
	
	public static function getChildren($iPid = 0){
		
		$sChild = User::getHospitalCache();
		$aChild = json_decode($sChild);
		if(isset($aChild->$iPid)){
			return $aChild->$iPid;
		}else{
			return array();
		}
	}
	
	public static function getChildrenAPI($iPid = 0){
		$arr = Hospital::getChildren($iPid);
		$aRes = array();
		foreach($arr as $k=>$v){
			$aRes[] = array('id'=>$k,'name'=>$v);
		}
		return $aRes;
	}
}