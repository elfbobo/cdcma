<?php
class AspirinScreeningHitLog  extends Eloquent {

	protected $table='aspirin_screening_hit_log';
	protected $guarded = array('id');
	public $timestamps = true;
	
}