<?php

class AspirinVideoAd extends Eloquent
{
    protected $table = 'aspirin_video_ad';
    // 集体赋值黑名单
    protected $guarded = array('id');

    public $timestamps = true;
}