<?php
class CaseVote extends Eloquent {


	protected $table='case_vote';
	protected $guarded = array('id');
	public $timestamps = true;
    //protected $softDelete = true;
    
	public function scopeOrderByCreatedAt($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }
    
}