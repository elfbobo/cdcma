<?php

use Patchwork\PHP\Shim\Iconv;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	const HASH_EXT_KEY_HASHID = 'dasfgfsdbz';
	const HASH_EXT_KEY_CHECKID = 'hiewrsbzxc';
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');
	
	public $timestamps = true;
//    protected $softDelete = true;

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'remember_token';
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->user_email;
	}
	
	public function scopeOrderByCreatedAt($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }
	
    //取得所有非超管用户
	public static function findAllUser(){
		$oUsers = User::where('role_id','>',1)->orderByCreatedAt()->get();
		return $oUsers;
	}
	
	//用户验证字段(后台)
	public static function getUserRule(){
		return array(
				'user_name'			=> 'required',
//				'role_id'			=> 'required',
//				'user_cwid'			=> 'required',
				'user_nick'			=> 'required',
				'user_tel'			=> 'required',
				'user_email'		=> 'required',
				'user_city'         => 'required',
//				'user_department'	=> 'required',
//				'user_company'		=> 'required',
		);
	}
	
	//添加用户
	public function AddUser($aUser){
		if(!isset($this->role_id)){
			$aUser['role_id'] = 2 ;	//表示代表
		}
		$sPassword = '123456';
		if($aUser['user_tel']){
			$sPassword = $aUser['user_tel'];
		}
		if(!isset($this->password)){
			$aUser['password'] = Hash::make($sPassword);
		}
		
		$aUser['user_cwid'] =  strtoupper($aUser['user_cwid']);
		
		foreach($aUser as $k=>$v){
			$this->$k = $v;
		}
		
		return $this;
	}
	
	//添加分组
	public function AddGroup($aUser){
		$aUser['role_id'] = 2 ;	//表示代表
		$sPassword = '123456';
		$aUser['password'] = Hash::make($sPassword);
		$aUser['rep_type'] = 3;
		foreach($aUser as $k=>$v){
			$this->$k = $v;
		}
	
		return $this;
	}
	
	//注册用户
	public function Register($aUser,$iRoleId=3){
		$aUser['role_id'] = $iRoleId ;
		$sPassword = $aUser['password'];
		$aUser['password'] = Hash::make($sPassword);
		
		foreach($aUser as $k=>$v){
			$this->$k = $v;
		}
		
		if($iRoleId==2){		//代表
			$this->user_cwid = $aUser['user_nick'];
		}
		
		return $this;
	}
	
	//用户信息是否重复
	public static function IsUserMsgRepeat($aUser,$id= 0){
		$rule = array(
						'user_nick' => '用户名',
						'user_tel' => '手机号码',
//						'user_email' => '邮箱'
						);
		foreach($rule as $k=>$v){
			$oUser = User::where($k,$aUser[$k]);
			if($id){
				$oUser = $oUser->where('id','!=',$id);
			}
			$oUser = $oUser->get();
			if(count($oUser)){
				return "该".$v."用户已经存在！";
			}
		}
		return false;
	}
	
	//用户昵称是否重复
	public static function IsUserNickRepeat($aUser,$id= 0){
		$rule = array(
						'user_nick' => '用户名',
						);
		foreach($rule as $k=>$v){
			$oUser = User::where($k,$aUser[$k]);
			if($id){
				$oUser = $oUser->where('id','!=',$id);
			}
			$oUser = $oUser->get();
			if(count($oUser)){
				return "该".$v."用户已经存在！";
			}
		}
		return false;
	}
	
	//用户手机号码和邮箱是否重复
	public static function IsUserTelEmailRepeat($aUser,$id= 0){
		$rule = array(
				'user_tel' => 'tel_repeat',
				'user_email' => 'email_repeat',
		);
		foreach($rule as $k=>$v){
			$oUser = User::where($k,$aUser[$k]);
			if($id){
				$oUser = $oUser->where('id','!=',$id);
			}
			$oUser = $oUser->get();
			if(count($oUser)){
				return $v;
			}
		}
		return false;
	}
	
	/*获得积分排行
	 */
	public static function getUserScoreList($iRoleId,$iCount=100,$iUserId=0,$scoretype=''){
		if($iUserId>0&&$iRoleId==2){
			$oUser = User::select('role_id','user_regin')->where('id',$iUserId)->first();
			$aReginMe = Config::get('config.regin_me');
			if($oUser&&$oUser->role_id==2&&in_array($oUser->user_regin, $aReginMe)){
				//表示ME的代表
				$sCreditRank_key = 'creditRankMe'.$iRoleId."_".$scoretype;
				if (Cache::has($sCreditRank_key)) {
					$aUser = unserialize(Cache::get($sCreditRank_key));
				}else{
					//获取前一百名用户的积分排行
					if($scoretype=="integral"){
						$aUser = User::select('user_name','health_live_integral','health_video_integral','health_integral','health_integrals','id','role_id')
								->where('role_id',$iRoleId)
								//->where(0,'=',0)
								->whereIn('user_regin',$aReginMe)
								->orderBy('health_integrals','DESC')
								->take($iCount)
								->get()
								->toArray();
					}else{
						$aUser = User::select('user_name','user_live_score','user_review_score','user_score','id','role_id')
								->where('role_id',$iRoleId)
								->where('user_score','>',0)
								->whereIn('user_regin',$aReginMe)
								->orderBy('user_score','DESC')
								->take($iCount)
								->get()
								->toArray();
					}
					//缓存是10分钟
					Cache::put($sCreditRank_key,serialize($aUser),10);
				}
				return $aUser;
			}
		}
		$sCreditRank_key = 'creditRank'.$iRoleId."_".$scoretype;
		if (Cache::has($sCreditRank_key)) {
			$aUser = unserialize(Cache::get($sCreditRank_key));
		}else{
			//获取前一百名用户的积分排行
			if($scoretype=="integral"){
				$aUser = User::select('user_name','health_live_integral','health_video_integral','health_integral','health_integrals','id','role_id')
					->where('role_id',$iRoleId)
					//->where(0,'=',0)
					->orderBy('health_integrals','DESC')
					->take($iCount)
					->get()
					->toArray();
			}else{
				$aUser = User::select('user_name','user_live_score','user_review_score','user_score','id','role_id')
					->where('role_id',$iRoleId)
					->where('user_score','>',0)
					->orderBy('user_score','DESC')
					->take($iCount)
					->get()
					->toArray();
			}
			//缓存是10分钟
			Cache::put($sCreditRank_key,serialize($aUser),10);
		}
		return $aUser;
	}
	
	/*获得积分排行
	 */
	public static function getUserScoreListObj($iRoleId,$iCount=100){
		$sCreditRank_key = 'creditRankobj'.$iRoleId;
		if (Cache::has($sCreditRank_key)) {
			$aUser = unserialize(Cache::get($sCreditRank_key));
		}else{
			//获取前一百名用户的积分排行
			$aUser = User::select('user_name','user_live_score','user_review_score','user_score','id','role_id')
			->where('role_id',$iRoleId)
			->where('user_score','>',0)
			->orderBy('user_score','DESC')
			->take($iCount)
			->get();
			//缓存是10分钟
			Cache::put($sCreditRank_key,serialize($aUser),10);
		}
		return $aUser;
	}
	
	/*
	 * 获取当前用户积分排行
	 */
	public static function getUserRankById($iRoleId,$iUserId,$scoretype=''){
		$iUserRank = 0;
		if(!$scoretype){
			$aRank = User::where('role_id',$iRoleId)->where('user_score','>',0)
				->orderBy('user_score','desc')
				->lists('user_score','id');
		}else{
			$aRank = User::where('role_id',$iRoleId) //->where(0,'=',0)
				->orderBy('health_integrals','desc')
				->lists('health_integrals','id');
		}
		$aRank = array_keys($aRank);
		if(in_array($iUserId,$aRank)){
			$aRankInfo = array_keys($aRank,$iUserId);
			$iUserRank = implode('',$aRankInfo) + 1;
		}
		return $iUserRank;
	}

	//积分排行榜用户重新排序
	public static function SortUsers($oUser){
		$iCount = count($oUser);
		for($i=0;$i<$iCount-1;$i++){
			for($j=$i+1;$j<$iCount;$j++){
				if($oUser[$i]->user_score<$oUser[$j]->user_score){
					$temp = $oUser[$i];
					$oUser[$i] = $oUser[$j];
					$oUser[$j] = $temp;
				}
			}
		}
		return $oUser;
	}
	
	public static function getDaibiaoDocLiveMin($iUid,$type=1){
		$oUser = User::where('link_user_id',$iUid)->get();
		$iScore = 0;
		foreach($oUser as $k=>$v){
			$iS = VideoLog::getUserMinByType($v->id,$type);
			$iScore += $iS;
		}
		return $iScore;
	}
	
	//生成邀请码
	public static function MakeInviteCode(){
		$s = User::GenerateStr();
		if($s=="BAYER1"||$s=="BAYER2"){
			return User::MakeInviteCode();
		}
		//该邀请码是否存在
		$iCount = User::where('invite_code',$s)->count();
		if($iCount==0){
			return $s;
		}else{
			return User::MakeInviteCode();
		}
	}
	
	public static function  GenerateStr( $length = 6 ) {  
		// 密码字符集，可任意添加你需要的字符  
//		$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';  
		$chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';
		$str = '';  
		for ( $i = 0; $i < $length; $i++ )  
		{  
		// 这里提供两种字符获取方式  
		// 第一种是使用 substr 截取$chars中的任意一位字符；  
		// 第二种是取字符数组 $chars 的任意元素  
		// $password .= substr($chars, mt_rand(0, strlen($chars) – 1), 1);  
			$str .= $chars[ mt_rand(0, strlen($chars) - 1) ];  
		}  
		return strtoupper($str);
	} 
	
	//liuy  S==========================================================
	/**
	 * 根据邀请码 获取用户信息
	 * @param unknown_type $sCode  邀请码
	 */
	public static function getExitUserByCode( $sCode ){
		return self::where('invite_code', $sCode)
							->first();
	}
	//liuy  E==========================================================
	//前台注册
	public static function getRegUserRule(){
		return array(
			'user_nick' =>  'required|unique:user',
			'password' =>  'required|confirmed',//confirmed用于验证两次密码一致
			'user_name' => 'required',
			'user_sex' => 'required',
			'user_tel' => 'required|unique:user',
			'user_email' => 'required|email|unique:user',
			'user_department' => 'required',
			'user_position' => 'required',
//			'user_company' => 'required'
		);
	}
	//前台注册--代表
	public static function getRegUserRuleRep(){
		return array(
			'user_nick' =>  'required|unique:user',
			'password' =>  'required|confirmed',//confirmed用于验证两次密码一致
			'user_name' => 'required',
			'user_sex' => 'required',
			'user_tel' => 'required|unique:user',
			'user_email' => 'required|email|unique:user',
			'user_regin' => 'required',
			'user_area' => 'required',
//			'user_company' => 'required'
		);
	}
	public static function getRegUserError(){
		return array(
			'user_nick.required' => '用户名不可为空！',
			'user_nick.unique' => '该用户名已被占用！',
			'password.required' => '密码不可为空！',
			'password.confirmed' => '两次密码输入不一致！',
			'user_name.required' => '姓名不可为空！',
			'user_sex.required' => '性别不可为空！',
			'user_tel.required' => '手机不可为空！',
			'user_tel.unique' => '手机已注册！',
			'user_email.required' => '邮箱不可为空！',
			'user_email.email' => '邮箱格式错误！',
			'user_email.unique' => '邮箱已注册！',
			'user_department.required' => '科室不可为空！',
			'user_position.required' => '职称不可为空',
			'user_company.required' => '单位不可为空！'
		);
	}
	 static function getRegUserErrorRep(){
		return array(
			'user_nick.required' => '用户名不可为空！',
			'user_nick.unique' => '该用户名已被占用！',
			'password.required' => '密码不可为空！',
			'password.confirmed' => '两次密码输入不一致！',
			'user_name.required' => '姓名不可为空！',
			'user_sex.required' => '性别不可为空！',
			'user_tel.required' => '手机不可为空！',
			'user_tel.unique' => '手机已注册！',
			'user_email.required' => '邮箱不可为空！',
			'user_email.email' => '邮箱格式错误！',
			'user_email.unique' => '邮箱已注册！',
			'user_regin.required' => '大区不可为空！',
			'user_area.required' => '地区不可为空！',
			'user_company.required' => '单位不可为空！'
		);
	}
	
	/**
	 * 后台导入用户
	 */
	public function adminImport($aFile){
		if ($aFile['file']){
			if($aFile['file']['error']>0){
				$error = $aFile['file']['error'];
				$json = array('error'=>$error);
			    echo 1;
			    die;
			}else{
			    $ext = get_filetype($aFile['file']['name']);
			    if(trim($ext) != 'csv'){
			        showmessage('文件类型有误，请选择csv文件');
			        die;
			    }
	
			    $handle = @fopen($aFile['file']['tmp_name'], 'r');
				$total_num =0;
				$success_num = 0;
				$erreo_num = 0;
				$erreo_name = '';
				$key = 0;
				$oRoles = DB::table('role')->lists('name','id');
				while (!feof($handle)){
					$key++;
				   $value = eval('return '.mb_convert_encoding(var_export(fgetcsv($handle),true),'utf-8','gbk').';');//编码转换
				    if($key == 1){
						continue;
					}
					if(!$value){
						$erreo_num++;
					    $total_num++;
						$erreo_name .= $key.'行(未读取到该行数据或者该行为空)、<br>';
						continue ;
					}
					if ( !$value[0] ||!$value[1]||!$value[2]||!$value[3]){
						$erreo_num++;
					    $total_num++;
						$erreo_name .= $key.'行(不能为空的字段存在空数据)、<br>';
					}else{
						$sex = 1;
						if($value[10] == 2){
							$sex=2;
						}
						$aUser = array(
										'user_name'			=>	$value[0],
										'user_cwid'			=>	$value[1],
										'user_nick'			=>	$value[2],
										'user_tel'			=>	$value[3],
										'user_email'		=>	$value[4],
										'user_address'		=>	$value[5],
										'user_city'			=>	$value[6],
										'user_department'	=>	$value[7],
										'user_position'		=>	$value[8],
										'user_company'		=>	$value[9],
										'user_sex'			=>	$sex,
										'user_regin'		=>	$value[11],
										'user_area'			=>	$value[12],
						);
						//用户信息是否重复
						$msg = User::IsUserMsgRepeat($aUser);
						if($msg){
							$erreo_num++;
						    $total_num++;
							$erreo_name .= $key.'行('.$msg.')、<br>';
						}else{
							//开始导入
							$oUser = new User();
							//验证成功
							if($oUser->addUser($aUser)->save()){
								$success_num++;
								$total_num++;
							}else{
								$erreo_num++;
							    $total_num++;
								$erreo_name .= $key.'行(添加失败！)、<br>';
							}
						}
					}
				}
					
			}
		}
		$result= '表中一共有'.$total_num.'条记录需要导入。<br>成功导入：'.$success_num.'条（默认密码为用户手机号码）。<br>';
		$result .= '未成功导入：'.$erreo_num.'条。<br>'.$erreo_name;
		return $result;
	}
	
	public static function getDepartment(){
		$arr = array(
				'1'	=> '心血管内科',
				'2' => '神经内科',
				'3' => '肾病内科',
				'4' => '内分泌科',
				'5' => '其它'
		);
		return $arr;
	}
	
	public static function getDepartmentNonTarget(){
		$arr = array(
				'1'	=> '心血管内科',
				'2' => '神经内科',
				'3' => '肾病内科',
				'4' => '内分泌科',
				'6'	=> '内科',
				'7'	=> '外科',
				'8' => '中医科',
				'9'	=> '药剂科',
				'10'=> '行政科室',
				'5' => '其它'
		);
		return $arr;
	}
	
	public static function getPosition(){
		$arr = array(
				'1'	=> '住院医师',
				'2' => '主治医师',
				'3' => '副主任医师',
				'4' => '主任医师',
				'5' => '其它'
		);
		return $arr;
	}
	
	//获取试点城市id数组
	public static function getPilotCity(){
		$arr = array(
			'1'=> '420500',
			'2'=>'371000',
			'3'=>'320100',
			'4'=>'320300',
			'5'=>'110100',
			'6'=>'120100',
			'7'=>'310100',
			'8'=>'320500',
			'9'=>'330100',
			'10'=>'370100',
			'11'=>'370200',
			'12'=>'420100',
			'13'=>'430100',
			'14'=>'440100',
			'15'=>'510100'
		);
		// $arr = array('1'=> '420500','2'=>'371000','3'=>'320100','4'=>'320300');
// 		$arr = array('1'=> '420500','2'=>'110100');
		return $arr;
	}
	//获取试点区县id数组
	public static function getPilotCountry(){
		$arr = array('1'=> '371081','2'=> '371082','3'=> '320321','4'=> '320322',
				'5'=> '320302','6'=> '320303','7'=> '320305','8'=> '320311',
				'9'=> '320312','10'=> '320324','11'=> '320381','12'=> '320382');
		return $arr;
	}
	
	public static function getClusterByReginId($iReginId){
		$sCluster = $iReginId;
		if(!is_numeric($iReginId)){
			return $sCluster;die;
		}
		$iClusterid = Regin::where('id',$iReginId)->pluck('cluster_id');
		if(!$iClusterid){
		 return $iReginId;
		}
		$oClusters = Cluster::find($iClusterid);
		if(!$oClusters){
		  return $iReginId;
		}
		$sCluster = $oClusters ->cluster_name;
		if(!$sCluster){
			return $iReginId;
		}
		return $sCluster;
	}
	
	public static function getClusterByReginId2($iReginId){
		if(!is_numeric($iReginId)){
			return '';
		}else{
			return User::getClusterByReginId($iReginId);
		}
		
	}
	
	public static function getAllCluster(){
		return Cluster::lists('cluster_name','id');
	}
	
	public static function getRepId($sSuperiorIds){
		if(!$sSuperiorIds){
			return 0;
		}
		$aSuperiorIds = mb_unserialize($sSuperiorIds);
		if(count($aSuperiorIds)==1){
			return $aSuperiorIds[0];
		}
		foreach($aSuperiorIds as $k=>$v){
			$oUser = User::find($v);
			if($oUser&&$oUser->role_id==2){
				return $v;
			}
		}
		return 0;
	}
	
	/**
	 * 更新代表的大区地区信息
	 */
	public function UpdateUserReginAreaDo($aFile){
		if ($aFile['file']){
			if($aFile['file']['error']>0){
				$error = $aFile['file']['error'];
				$json = array('error'=>$error);
				echo 1;
				die;
			}else{
				$ext = get_filetype($aFile['file']['name']);
				if(trim($ext) != 'csv'){
					showmessage('文件类型有误，请选择csv文件');
					die;
				}
	
				$handle = @fopen($aFile['file']['tmp_name'], 'r');
				$total_num =0;
				$success_num = 0;
				$erreo_num = 0;
				$erreo_name = '';
				$key = 0;
				$oRoles = DB::table('role')->lists('name','id');
				while (!feof($handle)){
					$key++;
					$value = eval('return '.mb_convert_encoding(var_export(fgetcsv($handle),true),'utf-8','gbk').';');//编码转换
					if(!$value){
						$erreo_num++;
						$total_num++;
						$erreo_name .= $key.'行(未读取到该行数据或者该行为空)、<br>';
						continue ;
					}
					if ( !$value[0] ||!$value[1]||!$value[2]){
						$erreo_num++;
						$total_num++;
						$erreo_name .= $key.'行(不能为空的字段存在空数据)、<br>';
					}else{
						$oUser = User::where('user_cwid',trim($value[0]))->first();
						if(!$oUser){
							$erreo_num++;
							$total_num++;
							$erreo_name .= $key.'行(未找到该cwid用户)、<br>';
						}else{
							if(!is_numeric($value[1])||!is_numeric($value[2])){
								$erreo_num++;
								$total_num++;
								$erreo_name .= $key.'行(大区地区id必须为数字)、<br>';
							}else{
								//验证成功
								$oUser->user_regin = trim($value[1]);
								$oUser->user_area = trim($value[2]);
								if($oUser->save()){
									$success_num++;
									$total_num++;
								}else{
									$erreo_num++;
									$total_num++;
									$erreo_name .= $key.'行(更新失败！)、<br>';
								}
							}
						}
					}
				}
					
			}
		}
		$result= '表中一共有'.$total_num.'条记录需要导入。<br>成功导入：'.$success_num.'条。<br>';
		$result .= '未成功导入：'.$erreo_num.'条。<br>'.$erreo_name;
		return $result;
	}
	
	//取缓存数据处理
	public static function getAreaCache(){
		if(Cache::has('area')){
			return Cache::get('area','');
		}
		$oArea = DB::table('area')->get();
		$aArea = array();
		foreach($oArea as $k=>$v){
			$aArea[$v->regin_id][$v->id] = $v->area_name;
		}
		$sArea = json_encode($aArea);
		Cache::forever('area', $sArea);
		return $sArea;
	}
	
	public static function getReginCache(){
		/*
		if(Cache::has('regin')){
			return Cache::get('regin','');
		}*/
		$oRegin = DB::table('regin')->get();
		$aRegin = array();
		foreach($oRegin as $k=>$v){
			$aRegin[$v->id] = $v->regin_name;
		}
		$sRegin = json_encode($aRegin);
		Cache::forever('regin', $sRegin);
		return $sRegin;
	}
	
	public static function getHospitalCache(){
		if(Cache::has('cache_hospital')){
			return Cache::get('cache_hospital','');
		}
		$aCacheHospital = array();
		$aProvince = Hospital::where('parent_id',0)
							->lists('name','code');
		$aCacheHospital[0] = $aProvince;
		foreach($aProvince as $key_pro=>$val_pro){
			$aCity = Hospital::where('parent_id',$key_pro)
							->lists('name','code');
			$aCacheHospital[$key_pro] = $aCity;
			foreach($aCity as $key_city=>$val_city){
				$aCounty = Hospital::where('parent_id',$key_city)
							->lists('name','code');
				$aCacheHospital[$key_city] = $aCounty;
				foreach($aCounty as $key_county=>$val_county){
					$aHospital = Hospital::where('parent_id',$key_county)
							->lists('name','code');
					$aCacheHospital[$key_county] = $aHospital;
				}
			}
		}
		Cache::forever('cache_hospital', json_encode($aCacheHospital));
		return json_encode($aCacheHospital);
	}
	
	//为用户注册医脉通
	public static function RegMedlive($oUser){
		if(!$oUser||$oUser->medlive_id>0){
			return;
		}
		
		//慢病重复用户数据操作start
		//查找慢病用户是否有和当前用户手机号码重复的医生
		$oUserRepeat = User::where('user_tel',$oUser->user_tel)->where('role_id',$oUser->role_id);
		$oUserRepeat = $oUserRepeat->where('id','!=',$oUser->id);
		$oUserRepeat = $oUserRepeat->first();
		if($oUserRepeat){
			$iUserRepeatId = $oUser->id;		
			if($oUserRepeat->medlive_id){
				$oUser->medlive_id = $oUserRepeat->medlive_id;
				$oUser->save();
				$oUserMedliveRegLog = UserMedliveRegLog::where('user_id',$oUserRepeat->id)
										->orderBy('id','DESC')
										->first();
				$aUserMedliveRegLog = array(
							'user_id'		=> $oUser->id,
							'medlive_id'	=> $oUserRepeat->medlive_id,
							'success_flag'	=> $oUserMedliveRegLog?$oUserMedliveRegLog->success_flag:0,
							'success_msg'	=> $oUserMedliveRegLog?$oUserMedliveRegLog->success_msg:'',
							'default_password'=> $oUserMedliveRegLog?$oUserMedliveRegLog->default_password:'',
				);
				$oUserMedliveRegLog = new UserMedliveRegLog($aUserMedliveRegLog);
				$oUserMedliveRegLog->save();
				return ;
			}
		}
		
		//慢病重复用户数据操作end
		
		$sTelBack = str_replace('-','',trim($oUser->user_tel));
		$sTel = $sTelBack;
		//判断是否符合医脉通的手机号码正则
		$mobile =  '/^1[34578][0-9]{9}$/';
		if(!preg_match($mobile,$sTel)){
			//手机验证未通过
			$sTel = '';
		}
		
		$sEmail = trim($oUser->user_email);
		$email =  '/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/';
		if(!preg_match($email,$sEmail)){
			$sEmail = '';
		}
		
		if($sTelBack){
			$sPassword = substr($sTelBack, -6,6);
		}else{
			$sPassword = '123456';
		}
		//拼出注册医脉通需要的字段数组
		$aMedliveInfo = array(
				'password'		=> $sPassword,
				'email'			=> $sEmail,
				'name'			=> $oUser->user_name,
				'gender'		=> $oUser->user_sex-1,
				'company1'		=> $oUser->user_province,
				'company2'		=> $oUser->user_city,
				'company3'		=> $oUser->user_county,
				'company4'		=> $oUser->user_company,
				'company_other'	=> $oUser->user_company_name,
				'app_name'		=> Config::get('config.medlive_reg_app_name'),
				'reg_type'		=> 'web'
		);
		
		if($sTel){
			$aMedliveInfo['mobile'] = $sTel;
		}
		
		switch ($oUser->user_department){
			case '心血管内科':
				$aMedliveInfo['profession'] = '10000';
				$aMedliveInfo['profession2'] = '10100';
				$aMedliveInfo['profession3'] = '10101';
				break;
			case '神经内科':
				$aMedliveInfo['profession'] = '10000';
				$aMedliveInfo['profession2'] = '10100';
				$aMedliveInfo['profession3'] = '10102';
				break;
			case '肾病内科':
				$aMedliveInfo['profession'] = '10000';
				$aMedliveInfo['profession2'] = '10100';
				$aMedliveInfo['profession3'] = '10107';
				break;
			case '内分泌科':
				$aMedliveInfo['profession'] = '10000';
				$aMedliveInfo['profession2'] = '10100';
				$aMedliveInfo['profession3'] = '10104';
				break;
			default:
				break;
		}
		//调用接口注册医脉通
		$sMedliveRegApi = MEDLIVE_REGISTER_API;
		$aRes = requestApiByCurl($sMedliveRegApi,$aMedliveInfo);
		$iSuccessFlag = 0;
		$iMedliveId = 0;
		$sSuccessMsg = '';
		if(!empty($aRes)&&!empty($aRes['data'])&&!empty($aRes['data']['user'])){
			//成功返回用户的medlive_id
			$iMedliveId = intval($aRes['data']['user']);
			if(!empty($aRes['err_msg'])){
				$iSuccessFlag = 2;
				$sSuccessMsg = $aRes['err_msg'];
			}else{
				$iSuccessFlag = 1;
				$sSuccessMsg = "注册医脉通成功";
			}
		}else{
			$iMedliveId = 0;
			$iSuccessFlag = 0;
			if(!empty($aRes['err_msg'])){
				$sSuccessMsg = $aRes['err_msg'];
			}else{
				$sSuccessMsg = "接口未成功返回医脉通用户信息";
			}
		}
		$aUserMedliveRegLog = array(
				'user_id'		=> $oUser->id,
				'medlive_id'	=> $iMedliveId,
				'success_flag'	=> $iSuccessFlag,
				'success_msg'	=> $sSuccessMsg,
				'default_password'=> ($iSuccessFlag==1)?$sPassword:''
		);
		$oUserMedliveRegLog = new UserMedliveRegLog($aUserMedliveRegLog);
		$oUserMedliveRegLog->save();
		
		if($iMedliveId>0){
			//更新用户的medlive_id字段
			$oUser->medlive_id = $iMedliveId;
			$oUser->save();
		}
		return $iMedliveId;
	}
	
	public static function getUserAdalateType(){
		return 1;
	}
	
	/**
	 * 判断当前用户是否是病例征集的管理员
	 */
	public static function isCaseAdmin(){
		if (Auth::check()){
			$oUserAdminCase = UserAdminCase::where('user_id',Auth::User()->id)->first();
			if(count($oUserAdminCase)||Auth::User()->role_id==1){
				return true;
			}
		}
		return false;
	}
	
	//获取hashid和checkid 通过不同的key值 31730733268915, 332380283569776
	public static function getHashidOrCheckid($user, $downloadKey=''){
		//如果当前用户为空或者不存在，直接返回0
		if (empty($user)) {
			return '0';
		}
		$crc = intval(sprintf('%u', crc32($downloadKey . "asdfwrew.USER_SEED")));
		$hash = $crc - $user;
		$hash2 = sprintf('%u', crc32($hash . 'werhhs.USER_SEED2'));
		$k1 = substr($hash2, 0, 3);
		$k2 = substr($hash2, -2);
		return $k1 . $hash . $k2;
	}
	
	/**
	 * 根据用户id获取医脉通id
	 * @param unknown $iUserId
	 */
	public static function getDocUserMedliveId($iUserId){
		$oUser = User::select('id','role_id','medlive_id')->where('id',$iUserId)->first();
		//若不是医生，不加麦粒
		if(!$oUser||$oUser->role_id != 3){
			return 0;
		}
		
		$iMedliveId = $oUser->medlive_id;
		if(!$iMedliveId){
			//为用户注册医脉通
			$oUser = User::find($iUserId);
			$iMedliveId = User::RegMedlive($oUser);
		}
		return $iMedliveId;
	}
	
	//获取当前用户的麦粒数
	public static function getUserMLCount($iMedliveId){
		$sHashId =  self::getHashidOrCheckid($iMedliveId, self::HASH_EXT_KEY_HASHID);
		$sCheckId = self::getHashidOrCheckid($iMedliveId, self::HASH_EXT_KEY_CHECKID);
		$sUrl = 'http://api.'.TOP_DOMAIN.'/user/get_user_account.php';
		$aReturn = requestApiByCurl($sUrl,array('hashid'=>$sHashId,'checkid'=>$sCheckId),'get');
		if(isset($aReturn['data'])&&isset($aReturn['data']['maili'])){
			return ($aReturn['data']['maili']+(isset($aReturn['data']['maili_lastyear'])?$aReturn['data']['maili_lastyear']:0));
		}else{
			return 0;
		}
	}
	
}
