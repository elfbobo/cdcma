<?php
class AspirinOnlineVideoScore  extends Eloquent {


	protected $table='aspirin_online_video_score';
	protected $guarded = array('id');
	public $timestamps = true;
	
	public static function submitScore($iUid,$iVideoId,$iScore1,$iScore2,$iScore3)
	{
		$oVideo = AspirinOnlineSpeaker::where('id',$iVideoId)->first();
		if(!$oVideo){
			echo 'error';die;
		}
		$oScoreLog = AspirinOnlineVideoScore::where('video_id',$iVideoId)->where('user_id',$iUid)->first();
		if(count($oScoreLog)){
			if($oScoreLog->content_degree != 0 && $oScoreLog->class_level != 0 && $oScoreLog->overall_score != 0){
				echo 'again';die;
			}
			if($iScore1 != 0){
				$oScoreLog->content_degree = $iScore1;
			}
			if($iScore2 != 0){
				$oScoreLog->class_level = $iScore2;
			}
			if($iScore3 != 0){
				$oScoreLog->overall_score = $iScore3;
			}
			$oScoreLog->save();
		}else{
			$aScore = array(
					'video_id'       => $iVideoId,
					'user_id'        => $iUid,
					'content_degree' => isset($iScore1)?$iScore1:0,
					'class_level'    => isset($iScore2)?$iScore2:0,
					'overall_score'  => isset($iScore3)?$iScore3:0,
					'created_at'     => date('Y-m-d H:i:s',time())
			);
			$oScore = new AspirinOnlineVideoScore($aScore);
			$oScore->save();
		}
		echo 'success';die;
	}
}