<?php
class AspirinOnline  extends Eloquent {


	protected $table='aspirin_online';
	protected $guarded = array('id');
	public $timestamps = true;
	
	/**
	 * 根据起始和结束日期获取此时间段内非周末的日期数组   dll
	 */
	public static function getOnlineDate($sStartTime,$sEndTime)
	{
		$sStart = strtotime($sStartTime);
		$sEnd = strtotime($sEndTime);
		$aDateInfo = array();
		for($i = $sStart;$i<=$sEnd;$i += 86400){
			$aDateInfo[] = date('Y-m-d',$i);
		}
		
		foreach($aDateInfo as $v){
			$iWeek = date('w',strtotime($v));
			if($iWeek != 0 && $iWeek != 6){
				$aDate[] = $v;
			}
		}
		return $aDate;
	}
	/***
	 * 以半小时为基准，09:00-09:29用91表示，09:30-09:59用92表示； 13:00-13:29用131表示，13:30-13:59用132表示； 
	 */
	public static function getTimePeriod($sStart,$sEnd)
	{
// 		$sStart = '2016-09-22 09:00:00';
// 		$sEnd = '2016-09-22 10:00:00';
		
		//开始时间
		if(substr($sStart,14,2)<30 && substr($sStart,14,2)>=0){
			if(substr($sStart,11,1) == 0){
				$iStart = substr($sStart,12,1).'1';
			}else{
				$iStart = substr($sStart,11,2).'1';
			}
		}elseif(substr($sStart,14,2)>=30 && substr($sStart,14,2)<=59){
			if(substr($sStart,11,1) == 0){
				$iStart = substr($sStart,12,1).'2';
			}else{
				$iStart = substr($sStart,11,2).'2';
			}
		}
		
		//结束时间
		if(substr($sEnd,14,2)<=30 && substr($sEnd,14,2)>0){
			if(substr($sEnd,11,1) == 0){
				$iEnd = substr($sEnd,12,1).'1';
			}else{
				$iEnd = substr($sEnd,11,2).'1';
			}
		}elseif(substr($sEnd,14,2)>30 && substr($sEnd,14,2)<=59){
			if(substr($sEnd,11,1) == 0){
				$iEnd = substr($sEnd,12,1).'2';
			}else{
				$iEnd = substr($sEnd,11,2).'2';
			}
		}elseif(substr($sEnd,14,2) == 0){
			if(substr($sEnd,11,1) == 0){
				$iEnd = (substr($sEnd,12,1)-1).'2';
			}else{
				$iEnd = (substr($sEnd,11,2)-1).'2';
			}
		}
		
		$aTime = Config::get('config.time_code_flag');
		$ikey1 = implode(array_keys($aTime,$iStart));
		$ikey2 = implode(array_keys($aTime,$iEnd));
		$iLength = $ikey2-$ikey1+1;
		$res = array_slice($aTime, $ikey1,$iLength);
		return $res;
	}
	
}