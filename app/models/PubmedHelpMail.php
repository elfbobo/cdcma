<?php
class PubmedHelpMail extends Eloquent{
    protected $table = 'pubmed_help_email';
    public $timestamps = true;
    protected $guarded = array('id');
    
    //文献求助发送邮件
    public static function SendEmail($subject,$content){
    	$data = array('content'=>$content);
    	Mail::send('emails.pubmed.reminder', $data, function($message) use ($subject)
    	{
    		$sEmail = Config::get('config.pubmed_email');
    		$message->to($sEmail, 'Pubmed')->subject($subject);
    	});
    }
}
