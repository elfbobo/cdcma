<?php
class FaceVideoScore extends Eloquent {

	protected $table='face_video_score';
	protected $guarded = array('id');
	public $timestamps = true;
	
	public static function addScoreLog($iVideoId,$iUserId,$iScore1,$iScore2,$iScore3,$iDevice)
	{
		$oScoreLog = FaceVideoScore::where('face_video_id',$iVideoId)->where('user_id',$iUserId)->first();
		if(count($oScoreLog)){
			if($iScore1 != 0){
				$oScoreLog->content_degree = $iScore1;
			}
			if($iScore2 != 0){
				$oScoreLog->class_level = $iScore2;
			}
			if($iScore3 != 0){
				$oScoreLog->overall_score = $iScore3;
			}
			$oScoreLog->device_type = $iDevice;
			$oScoreLog->save();
		}else{
			$aScore = array(
					'face_video_id'  => $iVideoId,
					'user_id'        => $iUserId,
					'content_degree' => isset($iScore1)?$iScore1:0,
					'class_level'    => isset($iScore2)?$iScore2:0,
					'overall_score'  => isset($iScore3)?$iScore3:0,
					'device_type'    => $iDevice,
					'created_at'     => date('Y-m-d H:i:s',time())
			);
			$oScore = new FaceVideoScore($aScore);
			$oScore->save();
		}
	}
}