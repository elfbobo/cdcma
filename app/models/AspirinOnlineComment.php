<?php
class AspirinOnlineComment  extends Eloquent {

	protected $table='aspirin_online_comment';
	protected $guarded = array('id');
	public $timestamps = true;
    
    public static function getComment($iId,$iPage,$iPagesize){
    	if($iPage != 0 && $iPagesize != 0){
    		$oComments = AspirinOnlineComment::where('video_id',$iId)
							    	->orderBy('created_at','desc')
							    	->skip(($iPage-1)*$iPagesize)
									->take($iPagesize)
    								->get();
    	}else{
    		$oComments = AspirinOnlineComment::where('video_id',$iId)->orderBy('created_at','desc')->get();
    	}
    	foreach($oComments as $k=>$v){
    		$iUid = $v->user_id;
    		$sUnick = User::where('id',$iUid)->pluck('user_nick');
    		$sUthumb = User::where('id',$iUid)->pluck('user_thumb');
    		$oComments[$k]->user_nick = $sUnick;
    		$oComments[$k]->user_thumb = $sUthumb;
    	}
    	return $oComments;
    }
}