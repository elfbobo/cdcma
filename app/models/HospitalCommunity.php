<?php
class HospitalCommunity  extends Eloquent {
	
	protected $table='hospital_community';
	public $timestamps = true;
	protected $guarded = array('id');
	
}