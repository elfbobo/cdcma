<?php
class UserSurveyQuestion  extends Eloquent {


	protected $table='user_survey_question';
	protected $guarded = array('id');
	public $timestamps = true;
    public function scopeOrder($query)
    {
        return $query->orderBy('q_order', 'asc');
    }
    
    
	public static function getAllQuestionAndItem($iPhaseId){
		$oQuestions = self::where('phase_id',$iPhaseId)->orderBy('q_order','ASC')->get();
		$oItems = UserSurveyItem::where('phase_id',$iPhaseId)->orderBy('item','ASC')->get();
		if(!count($oQuestions)){
			return false;
		}
		$aQuestion = array();
		$aItems = array();
		foreach($oItems as $k=>$v){
			$aItems[$v->question_id][$v->item] = $v->item_title;
		}
		foreach($oQuestions as $k=>$v){
			if(!empty($aItems[$v->id])){
				$aQ = array();
				$aQ['id'] = $v->id;
				$aQ['q_title'] = $v->q_title;
				$aQ['q_type'] = $v->q_type;
				$aQ['q_result'] = $v->q_result;
				$aQ['q_order'] = $v->q_order;
				$aQ['items'] = $aItems[$v->id];
				$aQuestion[] = $aQ;
			}
		}
		return $aQuestion;
	}
}