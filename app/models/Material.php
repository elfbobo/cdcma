<?php
class Material extends Eloquent{
	protected $table = 'material';
	public $timestamps = false;
	protected $guarded = array('id');
	
	public function scopeSelMaterial($query,$title){
		return $title?$query->where('title','like','%'.$title.'%'):$query;
	}
	public static function rules(){
		return array(
			'title'=>'required',
			'description'=>'required',
			'temporary_flag'=>'required|in:0,1',
			'type'=>'required|in:image,voice,thumb,video'
		);
	}
	public static function errorMessages(){
		return array(
			'title.required'=>'请添加标题',
			'description.required'=>'请添加简介',
			'temporary_flag.required'=>'请选择素材类型',
			'temporary_flag.in'=>'素材类型格式不正确',
			'type.required'=>'请选择资料类型',
			'type.in'=>'资料类型格式不正确'
		);
	}
}