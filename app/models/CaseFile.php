<?php
class CaseFile extends Eloquent {


	protected $table='case_file';
	protected $guarded = array('id');
	public $timestamps = true;
    //protected $softDelete = true;
    
	public function scopeOrderByCreatedAt($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }
    
}