<?php
class FaceLiveScore extends Eloquent {

	protected $table='face_live_score';
	protected $guarded = array('id');
	public $timestamps = true;
	
	public static function addScoreLog($iVideoId,$iUserId,$iScore1,$iScore2,$iScore3,$iScore4,$iScore5,$iScore6,$iDevice)
	{
		$oScoreLog = FaceLiveScore::where('face_video_id',$iVideoId)->where('user_id',$iUserId)->first();
		if(count($oScoreLog)){
			if($iScore1 != 0){
				$oScoreLog->course_skilled = $iScore1;
			}
			if($iScore2 != 0){
				$oScoreLog->course_logic = $iScore2;
			}
			if($iScore3 != 0){
				$oScoreLog->course_correct = $iScore3;
			}
			if($iScore4 != 0){
				$oScoreLog->course_vivid = $iScore4;
			}
			if($iScore5 != 0){
				$oScoreLog->course_effect = $iScore5;
			}
			if($iScore6 != 0){
				$oScoreLog->course_all = $iScore6;
			}
			$oScoreLog->device_type = $iDevice;
			$oScoreLog->save();
		}else{
			$aScore = array(
					'face_video_id'  => $iVideoId,
					'user_id'        => $iUserId,
					'course_skilled' => isset($iScore1)?$iScore1:0,
					'course_logic'    => isset($iScore2)?$iScore2:0,
					'course_correct'  => isset($iScore3)?$iScore3:0,
					'course_vivid'  => isset($iScore4)?$iScore4:0,
					'course_effect'  => isset($iScore5)?$iScore5:0,
					'course_all'  => isset($iScore6)?$iScore6:0,
					'device_type'    => $iDevice,
					'created_at'     => date('Y-m-d H:i:s',time())
			);
			$oScore = new FaceLiveScore($aScore);
			$oScore->save();
		}
	}

	//取得指定专家下的所有录播视频
	public static function findScoreByiVideoId($iVideoId){
		$oVideos = FaceLiveScore::where('face_video_id',$iVideoId)->get();
		return $oVideos;
	}
}