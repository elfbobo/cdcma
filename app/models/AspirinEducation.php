<?php
class AspirinEducation  extends Eloquent {


	protected $table='aspirin_education';
	protected $guarded = array('id');
	public $timestamps = true;
	protected $softDelete = true;
	
	//审核状态（0：未审核；1：审核通过；2：审核未通过）
	const CHECK_TYPE_DEFAULT = 0;
	const CHECK_TYPE_SUCCESS = 1;
	const CHECK_TYPE_FAIL = 2;
	
	/**
	 * 根据月份判断当前所属季度   dll
	 */
	public static function getQuarter($iMonth)
	{
		switch ($iMonth){
				case 1:case 2:case 3:
					$iQuarter = 1;
					break;
				case 4:case 5:case 6:
					$iQuarter = 2;
					break;
				case 7:case 8:case 9:
					$iQuarter = 3;
					break;
				case 10:case 11:case 12:
					$iQuarter = 4;
					break;
				default:
					break;
		}
		return $iQuarter;
	}

    /**
     * share_hits 访问器
     * @param $value
     * @return string
     */
    public function getShareHitsAttribute($value)
    {
        $share_hits = VideoViewLog::where('platform', 'qrcode')
            ->where('video_id', $this->id)
            ->whereBetween("created_at", array(date("Y-m-d 00:00:00", time()-86400*31), date("Y-m-d 23:59:59")))
            ->count();

        return $share_hits;
    }
}