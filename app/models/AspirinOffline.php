<?php
class AspirinOffline  extends Eloquent {


	protected $table='aspirin_offline';
	protected $guarded = array('id');
	public $timestamps = true;
	
	public function apply(){
		return $this->hasMany('AspirinOfflineApply','offline_id');
	}
}