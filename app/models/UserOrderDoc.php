<?php
class UserOrderDoc  extends Eloquent {


	protected $table='user_order_doc';
	protected $guarded = array('id');
	public $timestamps = true;
//    protected $softDelete = true;
    
    public static function getUserOrderDocTrainRule(){
    	return array(
				'doc_name'			=> 'required',
				'doc_department'	=> 'required',
				'doc_hospital'		=> 'required',
				'doc_thumb'			=> 'required',
    			'start_time'		=> 'required',
				'end_time'			=> 'required',
		);
    }
    
    public function addUserOrderDoc($aDoc){
    	foreach($aDoc as $k=>$v){
			$this->$k = $v;
		}
		
		return $this;
    }
    
}