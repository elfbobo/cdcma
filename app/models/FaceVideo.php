<?php
class FaceVideo  extends Eloquent {


	protected $table='face_video';
	protected $guarded = array('id');
	public $timestamps = true;
   protected $softDelete = true;
    
    public function SurveyQuestion()
    {
        return $this->hasMany('SurveyQuestion');
    }
    
    
	public function scopeOrderByCreatedAt($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }
    public function scopeSelVideo($query){
    	return $query->orderBy('start_time','ASC');
    }
    
	//取得所有视频
	public static function findAllVideo(){
		$oVideos = FaceVideo::orderByCreatedAt();
		return $oVideos;
	}
	
	//取得指定专家下的所有录播视频
	public static function findVideoByDocId($iDocId){
		$oVideos = FaceVideo::orderByCreatedAt()->where('video_type',1)->where('doc_id',$iDocId);
								//->get();
		return $oVideos;
	}
	
	//验证字段
	public static function getVideoRule(){
		return array(
				'video_title'			=> 'required',
				'video_type'			=> 'required',
				'video_thumb'			=> 'required',
				'doc_id'				=> 'min:0',
				'video_introduce'		=> 'required',
				// 'video_url'				=> 'required',
				'start_time'				=> 'required',
				'end_time'				=> 'required'
		);
	}
	
	//添加专家
	public function AddVideo($aDoc){
		foreach($aDoc as $k=>$v){
			$this->$k = $v;
		}
		return $this;
	}
	
	//获取最新预告
	public static function getLatestFaceVideoAdvance($iId=''){
		
		$oFaceVideo = FaceVideo::where('video_type',2)->where('video_rec','=','0')->where('end_time','>',date('Y-m-d H:i:s'));
		// var_dump($oFaceVideo);exit;
		if(!Auth::User()||Auth::User()->role_id!=2){
			$oFaceVideo = $oFaceVideo->where('video_permission','0');
		}
		if($iId){
			$oFaceVideo = $oFaceVideo->where('id','<>',$iId);
		}
		$oFaceVideo = $oFaceVideo->orderBy('start_time','asc')->first();
		if(!$oFaceVideo){
			if(!Auth::User()||Auth::User()->role_id!=2){
				$oFaceVideo = FaceVideo::where('video_permission','0')->orderBy('start_time','DESC')->first();
			}else{
				$oFaceVideo = FaceVideo::orderBy('start_time','DESC')->first();
			}
		}
		if($oFaceVideo){
			$iDocId = $oFaceVideo->doc_id;
			$oDocInfo = FaceDoc::find($iDocId);
			if($oDocInfo){
				$oFaceVideo->doc_info = $oDocInfo;
			}
		}
		return $oFaceVideo;
	}
	
	//API获取最新预告
	public static function getLatestFaceVideoAdvanceAPI($iId='',$iRoleId){
		$oFaceVideo = FaceVideo::where('video_type',2)->where('video_rec','=','0')->where('end_time','>',date('Y-m-d H:i:s'));
		if($iRoleId!=2){
			$oFaceVideo = $oFaceVideo->where('video_permission','0');
		}
		if($iId){
			$oFaceVideo = $oFaceVideo->where('id','<>',$iId);
		}
		$oFaceVideo = $oFaceVideo->orderBy('start_time','asc')->take(3)->get();
		if(!count($oFaceVideo)){
			if($iId){
				$oFaceVideo = array();
			}else{
				if($iRoleId!=2){
					$oFaceVideo = FaceVideo::where('video_permission','0')->where('video_rec','=','0')->orderBy('start_time','DESC')->take(1)->get();
				}else{
					$oFaceVideo = FaceVideo::where('video_rec','=','0')->orderBy('start_time','DESC')->take(1)->get();
				}
			}
		}
		foreach($oFaceVideo as $k=>$v){
			$iDocId = $v->doc_id;
			$oDocInfo = FaceDoc::find($iDocId);
			if($oDocInfo){
				$oFaceVideo[$k]->doc_name = $oDocInfo->doc_name;
				$oFaceVideo[$k]->doc_hospital = $oDocInfo->doc_hospital;
			}
		}
		return $oFaceVideo;
	}
	
	//获取前几条精彩回顾
	public static function getLatestFaceVideoReview($iCount, $vRec="0"){
		if(!Auth::User()||Auth::User()->role_id!=2){
			$oFaceVideo = FaceVideo::select('face_video.*','face_doc.doc_name','face_doc.doc_thumb','face_doc.doc_position','face_doc.doc_department','face_doc.doc_hospital','face_doc.doc_introduction')
								->leftJoin('face_doc','face_video.doc_id','=','face_doc.id');
								if($vRec=="0") $oFaceVideo = $oFaceVideo->where('video_type',1);
								$oFaceVideo = $oFaceVideo->where('video_rec', '=', $vRec)
								->where('video_permission','0')
								->orderBy('start_time','desc')
								->take($iCount)
								->get();
		}else{
			$oFaceVideo = FaceVideo::select('face_video.*','face_doc.doc_name','face_doc.doc_thumb','face_doc.doc_position','face_doc.doc_department','face_doc.doc_hospital','face_doc.doc_introduction')
								->leftJoin('face_doc','face_video.doc_id','=','face_doc.id');
								if($vRec=="0") $oFaceVideo = $oFaceVideo->where('video_type',1);
								$oFaceVideo = $oFaceVideo->where('video_rec', '=', $vRec)
								->orderBy('start_time','desc')
								->take($iCount)
								->get();
		}
//		foreach($oFaceVideo as $k=>$v){
//			$iDocId = $v->doc_id;
//			$oDocInfo = FaceDoc::find($iDocId);
//			if($oDocInfo){
//				$oFaceVideo[$k]->doc_info = $oDocInfo;
//			}
//		}
		return $oFaceVideo;
	}
	
	//处理用户观看视频积分等操作
	/*
	 * iId 为直播或录播id
	 * iMinutes观看时长,,系统自动判断时长
	 * iUid 用户id
	 */
	public static function addUserScoreAndViewLog($iId,$iMinutes=0,$iUid){
		//栏目id
		$iMinutes = intval($iMinutes);
		$iCatId = 1;
		$oFaceVideo = FaceVideo::find($iId);
		if($oFaceVideo->video_permission==1){
			//仅代表可见，不加积分
			return ;
		}
		$oUser = User::find($iUid);
		$iRoleId = $oUser->role_id;
//		$iUid = Auth::User()->id;
		if($oFaceVideo->video_type==2){			//直播
			//查看是否有该用户看该直播的记录
			$oVideoLog = VideoLog::HasLog($iCatId,$iId,$oFaceVideo->video_type,$iUid,0);
			$iEndTime = strtotime($oFaceVideo->end_time);
			$iStartTime = strtotime($oFaceVideo->start_time);
			$iNowTime = time();
//			if($iEndTime>$iNowTime&&$iNowTime>$iStartTime){		//如果直播正在进行中
			if(1==1){
			//如果没有用户查看直播记录则存log,加积分操作
				if(!$oVideoLog){
					$iMinutes = $iMinutes>LIVE_LIMIT ? LIVE_LIMIT : $iMinutes;
					//存log
					$aVideoLog = array(
										'cat_id'		=> $iCatId,
										'user_id'		=> $iUid,
										'video_id'		=> $iId,
										'video_type'	=> $oFaceVideo->video_type,
										'watch_minutes'	=> $iMinutes
					);
					$oVideoLog = new VideoLog($aVideoLog);
					$oVideoLog->save();
					//用户积分+
					if($iRoleId==2){
						//代表
						$iThisMin = $oUser->user_score+$iMinutes;
						$oUser->user_score = $iThisMin;
						$oUser->user_live_score = $oUser->user_live_score+$iMinutes;
						$oUser->save();
					}elseif($iRoleId==3){
						//医生
						$iThisMin = $oUser->user_score+$iMinutes;
						$oUser->user_score = $iThisMin;
						$oUser->user_live_score = $oUser->user_live_score+$iMinutes;
						$oUser->save();
						
						//该医生所有的上级加积分操作（gk）
						$aSuperior = unserialize($oUser->superior_ids);
						if (!empty($aSuperior)){
						       foreach ($aSuperior as $key=>$val){
						            $aVideoLog = array(
													'cat_id'		=> $iCatId,
													'link_user_id'	=> $iUid,
													'user_id'		=> $val,
													'video_id'		=> $iId,
													'video_type'	=> $oFaceVideo->video_type,
													'watch_minutes'	=> $iMinutes
											      );
									$oVideoLog = new VideoLog($aVideoLog);
									$oVideoLog->save(); 
											
									$oUser = User::find($val);
									$oUser->user_score = $oUser->user_score+$iMinutes;
									$oUser->user_live_score = $oUser->user_live_score+$iMinutes;
									$oUser->save();
						       }
						}
						
					}
				}else{
					//有该条记录
					$iWatchMinutes = $oVideoLog->watch_minutes;
					if($iWatchMinutes>=LIVE_LIMIT){
						//记录时长超过规定的最高积分
						return ;
					}
					//最多还能加多少分
					$iCanAddMin = LIVE_LIMIT - $iWatchMinutes;
					if($iMinutes > $iCanAddMin){
						$iMinutes = $iCanAddMin;
					}
					if($iRoleId==2){
						//代表
						$oVideoLog->watch_minutes = $oVideoLog->watch_minutes+$iMinutes;
						$oVideoLog->save();
						
						//user表加积分
						$iThisMin = $oUser->user_score+$iMinutes;
						$oUser->user_score = $iThisMin;
						$oUser->user_live_score = $oUser->user_live_score+$iMinutes;
						$oUser->save();
					}elseif($iRoleId==3){
						//医生
						$oVideoLog->watch_minutes = $oVideoLog->watch_minutes+$iMinutes;
						$oVideoLog->save();
						//user表加积分
						$iThisMin = $oUser->user_score+$iMinutes;
						$oUser->user_score = $iThisMin;
						$oUser->user_live_score = $oUser->user_live_score+$iMinutes;
						$oUser->save();
						
						//该医生所有的上级加积分操作（gk）
                        $aSuperior = unserialize($oUser->superior_ids);
                        if (!empty($aSuperior)){
                        	foreach ($aSuperior as $key=>$val){
                        	    $oVideoLog = VideoLog::HasLog($iCatId,$iId,$oFaceVideo->video_type,$val,$iUid);
								$oVideoLog->watch_minutes = $oVideoLog->watch_minutes+$iMinutes;
								$oVideoLog->save();
								
								$oUser = User::find($val);
								$oUser->user_score = $oUser->user_score+$iMinutes;
								$oUser->user_live_score = $oUser->user_live_score+$iMinutes;
								$oUser->save();
                        	}
                        }
						
					}
				}
			}
		}else{
			//录播
			//限制时长
			$iMinutes = $iMinutes>REVIEW_LIMIT ? REVIEW_LIMIT : $iMinutes;
			$oVideoLog = VideoLog::HasLog($iCatId,$iId,$oFaceVideo->video_type,$iUid,0);
			if(!$oVideoLog){
				//插入该条记录
				$aVideoLog = array(
									'cat_id'		=> $iCatId,
									'user_id'		=> $iUid,
									'video_id'		=> $iId,
									'video_type'	=> $oFaceVideo->video_type,
									'watch_minutes'	=> $iMinutes
				);
				$oVideoLog = new VideoLog($aVideoLog);
				$oVideoLog->save();
				$oUser = User::find($iUid);
				$oUser->user_score = $oUser->user_score+intval($iMinutes);
				$oUser->user_review_score = $oUser->user_review_score+intval($iMinutes);
				$oUser->save();
				$iRoleId = $oUser->role_id;
				if($iRoleId==3){
					//该医生所有的上级加积分操作（gk）
					$aSuperior = unserialize($oUser->superior_ids);
					if (!empty($aSuperior)){
                          foreach ($aSuperior as $key=>$val){
								$aVideoLog = array(
											'cat_id'		=> $iCatId,
											'user_id'		=> $val,
											'link_user_id'	=> $iUid,
											'video_id'		=> $iId,
											'video_type'	=> $oFaceVideo->video_type,
											'watch_minutes'	=> $iMinutes
								);
								$oVideoLog = new VideoLog($aVideoLog);
								$oVideoLog->save();
								
								$oUser = User::find($val);
								$oUser->user_score = $oUser->user_score+intval($iMinutes);
								$oUser->user_review_score = $oUser->user_review_score+intval($iMinutes);
								$oUser->save();
					      }
					}
				}
			}else{
				//存在该条记录，更新
				$iOldMin = $oVideoLog->watch_minutes;
				if($iMinutes>$iOldMin){
					//当当前积分高于数据库记录积分时，执行代码
					$oVideoLog->watch_minutes = $iMinutes;
					$oVideoLog->save();
					
					//用户积分
					$iReduceUserScore = intval($iOldMin);
					$iAddUserScore = intval($iMinutes);
					$oUser = User::find($iUid);
					$iThisMin = $oUser->user_score+$iAddUserScore-$iReduceUserScore;
					$oUser->user_score = $iThisMin;
					$oUser->user_review_score =  $oUser->user_review_score+$iAddUserScore-$iReduceUserScore;
					$oUser->save();
					$iRoleId = $oUser->role_id;
					if($iRoleId==3){
						 //该医生所有的上级加积分操作（gk）
                         $aSuperior = unserialize($oUser->superior_ids);
                         if (!empty($aSuperior)){
                         	foreach ($aSuperior as $key=>$val){
	                         	if($val){
										$oVideoLog = VideoLog::HasLog($iCatId,$iId,$oFaceVideo->video_type,$val,$iUid);
										if(!$oVideoLog){
											$aVideoLog = array(
												'cat_id'		=> $iCatId,
												'user_id'		=> $val,
												'link_user_id'	=> $iUid,
												'video_id'		=> $iId,
												'video_type'	=> $oFaceVideo->video_type,
												'watch_minutes'	=> $iMinutes
											);
											$oVideoLog = new VideoLog($aVideoLog);
											$oVideoLog->save();
										}
										$oVideoLog->watch_minutes = $iMinutes;
										$oVideoLog->save();
										
										$oUser = User::find($val);
										$oUser->user_score = $oUser->user_score+$iAddUserScore-$iReduceUserScore;
										$oUser->user_review_score = $oUser->user_review_score+$iAddUserScore-$iReduceUserScore;
										$oUser->save();
								 }
                         	}
                         }
						
					}
					
				}
			}
		}
	}
	//签到加积分（直播或录播）
	public static function addUserScore($iId,$iMinutes=0,$iUid,$iVideoType){
		//栏目id
		$iMinutes = intval($iMinutes);
		$iCatId = 1;
		$oFaceVideo = FaceVideo::find($iId);
		if($oFaceVideo->video_permission==1){
			//仅代表可见，不加积分
			return ;
		}
		$oUser = User::find($iUid);
		$iRoleId = $oUser->role_id;
		$iMinutes = $iMinutes>LIVE_LIMIT ? LIVE_LIMIT : $iMinutes;
		if($iVideoType==2){
			//加直播积分
			$sScoreFlag = 'user_live_score';
		}else{
			$sScoreFlag = 'user_review_score';
		}
		$oVideoLog = VideoLog::HasLog($iCatId,$iId,$iVideoType,$iUid,0);
		if(!$oVideoLog){
			//存log
			$aVideoLog = array(
								'cat_id'		=> $iCatId,
								'user_id'		=> $iUid,
								'video_id'		=> $iId,
								'video_type'	=> $iVideoType,
								'watch_minutes'	=> $iMinutes
			);
			$oVideoLog = new VideoLog($aVideoLog);
			$oVideoLog->save();
			//用户积分+
			if($iRoleId==2){
				//代表
				$iThisMin = $oUser->user_score+$iMinutes;
				$oUser->user_score = $iThisMin;
				$oUser->$sScoreFlag = $oUser->$sScoreFlag+$iMinutes;
				$oUser->save();
			}elseif($iRoleId==3){
				//医生
				$iThisMin = $oUser->user_score+$iMinutes;
				$oUser->user_score = $iThisMin;
				$oUser->$sScoreFlag = $oUser->$sScoreFlag+$iMinutes;
				$oUser->save();
				
				//该医生所有的上级加积分操作（gk）
				$aSuperior = unserialize($oUser->superior_ids);
				if (!empty($aSuperior)){
				       foreach ($aSuperior as $key=>$val){
				            $aVideoLog = array(
											'cat_id'		=> $iCatId,
											'link_user_id'	=> $iUid,
											'user_id'		=> $val,
											'video_id'		=> $iId,
											'video_type'	=> $iVideoType,
											'watch_minutes'	=> $iMinutes
									      );
							$oVideoLog = new VideoLog($aVideoLog);
							$oVideoLog->save(); 
									
							$oUser = User::find($val);
							$oUser->user_score = $oUser->user_score+$iMinutes;
							$oUser->$sScoreFlag = $oUser->$sScoreFlag+$iMinutes;
							$oUser->save();
				       }
				}
				
			}
		}else{
			//存在该条记录
			$oVideoLog->watch_minutes = $iMinutes;
			$oVideoLog->save();
			//用户积分+
			if($iRoleId==2){
				//代表
				$iThisMin = $oUser->user_score+$iMinutes;
				$oUser->user_score = $iThisMin;
				$oUser->$sScoreFlag = $oUser->$sScoreFlag+$iMinutes;
				$oUser->save();
			}elseif($iRoleId==3){
				//医生
				$iThisMin = $oUser->user_score+$iMinutes;
				$oUser->user_score = $iThisMin;
				$oUser->$sScoreFlag = $oUser->$sScoreFlag+$iMinutes;
				$oUser->save();
				
				//该医生所有的上级加积分操作（gk）
				$aSuperior = unserialize($oUser->superior_ids);
				if (!empty($aSuperior)){
				       foreach ($aSuperior as $key=>$val){
				       		$oVideoLog = VideoLog::HasLog($iCatId,$iId,$iVideoType,$val,0);
				       		if($oVideoLog){
					            $oVideoLog->watch_minutes = $iMinutes;
								$oVideoLog->save(); 
										
								$oUser = User::find($val);
								$oUser->user_score = $oUser->user_score+$iMinutes;
								$oUser->$sScoreFlag = $oUser->$sScoreFlag+$iMinutes;
								$oUser->save();
				       		}
				       }
				}
				
			}
		}
					
	}
	public static function getFaceVideoReviewById($iId){
		$oFaceVideo = FaceVideo::find($iId);
		if($oFaceVideo){
			$iDocId = $oFaceVideo->doc_id;
			$oDocInfo = FaceDoc::find($iDocId);
			if($oDocInfo){
				$oFaceVideo->doc_info = $oDocInfo;
			}
		}
		return $oFaceVideo;
	}
}