<?php
class  MeetingMessageLog extends Eloquent {
	protected $table = 'meeting_message_log';
	public $timestamps = true;
	protected $guarded = array('id');
	
	public static function sendMessage($sMobile,$iModelId,$sValue1,$sValue2){
			$arr = array(
					"calleeNbr" => $sMobile,//短信接收号码
					"templetid" => $iModelId,//模板id
					"value1" => $sValue1, 
					"value2" => $sValue2       
							
			);
			$oCallClient = new CallClient();
			$message = $oCallClient->smsbytemplet($arr);
				
			$oMessage = json_decode($message);
			$sSmsMessage = $oMessage->msg; 
			 
//          短信发送记录存数据库(手机号、短信模板id、短信内容、时间)
			$sMessageInfo = MeetingMessageLog::getMessageInfo($iModelId,$sValue1,$sValue2);
			$aLog = array(
						'mobile'   => $sMobile,
						'message'  => $sMessageInfo,
						'model_id' => $iModelId,
						'status'   => $oMessage->msg,
						'created_at'   => date('Y-m-d H:i:s',time())
			);
			$oMessageLog = new MeetingMessageLog($aLog);
			$oMessageLog->save();
	}
	
	public static function getMessageInfo($iModelId,$sValue1,$sValue2){
		switch ($iModelId){
			case '102290':
				$sMessageInfo = '您好，您预约的'.$sValue1.'会议已被确认，请及时查看会议详情确认会议信息。';
				return $sMessageInfo;
		    	break;
		    case '102291':
				$sMessageInfo = '您好，您创建的'.$sValue1.'会议已被'.$sValue2.'人预约，请及时查看并确认会议，会议开始一周前如未确认预约人,会议将被自动取消。';
				return $sMessageInfo;
		    	break;
		    case '102292':
				$sMessageInfo = '您好，您'.$sValue1.'会议已取消，如有疑问请尽快联系'.$sValue2;
				return $sMessageInfo;
		    	break;
		    case '102293':
				$sMessageInfo = '您好，很遗憾，您预约的'.$sValue1.'会议预约失败，可选择其它可预约会议，谢谢。';
				return $sMessageInfo;
		    	break;
		    case '102294':
				$sMessageInfo = '您好，您创建的'.$sValue1.'会议未确认会议预约人，会议已被取消。';
				return $sMessageInfo;
		    	break;
		    case '102295':
				$sMessageInfo = '您好，您预约的'.$sValue1.'会议已被取消，请预约其它会议，谢谢。';
				return $sMessageInfo;
		    	break;
	    }
	}
	
}