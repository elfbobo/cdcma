<?php
class AspirinSupportLog  extends Eloquent {


	protected $table='aspirin_support_log';
	protected $guarded = array('id');
	public $timestamps = true;
	
	/**
	 * 判断用户是否已经点赞
	 */
	public static function hasSupport($iCatId,$iUId,$iSourceId){
		if($iUId>0){
			//app端视频点赞
			$oSupportLog = AspirinSupportLog::where('cat_id',$iCatId)
								->where('user_id',$iUId)
								->where('source_id',$iSourceId)
								->first();
			if($oSupportLog){
				return 1;
			}else{
				return 0;
			}
		}else{
			//分享的移动端点赞
			$ip = GetIP();
			$sUserAgent = isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'';
			$oSupportLog = AspirinSupportLog::where('cat_id',$iCatId)
						->where('user_id',$iUId)
						->where('source_id',$iSourceId)
						->where('user_agent',$sUserAgent)
						->where('ip',$ip)
						->first();
			if($oSupportLog){
				return 1;
			}else{
				return 0;
			}
		}
	}
}