<?php
class UserTrain  extends Eloquent {


	protected $table='user_train';
	protected $guarded = array('id');
	public $timestamps = true;
//    protected $softDelete = true;
    
    
	public function scopeOrderByCreatedAt($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }
    
	//我的培训验证
	public static function getUserTrainRule(){
		return array(
				'train_title'		=> 'required',
				'train_url'			=> 'required',
				'start_time'		=> 'required',
				'end_time'			=> 'required',
		);
	}
	
	//添加培训
	public function AddTrain($aTrain){
		foreach($aTrain as $k=>$v){
			$this->$k = $v;
		}
		
		return $this;
	}
	
	//获得我的培训
	public static function getAllTrain(){
		$datetime = date('Y-m-d H:i:s');
		$oUserTrain1 = UserTrain::where('end_time','>=',$datetime)->orderBy('start_time','ASC')->get();
		$oUserTrain2 = UserTrain::where('end_time','<',$datetime)->orderBy('start_time','DESC')->get();
		$oTrain = null;
		foreach($oUserTrain1 as $k=>$v){
			$oTrain[] = $v;
		}
		foreach($oUserTrain2 as $k=>$v){
			$oTrain[] = $v;
		}
		return $oTrain;
	}
}