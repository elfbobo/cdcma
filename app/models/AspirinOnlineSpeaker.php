<?php
class AspirinOnlineSpeaker  extends Eloquent {


	protected $table='aspirin_online_speaker';
	protected $guarded = array('id');
	public $timestamps = true;
	
	/**
	 * 获取精彩回顾列表
	 */
	public static function getVideoList($iCatId,$sTitle,$sName,$iPage,$iPagesize){
		
		if(!empty($sTitle)||!empty($sName)){
			$oVideoList = DB::table('aspirin_online_speaker')
				->join('user','user.id','=','aspirin_online_speaker.speaker_id')
				->join('aspirin_online','aspirin_online.id','=','aspirin_online_speaker.online_id')
				->Join('aspirin_online_time','aspirin_online_time.id','=','aspirin_online_speaker.online_time_id')
				->select('aspirin_online_speaker.*','user.user_name','aspirin_online.online_title','aspirin_online_time.online_date')
				->where('open_flag',1)
				->where('cancel_flag',0)
				->where('video_url','!=','')
				->where('online_title','like','%'.$sTitle.'%')
				->where('user_name','like','%'.$sName.'%');
			if(in_array($iCatId, [1,2,3,4])){
				$oVideoList = $oVideoList->where('online_catid',$iCatId);
			}
			$oVideoList = $oVideoList->orderBy('online_date','desc')->get();
			
		}else{
			$oVideoList = self::leftJoin('aspirin_online_time','aspirin_online_time.id','=','aspirin_online_speaker.online_time_id')
									->select('aspirin_online_time.online_date','aspirin_online_speaker.*')
									->where('open_flag',1)
									->where('cancel_flag',0)
									->where('video_url','!=','');
			if(in_array($iCatId, [1,2,3,4])){
				$oVideoList = $oVideoList->where('online_catid',$iCatId);
			}
			$oVideoList = $oVideoList->orderBy('online_date','desc');
			if($iPage != 0 && $iPagesize != 0){
				$oVideoList = $oVideoList->skip(($iPage-1)*$iPagesize)->take($iPagesize)->get();
			}else{
				$oVideoList = $oVideoList->get();
			}
		}
		
		$aVideoList = array();
		if(!count($oVideoList)){
			return $aVideoList;
		}
	
		foreach($oVideoList as $oVideo){
// 			$oOnline = AspirinOnline::find($oVideo->online_id);
			$oPpt = AspirinOnlinePpt::find($oVideo->ppt_id);
			$aVideo = array();
			$aVideo['video_id']     = $oVideo->id;
			$aVideo['online_title'] = $oPpt?$oPpt->ppt_title:'';
			$aVideo['online_thumb'] = $oPpt?Config::get('app.url').$oPpt->ppt_thumb:'';
			$aVideo['online_date']  = substr($oVideo->online_date,0,10);
			$aVideo['video_hits']   = $oVideo->video_hits;
			$aVideo['doc_name']     = '';
			$aVideo['doc_hospital'] = '';
	
			$oUser = User::select('user_name','user_company','user_company_name')->where('id',$oVideo->speaker_id)->first();
			if($oUser){
				$aVideo['doc_name'] = $oUser->user_name;
	
				$sUserCompany = $oUser->user_company;
				if(is_numeric($sUserCompany)&&$sUserCompany!=0){
					$sUserCompany = Hospital::where('id',$sUserCompany)->pluck('name');
				}
				if(!$sUserCompany){
					$sUserCompany = $oUser->user_company_name;
				}
	
				$aVideo['doc_hospital'] = $sUserCompany;
			}
	
			$aVideoList[] = $aVideo;
		}
		return $aVideoList;
	}
	
	/**
	 * 会议取消
	 */
	public static function getCancel($iSpeakerInfoId,$iOnlineTimeId,$iUid){
		
		$oOnlineTime = AspirinOnlineTime::find($iOnlineTimeId);
		if($oOnlineTime && $oOnlineTime->speaker_count != 0){
			$oOnlineTime->speaker_count = $oOnlineTime->speaker_count - 1;
			$oOnlineTime->save();
		}
// 		//会议取消时 会畅接口相关记录清除
// 		$oMeetingTimeLog = MeetingTime::where('online_id',$iSpeakerInfoId)->get();
// 		if(count($oMeetingTimeLog)){
// 			foreach($oMeetingTimeLog as $k=>$v){
// 				$v->delete();
// 			}
// 		}
// 		$oMeetingApiLog = MeetingApiLog::where('online_id',$iSpeakerInfoId)->first();
// 		if(count($oMeetingApiLog)){
// 			$oMeetingApiLog->delete();
// 		}
		
		//讲者取消会议时 给听者推送通知
		$oListenerInfo = AspirinOnlineListener::where('online_time_id',$iOnlineTimeId)->where('speaker_id',$iUid)->get();
		$sName  = User::where('id',$iUid)->pluck('user_name');
		$oOnlineTime = AspirinOnlineTime::find($iOnlineTimeId);
		$oOnline = AspirinOnline::find($oOnlineTime->online_id);
		$sOnlineTitle = $oOnline->online_title;
		if(count($oListenerInfo)){
			foreach($oListenerInfo as $k=>$v){
				//发送消息通知
				$sPushContent = PushService::ONLINE_SPEAKER_CANCEL;
				$sPushContent = str_replace('{{username}}', $sName, $sPushContent);
				$sPushContent = str_replace('{{title}}', $sOnlineTitle, $sPushContent);
				PushService::postClient('国卫健康云',$sPushContent,14,$v['listener_id'],$oOnline->catid);
		
				$v->delete();
			}
		}
		return  true;
	}
}