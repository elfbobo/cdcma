<?php
class Version  extends Eloquent {


	protected $table='version';
	protected $guarded = array('id');
	public $timestamps = true;
    
	//取得所有版本
	public static function getNewVersion($type = 'ios'){
		$oVersion = Version::where('types',$type)->where('isopen',1)->orderBy('updated_at','DESC')->first();
		return $oVersion;
	}
	
	//验证字段
	public static function getRule(){
		return array(
				'number'	=> 'required',
				'types'		=> 'required',
				'url'		=> 'required',
				'content'	=> 'required',
		);
	}
	
	//添加版本
	public function AddVersion($aDoc){
		foreach($aDoc as $k=>$v){
			$this->$k = $v;
		}
		return $this;
	}
}