<?php
/**
 * 调用呼叫中心接口类
 * @author liyandong 2014/11/28
 */
class CallClient
{
	//kingyee呼叫中心域名
 	const DEFAULT_SERVER = "http://call.local:1111";// 内网环境
	//注册的domain，需要向呼叫中心管理员申请
	const USER_DOMAIN = "cdma.medlive.cn";
	//点击呼叫接口
	const CLICK2CALL_API = "/apifj/click2call";
	//终止点击拨号
	const CALLSTOP_API = "/apifj/callstop";
	//发送模板短信接口
	const SMSBYTEMPLET_API = "/apifj/smsbytemplet";
	//发送验证码短信接口
	const SMSCAPTCHA_API = "/apifj/smscaptcha";
	
	
	function __construct()
	{
		
	}
	
	private function curl_post($url, $post_data)
	{
		$headers = array("Content-Type: application/json;charset=UTF-8");
		$shared_curl_opt_arr = array(
			CURLOPT_POST => 1,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_SSL_VERIFYHOST => 0,
			CURLOPT_ENCODING => "",
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_TIMEOUT => 100,
			CURLOPT_CONNECTTIMEOUT => 60
		);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt_array($ch, $shared_curl_opt_arr);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		
		$response = curl_exec($ch);
		$err_no = curl_errno($ch);
		$err_msg = curl_error($ch);
		curl_close($ch);
		//returns when an error occurs
		if ($err_no) {
			return 'Error: '.$err_msg;
		}
		
		//http trasaction completed without error but response is not available
		if(!$response)
		{
			return 'Error: Failed to get response';
		}
		//return the response json string
		return $response;
	}
	/**
	 * 设置post数据，数据加密等
	 * @param $post_array
	 * json_encode
	 */
	private function set_post_date($post_array){
		$post_array['domain'] = CallClient::USER_DOMAIN;
		return json_encode($post_array);
	}
	
	
	/**
	 * 终止点击拨号
	 * 数组中参数如下
	 * @sessionid    API调用时返回的sessionid值，32位16进制字符串
	 */
	function callstop($post_array)
	{
		$url = CallClient::DEFAULT_SERVER.CallClient::CALLSTOP_API;
		$post_date = $this->set_post_date($post_array);
		return $this->curl_post($url, $post_date);
	}
	
	/**
	 * 点击拨号
	 * 数组中参数如下
	 * @displayNbr（可填，默认为主叫号码） 被叫终端上显示的号码。 填写MSISDN的值包含国家码如： +8675512345678。
	 * @callerNbr（必填）主叫号码。填写MSISDN的值包含国家码如： +8675512345678。
	 * @calleeNbr（必填）被叫号码。填写MSISDN的值包含国家码如： +8675512345678。
	 */
	function click2call($post_array)
	{
		$url = CallClient::DEFAULT_SERVER.CallClient::CLICK2CALL_API;
		$post_date = $this->set_post_date($post_array);
		return $this->curl_post($url, $post_date);
	}
	/**
	 * 发送模板短信
	 * 数组中参数如下
	 * "calleeNbr" => 18618366093,//短信接收号码
		"templetid" => '100170',//模板id
		"value1" => '医脉通',
		"value2" => '123456',
		"value3" => '',
		"value4" => '[测试]',
		"value5" => '',
		"value6" => ''
	 */
	function smsbytemplet($post_array)
	{
		$url = CallClient::DEFAULT_SERVER.CallClient::SMSBYTEMPLET_API;
		$post_date = $this->set_post_date($post_array);
		return $this->curl_post($url, $post_date);
	}
	/**
	 * 发送验证码短信
	 * 数组中参数如下
	 * "calleeNbr" => 18618366093,//短信接收号码
		"templetid" => '100170',//模板id
		"value1" => '医脉通',
		"value2" => '123456',
		"value3" => '',
		"value4" => '[测试]',
		"value5" => '',
		"value6" => ''
	 */
	function smscaptcha($post_array)
	{
		$url = CallClient::DEFAULT_SERVER.CallClient::SMSCAPTCHA_API;
		$post_date = $this->set_post_date($post_array);
		return $this->curl_post($url, $post_date);
	}
	
}
?>