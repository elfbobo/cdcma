<?php
use Illuminate\Console\Command;
class Comment  extends Eloquent {


	protected $table='comment';
	protected $guarded = array('id');
	public $timestamps = true;
//    protected $softDelete = true;
    
    public static function getComment($iCatId,$iId){
    	$oComments = Comment::where('cat_id',$iCatId)
    							->where('source_id',$iId)
    							->get();
    	foreach($oComments as $k=>$v){
    		$iUid = $v->user_id;
    		$sUnick = User::where('id',$iUid)->pluck('user_nick');
    		$sUthumb = User::where('id',$iUid)->pluck('user_thumb');
    		$oComments[$k]->user_nick = $sUnick;
    		$oComments[$k]->user_thumb = $sUthumb;
    	}
    	return $oComments;
    }
}