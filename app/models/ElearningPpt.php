<?php
class ElearningPpt  extends Eloquent {


	protected $table='e_learning_ppt';
	protected $guarded = array('id');
	public $timestamps = true;
	public static function getPptList($iPageSize){
		$oPptList = self::orderBy('created_at','desc')->paginate($iPageSize);;
		return $oPptList;
	}
}