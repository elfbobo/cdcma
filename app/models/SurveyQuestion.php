<?php
class SurveyQuestion  extends Eloquent {


	protected $table='survey_question';
	protected $guarded = array('id');
	public $timestamps = true;
//    protected $softDelete = true;
    
	public function SurveyQuestionAnswer()
    {
        return $this->hasMany('SurveyQuestionAnswer');
    }
    
	public function SurveyQuestionOption()
    {
        return $this->hasMany('SurveyQuestionOption');
    }
    
	public static function getFieldSurvey(){
    	return array(
			'title'=>'标题',
			'type'=>'类型',
			'result'=>'正确答案',
			'listorder'=>'排序位',
		);
    } 
}