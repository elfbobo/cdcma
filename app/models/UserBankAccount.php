<?php
class UserBankAccount extends Eloquent {

	protected $table='user_bank_account';
	public $timestamps = false;
    
	public function AddDoc($aDoc){
		foreach($aDoc as $k=>$v){
			$this->$k = $v;
		}
		return $this;
	}
	
}