<?php
class AspirinResearchApply  extends Eloquent {


	protected $table='aspirin_research_apply';
	protected $guarded = array('id');
	public $timestamps = true;
	
	//审核状态（0：未审核；1：审核通过；2：审核未通过）
	const CHECK_TYPE_DEFAULT = 0;
	const CHECK_TYPE_SUCCESS = 1;
	const CHECK_TYPE_FAIL = 2;
	
	//获取科研简介页面
	public static function getBriefInfo()
	{
		$sContent = '';
		$sContent .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
		$sContent .= '<html xmlns="http://www.w3.org/1999/xhtml">';
		$sContent .= '<head>';
		$sContent .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
		$sContent .= '<title>科研培训简介</title>';
		$sContent .= '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">';
		$sContent .= '<meta name="apple-mobile-web-app-capable" content="yes">';
		$sContent .= '<meta name="apple-mobile-web-app-status-bar-style" content="black">';
		$sContent .= '<meta name="format-detection" content="telephone=no">';
		$sContent .= '<link rel="stylesheet" type="text/css" href="'.Config::get('app.url').'/assets/css/aspirin.css?v=1" />';
		$sContent .= '<script src="http://libs.baidu.com/jquery/1.7.2/jquery.js"></script>';
		$sContent .= '</head>';
		$sContent .= '<body>';
		$sContent .= '<div class="page_cont_box">';
		$sContent .= '<div><img class="wid100" src="'.Config::get('app.url').'/assets/images/aspirin/img03.jpg" /></div>';
		$sContent .= '<div class=" infor_box">';
		$sContent .= '<div class=" infor_tit">背景介绍</div>';
		$sContent .= '<div class=" infor_cont">';
		$sContent .= '<p class="indent">';
		$sContent .= '随着我国人口老龄化的加剧以及心血管疾病危险因素的流行，心脑血管疾病的患病率及死亡率不断上升，及早采取积极的干预措施并加强心血管疾病的防治工作刻不容缓。';
		$sContent .= '</p>';
		$sContent .= '<p class="indent">';
		$sContent .= ' 为增加医生对心血管疾病预防理念的理解及重视，增强医生对阿司匹林在心血管疾病预防中的认识及规范使用；加强我国心血管医生与国际上的合作与沟通，提高临床医生在心血管疾病预防领域的科研工作能力，心馨心血管健康（苏州工业园区）基金会特开展阿司匹林专项基金科研培训项目。';
		$sContent .= '</p>';
		$sContent .= '</div>';
		$sContent .= '</div>';
		$sContent .= '<div class=" infor_box">';
		$sContent .= '<div class=" infor_tit clear"style="background-color:#75b437;">';
		$sContent .= '培训介绍';
		$sContent .= '</div>';
		$sContent .= '<div class=" infor_cont">';
		$sContent .= '<p style="margin-bottom: 20px">';
		$sContent .= '<span class="bold">活动发起：</span>心馨心血管健康（苏州工业园区）基金会2017年4月启动该项目，向心内科医生发起科研培训项目介绍以及报名通知，在心血管预防科研领域有经验及意向者可以下载申请表填写并发到基金会的专用邮箱: aspirin@ccahouse.org ，申请表收集时限为2017年4月至7月底。';
		$sContent .= '</p>';
		$sContent .= '<p style="margin-bottom: 20px">';
		$sContent .= '<span class="bold">申请人筛选及科研课程培训：</span>心馨心血管健康（苏州工业园区）基金会收集整理所有参与者的项目申请书，并在8月份收集整理所有申请者资料，将针对不同申请群体提供：1）浅显易懂的网络培训课程；2）心血管领域，针对科研项目实施中的问题进行专业培训；3）针对申请人在心血管领域科研项目实施过程中的共性问题，进行深入探讨及培训。';
		$sContent .= '</p>';
		$sContent .= '<p style="margin-bottom: 20px">';
		$sContent .= '<span class="bold">科研培训内容：</span>心馨心血管健康（苏州工业园区）基金会预计在8-9月召开面对面培训会议。上午集中授课：由专业科研机构Medsci公司结合心血管预防领域的科研工作进行授课；下午实战演练及专家面对面： 20-30名学员分成2组，由Medsci培训师以及2名心血管领域科研经验丰富的专家指导。';
		$sContent .= '</p>';
		$sContent .= '<p style="margin-bottom: 20px">';
		$sContent .= '<span class="bold">电话培训具体内容：</span>专业医学培训公司针对30位优秀申请人进行一对一电话培训，每人3次课程，具体包括临床研究类型及研究思维设计，国自然标书撰写，临床研究数据库的建立与管理等，培训过程可以阿司匹林既往已经发表的文献为例。';
		$sContent .= '</p>';
		$sContent .= '<div class="qr_code_box"><img onclick="window.location.href='.'http://e.dxy.cn/ccahouse/'.'" src="'.Config::get('app.url').'/assets/images/aspirin/qr_code.png" alt=""><p>了解更多专项目基金详情内容请点击二维码查看</p></div>';
		$sContent .= '</div>';
		$sContent .= '</div>';
		$sContent .= '</div>';
		$sContent .= '</body>';
		$sContent .= '</html>';
		return $sContent;
	}
}