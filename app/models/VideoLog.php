<?php
class VideoLog  extends Eloquent {


	protected $table='video_log';
	protected $guarded = array('id');
	public $timestamps = true;
    // protected $softDelete = true;
    
	public function scopeOrderByCreatedAt($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }
    
    //查看是否有该条记录
    public static function HasLog($iCatId,$iVideoId,$iType,$iUserId,$iLinkUId){
    	$oVideoLog = VideoLog::where('cat_id',$iCatId)
			->where('video_id',$iVideoId)
			->where('video_type',$iType)
			->where('user_id',$iUserId)
			->where('link_user_id',$iLinkUId)
			->first();
    	if($oVideoLog){
    		return $oVideoLog;
    	}else{
    		return false;
    	}
    }
    
	public static function HasLogSign($iCatId,$iVideoId,$iType,$iUserId,$iLinkUId){
    	$oVideoLog = VideoLog::where('cat_id',$iCatId)
			->where('video_id',$iVideoId)
			->where('video_type',$iType)
			->where('user_id',$iUserId)
			->where('link_user_id',$iLinkUId)
			->where('watch_minutes','>',0)
			->first(); 
    	if($oVideoLog){
    		return $oVideoLog;
    	}else{
    		return false;
    	}
    }
    
    public static function getUserMinByType($iUid,$iType=1){
    	$iMin = VideoLog::where('user_id',$iUid)
			->where('video_type',$iType)
			->where('link_user_id',0)
			->sum('watch_minutes');
    	if(!$iMin){
    		$iMin = 0;
    	}
    	return intval($iMin);
    }
    
    //获得列表页字段
    public static function getUserVideoLog($iUid,$pagesize=0){
    	$oVideoLog =  VideoLog::select('video_log.*','face_video.video_title')
			->leftJoin('face_video','video_log.video_id','=','face_video.id')
			->where('user_id',$iUid)
			->where('link_user_id',0)
			->where('watch_minutes','>',0)
			->orderBy('created_at','DESC');
        if($pagesize){
            $oVideoLog = $oVideoLog->paginate($pagesize);
        }else{
            $oVideoLog = $oVideoLog->get();
    	}
        foreach($oVideoLog as $k=>$v){
            // $iCatId = $v->cat_id;
    		$iVideoId = $v->video_id;
            // if($iCatId==1){
                // $sTitle = FaceVideo::where('id',$iVideoId)->pluck('video_title');
    			if($v->video_type==1){
    				$sUrl = '/docface/review-show/'.$iVideoId;
    			}else{
    				$sUrl = '#';
    			}
       		/*}else{
       			dd('erqi');
       		}
       		$oVideoLog[$k]->video_title = $sTitle;*/
    		$oVideoLog[$k]->video_url = $sUrl;
    	}
    	return $oVideoLog;
    }
    
}