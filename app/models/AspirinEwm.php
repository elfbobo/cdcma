<?php
class AspirinEwm  extends Eloquent {


	protected $table='aspirin_ewm';
	protected $guarded = array('id');
	public $timestamps = true;
	
	/**
	 * 获取二维码
	 * @param unknown $sStr
	 */
	public static function getErweima($sUrl){
		$oEwm = self::where('url',$sUrl)->first();
		if($oEwm && is_file(public_path() . $oEwm->ewm_url)){
			return $oEwm->ewm_url;
		}
		$logo = '/assets/images/mobile/logo.png';
		$sEwmUrl = EwmService::createErweima($sUrl,$logo);
		
		//保存二维码图片
        if ($oEwm) {
            $oEwm->ewm_url = $sEwmUrl;
        } else {
            $aEwm = array(
                'url' => $sUrl,
                'ewm_url' => $sEwmUrl
            );
            $oEwm = new self($aEwm);
        }
		$oEwm->save();

        return $sEwmUrl;
	}
}