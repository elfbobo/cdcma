<?php
class AspirinOnlineListener  extends Eloquent {


	protected $table='aspirin_online_listener';
	protected $guarded = array('id');
	public $timestamps = true;
	protected $softDelete = true;
	
	/**
	 * 听者报名成功信息
	 */
	public static function getJoinInfo($iOnlineTimeId,$iSpeakerId){
		// ECHO $iOnlineTimeId;DIE;
		$oOnlineTime = AspirinOnlineTime::find($iOnlineTimeId);
		//$oOnline = AspirinOnline::find($oOnlineTime->online_id);
		$oSpeakerInfo = AspirinOnlineSpeaker::where('online_time_id',$iOnlineTimeId)
			->where('speaker_id',$iSpeakerId)->first();
		if(!$oSpeakerInfo) return array();
		@$oPpt = AspirinOnlinePpt::find($oSpeakerInfo->ppt_id);
		//会议时间
		$iBtnFlag = 0; 
		$sNowTime = date('Y-m-d H:i',time());
		@$sEndTime = substr($oOnlineTime->online_date,0,10).' '.substr($oOnlineTime->time_period,6,5);
		if($sNowTime > $sEndTime){
			$iBtnFlag = 1;         //未参加会议已结束 
		}
		$aInfo = array();
		if($iBtnFlag != 1){
			$aInfo = array(
					'speaker_id'    => $iSpeakerId,
					'online_time_id'=> $iOnlineTimeId,
					'online_title'  => @$oPpt->ppt_title,
					'online_date'   => date('Y年m月d日',strtotime($oOnlineTime->online_date)),
					'time_period'   => $oOnlineTime->time_period,
					'btn_flag'      => $iBtnFlag
			);
			$oUser = User::select('user_name','user_company','user_company_name')->where('id',$iSpeakerId)->first();
			if($oUser){
				$aInfo['doc_name'] = $oUser->user_name;
			
				$sUserCompany = $oUser->user_company;
				if(is_numeric($sUserCompany)&&$sUserCompany!=0){
					$sUserCompany = Hospital::where('id',$sUserCompany)->pluck('name');
				}
				if(!$sUserCompany){
					$sUserCompany = $oUser->user_company_name;
				}
			
				$aInfo['doc_hospital'] = $sUserCompany;
			}
		}
		
		return $aInfo;
	}
	/**
	 * 听者被邀请信息
	 * iOnlineTimeId 某时间段会议id
	 * iSpeakerId  讲者id
	 * iBtnFlag 同意和忽略按钮状态（0：可点；1：已忽略，不可点）
	 */
	public static function getInviteInfo($iOnlineTimeId,$iSpeakerId,$iBtnFlag){
	
		$oOnlineTime = AspirinOnlineTime::find($iOnlineTimeId);
// 		$oOnline = AspirinOnline::find($oOnlineTime->online_id);
		$oSpeakerInfo = AspirinOnlineSpeaker::where('online_time_id',$iOnlineTimeId)
											->where('speaker_id',$iSpeakerId)->first();
		$oPpt = AspirinOnlinePpt::find($oSpeakerInfo->ppt_id);
		
		$sNowTime = date('Y-m-d H:i',time());
		$sEndTime = substr($oOnlineTime->online_date,0,10).' '.substr($oOnlineTime->time_period,6,5);
		if($sNowTime > $sEndTime){
			$iBtnFlag = 1;         //未同意 会议已结束
		}
		$aInfo = array();
		if($iBtnFlag != 1){
			$aInfo = array(
					'speaker_id'    => $iSpeakerId,
					'online_time_id'=> $iOnlineTimeId,
					'online_title'  => isset($oPpt)?$oPpt->ppt_title:'',
					'online_date'   => date('Y年m月d日',strtotime($oOnlineTime->online_date)),
					'time_period'   => $oOnlineTime->time_period,
					'agree_flag'    => $iBtnFlag,
					'ignore_flag'   => $iBtnFlag,
	 		);
			$oUser = User::select('user_name','user_company','user_company_name')->where('id',$iSpeakerId)->first();
			if($oUser){
				$aInfo['doc_name'] = $oUser->user_name;
		
				$sUserCompany = $oUser->user_company;
				if(is_numeric($sUserCompany)&&$sUserCompany!=0){
					$sUserCompany = Hospital::where('id',$sUserCompany)->pluck('name');
				}
				if(!$sUserCompany){
					$sUserCompany = $oUser->user_company_name;
				}
		
				$aInfo['doc_hospital'] = $sUserCompany;
			}
		}
		return $aInfo;
	}
}