<?php
class UserOrder  extends Eloquent {


	protected $table='user_order';
	protected $guarded = array('id');
	public $timestamps = true;
//    protected $softDelete = true;
    
    
    public static function getValidateRule(){
    	return array(
    		'doc_name' =>  'required',
    		'doc_department' => 'required',
    		'order_time' => 'required'
    	);
    }
    public static function getValidateError(){
    	return array(
    	'doc_name.required' => '专家不可为空！',
    	'doc_department.required' => '科室不可为空！',
    	'order_time.required' => '预约时间不可为空！'
    	);
    }
}