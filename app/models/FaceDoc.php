<?php
class FaceDoc  extends Eloquent {


	protected $table='face_doc';
	protected $guarded = array('id');
	public $timestamps = true;
//    protected $softDelete = true;
    
	public function scopeOrderByCreatedAt($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }
    
	//取得所有专家
	public static function findAllDoc($count = 0){
		if(!$count){
			$oDocs = FaceDoc::where('id','!=',17)->orderByCreatedAt()->get();
		}else{
			$oDocs = FaceDoc::where('id','!=',17)->orderByCreatedAt()->take($count)->get();
		}
		return $oDocs;
	}
	
	//验证字段
	public static function getDocRule(){
		return array(
				'doc_name'			=> 'required',
				'doc_thumb'			=> 'required',
				'doc_position'		=> 'required',
				'doc_department'	=> 'required',
				'doc_hospital'		=> 'required',
				'doc_introduction'	=> 'required',
		);
	}
	
	//添加专家
	public function AddDoc($aDoc){
		foreach($aDoc as $k=>$v){
			$this->$k = $v;
		}
		return $this;
	}
}