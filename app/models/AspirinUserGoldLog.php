<?php
class AspirinUserGoldLog  extends Eloquent {


	protected $table='aspirin_user_gold_log';
	protected $guarded = array('id');
	public $timestamps = true;
	
	/**
	 * 用户是否加过麦粒
	 * @param unknown $iUserId
	 * @param unknown $iCatId
	 * @param unknown $iSourceId
	 */
	public static function isUserAddGold($iUserId,$iCatId,$iSourceId){
		$oGoldLog = self::where('user_id',$iUserId)
							->where('gold_count','>',0)
							->where('gold_type',1)
							->where('source_type',$iCatId)
							->where('source_id',$iSourceId)
// 							->where('return_msg','success')
							->first();
		if($oGoldLog){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 用户是否减过麦粒
	 * @param unknown $iUserId
	 * @param unknown $iCatId
	 * @param unknown $iSourceId
	 */
	public static function isUserReduceGold($iUserId,$iCatId,$iSourceId){
		$oGoldLog = self::where('user_id',$iUserId)
							->where('gold_count','>',0)
							->where('gold_type',2)
							->where('source_type',$iCatId)
							->where('source_id',$iSourceId)
// 							->where('return_msg','success')
							->first();
		if($oGoldLog){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 获取用户已消耗麦粒数量
	 * @param unknown $iUserId
	 */
	public static function getUserUsedMailiCount($iUserId){
		$iCount = self::selectRaw('sum(`gold_count`) as total')
						->where('user_id',$iUserId)
						->where('gold_type',2)
						->pluck('total');
		return $iCount;
	}
}