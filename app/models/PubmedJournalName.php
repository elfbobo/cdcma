<?php
class PubmedJournalName extends Eloquent{
    protected $table = 'pubmed_journal_name';
    public $timestamps = false;
    protected $guarded = array('id');
    
}
