<?php
class ElearningPdf  extends Eloquent {


	protected $table='e_learning_pdf';
	protected $guarded = array('id');
	public $timestamps = true;
	public static function getPdfList($iPageSize){
		$oPdfList = self::orderBy('created_at','desc')->paginate($iPageSize);;
		return $oPdfList;
	}
}