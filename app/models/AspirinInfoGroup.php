<?php
class AspirinInfoGroup  extends Eloquent {

	protected $table='aspirin_info_group';
	protected $guarded = array('id');
	public $timestamps = true;
	
	public function group(){
		return $this->hasMany('AspirinInfo','group_id');
	}
}