<?php
class  MeetingOrder extends Eloquent {
	protected $table = 'meeting_order';
	public $timestamps = true;
	protected $guarded = array('id');
	 protected $softDelete = true;
}