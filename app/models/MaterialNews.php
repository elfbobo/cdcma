<?php
class MaterialNews extends Eloquent{
	protected $table = 'material_news';
	public $timestamps = false;
	protected $guarded = array('id');
	
	public function scopeSelNews($query,$title){
		return $title?$query->where('title','like','%'.$title.'%'):$query;
	}
	public static function rules(){
		return array(
			'title'=>'required',
			'temporary_flag'=>'required|in:0,1',
			'material_id'=>'required',
			'digest'=>'required',
			'content'=>'required'
		);
	}
	
	public static function errorMessages(){
		return array(
			'title.required'=>'标题不能为空',
			'temporary_flag.required'=>'请选择素材类型',
			'temporary_flag.in'=>'素材类型格式不正确',
			'material_id.required'=>'封面图片素材不能为空',
			'digest.required'=>'简介不能为空',
			'content.required'=>'详细内容不能为空'
		);
	}
}