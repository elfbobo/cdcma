<?php

class IntegralLog extends Eloquent {

	protected $table='user_health_integral_log';
	protected $guarded = array('id');
	public $timestamps = true;
	// protected $softDelete = true;
	
	public function scopeOrderByCreatedAt($query)
	{
		return $query->orderBy('created_at', 'DESC');
	}

	public function AddItem($aDoc){
		foreach($aDoc as $k=>$v){
			$this->$k = $v;
		}
		return $this;
	}

	//积分操作
	public function UpdateIntegral($iUid,$obj_id,$obj_type){
		//获取系统基础设置信息
        $obj_type = strtolower($obj_type);
		$Configs = new Configs();
		$configSys = $Configs::find(1);
		$oUser = User::select("health_integrals","health_integral","health_live_integral","health_video_integral")->find($iUid );
		if( !$oUser ){
			return array('isok'=>false, 'msg'=>'用户不存在');
			return $this->showMessage('用户不存在');
			// echo $this->formatRet(false, '用户不存在');
			// exit;
		}
		$uArr = array(
			'user_id'	=>	$iUid,
			'obj_id'	=>	$obj_id,
			'obj_type'	=>	$obj_type,
			'balance'	=>	$oUser->health_integral,
			'ctime'		=>	time()
		);
		$integralArr = array();
		$types = '';
		switch ($obj_type) {
			case 'video': //观看精品课程扣除
				$uArr['number'] = $configSys->view_video_deduct_integral;
				$uArr['types'] = $types = "out";
				$uArr['obj_title'] = FaceVideo::where('id',$obj_id)->pluck('video_title');
				$integralArr = array(
					'health_integral' => $oUser->health_integral-$configSys->view_video_deduct_integral,
				);
				break;
			case 'class': //参会赠送
				$uArr['number'] = $configSys->live_integral;
				$uArr['types'] = $types = "in";
				$uArr['obj_title'] = "参会赠送"; //FaceVideo::where('id',$oInput['obj_id'])->pluck('video_title');
				$integralArr = array(
					'health_integral' 		=> $oUser->health_integral+$configSys->live_integral,
					'health_integrals' 		=> $oUser->health_integrals+$configSys->live_integral,
					'health_live_integral'	=> $oUser->health_live_integral+$configSys->live_integral
				);
				break;
			case 'view': //观看健康教育视频赠送
				$uArr['number'] = $configSys->video_integral;
				$uArr['types'] = $types = "in";
				$uArr['obj_title'] = AspirinEducation::where('id',$obj_id)->pluck('ppt_title');
				$integralArr = array(
					'health_integral' 		=> $oUser->health_integral+$configSys->video_integral,
					'health_integrals' 		=> $oUser->health_integrals+$configSys->video_integral,
					'health_video_integral'	=> $oUser->health_video_integral+$configSys->video_integral
				);
				break;
			case 'buycme': //兑换好医生CME课程消耗
				$uArr['number'] = $configSys->cmc_use_integral;
				$uArr['obj_title'] = $obj_title = "购买好医生CME课程。";
				$uArr['types'] = $types = "out";
				$integralArr = array(
					'health_integral' => $oUser->health_integral-$configSys->cmc_use_integral,
				);
				break;
			case 'buykypx': //兑换科研培训消耗
				$uArr['number'] = $configSys->kypx_use_integral;
				$uArr['obj_title'] = $obj_title = "兑换科研培训。";
				$uArr['types'] = $types = "out";
				$integralArr = array(
					'health_integral' => $oUser->health_integral-$configSys->kypx_use_integral,
				);
				break;
		}
		if($integralArr['health_integral']<0){
			return array('isok'=>false, 'msg'=>'对不起，您的积分余额不足。');
			return $this->showMessage('对不起，您的积分余额不足。');
			// echo  $this->formatRet(false,"对不起，您的积分余额不足。");
			// exit;
		}
		$oUser->id = $iUid;
		foreach($integralArr as $k=>$v){
			$oUser->$k = $v;
		}
		if($oUser->save()){
			$this->AddItem($uArr)->save();
			return array('isok'=>true, 'msg'=>'');
			return true;
			// echo  $this->formatRet(true,"恭喜您，操作成功！");
			// exit;
		}else{
			return array('isok'=>false, 'msg'=>'对不起，操作失败。');
			return $this->showMessage('对不起，操作失败。');
			// echo  $this->formatRet(false,"对不起，操作失败。");
			// exit;
		}
	}
	
	//查看是否有该条记录
	public static function HasLog($iCatId,$iVideoId,$iType,$iUserId,$iLinkUId){
		$oVideoLog = VideoLog::where('obj_id',$iVideoId)
			->where('obj_type',$iType)
			->where('user_id',$iUserId)
			->first();
		if($oVideoLog){
			return $oVideoLog;
		}else{
			return false;
		}
	}
	
	public static function HasLogSign($iCatId,$iVideoId,$iType,$iUserId,$iLinkUId){
		$oVideoLog = VideoLog::where('obj_id',$iVideoId)
			->where('obj_type',$iType)
			->where('user_id',$iUserId)
			->first(); 
		if($oVideoLog){
			return $oVideoLog;
		}else{
			return false;
		}
	}
	
}