<?php

class TrainVideoZanLog extends Eloquent
{

    protected $table = 'train_video_zan_log';
    protected $guarded = array('id');
    public $timestamps = true;
//    protected $softDelete = true;

    //查看用户是否点赞
    public static function isUserZan($iVideoId, $iUserId, $iCatId = 1)
    {
        $oFaceVideoZanLog = self::where('video_id', $iVideoId)
            ->where('user_id', $iUserId)
            ->where('cat_id', $iCatId)
            ->first();
        if ($oFaceVideoZanLog) {
            return true;
        } else {
            return false;
        }
    }

    public function scopeSelzanlog($query, $iVideoId, $iCatId = 1)
    {
        return $query->where('video_id', '=', $iVideoId)->where('cat_id', $iCatId);
    }

    //点赞
    public static function UserZan($iVideoId, $iUserId, $iCatId = 1)
    {
        if ($iCatId == 1) {
            $oFaceVideo = TrainVideo::find($iVideoId);
            $oFaceVideo->increment('video_support');
        }
        $aFaceVideoZan = array(
            'video_id' => $iVideoId,
            'user_id' => $iUserId,
            'cat_id' => $iCatId
        );
        $oFaceVideoZan = new TrainVideoZanLog($aFaceVideoZan);
        $oFaceVideoZan->save();
    }
}