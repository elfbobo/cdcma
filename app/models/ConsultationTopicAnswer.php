<?php
class ConsultationTopicAnswer  extends Eloquent {


	protected $table='consultation_topic_answer';
	protected $guarded = array('id');
	public $timestamps = true;
//    protected $softDelete = true;
    
	public function scopeOrderByCreatedAt($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }
}