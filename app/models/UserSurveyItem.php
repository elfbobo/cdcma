<?php
class UserSurveyItem  extends Eloquent {


	protected $table='user_survey_item';
	protected $guarded = array('id');
	public $timestamps = true;
    public function scopeOrder($query)
    {
        return $query->orderBy('phase_id', 'asc');
    }
}