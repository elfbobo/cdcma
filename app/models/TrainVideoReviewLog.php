<?php

class TrainVideoReviewLog extends Eloquent
{

    protected $table = 'train_video_review_log';
    protected $guarded = array('id');
    public $timestamps = true;

    //操作
    public static function addUserReviewLog($iVideoId,$iUserId,$iMin,$type=0){
//		if($iMin>=20){
        $iMin = $iMin?$iMin:0;
        if($iMin>REVIEW_LIMIT){
            $iMin = REVIEW_LIMIT;
        }
        //该用户一个小时之内的记录是否存在
        $iOneHourAgo = time()-1*60*60;
        $sOneHourAgo = date('Y-m-d H:i:s',$iOneHourAgo);
        $oLog = TrainVideoReviewLog::where('user_id',$iUserId)
            ->where('video_id',$iVideoId)
            ->where('created_at','>',$sOneHourAgo)
            ->orderBY('created_at','DESC')
            ->first();
        if($oLog){
            if($oLog->watch_minutes<$iMin){
                $oLog->watch_minutes = $iMin;
                $oLog->save();
            }
        }else{
            //创建一条记录
            $aLog = array(
                'user_id'		=> $iUserId,
                'video_id'		=> $iVideoId,
                'watch_minutes'	=> $iMin,
                'type'			=> $type
            );
            $oLog = new TrainVideoReviewLog($aLog);
            $oLog->save();
        }
//		}

    }
}