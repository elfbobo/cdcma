<?php
class Information  extends Eloquent {


	protected $table='information';
	protected $guarded = array('id');
	public $timestamps = true;
    public function scopeOrder($query)
    {
        return $query->orderBy('created_at', 'desc');
    }
}