<?php
class CaseInfo extends Eloquent {


	protected $table='case_info';
	protected $guarded = array('id');
	public $timestamps = true;
    //protected $softDelete = true;
    
	public function scopeOrderByCreatedAt($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }
    
}