<?php
class AspirinScreening  extends Eloquent {


	protected $table='aspirin_screening';
	protected $guarded = array('id');
	public $timestamps = true;
	
	//选项值（0：默认值；1：否；2：是； 3：不清楚；）
	const SCREENING_DEFAULT = 0;
	const SCREENING_NO = 1;
	const SCREENING_YES = 2;
	const SCREENING_NONE = 3;
	
	public static function getRealAge($age,$key1,$key2)
	{
		$year = substr($age,0,strrpos($age,$key1));
		$month = mb_substr($age,strrpos($age,$key1)+1);
	    $month = substr($month,0,strrpos($month,$key2));
	    if($month <= 6){
	    	$age = date('Y',time())-$year;
	    }else{
	    	$age = date('Y',time())-$year-1;
	    }
	    if(date('Y',time())-$year == 0){
	    	$age = 1;
	    }
	    return $age;
	}
	//年龄得分
	public static function getAgeScore($sex,$age){
		$aAge = range(30, 74);
		//年龄数组拆分成9组数据
		$aAgeInfo = array_chunk($aAge,5);
		if(in_array($age,$aAgeInfo[0]) || $age<30){
			if($sex == 1){       //男
				$iAgeScore = -1;
			}elseif($sex == 0){   //女
				$iAgeScore = -9;
			}
		}elseif(in_array($age,$aAgeInfo[1])){
			if($sex == 1){
				$iAgeScore = 0;
			}elseif($sex == 0){
				$iAgeScore = -4;
			}
		}elseif(in_array($age,$aAgeInfo[2])){
			if($sex == 1){
				$iAgeScore = 1;
			}elseif($sex == 0){
				$iAgeScore = 0;
			}
		}elseif(in_array($age,$aAgeInfo[3])){
			if($sex == 1){
				$iAgeScore = 2;
			}elseif($sex == 0){
				$iAgeScore = 3;
			}
		}elseif(in_array($age,$aAgeInfo[4])){
			if($sex == 1){
				$iAgeScore = 3;
			}elseif($sex == 0){
				$iAgeScore = 6;
			}
		
		}elseif(in_array($age,$aAgeInfo[5])){
			if($sex == 1){
				$iAgeScore = 4;
			}elseif($sex == 0){
				$iAgeScore = 7;
			}
		}elseif(in_array($age,$aAgeInfo[6])){
			if($sex == 1){
				$iAgeScore = 5;
			}elseif($sex == 0){
				$iAgeScore = 8;
			}
		}elseif(in_array($age,$aAgeInfo[7])){
			if($sex == 1){
				$iAgeScore = 6;
			}elseif($sex == 0){
				$iAgeScore = 8;
			}
		}elseif(in_array($age,$aAgeInfo[8]) || $age>74){
			if($sex == 1){
				$iAgeScore = 7;
			}elseif($sex == 0){
				$iAgeScore = 8;
			}
		}
		return $iAgeScore;
	}
	//TC得分
	public static function getTcScore($sex,$tc){
			if ($sex == 0){        //女
				switch ($tc){
					case 1:
						$iTcScore = -2;
						break;
					case 2:case 6:case 0:
						$iTcScore = 0;
						break;
					case 3:
						$iTcScore = 1;
						break;
					case 4:
						$iTcScore = 1;
						break;
					case 5:
						$iTcScore = 3;
						break;
					default:
						break;
				}
			}else{            //男
				switch ($tc){
					case 1:
						$iTcScore = -3;
						break;
					case 2:case 6:case 0:
						$iTcScore = 0;
						break;
					case 3:
						$iTcScore = 1;
						break;
					case 4:
						$iTcScore = 2;
						break;
					case 5:
						$iTcScore = 3;
						break;
					default:
						break;
				}
			}
		return $iTcScore;
	}
	//血压得分
	public static function getBpScore($sex,$sbp,$dbp){
		$aSbp1 = range(120,129);
		$aSbp2 = range(130,139);
		$aSbp3 = range(140,159);
		//收缩压
		if($sbp == 0){
			$iSbpScore = 0;
		}
		if(in_array($sbp,$aSbp1)){
			$iSbpScore = 0;
		}elseif(in_array($sbp,$aSbp2)){
			if($sex == 1){
				$iSbpScore = 1;
			}else{
				$iSbpScore = 0;
			}
		}elseif(in_array($sbp,$aSbp3)){
			$iSbpScore = 2;
		}
		if($sbp >= 160){
			$iSbpScore = 3;
		}
		if($sbp < 120 && $sbp > 0){
			if($sex == 1){
				$iSbpScore = 0;
			}else{
				$iSbpScore = -3;
			}
		}
		
		//舒张压
		if($dbp == 1){
			if($sex == 1){
				$iDbpScore = 0;
			}else{
				$iDbpScore = -3;
			}
		}elseif($dbp == 2 || $dbp == 6 || $dbp == 0){
			$iDbpScore = 0;
		}elseif($dbp == 3){
			if($sex == 1){
				$iDbpScore = 1;
			}else{
				$iDbpScore = 0;
			}
		}elseif($dbp == 4){
			$iDbpScore = 2;
		}elseif($dbp == 5){
			$iDbpScore = 3;
		}
		
		//当舒张压和收缩压得分不一致时，取用高值
		$iBpScore = $iSbpScore>=$iDbpScore?$iSbpScore:$iDbpScore;
		return $iBpScore;
	}
	//糖尿病得分
	public static function getDmScore($sex,$diabates){
		if($diabates == 0 || $diabates == 1 || $diabates == 3){
			$iDmScore = 0;
		}else{
			if($sex == 1){
				$iDmScore = 2;
			}elseif($sex == 0){
				$iDmScore = 4;
			}
		}
		return $iDmScore;
	}
	//吸烟得分
	public static function getSmokeScore($smoke){
		if($smoke == 0 || $smoke == 1){
			$iSmokeScore = 0;
		}else{
			$iSmokeScore = 2;
		}
		return $iSmokeScore;
	}
	//评估CHD风险百分比
	public static function getPercent($sex,$iScore){
		if($sex == 1){
			$aPercent = array('0'=> '3%','1'=> '3%','2'=> '4%','3'=> '5%','4'=> '7%','5'=> '8%','6'=> '10%',
					'7'=> '13%','8'=> '16%','9'=> '20%','10'=> '25%','11'=> '31%','12'=> '37%','13'=> '45%');
			if($iScore <= -1){
				$sPercent = '2%';
			}elseif($iScore >= 14){
				$sPercent = '53%';
			}else{
				$sPercent = $aPercent[$iScore];
			}
		}else{
			$aPercent = array('-1'=> '2%','0'=> '2%','1'=> '2%','2'=> '3%','3'=> '3%','4'=> '4%','5'=> '4%','6'=> '5%',
					'7'=> '6%','8'=> '7%','9'=> '8%','10'=> '10%','11'=> '11%','12'=> '13%','13'=> '15%','14'=> '18%',
					'15'=> '20%','16'=> '24%',);
			if($iScore <= -2){
				$sPercent = '1%';
			}elseif($iScore >= 17){
				$sPercent = '27%';
			}else{
				$sPercent = $aPercent[$iScore];
			}
		}
		return $sPercent;
	}
	//获取界面呈现内容
	public static function getInfo($sPercent,$aDanger,$iBmi){
		$aDangerInfo = array(
				'高血压' => '降低血压',
				'高血脂' => '调整血脂',
				'糖尿病' => '控制血糖',
				'吸烟'  => '戒烟'
		);
		$aPre = array();
		$sPre = '';
		foreach($aDangerInfo as $k=>$v){
			if($aDanger && in_array($k,$aDanger)){
				$aPre[] = $v;
			}
		}
		if($iBmi < 18 || $iBmi > 25){
			$aPre[] = '控制体重';
		}
		if($aPre){
			$sPre = implode(',', $aPre);
		}
		$sPercent=str_replace('%','',$sPercent)*0.01;
		if($sPercent > 0.2){
			$aContent = array('3','高风险','您应针对'.$sPre.'，应用阿司匹林抗血小板治疗以降低心血管风险。');
		}elseif($sPercent < 0.1){
			$aContent = array('1','低风险','您应继续保持健康的生活方式，预防高血压，高血脂，高血糖。');
		}else{
			$aContent = array('2','中风险','您应针对'.$sPre.'，可考虑给予阿司匹林治疗。');
		}
		return $aContent;
	}
	//获取心血管危险因素
	public static function getDangerElement($iId)
	{
		$oScreening = AspirinScreening::find($iId);
		$aDanger = array();
		if($oScreening->question1 == AspirinScreening::SCREENING_YES){
			$aDanger[] = '吸烟';
		}
		if($oScreening->question2 == AspirinScreening::SCREENING_YES || $oScreening->question6 == AspirinScreening::SCREENING_YES || $oScreening->question7 == AspirinScreening::SCREENING_YES){
			$aDanger[] = '家族病史';
		}
		if($oScreening->question3 == AspirinScreening::SCREENING_YES){
			$aDanger[] = '糖尿病';
		}
		if($oScreening->question4 == AspirinScreening::SCREENING_YES){
			$aDanger[] = '高血脂';
		}
		if($oScreening->question5 == AspirinScreening::SCREENING_YES){
			$aDanger[] = '高血压';
		}
		if($oScreening->question0 == AspirinScreening::SCREENING_YES){
			$aDanger[] = '绝经';
		}
		return $aDanger;
	}
}