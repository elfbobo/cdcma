<?php

class TrainVideo extends Eloquent
{
    protected $table = 'train_video';
    protected $guarded = array('id');
    public $timestamps = true;
    protected $softDelete = true;

    public function scopeOrderByCreatedAt($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }

    //取得所有视频
    public static function findAllVideo()
    {
        return self::orderByCreatedAt();
    }

    //取得指定专家下的所有录播视频
    public static function findVideoByDocId($iDocId)
    {
        return self::orderByCreatedAt()->where('doc_id', $iDocId);
    }

    //验证字段
    public static function getVideoRule()
    {
        return array(
            'video_title' => 'required',
            'video_thumb' => 'required',
            'doc_id' => 'min:0',
            'video_introduce' => 'required',
            // 'video_url'				=> 'required',
        );
    }

    //添加视频
    public function AddVideo($aDoc)
    {
        foreach ($aDoc as $k => $v) {
            $this->$k = $v;
        }
        return $this;
    }

    public static function getFaceVideoReviewById($iId)
    {
        $oFaceVideo = TrainVideo::find($iId);
        if ($oFaceVideo) {
            $iDocId = $oFaceVideo->doc_id;
            $oDocInfo = FaceDoc::find($iDocId);
            if ($oDocInfo) {
                $oFaceVideo->doc_info = $oDocInfo;
            }
        }
        return $oFaceVideo;
    }
}