<?php
class AspirinScreeningLog  extends Eloquent {


	protected $table='aspirin_screening_log';
	protected $guarded = array('id');
	public $timestamps = true;
	
	const MAILI_LIMIT = 100;
}