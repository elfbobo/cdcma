<?php
class CommentZanLog  extends Eloquent {
	protected $table = 'comment_zan_log';
	public $timestamps = true;
	protected $guarded = array('id');
	
	public static function getCommentZanLog($comment_id,$user_id){
		return self::where('comment_id',$comment_id)->where('user_id',$user_id)->first();
	}
}