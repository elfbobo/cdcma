<?php
class AspirinOnlineLog  extends Eloquent {


	protected $table='aspirin_online_log';
	protected $guarded = array('id');
	public $timestamps = true;
	//直播停留够该时间（秒）加麦粒
	public static $iAddGoldSecond = 1200;
	
	/**
	 * 记录用户进入会议log
	 * @param unknown $iUserId
	 * @param unknown $iOnlineId
	 * @return boolean
	 */
	public static function addLog($iUserId,$iOnlineId){
		
		//判断是否有该条记录的log
		$oOnline = self::select('id')->where('online_id',$iOnlineId)->where('user_id',$iUserId)->first();
		if(!$oOnline){
			$aOnline = array(
					'online_id'	=> $iOnlineId,
					'user_id'	=> $iUserId
			);
			$oOnline = new self($aOnline);
			$oOnline->save();
		}
		//如果已经邀请了该用户,且未设置为已参加状态，设置已参加状态
		$oInvite = AspirinOnlineInvite::select('id','has_attend')
								->where('online_id',$iOnlineId)
								->where('user_id',$iUserId)
								->where('has_attend',0)
								->first();
		if($oInvite){
			$oInvite->has_attend = 1;
			$oInvite->save();
		}
		return true;
	}
	
	/**
	 * 直播停留时间统计
	 * @param unknown $iUserId
	 * @param unknown $iOnlineId
	 * @param number $iSecond
	 */
	public static function addTime($iUserId,$iOnlineId,$iSecond=0){
		//判断是否有该条记录的log
		$oOnline = self::select('id','online_time')->where('online_id',$iOnlineId)->where('user_id',$iUserId)->first();
		if(!$oOnline){
			$aOnline = array(
					'online_id'	=> $iOnlineId,
					'user_id'	=> $iUserId,
					'online_time'	=> $iSecond
			);
			$oOnline = new self($aOnline);
			$oOnline->save();
			//记录用户总停留时间
			$iTotalSecond = $iSecond;
		}else{
			$iTotalSecond = $iSecond+$oOnline->online_time;
			$oOnline->online_time = $iTotalSecond;
			$oOnline->save();
		}
		
		if($iTotalSecond>=self::$iAddGoldSecond){
			//给参与直播的用户加相应麦粒
			$iOnlineUserId = AspirinOnline::where('id',$iOnlineId)->pluck('user_id');
			if($iOnlineUserId == $iUserId){
				//组织者不加麦粒
				return ;
			}
// 			GoldService::addGold($iUserId, 11, $iOnlineId);
		}
		return true;
	}
	
}