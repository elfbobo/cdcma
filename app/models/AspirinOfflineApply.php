<?php
class AspirinOfflineApply  extends Eloquent {


	protected $table='aspirin_offline_apply';
	protected $guarded = array('id');
	public $timestamps = true;
	
	//审核状态（0：未审核；1：审核通过；2：审核未通过）
	const CHECK_TYPE_DEFAULT = 0;
	const CHECK_TYPE_SUCCESS = 1;
	const CHECK_TYPE_FAIL = 2;
}