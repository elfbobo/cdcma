<?php
class ElearningVideo  extends Eloquent {


	protected $table='e_learning_video';
	protected $guarded = array('id');
	public $timestamps = true;
	
	public static function getVideoList($iPageSize){
		$oVideoList = self::orderBy('created_at','desc')->paginate($iPageSize);;
		return $oVideoList;
	}
}