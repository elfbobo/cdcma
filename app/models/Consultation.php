<?php
class Consultation  extends Eloquent {


	protected $table='consultation';
	protected $guarded = array('id');
	public $timestamps = true;
//    protected $softDelete = true;
    
	public function scopeOrderByCreatedAt($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }
    
	//取得所有视频
	public static function findAllVideo($catid=0){
		if($catid){
			$oVideos = Consultation::where('meeting_type',$catid)->orderByCreatedAt();
		}else{
			$oVideos = Consultation::orderByCreatedAt();
		}
		return $oVideos;
	}
    
	//取得所有视频
	public static function findAllVideoCount($catid=0,$count = 0){
		if($catid){
			$oVideos = Consultation::select('consultation.*','face_doc.doc_name','face_doc.doc_thumb','face_doc.doc_position','face_doc.doc_department','face_doc.doc_hospital','face_doc.doc_introduction')
									->leftJoin('face_doc','consultation.expert_id','=','face_doc.id')
									->where('meeting_type',$catid)
									->orderBy('consultation.created_at', 'DESC')
									->take($count)
									->get();
		}else{
			$oVideos = Consultation::select('consultation.*','face_doc.doc_name','face_doc.doc_thumb','face_doc.doc_position','face_doc.doc_department','face_doc.doc_hospital','face_doc.doc_introduction')
									->leftJoin('face_doc','consultation.expert_id','=','face_doc.id')
									->orderBy('consultation.created_at', 'DESC')
									->take($count)
									->get();
		}
//		foreach ($oVideos as $k=>$v){
//			$iDocId = $v->expert_id;
//			$oDocInfo = FaceDoc::find($iDocId);
//			if($oDocInfo){
//				$oVideos[$k]->doc_info = $oDocInfo;
//			}
//		}
		return $oVideos;
	}
	
	//验证字段
	public static function getVideoRule(){
		return array(
				'meeting_title'			=> 'required',
				'meeting_url'			=> 'required',
//				'meeting_starttime'		=> 'required',
				'meeting_description'		=> 'required',
		);
	}
	
	//添加
	public function AddVideo($aVideo,$codeFlag=true){
		
		foreach($aVideo as $k=>$v){
			$this->$k = $v;
		}
		if($codeFlag){
			$this->meeting_code =  Consultation::MakeInviteCode();
		}
		//更新前台首页缓存内容
		//redis操作=======Start
		$iType = $this->meeting_type;
		switch($iType){
			case 2:
				if (Cache::has('Consultation2'))
				{
					Cache::forget('Consultation2');
				}
				break;
			case 3:
				if (Cache::has('Consultation3'))
				{
					Cache::forget('Consultation3');
				}
				break;
			case 4:
				if (Cache::has('Consultation4'))
				{
					Cache::forget('Consultation4');
				}
				break;
			case 5:
				if (Cache::has('Consultation5'))
				{
					Cache::forget('Consultation5');
				}
				break;
		}
		
		//END====
		return $this;
	}
	
	//生成邀请码
	public static function MakeInviteCode(){
		$s = User::GenerateStr(5);
		//该邀请码是否存在
		$iCount = Consultation::where('meeting_code',$s)->count();
		if($iCount==0){
			return $s;
		}else{
			return Consultation::MakeInviteCode();
		}
	}
	
	//获取指定类型最新一条数据
	public static function getLatestConsultation($type=1){
		
		$oVideos = Consultation::select('consultation.*','face_doc.doc_name','face_doc.doc_thumb','face_doc.doc_position','face_doc.doc_department','face_doc.doc_hospital','face_doc.doc_introduction')
									->leftJoin('face_doc','consultation.expert_id','=','face_doc.id')
									->where('meeting_type',$type)
									->orderBy('consultation.created_at', 'DESC')
									->first();
//		if($oVideos){
//			$iDocId = $oVideos->expert_id;
//			$oDocInfo = FaceDoc::find($iDocId);
//			if($oDocInfo){
//				$oVideos->doc_info = $oDocInfo;
//			}
//		}
		return $oVideos;
	}
	
	public static function getShowById($iId){
		$oConsultation = Consultation::find($iId);
		if($oConsultation){
			$iDocId = $oConsultation->expert_id;
			$oDocInfo = FaceDoc::find($iDocId);
			if($oDocInfo){
				$oConsultation->doc_info = $oDocInfo;
			}
		}
		return $oConsultation;
	}
}