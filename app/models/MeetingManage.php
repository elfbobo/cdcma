<?php
/**
 *                             _ooOoo_
 *                            o8888888o
 *                            88" . "88
 *                            (| -_- |)
 *                            O\  =  /O
 *                         ____/`---'\____
 *                       .'  \\|     |//  `.
 *                      /  \\|||  :  |||//  \
 *                     /  _||||| -:- |||||-  \
 *                     |   | \\\  -  /// |   |
 *                     | \_|  ''\---/''  |   |
 *                     \  .-\__  `-`  ___/-. /
 *                   ___`. .'  /--.--\  `. . __
 *                ."" '<  `.___\_<|>_/___.'  >'"".
 *               | | :  `- \`.;`\ _ /`;.`/ - ` : | |
 *               \  \ `-.   \_ __\ /__ _/   .-` /  /
 *          ======`-.____`-.___\_____/___.-`____.-'======
 *                             `=---='
 *          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *                     佛祖保佑        永无BUG
 *            佛曰:
 *                   写字楼里写字间，写字间里程序员；
 *                   程序人员写程序，又拿程序换酒钱。
 *                   酒醒只在网上坐，酒醉还来网下眠；
 *                   酒醉酒醒日复日，网上网下年复年。
 *                   但愿老死电脑间，不愿鞠躬老板前；
 *                   奔驰宝马贵者趣，公交自行程序员。
 *                   别人笑我忒疯癫，我笑自己命太贱；
 *                   不见满街漂亮妹，哪个归得程序员？
 */
class MeetingManage extends Eloquent
{
    protected $table = 'meeting_manage';
    // 集体赋值字段
    protected $fillable = array('type','start_date','end_date','title');
    // 集体赋值黑名单
    protected $guarded = array('id');

    public $timestamps = true;

    /**
     * 报名人数访问器
     * @param $value
     * @return mixed
     */
    public function getSignNumAttribute($value)
    {
        return MeetingManageSign::where('meeting_manage_id', $this->id)->count();
    }

    /**
     * 会议时长访问器
     * @param $value
     * @return mixed
     */
    public function getDateLenAttribute($value)
    {
        return floor((strtotime($this->end_date)-strtotime($this->start_date))/3600);
    }
}