<?php
class UserInformationLog  extends Eloquent {
	protected $table='user_information_log';
	protected $guarded = array('id');
	public $timestamps = true;
}