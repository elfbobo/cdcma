<?php
class ConsultationUserReg  extends Eloquent {


	protected $table='consultation_user_reg';
	protected $guarded = array('id');
	public $timestamps = true;
//    protected $softDelete = true;
    
	public function scopeOrderByCreatedAt($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }
}