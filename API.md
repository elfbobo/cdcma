# 国卫健康云 API 接口

### 自主会议相关

#### 课件列表

> 接口：[GET] /apiaspirinonline/ppt?catid=3&token=

参数：
```
catid 为自主会议分类 id （3：全科 1：心内；2：神内，4：神外）
```

响应 success:
```
{
    "success": true,
    "msg": "成功",
    "datalist": [
        {
            "id": 18,
            "ppt_title": "全科会议主题课件",
            "ppt_thumb": "http://cdcma.wenav.net/upload/online/20180925/201809251108288213_s.jpg",
            "created_at": "2018-09-25"
        }
    ]
}
```

#### 精彩回顾接口

> 接口：[GET] /apiaspirinonline/video-list?catid=1&token=

参数：
```
catid 为自主会议分类 id （3：全科 1：心内；2：神内，4：神外）
```

响应 success:
```
{
    "success": true,
    "msg": "成功",
    "datalist": [
        {
            "video_id": 29,
            "online_title": "从2016USPSTF建议声明更新再看阿司匹林一级预防",
            "online_thumb": "http://cdcma.wenav.net/upload/online/20170925/201709251818203980_s.jpg",
            "online_date": "2017-08-30",
            "video_hits": 269,
            "doc_name": "杨卫东",
            "doc_hospital": "邳州市人民医院"
        },
        {
            "video_id": 26,
            "online_title": "稳定性冠心病患者的抗血小板治疗",
            "online_thumb": "http://cdcma.wenav.net/upload/online/20170925/201709251826121096_s.jpg",
            "online_date": "2017-08-14",
            "video_hits": 45,
            "doc_name": "耿传良",
            "doc_hospital": "文登中心医院"
        },
        ...
    ]
}
```

#### 获取当前季度自主会议主题

> 接口：[GET] /apiaspirinonline/meeting-subject?catid=1&token=

参数：
```
catid 为自主会议分类 id
```

响应 success:
```
{
    "success": true,
    "msg": "ok",
    "detail": [
        {
            "ppt_id": 1,
            "ppt_title": "从2016USPSTF建议声明更新再看阿司匹林一级预防"
        },
        {
            "ppt_id": 2,
            "ppt_title": "阿司匹林用于心血管疾病一级预防的现状和挑战"
        },
        {
            "ppt_id": 4,
            "ppt_title": "阿司匹林120周年历史回顾"
        }
    ]
}
```

#### aspirin 自助会议的视频列表

> 接口：[GET] /apiaspirinonline/ad-list?token=

响应 success:
```
{
    "status": true,
    "msg": "ok",
    "index": 1, // 当前需播放的位置
    "list": [   // 广告视频列表
        {
            "id": "1",
            "title": "心肌梗死的一级预防",
            "index": "1",
            "url": "http://cdcma.bizconf.cn/upload/video/ad/1.mp4",
            "created_at": "2018-10-23 08:30"
        },
        {
            "id": "2",
            "title": "心肌梗死二级预防中的应用",
            "index": "2",
            "url": "http://cdcma.bizconf.cn/upload/video/ad/2.mp4",
            "created_at": "2018-10-23 08:34"
        },
        {
            "id": "3",
            "title": "预防心血管事件-01",
            "index": "3",
            "url": "http://cdcma.bizconf.cn/upload/video/ad/3.mp4",
            "created_at": "2018-10-23 08:34"
        },
        {
            "id": "4",
            "title": "预防心血管事件-02",
            "index": "4",
            "url": "http://cdcma.bizconf.cn/upload/video/ad/4.mp4",
            "created_at": "2018-10-23 08:35"
        },
        {
            "id": "5",
            "title": "预防心血管事件-03",
            "index": "5",
            "url": "http://cdcma.bizconf.cn/upload/video/ad/5.mp4",
            "created_at": "2018-10-23 08:35"
        },
        {
            "id": "6",
            "title": "卒中二级预防中的应用",
            "index": "6",
            "url": "http://cdcma.bizconf.cn/upload/video/ad/6.mp4",
            "created_at": "2018-10-23 08:35"
        }
    ]
}
```

#### aspirin 上报广告视频观看记录

> 接口：[POST] /apiaspirinonline/ad-log?index=&token=

参数：
```
index 为 当前播放视频的 index
```

响应 success:
```
{
    "status": true,
    "msg": "上报成功"
}
```

### 系统相关

#### 获取积分排行

> 接口：[POST] /apiuser/integralrank?_client=ios&token=

`参数：当 ios 系统时需传参数 _client=ios 来控制审核时名字中间星号表示`

响应 success:
```
{
    "success": true,
    "msg": "",
    "detail": {
        "user_rank": 0
    },
    "datalist": [
        {
            "role_id": 2,
            "user_nick": "周*",
            "user_id": 803,
            "live_min": "",
            "review_min": "",
            "user_score": ""
        },
        {
            "role_id": 2,
            "user_nick": "史*琴",
            "user_id": 384,
            "live_min": "",
            "review_min": "",
            "user_score": ""
        },
        ...
    ]
}
```

#### 获取系统基础设置信息

> 接口：[POST] /apiuser/sysinfo?notice_id=1&token=

`注：view_teacher_video_agreement_img 字段为新版签名协议图片，如果该字段为空则表示为旧签名业务`

响应 success:
```
{
    "success": true,
    "msg": "",
    "detail": {
        "health_integral": 0,
        "video_integral": 60,
        "live_integral": 10,
        "view_video_deduct_integral": 10,
        "cmc_use_integral": 60,
        "integral_tel": "010-85879900-8067",
        "exchange_rule_integral": "每参加一次自助会议积10分。",
        "teach_agreement": "讲师报名成功后，参与者满6人（含讲者），提醒讲者签署协议。",
        "view_teacher_video_agreement": "讲师录制的视频发布并审核通过后，观看人数达到10人，提醒讲者签署协议。",
        "view_teacher_video_agreement_img": "http://cdcma.wenav.net/upload/aspirineducation/agreement/361.jpg",
        "doc_agreement": "                                     知情同意声明\r\n本人已阅读本知情同意声明中的信息，并确认在为拜耳医药保健有限公司（“ 拜耳”）提供讲者或其他类型的服务时将履行以下职责：\r\n   本人提供的任何信息（包括但不限于讲课幻灯/会议的讲稿、录音录像、会议资料）在用于拜耳组织的会议时事先经过拜耳的审核以保证所提供的信息符合拜耳政策和中国法律法规。本人保证在提交资料时为拜耳保留充足的时间进行审核和批准，在未提前告知拜耳以保证拜耳有充足的重新审核时间的情况下，本人不会对已经过拜耳审核的最终资料进行任何实质性的修改（会议时间、会议地点、讲者姓名、书写错误等修改除外）。\r\n   本人同意在获知拜耳产品的所有不良事件（“AE”）和产品技术投诉（“PTC”）后的一（1）个工作日内，通过传真（010-8550 5141）或电子邮件（pv.china@bayer.com）以书面形式向拜耳报告。\r\n 本人不会在介绍拜耳产品的资料和讨论中包含任何拜耳产品说明书外的信息，除非中国法律法规允许并得到拜耳同意以纯学术交流为目的的情况。产品说明书外的信息包括但不限于未经批准的产品、未经批准的适应症、未经批准的适用人群、未经批准的服用剂量或未经批准的服用方法等信息。\r\n本文件旨在向您告知有关拜耳医药保健有限公司（“Bayer”）在收集、使用和保护个人信息方面的政策，并且，一旦签署，您将允许 Bayer 使用您的个人信息。请仔细阅读本知情同意，并根据您的需要询问任何事项。\r\n收集个人信息：\r\n您已被告知，并且您已同意 Bayer 将收集和使用您所提供的如下个人信息，包括：（1）您的姓\r\n名，（2）您工作所在单位，（3）您的手机号码，（4）您的身份证号，（5）您的开户银行及帐号，（6）您的腾讯微信号，和（7）您的电子邮箱地址。\r\n使用个人信息：\r\n基于您愿意与 Bayer 开展合作，以向拜耳提供专家服务，接受 Bayer 提供的科学信息和产品信息，并参加相关意见调查，Bayer 将为以下目的使用您的个人信息：（1）Bayer 与您之间的学术交流；（2）邀请您参加学术会议；（3）邀请您参加意见调查；（4）发送文件或材料；和（5）支付专家服务费用。\r\n向第三方披露个人信息：\r\nBayer 将向国内或国外的 Bayer 关联方或 Bayer 所委托的第三方披露您的个人信息，并且该等关联方或第三方将为本知情同意中所规定之目的使用您的个人信息。\r\n个人信息保护：\r\nBayer 已制定了相关政策并采取了相关安全措施，以保护您的个人信息免受未经授权的访问、篡\r\n改、泄露、毁损或丢失。Bayer 将与接收您个人信息的任何授权第三方订立书面合同，确保该第三方承担有关采取安全措施保护您个人信息的合同义务。您有权在任何时间通过拨打 Bayer 热线电话 4008100360，撤销您所作出的有关 Bayer 使用您个人信息的同意，或要求 Bayer 停止向您发送任何信息或材料。\r\n如果您在将来任何时间针对您的个人信息有任何问题或投诉，也请拨打 Bayer 热线电话\r\n4008100360。\r\n                                       专家同意声明\r\n本人已阅读本知情同意中的信息。本人已获得机会询问相关问题。本人同意提供本人的个人信息，以供 Bayer 根据本知情同意使用，并且同意 Bayer 可以根据本知情同意向经授权的第三方披露本人的个人信息。本人声明和保证，本人在向 Bayer 提供个人信息前，已获得本人所在单位的相关批准或许可（如需要），并且本人提供个人信息的行为不会违反适用的法律法规以及本人所在单位的规章制度。\r\n      _______________         _______________        ____________\r\n          专家签署                专家姓名                签署日期",
        "kypx_use_integral": 30
    }
}
```

#### 获取 ios 审核时的默认邀请码

> 接口：[GET] /apiuser/check-invite-code?_client=ios&_version=1.0.0

参数描述
```
_client: 客户端为 ios
_version: 为当前版本 
```

响应 success:
```
{
    "status": true,
    "msg": "ok",
    "invite_code": "8XLFZJ"    // 当 invite_code 不为空时跳过邀请码步骤，并且在后续的注册中自动填充该邀请码
}
```

### 精品课程

#### 精口课程视频列表（加入音频类型）

> 接口：[GET] /apivideo/recordfqlist?token=

响应 success：
```
{
    "success": true,
    "msg": "成功",
    "data": {
        "list": [
            {
                "id": 313,
                "video_title": "降压达标，至优之选",
                "video_type": "3", // 视频类型（1：录播；2：直播；3：音频）
                "video_thumb": "http://cdcma.bizconf.cn/upload/video/jpg/20180630/201806301514162792_s.jpg",
                "doc_name": "解德琼",
                "doc_hospital": "宜宾市第二人民医院",
                "created_at": {
                    "date": "2018-06-30 15:15:22.000000",
                    "timezone_type": 3,
                    "timezone": "PRC"
                }
            },
            ...
        ]
    }
```

#### 精口课程视频详情（加入音频类型）

> 接口：[GET] /apivideo/show?videoid=326&token=

响应 success：
```
{
    "success": true,
    "msg": "成功",
    "datalist": {
        "video_title": "张碧晨、赵丽颖 - 望",
        "video_type": "3", // 视频类型（1：录播；2：直播；3：音频）
        "video_thumb": "http://cdcma.bizconf.cn",
        "video_url": "http://cdcma.bizconf.cn/upload/video/mp3/20180918/201809181732144135_s.mp3",
        "video_introduce": "<p><span style=\"color: rgb(51, 51, 51); font-family: arial, 宋体, sans-serif; font-size: 14px; text-indent: 28px; background-color: rgb(255, 255, 255);\">《楚乔传》全剧音乐由上海谭旋音乐工作室量身打造。主题曲《望》唱出的是一种呐喊，一种信仰，是命如草芥的女奴星儿手握火炬，逐渐蜕变成英雄谍者楚乔的内心之路。 “荆棘之上仰望的人 会看透风尘 在这善恶彼伏的城 我与你共存”</span><a target=\"_blank\" href=\"https://baike.baidu.com/item/%E6%A5%9A%E4%B9%94/18224713\" data-lemmaid=\"18224713\" style=\"color: rgb(19, 110, 194); text-decoration-line: none; font-family: arial, 宋体, sans-serif; font-size: 14px; text-indent: 28px; white-space: normal; background-color: rgb(255, 255, 255); user-select: text !important;\">楚乔</a><span style=\"color: rgb(51, 51, 51); font-family: arial, 宋体, sans-serif; font-size: 14px; text-indent: 28px; background-color: rgb(255, 255, 255);\">是一朵开在荆棘里的花，然而她却坚定地仰望着天空，即使在这世界上有着层层的善与恶，她也誓死与国家与信仰与自己的所爱之人共存亡。要如何从卑微变成高贵？如何才能不再像狗一样的活着？楚乔手握信仰，目中有光，她相信“敢乱世为王 必掷血屠狼 才有希望”，才能复那血海深仇，才能迎来国之安康。她“生如蜉蝣却要活得荡气回肠 相信前方就是故乡”这缕星火虽小，但终可以燎原，只要怀揣信仰，就定可以成王</span></p>",
        "video_hits": 11,
        "zancount": 0,
        "iszan": false,
        "start_time": "2018-09-18 00:00:00",
        "end_time": "2018-09-18 17:39:55",
        "doc_name": "施仲伟",
        "doc_thumb": "http://cdcma.bizconf.cn/upload/doc/jpg/20180910/201809101031047293_s.jpg",
        "doc_position": "医学博士，主任医师",
        "doc_department": "心血管内科",
        "doc_hospital": "上海交通大学医学院附属瑞金医院",
        "doc_introduction": "<p style=\";line-height:150%;margin-top:0;margin-bottom:0;text-align:left;direction:ltr;unicode-bidi:embed;vertical-align: bottom\"><span style=\"color: black; font-size: 16px; font-family: 微软雅黑, &quot;Microsoft YaHei&quot;;\">上海交通大学医学院附属瑞金医院心脏科教授；</span></p><p style=\";line-height:150%;margin-top:0;margin-bottom:0;text-align:left;direction:ltr;unicode-bidi:embed;vertical-align: bottom\"><span style=\"color: black; font-size: 16px; font-family: 微软雅黑, &quot;Microsoft YaHei&quot;;\">中国老年学学会心脑血管病专业委员会常务委员；</span></p><p style=\";line-height:150%;margin-top:0;margin-bottom:0;text-align:left;direction:ltr;unicode-bidi:embed;vertical-align: bottom\"><span style=\"color: black; font-size: 16px; font-family: 微软雅黑, &quot;Microsoft YaHei&quot;;\">上海市超声质量控制中心专家委员会委员；</span></p><p style=\";line-height:150%;margin-top:0;margin-bottom:0;text-align:left;direction:ltr;unicode-bidi:embed;vertical-align: bottom\"><span style=\"color: black; font-size: 16px; font-family: 微软雅黑, &quot;Microsoft YaHei&quot;;\">国家自然科学基金委员会医学科学部评审专家；</span></p><p style=\";line-height:150%;margin-top:0;margin-bottom:0;text-align:left;direction:ltr;unicode-bidi:embed;vertical-align: bottom\"><span style=\"font-family: 微软雅黑, &quot;Microsoft YaHei&quot;; font-size: 16px; color: black;\">《中华心脏与心律电子杂志》、《中华医学杂志》、《中华内科杂志》等10多本杂志的副主编、编委或特约审稿专家</span></p><p style=\";line-height:150%;margin-top:0;margin-bottom:0;text-align:left;direction:ltr;unicode-bidi:embed;vertical-align: bottom\"><span style=\"font-family: 微软雅黑, &quot;Microsoft YaHei&quot;; color: black; font-size: 16px;\">发表论文500多篇，主编或参编《内科手册》、《心血管疾病诊治策略》、《中国国家处方集》、《临床超声影像学》等专著30多本；</span></p><p style=\"line-height: 150%; margin-top: 0px; margin-bottom: 0px; text-align: left; direction: ltr; unicode-bidi: embed; vertical-align: bottom;\"><span style=\"font-size: 16px; font-family: 微软雅黑, &quot;Microsoft YaHei&quot;;\"><span style=\"font-size: 16px; color: black;\">1996</span><span style=\"font-size: 16px; color: black;\">年获世界心血管超声学会和中华医学会联合授予</span><span style=\"font-size: 16px; color: black;\">“</span><span style=\"font-size: 16px; color: black;\">超声贡献奖</span><span style=\"font-size: 16px; color: black;\">”</span><span style=\"font-size: 16px; color: black;\">，</span><span style=\"font-size: 16px; color: black;\">2013</span><span style=\"font-size: 16px; color: black;\">年获美国心脏学会授予</span><span style=\"font-size: 16px; color: black;\">“</span><span style=\"font-size: 16px; color: black;\">国际交流奖</span><span style=\"font-size: 16px; color: black;\">”</span><span style=\"font-size: 16px; color: black;\">。</span></span></p>",
        "open_type": 1,
        "score": [],
        "comments": [],
        "share_flag": 1,
        "share_title": "张碧晨、赵丽颖 - 望",
        "share_thumb": "http://cdcma.bizconf.cn/assets/images/mobile/logo.png",
        "share_url": "http://cdcma.bizconf.cn/mobile-docface/review-show/326",
        "buyTips": 1,
        "buyTipsTxt": "观看此视频需消耗10积分，是否观看？"
    }
}
```

### 科研培训相关

#### 科研培训科室分类

> 接口：[GET] apitrainvideo/department?token=

响应 success：
```
{
    "success": true,
    "msg": "success",
    "datalist": [
        {
            "id": 1,
            "name": "内分泌科"
        },
        {
            "id": 2,
            "name": "心血管内科"
        },
        {
            "id": 3,
            "name": "神经内科"
        },
        {
            "id": 4,
            "name": "肾病内科"
        },
        {
            "id": 5,
            "name": "社区"
        }
    ]
}
```

#### 科研培训视频列表

> 接口 [GET] apitrainvideo/list?departmentid=&page=1&pagesize=10&token=

参数：`departmentid`为科室id,为空时返回全部

响应 success：
```
{
    "success": true,
    "msg": "成功",
    "data": {
        list": [
            {
                "id": 2,
                "video_title": "科研培训视频1号",
                "video_thumb": "http://cdcma.weui.net/upload/video/png/20180917/201809171451273858_s.png",
                "doc_name": "施仲伟",
                "doc_hospital": "上海交通大学医学院附属瑞金医院",
                "created_at": "2018-09-17 14:52:00"
            },
            {
                "id": 1,
                "video_title": "科研培训视频1号",
                "video_thumb": "http://cdcma.weui.net/upload/video/png/20180917/201809171451273858_s.png",
                "doc_name": "施仲伟",
                "doc_hospital": "上海交通大学医学院附属瑞金医院",
                "created_at": "2018-09-17 14:51:33"
            }
        ]
    }
}
```

#### 科研培训视频详情

> 接口 [GET] apitrainvideo/show?videoid=1&token=

参数：`videoid=1`

响应 success:
```
{
    "success": true,
    "msg": "成功",
    "datalist": {
        "video_title": "科研培训视频1号",
        "video_thumb": "http://cdcma.xxx.net/upload/video/png/20180917/201809171451273858_s.png",
        "video_url": "http://cdcma.xxx.net/upload/video/mp4/20180917/201809171451184707_s.mp4",
        "video_introduce": "<p>科研培训视频</p>",
        "video_hits": 11,
        "zancount": 0,
        "iszan": false,
        "doc_name": "施仲伟",
        "doc_thumb": "http://cdcma.xxx.net/upload/doc/jpg/20180910/201809101031047293_s.jpg",
        "doc_position": "医学博士，主任医师",
        "doc_department": "心血管内科",
        "doc_hospital": "上海交通大学医学院附属瑞金医院",
        "doc_introduction": "<p style=\";line-height:150%;margin-top:0;margin-bottom:0;text-align:left;direction:ltr;unicode-bidi:embed;vertical-align: bottom\"><span style=\"color: black; font-size: 16px; font-family: 微软雅黑, &quot;Microsoft YaHei&quot;;\">上海交通大学医学院附属瑞金医院心脏科教授；</span></p><p style=\";line-height:150%;margin-top:0;margin-bottom:0;text-align:left;direction:ltr;unicode-bidi:embed;vertical-align: bottom\"><span style=\"color: black; font-size: 16px; font-family: 微软雅黑, &quot;Microsoft YaHei&quot;;\">中国老年学学会心脑血管病专业委员会常务委员；</span></p><p style=\";line-height:150%;margin-top:0;margin-bottom:0;text-align:left;direction:ltr;unicode-bidi:embed;vertical-align: bottom\"><span style=\"color: black; font-size: 16px; font-family: 微软雅黑, &quot;Microsoft YaHei&quot;;\">上海市超声质量控制中心专家委员会委员；</span></p><p style=\";line-height:150%;margin-top:0;margin-bottom:0;text-align:left;direction:ltr;unicode-bidi:embed;vertical-align: bottom\"><span style=\"color: black; font-size: 16px; font-family: 微软雅黑, &quot;Microsoft YaHei&quot;;\">国家自然科学基金委员会医学科学部评审专家；</span></p><p style=\";line-height:150%;margin-top:0;margin-bottom:0;text-align:left;direction:ltr;unicode-bidi:embed;vertical-align: bottom\"><span style=\"font-family: 微软雅黑, &quot;Microsoft YaHei&quot;; font-size: 16px; color: black;\">《中华心脏与心律电子杂志》、《中华医学杂志》、《中华内科杂志》等10多本杂志的副主编、编委或特约审稿专家</span></p><p style=\";line-height:150%;margin-top:0;margin-bottom:0;text-align:left;direction:ltr;unicode-bidi:embed;vertical-align: bottom\"><span style=\"font-family: 微软雅黑, &quot;Microsoft YaHei&quot;; color: black; font-size: 16px;\">发表论文500多篇，主编或参编《内科手册》、《心血管疾病诊治策略》、《中国国家处方集》、《临床超声影像学》等专著30多本；</span></p><p style=\"line-height: 150%; margin-top: 0px; margin-bottom: 0px; text-align: left; direction: ltr; unicode-bidi: embed; vertical-align: bottom;\"><span style=\"font-size: 16px; font-family: 微软雅黑, &quot;Microsoft YaHei&quot;;\"><span style=\"font-size: 16px; color: black;\">1996</span><span style=\"font-size: 16px; color: black;\">年获世界心血管超声学会和中华医学会联合授予</span><span style=\"font-size: 16px; color: black;\">“</span><span style=\"font-size: 16px; color: black;\">超声贡献奖</span><span style=\"font-size: 16px; color: black;\">”</span><span style=\"font-size: 16px; color: black;\">，</span><span style=\"font-size: 16px; color: black;\">2013</span><span style=\"font-size: 16px; color: black;\">年获美国心脏学会授予</span><span style=\"font-size: 16px; color: black;\">“</span><span style=\"font-size: 16px; color: black;\">国际交流奖</span><span style=\"font-size: 16px; color: black;\">”</span><span style=\"font-size: 16px; color: black;\">。</span></span></p>",
        "score": [],
        "comments": [],
        "buyTips": 1,
        "buyTipsTxt": "观看此视频需消耗60积分，是否观看？"
    }
}
```
积分评论后
```
{
    "success": true,
    "msg": "成功",
    "datalist": {
        "video_title": "科研培训视频1号",
        "video_thumb": "http://cdcma.weui.net/upload/video/png/20180917/201809171451273858_s.png",
        "video_url": "http://cdcma.weui.net/upload/video/mp4/20180917/201809171451184707_s.mp4",
        "video_introduce": "<p>科研培训视频</p>",
        "video_hits": 17,
        "zancount": 1,
        "iszan": true,
        "doc_name": "施仲伟",
        "doc_thumb": "http://cdcma.weui.net/upload/doc/jpg/20180910/201809101031047293_s.jpg",
        "doc_position": "医学博士，主任医师",
        "doc_department": "心血管内科",
        "doc_hospital": "上海交通大学医学院附属瑞金医院",
        "doc_introduction": "<p style=\";line-height:150%;margin-top:0;margin-bottom:0;text-align:left;direction:ltr;unicode-bidi:embed;vertical-align: bottom\"><span style=\"color: black; font-size: 16px; font-family: 微软雅黑, &quot;Microsoft YaHei&quot;;\">上海交通大学医学院附属瑞金医院心脏科教授；</span></p><p style=\";line-height:150%;margin-top:0;margin-bottom:0;text-align:left;direction:ltr;unicode-bidi:embed;vertical-align: bottom\"><span style=\"color: black; font-size: 16px; font-family: 微软雅黑, &quot;Microsoft YaHei&quot;;\">中国老年学学会心脑血管病专业委员会常务委员；</span></p><p style=\";line-height:150%;margin-top:0;margin-bottom:0;text-align:left;direction:ltr;unicode-bidi:embed;vertical-align: bottom\"><span style=\"color: black; font-size: 16px; font-family: 微软雅黑, &quot;Microsoft YaHei&quot;;\">上海市超声质量控制中心专家委员会委员；</span></p><p style=\";line-height:150%;margin-top:0;margin-bottom:0;text-align:left;direction:ltr;unicode-bidi:embed;vertical-align: bottom\"><span style=\"color: black; font-size: 16px; font-family: 微软雅黑, &quot;Microsoft YaHei&quot;;\">国家自然科学基金委员会医学科学部评审专家；</span></p><p style=\";line-height:150%;margin-top:0;margin-bottom:0;text-align:left;direction:ltr;unicode-bidi:embed;vertical-align: bottom\"><span style=\"font-family: 微软雅黑, &quot;Microsoft YaHei&quot;; font-size: 16px; color: black;\">《中华心脏与心律电子杂志》、《中华医学杂志》、《中华内科杂志》等10多本杂志的副主编、编委或特约审稿专家</span></p><p style=\";line-height:150%;margin-top:0;margin-bottom:0;text-align:left;direction:ltr;unicode-bidi:embed;vertical-align: bottom\"><span style=\"font-family: 微软雅黑, &quot;Microsoft YaHei&quot;; color: black; font-size: 16px;\">发表论文500多篇，主编或参编《内科手册》、《心血管疾病诊治策略》、《中国国家处方集》、《临床超声影像学》等专著30多本；</span></p><p style=\"line-height: 150%; margin-top: 0px; margin-bottom: 0px; text-align: left; direction: ltr; unicode-bidi: embed; vertical-align: bottom;\"><span style=\"font-size: 16px; font-family: 微软雅黑, &quot;Microsoft YaHei&quot;;\"><span style=\"font-size: 16px; color: black;\">1996</span><span style=\"font-size: 16px; color: black;\">年获世界心血管超声学会和中华医学会联合授予</span><span style=\"font-size: 16px; color: black;\">“</span><span style=\"font-size: 16px; color: black;\">超声贡献奖</span><span style=\"font-size: 16px; color: black;\">”</span><span style=\"font-size: 16px; color: black;\">，</span><span style=\"font-size: 16px; color: black;\">2013</span><span style=\"font-size: 16px; color: black;\">年获美国心脏学会授予</span><span style=\"font-size: 16px; color: black;\">“</span><span style=\"font-size: 16px; color: black;\">国际交流奖</span><span style=\"font-size: 16px; color: black;\">”</span><span style=\"font-size: 16px; color: black;\">。</span></span></p>",
        "score": {
            "score1": 1,
            "score2": 2,
            "score3": 3
        },
        "comments": [
            {
                "comment_id": 653,
                "user_nick": "GIULL",
                "user_thumb": "http://cdcma.weui.net/assets/images/dafault/default_doc.jpg",
                "comment": "挺好的呀",
                "zan_count": 0,
                "device_type": 2,
                "created_at": "2018-09-17"
            }
        ],
        "buyTips": 1,
        "buyTipsTxt": "观看此视频需消耗30积分，是否观看？"
    }
}
```

#### 科研培训视频积分扣除

> 接口：[GET] apiuser/integraldo?obj_type=buykypx&obj_id=&token=

参数：`obj_type=buykypx & obj_id=` 为科研培训扣除

#### 科研培训视频点赞

> 接口：[POST] /apitrainvideo/surpport?videoid=1&token=

参数：`videoid=1`

响应 success：
```
{
    "success": true,
    "msg": "支持成功"
}
```

#### 科研培训星级评分

> 接口：[POST] /apitrainvideo/score?videoid=1&score1=1&score2=1&score3=1&device=2&token=

参数：
```
videoid=1   视频 id
score1=1    内容丰富程度评分（最高为5星）
score2=1    专家讲课水平（最高为5星）
score3=1    整体综合评分（最高为5星）
device=2    访问来源（1：pc；2：app；3：微信）
```

响应 success:
```
{
    "success": true,
    "msg": "成功"
}
```

#### 科研培训视频评论

> 接口：[POST] /apitrainvideo/comment?videoid=1&comment=&token=

参数：
```
videoid 视频 id
comment 评论内容
```

响应 success:
```
{
    "success": true,
    "msg": "评论成功"
}
```

#### 科研培训评论列表 --- 暂未使用

> 接口：[GET] /apitrainvideo/comment-list?videoid=&token=

参数：`videoid=`

响应 success：
```
{
    "success": true,
    "msg": "获取记录成功",
    "datalist": [
        {
            "id": 653,
            "user_nick": "GIULL",
            "user_thumb": "/assets/images/dafault/default_doc.jpg",
            "comment": "挺好的呀",
            "zan_count": 0,
            "device_type": 2,
            "created_at": {
                "date": "2018-09-17 16:48:39.000000",
                "timezone_type": 3,
                "timezone": "PRC"
            }
        }
    ]
}
```

#### 科研培训评论点赞

> 接口：[POST] /apitrainvideo/comment-surpport?commentid=653&token=

参数：`commentid=653`

响应 success：
```
{
    "success": true,
    "msg": "点赞成功"
}
```

#### 科研培训视频时间 --- 暂未使用

> 接口：[POST] /apitrainvideo/time?videoid=1&time=21&token=

参数：`videoid=1&time=21`

响应 success：
```
{
    "success": true,
    "msg": "成功"
}
```

### 积分兑换

#### 积分兑换 CME 课程

> 接口：[GET] /apiuser/integraldo?token=

参数：obj_type=buycme&obj_id=0

响应 success：
```
{
    "success": true,
    "msg": "恭喜您，操作成功！",
    "obj_id": 1
}
```

响应 error：{'success' => false, 'msg' => '对不起，操作失败。'}

#### CME课程完善信息

> 接口：[POST] /apimy/buycme-info?token=

参数：

```
obj_id|兑换 cme 课程时返回的 obj_id
name|姓名
workplace|工作单位
dept|科室
professional|职称
position|职务
telephone|联系电话
areacode|区号
postalcode|邮政编码
```

响应 success：
```
{
    "success": true,
    "msg": "保存成功"
}
```

响应 error：{'success' => false, 'msg' => '对不起，操作失败。'}

#### 积分兑换详情

> 接口：[GET] /apiuser/integral-list?token=

参数：obj_type=buycme&page=1&pagesize=10

`types in:赚取的积分 out:兑换的积分`

`obj_type 为空时返回全部消费记录`

1. buycme：兑换好医生CME课程消耗
2. view：观看健康教育视频赠送
3. class：参会赠送
4. video：观看精品课程扣除

响应 success：
```
{
    "status": true,
    "msg": "ok",
    "data": [
        {
            "id": 1,
            "user_id": 46420,
            "obj_type": "buycme",
            "obj_id": 1,
            "obj_title": "购买好医生CME课程。",
            "types": "out",
            "number": 60,   // 积分
            "balance": 6000,
            "content": "",
            "created_at": "2018-08-30 08:42:20",    // 创建时间
            "updated_at": "2018-08-30 08:42:20",
            "deleted_at": null,
            "ctime": 1535589740,
            "status": 1,    // 审核状态，0：待审核 1：已审核
            "check_time": "2018-08-30 09:40:13" // 审核时间，没通过审核时为 null
        }
    ]
}
```

### 会议管理相关

#### 获取当天会议列表

> 接口：[GET] /apimeeting/meeting-list?date=2018-09-06&type=0&page=1&pagesize=10&token=

参数：
```
type: 会议类型，默认 0 (0：院内会 1：科室会)
date：为查询的日期，留空时查询当天 例：2018-09-06
isme: 是否仅统计我的会议 默认 0 (当 isme 为1 时，type 参数无效)
```

响应 success:
```
{
    "status": true,
    "msg": "ok",
    "data": [
        {
            "id": 7,
            "type": 0,
            "title": "院内会标题",
            "user_id": 46420,
            "province_name": "北京",
            "city_name": "东城区",
            "meeting_manage_id": 6,
            "sign_date": "2018-09-06",  // 会议日期
            "start_time": "08:00",  // 会议时间
            "end_time": "09:30",  // 会议时间
            "province": 110000,
            "city": 110101,
            "hospital": "测试医院",
            "chairman": "张三",   // 主席
            "isupload": 1,  // 是否上传资料 0：未上传 1：已上传
            "created_at": "2018-09-06 17:03",
            "updated_at": "2018-09-06 17:03",
            "status": 1
        },
        ...
    ]
}
```

#### 获取每月的会议数统计

> 接口 [GET] /apimeeting/month-meeting?year=2018&month=09&type=0&token=

参数：
```
year：年，默认当前年
month: 月，默认当前月
type: 会议类型，默认 0 (0：院内会 1：科室会)
isme: 是否仅统计我的会议 默认 0 (当 isme 为1 时，type 参数无效)
```

```
{
    "status": true,
    "msg": "ok",
    "data": [
        {
            "num": 6,   // 数量
            "type": 0,  // 会议类型
            "day": "06" // 日期
        }
    ]
}
```

#### 安排会议中会议报名接口

> 接口：[POST] /apimeeting/sign-meeting&token=

Content-Type：`application/json`

参数示例：
```
{
	"meeting_manage_id": 6,
	"sign_date": "2018-09-06",  // 新增
    "start_time": "08:00",  // 新增
    "end_time": "09:30",  // 新增
	"province": 110000,
	"city": 110101,
	"hospital": "测试医院",
	"chairman": "张三",
	"subject": [
		{
			"subject": "第一主题",
			"speaker": "第一讲者",
			"start_time": "08:00",  // 新增
            "end_time": "09:30",  // 新增
		},
		{
			"subject": "第一主题",
			"speaker": "第一讲者",
			"start_time": "08:00",  // 新增
            "end_time": "09:30",  // 新增
		}
	]
}
```
参数描述：
```
meeting_manage_id        待报名的会议 id
sign_date  会议日期 （Y-m-d 格式）
start_time  会议时间 （H:i 格式）
end_time  会议时间 （H:i 格式）
province    省 id
city        市 id
hospital    医院
chairman    主席 （科室会留空）

speaker    讲者
subject    主题
```

响应 success:
```
{
    "status": true,
    "msg": "报名成功"
}
```

#### 获取会议详情

> 接口 [GET] /apimeeting/meeting-info?id=&token=

参数：

```
id 会议 id
more 是否需要返回上传文件信息 默认为 0  （0：不需要 1:需要）
```

响应 success:
```
{
    "status": true,
    "msg": "ok",
    "data": {
        "id": 13,
        "user_id": 47092,
        "meeting_manage_id": 2,
        "sign_date": "2018-09-06",
        "start_time": "08:00",
        "end_time": "09:30",
        "province": 110000,
        "city": 110101,
        "hospital": "测试医院",
        "chairman": "张三",
        "isupload": 1,
        "created_at": "2018-10-01 08:37:33",
        "updated_at": "2018-10-04 09:10:21",
        "status": 1,
        "type": 1,
        "title": "张三",
        "province_name": "北京",
        "city_name": "东城区",
        "isedit": 0,    // 1是可编辑 0不可编辑
        "isown": true,
        "subject": [
            {
                "id": 16,
                "meeting_sign_id": 13,
                "user_id": null,
                "start_time": "08:00",
                "end_time": "09:30",
                "speaker": "第一讲者",
                "subject": "第一主题",
                "created_at": "2018-10-01 08:37:34",
                "updated_at": "2018-10-01 08:37:34",
                "status": 1
            },
            {
                "id": 17,
                "meeting_sign_id": 13,
                "user_id": null,
                "start_time": "08:00",
                "end_time": "09:30",
                "speaker": "第二讲者",
                "subject": "第二主题",
                "created_at": "2018-10-01 08:37:34",
                "updated_at": "2018-10-01 08:37:34",
                "status": 1
            }
        ]
    }
}
```

响应 (带文件信息) success:
```
{
    "status": true,
    "msg": "ok",
    "data": {
        "id": 13,
        "user_id": 47092,
        "meeting_manage_id": 2,
        "sign_date": "2018-09-06",
        "start_time": "08:00",
        "end_time": "09:30",
        "province": 110000,
        "city": 110101,
        "hospital": "测试医院",
        "chairman": "张三",
        "isupload": 1,
        "created_at": "2018-10-01 08:37:33",
        "updated_at": "2018-10-04 09:10:21",
        "status": 1,
        "type": 1,
        "title": "张三",
        "province_name": "北京",
        "city_name": "东城区",
        "isedit": 0,
        "isown": true,
        "subject": [
            {
                "id": 16,
                "meeting_sign_id": 13,
                "user_id": null,
                "start_time": "08:00",
                "end_time": "09:30",
                "speaker": "第一讲者",
                "subject": "第一主题",
                "created_at": "2018-10-01 08:37:34",
                "updated_at": "2018-10-01 08:37:34",
                "status": 1,
                "agreement": [
                    {
                        "id": 22,
                        "type": "agreement",
                        "thumb": "",
                        "filepath": "http://cdcma.weui.net/upload/meeting/20181004/201810040910027105.jpg",
                        "ext": "jpg",
                        "created_at": "2018-10-04 09:10:02"
                    }
                ],
                "bank": [
                    {
                        "id": 23,
                        "type": "bank",
                        "thumb": "",
                        "filepath": "http://cdcma.weui.net/upload/meeting/20181004/201810040910087273.jpg",
                        "ext": "jpg",
                        "created_at": "2018-10-04 09:10:08"
                    }
                ],
                 "idcard": [
                     {
                         "id": 3715,
                         "type": "idcard",
                         "thumb": "",
                         "filepath": "http://cdcma.wenav.net/upload/meeting/20181008/201810080909088963.png",
                         "ext": "png",
                         "created_at": "2018-10-08 09:09:08"
                     },
                     {
                         "id": 3716,
                         "type": "idcard",
                         "thumb": "",
                         "filepath": "http://cdcma.wenav.net/upload/meeting/20181008/201810080909092386.png",
                         "ext": "png",
                         "created_at": "2018-10-08 09:09:09"
                     }
                 ]
            },
            {
                "id": 17,
                "meeting_sign_id": 13,
                "user_id": null,
                "start_time": "08:00",
                "end_time": "09:30",
                "speaker": "第二讲者",
                "subject": "第二主题",
                "created_at": "2018-10-01 08:37:34",
                "updated_at": "2018-10-01 08:37:34",
                "status": 1
            }
        ],
        "video": [
            {
                "id": 19,
                "type": "video",
                "thumb": "http://cdcma.weui.net/upload/meeting/20181004/201810040905038221.jpg",
                "filepath": "http://cdcma.weui.net/upload/meeting/20181004/201810040905038221.mp4",
                "ext": "mp4",
                "created_at": "2018-10-04 09:05:03"
            }
        ],
        "photo": [
            {
                "id": 20,
                "type": "photo",
                "thumb": "",
                "filepath": "http://cdcma.weui.net/upload/meeting/20181004/201810040905453675.jpg",
                "ext": "jpg",
                "created_at": "2018-10-04 09:05:45"
            }
        ],
        "signin": [
            {
                "id": 21,
                "type": "signin",
                "thumb": "",
                "filepath": "http://cdcma.weui.net/upload/meeting/20181004/201810040905554633.jpg",
                "ext": "jpg",
                "created_at": "2018-10-04 09:05:55"
            }
        ],
         "meals": [
             {
                 "id": 24,
                 "type": "meals",
                 "thumb": "",
                 "filepath": "http://cdcma.weui.net/upload/meeting/20181004/201810040910147093.jpg",
                 "ext": "jpg",
                 "created_at": "2018-10-04 09:10:14"
             }
         ]
    }
}
```

#### 安排会议中的会议列表

> 接口 [GET] /apimeeting/list?token=

参数：

```
date  查询的日期 例：2018-09-09
```

响应 success:
```
{
    "status": true,
    "msg": "ok",
    "data": [
        {
            "id": 5,    // 会议 id
            "type": 1,  // 0：院内会 1：科室会
            "title": "科室会标题",
            "start_date": "2018-09-01 00:00:00",    // 由日期类型修改为时间类型
            "end_date": "2019-09-01 00:00:00",  // 修改类型为时间
            "is_sign": 0,   // 我是否报名
            "status": 1
        },
        ...
    ]
}
```

#### 安排会议中已报名的会议列表

> 接口 [GET] /apimeeting/sign-meeting-list?date=2018-09-06&page=1&pagesize=10token=

参数：
```
date=2018-09-06 // date 为查询的日期，留空为当天
page=1
pagesize=10
```

响应 success:
```
{
    "status": true,
    "msg": "ok",
    "data": [
        {
            "id": 6,    // 报名 id
            "type": 0,  // 会议类型
            "title": "院内会标题",   // 会议标题
            "province_name": "北京",  
            "city_name": "东城区",
            "meeting_manage_id": 6, // 会议模板 id
            "sign_date": "2018-09-06",  // 会议日期
            "start_time": "08:00",  // 会议时间
            "end_time": "09:30",  // 会议时间
            "province": 110000,
            "city": 110101,
            "hospital": "测试医院", // 医院
            "chairman": "张三",   // 主席
            "isupload": 1,  // 是否上传资料 0：未上传 1：已上传
            "created_at": "2018-09-06 15:45:52",
            "updated_at": "2018-09-07 09:51:28",
            "status": 1
        },
        ...
    ]
}
```

#### 我的会议列表

> 接口 [GET] /apimeeting/my-meeting-list?page=1&pagesize=10token=

参数：
```
page=1
pagesize=10
```

响应 success:
```
{
    "status": true,
    "msg": "ok",
    "data": [
        {
            "id": 6,    // 报名 id
            "type": 0,  // 会议类型
            "title": "院内会标题",   // 会议标题
            "province_name": "北京",  
            "city_name": "东城区",
            "meeting_manage_id": 6, // 会议模板 id
            "sign_date": "2018-09-06",  // 会议日期
            "start_time": "08:00",  // 会议时间
            "end_time": "09:30",  // 会议时间
            "province": 110000,
            "city": 110101,
            "hospital": "测试医院", // 医院
            "chairman": "张三",   // 主席
            "isupload": 1,  // 是否上传资料 0：未上传 1：已上传
            "created_at": "2018-09-06 15:45:52",
            "updated_at": "2018-09-07 09:51:28",
            "status": 1
        },
        ...
    ]
}
```

#### 安排会议中会议编辑资料接口

> 接口：[POST] /apimeeting/edit-sign?token=

Content-Type：`application/json`

参数示例：
```
{
    "id" => 1,
	"meeting_manage_id": 6,
	"sign_date": "2018-09-06",  // 新增
    "start_time": "08:00",  // 新增
    "end_time": "09:30",  // 新增
	"province": 110000,
	"city": 110101,
	"hospital": "测试医院",
	"chairman": "张三",
	"subject": [
		{
			"subject": "第一主题",
			"speaker": "第一讲者",
			"start_time": "08:00",  // 新增
            "end_time": "09:30",  // 新增
		},
		{
			"subject": "第一主题",
			"speaker": "第一讲者",
			"start_time": "08:00",  // 新增
            "end_time": "09:30",  // 新增
		}
	]
}
```
参数描述：
```
id          报名的 id
meeting_manage_id        待报名的会议 id
sign_date  会议日期 （Y-m-d 格式）
start_time  会议时间 （H:i 格式）
end_time  会议时间 （H:i 格式）
province    省 id
city        市 id
hospital    医院
chairman    主席 （科室会留空）

speaker    讲者
subject    主题
```

响应 success:
```
{
    "status": true,
    "msg": "修改成功"
}
```

#### 安排会议中会议资料上传接口

> 接口：[POST] /apimeeting/meeting-data-upload?token=

Content-Type：`multipart/form-data`

参数描述：
```
type        上传的文件归类（
                video：现场讲课视频5-10秒（须包含讲者，听众和PPT），
                meals：餐费表
                photo：现场照片
                schedule: 会议日程
                agreement：专家服务协议，
                signin：签到表，
                speaker：讲者身份证正反面复印件 （内容详情中展示时使用的是 idcard 字段）
                bank：银行卡正面复印件（在复印件上写明开户行名称和专家姓名）
            ）
sign_id     报名会议的 id
subject_id  讲者主题 id（服务协议，银行卡，身份证正反面资料时需传）
file        待上传的文件
```

Android 参考代码:
```
OkHttpClient client = new OkHttpClient();

MediaType mediaType = MediaType.parse("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
RequestBody body = RequestBody.create(mediaType, "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"file\"\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"type\"\r\n\r\nchairman\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"sign_id\"\r\n\r\n10\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--");
Request request = new Request.Builder()
  .url("http://cdcma.wenav.net/apimeeting/meeting-data-upload?token={{token}}")
  .post(body)
  .addHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
  .addHeader("Cache-Control", "no-cache")
  .build();

Response response = client.newCall(request).execute();
```

Objective-C 参考代码:
```
#import <Foundation/Foundation.h>

NSDictionary *headers = @{ @"content-type": @"multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                           @"Cache-Control": @"no-cache" };
NSArray *parameters = @[ @{ @"name": @"file", @"fileName": @"" },
                         @{ @"name": @"type", @"value": @"chairman" },
                         @{ @"name": @"sign_id", @"value": @"10" } ];
NSString *boundary = @"----WebKitFormBoundary7MA4YWxkTrZu0gW";

NSError *error;
NSMutableString *body = [NSMutableString string];
for (NSDictionary *param in parameters) {
    [body appendFormat:@"--%@\r\n", boundary];
    if (param[@"fileName"]) {
        [body appendFormat:@"Content-Disposition:form-data; name=\"%@\"; filename=\"%@\"\r\n", param[@"name"], param[@"fileName"]];
        [body appendFormat:@"Content-Type: %@\r\n\r\n", param[@"contentType"]];
        [body appendFormat:@"%@", [NSString stringWithContentsOfFile:param[@"fileName"] encoding:NSUTF8StringEncoding error:&error]];
        if (error) {
            NSLog(@"%@", error);
        }
    } else {
        [body appendFormat:@"Content-Disposition:form-data; name=\"%@\"\r\n\r\n", param[@"name"]];
        [body appendFormat:@"%@", param[@"value"]];
    }
}
[body appendFormat:@"\r\n--%@--\r\n", boundary];
NSData *postData = [body dataUsingEncoding:NSUTF8StringEncoding];

NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://cdcma.wenav.net/apimeeting/meeting-data-upload?token={{token}}"]
       cachePolicy:NSURLRequestUseProtocolCachePolicy
   timeoutInterval:10.0];
[request setHTTPMethod:@"POST"];
[request setAllHTTPHeaderFields:headers];
[request setHTTPBody:postData];

NSURLSession *session = [NSURLSession sharedSession];
NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"%@", error);
        } else {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSLog(@"%@", httpResponse);
        }
    }];
[dataTask resume];
```

响应 success:
```
{
    "status": true,
    "msg": "上传成功",
    "data": {
        "id": 18,
        "meeting_sign_id": "1",
        "type": "video",
        "ext": "mp4",
        "thumb": "http://cdcma.xxx.net/upload/meeting/20180915/201809151658178250.jpg",
        "filepath": "http://cdcma.xxx.net/upload/meeting/20180915/201809151658178250.mp4",
        "finish": false,    // 是否全部上传完成
        "error": [  // 错误详情, 当全部上传完成时 error 为空数组
            "专家服务协议不能为空",
            "签到表不能为空",
            "讲者身份证正反面复印件不能为空",
            "银行卡正面复印件不能为空",
            "会议日程不能为空",
            "现场照片不能为空"
        ]
    }
}
```

#### 安排会议中会议删除附件接口

> 接口：[GET] /apimeeting/del-data-file?id=17&token=

参数描述：
`id 上传附件后返回的 id`

响应 success:
```
{
    "status": true,
    "msg": "附件删除完成"
}
```

#### 获取会议资料列表

> 接口：[GET] /apimeeting/material-list?page=1&pagesize=10&token=

参数：
```
page：页码，从1开始
pagesize: 每页的条数，默认10
```

响应 success:
```
{
    "status": true,
    "msg": "ok",
    "data": [
        {
            "id": 4,
            "title": "从CHANCE到POINT寻找最佳抗血小板策略之路",
            "type": "0", // 0:ppt 1:视频
            "filepath": "http://cdcma.wenav.net/upload/meeting/manager/20181019/201810191504358385.pptx",
            "ext": "pptx",
            "extra": null,
            "created_at": "2018-10-19 15:04:38",
            "updated_at": "2018-10-19 15:04:38",
            "status": 1
        },
        {
            "id": 3,
            "title": "阿司匹林是否被新型的抗血小板药物所取代-卒中？180827",
            "type": "0",
            "filepath": "http://cdcma.wenav.net/upload/meeting/manager/20181019/201810191503543798.pptx",
            "ext": "pptx",
            "extra": null,
            "created_at": "2018-10-19 15:04:22",
            "updated_at": "2018-10-19 15:04:22",
            "status": 1
        },
        {
            "id": 2,
            "title": "阿司匹林是否被新型的抗血小板药物所取代",
            "type": "1",
            "filepath": "http://cdcma.wenav.net/upload/meeting/manager/20181019/201810191502492583.mp4",
            "ext": "mp4",
            "extra": null,
            "created_at": "2018-10-19 15:02:54",
            "updated_at": "2018-10-19 15:02:54",
            "status": 1
        },
        {
            "id": 1,
            "title": "从CHANCE到POINT寻找最佳抗血小板策略之路",
            "type": "1",
            "filepath": "http://cdcma.wenav.net/upload/meeting/manager/20181019/201810191458067408.mp4",
            "ext": "mp4",
            "extra": null,
            "created_at": "2018-10-19 14:58:09",
            "updated_at": "2018-10-19 14:58:09",
            "status": 1
        }
    ]
}
```

#### 会议资料发送
> 接口：[GET] /apimeeting/send-material?id=&mail=&token=

参数：
```
id：资料id
mail: 接收资料的邮箱
```

响应 success:
```
{
    "status": true,
    "msg": "文件已发送"
}
```