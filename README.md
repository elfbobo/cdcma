# 国卫健康云

#### 项目介绍
{**以下是码云平台说明，您可以替换为您的项目简介**
码云是开源中国推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用码云实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 接口描述

1. apiaspirinonline/ppt  讲者获取ppt列表
2. apiaspirinonline/enter-list  讲者报名时间列表
3. apiaspirinonline/speaker-enter  判断讲者是否有报名权限
4. apiaspirinonline/speaker-submit  讲者报名信息提交
5. apiaspirinonline/join-list  听者报名参会列表
6. apiaspirinonline/listener-enter 判断听者是否可以报名
7. apiaspirinonline/listener-submit  听者确认报名信息提交